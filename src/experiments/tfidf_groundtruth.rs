/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use cleanutils;
use std::collections::btree_map::BTreeMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::PathBuf;

#[derive(Debug, Clone)]
pub struct TFIDFGroundTruth {
    // CVE ID -> List<CPE URLs>
    pub cpe_urls: BTreeMap<String, Vec<CpeUrl>>,
}

#[derive(Debug, Clone)]
pub struct CpeUrl {
    pub software_vendor: String,
    pub software_name: String,
}

impl TFIDFGroundTruth {
    pub fn new(groundtruth_repository: &String) -> TFIDFGroundTruth {
        println!("Generating TFIDF groundtruth...");

        let mut result = TFIDFGroundTruth {
            cpe_urls: BTreeMap::new(),
        };

        let daily_files = provide_list_files(groundtruth_repository);

        for daily_file in daily_files {
            let f = File::open(daily_file).unwrap();
            let file = BufReader::new(f);

            let mut current_cve: String = "CVE-NULL".to_string();
            let mut current_cpe_urls = Vec::new();

            for l in file.lines() {
                let line = l.unwrap();

                if line.starts_with("CVE-") {
                    if current_cpe_urls.len() > 0 {
                        result
                            .cpe_urls
                            .insert(current_cve.clone(), current_cpe_urls.clone());
                    }

                    current_cve = line;
                    current_cpe_urls = Vec::new();
                } else {
                    current_cpe_urls.push(parse_cpe_url(line));
                }
            }
            if current_cpe_urls.len() > 0 {
                result
                    .cpe_urls
                    .insert(current_cve.clone(), current_cpe_urls.clone());
            }
        }

        println!(
            "TFIDF groundtruth generated. {} CPE URIs were imported.",
            result.cpe_urls.len()
        );

        result
    }
}

fn parse_cpe_url(cpe_url: String) -> CpeUrl {
    let splitted_url = cpe_url.split(':').collect::<Vec<&str>>();

    let result = CpeUrl {
        software_vendor: cleanutils::clean_document(&splitted_url[3].to_string()),
        software_name: cleanutils::clean_document(&splitted_url[4].to_string()),
    };

    result
}

fn provide_list_files(groundtruth_repository: &String) -> Vec<PathBuf> {
    let repo_path = PathBuf::from(&groundtruth_repository);

    if repo_path.is_dir() {
        let mut files = Vec::new();

        for file in std::fs::read_dir(repo_path).unwrap() {
            let filepath = file.unwrap().path();
            files.push(filepath.clone());
        }

        files
    } else {
        panic!("The repository should be a directory !");
    }
}
