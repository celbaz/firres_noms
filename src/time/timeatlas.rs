/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use chrono::Duration;
use chrono::NaiveDate;
use config::{FirresConfig, TimeAtlasConfig};
use time::timeframe::TimeFrame;

pub struct TimeAtlas {
    start_date: NaiveDate,
    days_per_iteration: u8,
    nb_iterations: u32,
    metadata_delay: u8,
}

impl TimeAtlas {
    pub fn new(config: &TimeAtlasConfig) -> TimeAtlas {
        let start_date = NaiveDate::parse_from_str(&config.start_date, "%Y-%m-%d").unwrap();

        TimeAtlas {
            start_date,
            days_per_iteration: config.days_per_iteration,
            nb_iterations: config.nb_iterations,
            metadata_delay: config.metadata_delay,
        }
    }

    pub fn get_next_frame(
        &self,
        previous_frame: TimeFrame,
        firres_config: &FirresConfig,
    ) -> TimeFrame {
        let next_frame_id = previous_frame.frame_id + 1;

        let next_date = previous_frame
            .current_date
            .checked_add_signed(Duration::days(self.days_per_iteration as i64))
            .unwrap();

        // Time frames are memory intensive, not super cool to have several of them in memory so we drop the previous one first
        drop(previous_frame);

        TimeFrame::new(next_frame_id, next_date, self.metadata_delay, firres_config)
    }

    pub fn precache_atlas(&self, firres_config: &FirresConfig) {
        let mut current_date = self.start_date;

        for i in 0..self.nb_iterations {
            TimeFrame::new(i, current_date, self.metadata_delay, firres_config);
            current_date = current_date
                .checked_add_signed(Duration::days(self.days_per_iteration as i64))
                .unwrap();
        }
    }

    pub fn fetch_frame(&self, frame_id: u32, firres_config: &FirresConfig) -> TimeFrame {
        if frame_id >= self.nb_iterations {
            panic!(
                "Asked for time frame # {} but there are only {} frames in this atlas !",
                frame_id, self.nb_iterations
            );
        }

        let frame_date = self
            .start_date
            .checked_add_signed(Duration::days(
                (self.days_per_iteration as u32 * frame_id) as i64,
            ))
            .unwrap();

        println!("Fetching frame at date {}...", &frame_date);

        TimeFrame::new(frame_id, frame_date, self.metadata_delay, firres_config)
    }

    pub fn get_nb_days_per_frame(&self) -> u8 {
        self.days_per_iteration
    }
}
