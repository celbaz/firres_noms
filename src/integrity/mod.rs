/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use chrono::NaiveDate;
use config::IntegrityConfig;
use config::{CVEConfig, IndexConfig};
use learning::index::WordIndex;
use vulnerability::cve;
use vulnerability::cve::CVEDictionary;

pub fn check_unmatched_vulnerabilities(cve_dictionary: &CVEDictionary, word_index: &WordIndex) {
    let mut without_keyword_count = 0;
    for entry in &cve_dictionary.entries {
        let index_features = cve::compute_cve_index_score_vector(&entry.words, word_index);

        let has_no_match = index_features.sorted_keys().is_empty();
        if has_no_match {
            without_keyword_count += 1;
            println!("=================");
            println!(
                "Vulnerability {} did not match with any keyword.",
                entry.cve_id
            );
            println!("{}", entry.description);
            println!("=================");
        }
    }
    println!(
        "There are {} vulnerabilities out of {} that did not match with any keyword.",
        without_keyword_count,
        cve_dictionary.entries.len()
    );
}

pub fn check_dictionary_determinism(
    integrity_config: &IntegrityConfig,
    cve_config: &CVEConfig,
    index_config: &IndexConfig,
    cve_dictionary: &CVEDictionary,
    cpe_index: &WordIndex,
    cpe_date_threshold: &NaiveDate,
) {
    if integrity_config.check_cve_dictionary_determinism {
        println!("Recomputing the CVE dictionary from scratch a second time to assert that its generation is deterministic");

        let cve_dictionary2 =
            cve::build_cve_dictionary(cpe_index, cve_config, index_config, cpe_date_threshold).0;

        assert_eq!(cve_dictionary.entries.len(), cve_dictionary2.entries.len());

        for i in 0..cve_dictionary.entries.len() {
            let first = &cve_dictionary.entries[i];
            let second = &cve_dictionary2.entries[i];

            assert_eq!(first.cve_id, second.cve_id);
            assert_eq!(first.description, second.description);
            assert_eq!(first.words, second.words);
        }
    }
}
