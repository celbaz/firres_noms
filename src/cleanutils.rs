/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use regex::Regex;

pub fn clean_word(original_word: String) -> String {
    let mut word = original_word;

    word = word.to_lowercase();
    word = word.trim().to_string();

    word
}

pub fn clean_document(original_document: &String) -> String {
    let mut cleaned_document = original_document.replace("/", " "); // We replace slash by spaces so URl will be splitted by individual path fragments

    lazy_static! {
        // I hate regexes. They are unreadable without explanations.
        // Here we replace any dot ('.') that is NOT preceded by number, by a space. Eg a version number such as 1.2 is kept, but "microsoft.com" becomes "microsoft com".
        // The (?P<first>) group is there to keep the character BEFORE the dot in the replacement (eg "microsoft.com" doesn't become "microsof com")
        // More infos at https://docs.rs/regex/1.1.0/regex/struct.Regex.html#method.replace
        static ref REG_REMOVE_SOME_DOTS: Regex = Regex::new(r"(?P<first>[^0-9])\.").unwrap();
        static ref REG_REMOVE_SOME_DASHS: Regex = Regex::new(r"(?P<first>[^0-9])-").unwrap();
        static ref REG_REMOVE_NON_ALPHANUMERIC: Regex = Regex::new(r"[^a-zA-Z0-9\.-]").unwrap();
    }

    cleaned_document = REG_REMOVE_SOME_DOTS
        .replace_all(&cleaned_document, "$first ")
        .to_string();

    cleaned_document = REG_REMOVE_SOME_DASHS
        .replace_all(&cleaned_document, "$first ")
        .to_string();

    cleaned_document = REG_REMOVE_NON_ALPHANUMERIC
        .replace_all(&cleaned_document, " ")
        .to_string();

    cleaned_document
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cleaning() {
        assert_eq!("baobab", clean_word("Baobab".to_string()));
        assert_eq!(
            "baobab",
            clean_word(clean_document(&"BaoBaB!!".to_string()))
        );

        assert_eq!(
            "test microsoft com test version 31.25",
            clean_document(&"test.microsoft.com/test version 31.25".to_string())
        );

        assert_eq!(
            "test microsoft com test with dash version 31.25-7",
            clean_document(&"test.microsoft.com/test-with-dash version 31.25-7".to_string())
        );

        assert_eq!(
            "href  http   microsoft ",
            clean_document(&"href=\"http://microsoft\"".to_string())
        );
    }
}
