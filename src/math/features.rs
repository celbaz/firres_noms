/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::collections::BTreeMap;

#[derive(Debug, Clone, PartialEq)]
pub struct Features {
    full_feature_length: u32,
    sorted_keys: Vec<u32>,
    data: BTreeMap<u32, f64>,
    squared_norm: f64,
}

#[derive(Debug, Clone)]
pub struct Feature {
    pub feature_id: u32,
    pub value: f64,
}

impl Features {
    pub fn sorted_keys(&self) -> &Vec<u32> {
        &self.sorted_keys
    }

    pub fn get_feature_value(&self, feature_id: u32) -> f64 {
        match self.data.get(&feature_id) {
            Some(res) => *res,
            None => 0.0,
        }
    }

    pub fn new(full_feature_length: u32, features_input: Vec<Feature>) -> Features {
        let mut result = Features {
            full_feature_length: full_feature_length,
            sorted_keys: Vec::new(),
            data: BTreeMap::new(),
            squared_norm: 0.0,
        };

        for feature_input in features_input {
            if feature_input.value != 0.0 as f64 {
                result.sorted_keys.push(feature_input.feature_id);
                result
                    .data
                    .insert(feature_input.feature_id, feature_input.value);
                result.squared_norm += feature_input.value * feature_input.value;
            }
        }

        result.sorted_keys.sort();

        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_features() {
        let features_input = vec![
            Feature {
                feature_id: 0,
                value: 46.7,
            },
            Feature {
                feature_id: 1,
                value: 5.7,
            },
            Feature {
                feature_id: 4,
                value: -1.7,
            },
        ];

        let features = Features::new(5, features_input);

        assert_eq!(3, features.sorted_keys().len());
        assert_eq!(5.7, features.get_feature_value(1));
    }
}
