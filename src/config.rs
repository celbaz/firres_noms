/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use config_crate;

#[derive(Debug, Deserialize)]
pub struct FirresConfig {
    pub cpe: CPEConfig,
    pub cve: CVEConfig,
    pub integrity: IntegrityConfig,
    pub time: TimeAtlasConfig,

    pub experiments: ExperimentConfig,
}

#[derive(Debug, Deserialize)]
pub struct TimeAtlasConfig {
    pub start_date: String,
    pub days_per_iteration: u8,
    pub nb_iterations: u32,
    pub metadata_delay: u8,
    pub start_interval_begin: u32,
    pub start_interval_end: u32,
}

#[derive(Debug, Deserialize)]
pub struct IntegrityConfig {
    pub check_cve_dictionary_determinism: bool,
}

#[derive(Debug, Deserialize)]
pub struct CPEConfig {
    pub repository: String,
}

#[derive(Debug, Deserialize)]
pub struct CVEConfig {
    pub repository: String,
}

#[derive(Debug, Deserialize)]
pub struct ExperimentConfig {
    pub tfidf: TFIDFXPConfig,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TFIDFXPConfig {
    pub automated_starting_frame_id: u32,
    pub automated_evaluation_nb_frames: u32,
    pub keyword_truncation: f64,
    pub groundtruth_repository: String,
    pub index: IndexConfig,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IndexConfig {
    pub consecutive_words: u8,
    pub boost_multiple_words: bool,
    pub boost_capitalized_words: bool,
    pub boost_lib: bool,
}

impl FirresConfig {
    pub fn new() -> FirresConfig {
        let mut config = config_crate::Config::default();
        config
            .merge(config_crate::File::with_name("firres"))
            .unwrap();

        config.try_into().unwrap()
    }
}
