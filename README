---------------------------------------------------------------------------------
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

---------------------------------------------------------------------------------

Firres ('FIRst RESponder') is an experimental companion software to the paper ' Automated Keyword Extraction from "One-day" Vulnerabilities at Disclosure ' by Clément Elbaz (Inria [1]), Louis Rilling (DGA, [2]), and Christine Morin (Inria, [1]).
Contact us at
[1] firstname.lastname@inria.fr
[2] firstname.lastname@irisa.fr

Firres is written in Rust. It was tested on Linux. There are no known obstacles to run in on Mac OS X or Windows, but no guarantees either.

How to run :

1. Make sure you have a working Rust developement environment.
See instructions at https://www.rust-lang.org/tools/install

2. Optionnaly, edit the firres.toml file and enable or disable the proposed heuristics (boost_multiple_words, boost_capitalized_words, boost_lib).

3. Compile and run Firres in release mode. In shell at the root of this repository, enter :
cargo run --release

4. Choose an experiment name. This will create a directory of the same name in experiments/.

5. The experiment will take several hours to complete.

6. In a shell, go to the experiments/ directory, then enter :
./xp_summarize.sh <experiment name>

This will display multiple metrics and create additional CSV metrics files. You can compare them to our own results :
 - Base TF-IDF weighting:       experiments 20190913_xp1_formal_noboosts_delay60
 - Capitalization heuristic:    20190913_xp2_formal_capitalization_delay60
 - Multiple words heuristic:    20190913_xp3_formal_multiplewords_delay60
 - Lib heuristic:               20190913_xp4_formal_lib_delay60
 - All heuristics combined:     20190913_xp5_formal_allboosts_delay60