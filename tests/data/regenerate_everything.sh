#!/bin/bash
rm -rf CVE_CPE_URLs
rm -rf CVE_DESCRIPTIONS
rm -rf CPE_URL_KEYWORDS

mkdir CVE_CPE_URLs
mkdir CVE_DESCRIPTIONS
mkdir CPE_URL_KEYWORDS

echo "Importing data from year 2007..."
./fullextract_nvd_file.sh nvdcve-1.0-2007.json

echo "Importing data from year 2008..."
./fullextract_nvd_file.sh nvdcve-1.0-2008.json

echo "Importing data from year 2009..."
./fullextract_nvd_file.sh nvdcve-1.0-2009.json

echo "Importing data from year 2010..."
./fullextract_nvd_file.sh nvdcve-1.0-2010.json

echo "Importing data from year 2011..."
./fullextract_nvd_file.sh nvdcve-1.0-2011.json

echo "Importing data from year 2012..."
./fullextract_nvd_file.sh nvdcve-1.0-2012.json

echo "Importing data from year 2013..."
./fullextract_nvd_file.sh nvdcve-1.0-2013.json

echo "Importing data from year 2014..."
./fullextract_nvd_file.sh nvdcve-1.0-2014.json

echo "Importing data from year 2015..."
./fullextract_nvd_file.sh nvdcve-1.0-2015.json

echo "Importing data from year 2016..."
./fullextract_nvd_file.sh nvdcve-1.0-2016.json

echo "Importing data from year 2017..."
./fullextract_nvd_file.sh nvdcve-1.0-2017.json

echo "Importing data from year 2018..."
./fullextract_nvd_file.sh nvdcve-1.0-2018.json