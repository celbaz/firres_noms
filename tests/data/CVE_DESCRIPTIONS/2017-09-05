CVE-2016-3086
The YARN NodeManager in Apache Hadoop 2.6.x before 2.6.5 and 2.7.x before 2.7.3 can leak the password for credential store provider used by the NodeManager to YARN Applications.
CVE-2017-1000083
backend/comics/comics-document.c (aka the comic book backend) in GNOME Evince before 3.24.1 allows remote attackers to execute arbitrary commands via a .cbt file that is a TAR archive containing a filename beginning with a "--" command-line option substring, as demonstrated by a --checkpoint-action=exec=bash at the beginning of the filename.
CVE-2017-1097
IBM Emptoris Strategic Supply Management Platform 10.0.0.x through 10.1.1.x is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 120657.
CVE-2017-1129
IBM Notes 8.5 and 9.0 is vulnerable to a denial of service. If a user is persuaded to click on a malicious link, it could cause the Notes client to hang and have to be restarted. IBM X-Force ID: 121370.
CVE-2017-1130
IBM Notes 8.5 and 9.0 is vulnerable to a denial of service. If a user is persuaded to click on a malicious link, it would open up many file select dialog boxes which would cause the client hang and have to be restarted. IBM X-Force ID: 121371.
CVE-2017-14108
libgedit.a in GNOME gedit through 3.22.1 allows remote attackers to cause a denial of service (CPU consumption) via a file that begins with many '\0' characters.
CVE-2017-14140
The move_pages system call in mm/migrate.c in the Linux kernel before 4.12.9 doesn't check the effective uid of the target process, enabling a local attacker to learn the memory layout of a setuid executable despite ASLR.
CVE-2017-14145
HelpDEZk 1.1.1 has SQL Injection in app\modules\admin\controllers\loginController.php via the admin/login/getWarningInfo/id/ PATH_INFO, related to the selectWarning function.
CVE-2017-14146
HelpDEZk 1.1.1 allows remote authenticated users to execute arbitrary PHP code by uploading a .php attachment and then requesting it in the helpdezk\app\uploads\helpdezk\attachments\ directory.
CVE-2017-14149
GoAhead 3.4.0 through 3.6.5 has a NULL Pointer Dereference in the websDecodeUrl function in http.c, leading to a crash for a "POST / HTTP/1.1" request.
CVE-2017-14151
An off-by-one error was discovered in opj_tcd_code_block_enc_allocate_data in lib/openjp2/tcd.c in OpenJPEG 2.2.0. The vulnerability causes an out-of-bounds write, which may lead to remote denial of service (heap-based buffer overflow affecting opj_mqc_flush in lib/openjp2/mqc.c and opj_t1_encode_cblk in lib/openjp2/t1.c) or possibly remote code execution.
CVE-2017-14152
A mishandled zero case was discovered in opj_j2k_set_cinema_parameters in lib/openjp2/j2k.c in OpenJPEG 2.2.0. The vulnerability causes an out-of-bounds write, which may lead to remote denial of service (heap-based buffer overflow affecting opj_write_bytes_LE in lib/openjp2/cio.c and opj_j2k_write_sot in lib/openjp2/j2k.c) or possibly remote code execution.
CVE-2017-14156
The atyfb_ioctl function in drivers/video/fbdev/aty/atyfb_base.c in the Linux kernel through 4.12.10 does not initialize a certain data structure, which allows local users to obtain sensitive information from kernel stack memory by reading locations associated with padding bytes.
CVE-2017-14158
Scrapy 1.4 allows remote attackers to cause a denial of service (memory consumption) via large files because arbitrarily many files are read into memory, which is especially problematic if the files are then individually written in a separate thread to a slow storage resource, as demonstrated by interaction between dataReceived (in core/downloader/handlers/http11.py) and S3FilesStore.
CVE-2017-14159
slapd in OpenLDAP 2.4.45 and earlier creates a PID file after dropping privileges to a non-root account, which might allow local users to kill arbitrary processes by leveraging access to this non-root account for PID file modification before a root script executes a "kill `cat /pathname`" command, as demonstrated by openldap-initscript.
CVE-2017-1457
IBM QRadar Network Security 5.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128376.
CVE-2017-1458
IBM QRadar Network Security 5.4 is vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 128377.
CVE-2017-1491
IBM QRadar Network Security 5.4 supports interaction between multiple actors and allows those actors to negotiate which algorithm should be used as a protection mechanism such as encryption or authentication, but it does not select the strongest algorithm that is available to both parties. IBM X-Force ID: 128689.
CVE-2017-2779
An exploitable memory corruption vulnerability exists in the RSRC segment parsing functionality of LabVIEW 2017, LabVIEW 2016, LabVIEW 2015, and LabVIEW 2014.  A specially crafted Virtual Instrument (VI) file can cause an attacker controlled looping condition resulting in an arbitrary null write.  An attacker controlled VI file can be used to trigger this vulnerability and can potentially result in code execution.
CVE-2017-2807
An exploitable buffer overflow vulnerability exists in the tag parsing functionality of Ledger-CLI 3.1.1. A specially crafted journal file can cause an integer underflow resulting in code execution. An attacker can construct a malicious journal file to trigger this vulnerability.
CVE-2017-2808
An exploitable use-after-free vulnerability exists in the account parsing component of the Ledger-CLI 3.1.1. A specially crafted ledger file can cause a use-after-free vulnerability resulting in arbitrary code execution. An attacker can convince a user to load a journal file to trigger this vulnerability.
CVE-2017-2821
An exploitable use-after-free exists in the PDF parsing functionality of Lexmark Perspective Document Filters 11.3.0.2400 and 11.4.0.2452. A crafted PDF document can lead to a use-after-free resulting in direct code execution.
CVE-2017-2822
An exploitable code execution vulnerability exists in the image rendering functionality of Lexmark Perceptive Document Filters 11.3.0.2400. A specifically crafted PDF can cause a function call on a corrupted DCTStream to occur, resulting in user controlled data being written to the stack. A maliciously crafted PDF file can be used to trigger this vulnerability.
CVE-2017-2862
An exploitable heap overflow vulnerability exists in the gdk_pixbuf__jpeg_image_load_increment functionality of Gdk-Pixbuf 2.36.6. A specially crafted jpeg file can cause a heap overflow resulting in remote code execution. An attacker can send a file or url to trigger this vulnerability.
CVE-2017-2870
An exploitable integer overflow vulnerability exists in the tiff_image_parse functionality of Gdk-Pixbuf 2.36.6 when compiled with Clang. A specially crafted tiff file can cause a heap-overflow resulting in remote code execution. An attacker can send a file or a URL to trigger this vulnerability.
CVE-2017-5698
Intel Active Management Technology, Intel Standard Manageability, and Intel Small Business Technology firmware versions 11.0.25.3001 and 11.0.26.3000 anti-rollback will not prevent upgrading to firmware version 11.6.x.1xxx which is vulnerable to CVE-2017-5689 and can be performed by a local user with administrative privileges.
CVE-2017-5716
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2017-12865.  Reason: This candidate is a reservation duplicate of CVE-2017-12865.  Notes: All CVE users should reference CVE-2017-12865 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
