CVE-2018-12416
The GridServer Broker and GridServer Director components of TIBCO Software Inc.'s TIBCO DataSynapse GridServer Manager contain vulnerabilities which may allow an unauthenticated user to perform cross-site request forgery (CSRF). Affected releases are TIBCO Software Inc. TIBCO DataSynapse GridServer Manager: versions up to and including 5.2.0; 6.0.0; 6.0.1; 6.0.2; 6.1.0; 6.1.1; 6.2.0; 6.3.0.
CVE-2018-14655
A flaw was found in Keycloak 3.4.3.Final, 4.0.0.Beta2, 4.3.0.Final. When using 'response_mode=form_post' it is possible to inject arbitrary Javascript-Code via the 'state'-parameter in the authentication URL. This allows an XSS-Attack upon succesfully login.
CVE-2018-14657
A flaw was found in Keycloak 4.2.1.Final, 4.3.0.Final. When TOPT enabled, an improper implementation of the Brute Force detection algorithm will not enforce its protection measures.
CVE-2018-14658
A flaw was found in JBOSS Keycloak 3.2.1.Final. The Redirect URL for both Login and Logout are not normalized in org.keycloak.protocol.oidc.utils.RedirectUtils before the redirect url is verified. This can lead to an Open Redirection attack
CVE-2018-15452
A vulnerability in the DLL loading component of Cisco Advanced Malware Protection (AMP) for Endpoints on Windows could allow an authenticated, local attacker to disable system scanning services or take other actions to prevent detection of unauthorized intrusions. To exploit this vulnerability, the attacker would need to have administrative credentials on the Windows system. The vulnerability is due to the improper validation of resources loaded by a system process at run time. An attacker could exploit this vulnerability by crafting a malicious DLL file and placing it in a specific location on the targeted system. A successful exploit could allow the attacker to disable the targeted system's scanning services and ultimately prevent the system from being protected from further intrusion. There are no workarounds that address this vulnerability.
CVE-2018-15771
Dell EMC RecoverPoint versions prior to 5.1.2.1 and RecoverPoint for VMs versions prior to 5.2.0.2 contain an information disclosure vulnerability. A malicious boxmgmt user may potentially be able to determine the existence of any system file via Boxmgmt CLI.
CVE-2018-15772
Dell EMC RecoverPoint versions prior to 5.1.2.1 and RecoverPoint for VMs versions prior to 5.2.0.2 contain an uncontrolled resource consumption vulnerability. A malicious boxmgmt user may potentially be able to consume large amount of CPU bandwidth to make the system slow or to determine the existence of any system file via Boxmgmt CLI.
CVE-2018-15795
Pivotal CredHub Service Broker, versions prior to 1.1.0, uses a guessable form of random number generation in creating service broker's UAA client. A remote malicious user may guess the client secret and obtain or modify credentials for users of the CredHub Service.
CVE-2018-16470
There is a possible DoS vulnerability in the multipart parser in Rack before 2.0.6. Specially crafted requests can cause the multipart parser to enter a pathological state, causing the parser to use CPU resources disproportionate to the request size.
CVE-2018-16471
There is a possible XSS vulnerability in Rack before 2.0.6 and 1.6.11. Carefully crafted requests can impact the data returned by the `scheme` method on `Rack::Request`. Applications that expect the scheme to be limited to 'http' or 'https' and do not escape the return value could be vulnerable to an XSS attack. Note that applications using the normal escaping mechanisms provided by Rails may not impacted, but applications that bypass the escaping mechanisms, or do not use them may be vulnerable.
CVE-2018-16850
postgresql before versions 11.1, 10.6 is vulnerable to a to SQL injection in pg_upgrade and pg_dump via CREATE TRIGGER ... REFERENCING. Using a purpose-crafted trigger definition, an attacker can cause arbitrary SQL statements to run, with superuser privileges.
CVE-2018-17187
The Apache Qpid Proton-J transport includes an optional wrapper layer to perform TLS, enabled by use of the 'transport.ssl(...)' methods. Unless a verification mode was explicitly configured, client and server modes previously defaulted as documented to not verifying a peer certificate, with options to configure this explicitly or select a certificate verification mode with or without hostname verification being performed. The latter hostname verifying mode was not implemented in Apache Qpid Proton-J versions 0.3 to 0.29.0, with attempts to use it resulting in an exception. This left only the option to verify the certificate is trusted, leaving such a client vulnerable to Man In The Middle (MITM) attack. Uses of the Proton-J protocol engine which do not utilise the optional transport TLS wrapper are not impacted, e.g. usage within Qpid JMS. Uses of Proton-J utilising the optional transport TLS wrapper layer that wish to enable hostname verification must be upgraded to version 0.30.0 or later and utilise the VerifyMode#VERIFY_PEER_NAME configuration, which is now the default for client mode usage unless configured otherwise.
CVE-2018-17614
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Losant Arduino MQTT Client prior to V2.7. User interaction is not required to exploit this vulnerability. The specific flaw exists within the parsing of MQTT PUBLISH packets. The issue results from the lack of proper validation of the length of user-supplied data prior to copying it to a fixed-length stack-based buffer. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-6436.
CVE-2018-1792
IBM WebSphere MQ 8.0.0.0 through 8.0.0.10, 9.0.0.0 through 9.0.0.5, 9.0.1 through 9.0.5, and 9.1.0.0 could allow a local user to inject code that could be executed with root privileges. IBM X-Force ID: 148947.
CVE-2018-1808
IBM WebSphere Commerce 9.0.0.0 through 9.0.0.6 could allow some server-side code injection due to inadequate input control. IBM X-Force ID: 149828.
CVE-2018-18591
A potential unauthorized disclosure of data vulnerability has been identified in Micro Focus Service Manager versions: 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, 9.41, 9.50, 9.51. The vulnerability could be exploited to release unauthorized disclosure of data.
CVE-2018-19244
An XML External Entity (XXE) vulnerability exists in the Charles 4.2.7 import/export setup option. If a user imports a "Charles Settings.xml" file from an attacker, an intranet network may be accessed and information may be leaked.
CVE-2018-19246
PHP-Proxy 5.1.0 allows remote attackers to read local files if the default "pre-installed version" (intended for users who lack shell access to their web server) is used. This occurs because the aeb067ca0aa9a3193dce3a7264c90187 app_key value from the default config.php is in place, and this value can be easily used to calculate the authorization data needed for local file inclusion.
CVE-2018-2473
SAP BusinessObjects Business Intelligence Platform Server, versions 4.1 and 4.2, when using Web Intelligence Richclient 3 tiers mode gateway allows an attacker to prevent legitimate users from accessing a service, either by crashing or flooding the service.
CVE-2018-2476
Due to insufficient URL Validation in forums in SAP NetWeaver versions 7.30, 7.31, 7.40, an attacker can redirect users to a malicious site.
CVE-2018-2477
Knowledge Management (XMLForms) in SAP NetWeaver, versions 7.30, 7.31, 7.40 and 7.50 does not sufficiently validate an XML document accepted from an untrusted source.
CVE-2018-2478
An attacker can use specially crafted inputs to execute commands on the host of a TREX / BWA installation, SAP Basis, versions: 7.0 to 7.02, 7.10 to 7.11, 7.30, 7.31, 7.40 and 7.50 to 7.53. Not all commands are possible, only those that can be executed by the <sid>adm user. The commands executed depend upon the privileges of the <sid>adm user.
CVE-2018-2479
SAP BusinessObjects Business Intelligence Platform (BIWorkspace), versions 4.1 and 4.2, does not sufficiently encode user-controlled inputs, resulting in Cross-Site Scripting (XSS) vulnerability.
CVE-2018-2481
In some SAP standard roles, in SAP_ABA versions, 7.00 to 7.02, 7.10 to 7.11, 7.30, 7.31, 7.40, 7.50, 75C to 75D, a transaction code reserved for customer is used. By implementing such transaction code a malicious user may execute unauthorized transaction functionality.
CVE-2018-2482
SAP Mobile Secure Android Application, Mobile-secure.apk Android client, before version 6.60.19942.0, allows an attacker to prevent legitimate users from accessing a service, either by crashing or flooding the service. Install the Mobile Secure Android client released in Mid-Oct 2018.
CVE-2018-2483
HTTP Verb Tampering is possible in SAP BusinessObjects Business Intelligence Platform, versions 4.1 and 4.2, Central Management Console (CMC) by changing request method.
CVE-2018-2485
It is possible for a malicious application or malware to execute JavaScript in a SAP Fiori application. This can include reading and writing of information and calling device specific JavaScript APIs in the application. SAP Fiori Client version 1.11.5 in Google Play store addresses these issues and users must update to that version.
CVE-2018-2487
SAP Disclosure Management 10.x allows an attacker to exploit through a specially crafted zip file provided by users: When extracted in specific use cases, files within this zip file can land in different locations than the originally intended extraction point.
CVE-2018-2488
It is possible for a malware application installed on an Android device to send local push notifications with an empty message to SAP Fiori Client and cause the application to crash. SAP Fiori Client version 1.11.5 in Google Play store addresses these issues and users must update to that version.
CVE-2018-2489
Locally, without any permission, an arbitrary android application could delete the SSO configuration of SAP Fiori Client. SAP Fiori Client version 1.11.5 in Google Play store addresses these issues and users must update to that version.
CVE-2018-2490
The broadcast messages received by SAP Fiori Client are not protected by permissions. SAP Fiori Client version 1.11.5 in Google Play store addresses these issues and users must update to that version.
CVE-2018-2491
When opening a deep link URL in SAP Fiori Client with log level set to "Debug", the client application logs the URL to the log file. If this URL contains malicious JavaScript code it can eventually run inside the built-in log viewer of the application in case user opens the viewer and taps on the hyperlink in the viewer. SAP Fiori Client version 1.11.5 in Google Play store addresses these issues and users must update to that version.
CVE-2018-6260
NVIDIA graphics driver contains a vulnerability that may allow access to application data processed on the GPU through a side channel exposed by the GPU performance counters. Local user access is required. This is not a network or remote attack vector.
CVE-2018-6980
VMware vRealize Log Insight (4.7.x before 4.7.1 and 4.6.x before 4.6.2) contains a vulnerability due to improper authorization in the user registration method. Successful exploitation of this issue may allow Admin users with view only permission to perform certain administrative functions which they are not allowed to perform.
CVE-2018-7910
Some Huawei smartphones ALP-AL00B 8.0.0.118D(C00), ALP-TL00B 8.0.0.118D(C01), BLA-AL00B 8.0.0.118D(C00), BLA-L09C 8.0.0.127(C432), 8.0.0.128(C432), 8.0.0.137(C432), BLA-L29C 8.0.0.129(C432), 8.0.0.137(C432) have an authentication bypass vulnerability. When the attacker obtains the user's smartphone, the vulnerability can be used to replace the start-up program so that the attacker can obtain the information in the smartphone and achieve the purpose of controlling the smartphone.
CVE-2018-7925
The radio module of some Huawei smartphones Emily-AL00A The versions before 8.1.0.171(C00) have a lock-screen bypass vulnerability. An unauthenticated attacker could start third-part input method APP through certain operations to bypass lock-screen by exploit this vulnerability.
CVE-2018-7926
Huawei Watch 2 with versions and earlier than OWDD.180707.001.E1 have an improper authorization vulnerability. Due to improper permission configuration for specific operations, an attacker who obtained the Huawei ID bound to the watch can bypass permission verification to perform specific operations and modify some data on the watch.
CVE-2018-8009
Apache Hadoop 3.1.0, 3.0.0-alpha to 3.0.2, 2.9.0 to 2.9.1, 2.8.0 to 2.8.4, 2.0.0-alpha to 2.7.6, 0.23.0 to 0.23.11 is exploitable via the zip slip vulnerability in places that accept a zip file.
