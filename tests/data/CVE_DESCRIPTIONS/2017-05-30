CVE-2016-3083
Apache Hive (JDBC + HiveServer2) implements SSL for plain TCP and HTTP connections (it supports both transport modes). While validating the server's certificate during the connection setup, the client in Apache Hive before 1.2.2 and 2.0.x before 2.0.1 doesn't seem to be verifying the common name attribute of the certificate. In this way, if a JDBC client sends an SSL request to server abc.com, and the server responds with a valid certificate (certified by CA) but issued to xyz.com, the client will accept that as a valid certificate and the SSL handshake will go through.
CVE-2017-2300
On Juniper Networks SRX Series Services Gateways chassis clusters running Junos OS 12.1X46 prior to 12.1X46-D65, 12.3X48 prior to 12.3X48-D40, 12.3X48 prior to 12.3X48-D60, flowd daemon on the primary node of an SRX Series chassis cluster may crash and restart when attempting to synchronize a multicast session created via crafted multicast packets.
CVE-2017-2301
On Juniper Networks products or platforms running Junos OS 11.4 prior to 11.4R13-S3, 12.1X46 prior to 12.1X46-D60, 12.3 prior to 12.3R12-S2 or 12.3R13, 12.3X48 prior to 12.3X48-D40, 13.2X51 prior to 13.2X51-D40, 13.3 prior to 13.3R10, 14.1 prior to 14.1R8, 14.1X53 prior to 14.1X53-D12 or 14.1X53-D35, 14.1X55 prior to 14.1X55-D35, 14.2 prior to 14.2R7, 15.1 prior to 15.1F6 or 15.1R3, 15.1X49 prior to 15.1X49-D60, 15.1X53 prior to 15.1X53-D30 and DHCPv6 enabled, when a crafted DHCPv6 packet is received from a subscriber, jdhcpd daemon crashes and restarts. Repeated crashes of the jdhcpd process may constitute an extended denial of service condition for subscribers attempting to obtain IPv6 addresses.
CVE-2017-2302
On Juniper Networks products or platforms running Junos OS 12.1X46 prior to 12.1X46-D55, 12.1X47 prior to 12.1X47-D45, 12.3R13 prior to 12.3R13, 12.3X48 prior to 12.3X48-D35, 13.3 prior to 13.3R10, 14.1 prior to 14.1R8, 14.1X53 prior to 14.1X53-D40, 14.1X55 prior to 14.1X55-D35, 14.2 prior to 14.2R6, 15.1 prior to 15.1F2 or 15.1R1, 15.1X49 prior to 15.1X49-D20 where the BGP add-path feature is enabled with 'send' option or with both 'send' and 'receive' options, a network based attacker can cause the Junos OS rpd daemon to crash and restart. Repeated crashes of the rpd daemon can result in an extended denial of service condition.
CVE-2017-2303
On Juniper Networks products or platforms running Junos OS 12.1X46 prior to 12.1X46-D50, 12.1X47 prior to 12.1X47-D40, 12.3 prior to 12.3R13, 12.3X48 prior to 12.3X48-D30, 13.2X51 prior to 13.2X51-D40, 13.3 prior to 13.3R10, 14.1 prior to 14.1R8, 14.1X53 prior to 14.1X53-D35, 14.1X55 prior to 14.1X55-D35, 14.2 prior to 14.2R5, 15.1 prior to 15.1F6 or 15.1R3, 15.1X49 prior to 15.1X49-D30 or 15.1X49-D40, 15.1X53 prior to 15.1X53-D35, and where RIP is enabled, certain RIP advertisements received by the router may cause the RPD daemon to crash resulting in a denial of service condition.
CVE-2017-2304
Juniper Networks QFX3500, QFX3600, QFX5100, QFX5200, EX4300 and EX4600 devices running Junos OS 14.1X53 prior to 14.1X53-D40, 15.1X53 prior to 15.1X53-D40, 15.1 prior to 15.1R2, do not pad Ethernet packets with zeros, and thus some packets can contain fragments of system memory or data from previous packets. This issue is also known as 'Etherleak'
CVE-2017-2305
On Juniper Networks Junos Space versions prior to 16.1R1, due to an insufficient authorization check, readonly users on the Junos Space administrative web interface can create privileged users, allowing privilege escalation.
CVE-2017-2306
On Juniper Networks Junos Space versions prior to 16.1R1, due to an insufficient authorization check, readonly users on the Junos Space administrative web interface can execute code on the device.
CVE-2017-2307
A reflected cross site scripting vulnerability in the administrative interface of Juniper Networks Junos Space versions prior to 16.1R1 may allow remote attackers to steal sensitive information or perform certain administrative actions on Junos Space.
CVE-2017-2308
An XML External Entity Injection vulnerability in Juniper Networks Junos Space versions prior to 16.1R1 may allow an authenticated user to read arbitrary files on the device.
CVE-2017-2309
On Juniper Networks Junos Space versions prior to 16.1R1 when certificate based authentication is enabled for the Junos Space cluster, some restricted web services are accessible over the network. This represents an information leak risk.
CVE-2017-2310
A firewall bypass vulnerability in the host based firewall of Juniper Networks Junos Space versions prior to 16.1R1 may permit certain crafted packets, representing a network integrity risk.
CVE-2017-2311
On Juniper Networks Junos Space versions prior to 16.1R1, an unauthenticated remote attacker with network access to Junos space device can easily create a denial of service condition.
CVE-2017-7494
Samba since version 3.5.0 and before 4.6.4, 4.5.10 and 4.4.14 is vulnerable to remote code execution vulnerability, allowing a malicious client to upload a shared library to a writable share, and then cause the server to load and execute it.
CVE-2017-7502
Null pointer dereference vulnerability in NSS since 3.24.0 was found when server receives empty SSLv2 messages resulting into denial of service by remote attacker.
CVE-2017-7511
poppler since version 0.17.3 has been vulnerable to NULL pointer dereference in pdfunite triggered by specially crafted documents.
