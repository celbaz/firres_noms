CVE-2009-4269
The password hash generation algorithm in the BUILTIN authentication functionality for Apache Derby before 10.6.1.0 performs a transformation that reduces the size of the set of inputs to SHA-1, which produces a small search space that makes it easier for local and possibly remote attackers to crack passwords by generating hash collisions, related to password substitution.
Per https://issues.apache.org/jira/browse/DERBY-4483, the reported version affected is 10.5.3.0.  Unable to determine if affected versions exist between 10.5.3.0 and 10.6.1.0
CVE-2010-1519
Multiple integer overflows in glpng.c in glpng 1.45 allow context-dependent attackers to execute arbitrary code via a crafted PNG image, related to (1) the pngLoadRawF function and (2) the pngLoadF function, leading to heap-based buffer overflows.
CVE-2010-1797
Multiple stack-based buffer overflows in the cff_decoder_parse_charstrings function in the CFF Type2 CharStrings interpreter in cff/cffgload.c in FreeType before 2.4.2, as used in Apple iOS before 4.0.2 on the iPhone and iPod touch and before 3.2.2 on the iPad, allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted CFF opcodes in embedded fonts in a PDF document, as demonstrated by JailbreakMe. NOTE: some of these details are obtained from third party information.
CVE-2010-1799
Stack-based buffer overflow in the error-logging functionality in Apple QuickTime before 7.6.7 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file.
CVE-2010-1886
Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 SP2 and R2, and Windows 7 allow local users to gain privileges by leveraging access to a process with NetworkService credentials, as demonstrated by TAPI Server, SQL Server, and IIS processes, and related to the Windows Service Isolation feature.  NOTE: the vendor states that privilege escalation from NetworkService to LocalSystem does not cross a "security boundary."
CVE-2010-2576
Opera before 10.61 does not properly suppress clicks on download dialogs that became visible after a recent tab change, which allows remote attackers to conduct clickjacking attacks, and consequently execute arbitrary code, via vectors involving (1) closing a tab or (2) hiding a tab, a related issue to CVE-2005-2407.
CVE-2010-2577
Multiple SQL injection vulnerabilities in Pligg before 1.1.1 allow remote attackers to execute arbitrary SQL commands via the title parameter to (1) storyrss.php or (2) story.php.
CVE-2010-2756
Search.pm in Bugzilla 2.19.1 through 3.2.7, 3.3.1 through 3.4.7, 3.5.1 through 3.6.1, and 3.7 through 3.7.2 allows remote attackers to determine the group memberships of arbitrary users via vectors involving the Search interface, boolean charts, and group-based pronouns.
CVE-2010-2757
The sudo feature in Bugzilla 2.22rc1 through 3.2.7, 3.3.1 through 3.4.7, 3.5.1 through 3.6.1, and 3.7 through 3.7.2 does not properly send impersonation notifications, which makes it easier for remote authenticated users to impersonate other users without discovery.
CVE-2010-2758
Bugzilla 2.17.1 through 3.2.7, 3.3.1 through 3.4.7, 3.5.1 through 3.6.1, and 3.7 through 3.7.2 generates different error messages depending on whether a product exists, which makes it easier for remote attackers to guess product names via unspecified use of the (1) Reports or (2) Duplicates page.
CVE-2010-2759
Bugzilla 2.23.1 through 3.2.7, 3.3.1 through 3.4.7, 3.5.1 through 3.6.1, and 3.7 through 3.7.2, when PostgreSQL is used, does not properly handle large integers in (1) bug and (2) attachment phrases, which allows remote authenticated users to cause a denial of service (bug invisibility) via a crafted comment.
CVE-2010-2827
Cisco IOS 15.1(2)T allows remote attackers to cause a denial of service (resource consumption and TCP outage) via spoofed TCP packets, related to embryonic TCP connections that remain in the SYN_RCVD or SYN_SENT state, aka Bug ID CSCti18193.
CVE-2010-3013
SQL injection vulnerability in groupadmin.php in Pligg before 1.1.1 allows remote attackers to execute arbitrary SQL commands via the role parameter, a different vulnerability than CVE-2010-2577.
CVE-2010-3019
Heap-based buffer overflow in Opera before 10.61 allows remote attackers to execute arbitrary code or cause a denial of service (application crash or hang) via vectors related to HTML5 canvas painting operations that occur during the application of transformations.
CVE-2010-3020
The news-feed preview feature in Opera before 10.61 does not properly remove scripts, which allows remote attackers to force subscriptions to arbitrary feeds via crafted content.
CVE-2010-3021
Unspecified vulnerability in Opera before 10.61 allows remote attackers to cause a denial of service (CPU consumption and application hang) via an animated PNG image.
CVE-2010-3022
Cross-site scripting (XSS) vulnerability in the Performance logging module in the Devel module 5.x before 5.x-1.3 and 6.x before 6.x-1.21 for Drupal allows remote authenticated users, with add url aliases and report access permissions, to inject arbitrary web script or HTML via crafted node paths in a URL.
CVE-2010-3023
Multiple cross-site scripting (XSS) vulnerabilities in DiamondList 0.1.6, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via the (1) category[description] parameter to user/main/update_category, which is not properly handled by _app/views/categories/index.html.erb; and the (2) setting[site_title] parameter to user/main/update_settings, which is not properly handled by _app/views/settings/_list_settings.rhtml.
CVE-2010-3024
Multiple cross-site request forgery (CSRF) vulnerabilities in user/main/update_user in DiamondList 0.1.6, and possibly earlier, allow remote attackers to hijack the authentication of administrators for requests that (1) change the administrative password or (2) change the site's configuration.
CVE-2010-3025
Multiple cross-site scripting (XSS) vulnerabilities in Tomaz Muraus Open Blog 1.2.1, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via the (1) excerpt parameter to application/modules/admin/controllers/posts.php, as reachable by admin/posts/edit; and the (2) content parameter to application/modules/admin/controllers/pages.php, as reachable by admin/posts/edit.
CVE-2010-3026
Cross-site request forgery (CSRF) vulnerability in application/modules/admin/controllers/users.php in Tomaz Muraus Open Blog 1.2.1, and possibly earlier, allows remote attackers to hijack the authentication of administrators for requests to admin/users/edit that grant administrative privileges.
CVE-2010-3027
SQL injection vulnerability in index.php in Tycoon Baseball Script 1.0.9 allows remote attackers to execute arbitrary SQL commands via the game_id parameter in a game_player action.
CVE-2010-3028
The Aardvertiser component before 2.2.1 for Joomla! uses insecure permissions (777) in unspecified folders, which allows local users to modify, create, or delete certain files.
CVE-2010-3029
SQL injection vulnerability in statistics.php in PHPKick 0.8 allows remote attackers to execute arbitrary SQL commands via the gameday parameter in an overview action.
