CVE-2012-5394
Cross-site request forgery (CSRF) vulnerability in the CentralAuth extension for MediaWiki before 1.19.9, 1.20.x before 1.20.8, and 1.21.x before 1.21.3 allows remote attackers to hijack the authentication of users for requests that login via vectors involving image loading.
CVE-2012-6151
Net-SNMP 5.7.1 and earlier, when AgentX is registering to handle a MIB and processing GETNEXT requests, allows remote attackers to cause a denial of service (crash or infinite loop, CPU consumption, and hang) by causing the AgentX subagent to timeout.
CVE-2013-0348
thttpd.c in sthttpd before 2.26.4-r2 and thttpd 2.25b use world-readable permissions for /var/log/thttpd.log, which allows local users to obtain sensitive information by reading the file.
CVE-2013-4567
Incomplete blacklist vulnerability in Sanitizer::checkCss in MediaWiki before 1.19.9, 1.20.x before 1.20.8, and 1.21.x before 1.21.3 allows remote attackers to conduct cross-site scripting (XSS) attacks via a \b (backspace) character in CSS.
Per: http://cwe.mitre.org/data/definitions/184.html

"CWE-184: Incomplete Blacklist"
CVE-2013-4568
Incomplete blacklist vulnerability in Sanitizer::checkCss in MediaWiki before 1.19.9, 1.20.x before 1.20.8, and 1.21.x before 1.21.3 allows remote attackers to conduct cross-site scripting (XSS) attacks via certain non-ASCII characters in CSS, as demonstrated using variations of "expression" containing (1) full width characters or (2) IPA extensions, which are converted and rendered by Internet Explorer.
Per: http://cwe.mitre.org/data/definitions/184.html

"CWE-184: Incomplete Blacklist"
CVE-2013-4569
The CleanChanges extension for MediaWiki before 1.19.9, 1.20.x before 1.20.8, and 1.21.x before 1.21.3, when "Group changes by page in recent changes and watchlist" is enabled, allows remote attackers to obtain sensitive information (revision-deleted IPs) via the Recent Changes page.
CVE-2013-4988
Stack-based buffer overflow in IcoFX 2.5 and earlier allows remote attackers to execute arbitrary code via a long idCount value in an ICONDIR structure in an ICO file.  NOTE: some of these details are obtained from third party information.
CVE-2013-5676
The Jenkins Plugin for SonarQube 3.7 and earlier allows remote authenticated users to obtain sensitive information (cleartext passwords) by reading the value in the sonar.sonarPassword parameter from jenkins/configure.
CVE-2013-6005
Cross-site scripting (XSS) vulnerability in Cybozu Dezie before 8.1.0 allows remote attackers to inject arbitrary web script or HTML via vectors related to the Cancel button.
CVE-2013-6048
The get_group_tree function in lib/Munin/Master/HTMLConfig.pm in Munin before 2.0.18 allows remote nodes to cause a denial of service (infinite loop and memory consumption in the munin-html process) via crafted multigraph data.
CVE-2013-6359
Munin::Master::Node in Munin before 2.0.18 allows remote attackers to cause a denial of service (abort data collection for node) via a plugin that uses "multigraph" as a multigraph service name.
CVE-2013-6394
Percona XtraBackup before 2.1.6 uses a constant string for the initialization vector (IV), which makes it easier for local users to defeat cryptographic protection mechanisms and conduct plaintext attacks.
CVE-2013-6400
Xen 4.2.x and 4.3.x, when using Intel VT-d and a PCI device has been assigned, does not clear the flag that suppresses IOMMU TLB flushes when unspecified errors occur, which causes the TLB entries to not be flushed and allows local guest administrators to cause a denial of service (host crash) or gain privileges via unspecified vectors.
CVE-2013-6809
Format string vulnerability in the client in Tftpd32 before 4.50 allows remote servers to cause a denial of service (crash) or possibly execute arbitrary code via format string specifiers in the Remote File field.
CVE-2013-6839
SQL injection vulnerability in InstantSoft InstantCMS 1.10.3 and earlier allows remote attackers to execute arbitrary SQL commands via the orderby parameter to catalog/[id].
CVE-2013-6956
Cross-site scripting (XSS) vulnerability in the Secure Access Service Web rewriting feature in Juniper Junos Pulse Secure Access Service (aka SSL VPN) with IVE OS before 7.1r17, 7.3 before 7.3r8, 7.4 before 7.4r6, and 8.0 before 8.0r1, when web rewrite is enabled, allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-6957
Cross-site scripting (XSS) vulnerability in the web administrative component in Juniper IDP allows remote attackers to inject arbitrary web script or HTML via unspecified vectors to the ACM web server.
CVE-2013-6958
Juniper NetScreen Firewall running ScreenOS 5.4, 6.2, or 6.3, when the Ping of Death screen is disabled, allows remote attackers to cause a denial of service via a crafted packet.
CVE-2013-7038
The MHD_http_unescape function in libmicrohttpd before 0.9.32 might allow remote attackers to obtain sensitive information or cause a denial of service (crash) via unspecified vectors that trigger an out-of-bounds read.
CVE-2013-7039
Stack-based buffer overflow in the MHD_digest_auth_check function in libmicrohttpd before 0.9.32, when MHD_OPTION_CONNECTION_MEMORY_LIMIT is set to a large value, allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long URI in an authentication header.
CVE-2013-7050
The get_main_source_dir function in scripts/uscan.pl in devscripts before 2.13.8, when using USCAN_EXCLUSION, allows remote attackers to execute arbitrary commands via shell metacharacters in a directory name.
CVE-2013-7091
Directory traversal vulnerability in /res/I18nMsg,AjxMsg,ZMsg,ZmMsg,AjxKeys,ZmKeys,ZdMsg,Ajx%20TemplateMsg.js.zgz in Zimbra 7.2.2 and 8.0.2 allows remote attackers to read arbitrary files via a ..  (dot dot) in the skin parameter.  NOTE: this can be leveraged to execute arbitrary code by obtaining LDAP credentials and accessing the service/admin/soap API.
CVE-2013-7092
Multiple SQL injection vulnerabilities in /admin/cgi-bin/rpc/doReport/18 in McAfee Email Gateway 7.6 allow remote authenticated users to execute arbitrary SQL commands via the (1) events_col, (2) event_id, (3) reason, (4) events_order, (5) emailstatus_order, or (6) emailstatus_col JSON keys.
CVE-2013-7093
SAP Network Interface Router (SAProuter) 39.3 SP4 allows remote attackers to bypass authentication and modify the configuration via unspecified vectors.
CVE-2013-7094
SQL injection vulnerability in the RSDDCVER_COUNT_TAB_COLS function in SAP NetWeaver 7.30 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2013-7095
The XML parser (crm_flex_data) in SAP Customer Relationship Management (CRM) 7.02 EHP 2 has unknown impact and attack vectors related to an XML External Entity (XXE) issue.
CVE-2013-7096
Multiple SQL injection vulnerabilities in SAP EMR Unwired allow remote attackers to execute arbitrary SQL commands via unspecified vectors.
