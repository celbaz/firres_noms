CVE-2008-4283
CRLF injection vulnerability in the WebContainer component in IBM WebSphere Application Server (WAS) 5.1.1.19 and earlier 5.1.x versions allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors.
CVE-2008-4284
Open redirect vulnerability in the ibm_security_logout servlet in IBM WebSphere Application Server (WAS) 5.1.1.19 and earlier 5.x versions, 6.0.x before 6.0.2.33, and 6.1.x before 6.1.0.23 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via the logoutExitPage feature.
CVE-2008-6068
SQL injection vulnerability in the JoomlaDate (com_joomladate) component 1.2 for Joomla! allows remote attackers to execute arbitrary SQL commands via the user parameter in a viewProfile action to index.php.
CVE-2008-6069
SQL injection vulnerability in e107chat.php in the eChat plugin 4.2 for e107, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the nick parameter.
CVE-2008-6070
Multiple heap-based buffer underflows in the ReadPALMImage function in coders/palm.c in GraphicsMagick before 1.2.3 allow remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted PALM image, a different vulnerability than CVE-2007-0770.  NOTE: some of these details are obtained from third party information.
CVE-2008-6071
Heap-based buffer overflow in the DecodeImage function in coders/pict.c in GraphicsMagick before 1.1.14, and 1.2.x before 1.2.3, allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted PICT image.  NOTE: some of these details are obtained from third party information.
CVE-2008-6072
Multiple unspecified vulnerabilities in GraphicsMagick before 1.1.14, and 1.2.x before 1.2.3, allow remote attackers to cause a denial of service (crash) via unspecified vectors in (1) XCF and (2) CINEON images.
CVE-2008-6073
StorageCrypt 2.0.1 does not properly encrypt disks, which allows local users to obtain sensitive information via unspecified vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6099
PHP remote file inclusion vulnerability in index.php in RPortal 1.1 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the file_op parameter.
CVE-2008-6100
Multiple SQL injection vulnerabilities in Discussion Forums 2k 3.3, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) CatID parameter to (a) RSS1.php and (b) RSS2.php in misc/; and the (2) SubID parameter to (c) misc/RSS5.php.
CVE-2008-6101
SQL injection vulnerability in click.php in Adult Banner Exchange Website allows remote attackers to execute arbitrary SQL commands via the targetid parameter.
CVE-2008-6102
SQL injection vulnerability in ratelink.php in Link Trader Script allows remote attackers to execute arbitrary SQL commands via the lnkid parameter.
CVE-2008-6103
PHP remote file inclusion vulnerability in index.php in A4Desk Event Calendar, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary PHP code via a URL in the v parameter.
CVE-2008-6104
SQL injection vulnerability in A4Desk PHP Event Calendar allows remote attackers to execute arbitrary SQL commands via the eventid parameter to admin/index.php.
CVE-2008-6105
Cross-site scripting (XSS) vulnerability in IBM Workplace for Business Controls and Reporting 2.x and IBM Workplace Web Content Management 6.x allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2008-6106
Cross-site request forgery (CSRF) vulnerability in IBM Workplace for Business Controls and Reporting 2.x and IBM Workplace Web Content Management 6.x has unknown impact and remote attack vectors.  NOTE: some of these details are obtained from third party information.
CVE-2008-6107
The (1) sys32_mremap function in arch/sparc64/kernel/sys_sparc32.c, the (2) sparc_mmap_check function in arch/sparc/kernel/sys_sparc.c, and the (3) sparc64_mmap_check function in arch/sparc64/kernel/sys_sparc.c, in the Linux kernel before 2.6.25.4, omit some virtual-address range (aka span) checks when the mremap MREMAP_FIXED bit is not set, which allows local users to cause a denial of service (panic) via unspecified mremap calls, a related issue to CVE-2008-2137.
CVE-2008-6108
Cross-site scripting (XSS) vulnerability in result.php in Galatolo WebManager (GWM) 1.0 allows remote attackers to inject arbitrary web script or HTML via the key parameter.
CVE-2009-0075
Microsoft Internet Explorer 7 does not properly handle errors during attempted access to deleted objects, which allows remote attackers to execute arbitrary code via a crafted HTML document, related to CFunctionPointer and the appending of document objects, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2009-0076
Microsoft Internet Explorer 7, when XHTML strict mode is used, allows remote attackers to execute arbitrary code via the zoom style directive in conjunction with unspecified other directives in a malformed Cascading Style Sheets (CSS) stylesheet in a crafted HTML document, aka "CSS Memory Corruption Vulnerability."
CVE-2009-0095
Microsoft Office Visio 2002 SP2, 2003 SP3, and 2007 SP1 does not properly validate object data in Visio files, which allows remote attackers to execute arbitrary code via a crafted file, aka "Memory Validation Vulnerability."
CVE-2009-0096
Microsoft Office Visio 2002 SP2, 2003 SP3, and 2007 SP1 does not properly perform memory copy operations for object data, which allows remote attackers to execute arbitrary code via a crafted Visio document, aka "Memory Corruption Vulnerability."
CVE-2009-0097
Microsoft Office Visio 2002 SP2 and 2003 SP3 does not properly validate memory allocation for Visio files, which allows remote attackers to execute arbitrary code via a crafted file, aka "Memory Corruption Vulnerability."
CVE-2009-0098
Microsoft Exchange 2000 Server SP3, Exchange Server 2003 SP2, and Exchange Server 2007 SP1 do not properly interpret Transport Neutral Encapsulation (TNEF) properties, which allows remote attackers to execute arbitrary code via a crafted TNEF message, aka "Memory Corruption Vulnerability."
CVE-2009-0099
The Electronic Messaging System Microsoft Data Base (EMSMDB32) provider in Microsoft Exchange 2000 Server SP3 and Exchange Server 2003 SP2, as used in Exchange System Attendant, allows remote attackers to cause a denial of service (application outage) via a malformed MAPI command, aka "Literal Processing Vulnerability."
CVE-2009-0305
Multiple stack-based buffer overflows in the Research in Motion RIM AxLoader ActiveX control in AxLoader.ocx and AxLoader.dll in BlackBerry Application Web Loader 1.0 allow remote attackers to execute arbitrary code via unspecified use of the (1) load or (2) loadJad method.
CVE-2009-0417
Cross-site scripting (XSS) vulnerability in the AgaviWebRouting::gen(null) method in Agavi 0.11 before 0.11.6 and 1.0 before 1.0.0 beta 8 allows remote attackers to inject arbitrary web script or HTML via a crafted URL with certain characters that are not properly handled by web browsers that do not strictly follow RFC 3986, such as Internet Explorer 6 and 7.
CVE-2009-0432
The installation process for the File Transfer servlet in the System Management/Repository component in IBM WebSphere Application Server (WAS) 6.1.x before 6.1.0.19 does not enable the secure version, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2009-0433
Unspecified vulnerability in IBM WebSphere Application Server (WAS) 5.1.x before 5.1.1.19, 6.0.x before 6.0.2.29, and 6.1.x before 6.1.0.19, when Web Server plug-in content buffering is enabled, allows attackers to cause a denial of service (daemon crash) via unknown vectors, related to a mishandling of client read failures in which clients receive many 500 HTTP error responses and backend servers are incorrectly labeled as down.
CVE-2009-0434
PerfServlet in the PMI/Performance Tools component in IBM WebSphere Application Server (WAS) 6.0.x before 6.0.2.31, 6.1.x before 6.1.0.21, and 7.0.x before 7.0.0.1, when Performance Monitoring Infrastructure (PMI) is enabled, allows local users to obtain sensitive information by reading the (1) systemout.log and (2) ffdc files.  NOTE: this is probably a duplicate of CVE-2008-5413.
CVE-2009-0435
Unspecified vulnerability in the IBM Asynchronous I/O (aka AIO or libibmaio) library in the Java Message Service (JMS) component in IBM WebSphere Application Server (WAS) 6.1.x before 6.1.0.17 on AIX 5.3 allows attackers to cause a denial of service (daemon crash) via vectors related to the aio_getioev2 and getEvent methods.
CVE-2009-0436
The (1) mod_ibm_ssl and (2) mod_cgid modules in IBM HTTP Server 6.0.x before 6.0.2.31 and 6.1.x before 6.1.0.19, as used in WebSphere Application Server (WAS), set incorrect permissions for AF_UNIX sockets, which has unknown impact and local attack vectors.
CVE-2009-0437
The Installation Factory installation process for IBM WebSphere Application Server (WAS) 6.0.2 on Windows, when WAS is registered as a Windows service, allows local users to obtain sensitive information by reading the logs/instconfigifwas6.log log file.
CVE-2009-0438
IBM WebSphere Application Server (WAS) 7 before 7.0.0.1 on Windows allows remote attackers to bypass "Authorization checking" and obtain sensitive information from JSP pages via a crafted request.  NOTE: this is probably a duplicate of CVE-2008-5412.
CVE-2009-0441
PHP remote file inclusion vulnerability in skin_shop/standard/2_view_body/body_default.php in TECHNOTE 7.2, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the shop_this_skin_path parameter, a different vector than CVE-2008-4138.
CVE-2009-0442
Directory traversal vulnerability in bbcode.php in PHPbbBook 1.3 and 1.3h allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the l parameter.
CVE-2009-0443
Stack-based buffer overflow in Elecard AVC HD PLAYER 5.5.90116 allows remote attackers to execute arbitrary code via an M3U file containing a long string in a URL.
CVE-2009-0444
Multiple PHP remote file inclusion vulnerabilities in GRBoard 1.8, when register_globals is enabled and magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary PHP code via a URL in the (1) theme parameter to (a) 179_squarebox_pds_list/view.php, (b) 179_squarebox_minishop_expand/view.php, (c) 179_squarebox_gallery_list_pds/view.php, (d) 179_squarebox_gallery_list/view.php, (e) 179_squarebox_gallery/view.php, (f) 179_squarebox_board_swfupload/view.php, (g) 179_squarebox_board_expand/view.php, (h) 179_squarebox_board_basic_with_grcode/view.php, (i) 179_squarebox_board_basic/view.php, (j) 179_simplebar_pds_list/view.php, (k) 179_simplebar_notice/view.php, (l) 179_simplebar_gallery_list_pds/view.php, (m) 179_simplebar_gallery/view.php, and (n) 179_simplebar_basic/view.php in theme/; the (2) path parameter to (o) latest/sirini_gallery_latest/list.php; and the (3) grboard parameter to (p) include.php and (q) form_mail.php.
CVE-2009-0445
SQL injection vulnerability in index.php in Dreampics Gallery Builder allows remote attackers to execute arbitrary SQL commands via the exhibition_id parameter in a gallery.viewPhotos action.
CVE-2009-0446
SQL injection vulnerability in photo.php in WEBalbum 2.4b allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-0447
Multiple SQL injection vulnerabilities in default.asp in MyDesign Sayac 2.0 allow remote attackers to execute arbitrary SQL commands via (1) the user parameter (aka UserName field) or (2) the pass parameter (aka Pass field) to (a) admin/admin.asp or (b) the default URI under admin/.  NOTE: some of these details are obtained from third party information.
CVE-2009-0448
Directory traversal vulnerability in admin/modules/aa/preview.php in Syntax Desktop 2.7 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the synTarget parameter.
CVE-2009-0449
Buffer overflow in klim5.sys in Kaspersky Anti-Virus for Workstations 6.0 and Anti-Virus 2008 allows local users to gain privileges via an IOCTL 0x80052110 call.
CVE-2009-0450
Stack-based buffer overflow in BlazeVideo HDTV Player 3.5 and earlier allows remote attackers to execute arbitrary code via a long string in a playlist (aka .plf) file.
CVE-2009-0451
SQL injection vulnerability in Skalfa SkaLinks 1.5 allows remote attackers to execute arbitrary SQL commands via the Admin name field to the default URI under admin/.
CVE-2009-0452
Multiple SQL injection vulnerabilities in parents/login.php in Online Grades 3.2.4, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) uname or (2) pass parameter.
CVE-2009-0453
Online Grades 3.2.4 allows remote attackers to obtain configuration information via a direct request to phpinfo.php, which calls the phpinfo function.
CVE-2009-0454
Multiple SQL injection vulnerabilities in DMXReady Online Notebook Manager 1.1 allow remote attackers to execute arbitrary SQL commands via the (1) username or (2) password field.  NOTE: some third parties report inability to verify this issue.
CVE-2009-0456
PHP remote file inclusion vulnerability in examples/example_clientside_javascript.php in patForms, as used in Sourdough 0.3.5, allows remote attackers to execute arbitrary PHP code via a URL in the neededFiles[patForms] parameter.
CVE-2009-0457
Multiple directory traversal vulnerabilities in AJA Portal 1.2 allow remote attackers to include and execute arbitrary local files via directory traversal sequences in the currentlang parameter to admin/case.php in the (1) Contact_Plus and (2) Reviews modules, and (3) the module_name parameter to admin/includes/FANCYNLOptions.php in the Fancy_NewsLetter module.
CVE-2009-0458
Multiple SQL injection vulnerabilities in admin/login_submit.php in Whole Hog Ware Support 1.x allow remote attackers to execute arbitrary SQL commands via (1) the uid parameter (aka Username field) or (2) the pwd parameter (aka Password field).  NOTE: some of these details are obtained from third party information.
CVE-2009-0459
Multiple SQL injection vulnerabilities in admin/login_submit.php in Whole Hog Password Protect: Enhanced 1.x allow remote attackers to execute arbitrary SQL commands via (1) the uid parameter (aka Username field) or (2) the pwd parameter (aka Password field).  NOTE: some of these details are obtained from third party information.
CVE-2009-0460
Whole Hog Ware Support 1.x allows remote attackers to bypass authentication and obtain administrative access via an integer value in the adminid cookie.
CVE-2009-0461
Whole Hog Password Protect: Enhanced 1.x allows remote attackers to bypass authentication and obtain administrative access via an integer value in the adminid cookie.
CVE-2009-0462
Multiple SQL injection vulnerabilities in customer_login_check.asp in ClickTech ClickCart 6.0 allow remote attackers to execute arbitrary SQL commands via (1) the txtEmail parameter (aka E-MAIL field) or (2) the txtPassword parameter (aka password field) to customer_login.asp. NOTE: some of these details are obtained from third party information.
CVE-2009-0463
PHP remote file inclusion vulnerability in includes/header.php in Groone GLinks 2.1 allows remote attackers to execute arbitrary PHP code via a URL in the abspath parameter.
CVE-2009-0464
PHP remote file inclusion vulnerability in includes/header.php in Groone GBook 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the abspath parameter.
CVE-2009-0465
The SaveDoc method in the All_In_The_Box.AllBox ActiveX control in ALL_IN_THE_BOX.OCX in Synactis ALL In-The-Box ActiveX 3 allows remote attackers to create and overwrite arbitrary files via an argument ending in a '\0' character, which bypasses the intended .box filename extension, as demonstrated by a C:\boot.ini\0 argument.
CVE-2009-0466
Cross-site scripting (XSS) vulnerability in Vivvo CMS before 4.1.1 allows remote attackers to inject arbitrary web script or HTML via a URI that triggers a 404 Page Not Found response.
CVE-2009-0467
Cross-site scripting (XSS) vulnerability in proxy.html in Profense Web Application Firewall 2.6.2 and 2.6.3 allows remote attackers to inject arbitrary web script or HTML via the proxy parameter in a deny_log manage action.
CVE-2009-0468
Multiple cross-site request forgery (CSRF) vulnerabilities in ajax.html in Profense Web Application Firewall 2.6.2 and 2.6.3 allow remote attackers to hijack the authentication of administrators for requests that (1) shutdown the server, (2) send ping packets, (3) enable network services, (4) configure a proxy server, and (5) modify other settings via parameters in the query string.
CVE-2009-0469
Unspecified vulnerability in futomi's CGI Cafe Fulltext search CGI 1.1.2 allows remote attackers to gain administrative privileges via unknown vectors.
CVE-2009-0490
Stack-based buffer overflow in the String_parse::get_nonspace_quoted function in lib-src/allegro/strparse.cpp in Audacity 1.2.6 and other versions before 1.3.6 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a .gro file containing a long string.
CVE-2009-0491
Stack-based buffer overflow in Elecard MPEG Player 5.5 build 15884.081218 allows remote attackers to execute arbitrary code via a M3U file containing a long URL.
CVE-2009-0492
Unspecified vulnerability in SimpleIrcBot before 1.0 Stable has unknown impact and attack vectors related to an "auth vulnerability."
CVE-2009-0493
SQL injection vulnerability in login.php in IT!CMS 2.1a and earlier allows remote attackers to execute arbitrary SQL commands via the Username.
CVE-2009-0494
SQL injection vulnerability in the Portfol (com_portfol) 1.2 component for Joomla! allows remote attackers to execute arbitrary SQL commands via the vcatid parameter in a viewcategory action to index.php.
CVE-2009-0495
PHP remote file inclusion vulnerability in include/define.php in REALTOR 747 4.11 allows remote attackers to execute arbitrary PHP code via a URL in the INC_DIR parameter.
CVE-2009-0496
Multiple cross-site scripting (XSS) vulnerabilities in Ignite Realtime Openfire 3.6.2 allow remote attackers to inject arbitrary web script or HTML via the (1) log parameter to (a) logviewer.jsp and (b) log.jsp; (2) search parameter to (c) group-summary.jsp; (3) username parameter to (d) user-properties.jsp; (4) logDir, (5) maxTotalSize, (6) maxFileSize, (7) maxDays, and (8) logTimeout parameters to (e) audit-policy.jsp; (9) propName parameter to (f) server-properties.jsp; and the (10) roomconfig_roomname and (11) roomconfig_roomdesc parameters to (g) muc-room-edit-form.jsp.  NOTE: this can be leveraged for arbitrary code execution by using XSS to upload a malicious plugin.
CVE-2009-0497
Directory traversal vulnerability in log.jsp in Ignite Realtime Openfire 3.6.2 allows remote attackers to read arbitrary files via a ..\ (dot dot backslash) in the log parameter.
CVE-2009-0498
Virtual GuestBook (vgbook) 2.1 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file via a direct request to guestbook.mdb.
CVE-2009-0499
Cross-site request forgery (CSRF) vulnerability in the forum code in Moodle 1.7 before 1.7.7, 1.8 before 1.8.8, and 1.9 before 1.9.4 allows remote attackers to delete unauthorized forum posts via a link or IMG tag to post.php.
CVE-2009-0500
Cross-site scripting (XSS) vulnerability in course/lib.php in Moodle 1.6 before 1.6.9, 1.7 before 1.7.7, 1.8 before 1.8.8, and 1.9 before 1.9.4 allows remote attackers to inject arbitrary web script or HTML via crafted log table information that is not properly handled when it is displayed in a log report.
CVE-2009-0501
Unspecified vulnerability in the Calendar export feature in Moodle 1.8 before 1.8.8 and 1.9 before 1.9.4 allows attackers to obtain sensitive information and conduct "brute force attacks on user accounts" via unknown vectors.
CVE-2009-0502
Cross-site scripting (XSS) vulnerability in blocks/html/block_html.php in Snoopy 1.2.3, as used in Moodle 1.6 before 1.6.9, 1.7 before 1.7.7, 1.8 before 1.8.8, and 1.9 before 1.9.4, allows remote attackers to inject arbitrary web script or HTML via an HTML block, which is not properly handled when the "Login as" feature is used to visit a MyMoodle or Blog page.
