CVE-2015-7462
IBM WebSphere MQ 8.0.0.4 on IBM i platforms allows local users to discover cleartext certificate-keystore passwords within MQ trace output by leveraging administrator privileges to execute the mqcertck program.
CVE-2015-7775
Cross-site scripting (XSS) vulnerability in Cybozu Garoon 4.0.3 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2016-1197.
CVE-2015-7776
Cybozu Garoon 3.x and 4.x before 4.2.0 does not properly restrict loading of IMG elements, which makes it easier for remote attackers to track users via a crafted HTML e-mail message, a different vulnerability than CVE-2016-1196.
CVE-2016-0392
IBM General Parallel File System (GPFS) in GPFS Storage Server 2.0.0 through 2.0.7 and Elastic Storage Server 2.5.x through 2.5.5, 3.x before 3.5.5, and 4.x before 4.0.3, as distributed in Spectrum Scale RAID, allows local users to gain privileges via a crafted parameter to a setuid program.
CVE-2016-0911
EMC Data Domain OS 5.4 through 5.7 before 5.7.2.0 has a default no_root_squash option for NFS exports, which makes it easier for remote attackers to obtain filesystem access by leveraging client root privileges.
CVE-2016-0912
EMC Data Domain OS 5.4 through 5.7 before 5.7.2.0 allows remote authenticated users to bypass intended password-change restrictions by leveraging access to (1) a different account with the same role as a target account or (2) an account's session at an unattended workstation.
CVE-2016-1183
NTT Data TERASOLUNA Server Framework for Java(WEB) 2.0.0.1 through 2.0.6.1, as used in Fujitsu Interstage Business Application Server and other products, allows remote attackers to bypass a file-extension protection mechanism, and consequently read arbitrary files, via a crafted pathname.
CVE-2016-1191
Directory traversal vulnerability in the Files function in Cybozu Garoon 3.x and 4.x before 4.2.1 allows remote attackers to modify settings via unspecified vectors.
CVE-2016-1192
Directory traversal vulnerability in the logging implementation in Cybozu Garoon 3.7 through 4.2 allows remote authenticated users to read a log file via unspecified vectors.
CVE-2016-1195
Open redirect vulnerability in Cybozu Garoon 3.x and 4.x before 4.2.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a crafted URL.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2016-1196
Cybozu Garoon 3.x and 4.x before 4.2.1 allows remote authenticated users to bypass intended access restrictions and obtain sensitive Address Book information via an API call, a different vulnerability than CVE-2015-7776.
CVE-2016-1197
Cross-site scripting (XSS) vulnerability in Cybozu Garoon 4.x before 4.2.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2015-7775.
CVE-2016-1223
Directory traversal vulnerability in Trend Micro Office Scan 11.0, Worry-Free Business Security Service 5.x, and Worry-Free Business Security 9.0 allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2016-1224
CRLF injection vulnerability in Trend Micro Worry-Free Business Security Service 5.x and Worry-Free Business Security 9.0 allows remote attackers to inject arbitrary HTTP headers and conduct cross-site scripting (XSS) attacks via unspecified vectors.
CVE-2016-1225
Trend Micro Internet Security 8 and 10 allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2016-1226
Cross-site scripting (XSS) vulnerability in Trend Micro Internet Security 8 and 10 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1395
The web-based management interface on Cisco RV110W devices with firmware before 1.2.1.7, RV130W devices with firmware before 1.0.3.16, and RV215W devices with firmware before 1.3.0.8 allows remote attackers to execute arbitrary code as root via a crafted HTTP request, aka Bug ID CSCux82428.
CVE-2016-1396
Cross-site scripting (XSS) vulnerability in the web-based management interface on Cisco RV110W devices with firmware before 1.2.1.7, RV130W devices with firmware before 1.0.3.16, and RV215W devices with firmware before 1.3.0.8 allows remote attackers to inject arbitrary web script or HTML via a crafted parameter, aka Bug ID CSCux82583.
CVE-2016-1397
Buffer overflow in the web-based management interface on Cisco RV110W devices with firmware before 1.2.1.7, RV130W devices with firmware before 1.0.3.16, and RV215W devices with firmware before 1.3.0.8 allows remote authenticated users to cause a denial of service (device reload) via crafted configuration commands in an HTTP request, aka Bug ID CSCux82523.
CVE-2016-1424
Cisco IOS 15.2(1)T1.11 and 15.2(2)TST allows remote attackers to cause a denial of service (device crash) via a crafted LLDP packet, aka Bug ID CSCun63132.
CVE-2016-1860
Intel Graphics Driver in Apple OS X before 10.11.5 allows attackers to obtain sensitive kernel memory-layout information via a crafted app, a different vulnerability than CVE-2016-1862.
CVE-2016-1861
The NVIDIA Graphics Drivers subsystem in Apple OS X before 10.11.5 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1846.
CVE-2016-1862
Intel Graphics Driver in Apple OS X before 10.11.5 allows attackers to obtain sensitive kernel memory-layout information via a crafted app, a different vulnerability than CVE-2016-1860.
CVE-2016-1864
The XSS auditor in WebKit, as used in Apple iOS before 9.3 and Safari before 9.1, does not properly handle redirects in block mode, which allows remote attackers to obtain sensitive information via a crafted URL.
CVE-2016-4371
HPE Service Manager Software 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, and 9.41 allows remote authenticated users to obtain sensitive information, modify data, and conduct server-side request forgery (SSRF) attacks via unspecified vectors, related to the Server, Web Client, Windows Client, and Service Request components.
CVE-2016-4514
Moxa PT-7728 devices with software 3.4 build 15081113 allow remote authenticated users to change the configuration via vectors involving a local proxy.
CVE-2016-4518
OSIsoft PI AF Server before 2016 2.8.0 allows remote authenticated users to cause a denial of service (service outage) via a message.
CVE-2016-4530
OSIsoft PI SQL Data Access Server (aka OLE DB) 2016 1.5 allows remote authenticated users to cause a denial of service (service outage and data loss) via a message.
CVE-2016-4811
The NTT Broadband Platform Japan Connected-free Wi-Fi application 1.15.1 and earlier for Android and 1.13.0 and earlier for iOS allows man-in-the-middle attackers to obtain API access via unspecified vectors.
CVE-2016-4813
NetCommons 2.4.2.1 and earlier allows remote authenticated secretariat (aka CLERK) users to gain privileges by creating a SYSTEM_ADMIN account.
CVE-2016-4814
Directory traversal vulnerability in kml2jsonp.php in Geospatial Information Authority of Japan (aka GSI) Old_GSI_Maps before January 2015 on Windows allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2016-4815
Directory traversal vulnerability on BUFFALO WZR-600DHP3 devices with firmware 2.16 and earlier and WZR-S600DHP devices with firmware 2.16 and earlier allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2016-4816
BUFFALO WZR-600DHP3 devices with firmware 2.16 and earlier and WZR-S600DHP devices allow remote attackers to discover credentials and other sensitive information via unspecified vectors.
CVE-2016-4817
lib/http2/connection.c in H2O before 1.7.3 and 2.x before 2.0.0-beta5 mishandles HTTP/2 disconnection, which allows remote attackers to cause a denial of service (use-after-free and application crash) or possibly execute arbitrary code via a crafted packet.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-4819
The printfDx function in Takumi Yamada DX Library for Borland C++ 3.13f through 3.16b, DX Library for Gnu C++ 3.13f through 3.16b, and DX Library for Visual C++ 3.13f through 3.16b allows remote attackers to execute arbitrary code via a crafted string.
<a href="http://cwe.mitre.org/data/definitions/134.html">CWE-134: Use of Externally-Controlled Format String</a>
CVE-2016-4820
Cross-site request forgery (CSRF) vulnerability on I-O DATA DEVICE ETX-R devices allows remote attackers to hijack the authentication of arbitrary users.
CVE-2016-4821
I-O DATA DEVICE ETX-R devices allow remote attackers to cause a denial of service (web-server crash) via unspecified vectors.
