CVE-2015-5229
The calloc function in the glibc package in Red Hat Enterprise Linux (RHEL) 6.7 and 7.2 does not properly initialize memory areas, which might allow context-dependent attackers to cause a denial of service (hang or crash) via unspecified vectors.
CVE-2015-5969
The mysql-systemd-helper script in the mysql-community-server package before 5.6.28-2.17.1 in openSUSE 13.2 and before 5.6.28-13.1 in openSUSE Leap 42.1 and the mariadb package before 10.0.22-2.21.2 in openSUSE 13.2 and before 10.0.22-3.1 in SUSE Linux Enterprise (SLE) 12.1 and openSUSE Leap 42.1 allows local users to discover database credentials by listing a process and its arguments.
CVE-2015-6541
Multiple cross-site request forgery (CSRF) vulnerabilities in the Mail interface in Zimbra Collaboration Server (ZCS) before 8.5 allow remote attackers to hijack the authentication of arbitrary users for requests that change account preferences via a SOAP request to service/soap/BatchRequest.
CVE-2015-8840
The XML Data Archiving Service (XML DAS) in SAP NetWeaver AS Java does not check authorization, which allows remote authenticated users to obtain sensitive information, gain privileges, or possibly have unspecified other impact via requests to (1) webcontent/cas/cas_enter.jsp, (2) webcontent/cas/cas_validate.jsp, or (3) webcontent/aas/aas_store.jsp, aka SAP Security Note 1945215.
CVE-2016-1180
Cross-site scripting (XSS) vulnerability in the Cyber-Will Social-button Premium plugin before 1.1 for EC-CUBE 2.13.x allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1375
Cross-site scripting (XSS) vulnerability in Cisco IP Interoperability and Collaboration System 4.10(1) allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuy12339.
CVE-2016-2315
revision.c in git before 2.7.4 uses an incorrect integer data type, which allows remote attackers to execute arbitrary code via a (1) long filename or (2) many nested trees, leading to a heap-based buffer overflow.
CVE-2016-2324
Integer overflow in Git before 2.7.4 allows remote attackers to execute arbitrary code via a (1) long filename or (2) many nested trees, which triggers a heap-based buffer overflow.
CVE-2016-2381
Perl might allow context-dependent attackers to bypass the taint protection mechanism in a child process via duplicate environment variables in envp.
CVE-2016-2512
The utils.http.is_safe_url function in Django before 1.8.10 and 1.9.x before 1.9.3 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks or possibly conduct cross-site scripting (XSS) attacks via a URL containing basic authentication, as demonstrated by http://mysite.example.com\@attacker.com.
CVE-2016-2513
The password hasher in contrib/auth/hashers.py in Django before 1.8.10 and 1.9.x before 1.9.3 allows remote attackers to enumerate users via a timing attack involving login requests.
CVE-2016-3153
SPIP 2.x before 2.1.19, 3.0.x before 3.0.22, and 3.1.x before 3.1.1 allows remote attackers to execute arbitrary PHP code by adding content, related to the filtrer_entites function.
CVE-2016-3154
The encoder_contexte_ajax function in ecrire/inc/filtres.php in SPIP 2.x before 2.1.19, 3.0.x before 3.0.22, and 3.1.x before 3.1.1 allows remote attackers to conduct PHP object injection attacks and execute arbitrary PHP code via a crafted serialized object.
CVE-2016-3187
The Prepopulate module 7.x-2.x before 7.x-2.1 for Drupal allows remote attackers to modify the REQUEST superglobal array, and consequently have unspecified impact, via a base64-encoded pp parameter.
CVE-2016-3188
The _prepopulate_request_walk function in the Prepopulate module 7.x-2.x before 7.x-2.1 for Drupal allows remote attackers to modify the (1) actions, (2) container, (3) token, (4) password, (5) password_confirm, (6) text_format, or (7) markup field type, and consequently have unspecified impact, via unspecified vectors.
CVE-2016-3963
Siemens SCALANCE S613 allows remote attackers to cause a denial of service (web-server outage) via traffic to TCP port 443.
CVE-2016-3978
The Web User Interface (WebUI) in FortiOS 5.0.x before 5.0.13, 5.2.x before 5.2.3, and 5.4.x before 5.4.0 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks or cross-site scripting (XSS) attacks via the "redirect" parameter to "login."
CVE-2016-3979
Internet Communication Manager (aka ICMAN or ICM) in SAP JAVA AS 7.2 through 7.4 allows remote attackers to cause a denial of service (heap memory corruption and process crash) via a crafted HTTP request, related to the IctParseCookies function, aka SAP Security Note 2256185.
CVE-2016-3980
The Java Startup Framework (aka jstart) in SAP JAVA AS 7.2 through 7.4 allows remote attackers to cause a denial of service (process crash) via a crafted HTTP request, aka SAP Security Note 2259547.
CVE-2016-3983
McAfee Advanced Threat Defense (ATD) before 3.4.8.178 might allow remote attackers to bypass malware detection by leveraging information about the parent process.
CVE-2016-3984
The McAfee VirusScan Console (mcconsol.exe) in McAfee Active Response (MAR) before 1.1.0.161, Agent (MA) 5.x before 5.0.2 Hotfix 1110392 (5.0.2.333), Data Exchange Layer 2.x (DXL) before 2.0.1.140.1, Data Loss Prevention Endpoint (DLPe) 9.3 before Patch 6 and 9.4 before Patch 1 HF3, Device Control (MDC) 9.3 before Patch 6 and 9.4 before Patch 1 HF3, Endpoint Security (ENS) 10.x before 10.1, Host Intrusion Prevention Service (IPS) 8.0 before 8.0.0.3624, and VirusScan Enterprise (VSE) 8.8 before P7 (8.8.0.1528) on Windows allows local administrators to bypass intended self-protection rules and disable the antivirus engine by modifying registry keys.
