CVE-2008-2992
Stack-based buffer overflow in Adobe Acrobat and Reader 8.1.2 and earlier allows remote attackers to execute arbitrary code via a PDF file that calls the util.printf JavaScript function with a crafted format string argument, a related issue to CVE-2008-1104.
CVE-2008-4306
Buffer overflow in enscript before 1.6.4 has unknown impact and attack vectors, possibly related to the font escape sequence.
CVE-2008-4413
Unspecified vulnerability in HP System Management Homepage (SMH) 2.2.6 and earlier on HP-UX B.11.11 and B.11.23, and SMH 2.2.6 and 2.2.8 and earlier on HP-UX B.11.23 and B.11.31, allows local users to gain "unauthorized access" via unknown vectors, possibly related to temporary file permissions.
CVE-2008-4879
SQL injection vulnerability in prod.php in Maran PHP Shop allows remote attackers to execute arbitrary SQL commands via the cat parameter, a different vector than CVE-2008-4880.
CVE-2008-4880
SQL injection vulnerability in prodshow.php in Maran PHP Shop allows remote attackers to execute arbitrary SQL commands via the id parameter, a different vector than CVE-2008-4879.
CVE-2008-4881
SQL injection vulnerability in tr.php in YourFreeWorld Reminder Service Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4882
SQL injection vulnerability in tr.php in YourFreeWorld Autoresponder Hosting Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4883
SQL injection vulnerability in tr.php in YourFreeWorld Blog Blaster Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4884
SQL injection vulnerability in tr.php in YourFreeWorld Classifieds Hosting Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4885
SQL injection vulnerability in tr1.php in YourFreeWorld Scrolling Text Ads Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4886
SQL injection vulnerability in index.php in YourFreeWorld Shopping Cart Script allows remote attackers to execute arbitrary SQL commands via the c parameter.
CVE-2008-4887
SQL injection vulnerability in index.php in NetRisk 2.0 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter in a (1) profile page (profile.php) or (2) game page (game.php).  NOTE: some of these details are obtained from third party information.
CVE-2008-4888
Cross-site scripting (XSS) vulnerability in error.php in NetRisk 2.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the error parameter to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-4889
SQL injection vulnerability in index.php in deV!L'z Clanportal (DZCP) 1.4.9.6 and earlier allows remote attackers to execute arbitrary SQL commands via the users parameter in an addbuddy operation in a buddys action.
CVE-2008-4890
SQL injection vulnerability in products.php in 1st News 4 Professional (PR 1) allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4891
Cross-site scripting (XSS) vulnerability in signme.inc.php in Planetluc SignMe 1.5 before 1.55 allows remote attackers to inject arbitrary web script or HTML via the hash parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-4892
Cross-site scripting (XSS) vulnerability in gallery.inc.php in Planetluc MyGallery 1.7.2 and earlier, and possibly other versions before 1.8.1, allows remote attackers to inject arbitrary web script or HTML via the mghash parameter.  NOTE: some of these details are obtained from third party information.
Patch Information - http://planetluc.com/en/scripts_mygallery.php
CVE-2008-4893
Cross-site scripting (XSS) vulnerability in templates/mytribiqsite/tribal-GPL-1066/includes/header.inc.php in Tribiq CMS 5.0.10a, when register_globals is enabled, allows remote attackers to inject arbitrary web script or HTML via the template_path parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4894
Directory traversal vulnerability in templates/mytribiqsite/tribal-GPL-1066/includes/header.inc.php in Tribiq CMS 5.0.10a, when register_globals is enabled and magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the template_path parameter.  NOTE: it was later reported that this issue also affects 5.0.12c.
CVE-2008-4895
SQL injection vulnerability in tr.php in YourFreeWorld Downline Builder allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4896
Cross-site scripting (XSS) vulnerability in fichiers/add_url.php in Logz CMS 1.3.1 allows remote attackers to inject arbitrary web script or HTML via the art parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4897
SQL injection vulnerability in fichiers/add_url.php in Logz podcast CMS 1.3.1, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the art parameter.
CVE-2008-4898
Cross-site scripting (XSS) vulnerability in planetluc RateMe 1.3.3 allows remote attackers to inject arbitrary web script or HTML via the rate parameter in a submit rate action.
CVE-2008-4899
Cross-site request forgery (CSRF) vulnerability in Planetluc RateMe 1.3.3 allows remote attackers to perform unauthorized actions as other users via unspecified vectors.
CVE-2008-4900
SQL injection vulnerability in tr.php in YourFreeWorld Classifieds Blaster Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-4901
SQL injection vulnerability in admin/admin.php in Article Publisher Pro 1.5 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2008-4902
SQL injection vulnerability in contact_author.php in Article Publisher Pro 1.5 allows remote attackers to execute arbitrary SQL commands via the userid parameter.
CVE-2008-4903
Cross-site scripting (XSS) vulnerability in the leave comment (feedback) feature in Typo 5.1.3 and earlier allows remote attackers to inject arbitrary web script or HTML via the (1) comment[author] (Name) and (2) comment[url] (Website) parameters.
CVE-2008-4904
SQL injection vulnerability in the "Manage pages" feature (admin/pages) in Typo 5.1.3 and earlier allows remote authenticated users with "blog publisher" rights to execute arbitrary SQL commands via the search[published_at] parameter.
CVE-2008-4905
Typo 5.1.3 and earlier uses a hard-coded salt for calculating password hashes, which makes it easier for attackers to guess passwords via a brute force attack.
CVE-2008-4906
SQL injection vulnerability in lyrics_song.php in the Lyrics (lyrics_menu) plugin 0.42 for e107 allows remote attackers to execute arbitrary SQL commands via the l_id parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-4907
The message parsing feature in Dovecot 1.1.4 and 1.1.5, when using the FETCH ENVELOPE command in the IMAP client, allows remote attackers to cause a denial of service (persistent crash) via an email with a malformed From address, which triggers an assertion error, aka "invalid message address parsing bug."
CVE-2008-4908
maps/Info/combine.pl in CrossFire crossfire-maps 1.11.0 allows local users to overwrite arbitrary files via a symlink attack on a temporary file.
CVE-2008-4909
Cross-site request forgery (CSRF) vulnerability in CompactCMS 1.1 and earlier allows remote attackers to perform unauthorized actions as legitimate users via unspecified vectors.
CVE-2008-4910
The BasicService in Sun Java Web Start allows remote attackers to execute arbitrary programs on a client machine via a file:// URL argument to the showDocument method.
CVE-2008-4911
PHP remote file inclusion vulnerability in read.php in Chattaitaliano Istant-Replay allows remote attackers to execute arbitrary PHP code via a URL in the data parameter.
CVE-2008-4912
SQL injection vulnerability in popup_img.php in the fotogalerie module in RS MAXSOFT allows remote attackers to execute arbitrary SQL commands via the fotoID parameter.  NOTE: this issue was disclosed by an unreliable researcher, so it might be incorrect.
CVE-2008-4913
Directory traversal vulnerability in admin.php in LokiCMS 0.3.3 and earlier allows remote attackers to delete arbitrary files via a .. (dot dot) in the delete parameter.
CVE-2008-4918
Cross-site scripting (XSS) vulnerability in SonicWALL SonicOS Enhanced before 4.0.1.1, as used in SonicWALL Pro 2040 and TZ 180 and 190, allows remote attackers to inject arbitrary web script or HTML into arbitrary web sites via a URL to a site that is blocked based on content filtering, which is not properly handled in the CFS block page, aka "universal website hijacking."
CVE-2008-4919
Insecure method vulnerability in VISAGESOFT eXPert PDF Viewer X ActiveX control (VSPDFViewerX.ocx) 3.0.990.0 allows remote attackers to overwrite arbitrary files via a full pathname to the savePageAsBitmap method.
CVE-2008-4920
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: None.  Reason: this candidate was based on an incorrect claim regarding a directory issue in Agavi.  The vendor has disputed the issue and the original researcher has retracted the original claim, so this is not a vulnerability.  Further investigation by the vendor and original researcher show that the original issue was in a site-specific modification, which is outside the scope of CVE.  Notes: CVE users should not use this identifier.
CVE-2008-4921
board/admin/reguser.php in Chipmunk CMS 1.3 allows remote attackers to bypass authentication and gain administrator privileges via a direct request.  NOTE: some of these details are obtained from third party information.
CVE-2008-4922
Buffer overflow in the DjVu ActiveX Control 3.0 for Microsoft Office (DjVu_ActiveX_MSOffice.dll) allows remote attackers to execute arbitrary code via a long (1) ImageURL property, and possibly the (2) Mode, (3) Page, or (4) Zoom properties.
CVE-2008-4923
Multiple insecure method vulnerabilities in MW6 Technologies Aztec ActiveX control (AZTECLib.MW6Aztec, Aztec.dll) 3.0.0.1 allow remote attackers to overwrite arbitrary files via a full pathname argument to the (1) SaveAsBMP and (2) SaveAsWMF methods.
CVE-2008-4924
Multiple insecure method vulnerabilities in MW6 Technologies 1D Barcode ActiveX control (BARCODELib.MW6Barcode, Barcode.dll) 3.0.0.1 allow remote attackers to overwrite arbitrary files via a full pathname argument to the (1) SaveAsBMP and (2) SaveAsWMF methods.
CVE-2008-4925
Multiple insecure method vulnerabilities in MW6 Technologies DataMatrix ActiveX control (DATAMATRIXLib.MW6DataMatrix, DataMatrix.dll) 3.0.0.1 allow remote attackers to overwrite arbitrary files via a full pathname argument to the (1) SaveAsBMP and (2) SaveAsWMF methods.
CVE-2008-4926
Multiple insecure method vulnerabilities in MW6 Technologies PDF417 ActiveX control (MW6PDF417Lib.PDF417, MW6PDF417.dll) 3.0.0.1 allow remote attackers to overwrite arbitrary files via a full pathname argument to the (1) SaveAsBMP and (2) SaveAsWMF methods.
CVE-2008-4927
Microsoft Windows Media Player (WMP) 9.0 through 11 allows user-assisted attackers to cause a denial of service (application crash) via a malformed (1) MIDI or (2) DAT file, related to "MThd Header Parsing." NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4928
Cross-site scripting (XSS) vulnerability in the redirect function in functions.php in MyBB (aka MyBulletinBoard) 1.4.2 allows remote attackers to inject arbitrary web script or HTML via the url parameter in a removesubscriptions action to moderation.php, related to use of the ajax option to request a JavaScript redirect.  NOTE: this can be leveraged to execute PHP code and bypass cross-site request forgery (CSRF) protection.
CVE-2008-4929
MyBB (aka MyBulletinBoard) 1.4.2 uses insufficient randomness to compose filenames of uploaded files used as attachments, which makes it easier for remote attackers to read these files by guessing filenames.
CVE-2008-4930
MyBB (aka MyBulletinBoard) 1.4.2 does not properly handle an uploaded file with a nonstandard file type that contains HTML sequences, which allows remote attackers to cause that file to be processed as HTML by Internet Explorer's content inspection, aka "Incomplete protection against MIME-sniffing." NOTE: this could be leveraged for XSS and other attacks.
CVE-2008-6432
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-6432.  Reason: This candidate is a duplicate of CVE-2007-6432.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2007-6432 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
