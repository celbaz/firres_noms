CVE-2010-5293
wp-includes/comment.php in WordPress before 3.0.2 does not properly whitelist trackbacks and pingbacks in the blogroll, which allows remote attackers to bypass intended spam restrictions via a crafted URL, as demonstrated by a URL that triggers a substring match.
CVE-2010-5294
Multiple cross-site scripting (XSS) vulnerabilities in the request_filesystem_credentials function in wp-admin/includes/file.php in WordPress before 3.0.2 allow remote servers to inject arbitrary web script or HTML by providing a crafted error message for a (1) FTP or (2) SSH connection attempt.
CVE-2010-5295
Cross-site scripting (XSS) vulnerability in wp-admin/plugins.php in WordPress before 3.0.2 might allow remote attackers to inject arbitrary web script or HTML via a plugin's author field, which is not properly handled during a Delete Plugin action.
CVE-2010-5296
wp-includes/capabilities.php in WordPress before 3.0.2, when a Multisite configuration is used, does not require the Super Admin role for the delete_users capability, which allows remote authenticated administrators to bypass intended access restrictions via a delete action.
CVE-2010-5297
WordPress before 3.0.1, when a Multisite installation is used, permanently retains the "site administrators can add users" option once changed, which might allow remote authenticated administrators to bypass intended access restrictions in opportunistic circumstances via an add action after a temporary change.
CVE-2011-5270
wp-admin/press-this.php in WordPress before 3.0.6 does not enforce the publish_posts capability requirement, which allows remote authenticated users to perform publish actions by leveraging the Contributor role.
CVE-2012-2997
XML External Entity (XXE) vulnerability in sam/admin/vpe2/public/php/server.php in F5 BIG-IP 10.0.0 through 10.2.4 and 11.0.0 through 11.2.1 allows remote authenticated users to read arbitrary files via a crafted XML file.
CVE-2012-6633
Cross-site scripting (XSS) vulnerability in wp-includes/default-filters.php in WordPress before 3.3.3 allows remote attackers to inject arbitrary web script or HTML via an editable slug field.
CVE-2012-6634
wp-admin/media-upload.php in WordPress before 3.3.3 allows remote attackers to obtain sensitive information or bypass intended media-attachment restrictions via a post_id value.
CVE-2012-6635
wp-admin/includes/class-wp-posts-list-table.php in WordPress before 3.3.3 does not properly restrict excerpt-view access, which allows remote authenticated users to obtain sensitive information by visiting a draft.
CVE-2013-0157
(a) mount and (b) umount in util-linux 2.14.1, 2.17.2, and probably other versions allow local users to determine the existence of restricted directories by (1) using the --guess-fstype command-line option or (2) attempting to mount a non-existent device, which generates different error messages depending on whether the directory exists.
CVE-2013-0339
libxml2 through 2.9.1 does not properly handle external entities expansion unless an application developer uses the xmlSAX2ResolveEntity or xmlSetExternalEntityLoader function, which allows remote attackers to cause a denial of service (resource consumption), send HTTP requests to intranet servers, or read arbitrary files via a crafted XML document, aka an XML External Entity (XXE) issue.  NOTE: it could be argued that because libxml2 already provides the ability to disable external entity expansion, the responsibility for resolving this issue lies with application developers; according to this argument, this entry should be REJECTed and each affected application would need its own CVE.
CVE-2013-0340
expat 2.1.0 and earlier does not properly handle entities expansion unless an application developer uses the XML_SetEntityDeclHandler function, which allows remote attackers to cause a denial of service (resource consumption), send HTTP requests to intranet servers, or read arbitrary files via a crafted XML document, aka an XML External Entity (XXE) issue.  NOTE: it could be argued that because expat already provides the ability to disable external entity expansion, the responsibility for resolving this issue lies with application developers; according to this argument, this entry should be REJECTed, and each affected application would need its own CVE.
CVE-2013-0485
Unspecified vulnerability in IBM Java SDK 7 before SR4-FP1, 6 before SR13-FP1, 5.0 before SR16-FP1, and 1.4.2 before SR13-FP16 has unknown impact and attack vectors related to Class Libraries.
CVE-2013-1361
Untrusted search path vulnerability in Lenovo Thinkpad Bluetooth with Enhanced Data Rate Software 6.4.0.2900 and earlier allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse DLL that is located in the same folder as a file that is processed by Lenovo Bluetooth.
Per: http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2013-1769
A certain hashing algorithm in Telepathy Gabble 0.16.x before 0.16.5 and 0.17.x before 0.17.3 allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via a crafted message.
CVE-2013-1923
rpc-gssd in nfs-utils before 1.2.8 performs reverse DNS resolution for server names during GSSAPI authentication, which might allow remote attackers to read otherwise-restricted files via DNS spoofing attacks.
CVE-2013-2104
python-keystoneclient before 0.2.4, as used in OpenStack Keystone (Folsom), does not properly check expiry for PKI tokens, which allows remote authenticated users to (1) retain use of a token after it has expired, or (2) use a revoked token once it expires.
CVE-2013-2151
Unquoted Windows search path vulnerability in Red Hat Enterprise Virtualization (RHEV) 3 and 3.2 allows local users to gain privileges via a crafted application in an unspecified folder.
Per: http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2013-2152
Unquoted Windows search path vulnerability in the SPICE service, as used in Red Hat Enterprise Virtualization (RHEV) 3.2, allows local users to gain privileges via a crafted application in an unspecified folder.
Per: http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2013-2594
SQL injection vulnerability in reports/calldiary.php in Hornbill Supportworks ITSM 1.0.0 through 3.4.14 allows remote attackers to execute arbitrary SQL commands via the callref parameter.
CVE-2013-4030
Integrated Management Module (IMM) 2 1.00 through 2.00 on IBM System X and Flex System servers supports SSL cipher suites with short keys, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via a brute-force attack against (1) SSL or (2) TLS traffic.
CVE-2013-4160
Little CMS (lcms2) before 2.5, as used in OpenJDK 7 and possibly other products, allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via vectors related to (1) cmsStageAllocLabV2ToV4curves, (2) cmsPipelineDup, (3) cmsAllocProfileSequenceDescription, (4) CurvesAlloc, and (5) cmsnamed.
Per: http://cwe.mitre.org/data/definitions/476.html "CWE-476: NULL Pointer Dereference"
CVE-2013-4200
The isURLInPortal method in the URLTool class in in_portal.py in Plone 2.1 through 4.1, 4.2.x through 4.2.5, and 4.3.x through 4.3.1 treats URLs starting with a space as a relative URL, which allows remote attackers to bypass the allow_external_login_sites filtering property,  redirect users to arbitrary web sites, and conduct phishing attacks via a space before a URL in the "next" parameter to acl_users/credentials_cookie_auth/require_login.
CVE-2013-4884
Cross-site scripting (XSS) vulnerability in McAfee SuperScan 4.0 allows remote attackers to inject arbitrary web script or HTML via UTF-7 encoded sequences in a server response, which is not properly handled in the SuperScan HTML report.
CVE-2013-5429
The Risk Based Access functionality in IBM Tivoli Federated Identity Manager (TFIM) 6.2.2 before FP9 and Tivoli Federated Identity Manager Business Gateway (TFIMBG) 6.2.2 before FP9 does not prevent reuse of One Time Password (OTP) tokens, which makes it easier for remote authenticated users to complete transactions by leveraging access to an already-used token.
CVE-2013-5986
Unspecified vulnerability in NVIDIA graphics driver Release 331, 325, 319, 310, and 304 has unknown impact and attack vectors, a different vulnerability than CVE-2013-5987.
CVE-2013-5987
Unspecified vulnerability in NVIDIA graphics driver Release 331, 325, 319, 310, and 304 allows local users to bypass intended access restrictions for the GPU and gain privileges via unknown vectors.
CVE-2013-6040
Multiple unspecified vulnerabilities in the MW6 Aztec, DataMatrix, and MaxiCode ActiveX controls allow remote attackers to execute arbitrary code via a crafted HTML document.
CVE-2013-6305
IBM Platform Symphony 5.2 before build 229037 and 6.1.0.1 before build 229073 uses the same credentials encryption key across different customers' installations, which makes it easier for context-dependent attackers to obtain sensitive information by leveraging knowledge of this key.
CVE-2013-6872
SQL injection vulnerability in managetimetracker.php in Collabtive before 1.2 allows remote authenticated users to execute arbitrary SQL commands via the id parameter in a projectpdf action.
CVE-2013-6922
Multiple cross-site request forgery (CSRF) vulnerabilities in the Seagate BlackArmor NAS 220 devices with firmware sg2000-2000.1331 allow remote attackers to hijack the authentication of administrators for requests that (1) add user accounts via a crafted request to admin/access_control_user_add.php; (2) modify or (3) delete user accounts; (4) perform a factory reset; (5) perform a device reboot; or (6) add, (7) modify, or (8) delete shares and volumes.
CVE-2013-7219
SQL injection vulnerability in vote.php in the 2Glux Sexy Polling (com_sexypolling) component before 1.0.9 for Joomla! allows remote attackers to execute arbitrary SQL commands via the answer_id[] parameter.
CVE-2014-0753
Stack-based buffer overflow in the SCADA server in Ecava IntegraXor before 4.1.4390 allows remote attackers to cause a denial of service (system crash) by triggering access to DLL code located in the IntegraXor directory.
CVE-2014-1452
Stack-based buffer overflow in lib/snmpagent.c in bsnmpd, as used in FreeBSD 8.3 through 10.0, allows remote attackers to cause a denial of service (daemon crash) and possibly execute arbitrary code via a crafted GETBULK PDU request.
CVE-2014-1618
Multiple SQL injection vulnerabilities in UAEPD Shopping Cart Script allow remote attackers to execute arbitrary SQL commands via the (1) cat_id or (2) p_id parameter to products.php or id parameter to (3) page.php or (4) news.php.
CVE-2014-1619
Multiple SQL injection vulnerabilities in Cubic CMS 5.1.1, 5.1.2, and 5.2 allow remote attackers to execute arbitrary SQL commands via the (1) resource_id or (2) version_id parameter to recursos/agent.php or (3) login or (4) pass parameter to login.usuario.
CVE-2014-1620
Multiple cross-site scripting (XSS) vulnerabilities in add.php in HIOX Guest Book (HGB) 5.0 allow remote attackers to inject arbitrary web script or HTML via the (1) name1, (2) email, or (3) cmt parameter.
