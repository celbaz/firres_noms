CVE-2016-2922
IBM Rational ClearQuest 8.0 through 8.0.1.9 and 9.0 through 9.0.1.3 (CQ OSLC linkages, EmailRelay) fails to check the SSL certificate against the requested hostname. It is subject to a man-in-the-middle attack with an impersonating server observing all the data transmitted to the real server. IBM X-Force ID: 113353.
CVE-2017-1286
Sensitive information about the configuration of the IBM UrbanCode Deploy 6.1 through 6.9.6.0 server and database can be obtained by a user who has been given elevated permissions in the UI, even after those elevated permissions have been revoked. IBM X-Force ID: 125147.
CVE-2017-15138
The OpenShift Enterprise cluster-read can access webhook tokens which would allow an attacker with sufficient privileges to view confidential webhook tokens.
CVE-2017-1749
IBM UrbanCode Deploy 6.1 through 6.9.6.0 could allow a remote attacker to traverse directories on the system. An unauthenticated attacker could alter UCD deployments. IBM X-Force ID: 135522.
CVE-2017-7500
It was found that rpm did not properly handle RPM installations when a destination path was a symbolic link to a directory, possibly changing ownership and permissions of an arbitrary directory, and RPM files being placed in an arbitrary destination. An attacker, with write access to a directory in which a subdirectory will be installed, could redirect that directory to an arbitrary location and gain root privilege.
CVE-2018-0714
Command injection vulnerability in Helpdesk versions 1.1.21 and earlier in QNAP QTS 4.2.6 build 20180531, QTS 4.3.3 build 20180528, QTS 4.3.4 build 20180528 and their earlier versions could allow remote attackers to run arbitrary commands in the compromised application.
CVE-2018-10569
An issue was discovered in Edimax EW-7438RPn Mini v2 before version 1.26. There is XSS in an SSID field.
CVE-2018-10598
CNCSoft Version 1.00.83 and prior with ScreenEditor Version 1.00.54 has two out-of-bounds read vulnerabilities could cause the software to crash due to lacking user input validation for processing project files. Which may allow an attacker to gain remote code execution with administrator privileges if exploited.
CVE-2018-10634
Medtronic MMT 508 MiniMed insulin pump, 522 / MMT - 722 Paradigm REAL-TIME, 523 / MMT - 723 Paradigm Revel, 523K / MMT - 723K Paradigm Revel, and 551 / MMT - 751 MiniMed 530G communications between the pump and wireless accessories are transmitted in cleartext. A sufficiently skilled attacker could capture these transmissions and extract sensitive information, such as device serial numbers.
CVE-2018-10636
CNCSoft Version 1.00.83 and prior with ScreenEditor Version 1.00.54 has multiple stack-based buffer overflow vulnerabilities that could cause the software to crash due to lacking user input validation before copying data from project files onto the stack. Which may allow an attacker to gain remote code execution with administrator privileges if exploited.
CVE-2018-10842
It was found that an authenticated user could manipulate user session information to trigger an infinite loop in keycloak. A malicious user could use this flaw to conduct a denial of service attack against the server.
CVE-2018-10864
An uncontrolled resource consumption flaw has been discovered in redhat-certification in the way documents are loaded. A remote attacker may provide an existing but invalid XML file which would be opened and never closed, possibly producing a Denial of Service.
CVE-2018-11770
From version 1.3.0 onward, Apache Spark's standalone master exposes a REST API for job submission, in addition to the submission mechanism used by spark-submit. In standalone, the config property 'spark.authenticate.secret' establishes a shared secret for authenticating requests to submit jobs via spark-submit. However, the REST API does not use this or any other authentication mechanism, and this is not adequately documented. In this case, a user would be able to run a driver program without authenticating, but not launch executors, using the REST API. This REST API is also used by Mesos, when set up to run in cluster mode (i.e., when also running MesosClusterDispatcher), for job submission. Future versions of Spark will improve documentation on these points, and prohibit setting 'spark.authenticate.secret' when running the REST APIs, to make this clear. Future versions will also disable the REST API by default in the standalone master by changing the default value of 'spark.master.rest.enabled' to 'false'.
CVE-2018-12587
A cross-site scripting (XSS) vulnerability was found in valeuraddons German Spelling Dictionary v1.3 (an Opera Browser add-on). Instead of providing text for a spelling check, remote attackers may inject arbitrary web script or HTML via the ajax query parameter in the URL Address Bar.
CVE-2018-13392
Several resources in Atlassian Fisheye and Crucible before version 4.6.0 allow remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in linked issue keys.
CVE-2018-13415
In Plex Media Server 1.13.2.5154, the XML parsing engine for SSDP/UPnP functionality is vulnerable to an XML External Entity Processing (XXE) attack. Remote, unauthenticated attackers can use this vulnerability to: (1) Access arbitrary files from the filesystem with the same permission as the user account running Plex, (2) Initiate SMB connections to capture a NetNTLM challenge/response and crack to cleartext password, or (3) Initiate SMB connections to relay a NetNTLM challenge/response and achieve Remote Command Execution in Windows domains.
CVE-2018-13417
In Vuze Bittorrent Client 5.7.6.0, the XML parsing engine for SSDP/UPnP functionality is vulnerable to an XML External Entity Processing (XXE) attack. Remote, unauthenticated attackers can use this vulnerability to: (1) Access arbitrary files from the filesystem with the same permission as the user account running Vuze, (2) Initiate SMB connections to capture a NetNTLM challenge/response and crack to cleartext password, or (3) Initiate SMB connections to relay a NetNTLM challenge/response and achieve Remote Command Execution in Windows domains.
CVE-2018-14781
Medtronic MMT 508 MiniMed insulin pump, 522 / MMT - 722 Paradigm REAL-TIME, 523 / MMT - 723 Paradigm Revel, 523K / MMT - 723K Paradigm Revel, and 551 / MMT - 751 MiniMed 530G The models identified above, when paired with a remote controller and having the "easy bolus" and "remote bolus" options enabled (non-default), are vulnerable to a capture-replay attack. An attacker can capture the wireless transmissions between the remote controller and the pump and replay them to cause an insulin (bolus) delivery.
CVE-2018-14849
Tiki before 18.2, 15.7 and 12.14 has XSS via link attributes, related to lib/core/WikiParser/OutputLink.php and lib/parser/parserlib.php.
CVE-2018-14850
Stored XSS vulnerabilities in Tiki before 18.2, 15.7 and 12.14 allow an authenticated user injecting JavaScript to gain administrator privileges if an administrator opens a wiki page and moves the mouse pointer over a modified link or thumb image.
CVE-2018-14878
JetBrains dotPeek before 2018.2 and ReSharper Ultimate before 2018.1.4 allow attackers to execute code by decompiling a compiled .NET object (such as a DLL or EXE file) with a specific file, because of Deserialization of Untrusted Data.
CVE-2018-15123
Insecure configuration storage in Zipato Zipabox Smart Home Controller BOARD REV - 1 with System Version -118 allows remote attacker perform new attack vectors and take under control device and smart home.
CVE-2018-15124
Weak hashing algorithm in Zipato Zipabox Smart Home Controller BOARD REV - 1 with System Version -118 allows unauthenticated attacker extract clear text passwords and get root access on the device.
CVE-2018-15125
Sensitive Information Disclosure in Zipato Zipabox Smart Home Controller allows remote attacker get sensitive information that expands attack surface.
CVE-2018-15139
Unrestricted file upload in interface/super/manage_site_files.php in versions of OpenEMR before 5.0.1.4 allows a remote authenticated attacker to execute arbitrary PHP code by uploading a file with a PHP extension via the images upload form and accessing it in the images directory.
CVE-2018-15140
Directory traversal in portal/import_template.php in versions of OpenEMR before 5.0.1.4 allows a remote attacker authenticated in the patient portal to read arbitrary files via the "docid" parameter when the mode is set to get.
CVE-2018-15141
Directory traversal in portal/import_template.php in versions of OpenEMR before 5.0.1.4 allows a remote attacker authenticated in the patient portal to delete arbitrary files via the "docid" parameter when the mode is set to delete.
CVE-2018-15142
Directory traversal in portal/import_template.php in versions of OpenEMR before 5.0.1.4 allows a remote attacker authenticated in the patient portal to execute arbitrary PHP code by writing a file with a PHP extension via the "docid" and "content" parameters and accessing it in the traversed directory.
CVE-2018-15143
Multiple SQL injection vulnerabilities in portal/find_appt_popup_user.php in versions of OpenEMR before 5.0.1.4 allow a remote attacker to execute arbitrary SQL commands via the (1) catid or (2) providerid parameter.
CVE-2018-15144
SQL injection vulnerability in interface/de_identification_forms/find_drug_popup.php in versions of OpenEMR before 5.0.1.4 allows a remote authenticated attacker to execute arbitrary SQL commands via the search_term parameter.
CVE-2018-15145
Multiple SQL injection vulnerabilities in portal/add_edit_event_user.php in versions of OpenEMR before 5.0.1.4 allow a remote attacker to execute arbitrary SQL commands via the (1) eid, (2) userid, or (3) pid parameter.
CVE-2018-3780
A missing sanitization of search results for an autocomplete field in NextCloud Server <13.0.5 could lead to a stored XSS requiring user-interaction. The missing sanitization only affected user names, hence malicious search results could only be crafted by authenticated users.
CVE-2018-3781
A missing sanitization of search results for an autocomplete field in NextCloud Talk <3.2.5 could lead to a stored XSS requiring user-interaction. The missing sanitization only affected user names, hence malicious search results could only be crafted by authenticated users.
CVE-2018-3782
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-1002203.  Reason: This candidate is a reservation duplicate of CVE-2018-1002203.  Notes: All CVE users should reference CVE-2018-1002203 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-5924
A security vulnerability has been identified with certain HP Inkjet printers. A maliciously crafted file sent to an affected device can cause a stack buffer overflow, which could allow remote code execution.
CVE-2018-5925
A security vulnerability has been identified with certain HP Inkjet printers. A maliciously crafted file sent to an affected device can cause a static buffer overflow, which could allow remote code execution.
CVE-2018-6414
A buffer overflow vulnerability in the web server of some Hikvision IP Cameras allows an attacker to send a specially crafted message to affected devices. Due to the insufficient input validation, successful exploit can corrupt memory and lead to arbitrary code execution or crash the process.
CVE-2018-6970
VMware Horizon 6 (6.x.x before 6.2.7), Horizon 7 (7.x.x before 7.5.1), and Horizon Client (4.x.x and prior before 4.8.1) contain an out-of-bounds read vulnerability in the Message Framework library. Successfully exploiting this issue may allow a less-privileged user to leak information from a privileged process running on a system where Horizon Connection Server, Horizon Agent or Horizon Client are installed. Note: This issue doesn't apply to Horizon 6, 7 Agents installed on Linux systems or Horizon Clients installed on non-Windows systems.
