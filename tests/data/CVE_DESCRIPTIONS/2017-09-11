CVE-2015-4523
Blue Coat Malware Analysis Appliance (MAA) before 4.2.5 and Malware Analyzer G2 allow remote attackers to bypass a virtual machine protection mechanism and consequently write to arbitrary files, cause a denial of service (host reboot or reset to factory defaults), or execute arbitrary code via vectors related to saving files during analysis.
CVE-2015-4687
Cross-site scripting (XSS) vulnerability in Ellucian (formerly SunGard) Banner Student 8.5.1.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4688
Ellucian (formerly SunGard) Banner Student 8.5.1.2 through 8.7 allow remote attackers to enumerate user accounts via a series of requests.
CVE-2015-4689
Ellucian (formerly SunGard) Banner Student 8.5.1.2 through 8.7 allows remote attackers to reset arbitrary passwords via unspecified vectors, aka "Weak Password Reset."
CVE-2015-5054
Open redirect vulnerability in Ellucian (formerly SunGard) Banner Student 8.5.1.2 through 8.7 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in an unspecified parameter.
CVE-2015-7877
Multiple SQL injection vulnerabilities in the User Dashboard module 7.x before 7.x-1.4 for Drupal allow remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-7879
Cross-site scripting (XSS) vulnerability in the Stickynote module 7.x before 7.x-1.3 for Drupal allows remote authenticated users with permission to create or edit a stickynote to inject arbitrary web script or HTML via note text on the admin listing page.
CVE-2015-8349
Cross-site scripting (XSS) vulnerability in SourceBans before 2.0 pre-alpha allows remote attackers to inject arbitrary web script or HTML via the advSearch parameter to index.php.
CVE-2015-8350
Multiple cross-site scripting (XSS) vulnerabilities in the Calls to Action plugin before 2.5.1 for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) open-tab parameter in a wp_cta_global_settings action to wp-admin/edit.php or (2) wp-cta-variation-id parameter to ab-testing-call-to-action-example/.
CVE-2015-8351
PHP remote file inclusion vulnerability in the Gwolle Guestbook plugin before 1.5.4 for WordPress, when allow_url_include is enabled, allows remote authenticated users to execute arbitrary PHP code via a URL in the abspath parameter to frontend/captcha/ajaxresponse.php.  NOTE: this can also be leveraged to include and execute arbitrary local files via directory traversal sequences regardless of whether allow_url_include is enabled.
CVE-2015-8353
Cross-site scripting (XSS) vulnerability in the Role Scoper plugin before 1.3.67 for WordPress allows remote attackers to inject arbitrary web script or HTML via the object_name parameter in a rs-object_role_edit page to wp-admin/admin.php.
CVE-2015-8354
Cross-site scripting (XSS) vulnerability in the Ultimate Member WordPress plugin before 1.3.29 for WordPress allows remote attackers to inject arbitrary web script or HTML via the _refer parameter to wp-admin/users.php.
CVE-2015-9226
Multiple SQL injection vulnerabilities in AlegroCart 1.2.8 allow remote administrators to execute arbitrary SQL commands via the download parameter in the (1) check_download and possibly (2) check_filename function in upload/admin2/model/products/model_admin_download.php or remote authenticated users with a valid Paypal transaction token to execute arbitrary SQL commands via the ref parameter in the (3) orderUpdate function in upload/catalog/extension/payment/paypal.php.
CVE-2015-9227
PHP remote file inclusion vulnerability in the get_file function in upload/admin2/controller/report_logs.php in AlegroCart 1.2.8 allows remote administrators to execute arbitrary PHP code via a URL in the file_path parameter to upload/admin2.
CVE-2017-1000249
An issue in file() was introduced in commit 9611f31313a93aa036389c5f3b15eea53510d4d1 (Oct 2016) lets an attacker overwrite a fixed 20 bytes stack buffer with a specially crafted .notes section in an ELF binary. This was fixed in commit 35c94dc6acc418f1ad7f6241a6680e5327495793 (Aug 2017).
CVE-2017-14075
This vulnerability allows local attackers to escalate privileges on Jungo WinDriver 12.4.0 and earlier. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability. The specific flaw exists within the processing of IOCTL 0x953824a7 by the windrvr1240 kernel driver. The issue lies in the failure to properly validate user-supplied data which can result in an out-of-bounds write condition. An attacker can leverage this vulnerability to execute arbitrary code under the context of kernel.
CVE-2017-14153
This vulnerability allows local attackers to escalate privileges on Jungo WinDriver 12.4.0 and earlier. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability. The specific flaw exists within the processing of IOCTL 0x953824b7 by the windrvr1240 kernel driver. The issue lies in the failure to properly validate user-supplied data which can result in a kernel pool overflow. An attacker can leverage this vulnerability to execute arbitrary code under the context of kernel.
CVE-2017-14238
SQL injection vulnerability in admin/menus/edit.php in Dolibarr ERP/CRM version 6.0.0 allows remote attackers to execute arbitrary SQL commands via the menuId parameter.
CVE-2017-14239
Multiple cross-site scripting (XSS) vulnerabilities in Dolibarr ERP/CRM 6.0.0 allow remote authenticated users to inject arbitrary web script or HTML via the (1) CompanyName, (2) CompanyAddress, (3) CompanyZip, (4) CompanyTown, (5) Fax, (6) EMail, (7) Web, (8) ManagingDirectors, (9) Note, (10) Capital, (11) ProfId1, (12) ProfId2, (13) ProfId3, (14) ProfId4, (15) ProfId5, or (16) ProfId6 parameter to htdocs/admin/company.php.
CVE-2017-14240
There is a sensitive information disclosure vulnerability in document.php in Dolibarr ERP/CRM version 6.0.0 via the file parameter.
CVE-2017-14241
Cross-site scripting (XSS) vulnerability in Dolibarr ERP/CRM 6.0.0 allows remote authenticated users to inject arbitrary web script or HTML via the Title parameter to htdocs/admin/menus/edit.php.
CVE-2017-14242
SQL injection vulnerability in don/list.php in Dolibarr version 6.0.0 allows remote attackers to execute arbitrary SQL commands via the statut parameter.
CVE-2017-14247
SQL Injection exists in the EyesOfNetwork web interface (aka eonweb) 5.1-0 via the user_id cookie to header.php, a related issue to CVE-2017-1000060.
CVE-2017-14248
A heap-based buffer over-read in SampleImage() in MagickCore/resize.c in ImageMagick 7.0.6-8 Q16 allows remote attackers to cause a denial of service via a crafted file.
CVE-2017-14249
ImageMagick 7.0.6-8 Q16 mishandles EOF checks in ReadMPCImage in coders/mpc.c, leading to division by zero in GetPixelCacheTileSize in MagickCore/cache.c, allowing remote attackers to cause a denial of service via a crafted file.
CVE-2017-14251
Unrestricted File Upload vulnerability in the fileDenyPattern in sysext/core/Classes/Core/SystemEnvironmentBuilder.php in TYPO3 7.6.0 to 7.6.21 and 8.0.0 to 8.7.4 allows remote authenticated users to upload files with a .pht extension and consequently execute arbitrary PHP code.
CVE-2017-14252
SQL Injection exists in the EyesOfNetwork web interface (aka eonweb) 5.1-0 via the group_id cookie to side.php.
CVE-2017-14257
In the SDK in Bento4 1.5.0-616, AP4_AtomSampleTable::GetSample in Core/Ap4AtomSampleTable.cpp contains a Read Memory Access Violation vulnerability. It is possible to exploit this vulnerability by opening a crafted .MP4 file.
CVE-2017-14258
In the SDK in Bento4 1.5.0-616, SetItemCount in Core/Ap4StscAtom.h file contains a Write Memory Access Violation vulnerability. It is possible to exploit this vulnerability and possibly execute arbitrary code by opening a crafted .MP4 file.
CVE-2017-14259
In the SDK in Bento4 1.5.0-616, the AP4_StscAtom class in Ap4StscAtom.cpp contains a Write Memory Access Violation vulnerability. It is possible to exploit this vulnerability and possibly execute arbitrary code by opening a crafted .MP4 file.
CVE-2017-14260
In the SDK in Bento4 1.5.0-616, the AP4_StssAtom class in Ap4StssAtom.cpp contains a Write Memory Access Violation vulnerability. It is possible to exploit this vulnerability and possibly execute arbitrary code by opening a crafted .MP4 file.
CVE-2017-14261
In the SDK in Bento4 1.5.0-616, the AP4_StszAtom class in Ap4StszAtom.cpp file contains a Read Memory Access Violation vulnerability. It is possible to exploit this vulnerability by opening a crafted .MP4 file.
CVE-2017-14262
On Samsung NVR devices, remote attackers can read the MD5 password hash of the 'admin' account via certain szUserName JSON data to cgi-bin/main-cgi, and login to the device with that hash in the szUserPasswd parameter.
CVE-2017-14263
Honeywell NVR devices allow remote attackers to create a user account in the admin group by leveraging access to a guest account to obtain a session ID, and then sending that session ID in a userManager.addUser request to the /RPC2 URI. The attacker can login to the device with that new user account to fully control the device.
CVE-2017-14265
A Stack-based Buffer Overflow was discovered in xtrans_interpolate in internal/dcraw_common.cpp in LibRaw before 0.18.3. It could allow a remote denial of service or code execution attack.
CVE-2017-14267
EE 4GEE WiFi MBB (before EE60_00_05.00_31) devices have CSRF, related to goform/AddNewProfile, goform/setWanDisconnect, goform/setSMSAutoRedirectSetting, goform/setReset, and goform/uploadBackupSettings.
CVE-2017-14268
EE 4GEE WiFi MBB (before EE60_00_05.00_31) devices have XSS in the sms_content parameter in a getSMSlist request.
CVE-2017-14269
EE 4GEE WiFi MBB (before EE60_00_05.00_31) devices allow remote attackers to obtain sensitive information via a JSONP endpoint, as demonstrated by passwords and SMS content.
CVE-2017-14270
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at ntdll_77400000!RtlFillMemoryUlong+0x0000000000000010."
CVE-2017-14271
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at ntdll_77400000!RtlImpersonateSelfEx+0x000000000000024e."
CVE-2017-14272
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at jbig2dec+0x000000000000595d."
CVE-2017-14273
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at ntdll_77400000!RtlInterlockedPopEntrySList+0x00000000000003b0."
CVE-2017-14274
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at jbig2dec+0x0000000000008706."
CVE-2017-14275
XnView Classic for Windows Version 2.40 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV near NULL starting at wow64!Wow64NotifyDebugger+0x000000000000001d."
CVE-2017-14276
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Possible Stack Corruption starting at jbig2dec+0x0000000000002fbe."
CVE-2017-14277
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at jbig2dec+0x0000000000005956."
CVE-2017-14278
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at jbig2dec+0x0000000000005940."
CVE-2017-14279
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at jbig2dec+0x0000000000005643."
CVE-2017-14280
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at jbig2dec+0x000000000000571d."
CVE-2017-14281
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address is used as one or more arguments in a subsequent Function Call starting at jbig2dec+0x00000000000090f1."
CVE-2017-14282
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at jbig2dec+0x0000000000005862."
CVE-2017-14283
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at jbig2dec+0x0000000000008fe4."
CVE-2017-14284
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at ntdll_77400000!RtlGetCurrentDirectory_U+0x000000000000016c."
CVE-2017-14285
XnView Classic for Windows Version 2.40 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at ntdll_77400000!RtlInterlockedPopEntrySList+0x000000000000039b."
CVE-2017-14286
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllUnregisterServer+0x000000000000cb8c."
CVE-2017-14287
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "Read Access Violation on Control Flow starting at STDUJBIG2File+0x00000000000015eb."
CVE-2017-14288
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllGetClassObject+0x0000000000002ff7."
CVE-2017-14289
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllGetClassObject+0x000000000000303e."
CVE-2017-14290
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Heap Corruption starting at wow64!Wow64NotifyDebugger+0x000000000000001d."
CVE-2017-14291
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllUnregisterServer+0x00000000000076d8."
CVE-2017-14292
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllUnregisterServer+0x000000000000570e."
CVE-2017-14293
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Heap Corruption starting at wow64!Wow64LdrpInitialize+0x00000000000008e1."
CVE-2017-14294
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to a "User Mode Write AV starting at STDUJBIG2File!DllUnregisterServer+0x000000000000566e."
CVE-2017-14295
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls Code Flow starting at STDUJBIG2File+0x00000000000015e9."
CVE-2017-14296
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at STDUJBIG2File!DllGetClassObject+0x00000000000043e6."
CVE-2017-14297
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls Code Flow starting at STDUJBIG2File!DllGetClassObject+0x0000000000002f35."
CVE-2017-14298
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at STDUJBIG2File!DllGetClassObject+0x00000000000038e8."
CVE-2017-14299
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at STDUJBIG2File!DllGetClassObject+0x000000000000384b."
CVE-2017-14300
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at STDUJBIG2File!DllGetClassObject+0x0000000000004479."
CVE-2017-14301
STDU Viewer 1.6.375 allows attackers to execute arbitrary code or cause a denial of service via a crafted .jb2 file, related to "Data from Faulting Address controls subsequent Write Address starting at STDUJBIG2File!DllUnregisterServer+0x00000000000076d3."
CVE-2017-14302
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at STDUJBIG2File!DllGetClassObject+0x00000000000064d7."
CVE-2017-14303
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllGetClassObject+0x0000000000003047."
CVE-2017-14304
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllGetClassObject+0x00000000000043e0."
CVE-2017-14305
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at STDUJBIG2File!DllUnregisterServer+0x0000000000005578."
CVE-2017-14306
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllUnregisterServer+0x0000000000006e10."
CVE-2017-14307
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to "Data from Faulting Address controls Branch Selection starting at ntdll_77400000!TpAllocCleanupGroup+0x0000000000000402."
CVE-2017-14308
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllUnregisterServer+0x0000000000006ddd."
CVE-2017-14309
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllUnregisterServer+0x0000000000006ec8."
CVE-2017-14310
STDU Viewer 1.6.375 allows attackers to cause a denial of service or possibly have unspecified other impact via a crafted .jb2 file, related to a "Read Access Violation starting at STDUJBIG2File!DllUnregisterServer+0x0000000000001869."
CVE-2017-14312
Nagios Core through 4.3.4 initially executes /usr/sbin/nagios as root but supports configuration options in which this file is owned by a non-root account (and similarly can have nagios.cfg owned by a non-root account), which allows local users to gain privileges by leveraging access to this non-root account.
CVE-2017-7649
The network enabled distribution of Kura before 2.1.0 takes control over the device's firewall setup but does not allow IPv6 firewall rules to be configured. Still the Equinox console port 5002 is left open, allowing to log into Kura without any user credentials over unencrypted telnet and executing commands using the Equinox "exec" command. As the process is running as "root" full control over the device can be acquired. IPv6 is also left in auto-configuration mode, accepting router advertisements automatically and assigns a MAC address based IPv6 address.
CVE-2017-7650
In Mosquitto before 1.4.12, pattern based ACLs can be bypassed by clients that set their username/client id to '#' or '+'. This allows locally or remotely connected clients to access MQTT topics that they do have the rights to. The same issue may be present in third party authentication/access control plugins for Mosquitto.
