CVE-2012-4348
The management console in Symantec Endpoint Protection (SEP) 11.0 before RU7-MP3 and 12.1 before RU2, and Symantec Endpoint Protection Small Business Edition 12.x before 12.1 RU2, does not properly validate input for PHP scripts, which allows remote authenticated users to execute arbitrary code via unspecified vectors.
CVE-2012-4350
Multiple unquoted Windows search path vulnerabilities in the (1) Manager and (2) Agent components in Symantec Enterprise Security Manager (ESM) before 11.0 allow local users to gain privileges via unspecified vectors.
Per http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2012-4691
Memory leak in Siemens Automation License Manager (ALM) 4.x and 5.x before 5.2 allows remote attackers to cause a denial of service (memory consumption) via crafted packets.
Per: http://www.siemens.com/corporate-technology/pool/de/forschungsfelder/siemens_security_advisory_ssa-783261.pdf

"The attacker must have access to the local subnet where ALM is located. During installation, the default setting of the Windows firewall is to block the port used by ALM for all networks except the local subnet. If this setting has not been changed by the administrator, these vulnerabilities cannot be exploited from remote networks. Additionally, communication to this port should be blocked at network borders using appropriate security measures like firewalls."
CVE-2012-4693
Invensys Wonderware InTouch 2012 R2 and earlier and Siemens ProcessSuite use a weak encryption algorithm for data in Ps_security.ini, which makes it easier for local users to discover passwords by reading this file.
CVE-2012-4898
Mesh OS before 7.9.1.1 on Tropos wireless mesh routers does not use a sufficient source of entropy for SSH keys, which makes it easier for man-in-the-middle attackers to spoof a device or modify a client-server data stream by leveraging knowledge of a key from a product installation elsewhere.
CVE-2012-5195
Heap-based buffer overflow in the Perl_repeatcpy function in util.c in Perl 5.12.x before 5.12.5, 5.14.x before 5.14.3, and 5.15.x before 15.15.5 allows context-dependent attackers to cause a denial of service (memory consumption and crash) or possibly execute arbitrary code via the 'x' string repeat operator.
CVE-2012-5468
Heap-based buffer overflow in iconvert.c in the bogolexer component in Bogofilter before 1.2.3 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via an email containing a base64 string that is decoded to incomplete multibyte characters.
CVE-2012-5563
OpenStack Keystone, as used in OpenStack Folsom 2012.2, does not properly implement token expiration, which allows remote authenticated users to bypass intended authorization restrictions by creating new tokens through token chaining.  NOTE: this issue exists because of a CVE-2012-3426 regression.
CVE-2012-5571
OpenStack Keystone Essex (2012.1) and Folsom (2012.2) does not properly handle EC2 tokens when the user role has been removed from a tenant, which allows remote authenticated users to bypass intended authorization restrictions by leveraging a token for the removed user role.
CVE-2012-5574
lib/form/sfForm.class.php in Symfony CMS before 1.4.20 allows remote attackers to read arbitrary files via a crafted upload request.
CVE-2012-5576
Multiple stack-based buffer overflows in file-xwd.c in the X Window Dump (XWD) plug-in in GIMP 2.8.2 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large (1) red, (2) green, or (3) blue color mask in an XWD file.
CVE-2012-5606
Multiple cross-site scripting (XSS) vulnerabilities in ownCloud before 4.0.9 and 4.5.0 allow remote attackers to inject arbitrary web script or HTML via the (1) file name to apps/files_versions/js/versions.js or (2) apps/files/js/filelist.js; or (3) event title to 3rdparty/fullcalendar/js/fullcalendar.js.
CVE-2012-5607
The "Lost Password" reset functionality in ownCloud before 4.0.9 and 4.5.0 does not properly check the security token, which allows remote attackers to change an accounts password via unspecified vectors related to a "Remote Timing Attack."
CVE-2012-5608
Cross-site scripting (XSS) vulnerability in apps/user_webdavauth/settings.php in ownCloud 4.5.x before 4.5.2 allows remote attackers to inject arbitrary web script or HTML via arbitrary POST parameters.
CVE-2012-5609
Incomplete blacklist vulnerability in lib/migrate.php in ownCloud before 4.5.2 allows remote authenticated users to execute arbitrary PHP code by uploading a crafted mount.php file in a ZIP file.
Per: http://cwe.mitre.org/data/definitions/184.html 'CWE-184: Incomplete Blacklist'
CVE-2012-5610
Incomplete blacklist vulnerability in lib/filesystem.php in ownCloud before 4.0.9 and 4.5.x before 4.5.2 allows remote authenticated users to execute arbitrary PHP code by uploading a file with a special crafted name.
Per: http://cwe.mitre.org/data/definitions/184.html 'CWE-184: Incomplete Blacklist'
CVE-2012-5622
Cross-site request forgery (CSRF) vulnerability in the management console (openshift-console/app/controllers/application_controller.rb) in OpenShift 0.0.5 allows remote attackers to hijack the authentication of arbitrary users via unspecified vectors.
CVE-2012-6422
The kernel in Samsung Galaxy S2, Galaxy Note 2, MEIZU MX, and possibly other Android devices, when running an Exynos 4210 or 4412 processor, uses weak permissions (0666) for /dev/exynos-mem, which allows attackers to read or write arbitrary physical memory and gain privileges via a crafted application, as demonstrated by ExynosAbuse.
