CVE-2007-5655
TIBCO SmartSockets RTserver 6.8.0 and earlier, RTworks before 4.0.4, and Enterprise Message Service (EMS) 4.0.0 through 4.4.1 allows remote attackers to execute arbitrary code via crafted requests containing values that are used as pointers.
CVE-2007-5656
TIBCO SmartSockets RTserver 6.8.0 and earlier, RTworks before 4.0.4, and Enterprise Message Service (EMS) 4.0.0 through 4.4.1 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via crafted requests that control loop operations related to memory.
CVE-2007-5657
TIBCO SmartSockets RTserver 6.8.0 and earlier, RTworks before 4.0.4, and Enterprise Message Service (EMS) 4.0.0 through 4.4.1 allows remote attackers to execute arbitrary code via crafted requests containing values that are used as pointer offsets.
CVE-2007-5658
Heap-based buffer overflow in TIBCO SmartSockets RTserver 6.8.0 and earlier, RTworks before 4.0.4, and Enterprise Message Service (EMS) 4.0.0 through 4.4.1 allows remote attackers to execute arbitrary code via crafted requests containing size and copy-length values that trigger the overflow.
CVE-2008-0031
Unspecified vulnerability in Apple QuickTime before 7.4 allows remote attackers to cause a denial of service (application termination) and execute arbitrary code via a crafted Sorenson 3 video file, which triggers memory corruption.
CVE-2008-0032
Apple QuickTime before 7.4 allows remote attackers to execute arbitrary code via a movie file containing a Macintosh Resource record with a modified length value in the resource header, which triggers heap corruption.
CVE-2008-0033
Unspecified vulnerability in Apple QuickTime before 7.4 allows remote attackers to cause a denial of service (application termination) and execute arbitrary code via a movie file with Image Descriptor (IDSC) atoms containing an invalid atom size, which triggers memory corruption.
CVE-2008-0034
Unspecified vulnerability in Passcode Lock in Apple iPhone 1.0 through 1.1.2 allows users with physical access to execute applications without entering the passcode via vectors related to emergency calls.
CVE-2008-0035
Unspecified vulnerability in Foundation, as used in Apple iPhone 1.0 through 1.1.2, iPod touch 1.1 through 1.1.2, and Mac OS X 10.5 through 10.5.1, allows remote attackers to cause a denial of service (application termination) or execute arbitrary code via a crafted URL that triggers memory corruption in Safari.
CVE-2008-0036
Buffer overflow in Apple QuickTime before 7.4 allows remote attackers to execute arbitrary code via a crafted compressed PICT image, which triggers the overflow during decoding.
CVE-2008-0081
Unspecified vulnerability in Microsoft Excel 2000 SP3 through 2003 SP2, Viewer 2003, and Office 2004 for Mac allows user-assisted remote attackers to execute arbitrary code via crafted macros, aka "Macro Validation Vulnerability," a different vulnerability than CVE-2007-3490.
CVE-2008-0122
Off-by-one error in the inet_network function in libbind in ISC BIND 9.4.2 and earlier, as used in libc in FreeBSD 6.2 through 7.0-PRERELEASE, allows context-dependent attackers to cause a denial of service (crash) and possibly execute arbitrary code via crafted input that triggers memory corruption.
CVE-2008-0216
The ptsname function in FreeBSD 6.0 through 7.0-PRERELEASE does not properly verify that a certain portion of a device name is associated with a pty of a user who is calling the pt_chown function, which might allow local users to read data from the pty from another user.
CVE-2008-0217
The script program in FreeBSD 5.0 through 7.0-PRERELEASE invokes openpty, which creates a pseudo-terminal with world-readable and world-writable permissions when it is not run as root, which allows local users to read data from the terminal of the user running script.
CVE-2008-0285
ngIRCd 0.10.x before 0.10.4 and 0.11.0 before 0.11.0-pre2 allows remote attackers to cause a denial of service (crash) via crafted IRC PART message, which triggers an invalid dereference.
CVE-2008-0286
SQL injection vulnerability in admin/login.php in Article Dashboard allows remote attackers to execute arbitrary SQL commands via the (1) user or (2) password fields.
CVE-2008-0287
PHP remote file inclusion vulnerability in VisionBurst vcart 3.3.2 allows remote attackers to execute arbitrary PHP code via a URL in the abs_path parameter to (1) index.php and (2) checkout.php.
CVE-2008-0288
Multiple SQL injection vulnerabilities in ImageAlbum 2.0.0b2 allow remote attackers to execute arbitrary SQL commands via the id, which is not properly handled in (1) classes/IADomain.php, (2) classes/IACollection.php, and (3) classes/IAUser.php, as demonstrated via the id parameter in a collection.imageview action.
CVE-2008-0289
PHP remote file inclusion vulnerability in view_func.php in Member Area System (MAS) 1.7 and possibly others allows remote attackers to execute arbitrary PHP code via a URL in the i parameter.  NOTE: a second vector might exist via the l parameter.  NOTE: as of 20080118, the vendor has disputed the set of affected versions, stating that the issue "is already fixed, for almost a year."
CVE-2008-0290
Multiple SQL injection vulnerabilities in Digital Hive 2.0 RC2 and earlier allow (1) remote attackers to execute arbitrary SQL commands via the selectskin parameter to an unspecified program, or (2) remote authenticated administrators to execute arbitrary SQL commands via the user_id parameter in the gestion_membre.php page to base.php.
CVE-2008-0291
SQL injection vulnerability in showproduct.asp in RichStrong CMS allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2008-0292
Cross-site scripting (XSS) vulnerability in photo_album.pl in Dansie Photo Album 1.0 allows remote attackers to inject arbitrary web script or HTML via the search parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0293
Unspecified vulnerability in cron.php in FreeSeat before 1.1.5d, when format.php has certain modifications, allows remote attackers to bypass authentication and gain privileges via unspecified vectors related to the show_foot function.
CVE-2008-0294
Unspecified vulnerability in the seat-locking implementation in FreeSeat before 1.1.5d allows attackers to book a seat more than once via unspecified vectors.
CVE-2008-0295
Heap-based buffer overflow in modules/access/rtsp/real_sdpplin.c in the Xine library, as used in VideoLAN VLC Media Player 0.8.6d and earlier, allows user-assisted remote attackers to cause a denial of service (crash) or execute arbitrary code via long Session Description Protocol (SDP) data.
CVE-2008-0296
Heap-based buffer overflow in the libaccess_realrtsp plugin in VideoLAN VLC Media Player 0.8.6d and earlier on Windows might allow remote RTSP servers to cause a denial of service (application crash) or execute arbitrary code via a long string.
CVE-2008-0297
PhotoKorn allows remote attackers to obtain database credentials via a direct request to update/update3.php, which includes the credentials in its output.
CVE-2008-0298
KHTML WebKit as used in Apple Safari 2.x allows remote attackers to cause a denial of service (browser crash) via a crafted web page, possibly involving a STYLE attribute of a DIV element.
CVE-2008-0299
common.py in Paramiko 1.7.1 and earlier, when using threads or forked processes, does not properly use RandomPool, which allows one session to obtain sensitive information from another session by predicting the state of the pool.
