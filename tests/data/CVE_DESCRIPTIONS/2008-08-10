CVE-2008-3273
JBoss Enterprise Application Platform (aka JBossEAP or EAP) before 4.2.0.CP03, and 4.3.0 before 4.3.0.CP01, allows remote attackers to obtain sensitive information about "deployed web contexts" via a request to the status servlet, as demonstrated by a full=true query string.
CVE-2008-3561
SQL injection vulnerability in s03.php in Powergap Shopsystem, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the ag parameter.
CVE-2008-3562
Directory traversal vulnerability in index.php in the Contact module in Chupix CMS 0.1.0, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the mods parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3563
Multiple SQL injection vulnerabilities in Plogger 3.0 and earlier allow remote attackers to execute arbitrary SQL commands via (1) the checked array parameter to plog-download.php in an album action and (2) unspecified parameters to plog-remote.php, and (3) allow remote authenticated administrators to execute arbitrary SQL commands via the activate parameter to admin/plog-themes.php, related to theme_dir settings.
CVE-2008-3564
Multiple directory traversal vulnerabilities in index.php in Dayfox Blog 4 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the (1) p, (2) cat, and (3) archive parameters.  NOTE: in some environments, this can be leveraged for remote file inclusion by using a UNC share pathname or an ftp, ftps, or ssh2.sftp URL.
CVE-2008-3565
Multiple cross-site scripting (XSS) vulnerabilities in Meeting Room Booking System (MRBS) 1.2.6 allow remote attackers to inject arbitrary web script or HTML via the area parameter to (1) day.php, (2) week.php, (3) month.php, (4) search.php, (5) report.php, and (6) help.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3566
Cross-site scripting (XSS) vulnerability in ZoneO-soft freeForum 1.7 allows remote attackers to inject arbitrary web script or HTML via the acuparam parameter to (1) the default URI or (2) index.php, or (3) the PATH_INFO to index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3567
Cross-zone scripting vulnerability in the NowPlaying functionality in NullSoft Winamp before 5.541 allows remote attackers to conduct cross-site scripting (XSS) attacks via an MP3 file with JavaScript in id3 tags.
CVE-2008-3568
Absolute path traversal vulnerability in fckeditor/editor/filemanager/browser/default/connectors/php/connector.php in UNAK-CMS 1.5.5 allows remote attackers to include and execute arbitrary local files via a full pathname in the Dirroot parameter, a different vulnerability than CVE-2006-4890.1.
CVE-2008-3569
Multiple cross-site scripting (XSS) vulnerabilities in XAMPP 1.6.7, when register_globals is enabled, allow remote attackers to inject arbitrary web script or HTML via the text parameter to (1) iart.php and (2) ming.php.
CVE-2008-3570
PHP remote file inclusion vulnerability in index.php in Africa Be Gone (ABG) 1.0a allows remote attackers to execute arbitrary PHP code via a URL in the abg_path parameter.
CVE-2008-3571
The Xerox Phaser 8400 allows remote attackers to cause a denial of service (reboot) via an empty UDP packet to port 1900.
CVE-2008-3572
Cross-site scripting (XSS) vulnerability in index.php in Pligg 9.9.5 allows remote attackers to inject arbitrary web script or HTML via the category parameter.
CVE-2008-3573
The CAPTCHA implementation in (1) Pligg 9.9.5 and possibly (2) Francisco Burzi PHP-Nuke 8.1 provides a critical random number (the ts_random value) within the URL in the SRC attribute of an IMG element, which allows remote attackers to pass the CAPTCHA test via a calculation that combines this value with the current date and the HTTP User-Agent string.
CVE-2008-3574
Multiple cross-site scripting (XSS) vulnerabilities in Pluck 4.5.2, when register_globals is enabled, allow remote attackers to inject arbitrary web script or HTML via the (1) lang_footer parameter to (a) data/inc/footer.php; the (2) pluck_version, (3) lang_install22, (4) titelkop, (5) lang_kop1, (6) lang_kop2, (7) lang_modules, (8) lang_kop4, (9) lang_kop15, (10) lang_kop5, and (11) titelkop parameters to (b) data/inc/header.php; the pluck_version and titelkop parameters to (c) data/inc/header2.php; and the (14) lang_theme6 parameter to (d) data/inc/themeinstall.php.
CVE-2008-3575
PHP remote file inclusion vulnerability in modules/calendar/minicalendar.php in ezContents CMS allows remote attackers to execute arbitrary PHP code via a URL in the GLOBALS[gsLanguage] parameter, a different vector than CVE-2006-4477 and CVE-2004-0132.
CVE-2008-3576
Buffer overflow in the TruncateString function in src/gfx.cpp in OpenTTD before 0.6.2 allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via a crafted string.  NOTE: some of these details are obtained from third party information.
CVE-2008-3577
Buffer overflow in src/openttd.cpp in OpenTTD before 0.6.2 allows local users to execute arbitrary code via a large filename supplied to the "-g" parameter in the ttd_main function.  NOTE: it is unlikely that this issue would cross privilege boundaries in typical environments.
CVE-2008-3578
HydraIRC 0.3.164 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a long irc:// URI.
CVE-2008-3579
Calacode @Mail 5.41 on Linux does not require administrative authentication for build-plesk-upgrade.php, which allows remote attackers to obtain sensitive information by creating and downloading a backup archive of the entire @Mail directory tree.  NOTE: this can be leveraged for remote exploitation of CVE-2008-3395.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3580
Multiple SQL injection vulnerabilities in Qsoft K-Links allow remote attackers to execute arbitrary SQL commands via (1) the id parameter to visit.php, or the PATH_INFO to the default URI under (2) report/, (3) addreview/, or (4) refer/.
CVE-2008-3581
Cross-site scripting (XSS) vulnerability in index.php in Qsoft K-Links allows remote attackers to inject arbitrary web script or HTML via the login_message parameter in a login action.
CVE-2008-3582
SQL injection vulnerability in login.php in Keld PHP-MySQL News Script 0.7.1 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2008-3583
Buffer overflow in the HTML parser in IntelliTamper 2.07 allows remote attackers to execute arbitrary code via a long URL in the SRC attribute of an IMG element.  NOTE: this might be related to CVE-2008-3360.  NOTE: it was later reported that 2.08 Beta 4 is also affected.
