CVE-2011-4941
Unspecified vulnerability in Piwik 1.2 through 1.4 allows remote attackers with the view permission to execute arbitrary code via unknown attack vectors.
CVE-2012-1183
Stack-based buffer overflow in the milliwatt_generate function in the Miliwatt application in Asterisk 1.4.x before 1.4.44, 1.6.x before 1.6.2.23, 1.8.x before 1.8.10.1, and 10.x before 10.2.1, when the o option is used and the internal_timing option is off, allows remote attackers to cause a denial of service (application crash) via a large number of samples in an audio packet.
CVE-2012-1184
Stack-based buffer overflow in the ast_parse_digest function in main/utils.c in Asterisk 1.8.x before 1.8.10.1 and 10.x before 10.2.1 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long string in an HTTP Digest Authentication header.
CVE-2012-1654
Multiple cross-site scripting (XSS) vulnerabilities in the Data module 6.x-1.x before 6.x-1.0 and 7.x-1.x before 7.x-1.0-alpha3 for Drupal allow remote authenticated users with the administer data tables permission to inject arbitrary web script or HTML via the title parameter in (1) data.views.inc and (2) data_ui/data_ui.admin.inc.
CVE-2012-1655
Unspecified vulnerability in the UC PayDutchGroup / WeDeal payment module 6.x-1.0 for Drupal allows remote authenticated users to obtain account credentials via unknown attack vectors.
CVE-2012-1656
SQL injection vulnerability in the Multisite Search module 6.x-2.2 for Drupal allows remote authenticated users with certain permissions to execute arbitrary SQL commands via the Site table prefix field.
CVE-2012-1657
Cross-site scripting (XSS) vulnerability in block_class.module in the Block Class module before 7.x-1.1 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via the class name.
CVE-2012-1658
Cross-site scripting (XSS) vulnerability in the Read More Link module 6.x-3.x before 6.x-3.1 for Drupal allows remote authenticated users with the access administration pages permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1659
Cross-site scripting (XSS) vulnerability in the Node Recommendation module 6.x-1.x before 6.x-1.1 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1660
Multiple cross-site scripting (XSS) vulnerabilities in components/select.inc in the Webform module 6.x-3.x before 6.x-3.17 and 7.x-3.x before 7.x-3.17 for Drupal, when the "Select (or other)" module is enabled, allow remote authenticated users with the create webform content permission to inject arbitrary web script or HTML via vectors related to (1) checkboxes or (2) radios.
CVE-2012-1901
Multiple cross-site request forgery (CSRF) vulnerabilities in FlexCMS 3.2.1 and earlier allow remote attackers to (1) hijack the authentication of users for requests that change account settings via a request to index.php/profile-edit-save or (2) hijack the authentication of administrators for requests that add a new page via a request to admin/pages-new-save.
CVE-2012-2993
Microsoft Windows Phone 7 does not verify the domain name in the subject's Common Name (CN) field of an X.509 certificate, which allows man-in-the-middle attackers to spoof an SSL server for the (1) POP3, (2) IMAP, or (3) SMTP protocol via an arbitrary valid certificate.
Per http://www.kb.cert.org/vuls/id/389795 "A remote attacker with the ability to pose as a man-in-the-middle may be able to view the login or session data in the corresponding protocol (e.g., SMTP, POP3, etc.)."
CVE-2012-2994
The CoSoSys Endpoint Protector 4 appliance establishes an EPProot password based entirely on the appliance serial number, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2012-3028
Cross-site request forgery (CSRF) vulnerability in WebNavigator in Siemens WinCC 7.0 SP3 and earlier, as used in SIMATIC PCS7 and other products, allows remote attackers to hijack the authentication of arbitrary users for requests that modify data or cause a denial of service.
CVE-2012-3030
WebNavigator in Siemens WinCC 7.0 SP3 and earlier, as used in SIMATIC PCS7 and other products, stores sensitive information under the web root with insufficient access control, which allows remote attackers to read a (1) log file or (2) configuration file via a direct request.
CVE-2012-3031
Multiple cross-site scripting (XSS) vulnerabilities in WebNavigator in Siemens WinCC 7.0 SP3 and earlier, as used in SIMATIC PCS7 and other products, allow remote attackers to inject arbitrary web script or HTML via a (1) GET parameter, (2) POST parameter, or (3) Referer HTTP header.
CVE-2012-3032
SQL injection vulnerability in WebNavigator in Siemens WinCC 7.0 SP3 and earlier, as used in SIMATIC PCS7 and other products, allows remote attackers to execute arbitrary SQL commands via a crafted SOAP message.
CVE-2012-3034
WebNavigator in Siemens WinCC 7.0 SP3 and earlier, as used in SIMATIC PCS7 and other products, allows remote attackers to discover a username and password via crafted parameters to unspecified methods in ActiveX controls.
CVE-2012-3524
libdbus 1.5.x and earlier, when used in setuid or other privileged programs in X.org and possibly other products, allows local users to gain privileges and execute arbitrary code via the DBUS_SYSTEM_BUS_ADDRESS environment variable.  NOTE: libdbus maintainers state that this is a vulnerability in the applications that do not cleanse environment variables, not in libdbus itself: "we do not support use of libdbus in setuid binaries that do not sanitize their environment before their first call into libdbus."
CVE-2012-3547
Stack-based buffer overflow in the cbtls_verify function in FreeRADIUS 2.1.10 through 2.1.12, when using TLS-based EAP methods, allows remote attackers to cause a denial of service (server crash) and possibly execute arbitrary code via a long "not after" timestamp in a client certificate.
CVE-2012-4405
Multiple integer underflows in the icmLut_allocate function in International Color Consortium (ICC) Format library (icclib), as used in Ghostscript 9.06 and Argyll Color Management System, allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted (1) PostScript or (2) PDF file with embedded images, which triggers a heap-based buffer overflow.  NOTE: this issue is also described as an array index error.
CVE-2012-4413
OpenStack Keystone 2012.1.3 does not invalidate existing tokens when granting or revoking roles, which allows remote authenticated users to retain the privileges of the revoked roles.
CVE-2012-4425
libgio, when used in setuid or other privileged programs in spice-gtk and possibly other products, allows local users to gain privileges and execute arbitrary code via the DBUS_SYSTEM_BUS_ADDRESS environment variable.  NOTE: it could be argued that this is a vulnerability in the applications that do not cleanse environment variables, not in libgio itself.
CVE-2012-4969
Use-after-free vulnerability in the CMshtmlEd::Exec function in mshtml.dll in Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code via a crafted web site, as exploited in the wild in September 2012.
