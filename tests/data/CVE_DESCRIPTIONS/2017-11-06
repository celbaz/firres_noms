CVE-2015-7529
sosreport in SoS 3.x allows local users to obtain sensitive information from sosreport files or gain privileges via a symlink attack on an archive file in a temporary directory, as demonstrated by sosreport-$hostname-$date.tar in /tmp/sosreport-$hostname-$date.
CVE-2015-7878
Cross-site scripting (XSS) vulnerability in the Taxonomy Find module 6.x-2.x through 6.x-1.2 and 7.x-2.x through 7.x-1.0 in Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via taxonomy vocabulary and term names.
CVE-2017-11177
TRITON AP-EMAIL 8.2 before 8.2 IB does not properly restrict file access in an unspecified directory.
CVE-2017-12719
An Untrusted Pointer Dereference issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. A remote attacker is able to execute code to dereference a pointer within the program causing the application to become unavailable.
CVE-2017-13680
Prior to SEP 12.1 RU6 MP9 & SEP 14 RU1 Symantec Endpoint Protection Windows endpoint can encounter a situation whereby an attacker could use the product's UI to perform unauthorized file deletes on the resident file system.
CVE-2017-13681
Symantec Endpoint Protection prior to SEP 12.1 RU6 MP9 could be susceptible to a privilege escalation vulnerability, which is a type of issue that allows a user to gain elevated access to resources that are normally protected at lower access levels. In the circumstances of this issue, the capability of exploit is limited by the need to perform multiple file and directory writes to the local filesystem and as such, is not feasible in a standard drive-by type attack.
CVE-2017-14016
A Stack-based Buffer Overflow issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. The application lacks proper validation of the length of user-supplied data prior to copying it to a stack-based buffer, which could allow an attacker to execute arbitrary code under the context of the process.
CVE-2017-14023
An Improper Input Validation issue was discovered in Siemens SIMATIC PCS 7 V8.1 prior to V8.1 SP1 with WinCC V7.3 Upd 13, and V8.2 all versions. The improper input validation vulnerability has been identified, which may allow an authenticated remote attacker who is a member of the administrators group to crash services by sending specially crafted messages to the DCOM interface.
CVE-2017-14025
An Improper Input Validation issue was discovered in ABB FOX515T release 1.0. An improper input validation vulnerability has been identified, allowing a local attacker to provide a malicious parameter to the script that is not validated by the application, This could enable the attacker to retrieve any file on the server.
CVE-2017-14029
An Uncontrolled Search Path Element issue was discovered in Trihedral VTScada 11.3.03 and prior. The program will execute specially crafted malicious dll files placed on the target machine.
CVE-2017-14031
An Improper Access Control issue was discovered in Trihedral VTScada 11.3.03 and prior. A local, non-administrator user has privileges to read and write to the file system of the target machine.
CVE-2017-15039
Cross-site scripting (XSS) exists in Zurmo 3.2.1.57987acc3018 via a data: URL in the redirectUrl parameter to app/index.php/meetings/default/createMeeting.
CVE-2017-15306
The kvm_vm_ioctl_check_extension function in arch/powerpc/kvm/powerpc.c in the Linux kernel before 4.13.11 allows local users to cause a denial of service (NULL pointer dereference and system crash) via a KVM_CHECK_EXTENSION KVM_CAP_PPC_HTM ioctl call to /dev/kvm.
CVE-2017-15672
The read_header function in libavcodec/ffv1dec.c in FFmpeg 3.3.4 and earlier allows remote attackers to have unspecified impact via a crafted MP4 file, which triggers an out-of-bounds read.
CVE-2017-16001
In HashiCorp Vagrant VMware Fusion plugin (aka vagrant-vmware-fusion) 5.0.1, a local attacker or malware can silently subvert the plugin update process in order to escalate to root privileges.
CVE-2017-16524
Web Viewer 1.0.0.193 on Samsung SRN-1670D devices suffers from an Unrestricted file upload vulnerability: 'network_ssl_upload.php' allows remote authenticated attackers to upload and execute arbitrary PHP code via a filename with a .php extension, which is then accessed via a direct request to the file in the upload/ directory. To authenticate for this attack, one can obtain web-interface credentials in cleartext by leveraging the existing Local File Read Vulnerability referenced as CVE-2015-8279, which allows remote attackers to read the web-interface credentials via a request for the cslog_export.php?path=/root/php_modules/lighttpd/sbin/userpw URI.
CVE-2017-16547
The DrawImage function in magick/render.c in GraphicsMagick 1.3.26 does not properly look for pop keywords that are associated with push keywords, which allows remote attackers to cause a denial of service (negative strncpy and application crash) or possibly have unspecified other impact via a crafted file.
CVE-2017-16548
The receive_xattr function in xattrs.c in rsync 3.1.2 and 3.1.3-development does not check for a trailing '\0' character in an xattr name, which allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) or possibly have unspecified other impact by sending crafted data to the daemon.
CVE-2017-16563
Cross-Site Request Forgery (CSRF) in the Basic Settings screen on Vonage (Grandstream) HT802 devices allows attackers to modify settings, related to cgi-bin/update.
CVE-2017-16564
Stored Cross-site scripting (XSS) vulnerability in /cgi-bin/config2 on Vonage (Grandstream) HT802 devices allows remote authenticated users to inject arbitrary web script or HTML via the DHCP vendor class ID field (P148).
CVE-2017-16565
Cross-Site Request Forgery (CSRF) in /cgi-bin/login on Vonage (Grandstream) HT802 devices allows attackers to authenticate a user via the login screen using the default password of 123 and submit arbitrary requests.
CVE-2017-16569
An Open URL Redirect issue exists in Zurmo 3.2.1.57987acc3018 via an http: URL in the redirectUrl parameter to app/index.php/meetings/default/createMeeting.
CVE-2017-16570
KeystoneJS before 4.0.0-beta.7 allows application-wide CSRF bypass by removing the CSRF parameter and value, aka SecureLayer7 issue number SL7_KEYJS_03. In other words, it fails to reject requests that lack an x-csrf-token header.
CVE-2017-16635
In TinyWebGallery v2.4, an XSS vulnerability is located in the `mkname`, `mkitem`, and `item` parameters of the `Add/Create` module. Remote attackers with low-privilege user accounts for backend access are able to inject malicious script codes into the `TWG Explorer` item listing. The request method to inject is POST and the attack vector is located on the application-side of the service. The injection point is the add/create input field and the execution point occurs in the item listing after the add or create.
CVE-2017-16636
In Bludit v1.5.2 and v2.0.1, an XSS vulnerability is located in the new page, new category, and edit post function body message context. Remote attackers are able to bypass the basic editor validation to trigger cross site scripting. The XSS is persistent and the request method to inject via editor is GET. To save the editor context, the followup POST method request must be processed to perform the attack via the application side. The basic validation of the editor does not allow injecting script codes and blocks the context. Attackers can inject the code by using an editor tag that is not recognized by the basic validation. Thus allows a restricted user account to inject malicious script code to perform a persistent attack against higher privilege web-application user accounts.
CVE-2017-16637
In Vectura Perfect Privacy VPN Manager v1.10.10 and v1.10.11, when resetting the network data via the software client, with a running VPN connection, a critical error occurs which leads to a "FrmAdvancedProtection" crash. Although the mechanism malfunctions and an error occurs during the runtime with the stack trace being issued, the software process is not properly terminated. The software client is still attempting to maintain the connection even though the network connection information is being reset live. In that insecure mode, the "FrmAdvancedProtection" component crashes, but the process continues to run with different errors and process corruptions. This local corruption vulnerability can be exploited by local attackers.
CVE-2017-16638
The Gentoo net-misc/vde package before version 2.3.2-r4 may allow members of the "qemu" group to gain root privileges by creating a hard link in a directory on which "chown" is called recursively by the OpenRC service script.
CVE-2017-6331
Prior to SEP 14 RU1 Symantec Endpoint Protection product can encounter an issue of Tamper-Protection Bypass, which is a type of attack that bypasses the real time protection for the application that is run on servers and clients.
CVE-2017-7425
Multiple potential reflected XSS issues exist in NetIQ iManager versions before 2.7.7 Patch 10 HF2 and 3.0.3.2.
