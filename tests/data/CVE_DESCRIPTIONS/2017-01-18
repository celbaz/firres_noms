CVE-2014-9909
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-31676542. References: B-RB#26684.
CVE-2014-9910
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-31746399. References: B-RB#26710.
CVE-2014-9913
Buffer overflow in the list_files function in list.c in Info-Zip UnZip 6.0 allows remote attackers to cause a denial of service (crash) via vectors related to the compression method.
CVE-2015-8667
Cross-site scripting (XSS) vulnerability in Reset Your Password module in Exponent CMS before 2.3.5 allows remote attackers to inject arbitrary web script or HTML via the Username/Email.
CVE-2015-8684
Exponent CMS before 2.3.7 does not properly restrict the types of files that can be uploaded, which allows remote attackers to conduct cross-site scripting (XSS) attacks and possibly have other unspecified impact as demonstrated by uploading a file with an .html extension, then accessing it via the elFinder functionality.
CVE-2016-10086
RESTful web services in CA Service Desk Manager 12.9 and CA Service Desk Management 14.1 might allow remote authenticated users to read or modify task information by leveraging incorrect permissions applied to a RESTful request.
CVE-2016-10147
crypto/mcryptd.c in the Linux kernel before 4.8.15 allows local users to cause a denial of service (NULL pointer dereference and system crash) by using an AF_ALG socket with an incompatible algorithm, as demonstrated by mcryptd(md5).
CVE-2016-10148
The wp_ajax_update_plugin function in wp-admin/includes/ajax-actions.php in WordPress before 4.6 makes a get_plugin_data call before checking the update_plugins capability, which allows remote authenticated users to bypass intended read-access restrictions via the plugin parameter to wp-admin/admin-ajax.php, a related issue to CVE-2016-6896.
CVE-2016-2087
Directory traversal vulnerability in the client in HexChat 2.11.0 allows remote IRC servers to read or modify arbitrary files via a .. (dot dot) in the server name.
CVE-2016-2233
Stack-based buffer overflow in the inbound_cap_ls function in common/inbound.c in HexChat 2.10.2 allows remote IRC servers to cause a denial of service (crash) via a large number of options in a CAP LS message.
CVE-2016-3401
Unspecified vulnerability in Zimbra Collaboration before 8.7.0 allows remote authenticated users to affect integrity via unknown vectors, aka bug 99810.
CVE-2016-3402
Unspecified vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to affect confidentiality via unknown vectors, aka bug 99167.
CVE-2016-3404
Unspecified vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to affect integrity via unknown vectors, aka bug 103959.
CVE-2016-3405
Multiple unspecified vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to affect integrity via unknown vectors, aka bugs 103961 and 104828.
CVE-2016-3406
Multiple cross-site request forgery (CSRF) vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to hijack the authentication of unspecified victims via vectors involving (1) the Client uploader extension or (2) extension REST handlers, aka bugs 104294 and 104456.
CVE-2016-3407
Multiple cross-site scripting (XSS) vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bugs 104222, 104910, 105071, and 105175.
CVE-2016-3408
Cross-site scripting (XSS) vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bug 101813.
CVE-2016-3409
Cross-site scripting (XSS) vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bug 102637.
CVE-2016-3410
Multiple cross-site scripting (XSS) vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bugs 103956, 103995, 104475, 104838, and 104839.
CVE-2016-3411
Cross-site scripting (XSS) vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bug 103609.
CVE-2016-3412
Multiple cross-site scripting (XSS) vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bugs 103997, 104413, 104414, 104777, and 104791.
CVE-2016-3413
Unspecified vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to affect integrity via unknown vectors, aka bug 103996.
CVE-2016-3414
Unspecified vulnerability in Zimbra Collaboration before 8.6.0 Patch 7 allows remote authenticated users to affect availability via unknown vectors, aka bug 102029.
CVE-2016-3415
Zimbra Collaboration before 8.7.0 allows remote attackers to conduct deserialization attacks via unspecified vectors, aka bug 102276.
CVE-2016-3999
Multiple cross-site scripting (XSS) vulnerabilities in Zimbra Collaboration before 8.7.0 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka bugs 104552 and 104703.
CVE-2016-4019
Unspecified vulnerability in Zimbra Collaboration before 8.7.0 allows remote attackers to affect integrity via unknown vectors, aka bug 104477.
CVE-2016-6271
The Bzrtp library (aka libbzrtp) 1.0.x before 1.0.4 allows man-in-the-middle attackers to conduct spoofing attacks by leveraging a missing HVI check on DHPart2 packet reception.
CVE-2016-6283
Cross-site scripting (XSS) vulnerability in Atlassian Confluence before 5.10.6 allows remote attackers to inject arbitrary web script or HTML via the newFileName parameter to pages/doeditattachment.action.
CVE-2016-6497
main/java/org/apache/directory/groovyldap/LDAP.java in the Groovy LDAP API in Apache allows attackers to conduct LDAP entry poisoning attacks by leveraging setting returnObjFlag to true for all search methods.
CVE-2016-6526
The SpamCall Activity component in Telecom application on Samsung Note device L(5.0/5.1) and M(6.0) allows attackers to cause a denial of service (crash and reboot) or possibly gain privileges via a malformed serializable object.
CVE-2016-6527
The SmartCall Activity component in Telecom application on Samsung Note device L(5.0/5.1) and M(6.0) allows attackers to cause a denial of service (crash and reboot) or possibly gain privileges via a malformed serializable object.
CVE-2016-6823
Integer overflow in the BMP coder in ImageMagick before 7.0.2-10 allows remote attackers to cause a denial of service (crash) via crafted height and width values, which triggers an out-of-bounds write.
CVE-2016-6896
Directory traversal vulnerability in the wp_ajax_update_plugin function in wp-admin/includes/ajax-actions.php in WordPress 4.5.3 allows remote authenticated users to cause a denial of service or read certain text files via a .. (dot dot) in the plugin parameter to wp-admin/admin-ajax.php, as demonstrated by /dev/random read operations that deplete the entropy pool.
CVE-2016-6897
Cross-site request forgery (CSRF) vulnerability in the wp_ajax_update_plugin function in wp-admin/includes/ajax-actions.php in WordPress before 4.6 allows remote attackers to hijack the authentication of subscribers for /dev/random read operations by leveraging a late call to the check_ajax_referer function, a related issue to CVE-2016-6896.
CVE-2016-7101
The SGI coder in ImageMagick before 7.0.2-10 allows remote attackers to cause a denial of service (out-of-bounds read) via a large row value in an sgi file.
CVE-2016-7144
The m_authenticate function in modules/m_sasl.c in UnrealIRCd before 3.2.10.7 and 4.x before 4.0.6 allows remote attackers to spoof certificate fingerprints and consequently log in as another user via a crafted AUTHENTICATE parameter.
CVE-2016-7149
Cross-site scripting (XSS) vulnerability in b2evolution 6.7.5 and earlier allows remote attackers to inject arbitrary web script or HTML via vectors related to the autolink function.
CVE-2016-7150
Cross-site scripting (XSS) vulnerability in b2evolution 6.7.5 and earlier allows remote authenticated users to inject arbitrary web script or HTML via the site name.
CVE-2016-7563
The chartorune function in Artifex Software MuJS allows attackers to cause a denial of service (out-of-bounds read) via a * (asterisk) at the end of the input.
CVE-2016-7564
Heap-based buffer overflow in the Fp_toString function in jsfunction.c in Artifex Software MuJS allows attackers to cause a denial of service (crash) via crafted input.
CVE-2016-7799
MagickCore/profile.c in ImageMagick before 7.0.3-2 allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted file.
CVE-2016-7906
magick/attribute.c in ImageMagick 7.0.3-2 allows remote attackers to cause a denial of service (use-after-free) via a crafted file.
CVE-2016-7980
Cross-site request forgery (CSRF) vulnerability in ecrire/exec/valider_xml.php in SPIP 3.1.2 and earlier allows remote attackers to hijack the authentication of administrators for requests that execute the XML validator on a local file via a crafted valider_xml request.  NOTE: this issue can be combined with CVE-2016-7998 to execute arbitrary PHP code.
CVE-2016-7981
Cross-site scripting (XSS) vulnerability in valider_xml.php in SPIP 3.1.2 and earlier allows remote attackers to inject arbitrary web script or HTML via the var_url parameter in a valider_xml action.
CVE-2016-7982
Directory traversal vulnerability in ecrire/exec/valider_xml.php in SPIP 3.1.2 and earlier allows remote attackers to enumerate the files on the system via the var_url parameter in a valider_xml action.
CVE-2016-7996
Heap-based buffer overflow in the WPG format reader in GraphicsMagick 1.3.25 and earlier allows remote attackers to have unspecified impact via a colormap with a large number of entries.
CVE-2016-7997
The WPG format reader in GraphicsMagick 1.3.25 and earlier allows remote attackers to cause a denial of service (assertion failure and crash) via vectors related to a ReferenceBlob and a NULL pointer.
CVE-2016-7998
The SPIP template composer/compiler in SPIP 3.1.2 and earlier allows remote authenticated users to execute arbitrary PHP code by uploading an HTML file with a crafted (1) INCLUDE or (2) INCLURE tag and then accessing it with a valider_xml action.
CVE-2016-7999
ecrire/exec/valider_xml.php in SPIP 3.1.2 and earlier allows remote attackers to conduct server side request forgery (SSRF) attacks via a URL in the var_url parameter in a valider_xml action.
CVE-2016-9109
Artifex Software MuJS allows attackers to cause a denial of service (crash) via vectors related to incomplete escape sequences.  NOTE: this vulnerability exists due to an incomplete fix for CVE-2016-7563.
CVE-2016-9273
tiffsplit in libtiff 4.0.6 allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted file, related to changing td_nstrips in TIFF_STRIPCHOP mode.
CVE-2016-9278
The Samsung Exynos fimg2d driver for Android with Exynos 5433, 54xx, or 7420 chipsets allows local users to cause a denial of service (kernel panic) via a crafted ioctl command. The Samsung ID is SVE-2016-6736.
CVE-2016-9279
Use-after-free vulnerability in the Samsung Exynos fimg2d driver for Android with Exynos 5433, 54xx, or 7420 chipsets allows attackers to obtain sensitive information via unspecified vectors. The Samsung ID is SVE-2016-6853.
CVE-2016-9297
The TIFFFetchNormalTag function in LibTiff 4.0.6 allows remote attackers to cause a denial of service (out-of-bounds read) via crafted TIFF_SETGET_C16ASCII or TIFF_SETGET_C32_ASCII tag values.
CVE-2016-9584
libical allows remote attackers to cause a denial of service (use-after-free) and possibly read heap memory via a crafted ics file.
CVE-2016-9676
Buffer overflow in Citrix Provisioning Services before 7.12 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2016-9677
Citrix Provisioning Services before 7.12 allows attackers to obtain sensitive kernel address information via unspecified vectors.
CVE-2016-9678
Use-after-free vulnerability in Citrix Provisioning Services before 7.12 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2016-9679
Citrix Provisioning Services before 7.12 allows attackers to execute arbitrary code by overwriting a function pointer.
CVE-2016-9680
Citrix Provisioning Services before 7.12 allows attackers to obtain sensitive information from kernel memory via unspecified vectors.
CVE-2016-9844
Buffer overflow in the zi_short function in zipinfo.c in Info-Zip UnZip 6.0 allows remote attackers to cause a denial of service (crash) via a large compression method value in the central directory file header.
