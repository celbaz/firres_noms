CVE-2012-1664
Multiple cross-site scripting (XSS) vulnerabilities in the admin panel in osCMax before 2.5.1 allow remote attackers to inject arbitrary web script or HTML via the (1) username parameter in a process action to admin/login.php; (2) pageTitle, (3) current_product_id, or (4) cPath parameter to admin/new_attributes_include.php; (5) sb_id, (6) sb_key, (7) gc_id, (8) gc_key, or (9) path parameter to admin/htaccess.php; (10) title parameter to admin/information_form.php; (11) search parameter to admin/xsell.php; (12) gross or (13) max parameter to admin/stats_products_purchased.php; (14) status parameter to admin/stats_monthly_sales.php; (15) sorted parameter to admin/stats_customers.php; (16) information_id parameter to /admin/information_manager.php; or (17) zID parameter to /admin/geo_zones.php.
CVE-2012-1665
Multiple SQL injection vulnerabilities in the admin panel in osCMax before 2.5.1 allow (1) remote attackers to execute arbitrary SQL commands via the username parameter in a process action to admin/login.php or (2) remote administrators to execute arbitrary SQL commands via the status parameter to admin/stats_monthly_sales.php or (3) country parameter in a process action to admin/create_account_process.php.
CVE-2012-3243
Cross-site scripting (XSS) vulnerability in the SEOgento plugin for Magento allows remote attackers to inject arbitrary web script or HTML via the id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2012-4901
Cross-site scripting (XSS) vulnerability in Template CMS 2.1.1 and earlier allows remote attackers to inject arbitrary web script or HTML via the themes_editor parameter in an add_template action to admin/index.php.
CVE-2012-4902
Multiple cross-site request forgery (CSRF) vulnerabilities in Template CMS 2.1.1 and earlier allow remote attackers to hijack the authentication of administrators for requests that (1) create an administrator user via an add action to admin/index.php or (2) conduct static PHP code injection attacks via the themes_editor parameter in an edit_template action to admin/index.php.
CVE-2012-6691
Multiple cross-site request forgery (CSRF) vulnerabilities in the admin panel in osCMax before 2.5.1 allow remote attackers to hijack the authentication of administrators for requests that conduct SQL injection attacks via the (1) status parameter to admin/stats_monthly_sales.php or (2) country parameter in a process action to admin/create_account_process.php.
CVE-2014-4776
IBM License Metric Tool 9 before 9.1.0.2 does not have an off autocomplete attribute for authentication fields, which makes it easier for remote attackers to obtain access by leveraging an unattended workstation.
CVE-2014-6211
The command-line scripts in IBM WebSphere Commerce 6.0 through 6.0.0.11, 7.0 through 7.0.0.9, and 7.0 Feature Pack 2 through 8, when debugging is configured, do not properly restrict the logging of personal data, which allows local users to obtain sensitive information by reading a log file.
CVE-2014-8924
The server in IBM License Metric Tool 7.2.2 before IF15 and 7.5 before IF24 and Tivoli Asset Discovery for Distributed 7.2.2 before IF15 and 7.5 before IF24 allows remote attackers to read arbitrary files or send TCP requests to intranet servers via XML data containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-0189
The cluster repository manager in IBM WebSphere MQ 7.5 before 7.5.0.5 and 8.0 before 8.0.0.2 allows remote authenticated administrators to cause a denial of service (memory overwrite and daemon outage) by triggering multiple transmit-queue records.
CVE-2015-0740
Cross-site request forgery (CSRF) vulnerability in Cisco Unified Intelligence Center 10.6(1) allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCus28826.
CVE-2015-1188
The certificate verification functions in the HNDS service in Swisscom Centro Grande (ADB) DSL routers with firmware before 6.14.00 allows remote attackers to access the management functions via unknown vectors.
CVE-2015-1251
Use-after-free vulnerability in the SpeechRecognitionClient implementation in the Speech subsystem in Google Chrome before 43.0.2357.65 allows remote attackers to execute arbitrary code via a crafted document.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1252
common/partial_circular_buffer.cc in Google Chrome before 43.0.2357.65 does not properly handle wraps, which allows remote attackers to bypass a sandbox protection mechanism or cause a denial of service (out-of-bounds write) via vectors that trigger a write operation with a large amount of data, related to the PartialCircularBuffer::Write and PartialCircularBuffer::DoWrite functions.
CVE-2015-1253
core/html/parser/HTMLConstructionSite.cpp in the DOM implementation in Blink, as used in Google Chrome before 43.0.2357.65, allows remote attackers to bypass the Same Origin Policy via crafted JavaScript code that appends a child to a SCRIPT element, related to the insert and executeReparentTask functions.
CVE-2015-1254
core/dom/Document.cpp in Blink, as used in Google Chrome before 43.0.2357.65, enables the inheritance of the designMode attribute, which allows remote attackers to bypass the Same Origin Policy by leveraging the availability of editing.
CVE-2015-1255
Use-after-free vulnerability in content/renderer/media/webaudio_capturer_source.cc in the WebAudio implementation in Google Chrome before 43.0.2357.65 allows remote attackers to cause a denial of service (heap memory corruption) or possibly have unspecified other impact by leveraging improper handling of a stop action for an audio track.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1256
Use-after-free vulnerability in the SVG implementation in Blink, as used in Google Chrome before 43.0.2357.65, allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted document that leverages improper handling of a shadow tree for a use element.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1257
platform/graphics/filters/FEColorMatrix.cpp in the SVG implementation in Blink, as used in Google Chrome before 43.0.2357.65, does not properly handle an insufficient number of values in an feColorMatrix filter, which allows remote attackers to cause a denial of service (container overflow) or possibly have unspecified other impact via a crafted document.
CVE-2015-1258
Google Chrome before 43.0.2357.65 relies on libvpx code that was not built with an appropriate --size-limit value, which allows remote attackers to trigger a negative value for a size field, and consequently cause a denial of service or possibly have unspecified other impact, via a crafted frame size in VP9 video data.
CVE-2015-1259
PDFium, as used in Google Chrome before 43.0.2357.65, does not properly initialize memory, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2015-1260
Multiple use-after-free vulnerabilities in content/renderer/media/user_media_client_impl.cc in the WebRTC implementation in Google Chrome before 43.0.2357.65 allow remote attackers to cause a denial of service or possibly have unspecified other impact via crafted JavaScript code that executes upon completion of a getUserMedia request.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1261
android/java/src/org/chromium/chrome/browser/WebsiteSettingsPopup.java in Google Chrome before 43.0.2357.65 on Android does not properly restrict use of a URL's fragment identifier during construction of a page-info popup, which allows remote attackers to spoof the URL bar or deliver misleading popup content via crafted text.
CVE-2015-1262
platform/fonts/shaping/HarfBuzzShaper.cpp in Blink, as used in Google Chrome before 43.0.2357.65, does not initialize a certain width field, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via crafted Unicode text.
CVE-2015-1263
The Spellcheck API implementation in Google Chrome before 43.0.2357.65 does not use an HTTPS session for downloading a Hunspell dictionary, which allows man-in-the-middle attackers to deliver incorrect spelling suggestions or possibly have unspecified other impact via a crafted file.
CVE-2015-1264
Cross-site scripting (XSS) vulnerability in Google Chrome before 43.0.2357.65 allows user-assisted remote attackers to inject arbitrary web script or HTML via crafted data that is improperly handled by the Bookmarks feature.
CVE-2015-1265
Multiple unspecified vulnerabilities in Google Chrome before 43.0.2357.65 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2015-1902
Stack-based buffer overflow in IBM Domino 8.5 before 8.5.3 FP6 IF7 and 9.0 before 9.0.1 FP3 IF3 allows remote attackers to execute arbitrary code via a crafted BMP image, aka SPR KLYH9TSMLA.
CVE-2015-1903
Stack-based buffer overflow in IBM Domino 8.5 before 8.5.3 FP6 IF7 and 9.0 before 9.0.1 FP3 IF3 allows remote attackers to execute arbitrary code via a crafted BMP image, aka SPR KLYH9TSN3Y.
CVE-2015-1920
IBM WebSphere Application Server (WAS) 6.1 through 6.1.0.47, 7.0 before 7.0.0.39, 8.0 before 8.0.0.11, and 8.5 before 8.5.5.6 allows remote attackers to execute arbitrary code by sending crafted instructions in a management-port session.
CVE-2015-3141
Multiple cross-site request forgery (CSRF) vulnerabilities in Synametrics Technologies Xeams 4.5 Build 5755 and earlier allow remote attackers to hijack the authentication of administrators for requests that create an (1) SMTP domain or a (2) user via a request to /FrontController; or conduct cross-site scripting (XSS) attacks via the (3) domainname parameter to /FrontController, when creating a new SMTP domain configuration; the (4) txtRecipient parameter to /FrontController, when creating a new forwarder; the (5) popFetchServer, (6) popFetchUser, or (7) popFetchRecipient parameter to /FrontController, when creating a new POP3 Fetcher account; or the (8) Smtp HELO domain in the Advanced Server Configuration.
CVE-2015-3910
Multiple unspecified vulnerabilities in Google V8 before 4.3.61.21, as used in Google Chrome before 43.0.2357.65, allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2015-3990
The GMS ViewPoint (GMSVP) web application in Dell Sonicwall GMS, Analyzer, and UMA EM5000 before 7.2 SP4 allows remote authenticated users to execute arbitrary commands via vectors related to configuration.
CVE-2015-3999
Piriform CCleaner 3.26.0.1988 through 5.02.5101 writes the filenames to disk when overwriting files, which allows local users to obtain sensitive information by searching unallocated disk space.
CVE-2015-4016
The client detection protocol in Valve Steam allows remote attackers to cause a denial of service (process crash) via a crafted response to a broadcast packet.
