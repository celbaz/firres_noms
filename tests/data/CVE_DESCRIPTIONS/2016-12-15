CVE-2015-3271
Apache Tika server (aka tika-server) in Apache Tika 1.9 might allow remote attackers to read arbitrary files via the HTTP fileUrl header.
CVE-2015-6574
The SNAP Lite component in certain SISCO MMS-EASE and AX-S4 ICCP products allows remote attackers to cause a denial of service (CPU consumption) via a crafted packet.
CVE-2015-8542
An issue was discovered in Open-Xchange Guard before 2.2.0-rev8. The "getprivkeybyid" API call is used to download a PGP Private Key for a specific user after providing authentication credentials. Clients provide the "id" and "cid" parameter to specify the current user by its user- and context-ID. The "auth" parameter contains a hashed password string which gets created by the client by asking the user to enter his or her OX Guard password. This parameter is used as single point of authentication when accessing PGP Private Keys. In case a user has set the same password as another user, it is possible to download another user's PGP Private Key by iterating the "id" and "cid" parameters. This kind of attack would also be able by brute-forcing login credentials, but since the "id" and "cid" parameters are sequential they are much easier to predict than a user's login name. At the same time, there are some obvious insecure standard passwords that are widely used. A attacker could send the hashed representation of typically weak passwords and randomly fetch Private Key of matching accounts. The attack can be executed by both internal users and "guests" which use the external mail reader.
CVE-2016-2840
An issue was discovered in Open-Xchange Server 6 / OX AppSuite before 7.8.0-rev26. The "session" parameter for file-download requests can be used to inject script code that gets reflected through the subsequent status page. Malicious script code can be executed within a trusted domain's context. While no OX App Suite specific data can be manipulated, the vulnerability can be exploited without being authenticated and therefore used for social engineering attacks, stealing cookies or redirecting from trustworthy to malicious hosts.
CVE-2016-3173
An issue was discovered in Open-Xchange OX AppSuite before 7.8.0-rev27. The aria-label parameter of tiles at the Portal can be used to inject script code. Those labels use the name of the file (e.g. an image) which gets displayed at the portal application. Using script code at the file name leads to script execution. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.). Users actively need to add a file to the portal to enable this attack. In case of shared files however, a internal attacker may modify a previously embedded file to carry a malicious file name. Furthermore this vulnerability can be used to persistently execute code that got injected by a temporary script execution vulnerability.
CVE-2016-3174
An issue was discovered in Open-Xchange OX AppSuite before 7.8.0-rev27. The "defer" servlet offers to redirect a client to a specified URL. Since some checks were missing, arbitrary URLs could be provided as redirection target. Users can be tricked to follow a link to a trustworthy domain but end up at an unexpected service later on. This vulnerability can be used to prepare and enhance phishing attacks.
CVE-2016-4026
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev11. The content sanitizer component has an issue with filtering malicious content in case invalid HTML code is provided. In such cases the filter will output a unsanitized representation of the content. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.). Attackers can use this issue for filter evasion to inject script code later on.
CVE-2016-4027
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev10. App Suite frontend offers to control whether a user wants to store cookies that exceed the session duration. This functionality is useful when logging in from clients with reduced privileges or shared environments. However the setting was incorrectly recognized and cookies were stored regardless of this setting when the login was performed using a non-interactive login method. In case the setting was enforced by middleware configuration or the user went through the interactive login page, the workflow was correct. Cookies with authentication information may become available to other users on shared environments. In case the user did not properly log out from the session, third parties with access to the same client can access a user's account.
CVE-2016-4028
An issue was discovered in Open-Xchange OX Guard before 2.4.0-rev8. OX Guard uses an authentication token to identify and transfer guest users' credentials.  The OX Guard API acts as a padding oracle by responding with different error codes depending on whether the provided token matches the encryption padding.  In combination with AES-CBC, this allows attackers to guess the correct padding.  Attackers may run brute-forcing attacks on the content of the guest authentication token and discover user credentials.  For a practical attack vector, the guest users needs to have logged in, the content of the guest user's "OxReaderID" cookie and the value of the "auth" parameter needs to be known to the attacker.
CVE-2016-4045
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev11. Script code can be embedded to RSS feeds using a URL notation. In case a user clicks the corresponding link at the RSS reader of App Suite, code gets executed at the context of the user. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.). The attacker needs to reside within the same context to make this attack work.
CVE-2016-4046
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev11. The API to configure external mail accounts can be abused to map and access network components within the trust boundary of the operator. Users can inject arbitrary hosts and ports to API calls. Depending on the response type, content and latency, information about existence of hosts and services can be gathered. Attackers can get internal configuration information about the infrastructure of an operator to prepare subsequent attacks.
CVE-2016-4047
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev8. References to external Open XML document type definitions (.dtd resources) can be placed within .docx and .xslx files. Those resources were requested when parsing certain parts of the generated document. As a result an attacker can track access to a manipulated document. Usage of a document may get tracked and information about internal infrastructure may get exposed.
CVE-2016-4048
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev11. Custom messages can be shown at the login screen to notify external users about issues with sharing links. This mechanism can be abused to inject arbitrary text messages. Users may get tricked to follow instructions injected by third parties as part of social engineering attacks.
<a href="http://cwe.mitre.org/data/definitions/451.html">CWE-451: User Interface (UI) Misrepresentation of Critical Information</a>
CVE-2016-5124
An issue was discovered in Open-Xchange OX App Suite before 7.8.1-rev14. Adding images from external sources to HTML editors by drag&drop can potentially lead to script code execution in the context of the active user. To exploit this, a user needs to be tricked to use an image from a specially crafted website and add it to HTML editor areas of OX App Suite, for example E-Mail Compose or OX Text. This specific attack circumvents typical XSS filters and detection mechanisms since the code is not loaded from an external service but injected locally. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.). To exploit this vulnerability, a attacker needs to convince a user to follow specific steps (social-engineering).
CVE-2016-5740
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev5. JavaScript code can be used as part of ical attachments within scheduling E-Mails. This content, for example an appointment's location, will be presented to the user at the E-Mail App, depending on the invitation workflow. This code gets executed within the context of the user's current session. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6842
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. Setting the user's name to JS code makes that code execute when selecting that user's "Templates" folder from OX Documents settings. This requires the folder to be shared to the victim. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6843
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. Script code can be injected to contact names. When adding those contacts to a group, the script code gets executed in the context of the user which creates or changes the group by using autocomplete. In most cases this is a user with elevated permissions. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6844
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. Script code within SVG files is maintained when opening such files "in browser" based on our Mail or Drive app. In case of "a" tags, this may include link targets with base64 encoded "data" references. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6845
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. Script code within hyperlinks at HTML E-Mails is not getting correctly sanitized when using base64 encoded "data" resources. This allows an attacker to provide hyperlinks that may execute script code instead of directing to a proper location. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6847
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. SVG files can be used as mp3 album covers. In case their XML structure contains script code, that code may get executed when calling the related cover URL. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6848
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. API requests can be used to inject, generate and download executable files to the client ("Reflected File Download"). Malicious platform specific (e.g. Microsoft Windows) batch file can be created via a trusted domain without authentication that, if executed by the user, may lead to local code execution.
CVE-2016-6850
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. SVG files can be used as profile pictures. In case their XML structure contains iframes and script code, that code may get executed when calling the related picture URL or viewing the related person's image within a browser. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6851
An issue was discovered in Open-Xchange OX Guard before 2.4.2-rev5. Script code can be provided as parameter to the OX Guard guest reader web application. This allows cross-site scripting attacks against arbitrary users since no prior authentication is needed. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.) in case the user has an active session on the same domain already.
CVE-2016-6852
An issue was discovered in Open-Xchange OX App Suite before 7.8.2-rev8. Users can provide local file paths to the RSS reader; the response and error code give hints about whether the provided file exists or not. Attackers may discover specific system files or library versions on the middleware server to prepare further attacks.
CVE-2016-6853
An issue was discovered in Open-Xchange OX Guard before 2.4.2-rev5. Script code and references to external websites can be injected to the names of PGP public keys. When requesting that key later on using a specific URL, such script code might get executed. In case of injecting external websites, users might get lured into a phishing scheme. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6854
An issue was discovered in Open-Xchange OX Guard before 2.4.2-rev5. Script code which got injected to a mail with inline PGP signature gets executed when verifying the signature. Malicious script code can be executed within a user's context. This can lead to session hijacking or triggering unwanted actions via the web interface (sending mail, deleting data etc.).
CVE-2016-6933
Adobe Experience Manager Forms versions 6.2 and earlier, LiveCycle 11.0.1, LiveCycle 10.0.4 have an input validation issue in the AACComponent that could be used in cross-site scripting attacks.
CVE-2016-6934
Adobe Experience Manager Forms versions 6.2 and earlier, LiveCycle 11.0.1, LiveCycle 10.0.4 have an input validation issue in the PMAdmin module that could be used in cross-site scripting attacks.
CVE-2016-7856
Adobe DNG Converter versions 9.7 and earlier have an exploitable memory corruption vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7866
Adobe Animate versions 15.2.1.95 and earlier have an exploitable memory corruption vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7867
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable buffer overflow / underflow vulnerability in the RegExp class related to bookmarking in searches. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7868
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable buffer overflow / underflow vulnerability in the RegExp class related to alternation functionality. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7869
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable buffer overflow / underflow vulnerability in the RegExp class related to backtrack search functionality. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7870
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable buffer overflow / underflow vulnerability in the RegExp class for specific search strategies. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7871
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable memory corruption vulnerability in the Worker class. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7872
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the MovieClip class related to objects at multiple presentation levels. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7873
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable memory corruption vulnerability in the PSDK class related to ad policy functionality method. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7874
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable memory corruption vulnerability in the NetConnection class when handling the proxy types. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7875
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable integer overflow vulnerability in the BitmapData class. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7876
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable memory corruption vulnerability in the Clipboard class related to data handling functionality. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7877
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the Action Message Format serialization (AFM0). Successful exploitation could lead to arbitrary code execution.
CVE-2016-7878
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the PSDK's MediaPlayer class. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7879
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the NetConnection class when handling an attached script object. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7880
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability when setting the length property of an array object. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7881
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the MovieClip class when handling conversion to an object. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7882
Adobe Experience Manager versions 6.2 and earlier have an input validation issue in the WCMDebug filter that could be used in cross-site scripting attacks.
CVE-2016-7883
Adobe Experience Manager version 6.2 has an input validation issue in create Launch wizard that could be used in cross-site scripting attacks.
CVE-2016-7884
Adobe Experience Manager versions 6.1 and earlier have an input validation issue in the DAM create assets that could be used in cross-site scripting attacks.
CVE-2016-7885
Adobe Experience Manager versions 6.2 and earlier have a vulnerability that could be used in Cross-Site Request Forgery attacks.
CVE-2016-7886
Adobe InDesign version 11.4.1 and earlier, Adobe InDesign Server 11.0.0 and earlier have an exploitable memory corruption vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7887
Adobe ColdFusion Builder versions 2016 update 2 and earlier, 3.0.3 and earlier have an important vulnerability that could lead to information disclosure.
CVE-2016-7888
Adobe Digital Editions versions 4.5.2 and earlier has an important vulnerability that could lead to memory address leak.
CVE-2016-7889
Adobe Digital Editions versions 4.5.2 and earlier has an issue with parsing crafted XML entries that could lead to information disclosure.
CVE-2016-7890
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have security bypass vulnerability in the implementation of the same origin policy.
CVE-2016-7891
Adobe RoboHelp version 2015.0.3 and earlier, RoboHelp 11 and earlier have an input validation issue that could be used in cross-site scripting attacks.
CVE-2016-7892
Adobe Flash Player versions 23.0.0.207 and earlier, 11.2.202.644 and earlier have an exploitable use after free vulnerability in the TextField class. Successful exploitation could lead to arbitrary code execution.
CVE-2016-8708
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-8823.  Reason: This candidate is a reservation duplicate of CVE-2016-8823.  Notes: All CVE users should reference CVE-2016-8823 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-9565
MagpieRSS, as used in the front-end component in Nagios Core before 4.2.2 might allow remote attackers to read or write to arbitrary files by spoofing a crafted response from the Nagios RSS feed server.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2008-4796.
CVE-2016-9566
base/logging.c in Nagios Core before 4.2.4 allows local users with access to an account in the nagios group to gain root privileges via a symlink attack on the log file.  NOTE: this can be leveraged by remote attackers using CVE-2016-9565.
