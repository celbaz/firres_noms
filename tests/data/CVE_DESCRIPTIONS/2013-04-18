CVE-2012-4695
LogReceiver.exe in Rockwell Automation RSLinx Enterprise CPR9, CPR9-SR1, CPR9-SR2, CPR9-SR3, CPR9-SR4, CPR9-SR5, CPR9-SR5.1, and CPR9-SR6 allows remote attackers to cause a denial of service (service outage) via a zero-byte UDP packet that is not properly handled by Logger.dll.
CVE-2012-4713
Integer signedness error in RNADiagnostics.dll in Rockwell Automation FactoryTalk Services Platform (FTSP) CPR9, CPR9-SR1, CPR9-SR2, CPR9-SR3, CPR9-SR4, CPR9-SR5, CPR9-SR5.1, and CPR9-SR6 allows remote attackers to cause a denial of service (service outage or RNADiagReceiver.exe daemon crash) via UDP data that specifies a negative integer value.
CVE-2012-4714
Integer overflow in RNADiagnostics.dll in Rockwell Automation FactoryTalk Services Platform (FTSP) CPR9, CPR9-SR1, CPR9-SR2, CPR9-SR3, CPR9-SR4, CPR9-SR5, CPR9-SR5.1, and CPR9-SR6 allows remote attackers to cause a denial of service (service outage or RNADiagReceiver.exe daemon crash) via UDP data that specifies a large integer value.
CVE-2012-4715
Buffer overflow in LogReceiver.exe in Rockwell Automation RSLinx Enterprise CPR9, CPR9-SR1, CPR9-SR2, CPR9-SR3, CPR9-SR4, CPR9-SR5, CPR9-SR5.1, and CPR9-SR6 allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via a UDP packet with a certain integer length value that is (1) too large or (2) too small, leading to improper handling by Logger.dll.
CVE-2013-0132
The suexec implementation in Parallels Plesk Panel 11.0.9 contains a cgi-wrapper whitelist entry, which allows user-assisted remote attackers to execute arbitrary PHP code via a request containing crafted environment variables.
CVE-2013-0133
Untrusted search path vulnerability in /usr/local/psa/admin/sbin/wrapper in Parallels Plesk Panel 11.0.9 allows local users to gain privileges via a crafted PATH environment variable.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2013-0139
The Arecont Vision AV1355DN MegaDome camera allows remote attackers to cause a denial of service (video-capture outage) via a packet to UDP port 69.
CVE-2013-0687
The installer routine in Schneider Electric MiCOM S1 Studio uses world-writable permissions for executable files, which allows local users to modify the service or the configuration files, and consequently gain privileges or trigger incorrect protective-relay operation, via a Trojan horse executable file.
CVE-2013-1176
The DSP card on Cisco TelePresence MCU 4500 and 4501 devices before 4.3(2.30), TelePresence MCU MSE 8510 devices before 4.3(2.30), and TelePresence Server before 2.3(1.55) does not properly validate H.264 data, which allows remote attackers to cause a denial of service (device reload) via crafted RTP packets in a (1) SIP session or (2) H.323 session, aka Bug IDs CSCuc11328 and CSCub05448.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20130417-tpi

'Vulnerable Products
The following Cisco TelePresence Infrastructure products are affected by this vulnerability:

    Cisco TelePresence MCU 4501 Series, MCU 4500 Series and Cisco TelePresence MCU MSE 8510 versions 4.3(2.18) and earlier
    Cisco TelePresence Server versions 2.2(1.54) and earlier'

CVE-2013-1177
SQL injection vulnerability in Cisco Network Admission Control (NAC) Manager before 4.8.3.1 and 4.9.x before 4.9.2 allows remote attackers to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCub23095.
CVE-2013-1194
The ISAKMP implementation on Cisco Adaptive Security Appliances (ASA) devices generates different responses for IKE aggressive-mode messages depending on whether invalid VPN groups are specified, which allows remote attackers to enumerate groups via a series of messages, aka Bug ID CSCue73708.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityNotice/CVE-2013-1194

'A vulnerability in the Internet Security Association and Key Management Protocol (ISAKMP) implementation in Cisco Adaptive Security Appliance (ASA) Software could allow an unauthenticated, remote attacker to enumerate remote access VPN groups configured in a Cisco ASA device.'
CVE-2013-1199
Race condition in the CIFS implementation in the rewriter module in the Clientless SSL VPN component on Cisco Adaptive Security Appliances (ASA) devices allows remote authenticated users to cause a denial of service (device reload) by accessing resources within multiple sessions, aka Bug ID CSCub58996.
CVE-2013-1748
Multiple SQL injection vulnerabilities in PHP Address Book 8.2.5 allow remote attackers to execute arbitrary SQL commands via unspecified parameters to (1) edit.php or (2) import.php.  NOTE: the view.php id vector is already covered by CVE-2008-2565.1 and the edit.php id vector is already covered by CVE-2008-2565.2.
CVE-2013-1749
Cross-site scripting (XSS) vulnerability in edit.php in PHP Address Book 8.2.5 allows user-assisted remote attackers to inject arbitrary web script or HTML via the Address field.
