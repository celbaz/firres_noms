CVE-2008-0971
Multiple cross-site scripting (XSS) vulnerabilities in index.cgi in Barracuda Spam Firewall (BSF) before 3.5.12.007, Message Archiver before 1.2.1.002, Web Filter before 3.3.0.052, IM Firewall before 3.1.01.017, and Load Balancer before 2.3.024 allow remote attackers to inject arbitrary web script or HTML via (1) the Policy Name field in Search Based Retention Policy in Message Archiver; unspecified parameters in the (2) IP Configuration, (3) Administration, (4) Journal Accounts, (5) Retention Policy, and (6) GroupWise Sync components in Message Archiver; (7) input to search operations in Web Filter; and (8) input used in error messages and (9) hidden INPUT elements in (a) Spam Firewall, (b) IM Firewall, and (c) Web Filter.
CVE-2008-1094
SQL injection vulnerability in index.cgi in the Account View page in Barracuda Spam Firewall (BSF) before 3.5.12.007 allows remote authenticated administrators to execute arbitrary SQL commands via a pattern_x parameter in a search_count_equals action, as demonstrated by the pattern_0 parameter.
CVE-2008-4122
Joomla! 1.5.8 does not set the secure flag for the session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2008-5078
Multiple buffer overflows in the (1) recognize_eps_file function (src/psgen.c) and (2) tilde_subst function (src/util.c) in GNU enscript 1.6.1, and possibly earlier, might allow remote attackers to execute arbitrary code via an epsf escape sequence with a long filename.
CVE-2008-5086
Multiple methods in libvirt 0.3.2 through 0.5.1 do not check if a connection is read-only, which allows local users to bypass intended access restrictions and perform administrative actions.
CVE-2008-5249
Cross-site scripting (XSS) vulnerability in MediaWiki 1.13.0 through 1.13.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-5250
Cross-site scripting (XSS) vulnerability in MediaWiki before 1.6.11, 1.12.x before 1.12.2, and 1.13.x before 1.13.3, when Internet Explorer is used and uploads are enabled, or an SVG scripting browser is used and SVG uploads are enabled, allows remote authenticated users to inject arbitrary web script or HTML by editing a wiki page.
CVE-2008-5252
Cross-site request forgery (CSRF) vulnerability in the Special:Import feature in MediaWiki 1.3.0 through 1.6.10, 1.12.x before 1.12.2, and 1.13.x before 1.13.3 allows remote attackers to perform unspecified actions as authenticated users via unknown vectors.
CVE-2008-5663
Multiple unrestricted file upload vulnerabilities in Kusaba 1.0.4 and earlier allow remote authenticated users to execute arbitrary code by uploading a file with an executable extension using (1) load_receiver.php or (2) a shipainter action to paint_save.php, then accessing the uploaded file via a direct request to this file in their user directory.
CVE-2008-5664
Stack-based buffer overflow in Realtek Media Player (aka Realtek Sound Manager, RtlRack, or rtlrack.exe) 1.15.0.0 allows remote attackers to execute arbitrary code via a crafted playlist (PLA) file.
CVE-2008-5665
SQL injection vulnerability in index.php in the xhresim module in XOOPS allows remote attackers to execute arbitrary SQL commands via the no parameter.
CVE-2008-5666
WinFTP FTP Server 2.3.0, when passive (aka PASV) mode is used, allows remote authenticated users to cause a denial of service via a sequence of FTP sessions that include an invalid "NLST -1" command.
CVE-2008-5667
The scanning engine in VirusBlokAda VBA32 Personal Antivirus 3.12.8.x allows remote attackers to cause a denial of service (memory corruption and application crash) via a malformed RAR archive.
CVE-2008-5668
Multiple cross-site scripting (XSS) vulnerabilities in Textpattern (aka Txp CMS) 4.0.5 allow remote attackers to inject arbitrary web script or HTML via (1) the PATH_INFO to setup/index.php or (2) the name parameter to index.php in the comments preview section.
CVE-2008-5669
index.php in the comments preview section in Textpattern (aka Txp CMS) 4.0.5 allows remote attackers to cause a denial of service via a long message parameter.
CVE-2008-5670
Textpattern (aka Txp CMS) 4.0.5 does not ask for the old password during a password reset, which makes it easier for remote attackers to change a password after hijacking a session.
CVE-2008-5671
PHP remote file inclusion vulnerability in index.php in Joomla! 1.0.11 through 1.0.14, when RG_EMULATION is enabled in configuration.php, allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.
CVE-2008-5672
Multiple cross-site request forgery (CSRF) vulnerabilities in PHParanoid before 0.4 allow remote attackers to hijack the authentication of arbitrary users for requests that use (1) admin.php or (2) private messages.
CVE-2008-5673
PHParanoid before 0.4 does not properly restrict access to the members area by unauthenticated users, which has unknown impact and remote attack vectors.
CVE-2008-5674
Multiple array index errors in the HTTP server in Darkwet Network webcamXP 3.72.440.0 and earlier and beta 4.05.280 and earlier allow remote attackers to cause a denial of service (device crash) and read portions of memory via (1) an invalid camnum parameter to the pocketpc component and (2) an invalid id parameter to the show_gallery_pic component.
CVE-2008-5675
Unspecified vulnerability in IBM WebSphere Portal 6.0 before 6.0.1.5 has unknown impact and attack vectors related to "Access problems with BasicAuthTAI."
CVE-2008-5676
Multiple unspecified vulnerabilities in the ModSecurity (aka mod_security) module 2.5.0 through 2.5.5 for the Apache HTTP Server, when SecCacheTransformations is enabled, allow remote attackers to cause a denial of service (daemon crash) or bypass the product's functionality via unknown vectors related to "transformation caching."
CVE-2008-5677
Unrestricted file upload vulnerability in Kwalbum 2.0.4, 2.0.2, and earlier, when PICS_PATH is located in the web root, allows remote authenticated users with upload capability to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file under items/, related to the ReplaceBadFilenameChars function in include/ItemAdder.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-5678
Fretwell-Downing Informatics (FDI) OLIB7 WebView 2.5.1.1 allows remote authenticated users to obtain sensitive information from files via the infile parameter to the default URI under cgi/, as demonstrated by the (1) get_settings.ini, (2) setup.ini, and (3) text.ini files.
CVE-2008-5679
The HTML parsing engine in Opera before 9.63 allows remote attackers to execute arbitrary code via crafted web pages that trigger an invalid pointer calculation and heap corruption.
CVE-2008-5680
Multiple buffer overflows in Opera before 9.63 might allow (1) remote attackers to execute arbitrary code via a crafted text area, or allow (2) user-assisted remote attackers to execute arbitrary code via a long host name in a file: URL.  NOTE: this might overlap CVE-2008-5178.
CVE-2008-5681
Opera before 9.63 does not block unspecified "scripted URLs" during the feed preview, which allows remote attackers to read existing subscriptions and force subscriptions to arbitrary feed URLs.
CVE-2008-5682
Cross-site scripting (XSS) vulnerability in Opera before 9.63 allows remote attackers to inject arbitrary web script or HTML via built-in XSLT templates.
CVE-2008-5683
Unspecified vulnerability in Opera before 9.63 allows remote attackers to "reveal random data" via unknown vectors.
CVE-2008-5684
Unspecified vulnerability in the X Inter Client Exchange library (aka libICE) in Sun Solaris 8 through 10 and OpenSolaris before snv_85 allows context-dependent attackers to cause a denial of service (application crash), as demonstrated by a port scan that triggers a segmentation violation in the Gnome session manager (aka gnome-session).
CVE-2008-5685
Sun ScApp firmware 5.18.x, 5.19.x, and 5.20.0 through 5.20.10 on Sun Fire and Netra platforms allows remote attackers to access the System Controller (SC), the system console, and possibly the host OS, and cause a denial of service (shutdown or reboot), via spoofed IP packets.
Note: This issue only impacts systems that have a System Controller V2 without SSH enabled.
CVE-2008-5686
IBM Tivoli Provisioning Manager (TPM) before 5.1.1.1 IF0006, when its LDAP service is shared with other applications, does not require that an LDAP user be listed in the TPM user records, which allows remote authenticated users to execute SOAP commands that access arbitrary TPM functionality, as demonstrated by running provisioning workflows.
CVE-2008-5687
MediaWiki 1.11, and other versions before 1.13.3, does not properly protect against the download of backups of deleted images, which might allow remote attackers to obtain sensitive information via requests for files in images/deleted/.
CVE-2008-5688
MediaWiki 1.8.1, and other versions before 1.13.3, when the wgShowExceptionDetails variable is enabled, sometimes provides the full installation path in a debugging message, which might allow remote attackers to obtain sensitive information via unspecified requests that trigger an uncaught exception.
CVE-2008-5689
tun in IP Tunnel in Solaris 10 and OpenSolaris snv_01 through snv_76 allows local users to cause a denial of service (panic) and possibly execute arbitrary code via a crafted SIOCGTUNPARAM IOCTL request, which triggers a NULL pointer dereference.
Complete system compromise only affects x86 platforms (http://www.trapkit.de/advisories/TKADV2008-015.txt)
CVE-2008-5690
The Kerberos credential renewal feature in Sun Solaris 8, 9, and 10, and OpenSolaris build snv_01 through snv_104, allows local users to cause a denial of service (authentication failure) via unspecified vectors related to incorrect cache file permissions, and lack of credential storage by the store_cred function in pam_krb5.
CVE-2008-5691
Heap-based buffer overflow in the Phoenician Casino FlashAX ActiveX control 1.0.0.7 allows remote attackers to execute arbitrary code via a long argument to the SetID method.
CVE-2008-5692
Ipswitch WS_FTP Server Manager before 6.1.1, and possibly other Ipswitch products, allows remote attackers to bypass authentication and read logs via a logLogout action to FTPLogServer/login.asp followed by a request to FTPLogServer/LogViewer.asp with the localhostnull account name.
CVE-2008-5693
Ipswitch WS_FTP Server Manager 6.1.0.0 and earlier, and possibly other Ipswitch products, might allow remote attackers to read the contents of custom ASP files in WSFTPSVR/ via a request with an appended dot character.
CVE-2008-5694
PHP remote file inclusion vulnerability in lib/jpgraph/jpgraph_errhandler.inc.php in Sandbox 1.4.1 might allow remote attackers to execute arbitrary PHP code via unspecified vectors.  NOTE: the issue, if any, may be located in Aditus JpGraph rather than Sandbox. If so, then this should not be treated as an issue in Sandbox.
CVE-2008-5695
wp-admin/options.php in WordPress MU before 1.3.2, and WordPress 2.3.2 and earlier, does not properly validate requests to update an option, which allows remote authenticated users with manage_options and upload_files capabilities to execute arbitrary code by uploading a PHP script and adding this script's pathname to active_plugins.
CVE-2008-5696
Novell NetWare 6.5 before Support Pack 8, when an OES2 Linux server is installed into the NDS tree, does not require a password for the ApacheAdmin console, which allows remote attackers to reconfigure the Apache HTTP Server via console operations.
