CVE-2013-0522
The Notes Client Single Logon feature in IBM Notes 8.0, 8.0.1, 8.0.2, 8.5, 8.5.1, 8.5.2, 8.5.3, and 9.0 on Windows allows local users to discover passwords via vectors involving an unspecified operating system communication mechanism for password transmission between Windows and Notes. IBM X-Force ID: 82531.
CVE-2014-2079
X File Explorer (aka xfe) might allow local users to bypass intended access restrictions and gain access to arbitrary files by leveraging failure to use directory masks when creating files on Samba and NFS shares.
CVE-2017-15137
The OpenShift image import whitelist failed to enforce restrictions correctly when running commands such as "oc tag", for example. This could allow a user with access to OpenShift to run images from registries that should not be allowed.
CVE-2017-17541
A Cross-site Scripting (XSS) vulnerability in Fortinet FortiManager 6.0.0, 5.6.4 and below versions, FortiAnalyzer 6.0.0, 5.6.4 and below versions allows inject Javascript code and HTML tags through the CN value of CA and CRL certificates via the import CA and CRL certificates feature.
CVE-2017-2638
It was found that the REST API in Infinispan before version 9.0.0 did not properly enforce auth constraints. An attacker could use this vulnerability to read or modify data in the default cache or a known cache name.
CVE-2017-7468
In curl and libcurl 7.52.0 to and including 7.53.1, libcurl would attempt to resume a TLS session even if the client certificate had changed. That is unacceptable since a server by specification is allowed to skip the client certificate check on resume, and may instead use the old identity which was established by the previous certificate (or no certificate). libcurl supports by default the use of TLS session id/ticket to resume previous TLS sessions to speed up subsequent TLS handshakes. They are used when for any reason an existing TLS connection couldn't be kept alive to make the next handshake faster. This flaw is a regression and identical to CVE-2016-5419 reported on August 3rd 2016, but affecting a different version range.
CVE-2018-0341
A vulnerability in the web-based UI of Cisco IP Phone 6800, 7800, and 8800 Series with Multiplatform Firmware before 11.2(1) could allow an authenticated, remote attacker to perform a command injection and execute commands with the privileges of the web server. The vulnerability is due to insufficient input validation. An attacker could exploit this vulnerability by including arbitrary shell commands in a specific user input field. Cisco Bug IDs: CSCvi51426.
CVE-2018-0360
ClamAV before 0.100.1 has an HWP integer overflow with a resultant infinite loop via a crafted Hangul Word Processor file. This is in parsehwp3_paragraph() in libclamav/hwp.c.
CVE-2018-0361
ClamAV before 0.100.1 lacks a PDF object length check, resulting in an unreasonably long time to parse a relatively small file.
CVE-2018-0366
A vulnerability in the web-based management interface of Cisco Web Security Appliance (WSA) could allow an unauthenticated, remote attacker to conduct a reflected cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. The vulnerability is due to insufficient validation of user-supplied input by the web-based management interface of an affected device. An attacker could exploit this vulnerability by persuading a user of the interface to click a crafted link. A successful exploit could allow the attacker to execute arbitrary script code in the context of the interface or allow the attacker to access sensitive browser-based information. Cisco Bug IDs: CSCvf03514.
CVE-2018-0368
A vulnerability in Cisco Digital Network Architecture (DNA) Center could allow an authenticated, local attacker to access sensitive information on an affected system. The vulnerability is due to insufficient security restrictions imposed by the affected software. An attacker could exploit this vulnerability by accessing unprotected log files. A successful exploit could allow the attacker to access sensitive log files, which may include system credentials, on the affected device. Cisco Bug IDs: CSCvi22400.
CVE-2018-0369
A vulnerability in the reassembly logic for fragmented IPv4 packets of Cisco StarOS running on virtual platforms could allow an unauthenticated, remote attacker to trigger a reload of the npusim process, resulting in a denial of service (DoS) condition. There are four instances of the npusim process running per Service Function (SF) instance, each handling a subset of all traffic flowing across the device. It is possible to trigger a reload of all four instances of the npusim process around the same time. The vulnerability is due to improper handling of fragmented IPv4 packets containing options. An attacker could exploit this vulnerability by sending a malicious IPv4 packet across an affected device. An exploit could allow the attacker to trigger a restart of the npusim process, which will result in all traffic queued toward this instance of the npusim process to be dropped while the process is restarting. The npusim process typically restarts within less than a second. This vulnerability affects: Cisco Virtualized Packet Core-Single Instance (VPC-SI), Cisco Virtualized Packet Core-Distributed Instance (VPC-DI), Cisco Ultra Packet Core (UPC). Cisco Bug IDs: CSCvh29613.
CVE-2018-0370
A vulnerability in the detection engine of Cisco Firepower System Software could allow an unauthenticated, remote attacker to cause one of the detection engine processes to run out of memory and thus slow down traffic processing. The vulnerability is due to improper handling of traffic when the Secure Sockets Layer (SSL) inspection policy is enabled. An attacker could exploit this vulnerability by sending malicious traffic through an affected device. An exploit could allow the attacker to increase the resource consumption of a single instance of the Snort detection engine on an affected device. This will lead to performance degradation and eventually the restart of the affected Snort process. Cisco Bug IDs: CSCvi09219, CSCvi29845.
CVE-2018-0383
A vulnerability in the detection engine of Cisco FireSIGHT System Software could allow an unauthenticated, remote attacker to bypass a file policy that is configured to block the transfer of files to an affected system via FTP. The vulnerability exists because the affected software incorrectly handles FTP control connections. An attacker could exploit this vulnerability by sending a maliciously crafted FTP connection to transfer a file to an affected device. A successful exploit could allow the attacker to bypass a file policy that is configured to apply the Block upload with reset action to FTP traffic. Cisco Bug IDs: CSCvh70130.
CVE-2018-0384
A vulnerability in the detection engine of Cisco FireSIGHT System Software could allow an unauthenticated, remote attacker to bypass a URL-based access control policy that is configured to block traffic for an affected system. The vulnerability exists because the affected software incorrectly handles TCP packets that are received out of order when a TCP SYN retransmission is issued. An attacker could exploit this vulnerability by sending a maliciously crafted connection through an affected device. A successful exploit could allow the attacker to bypass a URL-based access control policy that is configured to block traffic for the affected system. Cisco Bug IDs: CSCvh84511.
CVE-2018-0385
A vulnerability in the detection engine parsing of Security Socket Layer (SSL) protocol packets for Cisco Firepower System Software could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition due to the Snort process unexpectedly restarting. The vulnerability is due to improper input handling of the SSL traffic. An attacker could exploit this vulnerability by sending a crafted SSL traffic to the detection engine on the targeted device. An exploit could allow the attacker to cause a DoS condition if the Snort process restarts and traffic inspection is bypassed or traffic is dropped. Cisco Bug IDs: CSCvi36434.
CVE-2018-1046
pdns before version 4.1.2 is vulnerable to a buffer overflow in dnsreplay. In the dnsreplay tool provided with PowerDNS Authoritative, replaying a specially crafted PCAP file can trigger a stack-based buffer overflow, leading to a crash and potentially arbitrary code execution. This buffer overflow only occurs when the -ecs-stamp option of dnsreplay is used.
CVE-2018-10840
Linux kernel is vulnerable to a heap-based buffer overflow in the fs/ext4/xattr.c:ext4_xattr_set_entry() function. An attacker could exploit this by operating on a mounted crafted ext4 image.
CVE-2018-10857
git-annex is vulnerable to a private data exposure and exfiltration attack. It could expose the content of files located outside the git-annex repository, or content from a private web server on localhost or the LAN.
CVE-2018-10859
git-annex is vulnerable to an Information Exposure when decrypting files. A malicious server for a special remote could trick git-annex into decrypting a file that was encrypted to the user's gpg key. This attack could be used to expose encrypted data that was never stored in git-annex
CVE-2018-10886
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: this candidate is not about any specific product, protocol, or design, that falls into the scope of the assigning CNA. Notes: None.
CVE-2018-11716
An issue was discovered in Zoho ManageEngine Desktop Central before 100230. There is unauthenticated remote access to all log files of a Desktop Central instance containing critical information (private information such as location of enrolled devices, cleartext passwords, patching level, etc.) via a GET request on port 8022, 8443, or 8444.
CVE-2018-11717
An issue was discovered in Zoho ManageEngine Desktop Central before 100251. By leveraging access to a log file, a context-dependent attacker can obtain (depending on the modules configured) the Base64 encoded Password/Username of AD accounts, the cleartext Password/Username and mail settings of the EAS account (an AD account used to send mail), the cleartext password of recovery_password of Android devices, the cleartext password of account "set", the location of devices enrolled in the platform (with UUID and information related to the name of the person at the location), critical information about all enrolled devices such as Serial Number, UUID, Model, Name, and auth_session_token (usable to spoof a terminal identity on the platform), etc.
CVE-2018-12584
The ConnectionBase::preparseNewBytes function in resip/stack/ConnectionBase.cxx in reSIProcate through 1.10.2 allows remote attackers to cause a denial of service (buffer overflow) or possibly execute arbitrary code when TLS communication is enabled.
CVE-2018-13387
The IncomingMailServers resource in Atlassian JIRA Server before version 7.6.7, from version 7.7.0 before version 7.7.5, from version 7.8.0 before version 7.8.5, from version 7.9.0 before version 7.9.3 and from version 7.10.0 before version 7.10.2 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the messagesThreshold parameter as the fix for CVE-2017-18039 was incomplete.
CVE-2018-13832
Multiple Persistent cross-site scripting (XSS) issues in the Techotronic all-in-one-favicon (aka All In One Favicon) plugin 4.6 for WordPress allow remote attackers to inject arbitrary web script or HTML via Apple-Text, GIF-Text, ICO-Text, PNG-Text, or JPG-Text.
CVE-2018-13980
The websites that were built from Zeta Producer Desktop CMS before 14.2.1 are vulnerable to unauthenticated file disclosure if the plugin "filebrowser" is installed, because of assets/php/filebrowser/filebrowser.main.php?file=../ directory traversal.
CVE-2018-13981
The websites that were built from Zeta Producer Desktop CMS before 14.2.1 are vulnerable to unauthenticated remote code execution due to a default component that permits arbitrary upload of PHP files, because the formmailer widget blocks .php files but not .php5 or .phtml files. This is related to /assets/php/formmailer/SendEmail.php and /assets/php/formmailer/functions.php.
CVE-2018-14071
The Geo Mashup plugin before 1.10.4 for WordPress has insufficient sanitization of post editor and other user input.
CVE-2018-14084
An issue was discovered in a smart contract implementation for MKCB, an Ethereum token. If the owner sets the value of sellPrice to a large number in setPrices() then the "amount * sellPrice" will cause an integer overflow in sell().
CVE-2018-14085
An issue was discovered in a smart contract implementation for UserWallet 0x0a7bca9FB7AfF26c6ED8029BB6f0F5D291587c42, an Ethereum token. First, suppose that the owner adds the evil contract address to his sweepers. The evil contract looks like this: contract Exploit { uint public start; function sweep(address _token, uint _amount) returns (bool) { start = 0x123456789; return true;} }. Then, when one calls the function sweep() in the UserWallet contract, it will change the sweeperList to 0X123456789.
CVE-2018-14086
An issue was discovered in a smart contract implementation for SingaporeCoinOrigin (SCO), an Ethereum token. The contract has an integer overflow. If the owner sets the value of sellPrice to a large number in setPrices() then the "amount * sellPrice" will cause an integer overflow in sell().
CVE-2018-14087
An issue was discovered in a smart contract implementation for EUC (EUC), an Ethereum token. The contract has an integer overflow. If the owner sets the value of buyPrice to a large number in setPrices() then the "msg.value * buyPrice" will cause an integer overflow in the fallback function.
CVE-2018-14088
An issue was discovered in a smart contract implementation for STeX White List (STE(WL)), an Ethereum token. The contract has an integer overflow. If the owner sets the value of amount to a large number then the "amount * 1000000000000000" will cause an integer overflow in withdrawToFounders().
CVE-2018-14089
An issue was discovered in a smart contract implementation for Virgo_ZodiacToken, an Ethereum token. In this contract, 'bool sufficientAllowance = allowance <= _value' will cause an arbitrary transfer in the function transferFrom because '<=' is used instead of '>=' (which was intended). An attacker can transfer from any address to his address, and does not need to meet the 'allowance > value' condition.
CVE-2018-14324
The demo feature in Oracle GlassFish Open Source Edition 5.0 has TCP port 7676 open by default with a password of admin for the admin account. This allows remote attackers to obtain potentially sensitive information, perform database operations, or manipulate the demo via a JMX RMI session, aka a "jmx_rmi remote monitoring and control problem." NOTE: this is not an Oracle supported product.
CVE-2018-14325
In MP4v2 2.0.0, there is an integer underflow (with resultant memory corruption) when parsing MP4Atom in mp4atom.cpp.
CVE-2018-14326
In MP4v2 2.0.0, there is an integer overflow (with resultant memory corruption) when resizing MP4Array for the ftyp atom in mp4array.h.
CVE-2018-5229
The NotificationRepresentationFactoryImpl class in Atlassian Universal Plugin Manager before version 2.22.9 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the name of user submitted add-on names.
CVE-2018-5239
Norton App Lock prior to v1.3.0.332 can be susceptible to a bypass exploit. In this type of circumstance, the exploit can allow the user to circumvent the app to prevent it from locking the device, thereby allowing the individual to gain device access.
