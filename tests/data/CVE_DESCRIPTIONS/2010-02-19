CVE-2009-4644
Accellion Secure File Transfer Appliance before 8_0_105 allows remote authenticated administrators to bypass the restricted shell and execute arbitrary commands via shell metacharacters to the ping command, as demonstrated by modifying the cli program.
CVE-2009-4645
Directory traversal vulnerability in web_client_user_guide.html in Accellion Secure File Transfer Appliance before 8_0_105 allows remote attackers to read arbitrary files via a .. (dot dot) in the lang parameter.
CVE-2009-4646
Static code injection vulnerability in the administrative web interface in Accellion Secure File Transfer Appliance allows remote authenticated administrators to inject arbitrary shell commands by appending them to a request to update the SNMP public community string.
CVE-2009-4647
Cross-site scripting (XSS) vulnerability in Accellion Secure File Transfer Appliance before 7_0_296 allows remote attackers to inject arbitrary web script or HTML via the username parameter, which is not properly handled when the administrator views audit logs.
CVE-2009-4648
Accellion Secure File Transfer Appliance before 8_0_105 does not properly restrict access to sensitive commands and arguments that run with extra sudo privileges, which allows local administrators to gain privileges via (1) arbitrary arguments in the --file_move action in /usr/local/bin/admin.pl, or a hard link attack in (2) chmod or (3) a certain cp command.
CVE-2010-0106
The on-demand scanning in Symantec AntiVirus 10.0.x and 10.1.x before MR9, AntiVirus 10.2.x, and Client Security 3.0.x and 3.1.x before MR9, when Tamper protection is disabled, allows remote attackers to cause a denial of service (prevention of on-demand scanning) via "specific events" that prevent the user from having read access to unspecified resources.
CVE-2010-0108
Buffer overflow in the cliproxy.objects.1 ActiveX control in the Symantec Client Proxy (CLIproxy.dll) in Symantec AntiVirus 10.0.x, 10.1.x before MR9, and 10.2.x before MR4; and Symantec Client Security 3.0.x and 3.1.x before MR9 allows remote attackers to execute arbitrary code via a long argument to the SetRemoteComputerName function.
CVE-2010-0149
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.2 before 7.2(4.46), 8.0 before 8.0(4.38), 8.1 before 8.1(2.29), and 8.2 before 8.2(1.5); and Cisco PIX 500 Series Security Appliance; allows remote attackers to cause a denial of service (prevention of new connections) via crafted TCP segments during termination of the TCP connection that cause the connection to remain in CLOSEWAIT status, aka "TCP Connection Exhaustion Denial of Service Vulnerability."
CVE-2010-0150
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.0 before 7.0(8.10), 7.2 before 7.2(4.45), 8.0 before 8.0(5.2), 8.1 before 8.1(2.37), and 8.2 before 8.2(1.16); and Cisco PIX 500 Series Security Appliance; allows remote attackers to cause a denial of service (device reload) via malformed SIP messages, aka Bug ID CSCsy91157.
CVE-2010-0151
The Cisco Firewall Services Module (FWSM) 4.0 before 4.0(8), as used in for the Cisco Catalyst 6500 switches, Cisco 7600 routers, and ASA 5500 Adaptive Security Appliances, allows remote attackers to cause a denial of service (crash) via a malformed Skinny Client Control Protocol (SCCP) message.
Per: http://www.cisco.com/en/US/products/products_security_advisory09186a0080b1910e.shtml

"All non-fixed 4.x versions of Cisco FWSM Software are affected by this vulnerability if SCCP inspection is enabled. SCCP inspection is enabled by default."
CVE-2010-0565
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.2 before 7.2(4.45), 8.0 before 8.0(4.44), 8.1 before 8.1(2.35), and 8.2 before 8.2(1.10), allows remote attackers to cause a denial of service (page fault and device reload) via a malformed DTLS message, aka Bug ID CSCtb64913 and "WebVPN DTLS Denial of Service Vulnerability."
CVE-2010-0566
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.0 before 7.0(8.10), 7.2 before 7.2(4.45), 8.0 before 8.0(4.44), 8.1 before 8.1(2.35), and 8.2 before 8.2(1.10) allows remote attackers to cause a denial of service (device reload) via a malformed TCP segment when certain NAT translation and Cisco AIP-SSM configurations are used, aka Bug ID CSCtb37219.
CVE-2010-0567
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.0 before 7.0(8.10), 7.2 before 7.2(4.45), 8.0 before 8.0(5.1), 8.1 before 8.1(2.37), and 8.2 before 8.2(1.15); and Cisco PIX 500 Series Security Appliance; allows remote attackers to cause a denial of service (active IPsec tunnel loss and prevention of new tunnels) via a malformed IKE message through an existing tunnel to UDP port 4500, aka Bug ID CSCtc47782.
CVE-2010-0568
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.0 before 7.0(8.10), 7.2 before 7.2(4.45), 8.0 before 8.0(5.7), 8.1 before 8.1(2.40), and 8.2 before 8.2(2.1); and Cisco PIX 500 Series Security Appliance; allows remote attackers to bypass NTLMv1 authentication via a crafted username, aka Bug ID CSCte21953.
CVE-2010-0569
Unspecified vulnerability in Cisco ASA 5500 Series Adaptive Security Appliance 7.0 before 7.0(8.10), 7.2 before 7.2(4.45), 8.0 before 8.0(5.2), 8.1 before 8.1(2.37), and 8.2 before 8.2(1.16); and Cisco PIX 500 Series Security Appliance; allows remote attackers to cause a denial of service (device reload) via malformed SIP messages, aka Bug ID CSCtc96018.
CVE-2010-0665
JAG (Just Another Guestbook) 1.14 stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain sensitive information via a direct request for jag/database.sql.
CVE-2010-0666
Unspecified vulnerability in eMBox in Novell eDirectory 8.8 SP5 Patch 2 and earlier allows remote attackers to cause a denial of service (crash) via unknown a crafted SOAP request, a different issue than CVE-2008-0926.
