CVE-2015-9266
The web management interface of Ubiquiti airMAX, airFiber, airGateway and EdgeSwitch XP (formerly TOUGHSwitch) allows an unauthenticated attacker to upload and write arbitrary files using directory traversal techniques. An attacker can exploit this vulnerability to gain root privileges. This vulnerability is fixed in the following product versions (fixes released in July 2015, all prior versions are affected): airMAX AC 7.1.3; airMAX M (and airRouter) 5.6.2 XM/XW/TI, 5.5.11 XM/TI, and 5.5.10u2 XW; airGateway 1.1.5; airFiber AF24/AF24HD 2.2.1, AF5x 3.0.2.1, and AF5 2.2.1; airOS 4 XS2/XS5 4.0.4; and EdgeSwitch XP (formerly TOUGHSwitch) 1.3.2.
CVE-2016-1000030
Pidgin version <2.11.0 contains a vulnerability in X.509 Certificates imports specifically due to improper check of return values from gnutls_x509_crt_init() and gnutls_x509_crt_import() that can result in code execution. This attack appear to be exploitable via custom X.509 certificate from another client. This vulnerability appears to have been fixed in 2.11.0.
CVE-2016-1000232
NodeJS Tough-Cookie version 2.2.2 contains a Regular Expression Parsing vulnerability in HTTP request Cookie Header parsing that can result in Denial of Service. This attack appear to be exploitable via Custom HTTP header passed by client. This vulnerability appears to have been fixed in 2.3.0.
CVE-2018-0502
An issue was discovered in zsh before 5.6. The beginning of a #! script file was mishandled, potentially leading to an execve call to a program named on the second line.
CVE-2018-1000662
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was inadvertently assigned by a different CNA at a time when the discoverer was communicating with the specific "Vendors and Projects" CNA for the product in question.
CVE-2018-1000672
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-16391, CVE-2018-16392, CVE-2018-16393, CVE-2018-16418, CVE-2018-16419, CVE-2018-16420, CVE-2018-16421, CVE-2018-16422, CVE-2018-16423, CVE-2018-16424, CVE-2018-16425, CVE-2018-16426, CVE-2018-16427. Reason: This candidate is a duplicate of CVE-2018-16391, CVE-2018-16392, CVE-2018-16393, CVE-2018-16418, CVE-2018-16419, CVE-2018-16420, CVE-2018-16421, CVE-2018-16422, CVE-2018-16423, CVE-2018-16424, CVE-2018-16425, CVE-2018-16426, and CVE-2018-16427. Notes: All CVE users should reference CVE-2018-16391, CVE-2018-16392, CVE-2018-16393, CVE-2018-16418, CVE-2018-16419, CVE-2018-16420, CVE-2018-16421, CVE-2018-16422, CVE-2018-16423, CVE-2018-16424, CVE-2018-16425, CVE-2018-16426, and/or CVE-2018-16427 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-13259
An issue was discovered in zsh before 5.6. Shebang lines exceeding 64 characters were truncated, potentially leading to an execve call to a program name that is a substring of the intended one.
CVE-2018-1353
An information disclosure vulnerability in Fortinet FortiManager 6.0.1 and below versions allows a standard user with adom assignment read the interface settings of vdoms unrelated to the assigned adom.
CVE-2018-14618
curl before version 7.61.1 is vulnerable to a buffer overrun in the NTLM authentication code. The internal function Curl_ntlm_core_mk_nt_hash multiplies the length of the password by two (SUM) to figure out how large temporary storage area to allocate from the heap. The length value is then subsequently used to iterate over the password and generate output into the allocated storage buffer. On systems with a 32 bit size_t, the math to calculate SUM triggers an integer overflow when the password length exceeds 2GB (2^31 bytes). This integer overflow usually causes a very small buffer to actually get allocated instead of the intended very huge one, making the use of that buffer end up in a heap buffer overflow. (This bug is almost identical to CVE-2017-8816.)
CVE-2018-14769
VIVOTEK FD8177 devices before XXXXXX-VVTK-xx06a allow CSRF.
CVE-2018-14770
VIVOTEK FD8177 devices before XXXXXX-VVTK-xx06a allow remote attackers to execute arbitrary code (issue 1 of 2) via the ONVIF interface, (/onvif/device_service).
CVE-2018-14771
VIVOTEK FD8177 devices before XXXXXX-VVTK-xx06a allow remote attackers to execute arbitrary code (issue 2 of 2) via eventscript.cgi.
CVE-2018-15676
An issue was discovered in BTITeam XBTIT. By using String.replace and eval, it is possible to bypass the includes/crk_protection.php anti-XSS mechanism that looks for a number of dangerous fingerprints.
CVE-2018-15677
The newsfeed (aka /index.php?page=viewnews) in BTITeam XBTIT 2.5.4 has stored XSS via the title of a news item. This is also exploitable via CSRF.
CVE-2018-15678
An issue was discovered in BTITeam XBTIT 2.5.4. The "act" parameter in the sign-up page available at /index.php?page=signup is vulnerable to reflected cross-site scripting.
CVE-2018-15679
An issue was discovered in BTITeam XBTIT 2.5.4. The "keywords" parameter in the search function available at /index.php?page=forums&action=search is vulnerable to reflected cross-site scripting.
CVE-2018-15680
An issue was discovered in BTITeam XBTIT 2.5.4. The hashed passwords stored in the xbtit_users table are stored as unsalted MD5 hashes, which makes it easier for context-dependent attackers to obtain cleartext values via a brute-force attack.
CVE-2018-15681
An issue was discovered in BTITeam XBTIT 2.5.4. When a user logs in, their password hash is rehashed using a predictable salt and stored in the "pass" cookie, which is not flagged as HTTPOnly. Due to the weak and predictable salt that is in place, an attacker who successfully steals this cookie can efficiently brute-force it to retrieve the user's cleartext password.
CVE-2018-15682
An issue was discovered in BTITeam XBTIT. Due to a lack of cross-site request forgery protection, it is possible to automate the action of sending private messages to users by luring an authenticated user to a web page that automatically submits a form on their behalf.
CVE-2018-15683
An issue was discovered in BTITeam XBTIT. The "returnto" parameter of the login page is vulnerable to an open redirect due to a lack of validation. If a user is already logged in when accessing the page, they will be instantly redirected.
CVE-2018-15684
An issue was discovered in BTITeam XBTIT. PHP error logs are stored in an open directory (/include/logs) using predictable file names, which can lead to full path disclosure and leakage of sensitive data.
CVE-2018-15917
Persistent cross-site scripting (XSS) issues in Jorani 0.6.5 allow remote attackers to inject arbitrary web script or HTML via the language parameter to session/language.
CVE-2018-15918
An issue was discovered in Jorani 0.6.5. SQL Injection (error-based) allows a user of the application without permissions to read and modify sensitive information from the database used by the application via the startdate or enddate parameter to leaves/validate.
CVE-2018-16144
The test connection functionality in the NetAudit section of Opsview Monitor before 5.3.1 and 5.4.x before 5.4.2 is vulnerable to command injection due to improper sanitization of the rancid_password parameter.
CVE-2018-16145
The /etc/init.d/opsview-reporting-module script that runs at boot time in Opsview Monitor before 5.3.1 and 5.4.x before 5.4.2 invokes a file that can be edited by the nagios user, and would allow attackers to elevate their privileges to root after a system restart, hence obtaining full control of the appliance.
CVE-2018-16146
The web management console of Opsview Monitor 5.4.x before 5.4.2 provides functionality accessible by an authenticated administrator to test notifications that are triggered under certain configurable events. The value parameter is not properly sanitized, leading to arbitrary command injection with the privileges of the nagios user account.
CVE-2018-16147
The data parameter of the /settings/api/router endpoint in Opsview Monitor before 5.3.1 and 5.4.x before 5.4.2 is vulnerable to Cross-Site Scripting.
CVE-2018-16148
The diagnosticsb2ksy parameter of the /rest endpoint in Opsview Monitor before 5.3.1 and 5.4.x before 5.4.2 is vulnerable to Cross-Site Scripting.
CVE-2018-16252
FsPro Labs Event Log Explorer 4.6.1.2115 has ".elx" FileType XML External Entity Injection.
CVE-2018-16307
An "Out-of-band resource load" issue was discovered on Xiaomi MIWiFi Xiaomi_55DD Version 2.8.50 devices. It is possible to induce the application to retrieve the contents of an arbitrary external URL and return those contents in its own response. If a domain name (containing a random string) is used in the HTTP Host header, the application performs an HTTP request to the specified domain. The response from that request is then included in the application's own response.
CVE-2018-16361
An issue was discovered in BTITeam XBTIT 2.5.4. news.php allows XSS via the id parameter.
CVE-2018-16381
e107 2.1.8 has XSS via the e107_admin/users.php?mode=main&action=list user_loginname parameter.
CVE-2018-16436
Gxlcms 2.0 before bug fix 20180915 has SQL Injection exploitable by an administrator.
CVE-2018-16437
Gxlcms 2.0 before bug fix 20180915 has Directory Traversal exploitable by an administrator.
CVE-2018-16509
An issue was discovered in Artifex Ghostscript before 9.24. Incorrect "restoration of privilege" checking during handling of /invalidaccess exceptions could be used by attackers able to supply crafted PostScript to execute code using the "pipe" instruction.
CVE-2018-16510
An issue was discovered in Artifex Ghostscript before 9.24. Incorrect exec stack handling in the "CS" and "SC" PDF primitives could be used by remote attackers able to supply crafted PDFs to crash the interpreter or possibly have unspecified other impact.
CVE-2018-16511
An issue was discovered in Artifex Ghostscript before 9.24. A type confusion in "ztype" could be used by remote attackers able to supply crafted PostScript to crash the interpreter or possibly have unspecified other impact.
CVE-2018-16513
In Artifex Ghostscript before 9.24, attackers able to supply crafted PostScript files could use a type confusion in the setcolor function to crash the interpreter or possibly have unspecified other impact.
CVE-2018-16516
helpers.py in Flask-Admin 1.5.2 has Reflected XSS via a crafted URL.
CVE-2018-16518
A directory traversal vulnerability with remote code execution in Prim'X Zed! FREE through 1.0 build 186 and Zed! Limited Edition through 6.1 build 2208 allows creation of arbitrary files on a user's workstation using crafted ZED! containers because the watermark loading function can place an executable file into a Startup folder.
CVE-2018-16521
An XML External Entity (XXE) vulnerability exists in HTML Form Entry 3.7.0, as distributed in OpenMRS Reference Application 2.8.0.
CVE-2018-16539
In Artifex Ghostscript before 9.24, attackers able to supply crafted PostScript files could use incorrect access checking in temp file handling to disclose contents of files on the system otherwise not readable.
CVE-2018-16540
In Artifex Ghostscript before 9.24, attackers able to supply crafted PostScript files to the builtin PDF14 converter could use a use-after-free in copydevice handling to crash the interpreter or possibly have unspecified other impact.
CVE-2018-16541
In Artifex Ghostscript before 9.24, attackers able to supply crafted PostScript files could use incorrect free logic in pagedevice replacement to crash the interpreter.
CVE-2018-16542
In Artifex Ghostscript before 9.24, attackers able to supply crafted PostScript files could use insufficient interpreter stack-size checking during error handling to crash the interpreter.
CVE-2018-16543
In Artifex Ghostscript before 9.24, gssetresolution and gsgetresolution allow attackers to have an unspecified impact.
CVE-2018-16545
Kaizen Asset Manager (Enterprise Edition) and Training Manager (Enterprise Edition) allow a remote attacker to achieve arbitrary code execution via file impersonation. For example, a malicious dynamic-link library (dll) assumed the identity of a temporary (tmp) file (isxdl.dll) and an executable file assumed the identity of a temporary file (996E.temp).
CVE-2018-16546
Amcrest networked devices use the same hardcoded SSL private key across different customers' installations, which allows remote attackers to defeat cryptographic protection mechanisms by leveraging knowledge of this key from another installation, as demonstrated by Amcrest_IPC-HX1X3X-LEXUS_Eng_N_AMCREST_V2.420.AC01.3.R.20180206.
CVE-2018-16548
An issue was discovered in ZZIPlib through 0.13.69. There is a memory leak triggered in the function __zzip_parse_root_directory in zip.c, which will lead to a denial of service attack.
CVE-2018-16549
HScripts PHP File Browser Script v1.0 allows Directory Traversal via the index.php path parameter.
CVE-2018-16550
TeamViewer 10.x through 13.x allows remote attackers to bypass the brute-force authentication protection mechanism by skipping the "Cancel" step, which makes it easier to determine the correct value of the default 4-digit PIN.
CVE-2018-16551
LavaLite 5.5 has XSS via a /edit URI, as demonstrated by client/job/job/Zy8PWBekrJ/edit.
CVE-2018-16552
MicroPyramid Django-CRM 0.2 allows CSRF for /users/create/, /users/##/edit/, and /accounts/##/delete/ URIs.
CVE-2018-9192
A plaintext recovery of encrypted messages or a Man-in-the-middle (MiTM) attack on RSA PKCS #1 v1.5 encryption may be possible without knowledge of the server's private key. Fortinet FortiOS 5.4.6 to 5.4.9, 6.0.0 and 6.0.1 are vulnerable by such attack under SSL Deep Inspection feature when CPx being used.
CVE-2018-9194
A plaintext recovery of encrypted messages or a Man-in-the-middle (MiTM) attack on RSA PKCS #1 v1.5 encryption may be possible without knowledge of the server's private key. Fortinet FortiOS 5.4.6 to 5.4.9, 6.0.0 and 6.0.1 are vulnerable by such attack under VIP SSL feature when CPx being used.
