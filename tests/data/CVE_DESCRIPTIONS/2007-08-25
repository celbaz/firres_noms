CVE-2007-4131
Directory traversal vulnerability in the contains_dot_dot function in src/names.c in GNU tar allows user-assisted remote attackers to overwrite arbitrary files via certain //.. (slash slash dot dot) sequences in directory symlinks in a TAR archive.
CVE-2007-4522
Multiple SQL injection vulnerabilities in Ripe Website Manager 0.8.9 and earlier allow remote authenticated users to execute arbitrary SQL commands via one or more of the following vectors: the (1) id parameter to (a) pages/delete_page.php, (b) navigation/delete_menu.php, and (c) navigation/delete_item.php in admin/; the (2) menu_id, (3) name, (3) page_id, and (4) url parameters in (d) admin/navigation/do_new_item.php; the (5) new_menuname parameter in (e) admin/navigation/do_new_nav.php; and (6) area1, name, and url parameters to (f) admin/pages/do_new_page.php.  NOTE: some vectors might be reachable through the url and name parameters to (g) admin/navigation/new_nav_item.php.  NOTE: the original disclosure does not precisely state which vectors are associated with SQL injection versus XSS.
CVE-2007-4523
Multiple cross-site scripting (XSS) vulnerabilities in Ripe Website Manager 0.8.9 and earlier allow remote authenticated users to inject arbitrary web script or HTML via one or more of the following vectors: the (1) id parameter to (a) pages/delete_page.php, (b) navigation/delete_menu.php, and (c) navigation/delete_item.php in admin/; the (2) menu_id, (3) name, (3) page_id, and (4) url parameters in (d) admin/navigation/do_new_item.php; the (5) new_menuname parameter in (e) admin/navigation/do_new_nav.php; and (6) area1, name, and url parameters to (f) admin/pages/do_new_page.php, probably involving the Title or textarea field as reachable through admin/pages/new_page.php.  NOTE: the original disclosure does not precisely state which vectors are associated with SQL injection versus XSS.
CVE-2007-4524
PHP remote file inclusion vulnerability in adisplay.php in PhPress 0.2.0 allows remote attackers to execute arbitrary PHP code via a URL in the lang parameter.
CVE-2007-4525
** DISPUTED **  PHP remote file inclusion vulnerability in inc-calcul.php3 in SPIP 1.7.2 allows remote attackers to execute arbitrary PHP code via a URL in the squelette_cache parameter, a different vector than CVE-2006-1702. NOTE: this issue has been disputed by third party researchers, stating that the squelette_cache variable is initialized before use, and is only used within the scope of a function.
CVE-2007-4526
The Client Login Extension (CLE) in Novell Identity Manager before 3.5.1 20070730 stores the username and password in a local file, which allows local users to obtain sensitive information by reading this file.
CVE-2007-4527
Unrestricted file upload vulnerability in phUploader.php in phphq.Net phUploader 1.2 allows remote attackers to upload and execute arbitrary code via unspecified vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-4528
The Foreign Function Interface (ffi) extension in PHP 5.0.5 does not follow safe_mode restrictions, which allows context-dependent attackers to execute arbitrary code by loading an arbitrary DLL and calling a function, as demonstrated by kernel32.dll and the WinExec function.  NOTE: this issue does not cross privilege boundaries in most contexts, so perhaps it should not be included in CVE.
CVE-2007-4529
The WebAdmin interface in TeamSpeak Server 2.0.20.1 allows remote authenticated users with the ServerAdmin flag to assign Registered users certain privileges, resulting in a privilege set that extends beyond that ServerAdmin's own servers, as demonstrated by the (1) AdminAddServer, (2) AdminDeleteServer, (3) AdminStartServer, and (4) AdminStopServer privileges; and administration of arbitrary virtual servers via a request to a .tscmd URI with a modified serverid parameter, as demonstrated by (a) add_server.tscmd, (b) ask_delete_server.tscmd, (c) start_server.tscmd, and (d) stop_server.tscmd.
CVE-2007-4530
Multiple cross-site scripting (XSS) vulnerabilities in TeamSpeak Server 2.0.20.1 allow remote attackers to inject arbitrary web script or HTML via (1) the error_text parameter to error_box.html or (2) the ok_title parameter to ok_box.html.
CVE-2007-4531
Soldat game server 1.4.2 and earlier, and dedicated server 2.6.2 and earlier, allows remote attackers to cause a client denial of service (crash) via (1) a long string to the file transfer port or (2) a long chat message, or (3) a server denial of service (continuous beep and slowdown) via a string containing many 0x07 or other control characters to the file transfer port.
CVE-2007-4532
Soldat game server 1.4.2 and earlier, and dedicated server 2.6.2 and earlier, allows remote attackers to cause a denial of service (client lockout) via a series of UDP join packets from a spoofed IP address, which triggers temporary blacklisting of this IP address.
CVE-2007-4533
Format string vulnerability in the Say command in sv_main.cpp in Vavoom 1.24 and earlier allows remote attackers to execute arbitrary code via format string specifiers in a chat message, related to a call to the BroadcastPrintf function.
CVE-2007-4534
Buffer overflow in the VThinker::BroadcastPrintf function in p_thinker.cpp in Vavoom 1.24 and earlier allows remote attackers to execute arbitrary code via (1) a long string in a chat message and possibly (2) a long name field.
CVE-2007-4535
The VStr::Resize function in str.cpp in Vavoom 1.24 and earlier allows remote attackers to cause a denial of service (daemon crash) via a string with a negative NewLen value within a certain UDP packet that triggers an assertion error.
CVE-2007-4536
TorrentTrader 1.07 and earlier sets insecure permissions for files in the root directory, which allows attackers to execute arbitrary PHP code by modifying (1) disclaimer.txt, (2) sponsors.txt, and (3) banners.txt, which are used in an include call.  NOTE: there might be local attack vectors that extend to other files.
