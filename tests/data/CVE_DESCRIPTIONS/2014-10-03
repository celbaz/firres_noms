CVE-2014-0754
Directory traversal vulnerability in SchneiderWEB on Schneider Electric Modicon PLC Ethernet modules 140CPU65x Exec before 5.5, 140NOC78x Exec before 1.62, 140NOE77x Exec before 6.2, BMXNOC0401 before 2.05, BMXNOE0100 before 2.9, BMXNOE0110x Exec before 6.0, TSXETC101 Exec before 2.04, TSXETY4103x Exec before 5.7, TSXETY5103x Exec before 5.9, TSXP57x ETYPort Exec before 5.7, and TSXP57x Ethernet Copro Exec before 5.5 allows remote attackers to visit arbitrary resources via a crafted HTTP request.
CVE-2014-3947
Unrestricted file upload vulnerability in the powermail extension before 1.6.11 and 2.x before 2.0.14 for TYPO3 allows remote attackers to execute arbitrary code by uploading a file with a crafted extension, then accessing it via unspecified vectors.
Vendor advisory - http://typo3.org/teams/security/security-bulletins/typo3-extensions/typo3-ext-sa-2014-007/
CVE-2014-4809
The WebSEAL component in IBM Security Access Manager for Web 7.x before 7.0.0-ISS-WGA-IF0009 and 8.x before 8.0.0-ISS-WGA-FP0005, when e-community SSO is enabled, allows remote attackers to cause a denial of service (component hang) via unspecified vectors.
CVE-2014-4823
The administration console in IBM Security Access Manager for Web 7.x before 7.0.0-ISS-WGA-IF0009 and 8.x before 8.0.0-ISS-WGA-FP0005, and Security Access Manager for Mobile 8.x before 8.0.0-ISS-ISAM-FP0005, allows remote attackers to inject system commands via unspecified vectors.
CVE-2014-5410
The DNP3 feature on Rockwell Automation Allen-Bradley MicroLogix 1400 1766-Lxxxxx A FRN controllers 7 and earlier and 1400 1766-Lxxxxx B FRN controllers before 15.001 allows remote attackers to cause a denial of service (process disruption) via malformed packets over (1) an Ethernet network or (2) a serial line.
CVE-2014-6079
Cross-site scripting (XSS) vulnerability in the Local Management Interface in IBM Security Access Manager for Web 7.x before 7.0.0-ISS-WGA-IF0009 and 8.x before 8.0.0-ISS-WGA-FP0005, and Security Access Manager for Mobile 8.x before 8.0.0-ISS-ISAM-FP0005, allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-6288
The powermail extension 2.x before 2.0.11 for TYPO3 allows remote attackers to bypass the CAPTCHA protection mechanism via unspecified vectors.
Per http://typo3.org/teams/security/security-bulletins/typo3-extensions/typo3-ext-sa-2014-006/, only version 2.0.0 - 2.0.10 are vulnerable.
CVE-2014-6289
The Ajax dispatcher for Extbase in the Yet Another Gallery (yag) extension before 3.0.1 and Tools for Extbase development (pt_extbase) extension before 1.5.1 allows remote attackers to bypass access restrictions and execute arbitrary controller actions via unspecified vectors.
CVE-2014-6290
The News (tt_news) extension before 3.5.2 for TYPO3 allows remote attackers to have unspecified impact via vectors related to an "insecure unserialize" issue.
CVE-2014-6291
Cross-site scripting (XSS) vulnerability in the Alphabetic Sitemap (alpha_sitemap) extension 0.0.3 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6292
The femanager extension before 1.0.9 for TYPO3 allows remote frontend users to modify or delete the records of other frontend users via unspecified vectors.
CVE-2014-6293
SQL injection vulnerability in the Statistics (ke_stats) extension before 1.1.2 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors, as exploited in the wild in February 2014.
CVE-2014-6294
Cross-site scripting (XSS) vulnerability in the External links click statistics (outstats) extension 0.0.3 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6295
SQL injection vulnerability in the WEC Map (wec_map) extension before 3.0.3 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-6296
Cross-site scripting (XSS) vulnerability in the WEC Map (wec_map) extension before 3.0.3 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6297
Cross-site scripting (XSS) vulnerability in the mm_forum extension before 1.9.3 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6298
Unrestricted file upload vulnerability in the mm_forum extension before 1.9.3 for TYPO3 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via unspecified vectors.
CVE-2014-6299
Cross-site request forgery (CSRF) vulnerability in the mm_forum extension before 1.9.3 for TYPO3 allows remote attackers to hijack the authentication of users for requests that create posts via unspecified vectors.
CVE-2014-6894
The Lucktastic (aka com.lucktastic.scratch) application 1.2.6 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6895
The Throne Rush (aka com.progrestar.bft) application 2.3.10 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6896
The Yik Yak (aka com.yik.yak) application 2.0.002 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6897
The Skyrim Map (aka com.neko.skyrimmap) application 2.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6898
The Boopsie MyLibrary (aka com.bredir.boopsie.mylibrary) application 4.5.110 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6899
The Jazeera Airways (aka com.winit.jazeeraairways) application 2.7 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6900
The EAGE Amsterdam 2014 (aka com.coreapps.android.followme.eage_2014) application 6.1.1.2 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6901
The RADIOS DEL ECUADOR (aka com.nobexinc.wls_87612622.rc) application 3.2.4 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6902
The Anjuke (aka com.anjuke.android.app) application 7.1.7 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6903
The Gulf Power Mobile Bill Pay (aka com.tionetworks.gulf) application 1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6905
The H2O Human Harmony Organization (aka com.netpia.ha.theh2o) application 1.6.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-7217
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 4.0.x before 4.0.10.4, 4.1.x before 4.1.14.5, and 4.2.x before 4.2.9.1 allow remote authenticated users to inject arbitrary web script or HTML via a crafted ENUM value that is improperly handled during rendering of the (1) table search or (2) table structure page, related to libraries/TableSearch.class.php and libraries/Util.class.php.
CVE-2014-7227
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187.  Reason: This candidate is a duplicate of CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, and CVE-2014-7187.  Notes: All CVE users should reference CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, and CVE-2014-7187 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
