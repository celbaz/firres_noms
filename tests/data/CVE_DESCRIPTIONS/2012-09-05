CVE-2010-4818
The GLX extension in X.Org xserver 1.7.7 allows remote authenticated users to cause a denial of service (server crash) and possibly execute arbitrary code via (1) a crafted request that triggers a client swap in glx/glxcmdsswap.c; or (2) a crafted length or (3) a negative value in the screen field in a request to glx/glxcmds.c.
CVE-2010-4819
The ProcRenderAddGlyphs function in the Render extension (render/render.c) in X.Org xserver 1.7.7 and earlier allows local users to read arbitrary memory and possibly cause a denial of service (server crash) via unspecified vectors related to an "input sanitization flaw."
CVE-2011-3146
librsvg before 2.34.1 uses the node name to identify the type of node, which allows context-dependent attackers to cause a denial of service (NULL pointer dereference) and possibly execute arbitrary code via a SVG file with a node with the element name starting with "fe," which is misidentified as a RsvgFilterPrimitive.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476 Null Pointer Dereference'

CVE-2011-4448
SQL injection vulnerability in actions/usersettings/usersettings.php in WikkaWiki 1.3.1 and 1.3.2 allows remote attackers to execute arbitrary SQL commands via the default_comment_display parameter in an update action.
CVE-2011-4449
actions/files/files.php in WikkaWiki 1.3.1 and 1.3.2, when INTRANET_MODE is enabled, supports file uploads for file extensions that are typically absent from an Apache HTTP Server TypesConfig file, which makes it easier for remote attackers to execute arbitrary PHP code by placing this code in a file whose name has multiple extensions, as demonstrated by a (1) .mm or (2) .vpp file.
CVE-2011-4450
Directory traversal vulnerability in handlers/files.xml/files.xml.php in WikkaWiki 1.3.1 and 1.3.2 allows remote attackers to read or delete arbitrary files via a non-initial .. (dot dot) in the file parameter, as demonstrated by the /../../wikka.config.php pathname in a download action.
CVE-2011-4451
** DISPUTED ** libs/Wakka.class.php in WikkaWiki 1.3.1 and 1.3.2, when the spam_logging option is enabled, allows remote attackers to write arbitrary PHP code to the spamlog_path file via the User-Agent HTTP header in an addcomment request.  NOTE: the vendor disputes this issue because the rendering of the spamlog_path file never uses the PHP interpreter.
CVE-2011-4452
Cross-site request forgery (CSRF) vulnerability in the AdminUsers component in WikkaWiki 1.3.1 and 1.3.2 allows remote attackers to hijack the authentication of administrators for requests that remove arbitrary user accounts via a delete operation, as demonstrated by an {{image}} action.
CVE-2012-2063
The Slidebox module before 7.x-1.4 for Drupal does not properly check permissions, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2012-2064
Cross-site scripting (XSS) vulnerability in theme/views_lang_switch.theme.inc in the Views Language Switcher module before 7.x-1.2 for Drupal allows remote attackers to inject arbitrary web script or HTML via the q parameter.
CVE-2012-2065
Cross-site scripting (XSS) vulnerability in the Language Icons module 6.x-2.x before 6.x-2.1 and 7.x-1.x before 7.x-1.0 for Drupal allows remote authenticated users with administer languages permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2066
Cross-site scripting (XSS) vulnerability in the FCKeditor module 6.x-2.x before 6.x-2.3 and the CKEditor module 6.x-1.x before 6.x-1.9 and 7.x-1.x before 7.x-1.7 for Drupal allows remote authenticated users or remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2067
Unspecified vulnerability in the CKeditor module 6.x-2.x before 6.x-2.3 and the CKEditor module 6.x-1.x before 6.x-1.9 and 7.x-1.x before 7.x-1.7 for Drupal, when the core PHP module is enabled, allows remote authenticated users or remote attackers to execute arbitrary PHP code via the text parameter to a text filter.  NOTE: some of these details are obtained from third party information.
Per http://drupal.org/node/1482528 the versions affected are "FCKeditor 6.x-2.x versions prior to 6.x-2.3, CKEditor 6.x-1.x versions prior to 6.x-1.9, and CKEditor 7.x-1.x versions prior to 7.x-1.7."

CVE-2012-2068
Multiple cross-site scripting (XSS) vulnerabilities in fancy_slide.module in the Fancy Slide module before 6.x-2.7 for Drupal allow remote authenticated users with the administer fancy_slide permission to inject arbitrary web script or HTML via the (1) node_title or (2) nodequeue_title parameter.
CVE-2012-3012
The Arbiter Power Sentinel 1133A device with firmware before 11Jun2012 Rev 421 allows remote attackers to cause a denial of service (Ethernet outage) via unspecified Ethernet traffic that fills a buffer, as demonstrated by a port scan.
CVE-2012-3509
Multiple integer overflows in the (1) _objalloc_alloc function in objalloc.c and (2) objalloc_alloc macro in include/objalloc.h in GNU libiberty, as used by binutils 2.22, allow remote attackers to cause a denial of service (crash) via vectors related to the "addition of CHUNK_HEADER_SIZE to the length," which triggers a heap-based buffer overflow.
CVE-2012-3526
The reverse proxy add forward module (mod_rpaf) 0.5 and 0.6 for the Apache HTTP Server allows remote attackers to cause a denial of service (server or application crash) via multiple X-Forwarded-For headers in a request.
CVE-2012-3527
view_help.php in the backend help system in TYPO3 4.5.x before 4.5.19, 4.6.x before 4.6.12 and 4.7.x before 4.7.4 allows remote authenticated backend users to unserialize arbitrary objects and possibly execute arbitrary PHP code via an unspecified parameter, related to a "missing signature (HMAC)."
CVE-2012-3528
Multiple cross-site scripting (XSS) vulnerabilities in the backend in TYPO3 4.5.x before 4.5.19, 4.6.x before 4.6.12 and 4.7.x before 4.7.4 allow remote authenticated backend users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-3529
The configuration module in the backend in TYPO3 4.5.x before 4.5.19, 4.6.x before 4.6.12 and 4.7.x before 4.7.4 allows remote authenticated backend users to obtain the encryption key via unspecified vectors.
CVE-2012-3530
Incomplete blacklist vulnerability in the t3lib_div::quoteJSvalue API function in TYPO3 4.5.x before 4.5.19, 4.6.x before 4.6.12 and 4.7.x before 4.7.4 allows remote attackers to conduct cross-site scripting (XSS) attacks via certain HTML5 JavaScript events.
Per: http://cwe.mitre.org/data/definitions/184.html

'CWE-184: Incomplete Blacklist'
CVE-2012-3531
Cross-site scripting (XSS) vulnerability in the Install Tool in TYPO3 4.5.x before 4.5.19, 4.6.x before 4.6.12 and 4.7.x before 4.7.4 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-3535
Heap-based buffer overflow in OpenJPEG 1.5.0 and earlier allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted JPEG2000 file.
CVE-2012-3537
The Crowbar Ohai plugin (chef/cookbooks/ohai/files/default/plugins/crowbar.rb) in the Deployer Barclamp in Crowbar, possibly 1.4 and earlier, allows local users to execute arbitrary shell commands via vectors related to "insecure handling of tmp files" and predictable file names.
CVE-2012-3540
Open redirect vulnerability in views/auth_forms.py in OpenStack Dashboard (Horizon) Essex (2012.1) allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the next parameter to auth/login/.  NOTE: this issue was originally assigned CVE-2012-3542 by mistake.
CVE-2012-3542
OpenStack Keystone, as used in OpenStack Folsom before folsom-rc1 and OpenStack Essex (2012.1), allows remote attackers to add an arbitrary user to an arbitrary tenant via a request to update the user's default tenant to the administrative API.  NOTE: this identifier was originally incorrectly assigned to an open redirect issue, but the correct identifier for that issue is CVE-2012-3540.
CVE-2012-3551
Cross-site scripting (XSS) vulnerability in crowbar_framework/app/views/support/index.html.haml in the Crowbar barclamp in Crowbar, possibly 1.4 and earlier, allows remote attackers to inject arbitrary web script or HTML via the file parameter to /utils.
CVE-2012-4386
The token check mechanism in Apache Struts 2.0.0 through 2.3.4 does not properly validate the token name configuration parameter, which allows remote attackers to perform cross-site request forgery (CSRF) attacks by setting the token name configuration parameter to a session attribute.
CVE-2012-4387
Apache Struts 2.0.0 through 2.3.4 allows remote attackers to cause a denial of service (CPU consumption) via a long parameter name, which is processed as an OGNL expression.
CVE-2012-4389
Incomplete blacklist vulnerability in lib/migrate.php in ownCloud before 4.0.7 allows remote attackers to execute arbitrary code by uploading a crafted .htaccess file in an import.zip file and accessing an uploaded PHP file.
Per: http://cwe.mitre.org/data/definitions/184.html

'CWE-184: Incomplete Blacklist'
CVE-2012-4390
(1) apps/calendar/appinfo/remote.php and (2) apps/contacts/appinfo/remote.php in ownCloud before 4.0.7 allows remote authenticated users to enumerate the registered users via unspecified vectors.
CVE-2012-4391
Cross-site request forgery (CSRF) vulnerability in core/ajax/appconfig.php in ownCloud before 4.0.7 allows remote attackers to hijack the authentication of administrators for requests that edit the app configurations.
CVE-2012-4392
index.php in ownCloud 4.0.7 does not properly validate the oc_token cookie, which allows remote attackers to bypass authentication via a crafted oc_token cookie value.
CVE-2012-4393
Multiple cross-site request forgery (CSRF) vulnerabilities in ownCloud before 4.0.6 allow remote attackers to hijack the authentication of arbitrary users for requests that use (1) addBookmark.php, (2) delBookmark.php, or (3) editBookmark.php in bookmarks/ajax/; (4) calendar/delete.php, (5) calendar/edit.php, (6) calendar/new.php, (7) calendar/update.php, (8) event/delete.php, (9) event/edit.php, (10) event/move.php, (11) event/new.php, (12) import/import.php, (13) settings/setfirstday.php, (14) settings/settimeformat.php, (15) share/changepermission.php, (16) share/share.php, (17) or share/unshare.php in calendar/ajax/; (18) external/ajax/setsites.php, (19) files/ajax/delete.php, (20) files/ajax/move.php, (21) files/ajax/newfile.php, (22) files/ajax/newfolder.php, (23) files/ajax/rename.php, (24) files_sharing/ajax/email.php, (25) files_sharing/ajax/setpermissions.php, (26) files_sharing/ajax/share.php, (27) files_sharing/ajax/toggleresharing.php, (28) files_sharing/ajax/togglesharewitheveryone.php, (29) files_sharing/ajax/unshare.php, (30) files_texteditor/ajax/savefile.php, (31) files_versions/ajax/rollbackVersion.php, (32) gallery/ajax/createAlbum.php, (33) gallery/ajax/sharing.php, (34) tasks/ajax/addtask.php, (35) tasks/ajax/addtaskform.php, (36) tasks/ajax/delete.php, or (37) tasks/ajax/edittask.php in apps/; or administrators for requests that use (38) changepassword.php, (39) creategroup.php, (40) createuser.php, (41) disableapp.php, (42) enableapp.php, (43) lostpassword.php, (44) removegroup.php, (45) removeuser.php, (46) setlanguage.php, (47) setloglevel.php, (48) setquota.php, or (49) togglegroups.php in settings/ajax/.
CVE-2012-4394
Cross-site scripting (XSS) vulnerability in apps/files/js/filelist.js in ownCloud before 4.0.5 allows remote attackers to inject arbitrary web script or HTML via the file parameter.
CVE-2012-4395
Cross-site scripting (XSS) vulnerability in index.php in ownCloud before 4.0.3 allows remote attackers to inject arbitrary web script or HTML via the redirect_url parameter.
CVE-2012-4396
Multiple cross-site scripting (XSS) vulnerabilities in ownCloud before 4.0.2 allow remote attackers to inject arbitrary web script or HTML via the (1) file names to apps/user_ldap/settings.php; (2) url or (3) title parameter to apps/bookmarks/ajax/editBookmark.php; (4) tag or (5) page parameter to apps/bookmarks/ajax/updateList.php; (6) identity to apps/user_openid/settings.php; (7) stack name in apps/gallery/lib/tiles.php; (8) root parameter to apps/gallery/templates/index.php; (9) calendar displayname in apps/calendar/templates/part.import.php; (10) calendar uri in apps/calendar/templates/part.choosecalendar.rowfields.php; (11) title, (12) location, or (13) description parameter in apps/calendar/lib/object.php; (14) certain vectors in core/js/multiselect.js; or (15) artist, (16) album, or (17) title comments parameter in apps/media/lib_scanner.php.
CVE-2012-4397
Multiple cross-site scripting (XSS) vulnerabilities in ownCloud before 4.0.1 allow remote attackers to inject arbitrary web script or HTML via the (1) calendar displayname to part.choosecalendar.rowfields.php or (2) part.choosecalendar.rowfields.shared.php in apps/calendar/templates/; or (3) unspecified vectors to apps/contacts/lib/vcard.php.
CVE-2012-4752
appconfig.php in ownCloud before 4.0.6 does not properly restrict access, which allows remote authenticated users to edit app configurations via unspecified vectors.  NOTE: this can be leveraged by unauthenticated remote attackers using CVE-2012-4393.
CVE-2012-4753
Multiple cross-site request forgery (CSRF) vulnerabilities in ownCloud before 4.0.5 allow remote attackers to hijack the authentication of unspecified victims via unknown vectors.
