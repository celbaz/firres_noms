CVE-2013-5391
IBM Worklight Consumer and Enterprise Editions 5.0.x before 5.0.6 Fix Pack 2 and 6.0.x before 6.0.0 Fix Pack 2, and Mobile Foundation Consumer and Enterprise Editions 5.0.x before 5.0.6 Fix Pack 2 and 6.0.0 Fix Pack 2 make it easier for attackers to defeat cryptographic protection mechanisms by leveraging improper initialization of the pseudo random number generator (PRNG) in Android and use of the Java Cryptography Architecture (JCA) by a Worklight program. IBM X-Force ID: 87128.
CVE-2013-5461
IBM Endpoint Manager for Remote Control 9.0.0 and 9.0.1 and Tivoli Remote Control 5.1.2 store multiple hashes of partial passwords, which makes it easier for remote attackers to decrypt passwords by leveraging access to the hashes. IBM X-Force ID: 88309.
CVE-2013-6739
IBM SPSS Modeler before 16 on UNIX allows remote authenticated users to bypass intended access restrictions via an SSO token. IBM X-Force ID: 89855.
CVE-2013-7201
WebHybridClient.java in PayPal 5.3 and earlier for Android ignores SSL errors, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information.
CVE-2013-7202
The WebHybridClient class in PayPal 5.3 and earlier for Android allows remote attackers to execute arbitrary JavaScript on the system.
CVE-2014-0841
IBM Rational Focal Point 6.4.0, 6.4.1, 6.5.1, 6.5.2, and 6.6.0 use a weak algorithm to hash passwords, which makes it easier for context-dependent attackers to obtain cleartext values via a brute-force attack. IBM X-Force ID: 90704.
CVE-2014-1845
An unspecified setuid root helper in Enlightenment before 0.17.6 allows local users to gain privileges by leveraging failure to properly sanitize the environment.
CVE-2014-1846
Enlightenment before 0.17.6 might allow local users to gain privileges via vectors involving the gdb method.
CVE-2014-2552
Brookins Consulting (BC) Collected Information Export extension for eZ Publish 1.1.0 does not properly restrict access, which allows remote attackers to gain access to sensitive data.
CVE-2015-1857
The odl-mdsal-apidocs feature in OpenDaylight Helium allow remote attackers to obtain sensitive information by leveraging missing AAA restrictions.
CVE-2017-1116
IBM Campaign 8.6, 9.0, 9.1, 9.1.1, 9.1.2, and 10.0 contains excessive details on the client side which could provide information useful for an authenticated user to conduct other attacks. IBM X-Force ID: 121154.
CVE-2018-10469
b3log Symphony (aka Sym) 2.6.0 allows remote attackers to upload and execute arbitrary JSP files via the name[] parameter to the /upload URI.
CVE-2018-10471
An issue was discovered in Xen through 4.10.x allowing x86 PV guest OS users to cause a denial of service (out-of-bounds zero write and hypervisor crash) via unexpected INT 80 processing, because of an incorrect fix for CVE-2017-5754.
CVE-2018-10472
An issue was discovered in Xen through 4.10.x allowing x86 HVM guest OS users (in certain configurations) to read arbitrary dom0 files via QMP live insertion of a CDROM, in conjunction with specifying the target file as the backing file of a snapshot.
CVE-2018-10503
An issue was discovered in index.php in baijiacms V4 v4_1_4_20170105. CSRF allows adding an administrator account via op=edituser, changing the administrator password via op=changepwd, or deleting an account via op=deleteuser.
CVE-2018-10504
The WebDorado "Form Maker by WD" plugin before 1.12.24 for WordPress allows CSV injection.
CVE-2018-10515
In CMS Made Simple (CMSMS) through 2.2.7, the "file unpack" operation in the admin dashboard contains a remote code execution vulnerability exploitable by an admin user because a .php file can be present in the extracted ZIP archive.
CVE-2018-10516
In CMS Made Simple (CMSMS) through 2.2.7, the "file rename" operation in the admin dashboard contains a sensitive information disclosure vulnerability, exploitable by an admin user, that can cause DoS by moving config.php to the upload/ directory.
CVE-2018-10517
In CMS Made Simple (CMSMS) through 2.2.7, the "module import" operation in the admin dashboard contains a remote code execution vulnerability, exploitable by an admin user, because an XML Package can contain base64-encoded PHP code in a data element.
CVE-2018-10518
In CMS Made Simple (CMSMS) through 2.2.7, the "file delete" operation in the admin dashboard contains an arbitrary file deletion vulnerability that can cause DoS, exploitable by an admin user, because the attacker can remove all lib/ files in all directories.
CVE-2018-10519
CMS Made Simple (CMSMS) 2.2.7 contains a privilege escalation vulnerability from ordinary user to admin user by arranging for the eff_uid value within $_COOKIE[$this->_loginkey] to equal 1, because files in the tmp/ directory are accessible through HTTP requests. NOTE: this vulnerability exists because of an incorrect fix for CVE-2018-10084.
CVE-2018-10520
In CMS Made Simple (CMSMS) through 2.2.7, the "module remove" operation in the admin dashboard contains an arbitrary file deletion vulnerability that can cause DoS, exploitable by an admin user, because the attacker can remove all lib/ files in all directories.
CVE-2018-10521
In CMS Made Simple (CMSMS) through 2.2.7, the "file move" operation in the admin dashboard contains an arbitrary file movement vulnerability that can cause DoS, exploitable by an admin user, because config.php can be moved into an incorrect directory.
CVE-2018-10522
In CMS Made Simple (CMSMS) through 2.2.7, the "file view" operation in the admin dashboard contains a sensitive information disclosure vulnerability, exploitable by ordinary users, because the product exposes unrestricted access to the PHP file_get_contents function.
CVE-2018-10523
CMS Made Simple (CMSMS) through 2.2.7 contains a physical path leakage Vulnerability via /modules/DesignManager/action.ajax_get_templates.php, /modules/DesignManager/action.ajax_get_stylesheets.php, /modules/FileManager/dunzip.php, or /modules/FileManager/untgz.php.
CVE-2018-1471
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-1473
IBM BigFix Platform 9.2 and 9.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 140691.
CVE-2018-1475
IBM BigFix Platform 9.2 and 9.5 uses an inadequate account lockout setting that could allow a remote attacker to brute force account credentials. IBM X-Force ID: 140756.
CVE-2018-1479
IBM BigFix Platform 9.2 and 9.5 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 140761.
CVE-2018-7669
An issue was discovered in Sitecore Sitecore.NET 8.1 rev. 151207 Hotfix 141178-1 and above. The 'Log Viewer' application is vulnerable to a directory traversal attack, allowing an attacker to access arbitrary files from the host Operating System using a sitecore/shell/default.aspx?xmlcontrol=LogViewerDetails&file= URI. Validation is performed to ensure that the text passed to the 'file' parameter correlates to the correct log file directory. This filter can be bypassed by including a valid log filename and then appending a traditional 'dot dot' style attack.
