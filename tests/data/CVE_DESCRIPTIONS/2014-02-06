CVE-2010-4226
cpio, as used in build 2007.05.10, 2010.07.28, and possibly other versions, allows remote attackers to overwrite arbitrary files via a symlink within an RPM package archive.
CVE-2012-1095
osc before 0.134 might allow remote OBS repository servers or package maintainers to execute arbitrary commands via a crafted (1) build log or (2) build status that contains an escape sequence for a terminal emulator.
CVE-2012-6152
The Yahoo! protocol plugin in libpurple in Pidgin before 2.10.8 does not properly validate UTF-8 data, which allows remote attackers to cause a denial of service (application crash) via crafted byte sequences.
CVE-2013-2038
The NMEA0183 driver in gpsd before 3.9 allows remote attackers to cause a denial of service (daemon termination) and possibly execute arbitrary code via a GPS packet with a malformed $GPGGA interpreted sentence that lacks certain fields and a terminator.  NOTE: a separate issue in the AIS driver was also reported, but it might not be a vulnerability.
CVE-2013-2962
Buffer overflow in the Launcher in IBM WebSphere Transformation Extender 8.4.x before 8.4.0.4 allows local users to cause a denial of service (process crash or Admin Console command-stream outage) via unspecified vectors.
CVE-2013-4463
OpenStack Compute (Nova) Folsom, Grizzly, and Havana does not properly verify the virtual size of a QCOW2 image, which allows local users to cause a denial of service (host file system disk consumption) via a compressed QCOW2 image.  NOTE: this issue is due to an incomplete fix for CVE-2013-2096.
CVE-2013-5983
Multiple cross-site scripting (XSS) vulnerabilities in GuppY before 4.6.28 allow remote attackers to inject arbitrary web script or HTML via the (1) "an" parameter to agenda.php or (2) cat parameter to mobile/thread.php.
CVE-2013-6332
Unrestricted file upload vulnerability in IBM Algo One UDS 4.7.0 through 5.0.0 allows remote authenticated users to execute arbitrary code by uploading a .jsp file and then launching it.
Per: http://cwe.mitre.org/data/definitions/434.html

"CWE-434: Unrestricted Upload of File with Dangerous Type"
CVE-2013-6393
The yaml_parser_scan_tag_uri function in scanner.c in LibYAML before 0.1.5 performs an incorrect cast, which allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via crafted tags in a YAML document, which triggers a heap-based buffer overflow.
CVE-2013-6477
Multiple integer signedness errors in libpurple in Pidgin before 2.10.8 allow remote attackers to cause a denial of service (application crash) via a crafted timestamp value in an XMPP message.
CVE-2013-6478
gtkimhtml.c in Pidgin before 2.10.8 does not properly interact with underlying library support for wide Pango layouts, which allows user-assisted remote attackers to cause a denial of service (application crash) via a long URL that is examined with a tooltip.
CVE-2013-6479
util.c in libpurple in Pidgin before 2.10.8 does not properly allocate memory for HTTP responses that are inconsistent with the Content-Length header, which allows remote HTTP servers to cause a denial of service (application crash) via a crafted response.
CVE-2013-6481
libpurple/protocols/yahoo/libymsg.c in Pidgin before 2.10.8 allows remote attackers to cause a denial of service (crash) via a Yahoo! P2P message with a crafted length field, which triggers a buffer over-read.
CVE-2013-6482
Pidgin before 2.10.8 allows remote MSN servers to cause a denial of service (NULL pointer dereference and crash) via a crafted (1) SOAP response, (2) OIM XML response, or (3) Content-Length header.
CVE-2013-6483
The XMPP protocol plugin in libpurple in Pidgin before 2.10.8 does not properly determine whether the from address in an iq reply is consistent with the to address in an iq request, which allows remote attackers to spoof iq traffic or cause a denial of service (NULL pointer dereference and application crash) via a crafted reply.
CVE-2013-6484
The STUN protocol implementation in libpurple in Pidgin before 2.10.8 allows remote STUN servers to cause a denial of service (out-of-bounds write operation and application crash) by triggering a socket read error.
CVE-2013-6485
Buffer overflow in util.c in libpurple in Pidgin before 2.10.8 allows remote HTTP servers to cause a denial of service (application crash) or possibly have unspecified other impact via an invalid chunk-size field in chunked transfer-coding data.
CVE-2013-6486
gtkutils.c in Pidgin before 2.10.8 on Windows allows user-assisted remote attackers to execute arbitrary programs via a message containing a file: URL that is improperly handled during construction of an explorer.exe command.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2011-3185.
CVE-2013-6487
Integer overflow in libpurple/protocols/gg/lib/http.c in the Gadu-Gadu (gg) parser in Pidgin before 2.10.8 allows remote attackers to have an unspecified impact via a large Content-Length value, which triggers a buffer overflow.
CVE-2013-6489
Integer signedness error in the MXit functionality in Pidgin before 2.10.8 allows remote attackers to cause a denial of service (segmentation fault) via a crafted emoticon value, which triggers an integer overflow and a buffer overflow.
CVE-2013-6490
The SIMPLE protocol functionality in Pidgin before 2.10.8 allows remote attackers to have an unspecified impact via a negative Content-Length header, which triggers a buffer overflow.
CVE-2013-7130
The i_create_images_and_backing (aka create_images_and_backing) method in libvirt driver in OpenStack Compute (Nova) Grizzly, Havana, and Icehouse, when using KVM live block migration, does not properly create all expected files, which allows attackers to obtain snapshot root disk contents of other users via ephemeral storage.
CVE-2013-7319
Cross-site scripting (XSS) vulnerability in the Download Manager plugin before 2.5.9 for WordPress allows remote attackers to inject arbitrary web script or HTML via the title field.
CVE-2013-7320
Cross-site request forgery (CSRF) vulnerability in D-Link DAP-2253 Access Point (Rev. A1) with firmware before 1.30 allows remote attackers to hijack the authentication of administrators for requests that modify configuration settings via unspecified vectors.
CVE-2013-7321
Cross-site scripting (XSS) vulnerability in D-Link DAP-2253 Access Point (Rev. A1) with firmware before 1.30 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-0020
The IRC protocol plugin in libpurple in Pidgin before 2.10.8 does not validate argument counts, which allows remote IRC servers to cause a denial of service (application crash) via a crafted message.
CVE-2014-0038
The compat_sys_recvmmsg function in net/compat.c in the Linux kernel before 3.13.2, when CONFIG_X86_X32 is enabled, allows local users to gain privileges via a recvmmsg system call with a crafted timeout pointer parameter.
CVE-2014-0330
Cross-site scripting (XSS) vulnerability in adminui/user_list.php on the Dell KACE K1000 management appliance 5.5.90545 allows remote attackers to inject arbitrary web script or HTML via the LABEL_ID parameter.
CVE-2014-0622
The web service in EMC Documentum Foundation Services (DFS) 6.5 through 6.7 before 6.7 SP1 P22, 6.7 SP2 before P08, 7.0 before P12, and 7.1 before P01 does not properly implement content uploading, which allows remote authenticated users to bypass intended content access restrictions via unspecified vectors.
CVE-2014-0815
The intent: URL implementation in Opera before 18 on Android allows attackers to read local files by leveraging an interaction error, as demonstrated by reading stored cookies.
CVE-2014-0822
The IMAP server in IBM Domino 8.5.x before 8.5.3 FP6 IF1 and 9.0.x before 9.0.1 FP1 allows remote attackers to cause a denial of service (daemon crash) via unspecified vectors, aka SPR KLYH9F4S2Z.
CVE-2014-1477
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2014-1478
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 27.0 and SeaMonkey before 2.24 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via vectors related to the MPostWriteBarrier class in js/src/jit/MIR.h and stack alignment in js/src/jit/AsmJS.cpp in OdinMonkey, and unknown other vectors.
CVE-2014-1479
The System Only Wrapper (SOW) implementation in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 does not prevent certain cloning operations, which allows remote attackers to bypass intended restrictions on XUL content via vectors involving XBL content scopes.
CVE-2014-1480
The file-download implementation in Mozilla Firefox before 27.0 and SeaMonkey before 2.24 does not properly restrict the timing of button selections, which allows remote attackers to conduct clickjacking attacks, and trigger unintended launching of a downloaded file, via a crafted web site.
CVE-2014-1481
Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 allow remote attackers to bypass intended restrictions on window objects by leveraging inconsistency in native getter methods across different JavaScript engines.
CVE-2014-1482
RasterImage.cpp in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 does not prevent access to discarded data, which allows remote attackers to execute arbitrary code or cause a denial of service (incorrect write operations) via crafted image data, as demonstrated by Goo Create.
CVE-2014-1483
Mozilla Firefox before 27.0 and SeaMonkey before 2.24 allow remote attackers to bypass the Same Origin Policy and obtain sensitive information by using an IFRAME element in conjunction with certain timing measurements involving the document.caretPositionFromPoint and document.elementFromPoint functions.
CVE-2014-1484
Mozilla Firefox before 27.0 on Android 4.2 and earlier creates system-log entries containing profile paths, which allows attackers to obtain sensitive information via a crafted application.
CVE-2014-1485
The Content Security Policy (CSP) implementation in Mozilla Firefox before 27.0 and SeaMonkey before 2.24 operates on XSLT stylesheets according to style-src directives instead of script-src directives, which might allow remote attackers to execute arbitrary XSLT code by leveraging insufficient style-src restrictions.
CVE-2014-1486
Use-after-free vulnerability in the imgRequestProxy function in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 allows remote attackers to execute arbitrary code via vectors involving unspecified Content-Type values for image data.
CVE-2014-1487
The Web workers implementation in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, and SeaMonkey before 2.24 allows remote attackers to bypass the Same Origin Policy and obtain sensitive authentication information via vectors involving error messages.
CVE-2014-1488
The Web workers implementation in Mozilla Firefox before 27.0 and SeaMonkey before 2.24 allows remote attackers to execute arbitrary code via vectors involving termination of a worker process that has performed a cross-thread object-passing operation in conjunction with use of asm.js.
CVE-2014-1489
Mozilla Firefox before 27.0 does not properly restrict access to about:home buttons by script on other pages, which allows user-assisted remote attackers to cause a denial of service (session restore) via a crafted web site.
CVE-2014-1490
Race condition in libssl in Mozilla Network Security Services (NSS) before 3.15.4, as used in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, SeaMonkey before 2.24, and other products, allows remote attackers to cause a denial of service (use-after-free) or possibly have unspecified other impact via vectors involving a resumption handshake that triggers incorrect replacement of a session ticket.
CVE-2014-1491
Mozilla Network Security Services (NSS) before 3.15.4, as used in Mozilla Firefox before 27.0, Firefox ESR 24.x before 24.3, Thunderbird before 24.3, SeaMonkey before 2.24, and other products, does not properly restrict public values in Diffie-Hellman key exchanges, which makes it easier for remote attackers to bypass cryptographic protection mechanisms in ticket handling by leveraging use of a certain value.
CVE-2014-1663
Unspecified vulnerability in Citrix XenMobile Device Manager server (formerly Zenprise Device Manager server) 8.5, 8.6, and MDM 8.0.1 allows remote attackers to obtain sensitive information via unknown vectors.
CVE-2014-1870
Opera before 19 on Mac OS X allows user-assisted remote attackers to spoof the address bar via vectors involving a drag-and-drop operation.
