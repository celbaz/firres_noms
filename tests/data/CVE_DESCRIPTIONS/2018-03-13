CVE-2016-9575
Ipa versions 4.2.x, 4.3.x before 4.3.3 and 4.4.x before 4.4.3 did not properly check the user's permissions while modifying certificate profiles in IdM's certprofile-mod command. An authenticated, unprivileged attacker could use this flaw to modify profiles to issue certificates with arbitrary naming or key usage information and subsequently use such certificates for other attacks.
CVE-2017-1002101
In Kubernetes versions 1.3.x, 1.4.x, 1.5.x, 1.6.x and prior to versions 1.7.14, 1.8.9 and 1.9.4 containers using subpath volume mounts with any volume type (including non-privileged pods, subject to file permissions) can access files/directories outside of the volume, including the host's filesystem.
CVE-2017-1002102
In Kubernetes versions 1.3.x, 1.4.x, 1.5.x, 1.6.x and prior to versions 1.7.14, 1.8.9 and 1.9.4 containers using a secret, configMap, projected or downwardAPI volume can trigger deletion of arbitrary files/directories from the nodes where they are running.
CVE-2017-16250
A vulnerability in Mitel ST 14.2, release GA28 and earlier, could allow an attacker to use the API function to enumerate through user-ids which could be used to identify valid user ids and associated user names.
CVE-2017-16251
A vulnerability in the conferencing component of Mitel ST 14.2, release GA28 and earlier, could allow an authenticated user to upload a malicious script to the Personal Library by a crafted POST request. Successful exploit could allow an attacker to execute arbitrary code within the context of the application.
CVE-2017-17442
In BlackBerry UEM Management Console version 12.7.1 and earlier, a reflected cross-site scripting vulnerability that could allow an attacker to execute script commands in the context of the affected UEM Management Console account by crafting a malicious link and then persuading a user with legitimate access to the Management Console to click on the malicious link.
CVE-2018-1000069
FreePlane version 1.5.9 and earlier contains a XML External Entity (XXE) vulnerability in XML Parser in mindmap loader that can result in stealing data from victim's machine. This attack appears to require the victim to open a specially crafted mind map file. This vulnerability appears to have been fixed in 1.6+.
CVE-2018-1000070
Bitmessage PyBitmessage version v0.6.2 (and introduced in or after commit 8ce72d8d2d25973b7064b1cf76a6b0b3d62f0ba0) contains a Eval injection vulnerability in main program, file src/messagetypes/__init__.py function constructObject that can result in Code Execution. This attack appears to be exploitable via remote attacker using a malformed message which must be processed by the victim - e.g. arrive from any sender on bitmessage network. This vulnerability appears to have been fixed in v0.6.3.
CVE-2018-1000071
roundcube version 1.3.4 and earlier contains an Insecure Permissions vulnerability in enigma plugin that can result in exfiltration of gpg private key. This attack appear to be exploitable via network connectivity.
CVE-2018-1000072
iRedMail version prior to commit f04b8ef contains a Insecure Permissions vulnerability in Roundcube Webmail that can result in Exfiltrate a user's password protected secret GPG key file and other important configuration files.. This attack appear to be exploitable via network connectivity. This vulnerability appears to have been fixed in Beta: 0.9.8-BETA1, Stable: 0.9.7.
CVE-2018-1000073
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Directory Traversal vulnerability in install_location function of package.rb that can result in path traversal when writing to a symlinked basedir outside of the root. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000074
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Deserialization of Untrusted Data vulnerability in owner command that can result in code execution. This attack appear to be exploitable via victim must run the `gem owner` command on a gem with a specially crafted YAML file. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000075
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a infinite loop caused by negative size vulnerability in ruby gem package tar header that can result in a negative size could cause an infinite loop.. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000076
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Improper Verification of Cryptographic Signature vulnerability in package.rb that can result in a mis-signed gem could be installed, as the tarball would contain multiple gem signatures.. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000077
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Improper Input Validation vulnerability in ruby gems specification homepage attribute that can result in a malicious gem could set an invalid homepage URL. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000078
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Cross Site Scripting (XSS) vulnerability in gem server display of homepage attribute that can result in XSS. This attack appear to be exploitable via the victim must browse to a malicious gem on a vulnerable gem server. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000079
RubyGems version Ruby 2.2 series: 2.2.9 and earlier, Ruby 2.3 series: 2.3.6 and earlier, Ruby 2.4 series: 2.4.3 and earlier, Ruby 2.5 series: 2.5.0 and earlier, prior to trunk revision 62422 contains a Directory Traversal vulnerability in gem installation that can result in the gem could write to arbitrary filesystem locations during installation. This attack appear to be exploitable via the victim must install a malicious gem. This vulnerability appears to have been fixed in 2.7.6.
CVE-2018-1000080
Ajenti version version 2 contains a Insecure Permissions vulnerability in Plugins download that can result in The download of any plugins as being a normal user. This attack appear to be exploitable via By knowing how the requisition is made, and sending it as a normal user, the server, in response, downloads the plugin.
CVE-2018-1000081
Ajenti version version 2 contains a Input Validation vulnerability in ID string on Get-values POST request that can result in Server Crashing. This attack appear to be exploitable via An attacker can freeze te server by sending a giant string to the ID parameter ..
CVE-2018-1000082
Ajenti version version 2 contains a Cross ite Request Forgery (CSRF) vulnerability in the command execution panel of the tool used to manage the server. that can result in Code execution on the server . This attack appear to be exploitable via Being a CSRF, victim interaction is needed, when the victim access the infected trigger of the CSRF any code that match the victim privledges on the server can be executed..
CVE-2018-1000083
Ajenti version version 2 contains a Improper Error Handling vulnerability in Login JSON request that can result in The requisition leaks a path of the server. This attack appear to be exploitable via By sending a malformed JSON, the tool responds with a traceback error that leaks a path of the server.
CVE-2018-1000084
WOlfCMS WolfCMS version version 0.8.3.1 contains a Stored Cross-Site Scripting vulnerability in Layout Name (from Layout tab) that can result in low privilege user can steal the cookie of admin user and compromise the admin account. This attack appear to be exploitable via Need to enter the Javascript code into Layout Name .
CVE-2018-1000085
ClamAV version version 0.99.3 contains a Out of bounds heap memory read vulnerability in XAR parser, function xar_hash_check() that can result in Leaking of memory, may help in developing exploit chains.. This attack appear to be exploitable via The victim must scan a crafted XAR file. This vulnerability appears to have been fixed in after commit d96a6b8bcc7439fa7e3876207aa0a8e79c8451b6.
CVE-2018-1000086
NPR Visuals Team Pym.js version versions 0.4.2 up to 1.3.1 contains a Cross ite Request Forgery (CSRF) vulnerability in Pym.js _onNavigateToMessage function. https://github.com/nprapps/pym.js/blob/master/src/pym.js#L573 that can result in Arbitrary javascript code execution. This attack appear to be exploitable via Attacker gains full javascript access to pages with Pym.js embeds when user visits an attacker crafted page.. This vulnerability appears to have been fixed in versions 1.3.2 and later.
CVE-2018-1000087
WolfCMS version version 0.8.3.1 contains a Reflected Cross Site Scripting vulnerability in "Create New File" and "Create New Directory" input box from 'files' Tab that can result in Session Hijacking, Spread Worms,Control the browser remotely. . This attack appear to be exploitable via Attacker can execute the JavaScript into the "Create New File" and "Create New Directory" input box from 'files'.
CVE-2018-1000088
Doorkeeper version 2.1.0 through 4.2.5 contains a Cross Site Scripting (XSS) vulnerability in web view's OAuth app form, user authorization prompt web view that can result in Stored XSS on the OAuth Client's name will cause users interacting with it will execute payload. This attack appear to be exploitable via The victim must be tricked to click an opaque link to the web view that runs the XSS payload. A malicious version virtually indistinguishable from a normal link.. This vulnerability appears to have been fixed in 4.2.6, 4.3.0.
CVE-2018-1000089
Anymail django-anymail version version 0.2 through 1.3 contains a CWE-532, CWE-209 vulnerability in WEBHOOK_AUTHORIZATION setting value that can result in An attacker with access to error logs could fabricate email tracking events. This attack appear to be exploitable via If you have exposed your Django error reports, an attacker could discover your ANYMAIL_WEBHOOK setting and use this to post fabricated or malicious Anymail tracking/inbound events to your app. This vulnerability appears to have been fixed in v1.4.
CVE-2018-1000090
textpattern version version 4.6.2 contains a XML Injection vulnerability in Import XML feature that can result in Denial of service in context to the web server by exhausting server memory resources. This attack appear to be exploitable via Uploading a specially crafted XML file.
CVE-2018-1000091
KadNode version version 2.2.0 contains a Buffer Overflow vulnerability in Arguments when starting up the binary that can result in Control of program execution flow, leading to remote code execution.
CVE-2018-1000092
CMS Made Simple version versions 2.2.5 contains a Cross ite Request Forgery (CSRF) vulnerability in Admin profile page that can result in Details can be found here http://dev.cmsmadesimple.org/bug/view/11715. This attack appear to be exploitable via A specially crafted web page. This vulnerability appears to have been fixed in 2.2.6.
CVE-2018-1000093
CryptoNote version version 0.8.9 and possibly later contain a local RPC server which does not require authentication, as a result the walletd and the simplewallet RPC daemons will process any commands sent to them, resulting in remote command execution and a takeover of the cryptocurrency wallet if an attacker can trick an application such as a web browser into connecting and sending a command for example. This attack appears to be exploitable via a victim visiting a webpage hosting malicious content that trigger such behavior.
CVE-2018-1000094
CMS Made Simple version 2.2.5 contains a Remote Code Execution vulnerability in File Manager that can result in Allows an authenticated admin that has access to the file manager to execute code on the server. This attack appear to be exploitable via File upload -> copy to any extension.
CVE-2018-1000095
oVirt version 4.2.0 to 4.2.2 contains a Cross Site Scripting (XSS) vulnerability in the name/description of VMs portion of the web admin application. This vulnerability appears to have been fixed in version 4.2.3.
CVE-2018-1000096
brianleroux tiny-json-http version all versions since commit 9b8e74a232bba4701844e07bcba794173b0238a8 (Oct 29 2016) contains a Missing SSL certificate validation vulnerability in The libraries core functionality is affected. that can result in Exposes the user to man-in-the-middle attacks.
CVE-2018-1000097
Sharutils sharutils (unshar command) version 4.15.2 contains a Buffer Overflow vulnerability in Affected component on the file unshar.c at line 75, function looks_like_c_code. Failure to perform checking of the buffer containing input line. that can result in Could lead to code execution. This attack appear to be exploitable via Victim have to run unshar command on a specially crafted file..
CVE-2018-1000098
Teluu PJSIP version 2.7.1 and earlier contains a Integer Overflow vulnerability in pjmedia SDP parsing that can result in Crash. This attack appear to be exploitable via Sending a specially crafted message. This vulnerability appears to have been fixed in 2.7.2.
CVE-2018-1000099
Teluu PJSIP version 2.7.1 and earlier contains a Access of Null/Uninitialized Pointer vulnerability in pjmedia SDP parsing that can result in Crash. This attack appear to be exploitable via Sending a specially crafted message. This vulnerability appears to have been fixed in 2.7.2.
CVE-2018-1000102
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2018-1000067. Reason: This candidate is a reservation duplicate of CVE-2018-1000067. Notes: All CVE users should reference CVE-2018-1000067 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-1000103
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2018-1000068. Reason: This candidate is a reservation duplicate of CVE-2018-1000068. Notes: All CVE users should reference CVE-2018-1000068 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-1000104
A plaintext storage of a password vulnerability exists in Jenkins Coverity Plugin 1.10.0 and earlier in CIMInstance.java that allows an attacker with local file system access or control of a Jenkins administrator's web browser (e.g. malicious extension) to retrieve the configured keystore and private key passwords.
CVE-2018-1000105
An improper authorization vulnerability exists in Jenkins Gerrit Trigger Plugin 2.27.4 and earlier in GerritManagement.java, GerritServer.java, and PluginImpl.java that allows an attacker with Overall/Read access to retrieve some configuration information about Gerrit in Jenkins.
CVE-2018-1000106
An improper authorization vulnerability exists in Jenkins Gerrit Trigger Plugin 2.27.4 and earlier in GerritManagement.java, GerritServer.java, and PluginImpl.java that allows an attacker with Overall/Read access to modify the Gerrit configuration in Jenkins.
CVE-2018-1000107
An improper authorization vulnerability exists in Jenkins Job and Node Ownership Plugin 0.11.0 and earlier in OwnershipDescription.java, JobOwnerJobProperty.java, and OwnerNodeProperty.java that allow an attacker with Job/Configure or Computer/Configure permission and without Ownership related permissions to override ownership metadata.
CVE-2018-1000108
A cross-site scripting vulnerability exists in Jenkins CppNCSS Plugin 1.1 and earlier in AbstractProjectAction/index.jelly that allow an attacker to craft links to Jenkins URLs that run arbitrary JavaScript in the user's browser when accessed.
CVE-2018-1000109
An improper authorization vulnerability exists in Jenkins Google Play Android Publisher Plugin version 1.6 and earlier in GooglePlayBuildStepDescriptor.java that allow an attacker to obtain credential IDs.
CVE-2018-1000110
An improper authorization vulnerability exists in Jenkins Git Plugin version 3.7.0 and earlier in GitStatus.java that allows an attacker with network access to obtain a list of nodes and users.
CVE-2018-1000111
An improper authorization vulnerability exists in Jenkins Subversion Plugin version 2.10.2 and earlier in SubversionStatus.java and SubversionRepositoryStatus.java that allows an attacker with network access to obtain a list of nodes and users.
CVE-2018-1000112
An improper authorization vulnerability exists in Jenkins Mercurial Plugin version 2.2 and earlier in MercurialStatus.java that allows an attacker with network access to obtain a list of nodes and users.
CVE-2018-1000113
A cross-site scripting vulnerability exists in Jenkins TestLink Plugin 2.12 and earlier in TestLinkBuildAction/summary.jelly and others that allow an attacker who can control e.g. TestLink report names to have Jenkins serve arbitrary HTML and JavaScript
CVE-2018-1000114
An improper authorization vulnerability exists in Jenkins Promoted Builds Plugin 2.31.1 and earlier in Status.java and ManualCondition.java that allow an attacker with read access to jobs to perform promotions.
CVE-2018-1000123
Ionic Team Cordova plugin iOS Keychain version before commit 18233ca25dfa92cca018b9c0935f43f78fd77fbf contains an Information Exposure Through Log Files (CWE-532) vulnerability in CDVKeychain.m that can result in login, password and other sensitive data leakage. This attack appear to be exploitable via Attacker must have access to victim's iOS logs. This vulnerability appears to have been fixed in after commit 18233ca25dfa92cca018b9c0935f43f78fd77fbf.
CVE-2018-1000124
I Librarian I-librarian version 4.8 and earlier contains a XML External Entity (XXE) vulnerability in line 154 of importmetadata.php(simplexml_load_string) that can result in an attacker reading the contents of a file and SSRF. This attack appear to be exploitable via posting xml in the Parameter form_import_textarea.
CVE-2018-1000125
inversoft prime-jwt version prior to version 1.3.0 or prior to commit 0d94dcef0133d699f21d217e922564adbb83a227 contains an input validation vulnerability in JWTDecoder.decode that can result in a JWT that is decoded and thus implicitly validated even if it lacks a valid signature. This attack appear to be exploitable via an attacker crafting a token with a valid header and body and then requests it to be validated. This vulnerability appears to have been fixed in 1.3.0 and later or after commit 0d94dcef0133d699f21d217e922564adbb83a227.
CVE-2018-1000126
Ajenti version 2 contains an Information Disclosure vulnerability in Line 176 of the code source that can result in user and system enumeration as well as data from the /etc/ajenti/config.yml file. This attack appears to be exploitable via network connectivity to the web application.
CVE-2018-1000127
memcached version prior to 1.4.37 contains an Integer Overflow vulnerability in items.c:item_free() that can result in data corruption and deadlocks due to items existing in hash table being reused from free list. This attack appear to be exploitable via network connectivity to the memcached service. This vulnerability appears to have been fixed in 1.4.37 and later.
CVE-2018-1000128
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-7752.  Reason: This candidate is a reservation duplicate of CVE-2018-7752.  Notes: All CVE users should reference CVE-2018-7752 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-1050
All versions of Samba from 4.0.0 onwards are vulnerable to a denial of service attack when the RPC spoolss service is configured to be run as an external daemon. Missing input sanitization checks on some of the input parameters to spoolss RPC calls could cause the print spooler service to crash.
CVE-2018-1057
On a Samba 4 AD DC the LDAP server in all versions of Samba from 4.0.0 onwards incorrectly validates permissions to modify passwords over LDAP allowing authenticated users to change any other users' passwords, including administrative users and privileged service accounts (eg Domain Controllers).
CVE-2018-1227
Pivotal Concourse after 2018-03-05 might allow remote attackers to have an unspecified impact, if a customer obtained the Concourse software from a DNS domain that is no longer controlled by Pivotal. The original domain for the Concourse CI (concourse-dot-ci) open source project has been registered by an unknown actor, and is therefore no longer the official website for Concourse CI. The new official domain is concourse-ci.org. At approximately 4 am EDT on March 7, 2018 the Concourse OSS team began receiving reports that the Concourse domain was not responding. The Concourse OSS team discovered, upon investigation with both the original and the new domain registrars, that the originating domain registrar had made the domain available for purchase. This was done despite the domain being renewed by the Concourse OSS team through August 2018. For a customer to be affected, they would have needed to access a download from a "concourse-dot-ci" domain web site after March 6, 2018 18:00:00 EST. Accessing that domain is NOT recommended by Pivotal. Anyone who had been using that domain should immediately begin using the concourse-ci.org domain instead. Customers can also safely access Concourse software from the traditionally available locations on the Pivotal Network or GitHub.
CVE-2018-6294
Unsecured way of firmware update in Hanwha Techwin Smartcams
CVE-2018-6295
Unencrypted way of remote control and communications in Hanwha Techwin Smartcams
CVE-2018-6296
An undocumented (hidden) capability for switching the web interface in Hanwha Techwin Smartcams
CVE-2018-6297
Buffer overflow in Hanwha Techwin Smartcams
CVE-2018-6298
Remote code execution in Hanwha Techwin Smartcams
CVE-2018-6299
Authentication bypass in Hanwha Techwin Smartcams
CVE-2018-6300
Remote password change in Hanwha Techwin Smartcams
CVE-2018-6301
Arbitrary camera access and monitoring via cloud in Hanwha Techwin Smartcams
CVE-2018-6302
Denial of service by blocking of new camera registration on the cloud server in Hanwha Techwin Smartcams
CVE-2018-6303
Denial of service by uploading malformed firmware in Hanwha Techwin Smartcams
CVE-2018-6304
Stack overflow in custom XML-parser in Gemalto's Sentinel LDK RTE version before 7.65 leads to remote denial of service
CVE-2018-6305
Denial of service in Gemalto's Sentinel LDK RTE version before 7.65
CVE-2018-7405
Cross-site scripting (XSS) in Zoho ManageEngine EventLog Analyzer before 11.12 Build 11120 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2018-7750
transport.py in the SSH server implementation of Paramiko before 1.17.6, 1.18.x before 1.18.5, 2.0.x before 2.0.8, 2.1.x before 2.1.5, 2.2.x before 2.2.3, 2.3.x before 2.3.2, and 2.4.x before 2.4.1 does not properly check whether authentication is completed before processing other requests, as demonstrated by channel-open. A customized SSH client can simply skip the authentication step.
CVE-2018-8078
YzmCMS 3.7 has Stored XSS via the title parameter to advertisement/adver/edit.html.
CVE-2018-8086
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-8087
Memory leak in the hwsim_new_radio_nl function in drivers/net/wireless/mac80211_hwsim.c in the Linux kernel through 4.15.9 allows local users to cause a denial of service (memory consumption) by triggering an out-of-array error case.
