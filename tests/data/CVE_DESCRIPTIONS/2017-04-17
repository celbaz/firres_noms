CVE-2015-8256
Multiple cross-site scripting (XSS) vulnerabilities in Axis network cameras.
CVE-2016-0228
IBM Marketing Platform 10.0 could allow a remote attacker to conduct phishing attacks, caused by an open redirect vulnerability in various scripts. An attacker could exploit this vulnerability to redirect a victim to arbitrary Web sites. IBM X-Force ID: 110236.
CVE-2016-3036
IBM Cognos TM1 10.1 and 10.2 is vulnerable to a denial of service, caused by a stack-based buffer overflow when parsing packets. A remote attacker could exploit this vulnerability to cause a denial of service. IBM X-Force ID: 114612.
CVE-2016-3037
IBM Cognos TM1 10.1 and 10.2 provides a service to return the victim's password with a valid session key. An authenticated attacker with user interaction could obtain this sensitive information. IBM X-Force ID: 114613.
CVE-2016-3038
IBM Cognos TM1 10.1 and 10.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 114614.
CVE-2016-4865
Cross-site scripting vulnerability in Cybozu Office 9.0.0 to 10.4.0 allows attackers with administrator rights to inject arbitrary web script or HTML via the Customapp function.
CVE-2016-4866
Cross-site scripting vulnerability in Cybozu Office 9.0.0 to 10.4.0 allows attackers with administrator rights to inject arbitrary web script or HTML via the Project function.
CVE-2016-4867
Cybozu Office 9.0.0 to 10.4.0 allows remote authenticated attackers to bypass access restriction to view unauthorized project information via the Project function.
CVE-2016-4868
Email header injection vulnerability in Cybozu Office 9.0.0 to 10.4.0 allows remote attackers to inject arbitrary email headers to send unintended emails via specially crafted requests.
CVE-2016-4869
Cybozu Office 9.0.0 to 10.4.0 allow remote attackers to obtain session information via a page where CGI environment variables are displayed.
CVE-2016-4870
Cross-site scripting vulnerability in Cybozu Office 9.0.0 to 10.4.0 allows remote authenticated attackers to inject arbitrary web script or HTML via the Schedule function.
CVE-2016-4871
Cybozu Office 9.0.0 through 10.4.0 allows remote attackers to cause a denial of service.
CVE-2016-4872
Cybozu Office 9.0.0 to 10.4.0 allows remote authenticated attackers to bypass access restrictions to view the names of unauthorized projects via a breadcrumb trail.
CVE-2016-4873
Cybozu Office 9.0.0 to 10.4.0 allows remote authenticated attackers to execute unintended operations via the Project function.
CVE-2016-4874
Cybozu Office 9.0.0 through 10.4.0 allows remote attackers to conduct a "reflected file download" attack.
CVE-2016-5396
Apache Traffic Server 6.0.0 to 6.2.0 are affected by an HPACK Bomb Attack.
CVE-2016-6726
Unspecified vulnerability in Qualcomm components in Android on Nexus 6 and Android One devices.
CVE-2016-6727
The Qualcomm GPS subsystem in Android on Android One devices allows remote attackers to execute arbitrary code.
CVE-2016-7551
chain_sip in Asterisk Open Source 11.x before 11.23.1 and 13.x 13.11.1 and Certified Asterisk 11.6 before 11.6-cert15 and 13.8 before 13.8-cert3 allows remote attackers to cause a denial of service (port exhaustion).
CVE-2017-1160
IBM Financial Transaction Manager for ACH Services for Multi-Platform 3.0.0.x is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 122892.
CVE-2017-1161
IBM API Connect 5.0.6.0 could allow a remote attacker to execute arbitrary commands on the system, caused by improper validation of URLs for the Developer Portal. By crafting a malicious URL, an attacker could exploit this vulnerability to execute arbitrary commands on the system with the privileges of the www-data user. IBM X-Force ID: 122956.
CVE-2017-5645
In Apache Log4j 2.x before 2.8.2, when using the TCP socket server or UDP socket server to receive serialized log events from another application, a specially crafted binary payload can be sent that, when deserialized, can execute arbitrary code.
CVE-2017-5647
A bug in the handling of the pipelined requests in Apache Tomcat 9.0.0.M1 to 9.0.0.M18, 8.5.0 to 8.5.12, 8.0.0.RC1 to 8.0.42, 7.0.0 to 7.0.76, and 6.0.0 to 6.0.52, when send file was used, results in the pipelined request being lost when send file processing of the previous request completed. This could result in responses appearing to be sent for the wrong request. For example, a user agent that sent requests A, B and C could see the correct response for request A, the response for request C for request B and no response for request C.
CVE-2017-5648
While investigating bug 60718, it was noticed that some calls to application listeners in Apache Tomcat 9.0.0.M1 to 9.0.0.M17, 8.5.0 to 8.5.11, 8.0.0.RC1 to 8.0.41, and 7.0.0 to 7.0.75 did not use the appropriate facade object. When running an untrusted application under a SecurityManager, it was therefore possible for that untrusted application to retain a reference to the request or response object and thereby access and/or modify information associated with another web application.
CVE-2017-5650
In Apache Tomcat 9.0.0.M1 to 9.0.0.M18 and 8.5.0 to 8.5.12, the handling of an HTTP/2 GOAWAY frame for a connection did not close streams associated with that connection that were currently waiting for a WINDOW_UPDATE before allowing the application to write more data. These waiting streams each consumed a thread. A malicious client could therefore construct a series of HTTP/2 requests that would consume all available processing threads.
CVE-2017-5651
In Apache Tomcat 9.0.0.M1 to 9.0.0.M18 and 8.5.0 to 8.5.12, the refactoring of the HTTP connectors introduced a regression in the send file processing. If the send file processing completed quickly, it was possible for the Processor to be added to the processor cache twice. This could result in the same Processor being used for multiple requests which in turn could lead to unexpected errors and/or response mix-up.
CVE-2017-5659
Apache Traffic Server before 6.2.1 generates a coredump when there is a mismatch between content length and chunked encoding.
CVE-2017-7885
Artifex jbig2dec 0.13 has a heap-based buffer over-read leading to denial of service (application crash) or disclosure of sensitive information from process memory, because of an integer overflow in the jbig2_decode_symbol_dict function in jbig2_symbol_dict.c in libjbig2dec.a during operation on a crafted .jb2 file.
CVE-2017-7889
The mm subsystem in the Linux kernel through 4.10.10 does not properly enforce the CONFIG_STRICT_DEVMEM protection mechanism, which allows local users to read or write to kernel memory locations in the first megabyte (and bypass slab-allocation access restrictions) via an application that opens the /dev/mem file, related to arch/x86/mm/init.c and drivers/char/mem.c.
CVE-2017-7891
sourcebans-pp (SourceBans++) 1.5.4.7 has XSS in admin.comms.php via the rebanid parameter.
CVE-2017-7892
Sandstorm Cap'n Proto before 0.5.3.1 allows remote crashes related to a compiler optimization. A remote attacker can trigger a segfault in a 32-bit libcapnp application because Cap'n Proto relies on pointer arithmetic calculations that overflow. An example compiler with optimization that elides a bounds check in such calculations is Apple LLVM version 8.1.0 (clang-802.0.41). The attack vector is a crafted far pointer within a message.
