CVE-2007-2029
File descriptor leak in the PDF handler in Clam AntiVirus (ClamAV) allows remote attackers to cause a denial of service via a crafted PDF file.
CVE-2007-2053
Multiple stack-based buffer overflows in AFFLIB before 2.2.6 allow remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via (1) a long LastModified value in an S3 XML response in lib/s3.cpp; (2) a long (a) path or (b) bucket in an S3 URL in lib/vnode_s3.cpp; or (3) a long (c) EFW, (d) AFD, or (c) aimage file path.  NOTE: the aimage vector (3c) has since been recalled from the researcher's original advisory, since the code is not called in any version of AFFLIB.
The vendor has addressed this issue through a product update:
http://www.afflib.org/downloads/

CVE-2007-2054
Multiple format string vulnerabilities in AFFLIB before 2.2.6 allow remote attackers to execute arbitrary code via certain command line parameters, which are used in (1) warn and (2) err calls in (a) lib/s3.cpp, (b) tools/afconvert.cpp, (c) tools/afcopy.cpp, (d) tools/afinfo.cpp, (e) aimage/aimage.cpp, (f) aimage/imager.cpp, and (g) tools/afxml.cpp.  NOTE: the aimage.cpp vector (e) has since been recalled from the researcher's original advisory, since the code is not called in any version of AFFLIB.
The vendor has addressed this issue through the following product update: http://www.afflib.org/downloads/

CVE-2007-2055
AFFLIB 2.2.8 and earlier allows attackers to execute arbitrary commands via shell metacharacters involving (1) certain command line parameters in tools/afconvert.cpp and (2) arguments to the get_parameter function in aimage/ident.cpp.  NOTE: it is unknown if the get_parameter vector (2) is ever called.
The vendor has addressed this issue through a product update which can be found at: http://www.afflib.org/downloads/
CVE-2007-2056
** REJECT **  The getlock function in aimage/aimage.cpp in AFFLIB 2.2.8 and earlier allows local users to overwrite arbitrary files via a symlink attack on temporary lock files (aka "time-of-check-time-of-use file race"). NOTE: the researcher has retracted the original advisory, stating that "the portion of vulnerable code is not called in any current version of AFFLIB and is therefore not exploitable."
CVE-2007-2349
Cross-site scripting (XSS) vulnerability in Invision Power Board (IP.Board) 2.1.x and 2.2.x allows remote attackers to inject arbitrary web script or HTML by uploading crafted images or PDF files.
The vendor has addressed this issue with the following product update:
http://forums.invisionpower.com/index.php?showtopic=234377
CVE-2007-2350
admin/config.php in the music-on-hold module in freePBX 2.2.x allows remote authenticated administrators to execute arbitrary commands via shell metacharacters in the del parameter.
CVE-2007-2351
Unspecified vulnerability in the HP Power Manager Remote Agent (RA) 4.0Build10 and earlier in HP-UX B.11.11 and B.11.23 allows local users to execute arbitrary code via unspecified vectors.
The vendor has addressed this issue with the following product update:
http://h18004.www1.hp.com/products/servers/proliantstorage/power-protection/software/power-manager/pm3-dl.html
CVE-2007-2352
Multiple format string vulnerabilities in AFFLIB 2.2.6 allow remote attackers to execute arbitrary code via certain command line parameters, which are used in (1) warn and (2) err calls, possibly involving (a) lib/s3.cpp, (b) tools/afconvert.cpp, (c) tools/afcopy.cpp, (d) tools/afinfo.cpp, (e) aimage/imager.cpp, and (f) tools/afxml.cpp.  NOTE: this identifier is intended to address the vectors that were not fixed in CVE-2007-2054, but the unfixed vectors were not explicitly listed.
The vendor has addressed this issue with the following product update: http://www.afflib.org/downloads/

CVE-2007-2353
Apache Axis 1.0 allows remote attackers to obtain sensitive information by requesting a non-existent WSDL file, which reveals the installation path in the resulting exception message.
CVE-2007-2354
Progress Webspeed Messenger allows remote attackers to obtain sensitive information via a WService parameter containing "wsbroker1/webutil/about.r", which reveals the operating system and product information.
CVE-2007-2355
The get_url function in DODS_Dispatch.pm for the CGI_server in OPeNDAP 3 allows remote attackers to execute arbitrary commands via shell metacharacters in a URL.
The vendor has addressed this issue with the following solution: http://www.opendap.org/server3-patch-04.27.2007.txt
CVE-2007-2356
Stack-based buffer overflow in the set_color_table function in sunras.c in the SUNRAS plugin in Gimp 2.2.14 allows user-assisted remote attackers to execute arbitrary code via a crafted RAS file.
CVE-2007-2357
Cross-site scripting (XSS) vulnerability in mods/Core/result.php in SineCms 2.3.4 allows remote attackers to inject arbitrary web script or HTML via the stringa parameter.
CVE-2007-2358
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in b2evolution allow remote attackers to execute arbitrary PHP code via a URL in the (1) inc_path parameter to (a) a_noskin.php, (b) a_stub.php, (c) admin.php, (d) contact.php, (e) default.php, (f) index.php, and (g) multiblogs.php in blogs/; the (2) view_path and (3) control_path parameters to blogs/admin.php; and the (4) skins_path parameter to (h) blogs/contact.php and (i) blogs/multiblogs.php.  NOTE: this issue is disputed by CVE, since the inc_path, view_path, control_path, and skins_path variables are all initialized in conf/_advanced.php before they are used.
CVE-2007-2359
Buffer overflow in Ghost Service Manager, as used in Symantec Norton Ghost, Norton Save & Recovery, LiveState Recovery, and BackupExec System Recovery before 20070426, allows local users to gain privileges via a long string.
CVE-2007-2360
Symantec Norton Ghost, Norton Save & Recovery, LiveState Recovery, and BackupExec System Recovery before 20070426, when remote backups of restore point images are configured, encrypt network share credentials with a key formed by a hash of the username, which allows local users to obtain the credentials by calculating the key.
"In order for this exploit to have an impact, administrators would either have to configure client machines to save restore points images to a private share, or the vulnerable machine would have to be shared by several users who each saved their restore points images to private shares."
CVE-2007-2361
Symantec Norton Ghost, Norton Save & Recovery, LiveState Recovery, and BackupExec System Recovery before 20070426, when remote backups of restore points images are configured, uses weak permissions (world readable) for a configuration file with network share credentials, which allows local users to obtain the credentials by reading the file.
CVE-2007-2362
Multiple buffer overflows in MyDNS 1.1.0 allow remote attackers to (1) cause a denial of service (daemon crash) and possibly execute arbitrary code via a certain update, which triggers a heap-based buffer overflow in update.c; and (2) cause a denial of service (daemon crash) via unspecified vectors that trigger an off-by-one stack-based buffer overflow in update.c.
Successful exploitation requires update privileges and that "allow-update" is set to "yes" in mydns.conf.
CVE-2007-2363
Buffer overflow in IrfanView 4.00 and earlier allows user-assisted remote attackers to execute arbitrary code via a crafted .IFF file.
CVE-2007-2364
Multiple PHP remote file inclusion vulnerabilities in burnCMS 0.2 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the root parameter to (1) mysql.class.php or (2) postgres.class.php in lib/db/; or (3) authuser.php, (4) misc.php, or (5) connect.php in lib/.
CVE-2007-2365
Buffer overflow in Adobe Photoshop CS2 and CS3, Photoshop Elements 5.0, Illustrator CS3, and GoLive 9 allows user-assisted remote attackers to execute arbitrary code via a crafted .PNG file.
CVE-2007-2366
Buffer overflow in Corel Paint Shop Pro 11.20 allows user-assisted remote attackers to execute arbitrary code via a crafted .PNG file.
CVE-2007-2367
Buffer overflow in wserve_console.exe in Wserve HTTP Server (whttp) 4.6 allows remote attackers to cause a denial of service (forced application exit) via a long directory name in the URI.
CVE-2007-2368
picture.php in WebSPELL 4.01.02 and earlier allows remote attackers to read arbitrary files via the file parameter.
CVE-2007-2369
Directory traversal vulnerability in picture.php in WebSPELL 4.01.02 and earlier, when PHP before 4.3.0 is used, allows remote attackers to read arbitrary files via a .. (dot dot) in the id parameter.
Successful exploitation requires that PHP before 4.3.0 is used.
CVE-2007-2370
SQL injection vulnerability in index.php in the John Mordo Jobs 2.4 and earlier module for XOOPS allows remote attackers to execute arbitrary SQL commands via the cid parameter in a jobsview action. NOTE: the module name was originally reported as Job Listings.
CVE-2007-2371
admin/index.php in Gregory Kokanosky phpMyNewsletter 0.8 beta5 and earlier provides access to configuration modification before login, which allows remote attackers to cause a denial of service (loss of configuration data), and possibly perform direct static code injection, via a saveGlobalconfig action.
CVE-2007-2372
admin/send_mod.php in Gregory Kokanosky phpMyNewsletter 0.8 beta5 and earlier prints a Location header but does not exit when administrative credentials are missing, which allows remote attackers to compose an e-mail message via a post with the subject, message, format, and list_id fields; and send the message via a direct request for the MsgId value under admin/.
CVE-2007-2373
SQL injection vulnerability in viewcat.php in the WF-Links (wflinks) 1.03 and earlier module for XOOPS allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2007-2374
Unspecified vulnerability in Microsoft Windows 2000, XP, and Server 2003 allows user-assisted remote attackers to execute arbitrary code via unspecified vectors.  NOTE: this information is based upon a vague pre-advisory with no actionable information. However, the advisory is from a reliable source.
CVE-2007-2375
The agent remote upgrade interface in Symantec Enterprise Security Manager (ESM) before 20070405 does not verify the authenticity of upgrades, which allows remote attackers to execute arbitrary code via software that implements the agent upgrade protocol.
CVE-2007-2376
The Dojo framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2377
The Getahead Direct Web Remoting (DWR) framework 1.1.4 exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2378
The Google Web Toolkit (GWT) framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2379
The jQuery framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2380
The Microsoft Atlas framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2381
The MochiKit framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2382
The Moo.fx framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2383
The Prototype (prototypejs) framework before 1.5.1 RC3 exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2384
The Script.aculo.us framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
CVE-2007-2385
The Yahoo! UI framework exchanges data using JavaScript Object Notation (JSON) without an associated protection scheme, which allows remote attackers to obtain the data via a web page that retrieves the data through a URL in the SRC attribute of a SCRIPT element and captures the data using other JavaScript code, aka "JavaScript Hijacking."
