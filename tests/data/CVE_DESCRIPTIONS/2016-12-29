CVE-2015-0854
App/HelperFunctions.pm in Shutter through 0.93.1 allows user-assisted remote attackers to execute arbitrary commands via a crafted image name that is mishandled during a "Show in Folder" action.
CVE-2015-8701
QEMU (aka Quick Emulator) built with the Rocker switch emulation support is vulnerable to an off-by-one error. It happens while processing transmit (tx) descriptors in 'tx_consume' routine, if a descriptor was to have more than allowed (ROCKER_TX_FRAGS_MAX=16) fragments. A privileged user inside guest could use this flaw to cause memory leakage on the host or crash the QEMU process instance resulting in DoS issue.
CVE-2015-8743
QEMU (aka Quick Emulator) built with the NE2000 device emulation support is vulnerable to an OOB r/w access issue. It could occur while performing 'ioport' r/w operations. A privileged (CAP_SYS_RAWIO) user/process could use this flaw to leak or corrupt QEMU memory bytes.
CVE-2015-8744
QEMU (aka Quick Emulator) built with a VMWARE VMXNET3 paravirtual NIC emulator support is vulnerable to crash issue. It occurs when a guest sends a Layer-2 packet smaller than 22 bytes. A privileged (CAP_SYS_RAWIO) guest user could use this flaw to crash the QEMU process instance resulting in DoS.
CVE-2015-8745
QEMU (aka Quick Emulator) built with a VMWARE VMXNET3 paravirtual NIC emulator support is vulnerable to crash issue. It could occur while reading Interrupt Mask Registers (IMR). A privileged (CAP_SYS_RAWIO) guest user could use this flaw to crash the QEMU process instance resulting in DoS.
CVE-2015-8817
QEMU (aka Quick Emulator) built to use 'address_space_translate' to map an address to a MemoryRegionSection is vulnerable to an OOB r/w access issue. It could occur while doing pci_dma_read/write calls. Affects QEMU versions >= 1.6.0 and <= 2.3.1. A privileged user inside guest could use this flaw to crash the guest instance resulting in DoS.
CVE-2015-8818
The cpu_physical_memory_write_rom_internal function in exec.c in QEMU (aka Quick Emulator) does not properly skip MMIO regions, which allows local privileged guest users to cause a denial of service (guest crash) via unspecified vectors.
CVE-2016-10081
/usr/bin/shutter in Shutter through 0.93.1 allows user-assisted remote attackers to execute arbitrary commands via a crafted image name that is mishandled during a "Run a plugin" action.
CVE-2016-1922
QEMU (aka Quick Emulator) built with the TPR optimization for 32-bit Windows guests support is vulnerable to a null pointer dereference flaw. It occurs while doing I/O port write operations via hmp interface. In that, 'current_cpu' remains null, which leads to the null pointer dereference. A user or process could use this flaw to crash the QEMU instance, resulting in DoS issue.
CVE-2016-1981
QEMU (aka Quick Emulator) built with the e1000 NIC emulation support is vulnerable to an infinite loop issue. It could occur while processing data via transmit or receive descriptors, provided the initial receive/transmit descriptor head (TDH/RDH) is set outside the allocated descriptor buffer. A privileged user inside guest could use this flaw to crash the QEMU instance resulting in DoS.
CVE-2016-2197
QEMU (aka Quick Emulator) built with an IDE AHCI emulation support is vulnerable to a null pointer dereference flaw. It occurs while unmapping the Frame Information Structure (FIS) and Command List Block (CLB) entries. A privileged user inside guest could use this flaw to crash the QEMU process instance resulting in DoS.
CVE-2016-2198
QEMU (aka Quick Emulator) built with the USB EHCI emulation support is vulnerable to a null pointer dereference flaw. It could occur when an application attempts to write to EHCI capabilities registers. A privileged user inside quest could use this flaw to crash the QEMU process instance resulting in DoS.
CVE-2016-2246
HP ThinPro 4.4 through 6.1 mishandles the keyboard layout control panel and virtual keyboard application, which allows local users to bypass intended access restrictions and gain privileges via unspecified vectors.
CVE-2016-5328
VMware Tools 9.x and 10.x before 10.1.0 on OS X, when System Integrity Protection (SIP) is enabled, allows local users to determine kernel memory addresses and bypass the kASLR protection mechanism via unspecified vectors.
CVE-2016-5329
VMware Fusion 8.x before 8.5 on OS X, when System Integrity Protection (SIP) is enabled, allows local users to determine kernel memory addresses and bypass the kASLR protection mechanism via unspecified vectors.
CVE-2016-5334
VMware Identity Manager 2.x before 2.7.1 and vRealize Automation 7.x before 7.2.0 allow remote attackers to read /SAAS/WEB-INF and /SAAS/META-INF files via unspecified vectors.
CVE-2016-7079
The graphic acceleration functions in VMware Tools 9.x and 10.x before 10.0.9 on OS X allow local users to gain privileges or cause a denial of service (NULL pointer dereference) via unspecified vectors, a different vulnerability than CVE-2016-7080.
CVE-2016-7080
The graphic acceleration functions in VMware Tools 9.x and 10.x before 10.0.9 on OS X allow local users to gain privileges or cause a denial of service (NULL pointer dereference) via unspecified vectors, a different vulnerability than CVE-2016-7079.
CVE-2016-7081
Multiple heap-based buffer overflows in VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows, when Cortado ThinPrint virtual printing is enabled, allow guest OS users to execute arbitrary code on the host OS via unspecified vectors.
CVE-2016-7082
VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows, when Cortado ThinPrint virtual printing is enabled, allow guest OS users to execute arbitrary code on the host OS or cause a denial of service (host OS memory corruption) via an EMF file.
CVE-2016-7083
VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows, when Cortado ThinPrint virtual printing is enabled, allow guest OS users to execute arbitrary code on the host OS or cause a denial of service (host OS memory corruption) via TrueType fonts embedded in EMFSPOOL.
CVE-2016-7084
tpview.dll in VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows, when Cortado ThinPrint virtual printing is enabled, allows guest OS users to execute arbitrary code on the host OS or cause a denial of service (host OS memory corruption) via a JPEG 2000 image.
CVE-2016-7085
Untrusted search path vulnerability in the installer in VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows allows local users to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2016-7086
The installer in VMware Workstation Pro 12.x before 12.5.0 and VMware Workstation Player 12.x before 12.5.0 on Windows allows local users to gain privileges via a Trojan horse setup64.exe file in the installation directory.
CVE-2016-7087
Directory traversal vulnerability in the Connection Server in VMware Horizon View 5.x before 5.3.7, 6.x before 6.2.3, and 7.x before 7.0.1 allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2016-7456
VMware vSphere Data Protection (VDP) 5.5.x though 6.1.x has an SSH private key with a publicly known password, which makes it easier for remote attackers to obtain login access via an SSH session.
CVE-2016-7457
VMware vRealize Operations (aka vROps) 6.x before 6.4.0 allows remote authenticated users to gain privileges, or halt and remove virtual machines, via unspecified vectors.
CVE-2016-7458
VMware vSphere Client 5.5 before U3e and 6.0 before U2a allows remote vCenter Server and ESXi instances to read arbitrary files via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2016-7459
VMware vCenter Server 5.5 before U3e and 6.0 before U2a allows remote authenticated users to read arbitrary files via a (1) Log Browser, (2) Distributed Switch setup, or (3) Content Library XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2016-7460
The Single Sign-On feature in VMware vCenter Server 5.5 before U3e and 6.0 before U2a and vRealize Automation 6.x before 6.2.5 allows remote attackers to read arbitrary files or cause a denial of service via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2016-7461
The drag-and-drop (aka DnD) function in VMware Workstation Pro 12.x before 12.5.2 and VMware Workstation Player 12.x before 12.5.2 and VMware Fusion and Fusion Pro 8.x before 8.5.2 allows guest OS users to execute arbitrary code on the host OS or cause a denial of service (out-of-bounds memory access on the host OS) via unspecified vectors.
CVE-2016-7462
The Suite REST API in VMware vRealize Operations (aka vROps) 6.x before 6.4.0 allows remote authenticated users to write arbitrary content to files or rename files via a crafted DiskFileItem in a relay-request payload that is mishandled during deserialization.
CVE-2016-7463
Cross-site scripting (XSS) vulnerability in the Host Client in VMware vSphere Hypervisor (aka ESXi) 5.5 and 6.0 allows remote authenticated users to inject arbitrary web script or HTML via a crafted VM.
CVE-2016-9776
QEMU (aka Quick Emulator) built with the ColdFire Fast Ethernet Controller emulator support is vulnerable to an infinite loop issue. It could occur while receiving packets in 'mcf_fec_receive'. A privileged user/process inside guest could use this issue to crash the QEMU process on the host leading to DoS.
CVE-2016-9845
QEMU (aka Quick Emulator) built with the Virtio GPU Device emulator support is vulnerable to an information leakage issue. It could occur while processing 'VIRTIO_GPU_CMD_GET_CAPSET_INFO' command. A guest user/process could use this flaw to leak contents of the host memory bytes.
CVE-2016-9846
QEMU (aka Quick Emulator) built with the Virtio GPU Device emulator support is vulnerable to a memory leakage issue. It could occur while updating the cursor data in update_cursor_data_virgl. A guest user/process could use this flaw to leak host memory bytes, resulting in DoS for a host.
CVE-2016-9877
An issue was discovered in Pivotal RabbitMQ 3.x before 3.5.8 and 3.6.x before 3.6.6 and RabbitMQ for PCF 1.5.x before 1.5.20, 1.6.x before 1.6.12, and 1.7.x before 1.7.7. MQTT (MQ Telemetry Transport) connection authentication with a username/password pair succeeds if an existing username is provided but the password is omitted from the connection request. Connections that use TLS with a client-provided certificate are not affected.
CVE-2016-9878
An issue was discovered in Pivotal Spring Framework before 3.2.18, 4.2.x before 4.2.9, and 4.3.x before 4.3.5. Paths provided to the ResourceServlet were not properly sanitized and as a result exposed to directory traversal attacks.
CVE-2016-9891
Cross-site scripting (XSS) vulnerability in admin/media.php and admin/media_item.php in Dotclear before 2.11 allows remote authenticated users to inject arbitrary web script or HTML via the upfiletitle or media_title parameter (aka the media title).
CVE-2016-9913
Memory leak in the v9fs_device_unrealize_common function in hw/9pfs/9p.c in QEMU (aka Quick Emulator) allows local privileged guest OS users to cause a denial of service (host memory consumption and possibly QEMU process crash) via vectors involving the order of resource cleanup.
CVE-2016-9914
Memory leak in hw/9pfs/9p.c in QEMU (aka Quick Emulator) allows local privileged guest OS users to cause a denial of service (host memory consumption and possibly QEMU process crash) by leveraging a missing cleanup operation in FileOperations.
CVE-2016-9915
Memory leak in hw/9pfs/9p-handle.c in QEMU (aka Quick Emulator) allows local privileged guest OS users to cause a denial of service (host memory consumption and possibly QEMU process crash) by leveraging a missing cleanup operation in the handle backend.
CVE-2016-9916
Memory leak in hw/9pfs/9p-proxy.c in QEMU (aka Quick Emulator) allows local privileged guest OS users to cause a denial of service (host memory consumption and possibly QEMU process crash) by leveraging a missing cleanup operation in the proxy backend.
