CVE-2014-9862
Integer signedness error in bspatch.c in bspatch in bsdiff, as used in Apple OS X before 10.11.6 and other products, allows remote attackers to execute arbitrary code or cause a denial of service (heap-based buffer overflow) via a crafted patch file.
CVE-2015-8946
ecryptfs-setup-swap in eCryptfs before 111 does not prevent the unencrypted swap partition from activating during boot when using GPT partitioning and certain versions of systemd, which allows local users to obtain sensitive information via unspecified vectors.
CVE-2016-1863
The kernel in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to gain privileges or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2016-4582 and CVE-2016-4653.
CVE-2016-1865
The kernel in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to cause a denial of service (NULL pointer dereference) via unspecified vectors.
CVE-2016-4582
The kernel in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to gain privileges or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2016-1863 and CVE-2016-4653.
CVE-2016-4583
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to bypass the Same Origin Policy and obtain image date from an unintended web site via a timing attack involving an SVG document.
CVE-2016-4584
The WebKit Page Loading implementation in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site.
CVE-2016-4585
Cross-site scripting (XSS) vulnerability in the WebKit Page Loading implementation in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to inject arbitrary web script or HTML via an HTTP response specifying redirection that is mishandled by Safari.
CVE-2016-4586
WebKit in Apple Safari before 9.1.2 and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site.
CVE-2016-4587
WebKit in Apple iOS before 9.3.3 and tvOS before 9.2.2 allows remote attackers to obtain sensitive information from uninitialized process memory via a crafted web site.
CVE-2016-4588
WebKit in Apple tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site.
CVE-2016-4589
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, a different vulnerability than CVE-2016-4622, CVE-2016-4623, and CVE-2016-4624.
CVE-2016-4590
WebKit in Apple iOS before 9.3.3 and Safari before 9.1.2 mishandles about: URLs, which allows remote attackers to bypass the Same Origin Policy via a crafted web site.
CVE-2016-4591
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 mishandles the location variable, which allows remote attackers to access the local filesystem via unspecified vectors.
CVE-2016-4592
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to cause a denial of service (memory consumption) via a crafted web site.
CVE-2016-4593
The Siri Contacts component in Apple iOS before 9.3.3 allows physically proximate attackers to read arbitrary Contact card information via unspecified vectors.
CVE-2016-4594
The Sandbox Profiles component in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows attackers to access the process list via a crafted app that makes an API call.
CVE-2016-4595
Safari Login AutoFill in Apple OS X before 10.11.6 allows physically proximate attackers to discover passwords by reading the screen during the login procedure.
CVE-2016-4596
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix bitmap image, a different vulnerability than CVE-2016-4597, CVE-2016-4600, and CVE-2016-4602.
CVE-2016-4597
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix bitmap image, a different vulnerability than CVE-2016-4596, CVE-2016-4600, and CVE-2016-4602.
CVE-2016-4598
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted image.
CVE-2016-4599
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Photoshop document.
CVE-2016-4600
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix bitmap image, a different vulnerability than CVE-2016-4596, CVE-2016-4597, and CVE-2016-4602.
CVE-2016-4601
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted SGI image.
CVE-2016-4602
QuickTime in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix bitmap image, a different vulnerability than CVE-2016-4596, CVE-2016-4597, and CVE-2016-4600.
CVE-2016-4603
Web Media in Apple iOS before 9.3.3 allows attackers to bypass the Private Browsing protection mechanism and obtain sensitive video URL information by leveraging Safari View Controller misbehavior.
CVE-2016-4604
Safari in Apple iOS before 9.3.3 allows remote attackers to spoof the displayed URL via an HTTP response specifying redirection to an invalid TCP port number.
CVE-2016-4605
Calendar in Apple iOS before 9.3.3 allows remote attackers to cause a denial of service (NULL pointer dereference and device restart) via a crafted invitation.
CVE-2016-4607
libxslt in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4608, CVE-2016-4609, CVE-2016-4610, and CVE-2016-4612.
CVE-2016-4608
libxslt in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4607, CVE-2016-4609, CVE-2016-4610, and CVE-2016-4612.
CVE-2016-4609
libxslt in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4607, CVE-2016-4608, CVE-2016-4610, and CVE-2016-4612.
CVE-2016-4610
libxslt in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4607, CVE-2016-4608, CVE-2016-4609, and CVE-2016-4612.
CVE-2016-4612
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-1683.  Reason: This candidate is a reservation duplicate of CVE-2016-1683.  Notes: All CVE users should reference CVE-2016-1683 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-4614
libxml2 in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4615, CVE-2016-4616, and CVE-2016-4619.
CVE-2016-4615
libxml2 in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4614, CVE-2016-4616, and CVE-2016-4619.
CVE-2016-4616
libxml2 in Apple iOS before 9.3.3, OS X before 10.11.6, iTunes before 12.4.2 on Windows, iCloud before 5.2.1 on Windows, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors, a different vulnerability than CVE-2016-4614, CVE-2016-4615, and CVE-2016-4619.
CVE-2016-4619
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2015-8317.  Reason: This candidate is a reservation duplicate of CVE-2015-8317.  Notes: All CVE users should reference CVE-2015-8317 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-4621
libc++abi in Apple OS X before 10.11.6 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-4622
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, a different vulnerability than CVE-2016-4589, CVE-2016-4623, and CVE-2016-4624.
CVE-2016-4623
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, a different vulnerability than CVE-2016-4589, CVE-2016-4622, and CVE-2016-4624.
CVE-2016-4624
WebKit in Apple iOS before 9.3.3, Safari before 9.1.2, and tvOS before 9.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, a different vulnerability than CVE-2016-4589, CVE-2016-4622, and CVE-2016-4623.
CVE-2016-4625
Use-after-free vulnerability in IOSurface in Apple OS X before 10.11.6 allows local users to gain privileges via unspecified vectors.
CVE-2016-4626
IOHIDFamily in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to gain privileges or cause a denial of service (NULL pointer dereference) via unspecified vectors.
CVE-2016-4627
IOAcceleratorFamily in Apple iOS before 9.3.3, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to gain privileges or cause a denial of service (NULL pointer dereference) via unspecified vectors.
CVE-2016-4628
IOAcceleratorFamily in Apple iOS before 9.3.3 and watchOS before 2.2.2 allows local users to obtain sensitive information from kernel memory or cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2016-4629
ImageIO in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted xStride and yStride values in an EXR image.
CVE-2016-4630
ImageIO in Apple OS X before 10.11.6 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted EXR image with B44 compression.
CVE-2016-4631
ImageIO in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted TIFF file.
CVE-2016-4632
ImageIO in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to cause a denial of service (memory consumption) via unspecified vectors.
CVE-2016-4633
Intel Graphics Driver in Apple OS X before 10.11.6 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-4634
The Graphics Drivers subsystem in Apple OS X before 10.11.6 allows local users to gain privileges or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2016-4635
FaceTime in Apple iOS before 9.3.3 and OS X before 10.11.6 allows man-in-the-middle attackers to spoof relayed-call termination, and obtain sensitive audio information in opportunistic circumstances, via unspecified vectors.
CVE-2016-4637
CoreGraphics in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted BMP image.
CVE-2016-4638
Login Window in Apple OS X before 10.11.6 allows attackers to gain privileges via a crafted app that leverages a "type confusion."
CVE-2016-4639
Login Window in Apple OS X before 10.11.6 does not properly initialize memory, which allows local users to cause a denial of service via unspecified vectors.
CVE-2016-4640
Login Window in Apple OS X before 10.11.6 allows attackers to execute arbitrary code in a privileged context, obtain sensitive user information, or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-4641
Login Window in Apple OS X before 10.11.6 allows attackers to execute arbitrary code in a privileged context or obtain sensitive user information via a crafted app that leverages a "type confusion."
CVE-2016-4645
CFNetwork in Apple OS X before 10.11.6 uses weak permissions for web-browser cookies, which allows local users to obtain sensitive information via unspecified vectors.
CVE-2016-4646
Audio in Apple OS X before 10.11.6 mishandles a size value, which allows remote attackers to obtain sensitive information or cause a denial of service (out-of-bounds read) via a crafted audio file.
CVE-2016-4647
Audio in Apple OS X before 10.11.6 allows local users to gain privileges or cause a denial of service (memory corruption) via a crafted file.
CVE-2016-4648
Audio in Apple OS X before 10.11.6 allows local users to obtain sensitive kernel memory-layout information or cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2016-4649
Audio in Apple OS X before 10.11.6 allows local users to cause a denial of service (NULL pointer dereference) via unspecified vectors.
CVE-2016-4651
Cross-site scripting (XSS) vulnerability in the WebKit JavaScript bindings in Apple iOS before 9.3.3 and Safari before 9.1.2 allows remote attackers to inject arbitrary web script or HTML via a crafted HTTP/0.9 response, related to a "cross-protocol cross-site scripting (XPXSS)" vulnerability.
CVE-2016-4652
CoreGraphics in Apple OS X before 10.11.6 allows local users to obtain sensitive information from kernel memory and consequently gain privileges, or cause a denial of service (out-of-bounds read), via unspecified vectors.
CVE-2016-4653
The kernel in Apple iOS before 9.3.3, OS X before 10.11.6, tvOS before 9.2.2, and watchOS before 2.2.2 allows local users to gain privileges or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2016-1863 and CVE-2016-4582.
CVE-2016-5743
Siemens SIMATIC WinCC before 7.3 Update 10 and 7.4 before Update 1, SIMATIC BATCH before 8.1 SP1 Update 9 as distributed in SIMATIC PCS 7 through 8.1 SP1, SIMATIC OpenPCS 7 before 8.1 Update 3 as distributed in SIMATIC PCS 7 through 8.1 SP1, SIMATIC OpenPCS 7 before 8.2 Update 1 as distributed in SIMATIC PCS 7 8.2, and SIMATIC WinCC Runtime Professional before 13 SP1 Update 9 allow remote attackers to execute arbitrary code via crafted packets.
CVE-2016-5744
Siemens SIMATIC WinCC 7.0 through SP3 and 7.2 allows remote attackers to read arbitrary WinCC station files via crafted packets.
CVE-2016-5874
Siemens SIMATIC NET PC-Software before 13 SP2 allows remote attackers to cause a denial of service (OPC UA service outage) via crafted TCP packets.
CVE-2016-6204
Cross-site scripting (XSS) vulnerability in the integrated web server in Siemens SINEMA Remote Connect Server before 1.2 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-6224
ecryptfs-setup-swap in eCryptfs does not prevent the unencrypted swap partition from activating during boot when using GPT partitioning on a (1) NVMe or (2) MMC drive, which allows local users to obtain sensitive information via unspecified vectors.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-8946.
