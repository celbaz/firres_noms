CVE-2013-4866
The LIXIL Corporation My SATIS Genius Toilet application for Android has a hardcoded Bluetooth PIN, which allows physically proximate attackers to trigger physical resource consumption (water or heat) or user discomfort.
CVE-2013-7439
Multiple off-by-one errors in the (1) MakeBigReq and (2) SetReqLen macros in include/X11/Xlibint.h in X11R6.x and libX11 before 1.6.0 allow remote attackers to have unspecified impact via a crafted request, which triggers a buffer overflow.
CVE-2015-0405
Unspecified vulnerability in Oracle MySQL Server 5.6.22 and earlier allows remote authenticated users to affect availability via unknown vectors related to XA.
CVE-2015-0423
Unspecified vulnerability in Oracle MySQL Server 5.6.22 and earlier allows remote authenticated users to affect availability via unknown vectors related to Optimizer.
CVE-2015-0433
Unspecified vulnerability in Oracle MySQL Server 5.5.41 and earlier, and 5.6.22 and earlier, allows remote authenticated users to affect availability via vectors related to InnoDB : DML.
CVE-2015-0438
Unspecified vulnerability in Oracle MySQL Server 5.6.22 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : Partition.
CVE-2015-0439
Unspecified vulnerability in Oracle MySQL Server 5.6.22 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : InnoDB, a different vulnerability than CVE-2015-4756.
CVE-2015-0440
Unspecified vulnerability in the Oracle Knowledge component in Oracle Right Now Service Cloud 8.2.3.10.1 and 8.4.7.2 allows remote attackers to affect integrity via unknown vectors related to Information Manager Console.
CVE-2015-0441
Unspecified vulnerability in Oracle MySQL Server 5.5.41 and earlier, and 5.6.22 and earlier, allows remote authenticated users to affect availability via unknown vectors related to Server : Security : Encryption.
CVE-2015-0447
Unspecified vulnerability in the Oracle Applications Technology Stack component in Oracle E-Business Suite 11.5.10.2, 12.0.6, 12.1.3, 12.2.3, and 12.2.4 allows remote attackers to affect confidentiality via vectors related to Configurator DMZ rules.
CVE-2015-0448
Unspecified vulnerability in Oracle Sun Solaris 11.2 allows local users to affect confidentiality, integrity, and availability via vectors related to ZFS File system.
CVE-2015-0449
Unspecified vulnerability in the Oracle WebLogic Server component in Oracle Fusion Middleware 10.3.6.0, 12.1.1.0, and 12.1.2.0 allows remote attackers to affect integrity via unknown vectors related to Console.
CVE-2015-0450
Unspecified vulnerability in the Oracle WebCenter Portal component in Oracle Fusion Middleware 11.1.1.8.0 allows remote attackers to affect integrity via unknown vectors related to WebCenter Spaces Application.
CVE-2015-0451
Unspecified vulnerability in the Oracle OpenSSO component in Oracle Fusion Middleware 3.0-04 allows remote authenticated users to affect confidentiality via vectors related to OpenSSO Web Agents.
CVE-2015-0452
Unspecified vulnerability in the Oracle VM Server for SPARC component in Oracle Sun Systems Products Suite 3.1 and 3.2 allows remote attackers to affect confidentiality via unknown vectors related to Ldom Manager.
CVE-2015-0453
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Products 8.53 and 8.54 allows remote attackers to affect confidentiality via vectors related to PORTAL.
CVE-2015-0455
Unspecified vulnerability in the XDB - XML Database component in Oracle Database Server 11.2.0.3, 11.2.0.4, 12.1.0.1, and 12.1.0.2 allows remote authenticated users to affect confidentiality via unknown vectors.
Per Oracle: The CVSS score is 6.8 only on Windows for Database versions prior to 12c. The CVSS is 4.0 (Confidentiality is "Partial+") for Database 12c on Windows and for all versions of Database on Linux, Unix and other platforms. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0456
Unspecified vulnerability in the Oracle WebCenter Portal component in Oracle Fusion Middleware 11.1.1.8.0 allows remote attackers to affect integrity via unknown vectors related to Portlet Services.
CVE-2015-0457
Unspecified vulnerability in the Java VM component in Oracle Database Server 11.1.0.7, 11.2.0.3, 11.2.0.4, 12.1.0.1, and 12.1.0.2 allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors, a different vulnerability than CVE-2015-2629.
Per Oracle: The CVSS score is 9.0 only on Windows for Database versions prior to 12c. The CVSS is 6.5 (Confidentiality, Integrity and Availability is "Partial+") for Database 12c on Windows and for all versions of Database on Linux, Unix and other platforms. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0458
Unspecified vulnerability in in Oracle Java SE 6u91, 7u76, and 8u40 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Deployment.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0459
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40, and JavaFX 2.2.76, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to 2D, a different vulnerability than CVE-2015-0491.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0460
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Hotspot.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0461
Unspecified vulnerability in the Oracle Access Manager component in Oracle Fusion Middleware 11.1.1.5 and 11.1.1.7 allows remote authenticated users to affect confidentiality and integrity via unknown vectors related to Authentication Engine.
CVE-2015-0462
Unspecified vulnerability in the Oracle Transportation Management component in Oracle Supply Chain Products Suite 6.1, 6.2, 6.3.0, 6.3.1, 6.3.2, 6.3.3, 6.3.4, 6.3.5, and 6.3.6 allows remote authenticated users to affect confidentiality via unknown vectors related to Security.
CVE-2015-0463
Unspecified vulnerability in the Oracle Transportation Management component in Oracle Supply Chain Products Suite 6.2, 6.3.0, 6.3.1, 6.3.2, 6.3.3, 6.3.4, 6.3.5, and 6.3.6 allows remote authenticated users to affect confidentiality via unknown vectors related to Security.
CVE-2015-0464
Unspecified vulnerability in the Oracle Transportation Management component in Oracle Supply Chain Products Suite 6.1, 6.2, 6.3.0, 6.3.1, 6.3.2, 6.3.3, 6.3.4, 6.3.5, and 6.3.6 allows remote attackers to affect confidentiality via unknown vectors related to Security.
CVE-2015-0465
Unspecified vulnerability in the Oracle Transportation Management component in Oracle Supply Chain Products Suite 6.1, 6.2, 6.3.0, 6.3.1, 6.3.2, 6.3.3, 6.3.4, 6.3.5, and 6.3.6 allows remote authenticated users to affect confidentiality via unknown vectors related to UI Infrastructure.
CVE-2015-0466
Unspecified vulnerability in the Oracle Retail Back Office component in Oracle Retail Applications 12.0, 12.0IN, 13.0, 13.1, 13.2, 13.3, 13.4, 14.0, and 14.1 allows remote attackers to affect integrity via unknown vectors.
CVE-2015-0469
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to 2D.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0470
Unspecified vulnerability in Oracle Java SE 8u40 allows remote attackers to affect integrity via unknown vectors related to Hotspot.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0471
Unspecified vulnerability in Oracle Sun Solaris 10 and 11.2 allows local users to affect confidentiality, integrity, and availability via unknown vectors related to libelfsign.
CVE-2015-0472
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Products 8.53 and 8.54 allows remote authenticated users to affect integrity via vectors related to PIA Core Technology, a different vulnerability than CVE-2015-0487.
CVE-2015-0473
Unspecified vulnerability in the Enterprise Manager Base Platform component in Oracle Enterprise Manager Grid Control MOS 12.1.0.5 and 12.1.0.6 allows remote attackers to affect integrity via unknown vectors related to My Oracle Support Plugin.
CVE-2015-0474
Unspecified vulnerability in the Oracle Outside In Technology component in Oracle Fusion Middleware 8.4.1, 8.5.0, and 8.5.1 allows local users to affect availability via unknown vectors related to Outside In Filters, a different vulnerability than CVE-2015-0493.
Per Oracle: Outside In Technology is a suite of software development kits (SDKs). It does not have any particular associated protocol. If the hosting software passes data received over the network to Outside In Technology code, the CVSS Base Score would increase to 6.8. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0475
Unspecified vulnerability in the JD Edwards EnterpriseOne Technology component in Oracle JD Edwards Products 9.1 allows remote authenticated users to affect confidentiality via unknown vectors related to Web Runtime Security.
CVE-2015-0476
Unspecified vulnerability in the SQL Trace Analyzer component in Oracle Support Tools before 12.1.11 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
Per Oracle:  Please refer to My Oracle Support Note 215187.1 for instructions on upgrading to SQLT version 12.1.11. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0477
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40 allows remote attackers to affect integrity via unknown vectors related to Beans.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0478
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40, and JRockit R28.3.5, allows remote attackers to affect confidentiality via vectors related to JCE.
Per Oracle:  Applies to client and server deployment of Java. This vulnerability can be exploited through sandboxed Java Web Start applications and sandboxed Java applets. It can also be exploited by supplying data to APIs in the specified Component without using sandboxed Java Web Start applications or sandboxed Java applets, such as through a web service. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0479
Unspecified vulnerability in the XDK and XDB - XML Database component in Oracle Database Server 11.2.0.3, 11.2.0.4, and 12.1.0.1 allows remote authenticated users to affect availability via unknown vectors.
CVE-2015-0480
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40 allows remote attackers to affect integrity and availability via unknown vectors related to Tools.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0482
Unspecified vulnerability in the Oracle WebLogic Server component in Oracle Fusion Middleware 12.1.2.0 and 12.1.3.0 allows remote authenticated users to affect confidentiality, integrity, and availability via vectors related to WLS-WebServices.
CVE-2015-0483
Unspecified vulnerability in the Core RDBMS component in Oracle Database Server 11.1.0.7, 11.2.0.3, 11.2.0.4, 12.1.0.1, and 12.1.0.2 allows remote authenticated users to affect integrity via unknown vectors.
CVE-2015-0484
Unspecified vulnerability in Oracle Java SE 7u76 and 8u40, and Java FX 2.2.76, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors, a different vulnerability than CVE-2015-0492.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0485
Unspecified vulnerability in the PeopleSoft Enterprise SCM Strategic Sourcing component in Oracle PeopleSoft Products 9.1 and 9.2 allows remote authenticated users to affect confidentiality via unknown vectors related to Security.
CVE-2015-0486
Unspecified vulnerability in Oracle Java SE 8u40 allows remote attackers to affect confidentiality via unknown vectors related to Deployment.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0487
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Products 8.53 and 8.54 allows remote authenticated users to affect integrity via vectors related to PIA Core Technology, a different vulnerability than CVE-2015-0472.
CVE-2015-0488
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40, and JRockit R28.3.5, allows remote attackers to affect availability via vectors related to JSSE.
Per Oracle:  Applies to client and server deployment of JSSE. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0489
Unspecified vulnerability in the Application Management Pack for Oracle E-Business Suite component in Oracle E-Business Suite AMP 121030 and 121020 allows local users to affect confidentiality via vectors related to EBS Plugin.
CVE-2015-0490
Unspecified vulnerability in the Oracle Agile Engineering Data Management component in Oracle Supply Chain Products Suite 6.1.3.0 allows remote authenticated users to affect confidentiality and integrity via vectors related to BAS - Base Component.
CVE-2015-0491
Unspecified vulnerability in Oracle Java SE 5.0u81, 6u91, 7u76, and 8u40, and Java FX 2.2.76, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to 2D, a different vulnerability than CVE-2015-0459.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0492
Unspecified vulnerability in Oracle Java SE 7u76 and 8u40, and JavaFX 2.2.76, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors, a different vulnerability than CVE-2015-0484.
Per Oracle:  Applies to client deployment of Java only. This vulnerability can be exploited only through sandboxed Java Web Start applications and sandboxed Java applets. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0493
Unspecified vulnerability in the Oracle Outside In Technology component in Oracle Fusion Middleware 8.4.1, 8.5.0, and 8.5.1 allows local users to affect availability via unknown vectors related to Outside In Filters, a different vulnerability than CVE-2015-0474.
Per Oracle: Outside In Technology is a suite of software development kits (SDKs). It does not have any particular associated protocol. If the hosting software passes data received over the network to Outside In Technology code, the CVSS Base Score would increase to 6.8. (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-0494
Unspecified vulnerability in the Oracle Retail Central Office component in Oracle Retail Applications 13.1, 13.2, 13.3, 13.4, 14.0, and 14.1 allows remote attackers to affect integrity via unknown vectors.
CVE-2015-0495
Unspecified vulnerability in the Oracle Commerce Guided Search / Oracle Commerce Experience Manager component in Oracle Commerce Platform 3.x and 11.x allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Workbench.
CVE-2015-0496
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Products 8.53 and 8.54 allows remote authenticated users to affect confidentiality via vectors related to PIA Search Functionality.
CVE-2015-0497
Unspecified vulnerability in the PeopleSoft Enterprise Portal Interaction Hub component in Oracle PeopleSoft Products 9.1.00 allows remote attackers to affect integrity via unknown vectors related to Enterprise Portal.
CVE-2015-0498
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Replication.
CVE-2015-0499
Unspecified vulnerability in Oracle MySQL Server 5.5.42 and earlier, and 5.6.23 and earlier, allows remote authenticated users to affect availability via unknown vectors related to Server : Federated.
CVE-2015-0500
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors.
CVE-2015-0501
Unspecified vulnerability in Oracle MySQL Server 5.5.42 and earlier, and 5.6.23 and earlier, allows remote authenticated users to affect availability via unknown vectors related to Server : Compiling.
CVE-2015-0502
Unspecified vulnerability in the Siebel UI Framework component in Oracle Siebel CRM 8.1 and 8.2 allows remote attackers to affect integrity via unknown vectors related to Portal Framework.
CVE-2015-0503
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : Partition.
CVE-2015-0504
Unspecified vulnerability in the Oracle Application Object Library component in Oracle E-Business Suite 12.0.6 and 12.1.3 allows remote attackers to affect integrity via unknown vectors related to Error Messages.
CVE-2015-0505
Unspecified vulnerability in Oracle MySQL Server 5.5.42 and earlier, and 5.6.23 and earlier, allows remote authenticated users to affect availability via vectors related to DDL.
CVE-2015-0506
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to InnoDB, a different vulnerability than CVE-2015-0508.
CVE-2015-0507
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : Memcached.
CVE-2015-0508
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : InnoDB, a different vulnerability than CVE-2015-0506.
CVE-2015-0509
Unspecified vulnerability in the Oracle Hyperion BI+ component in Oracle Hyperion 11.1.2.2 and 11.1.2.3 allows remote attackers to affect integrity via unknown vectors related to Reporting and Analysis.
CVE-2015-0510
Unspecified vulnerability in the Oracle Commerce Platform component in Oracle Commerce Platform 9.4, 10.0, and 10.2 allows remote attackers to affect integrity via vectors related to Dynamo Application Framework - HTML Admin User Interface.
CVE-2015-0511
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : SP.
CVE-2015-1314
The USAA Mobile Banking application before 7.10.1 for Android displays the most recently-used screen before prompting the user for login, which might allow physically proximate users to obtain banking account numbers and balances.
CVE-2015-1821
Heap-based buffer overflow in chrony before 1.31.1 allows remote authenticated users to cause a denial of service (chronyd crash) or possibly execute arbitrary code by configuring the (1) NTP or (2) cmdmon access with a subnet size that is indivisible by four and an address with a nonzero bit in the subnet remainder.
CVE-2015-1822
chrony before 1.31.1 does not initialize the last "next" pointer when saving unacknowledged replies to command requests, which allows remote authenticated users to cause a denial of service (uninitialized pointer dereference and daemon crash) or possibly execute arbitrary code via a large number of command requests.
CVE-2015-2565
Unspecified vulnerability in the Oracle Installed Base component in Oracle E-Business Suite 11.5.10.2, 12.0.4, 12.0.6, 12.1.1, 12.1.2, and 12.1.3 allows remote attackers to affect integrity via unknown vectors related to Create Item Instance.
CVE-2015-2566
Unspecified vulnerability in Oracle MySQL Server 5.6.22 and earlier allows remote authenticated users to affect availability via vectors related to DML.
CVE-2015-2567
Unspecified vulnerability in Oracle MySQL Server 5.6.23 and earlier allows remote authenticated users to affect availability via unknown vectors related to Server : Security : Privileges.
CVE-2015-2568
Unspecified vulnerability in Oracle MySQL Server 5.5.41 and earlier, and 5.6.22 and earlier, allows remote attackers to affect availability via unknown vectors related to Server : Security : Privileges.
CVE-2015-2570
Unspecified vulnerability in the Oracle Demand Planning component in Oracle Supply Chain Products Suite 11.5.10, 12.0, 12.1, and 12.2 allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors related to Security.
CVE-2015-2571
Unspecified vulnerability in Oracle MySQL Server 5.5.42 and earlier, and 5.6.23 and earlier, allows remote authenticated users to affect availability via unknown vectors related to Server : Optimizer.
CVE-2015-2572
Unspecified vulnerability in the Oracle Hyperion Smart View for Office component in Oracle Hyperion 11.1.2.5.216 and earlier, when running on Windows, allows local users to affect confidentiality, integrity, and availability via unknown vectors related to Core.
CVE-2015-2573
Unspecified vulnerability in Oracle MySQL Server 5.5.41 and earlier, and 5.6.22 and earlier, allows remote authenticated users to affect availability via vectors related to DDL.
CVE-2015-2574
Unspecified vulnerability in Oracle Sun Solaris 10 allows local users to affect confidentiality via unknown vectors related to Text Utilities.
CVE-2015-2575
Unspecified vulnerability in the MySQL Connectors component in Oracle MySQL 5.1.34 and earlier allows remote authenticated users to affect confidentiality and integrity via unknown vectors related to Connector/J.
CVE-2015-2576
Unspecified vulnerability in the MySQL Utilities component in Oracle MySQL 1.5.1 and earlier, when running on Windows, allows local users to affect integrity via unknown vectors related to Installation.
Per Oracle: This vulnerability is only applicable on Windows operating system (http://www.oracle.com/technetwork/topics/security/cpuapr2015-2365600.html)
CVE-2015-2577
Unspecified vulnerability in Oracle Sun Solaris 10 allows local users to affect confidentiality, integrity, and availability via unknown vectors related to Accounting commands.
CVE-2015-2578
Unspecified vulnerability in Oracle Sun Solaris 11.2 allows remote attackers to affect availability via vectors related to Kernel IDMap.
CVE-2015-2579
Unspecified vulnerability in the Oracle Health Sciences Argus Safety component in Oracle Health Sciences Applications 8.0 allows local users to affect confidentiality via vectors related to BIP Installer.
CVE-2015-3319
Hotspot Express hotEx Billing Manager 73 does not include the HTTPOnly flag in a Set-Cookie header, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.
CVE-2015-3320
Lenovo USB Enhanced Performance Keyboard software before 2.0.2.2 includes active debugging code in SKHOOKS.DLL, which allows local users to obtain keypress information by accessing debug output.
CVE-2015-3322
Lenovo ThinkServer RD350, RD450, RD550, RD650, and TD350 servers before 1.26.0 use weak encryption to store (1) user and (2) administrator BIOS passwords, which allows attackers to decrypt the passwords via unspecified vectors.
CVE-2015-3323
The ThinkServer System Manager (TSM) Baseboard Management Controller before firmware 1.27.73476 for ThinkServer RD350, RD450, RD550, RD650, and TD350 allows remote attackers to cause a denial of service (web interface crash) via a malformed HTTP request during authentication.
CVE-2015-3324
The ThinkServer System Manager (TSM) Baseboard Management Controller before firmware 1.27.73476 for ThinkServer RD350, RD450, RD550, RD650, and TD350 does not validate server certificates during an "encrypted remote KVM session," which allows man-in-the-middle attackers to spoof servers.
