CVE-2017-12699
An Incorrect Default Permissions issue was discovered in AzeoTech DAQFactory versions prior to 17.1. Local, non-administrative users may be able to replace or modify original application files with malicious ones.
CVE-2017-12731
A SQL Injection issue was discovered in OPW Fuel Management Systems SiteSentinel Integra 100, SiteSentinel Integra 500, and SiteSentinel iSite ATG consoles with the following software versions: older than V175, V175-V189, V191-V195, and V16Q3.1. The application is vulnerable to injection of malicious SQL queries via the input from the client.
CVE-2017-12733
A Missing Authentication for Critical Function issue was discovered in OPW Fuel Management Systems SiteSentinel Integra 100, SiteSentinel Integra 500, and SiteSentinel iSite ATG consoles with the following software versions: older than V175, V175-V189, V191-V195, and V16Q3.1. An attacker may create an application user account to gain administrative privileges.
CVE-2017-14222
In libavformat/mov.c in FFmpeg 3.3.3, a DoS in read_tfra() due to lack of an EOF (End of File) check might cause huge CPU and memory consumption. When a crafted MOV file, which claims a large "item_count" field in the header but does not contain sufficient backing data, is provided, the loop would consume huge CPU and memory resources, since there is no EOF check inside the loop.
CVE-2017-14223
In libavformat/asfdec_f.c in FFmpeg 3.3.3, a DoS in asf_build_simple_index() due to lack of an EOF (End of File) check might cause huge CPU consumption. When a crafted ASF file, which claims a large "ict" field in the header but does not contain sufficient backing data, is provided, the for loop would consume huge CPU and memory resources, since there is no EOF check inside the loop.
CVE-2017-14224
A heap-based buffer overflow in WritePCXImage in coders/pcx.c in ImageMagick 7.0.6-8 Q16 allows remote attackers to cause a denial of service or code execution via a crafted file.
CVE-2017-14225
The av_color_primaries_name function in libavutil/pixdesc.c in FFmpeg 3.3.3 may return a NULL pointer depending on a value contained in a file, but callers do not anticipate this, as demonstrated by the avcodec_string function in libavcodec/utils.c, leading to a NULL pointer dereference. (It is also conceivable that there is security relevance for a NULL pointer dereference in av_color_primaries_name calls within the ffprobe command-line program.)
CVE-2017-14226
WP1StylesListener.cpp, WP5StylesListener.cpp, and WP42StylesListener.cpp in libwpd 0.10.1 mishandle iterators, which allows remote attackers to cause a denial of service (heap-based buffer over-read in the WPXTableList class in WPXTable.cpp). This vulnerability can be triggered in LibreOffice before 5.3.7. It may lead to suffering a remote attack against a LibreOffice application.
CVE-2017-14227
In MongoDB libbson 1.7.0, the bson_iter_codewscope function in bson-iter.c miscalculates a bson_utf8_validate length argument, which allows remote attackers to cause a denial of service (heap-based buffer over-read in the bson_utf8_validate function in bson-utf8.c), as demonstrated by bson-to-json.c.
CVE-2017-14228
In Netwide Assembler (NASM) 2.14rc0, there is an illegal address access in the function paste_tokens() in preproc.c, aka a NULL pointer dereference. It will lead to remote denial of service.
CVE-2017-14229
There is an infinite loop in the jpc_dec_tileinit function in jpc/jpc_dec.c of Jasper 2.0.13. It will lead to a remote denial of service attack.
CVE-2017-5147
An Uncontrolled Search Path Element issue was discovered in AzeoTech DAQFactory versions prior to 17.1. An uncontrolled search path element vulnerability has been identified, which may execute malicious DLL files that have been placed within the search path.
CVE-2017-8040
In Single Sign-On for Pivotal Cloud Foundry (PCF) 1.3.x versions prior to 1.3.4 and 1.4.x versions prior to 1.4.3, an XXE (XML External Entity) attack was discovered in the Single Sign-On service dashboard. Privileged users can in some cases upload malformed XML leading to exposure of data on the Single Sign-On service broker file system.
CVE-2017-8041
In Single Sign-On for Pivotal Cloud Foundry (PCF) 1.3.x versions prior to 1.3.4 and 1.4.x versions prior to 1.4.3, a user can execute a XSS attack on certain Single Sign-On service UI pages by inputting code in the text field for an organization name.
