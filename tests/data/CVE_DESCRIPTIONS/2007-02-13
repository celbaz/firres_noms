CVE-2007-0025
The MFC component in Microsoft Windows 2000 SP4, XP SP2, and 2003 SP1 and Visual Studio .NET 2000, 2002 SP1, 2003, and 2003 SP1 allows user-assisted remote attackers to execute arbitrary code via an RTF file with a malformed OLE object that triggers memory corruption. NOTE: this might be due to a stack-based buffer overflow in the AfxOleSetEditMenu function in MFC42u.dll.
CVE-2007-0026
The OLE Dialog component in Microsoft Windows 2000 SP4, XP SP2, and 2003 SP1 allows user-assisted remote attackers to execute arbitrary code via an RTF file with a malformed OLE object that triggers memory corruption.
CVE-2007-0208
Microsoft Word in Office 2000 SP3, XP SP3, Office 2003 SP2, Works Suite 2004 to 2006, and Office 2004 for Mac does not correctly check the properties of certain documents and warn the user of macro content, which allows user-assisted remote attackers to execute arbitrary code.
CVE-2007-0209
Microsoft Word in Office 2000 SP3, XP SP3, Office 2003 SP2, Works Suite 2004 to 2006, and Office 2004 for Mac allows user-assisted remote attackers to execute arbitrary code via a Word file with a malformed drawing object, which leads to memory corruption.
CVE-2007-0210
The Window Image Acquisition (WIA) Service in Microsoft Windows XP SP2 allows local users to gain privileges via unspecified vectors involving an "unchecked buffer," probably a buffer overflow.
CVE-2007-0211
The hardware detection functionality in the Windows Shell in Microsoft Windows XP SP2 and Professional, and Server 2003 SP1 allows local users to gain privileges via an unvalidated parameter to a function related to the "detection and registration of new hardware."
CVE-2007-0214
The HTML Help ActiveX control (Hhctrl.ocx) in Microsoft Windows 2000 SP3, XP SP2 and Professional, 2003 SP1 allows remote attackers to execute arbitrary code via unspecified functions, related to uninitialized parameters.
CVE-2007-0217
The wininet.dll FTP client code in Microsoft Internet Explorer 5.01 and 6 might allow remote attackers to execute arbitrary code via an FTP server response of a specific length that causes a terminating null byte to be written outside of a buffer, which causes heap corruption.
CVE-2007-0219
Microsoft Internet Explorer 5.01, 6, and 7 uses certain COM objects from (1) Msb1fren.dll, (2) Htmlmm.ocx, and (3) Blnmgrps.dll as ActiveX controls, which allows remote attackers to execute arbitrary code via unspecified vectors, a different issue than CVE-2006-4697.
CVE-2007-0842
The 64-bit versions of Microsoft Visual C++ 8.0 standard library (MSVCR80.DLL) time functions, including (1) localtime, (2) localtime_s, (3) gmtime, (4) gmtime_s, (5) ctime, (6) ctime_s, (7) wctime, (8) wctime_s, and (9) fstat, trigger an assertion error instead of a NULL pointer or EINVAL when processing a time argument later than Jan 1, 3000, which might allow context-dependent attackers to cause a denial of service (application exit) via large time values. NOTE: it could be argued that this is a design limitation of the functions, and the vulnerability lies with any application that does not validate arguments to these functions.  However, this behavior is inconsistent with documentation, which does not list assertions as a possible result of an error condition.
CVE-2007-0895
Race condition in recursive directory deletion with the (1) -r or (2) -R option in rm in Solaris 8 through 10 before 20070208 allows local users to delete files and directories as the user running rm by moving a low-level directory to a higher level as it is being deleted, which causes rm to chdir to a ".." directory that is higher than expected, possibly up to the root file system, a related issue to CVE-2002-0435.
CVE-2007-0896
Cross-site scripting (XSS) vulnerability in the (1) Sage before 1.3.10, and (2) Sage++ extensions for Firefox, allows remote attackers to inject arbitrary web script or HTML via a "<SCRIPT/=''SRC='" sequence in an RSS feed, a different vulnerability than CVE-2006-4712.
CVE-2007-0900
Multiple PHP remote file inclusion vulnerabilities in TagIt! Tagboard 2.1.B Build 2 and earlier, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the (1) configpath parameter to (a) tagviewer.php, (b) tag_process.php, and (c) CONFIG/errmsg.inc.php; and (d) addTagmin.php, (e) ban_watch.php, (f) delTagmin.php, (g) delTag.php, (h) editTagmin.php, (i) editTag.php, (j) manageTagmins.php, and (k) verify.php in tagmin/; the (2) adminpath parameter to (l) tagviewer.php, (m) tag_process.php, and (n) tagmin/index.php; and the (3) admin parameter to (o) readconf.php, (p) updateconf.php, (q) updatefilter.php, and (r) wordfilter.php in tagmin/; different vectors than CVE-2006-5249.
CVE-2007-0901
Multiple cross-site scripting (XSS) vulnerabilities in Info pages in MoinMoin 1.5.7 allow remote attackers to inject arbitrary web script or HTML via the (1) hitcounts and (2) general parameters, different vectors than CVE-2007-0857.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0902
Unspecified vulnerability in the "Show debugging information" feature in MoinMoin 1.5.7 allows remote attackers to obtain sensitive information.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0903
Unspecified vulnerability in the mod_roster_odbc module in ejabberd before 1.1.3 has unknown impact and attack vectors.
CVE-2007-0904
SQL injection vulnerability in projects.php in LightRO CMS 1.0 allows remote attackers to execute arbitrary SQL commands via the ID parameter to index.php.
CVE-2007-0905
PHP before 5.2.1 allows attackers to bypass safe_mode and open_basedir restrictions via unspecified vectors in the session extension.  NOTE: it is possible that this issue is a duplicate of CVE-2006-6383.
CVE-2007-0906
Multiple buffer overflows in PHP before 5.2.1 allow attackers to cause a denial of service and possibly execute arbitrary code via unspecified vectors in the (1) session, (2) zip, (3) imap, and (4) sqlite extensions; (5) stream filters; and the (6) str_replace, (7) mail, (8) ibase_delete_user, (9) ibase_add_user, and (10) ibase_modify_user functions.  NOTE: vector 6 might actually be an integer overflow (CVE-2007-1885).  NOTE: as of 20070411, vector (3) might involve the imap_mail_compose function (CVE-2007-1825).
CVE-2007-0907
Buffer underflow in PHP before 5.2.1 allows attackers to cause a denial of service via unspecified vectors involving the sapi_header_op function.
CVE-2007-0908
The WDDX deserializer in the wddx extension in PHP 5 before 5.2.1 and PHP 4 before 4.4.5 does not properly initialize the key_length variable for a numerical key, which allows context-dependent attackers to read stack memory via a wddxPacket element that contains a variable with a string name before a numerical variable.
CVE-2007-0909
Multiple format string vulnerabilities in PHP before 5.2.1 might allow attackers to execute arbitrary code via format string specifiers to (1) all of the *print functions on 64-bit systems, and (2) the odbc_result_all function.
CVE-2007-0910
Unspecified vulnerability in PHP before 5.2.1 allows attackers to "clobber" certain super-global variables via unspecified vectors.
CVE-2007-0911
Off-by-one error in the str_ireplace function in PHP 5.2.1 might allow context-dependent attackers to cause a denial of service (crash).
CVE-2007-0912
Cross-Site Request Forgery (CSRF) vulnerability in admin/admin.adm.php in Jportal 2.3.1, and possibly earlier, allows remote attackers to perform privileged actions as administrators by tricking the admin into accessing a URL with modified arguments to admin/admin.adm.php.
