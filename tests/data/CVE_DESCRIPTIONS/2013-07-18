CVE-2012-6349
Buffer overflow in the .mdb parser in Autonomy KeyView IDOL, as used in IBM Notes 8.5.x before 8.5.3 FP4, allows remote attackers to execute arbitrary code via a crafted file, aka SPR KLYH92XL3W.
CVE-2013-1218
Cisco Intrusion Prevention System (IPS) Software in ASA 5500-X IPS-SSP software modules before 7.1(7)sp1E4 allows remote attackers to cause a denial of service (Analysis Engine process hang or device reload) via fragmented (1) IPv4 or (2) IPv6 packets, aka Bug ID CSCue51272.
CVE-2013-1243
The IP stack in Cisco Intrusion Prevention System (IPS) Software in ASA 5500-X IPS-SSP software and hardware modules before 7.1(5)E4, IPS 4500 sensors before 7.1(6)E4, and IPS 4300 sensors before 7.1(5)E4 allows remote attackers to cause a denial of service (MainApp process hang) via malformed IPv4 packets, aka Bug ID CSCtx18596.
CVE-2013-1606
Buffer overflow in the ubnt-streamer RTSP service on the Ubiquiti UBNT AirCam with airVision firmware before 1.1.6 allows remote attackers to execute arbitrary code via a long rtsp: URI in a DESCRIBE request.
Per: http://www.coresecurity.com/advisories/buffer-overflow-ubiquiti-aircam-rtsp-service

'Vulnerable Packages

    Cameras Models: airCam, airCam Mini, airCam Dome.
    Firmware Version Verified: AirCam v1.1.5.'
CVE-2013-3402
An unspecified function in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(2) allows remote authenticated users to execute arbitrary commands via unknown vectors, aka Bug ID CSCuh73440.
CVE-2013-3403
Multiple untrusted search path vulnerabilities in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(1a) allow local users to gain privileges by leveraging unspecified file-permission and environment-variable issues for privileged programs, aka Bug ID CSCuh73454.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2013-3404
SQL injection vulnerability in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(1a) allows remote attackers to execute arbitrary SQL commands via unspecified vectors, leading to discovery of encrypted credentials by leveraging metadata, aka Bug ID CSCuh01051.
CVE-2013-3410
Cisco Intrusion Prevention System (IPS) Software on IPS NME devices before 7.0(9)E4 allows remote attackers to cause a denial of service (device reload) via malformed IPv4 packets that trigger incorrect memory allocation, aka Bug ID CSCua61977.
CVE-2013-3411
The IDSM-2 drivers in Cisco Intrusion Prevention System (IPS) Software on Cisco Catalyst 6500 devices with an IDSM-2 module allow remote attackers to cause a denial of service (device hang) via malformed IPv4 TCP packets, aka Bug ID CSCuh27460.
CVE-2013-3412
SQL injection vulnerability in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(2) allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCuh81766.
CVE-2013-3420
Cross-site request forgery (CSRF) vulnerability in the web framework on the Cisco Identity Services Engine (ISE) allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCuh25506.
CVE-2013-3426
The Serviceability servlet on Cisco 9900 IP phones does not properly restrict paths, which allows remote attackers to read arbitrary files by specifying a pathname in a file request, aka Bug ID CSCuh52810.
CVE-2013-3433
Untrusted search path vulnerability in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(1a) allows local users to gain privileges by leveraging unspecified file-permission and environment-variable issues for privileged programs, aka Bug ID CSCui02276.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2013-3434
Untrusted search path vulnerability in Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(1a) allows local users to gain privileges by leveraging unspecified file-permission and environment-variable issues for privileged programs, aka Bug ID CSCui02242.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2013-3665
Unspecified vulnerability in Autodesk AutoCAD through 2014, AutoCAD LT through 2014, and DWG TrueView through 2014 allows remote attackers to execute arbitrary code via a crafted DWG file.
CVE-2013-4011
Multiple unspecified vulnerabilities in the InfiniBand subsystem in IBM AIX 6.1 and 7.1, and VIOS 2.2.2.2-FP-26 SP-02, allow local users to gain privileges via vectors involving (1) arp.ib or (2) ibstat.
CVE-2013-4141
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2013-4125.  Reason: This candidate is a reservation duplicate of CVE-2013-4125.  Notes: All CVE users should reference CVE-2013-4125 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2013-4668
Directory traversal vulnerability in File Roller 3.6.x before 3.6.4, 3.8.x before 3.8.3, and 3.9.x before 3.9.3, when libarchive is used, allows remote attackers to create arbitrary files via a crafted archive that is not properly handled in a "Keep directory structure" action, related to fr-archive-libarchive.c and fr-window.c.
CVE-2013-4778
core/getLog.php on the Siemens Enterprise OpenScape Branch appliance and OpenScape Session Border Controller (SBC) before 2 R0.32.0, and 7 before 7 R1.7.0, allows remote attackers to obtain sensitive server and statistics information via unspecified vectors.
CVE-2013-4779
Cross-site scripting (XSS) vulnerability in core/handleTw.php on the Siemens Enterprise OpenScape Branch appliance and OpenScape Session Border Controller (SBC) before 2 R0.32.0, and 7 before 7 R1.7.0, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-4780
core/getLog.php on the Siemens Enterprise OpenScape Branch appliance and OpenScape Session Border Controller (SBC) before 2 R0.32.0, and 7 before 7 R1.7.0, allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2013-4781
core/getLog.php on the Siemens Enterprise OpenScape Branch appliance and OpenScape Session Border Controller (SBC) before 2 R0.32.0, and 7 before 7 R1.7.0, allows remote attackers to execute arbitrary commands via unspecified vectors.
CVE-2013-4869
Cisco Unified Communications Manager (CUCM) 7.1(x) through 9.1(2) and the IM & Presence Service in Cisco Unified Presence Server through 9.1(2) use the same CTI and database-encryption key across different customers' installations, which makes it easier for context-dependent attackers to defeat cryptographic protection mechanisms by leveraging knowledge of this key, aka Bug IDs CSCsc69187 and CSCui01756.  NOTE: the vendor has provided a statement that the "hard-coded static encryption key is considered a hardening issue rather than a vulnerability, and as such, has a CVSS score of 0/0."
CVE-2013-4872
Google Glass before XE6 does not properly restrict the processing of QR codes, which allows physically proximate attackers to modify the configuration or redirect users to arbitrary web sites via a crafted symbol, as demonstrated by selecting a Wi-Fi access point in order to conduct a man-in-the-middle attack.
CVE-2013-4873
The Yahoo! Tumblr app before 3.4.1 for iOS sends cleartext credentials, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2013-4874
The Uboot bootloader on the Verizon Wireless Network Extender SCS-26UC4 allows physically proximate attackers to obtain root access by connecting a crafted HDMI cable and using a sys session to modify the ramboot environment variable.
CVE-2013-4875
The Uboot bootloader on the Verizon Wireless Network Extender SCS-2U01 allows physically proximate attackers to bypass the intended boot process and obtain a login prompt by connecting a crafted HDMI cable and sending a SysReq interrupt.
CVE-2013-4876
The Verizon Wireless Network Extender SCS-2U01 has a hardcoded password for the root account, which makes it easier for physically proximate attackers to obtain administrative access by leveraging a login prompt.
CVE-2013-4877
The Verizon Wireless Network Extender SCS-26UC4 and SCS-2U01 does not use CAVE authentication, which makes it easier for remote attackers to obtain ESN and MIN values from arbitrary phones, and conduct cloning attacks, by sniffing the network for registration packets.
CVE-2013-4878
The default configuration of Parallels Plesk Panel 9.0.x and 9.2.x on UNIX, and Small Business Panel 10.x on UNIX, has an improper ScriptAlias directive for phppath, which makes it easier for remote attackers to execute arbitrary code via a crafted request, a different vulnerability than CVE-2012-1823.
