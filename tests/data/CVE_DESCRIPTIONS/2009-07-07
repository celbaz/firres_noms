CVE-2008-0015
Stack-based buffer overflow in the CComVariant::ReadFromStream function in the Active Template Library (ATL), as used in the MPEG2TuneRequest ActiveX control in msvidctl.dll in DirectShow, in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista Gold, SP1, and SP2, and Server 2008 Gold and SP2 allows remote attackers to execute arbitrary code via a crafted web page, as exploited in the wild in July 2009, aka "Microsoft Video ActiveX Control Vulnerability."
CVE-2008-0020
Unspecified vulnerability in the Load method in the IPersistStreamInit interface in the Active Template Library (ATL), as used in the Microsoft Video ActiveX control in msvidctl.dll in DirectShow, in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista Gold, SP1, and SP2, and Server 2008 Gold and SP2 allows remote attackers to execute arbitrary code via unknown vectors that trigger memory corruption, aka "ATL Header Memcopy Vulnerability," a different vulnerability than CVE-2008-0015.
CVE-2008-6848
Cross-site scripting (XSS) vulnerability in index.php in phpGreetCards 3.7 allows remote attackers to inject arbitrary web script or HTML via the category parameter in a select action.
CVE-2008-6849
Unrestricted file upload vulnerability in index.php in phpGreetCards 3.7 allows remote attackers to execute arbitrary PHP code by uploading a file with an executable extension, then accessing it via a via a link that is listed by userfiles/number_shell.php.
CVE-2008-6850
Cross-site scripting (XSS) vulnerability in messages.php in PHP-Fusion 6.01.17 and 7.00.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-6851
SQL injection vulnerability in page.php in PHP Link Directory (phpLD) 3.3, when register_globals is enabled and magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the name parameter.
CVE-2008-6852
SQL injection vulnerability in the Ice Gallery (com_ice) component 0.5 beta 2 for Joomla! allows remote attackers to execute arbitrary SQL commands via the catid parameter to index.php.
CVE-2008-6853
SQL injection vulnerability in modules/poll/index.php in AIST NetCat 3.0 and 3.12 allows remote attackers to execute arbitrary SQL commands via the PollID parameter.
CVE-2009-2337
SQL injection vulnerability in includes/module/book/index.inc.php in w3b|cms Gaestebuch Guestbook Module 3.0.0, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the spam_id parameter.
CVE-2009-2338
Directory traversal vulnerability in includes/startmodules.inc.php in FreeWebshop.org 2.2.9 R2, when register_globals is enabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the lang_file parameter.
CVE-2009-2339
SQL injection vulnerability in index.php in Rentventory allows remote attackers to execute arbitrary SQL commands via the product parameter.
CVE-2009-2340
SQL injection vulnerability in admin/index.php in Opial 1.0 allows remote attackers to execute arbitrary SQL commands via the txtUserName (aka User Name) parameter.  NOTE: some of these details are obtained from third party information.
CVE-2009-2341
SQL injection vulnerability in albumdetail.php in Opial 1.0 allows remote attackers to execute arbitrary SQL commands via the albumid parameter.
CVE-2009-2342
Cross-site scripting (XSS) vulnerability in admin.php (aka the login page) in Content Management Made Easy (CMME) before 1.22 allows remote attackers to inject arbitrary web script or HTML via the username field.
CVE-2009-2343
Cross-site scripting (XSS) vulnerability in people.php in Zoph before 0.7.0.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2009-2344
The web-based management interfaces in Sourcefire Defense Center (DC) and 3D Sensor before 4.8.2 allow remote authenticated users to gain privileges via a $admin value for the admin parameter in an edit action to admin/user/user.cgi and unspecified other components.
CVE-2009-2345
Multiple SQL injection vulnerabilities in ClanSphere before 2009.0.1 allow remote attackers to execute arbitrary SQL commands via unknown parameters to the gbook module and unspecified other components.
CVE-2009-2350
Microsoft Internet Explorer 6.0.2900.2180 and earlier does not block javascript: URIs in Refresh headers in HTTP responses, which allows remote attackers to conduct cross-site scripting (XSS) attacks via vectors related to (1) injecting a Refresh header or (2) specifying the content of a Refresh header, a related issue to CVE-2009-1312.
CVE-2009-2351
Opera 9.52 and earlier does not block javascript: URIs in Refresh headers in HTTP responses, which allows remote attackers to conduct cross-site scripting (XSS) attacks via vectors related to (1) injecting a Refresh header or (2) specifying the content of a Refresh header, a related issue to CVE-2009-1312. NOTE: it was later reported that 10.00 Beta 3 Build 1699 is also affected.
CVE-2009-2352
Google Chrome 1.0.154.48 and earlier does not block javascript: URIs in Refresh headers in HTTP responses, which allows remote attackers to conduct cross-site scripting (XSS) attacks via vectors related to (1) injecting a Refresh header or (2) specifying the content of a Refresh header, a related issue to CVE-2009-1312. NOTE: it was later reported that 2.0.172.28, 2.0.172.37, and 3.0.193.2 Beta are also affected.
CVE-2009-2353
encoder.php in eAccelerator allows remote attackers to execute arbitrary code by copying a local executable file to a location under the web root via the -o option, and then making a direct request to this file, related to upload of image files.
CVE-2009-2354
SQL injection vulnerability in the auth_checkpass function in the login page in NullLogic Groupware 1.2.7 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2009-2355
The forum module in NullLogic Groupware 1.2.7 allows remote authenticated users to cause a denial of service (application crash) by specifying (1) an empty string or (2) a non-numeric string when selecting a forum, related to the fmessagelist function.
CVE-2009-2356
Multiple stack-based buffer overflows in the pgsqlQuery function in NullLogic Groupware 1.2.7, when PostgreSQL is used, might allow remote attackers to execute arbitrary code via input to the (1) POP3, (2) SMTP, or (3) web component that triggers a long SQL query.
CVE-2009-2357
The default configuration of TekRADIUS 3.0 uses the sa account to communicate with Microsoft SQL Server, which makes it easier for remote attackers to obtain privileged access to the database and the underlying Windows operating system.
CVE-2009-2358
TekRADIUS 3.0 uses BUILTIN\Users:R permissions for the TekRADIUS.ini file, which allows local users to obtain obfuscated database credentials by reading this file.
CVE-2009-2359
Multiple SQL injection vulnerabilities in TekRADIUS 3.0 allow context-dependent attackers to execute arbitrary SQL commands via (1) the GUI client, as demonstrated by input to the Browse Users text box in the Users tab; or (2) the command-line client, as demonstrated by a certain trcli -r command.
