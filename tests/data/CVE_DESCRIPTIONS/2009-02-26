CVE-2008-4308
The doRead method in Apache Tomcat 4.1.32 through 4.1.34 and 5.5.10 through 5.5.20 does not return a -1 to indicate when a certain error condition has occurred, which can cause Tomcat to send POST content from one request to a different request.
CVE-2008-5263
Multiple stack-based buffer overflows in the mt_codec::getHdrHead function in kernel/kls_hdr/fmt_codec_hdr.cpp in ksquirrel-libs 0.8.0 allow context-dependent attackers to execute arbitrary code via a crafted Radiance RGBE image (aka .hdr file).
CVE-2008-6289
SQL injection vulnerability in cityview.php in Tours Manager 1.0 allows remote attackers to execute arbitrary SQL commands via the cityid parameter.
CVE-2008-6290
Directory traversal vulnerability in includefile.php in nicLOR Sito, when register_globals is enabled or magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary files via a .. (dot dot) in the page_file parameter.
CVE-2008-6291
Acc PHP eMail 1.1 allows remote attackers to bypass authentication and gain administrative access by setting the NEWSLETTERLOGIN cookie to "admin".
CVE-2008-6292
Acc Autos 4.0 allows remote attackers to bypass authentication and gain administrative access by setting the (1) username_cookie to "admin," (2) right_cookie to "1," and (3) id_cookie to "1."
CVE-2008-6293
admin/Index.php in Acc Real Estate 4.0 allows remote attackers to bypass authentication and gain administrative access by setting the username_cookie to "admin."
CVE-2008-6294
admin/Index.php in Acc Statistics 1.1 allows remote attackers to bypass authentication and gain administrative access by setting the username_cookie cookie to "admin."
CVE-2008-6295
Multiple cross-site scripting (XSS) vulnerabilities in Camera Life 2.6.2b8 allow remote attackers to inject arbitrary web script or HTML via the q parameter to (1) search.php and (2) rss.php; the query string after the image name in (3) photos/photo; the path parameter to (4) folder.php; page parameter and REQUEST_URI to (5) login.php; ver parameter to (6) media.php; theme parameter to (7) modules/iconset/iconset-debug.php; and the REQUEST_URI to (8) index.php.
CVE-2008-6296
admin.php in Maran PHP Shop allows remote attackers to bypass authentication and gain administrative access by setting the user cookie to "demo."
CVE-2008-6297
Cross-site scripting (XSS) vulnerability in order.php in DHCart allows remote attackers to inject arbitrary web script or HTML via the (1) domain and (2) d1 parameters.
CVE-2008-6298
Unspecified vulnerability in sISAPILocation before 1.0.2.2 allows remote attackers to bypass intended access restrictions for character encoding and the cookie secure flag via unknown vectors related to the "HTTP header rewrite function."
CVE-2008-6299
Multiple cross-site scripting (XSS) vulnerabilities in Joomla! 1.5.7 and earlier allow remote authenticated users with certain privileges to inject arbitrary web script or HTML via (1) the title and description parameters to the com_weblinks module and (2) unspecified vectors in the com_content module related to "article submission."
CVE-2008-6300
Galatolo WebManager 1.3a allows remote attackers to bypass authentication and gain administrative access by setting the (1) gwm_user and (2) gwm_pass cookies to admin.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6301
SQL injection vulnerability in shoutbox_view.php in the Small ShoutBox module 1.4 for phpBB allows remote attackers to execute arbitrary SQL commands via the id parameter in a delete action.
CVE-2008-6302
TurnkeyForms Local Classifieds allows remote attackers to bypass authentication and gain administrative access via a direct request to Site_Admin/admin.php.
CVE-2008-6303
SQL injection vulnerability in tourview.php in ToursManager allows remote attackers to execute arbitrary SQL commands via the tourid parameter.
CVE-2008-6304
SQL injection vulnerability in xt:Commerce before 3.0.4 Sp2.1, when magic_quotes_gpc is enabled and the SEO URLs are activated, allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2008-6305
PHP remote file inclusion vulnerability in init.php in Free Directory Script 1.1.1, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the API_HOME_DIR parameter.
CVE-2008-6306
Cross-site scripting (XSS) vulnerability in signinform.php in Softbiz Classifieds Script allows remote attackers to inject arbitrary web script or HTML via the msg parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6307
E-topbiz Link Back Checker 1 allows remote attackers to bypass authentication and gain administrative access by setting the auth cookie to "admin."
CVE-2009-0114
Unspecified vulnerability in the Settings Manager in Adobe Flash Player 9.x before 9.0.159.0 and 10.x before 10.0.22.87, and possibly other versions, allows remote attackers to trick a user into visiting an arbitrary URL via unknown vectors, related to "a potential Clickjacking issue variant."
CVE-2009-0187
Stack-based buffer overflow in Orbit Downloader 2.8.2 and 2.8.3, and possibly other versions before 2.8.5, allows remote attackers to execute arbitrary code via a crafted HTTP URL with a long host name, which is not properly handled when constructing a "Connecting" log message.
CVE-2009-0208
Unspecified vulnerability in HP Virtual Rooms Client before 7.0.1, when running on Windows, allows remote attackers to execute arbitrary code via unknown vectors.
CVE-2009-0507
IBM WebSphere Process Server (WPS) 6.1.2 before 6.1.2.3 and 6.2 before 6.2.0.1 does not properly restrict configuration data during an export of the cluster configuration file from the administrative console, which allows remote authenticated users to obtain the (1) JMSAPI, (2) ESCALATION, and (3) MAILSESSION (aka mail session) cleartext passwords via vectors involving access to a cluster member.
CVE-2009-0519
Unspecified vulnerability in Adobe Flash Player 9.x before 9.0.159.0 and 10.x before 10.0.22.87 allows remote attackers to cause a denial of service (browser crash) or possibly execute arbitrary code via a crafted Shockwave Flash (aka .swf) file.
CVE-2009-0520
Adobe Flash Player 9.x before 9.0.159.0 and 10.x before 10.0.22.87 does not properly remove references to destroyed objects during Shockwave Flash file processing, which allows remote attackers to execute arbitrary code via a crafted file, related to a "buffer overflow issue."
CVE-2009-0521
Untrusted search path vulnerability in Adobe Flash Player 9.x before 9.0.159.0 and 10.x before 10.0.22.87 on Linux allows local users to obtain sensitive information or gain privileges via a crafted library in a directory contained in the RPATH.
http://www.adobe.com/support/security/bulletins/apsb09-01.html

"This update prevents a potential Linux-only information disclosure issue in the Flash Player binary that could lead to privilege escalation. (CVE-2009-0521)"
CVE-2009-0522
Adobe Flash Player 9.x before 9.0.159.0 and 10.x before 10.0.22.87 on Windows allows remote attackers to trick a user into visiting an arbitrary URL via an unspecified manipulation of the "mouse pointer display," related to a "Clickjacking attack."
Per: http://www.adobe.com/support/security/bulletins/apsb09-01.html

"This update resolves a Windows-only issue with mouse pointer display that could potentially contribute to a Clickjacking attack. (CVE-2009-0522)"
CVE-2009-0523
Cross-site scripting (XSS) vulnerability in Adobe RoboHelp Server 6 and 7 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, which is not properly handled when displaying the Help Errors log.
CVE-2009-0524
Cross-site scripting (XSS) vulnerability in Adobe RoboHelp 6 and 7, and RoboHelp Server 6 and 7, allows remote attackers to inject arbitrary web script or HTML via vectors involving files produced by RoboHelp.
CVE-2009-0614
Unspecified vulnerability in the Web Server in Cisco Unified MeetingPlace Web Conferencing 6.0 before 6.0(517.0) (aka 6.0 MR4) and 7.0 before 7.0(2) (aka 7.0 MR1) allows remote attackers to bypass authentication and obtain administrative access via a crafted URL.
CVE-2009-0615
Directory traversal vulnerability in Cisco Application Networking Manager (ANM) before 2.0 and Application Control Engine (ACE) Device Manager before A3(2.1) allows remote authenticated users to read or modify arbitrary files via unspecified vectors, related to "invalid directory permissions."
CVE-2009-0616
Cisco Application Networking Manager (ANM) before 2.0 uses default usernames and passwords, which makes it easier for remote attackers to access the application, or cause a denial of service via configuration changes, related to "default user credentials during installation."
CVE-2009-0617
Cisco Application Networking Manager (ANM) before 2.0 uses a default MySQL root password, which makes it easier for remote attackers to execute arbitrary operating-system commands or change system files.
CVE-2009-0618
Unspecified vulnerability in the Java agent in Cisco Application Networking Manager (ANM) before 2.0 Update A allows remote attackers to gain privileges, and cause a denial of service (service outage) by stopping processes, or obtain sensitive information by reading configuration files.
CVE-2009-0620
Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers before A2(1.1) uses default (1) usernames and (2) passwords for (a) the administrator and (b) web management, which makes it easier for remote attackers to perform configuration changes or obtain operating-system access.
CVE-2009-0621
Cisco ACE 4710 Application Control Engine Appliance before A1(8a) uses default (1) usernames and (2) passwords for (a) the administrator, (b) web management, and (c) device management, which makes it easier for remote attackers to perform configuration changes to the Device Manager and other components, or obtain operating-system access.
CVE-2009-0622
Unspecified vulnerability in Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers before A2(1.2) and Cisco ACE 4710 Application Control Engine Appliance before A1(8a) allows remote authenticated users to execute arbitrary operating-system commands through a command line interface (CLI).
Per: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a7bc82.shtml

Cisco ACE module software can be downloaded from:

http://tools.cisco.com/support/downloads/go/Redirect.x?mdfid=280557289

Cisco ACE 4710 Application Control Engine appliance software can be downloaded from:

http://tools.cisco.com/support/downloads/go/Redirect.x?mdfid=281222179
CVE-2009-0623
Unspecified vulnerability in Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers before A2(1.3) and Cisco ACE 4710 Application Control Engine Appliance before A3(2.1) allows remote attackers to cause a denial of service (device reload) via a crafted SSH packet.
CVE-2009-0624
Unspecified vulnerability in the SNMPv2c implementation in Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers before A2(1.3) and Cisco ACE 4710 Application Control Engine Appliance before A3(2.1) allows remote attackers to cause a denial of service (device reload) via a crafted SNMPv1 packet.
Per: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a7bc82.shtml

"Note: SNMPv2c must be explicitly configured in an affected device in order to process any SNMPv2c transactions. SNMPv2c is not enabled by default."
CVE-2009-0625
Unspecified vulnerability in Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers before A2(1.2) and Cisco ACE 4710 Application Control Engine Appliance before A1(8.0) allows remote attackers to cause a denial of service (device reload) via a crafted SNMPv3 packet.
CVE-2009-0742
The username command in Cisco ACE Application Control Engine Module for Catalyst 6500 Switches and 7600 Routers and Cisco ACE 4710 Application Control Engine Appliance stores a cleartext password by default, which allows context-dependent attackers to obtain sensitive information.
Note that CVE-2009-0742 is not referenced on the vendor advisory page at:

http://www.cisco.com/en/US/products/products_security_advisory09186a0080a7bc82.shtml
