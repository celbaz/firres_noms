CVE-2013-6494
fedup 0.9.0 in Fedora 19, 20, and 21 uses a temporary directory with a static name for its download cache, which allows local users to cause a denial of service (prevention of system updates).
CVE-2014-3065
Unspecified vulnerability in IBM Java Runtime Environment (JRE) 7 R1 before SR2 (7.1.2.0), 7 before SR8 (7.0.8.0), 6 R1 before SR8 FP2 (6.1.8.2), 6 before SR16 FP2 (6.0.16.2), and before SR16 FP8 (5.0.16.8) allows local users to execute arbitrary code via vectors related to the shared classes cache.
CVE-2014-3068
IBM Java Runtime Environment (JRE) 7 R1 before SR1 FP1 (7.1.1.1), 7 before SR7 FP1 (7.0.7.1), 6 R1 before SR8 FP1 (6.1.8.1), 6 before SR16 FP1 (6.0.16.1), and before 5.0 SR16 FP7 (5.0.16.7) allows attackers to obtain the private key from a Certificate Management System (CMS) keystore via a brute force attack.
CVE-2014-3703
OpenStack PackStack 2012.2.1, when the Open vSwitch (OVS) monolithic plug-in is not used, does not properly set the libvirt_vif_driver configuration option when generating the nova.conf configuration, which causes the firewall to be disabled and allows remote attackers to bypass intended access restrictions.
CVE-2014-5284
host-deny.sh in OSSEC before 2.8.1 writes to temporary files with predictable filenames without verifying ownership, which allows local users to modify access restrictions in hosts.deny and gain root privileges by creating the temporary files before automatic IP blocking is performed.
CVE-2014-8728
SQL injection vulnerability in the login page (login/login) in Subex ROC Fraud Management (aka Fraud Management System and FMS) 7.4 and earlier allows remote attackers to execute arbitrary SQL commands via the ranger_user[name] parameter.
CVE-2014-8754
Open redirect vulnerability in track-click.php in the Ad-Manager plugin 1.1.2 for WordPress allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the out parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html" target="_blank">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2014-8788
GleamTech FileVista before 6.1 allows remote authenticated users to obtain sensitive information via a crafted path when saving a zip file, which reveals the installation path in an error message.
CVE-2014-8789
GleamTech FileVista before 6.1 allows remote authenticated users to create arbitrary files and possibly execute arbitrary code via a crafted path in a zip archive, which is not properly handled during extraction.
CVE-2014-8791
project/register.php in Tuleap before 7.7, when sys_create_project_in_one_step is disabled, allows remote authenticated users to conduct PHP object injection attacks and execute arbitrary PHP code via the data parameter.
CVE-2014-8874
The ke_questionnaire extension 2.5.2 and earlier for TYPO3 uses predictable names for the questionnaire answer forms, which makes it easier for remote attackers to obtain sensitive information via a direct request.
CVE-2014-9112
Heap-based buffer overflow in the process_copy_in function in GNU Cpio 2.11 allows remote attackers to cause a denial of service via a large block value in a cpio archive.
CVE-2014-9113
CCH Wolters Kluwer ProSystem fx Engagement (aka PFX Engagement) 7.1 and earlier uses weak permissions (Authenticated Users: Modify and Write) for the (1) Pfx.Engagement.WcfServices, (2) PFXEngDesktopService, (3) PFXSYNPFTService, and (4) P2EWinService service files in PFX Engagement\, which allows local users to obtain LocalSystem privileges via a Trojan horse file.
CVE-2014-9116
The write_one_header function in mutt 1.5.23 does not properly handle newline characters at the beginning of a header, which allows remote attackers to cause a denial of service (crash) via a header with an empty body, which triggers a heap-based buffer overflow in the mutt_substrdup function.
CVE-2014-9173
SQL injection vulnerability in view.php in the Google Doc Embedder plugin before 2.5.15 for WordPress allows remote attackers to execute arbitrary SQL commands via the gpid parameter.
CVE-2014-9174
Cross-site scripting (XSS) vulnerability in the Google Analytics by Yoast (google-analytics-for-wordpress) plugin before 5.1.3 for WordPress allows remote attackers to inject arbitrary web script or HTML via the "Manually enter your UA code" (manual_ua_code_field) field in the General Settings.
CVE-2014-9175
SQL injection vulnerability in wpdatatables.php in the wpDataTables plugin 1.5.3 and earlier for WordPress allows remote attackers to execute arbitrary SQL commands via the table_id parameter in a get_wdtable action to wp-admin/admin-ajax.php.
CVE-2014-9176
Cross-site scripting (XSS) vulnerability in the InstaSqueeze Sexy Squeeze Pages plugin for WordPress allows remote attackers to inject arbitrary web script or HTML via the id parameter to lp/index.php.
CVE-2014-9177
The HTML5 MP3 Player with Playlist Free plugin before 2.7 for WordPress allows remote attackers to obtain the installation path via a request to html5plus/playlist.php.
CVE-2014-9178
Multiple SQL injection vulnerabilities in classes/ajax.php in the Smarty Pants Plugins SP Project & Document Manager plugin (sp-client-document-manager) 2.4.1 and earlier for WordPress allow remote attackers to execute arbitrary SQL commands via the (1) vendor_email[] parameter in the email_vendor function or id parameter in the (2) download_project, (3) download_archive, or (4) remove_cat function.
CVE-2014-9179
Cross-site scripting (XSS) vulnerability in the SupportEzzy Ticket System plugin 1.2.5 for WordPress allows remote authenticated users to inject arbitrary web script or HTML via the "URL (optional)" field in a new ticket.
CVE-2014-9180
Open redirect vulnerability in go.php in Eleanor CMS allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the QUERY_STRING.
<a href="http://cwe.mitre.org/data/definitions/601.html" target="_blank">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2014-9181
Multiple directory traversal vulnerabilities in Plex Media Server before 0.9.9.3 allow remote attackers to read arbitrary files via a .. (dot dot) in the URI to (1) manage/ or (2) web/ or remote authenticated users to read arbitrary files via a .. (dot dot) in the URI to resources/.
CVE-2014-9182
models/comment.php in Anchor CMS 0.9.2 and earlier allows remote attackers to inject arbitrary headers into mail messages via a crafted Host: header.
CVE-2014-9183
ZTE ZXDSL 831CII has a default password of admin for the admin account, which allows remote attackers to gain administrator privileges.
CVE-2014-9184
ZTE ZXDSL 831CII allows remote attackers to bypass authentication via a direct request to (1) main.cgi, (2) adminpasswd.cgi, (3) userpasswd.cgi, (4) upload.cgi, (5) conprocess.cgi, or (6) connect.cgi.
