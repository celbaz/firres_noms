CVE-2016-10349
The archive_le32dec function in archive_endian.h in libarchive 3.2.2 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted file.
CVE-2016-10350
The archive_read_format_cab_read_header function in archive_read_support_format_cab.c in libarchive 3.2.2 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted file.
CVE-2016-10351
Telegram Desktop 0.10.19 uses 0755 permissions for $HOME/.TelegramDesktop, which allows local users to obtain sensitive authentication information via standard filesystem operations.
CVE-2016-8649
lxc-attach in LXC before 1.0.9 and 2.x before 2.0.6 allows an attacker inside of an unprivileged container to use an inherited file descriptor, of the host's /proc, to access the rest of the host's filesystem via the openat() family of syscalls.
CVE-2017-5631
An issue was discovered in KMCIS CaseAware. Reflected cross site scripting is present in the user parameter (i.e., "usr") that is transmitted in the login.php query string.
CVE-2017-6128
An attacker may be able to cause a denial-of-service (DoS) attack against the sshd component in F5 BIG-IP, Enterprise Manager, BIG-IQ, and iWorkflow.
CVE-2017-6519
avahi-daemon in Avahi through 0.6.32 and 0.7 inadvertently responds to IPv6 unicast queries with source addresses that are not on-link, which allows remote attackers to cause a denial of service (traffic amplification) and may cause information leakage by obtaining potentially sensitive  information from the responding device via port-5353 UDP packets.  NOTE: this may overlap CVE-2015-2809.
CVE-2017-6520
The Multicast DNS (mDNS) responder used in BOSE Soundtouch 30 inadvertently responds to IPv4 unicast queries with source addresses that are not link-local, which allows remote attackers to cause a denial of service (traffic amplification) or obtain potentially sensitive information via port-5353 UDP packets.
CVE-2017-6564
On Franklin Fueling Systems TS-550 evo 2.3.0.7332 devices, the Guest user, which contains the lowest privileges, can post to the idSourceFileName parameter found within the /download directory. This ability allows for an attacker to download sensitive system files from the host machine such as databases which contain information that can aid in further attacks.
CVE-2017-6565
On Franklin Fueling Systems TS-550 evo 2.3.0.7332 devices, the roleDiag user, which can be obtained by exploiting CVE-2013-7247, has the ability to upload files to the server hosting the web service. As no sanitization checks are in place, an attacker can upload a malicious payload.
CVE-2017-8372
The mad_layer_III function in layer3.c in Underbit MAD libmad 0.15.1b, if NDEBUG is omitted, allows remote attackers to cause a denial of service (assertion failure and application exit) via a crafted audio file.
CVE-2017-8373
The mad_layer_III function in layer3.c in Underbit MAD libmad 0.15.1b allows remote attackers to cause a denial of service (heap-based buffer overflow and application crash) or possibly have unspecified other impact via a crafted audio file.
CVE-2017-8374
The mad_bit_skip function in bit.c in Underbit MAD libmad 0.15.1b allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted audio file.
CVE-2017-8376
GeniXCMS 1.0.2 has XSS triggered by an authenticated comment that is mishandled during a mouse operation by an administrator.
CVE-2017-8377
GeniXCMS 1.0.2 has SQL Injection in inc/lib/Control/Backend/menus.control.php via the menuid parameter.
CVE-2017-8378
Heap-based buffer overflow in the PdfParser::ReadObjects function in base/PdfParser.cpp in PoDoFo 0.9.5 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via vectors related to m_offsets.size.
CVE-2017-8383
Craft CMS before 2.6.2976 does not properly restrict viewing the contents of files in the craft/app/ folder.
CVE-2017-8384
Craft CMS before 2.6.2976 allows XSS attacks because an array returned by HttpRequestService::getSegments() and getActionSegments() need not be zero-based. NOTE: this vulnerability exists because of an incomplete fix for CVE-2017-8052.
CVE-2017-8385
Craft CMS before 2.6.2976 does not prevent modification of the URL in a forgot-password email message.
CVE-2017-8388
GeniXCMS 1.0.2 allows remote attackers to bypass the alertDanger MSG_USER_EMAIL_EXIST protection mechanism via a register.php?act=edit&id=1 request.
CVE-2017-8392
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to an invalid read of size 8 because of missing a check to determine whether symbols are NULL in the _bfd_dwarf2_find_nearest_line function. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objdump, to crash.
CVE-2017-8393
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to a global buffer over-read error because of an assumption made by code that runs for objcopy and strip, that SHT_REL/SHR_RELA sections are always named starting with a .rel/.rela prefix. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objcopy and strip, to crash.
CVE-2017-8394
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to an invalid read of size 4 due to NULL pointer dereferencing of _bfd_elf_large_com_section. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objcopy, to crash.
CVE-2017-8395
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to an invalid write of size 8 because of missing a malloc() return-value check to see if memory had actually been allocated in the _bfd_generic_get_section_contents function. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objcopy, to crash.
CVE-2017-8396
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to an invalid read of size 1 because the existing reloc offset range tests didn't catch small negative offsets less than the size of the reloc field. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objdump, to crash.
CVE-2017-8397
The Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, is vulnerable to an invalid read of size 1 and an invalid write of size 1 during processing of a corrupt binary containing reloc(s) with negative addresses. This vulnerability causes programs that conduct an analysis of binary programs using the libbfd library, such as objdump, to crash.
CVE-2017-8398
dwarf.c in GNU Binutils 2.28 is vulnerable to an invalid read of size 1 during dumping of debug information from a corrupt binary. This vulnerability causes programs that conduct an analysis of binary programs, such as objdump and readelf, to crash.
CVE-2017-8399
PCRE2 before 10.30 has an out-of-bounds write caused by a stack-based buffer overflow in pcre2_match.c, related to a "pattern with very many captures."
CVE-2017-8400
In SWFTools 0.9.2, an out-of-bounds write of heap data can occur in the function png_load() in lib/png.c:755. This issue can be triggered by a malformed PNG file that is mishandled by png2swf. Attackers could exploit this issue for DoS; it might cause arbitrary code execution.
CVE-2017-8401
In SWFTools 0.9.2, an out-of-bounds read of heap data can occur in the function png_load() in lib/png.c:724. This issue can be triggered by a malformed PNG file that is mishandled by png2swf. Attackers could exploit this issue for DoS.
CVE-2017-8403
360fly 4K cameras allow unauthenticated Wi-Fi password changes and complete access with REST by using the Bluetooth Low Energy pairing procedure, which is available at any time and does not require a password. This affects firmware 2.1.4. Exploitation can use the 360fly Android or iOS application, or the BlueZ gatttool program.
