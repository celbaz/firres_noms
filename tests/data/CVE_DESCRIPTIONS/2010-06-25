CVE-2009-4903
Cross-site scripting (XSS) vulnerability in index.php in oBlog allows remote attackers to inject arbitrary web script or HTML via the search parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-4904
article.php in oBlog does not properly restrict comments, which allows remote attackers to cause a denial of service (blog spam) via a comment=new action.
CVE-2009-4905
Multiple cross-site request forgery (CSRF) vulnerabilities in index.php in Acc Statistics 1.1 allow remote attackers to hijack the authentication of administrators for requests that change (1) passwords, (2) usernames, and (3) e-mail addresses.
CVE-2009-4906
Cross-site request forgery (CSRF) vulnerability in index.php in Acc PHP eMail 1.1 allows remote attackers to hijack the authentication of administrators for requests that change passwords.
CVE-2009-4907
Multiple cross-site request forgery (CSRF) vulnerabilities in oBlog allow remote attackers to hijack the authentication of administrators for requests that (1) change the admin password, (2) force an admin logout, (3) change the visibility of posts, (4) remove links, and (5) change the name fields of a blog.
CVE-2009-4908
Multiple cross-site scripting (XSS) vulnerabilities in oBlog allow remote attackers to inject arbitrary web script or HTML via the (1) commentName, (2) commentEmail, (3) commentWeb, or (4) commentText parameter to article.php; and allow remote authenticated administrators to inject arbitrary web script or HTML via the (5) article_id or (6) title parameter to admin/write.php, the (7) category_id or (8) category_name parameter to admin/groups.php, the (9) blogroll_id or (10) title parameter to admin/blogroll.php, or the (11) blog_name or (12) tag_line parameter to admin/settings.php.
CVE-2009-4909
admin/index.php in oBlog allows remote attackers to conduct brute-force password guessing attacks via HTTP requests.
CVE-2010-1206
The startDocumentLoad function in browser/base/content/browser.js in Mozilla Firefox 3.5.x before 3.5.11 and 3.6.x before 3.6.7, and SeaMonkey before 2.0.6, does not properly implement the Same Origin Policy in certain circumstances related to the about:blank document and a document that is currently loading, which allows (1) remote web servers to conduct spoofing attacks via vectors involving a 204 (aka No Content) status code, and allows (2) remote attackers to conduct spoofing attacks via vectors involving a window.stop call.
CVE-2010-2434
Buffer overflow in Arcext.dll 2.16.1 and earlier in pon software Explzh 5.62 and earlier allows remote attackers to execute arbitrary code via an LZH LHA file with a crafted header that is not properly handled during expansion.
CVE-2010-2444
parse/Csv2_parse.c in MaraDNS 1.3.03, and other versions before 1.4.03, does not properly handle hostnames that do not end in a "." (dot) character, which allows remote attackers to cause a denial of service (NULL pointer dereference) via a crafted csv2 zone file.
Per: http://cwe.mitre.org/data/definitions/476.html

'NULL Pointer Dereference'
CVE-2010-2454
Apple Safari does not properly manage the address bar between the request to open a URL and the retrieval of the new document's content, which might allow remote attackers to conduct spoofing attacks via a crafted HTML document, a related issue to CVE-2010-1206.
CVE-2010-2455
Opera does not properly manage the address bar between the request to open a URL and the retrieval of the new document's content, which might allow remote attackers to conduct spoofing attacks via a crafted HTML document, a related issue to CVE-2010-1206.
CVE-2010-2456
Multiple directory traversal vulnerabilities in index.php in Linker IMG 1.0 and earlier allow remote attackers to read and execute arbitrary local files via a URL in the (1) cook_lan cookie parameter ($lan_dir variable) or possibly (2) Sdb_type parameter.  NOTE: this was originally reported as remote file inclusion, but this may be inaccurate.
CVE-2010-2457
Cross-site scripting (XSS) vulnerability in index.php in K-Search allows remote attackers to inject arbitrary web script or HTML via the term parameter.
CVE-2010-2458
Cross-site scripting (XSS) vulnerability in video.php in 2daybiz Video Community Portal Script 1.0 allows remote attackers to inject arbitrary web script or HTML via the videoid parameter.
CVE-2010-2459
SQL injection vulnerability in video.php in 2daybiz Video Community Portal Script 1.0 allows remote attackers to execute arbitrary SQL commands via the videoid parameter.
CVE-2010-2460
SQL injection vulnerability in merchant_product_list.php in JCE-Tech Shareasale Script (SASS) 1 allows remote attackers to execute arbitrary SQL commands via the mechant_id parameter.
CVE-2010-2461
SQL injection vulnerability in storecat.php in JCE-Tech Overstock 1 allows remote attackers to execute arbitrary SQL commands via the store parameter.
CVE-2010-2462
SQL injection vulnerability in withdraw_money.php in Toma Cero OroHYIP allows remote attackers to execute arbitrary SQL commands via the id parameter in a cancel action.
CVE-2010-2463
Cross-site scripting (XSS) vulnerability in forum.php in Jamroom before 4.1.9 allows remote attackers to inject arbitrary web script or HTML via the post_id parameter in a modify action.
CVE-2010-2464
Multiple cross-site scripting (XSS) vulnerabilities in the RSComments (com_rscomments) component 1.0.0 Rev 2 for Joomla! allow remote attackers to inject arbitrary web script or HTML via the (1) website and (2) name parameters to index.php.
CVE-2010-2465
The S2 Security NetBox 2.5, 3.3, and 4.0, as used in the Linear eMerge 50 and 5000 and the Sonitrol eAccess, stores sensitive information under the web root with insufficient access control, which allows remote attackers to download node logs, photographs of persons, and backup files via unspecified HTTP requests.
Per:  http://www.kb.cert.org/vuls/id/MAPG-83TQL8

'Vendor Statement
S2 Security has made available patches or upgrades available to address this vulnerability in all versions of our product (2.5, 3.3, and 4.0). These are available through Linear Customer Support or S2 Customer Support'
CVE-2010-2466
The S2 Security NetBox, possibly 2.x and 3.x, as used in the Linear eMerge 50 and 5000 and the Sonitrol eAccess, does not properly prevent downloading of database backups, which allows remote attackers to obtain sensitive information via requests for full_*.dar files with predictable filenames.
CVE-2010-2467
The S2 Security NetBox, possibly 2.x and 3.x, as used in the Linear eMerge 50 and 5000 and the Sonitrol eAccess, does not require setting a password for the FTP server that stores database backups, which makes it easier for remote attackers to download backup files via unspecified FTP requests.
CVE-2010-2468
The S2 Security NetBox 2.x and 3.x, as used in the Linear eMerge 50 and 5000 and the Sonitrol eAccess, uses a weak hash algorithm for storing the Administrator password, which makes it easier for context-dependent attackers to obtain privileged access by recovering the cleartext of this password.
CVE-2010-2469
The Linear eMerge 50 and 5000 uses a default password of eMerge for the IEIeMerge account, which makes it easier for remote attackers to obtain Video Recorder data by establishing a session to the device.
