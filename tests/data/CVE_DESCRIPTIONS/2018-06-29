CVE-2018-10860
perl-archive-zip is vulnerable to a directory traversal in Archive::Zip. It was found that the Archive::Zip module did not properly sanitize paths while extracting zip files. An attacker able to provide a specially crafted archive for processing could use this flaw to write or overwrite arbitrary files in the context of the perl interpreter.
CVE-2018-12464
A SQL injection vulnerability in the web administration and quarantine components of Micro Focus Secure Messaging Gateway allows an unauthenticated remote attacker to execute arbitrary SQL statements against the database. This can be exploited to create an administrative account and used in conjunction with CVE-2018-12465 to achieve unauthenticated remote code execution. Affects Micro Focus Secure Messaging Gateway versions prior to 471. It does not affect previous versions of the product that use the GWAVA product name (i.e. GWAVA 6.5).
CVE-2018-12465
An OS command injection vulnerability in the web administration component of Micro Focus Secure Messaging Gateway (SMG) allows a remote attacker authenticated as a privileged user to execute arbitrary OS commands on the SMG server. This can be exploited in conjunction with CVE-2018-12464 to achieve unauthenticated remote code execution. Affects Micro Focus Secure Messaging Gateway versions prior to 471. It does not affect previous versions of the product that used GWAVA product name (i.e. GWAVA 6.5).
CVE-2018-12971
EasyCMS 1.3 has CSRF via the index.php?s=/admin/user/delAll URI to delete users.
CVE-2018-12972
An issue was discovered in OpenTSDB 2.3.0. Many parameters to the /q URI can execute commands, including o, key, style, and yrange and y2range and their JSON input.
CVE-2018-12973
An issue was discovered in OpenTSDB 2.3.0. There is XSS in parameter 'json' to the /q URI.
CVE-2018-12982
Invalid memory read in the PoDoFo::PdfVariant::DelayedLoad() function in PdfVariant.h in PoDoFo 0.9.6-rc1 allows remote attackers to have denial-of-service impact via a crafted file.
CVE-2018-12983
A stack-based buffer over-read in the PdfEncryptMD5Base::ComputeEncryptionKey() function in PdfEncrypt.cpp in PoDoFo 0.9.6-rc1 could be leveraged by remote attackers to cause a denial-of-service via a crafted pdf file.
CVE-2018-12984
Hycus CMS 1.0.4 allows Authentication Bypass via "'=' 'OR'" credentials.
CVE-2018-12988
GreenCMS 2.3.0603 has an arbitrary file download vulnerability via an index.php?m=admin&c=media&a=downfile URI.
CVE-2018-12992
An issue was discovered CMS MaeloStore V.1.5.0. There is stored XSS in the Telephone field of the admin interface.
CVE-2018-12993
onefilecms.php in OneFileCMS through 2012-04-14 might allow attackers to conduct brute-force attacks via the onefilecms_username and onefilecms_password fields.
CVE-2018-12994
onefilecms.php in OneFileCMS through 2012-04-14 might allow attackers to execute arbitrary PHP code via a .php filename on the New File screen.
CVE-2018-12995
onefilecms.php in OneFileCMS through 2012-04-14 might allow attackers to execute arbitrary PHP code via a .php filename on the Upload screen.
CVE-2018-12996
A reflected Cross-site scripting (XSS) vulnerability in Zoho ManageEngine Applications Manager before 13 (Build 13800) allows remote attackers to inject arbitrary web script or HTML via the parameter 'method' to GraphicalView.do.
CVE-2018-12997
Incorrect Access Control in FailOverHelperServlet in Zoho ManageEngine Netflow Analyzer before build 123137, Network Configuration Manager before build 123128, OpManager before build 123148, OpUtils before build 123161, and Firewall Analyzer before build 123147 allows attackers to read certain files on the web server without login by sending a specially crafted request to the server with the operation=copyfile&fileName= substring.
CVE-2018-12998
A reflected Cross-site scripting (XSS) vulnerability in Zoho ManageEngine Netflow Analyzer before build 123137, Network Configuration Manager before build 123128, OpManager before build 123148, OpUtils before build 123161, and Firewall Analyzer before build 123147 allows remote attackers to inject arbitrary web script or HTML via the parameter 'operation' to /servlet/com.adventnet.me.opmanager.servlet.FailOverHelperServlet.
CVE-2018-12999
Incorrect Access Control in AgentTrayIconServlet in Zoho ManageEngine Desktop Central 10.0.255 allows attackers to delete certain files on the web server without login by sending a specially crafted request to the server with a computerName=../ substring to the /agenttrayicon URI.
CVE-2018-13000
An XSS issue was discovered in Advanced Electron Forum (AEF) v1.0.9. A persistent XSS vulnerability is located in the `FTP Link` element of the `Private Message` module. The editor of the private message module allows inserting links without sanitizing the content. This allows remote attackers to inject malicious script code payloads as a private message (aka pmbody). The injection point is the editor ftp link element and the execution point occurs in the message body context on arrival. The request method to inject is POST with restricted user privileges.
CVE-2018-13001
An XSS issue was discovered in Sandoba CP:Shop v2016.1. The vulnerability is located in the `admin.php` file of the `./cpshop/` module. Remote attackers are able to inject their own script codes to the client-side requested vulnerable web-application parameters. The attack vector of the vulnerability is non-persistent and the request method to inject/execute is GET with the path, search, rename, or dir parameter.
CVE-2018-13002
An XSS issue was discovered in Inhaltsprojekte in Weblication CMS Core & Grid v12.6.24. The vulnerability is located in the `wFilemanager.php` and `index.php` files of the `/grid5/scripts/` modules. The injection point is located in the Project `Title` and the execution point occurs in the `Inhaltsprojekte` output listing section. Remote attackers with privileged user accounts are able to inject their own malicious script code with a persistent attack vector to compromise user session credentials or to manipulate the affected web-application module output context. The request method to inject is POST.
CVE-2018-13003
An issue was discovered in OpenTSDB 2.3.0. There is XSS in parameter 'type' to the /suggest URI.
CVE-2018-13005
An issue was discovered in MP4Box in GPAC 0.7.1. The function urn_Read in isomedia/box_code_base.c has a heap-based buffer over-read.
CVE-2018-13006
An issue was discovered in MP4Box in GPAC 0.7.1. There is a heap-based buffer over-read in the isomedia/box_dump.c function hdlr_dump.
CVE-2018-13007
An issue was discovered in gpmf-parser 1.1.2. There is a heap-based buffer over-read in GPMF_parser.c in the function GPMF_Next, related to certain checks for GPMF_KEY_END and nest_level (not conditional on a buffer_size_longs check).
CVE-2018-13008
An issue was discovered in gpmf-parser 1.1.2. There is a heap-based buffer over-read in GPMF_parser.c in the function GPMF_Next, related to certain checks for a positive nest_level.
CVE-2018-13009
An issue was discovered in gpmf-parser 1.1.2. There is a heap-based buffer over-read in GPMF_parser.c in the function GPMF_Next, related to certain checks for GPMF_KEY_END and nest_level (conditional on a buffer_size_longs check).
CVE-2018-13010
WSTMall v1.9.1_170316 has CSRF via the index.php?m=Admin&c=Users&a=edit URI to add a user account.
CVE-2018-13011
An issue was discovered in gpmf-parser 1.1.2. There is a heap-based buffer over-read in GPMF_parser.c in the function GPMF_Validate.
CVE-2018-13012
Download of code with improper integrity check in snsupd.exe and upd.exe in SAFE'N'SEC SoftControl/SafenSoft SysWatch, SoftControl/SafenSoft TPSecure, and SoftControl/SafenSoft Enterprise Suite before 4.4.12 allows the remote attacker to execute unauthorized code by substituting a forged update server.
CVE-2018-13013
Improper check of unusual conditions when launching msiexec.exe in safensec.com (SysWatch service) in SAFE'N'SEC SoftControl/SafenSoft SysWatch, SoftControl/SafenSoft TPSecure, and SoftControl/SafenSoft Enterprise Suite before 4.4.9 allows the local attacker to bypass a code-signing protection mechanism and install/execute an unauthorized program by modifying the system configuration and installing a forged MSI file. (The intended behavior is that the component SysWatch does not allow installation of MSI files unless they are signed by a limited list of certificates.)
CVE-2018-13014
Storing password in recoverable format in safensec.com (SysWatch service) in SAFE'N'SEC SoftControl/SafenSoft SysWatch, SoftControl/SafenSoft TPSecure, and SoftControl/SafenSoft Enterprise Suite before 4.4.2 allows the local attacker to restore the SysWatch password from the settings database and modify program settings.
CVE-2018-13021
An issue was discovered in HongCMS 3.0.0. There is an Arbitrary Script File Upload issue that can result in PHP code execution via the admin/index.php/template/upload URI.
CVE-2018-13024
Metinfo v6.0.0 allows remote attackers to write code into a .php file, and execute that code, via the module parameter to admin/column/save.php in an editor upload action.
CVE-2018-13025
protected/apps/admin/controller/photoController.php in YXcms 1.4.7 allows remote attackers to delete arbitrary files via the index.php?r=admin/photo/delpic picname parameter.
CVE-2018-8901
An issue was discovered in Ivanti Avalanche for all versions between 5.3 and 6.2. A local user with database access privileges can read the encrypted passwords for users who authenticate via LDAP to Avalanche services. These passwords are stored in the Avalanche databases. This issue only affects customers who have enabled LDAP authentication in their configuration.
CVE-2018-8902
An issue was discovered in Ivanti Avalanche for all versions between 5.3 and 6.2. The impacted products used a single shared key encryption model to encrypt data. A user with access to system databases can use the discovered key to access potentially confidential stored data, which may include Wi-Fi passwords. This discovered key can be used for all instances of the product.
