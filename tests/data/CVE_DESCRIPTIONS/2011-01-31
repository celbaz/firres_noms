CVE-2010-0110
Multiple stack-based buffer overflows in Intel Alert Management System (aka AMS or AMS2), as used in Symantec AntiVirus Corporate Edition (SAVCE) 10.x before 10.1 MR10, Symantec System Center (SSC) 10.x, and Symantec Quarantine Server 3.5 and 3.6, allow remote attackers to execute arbitrary code via (1) a long string to msgsys.exe, related to the AMSSendAlertAct function in AMSLIB.dll in the Intel Alert Handler service (aka Symantec Intel Handler service); a long (2) modem string or (3) PIN number to msgsys.exe, related to pagehndl.dll in the Intel Alert Handler service; or (4) a message to msgsys.exe, related to iao.exe in the Intel Alert Originator service.
CVE-2010-0111
HDNLRSVC.EXE in the Intel Alert Handler service (aka Symantec Intel Handler service) in Intel Alert Management System (aka AMS or AMS2), as used in Symantec AntiVirus Corporate Edition (SAVCE) 10.x before 10.1 MR10, Symantec System Center (SSC) 10.x, and Symantec Quarantine Server 3.5 and 3.6, allows remote attackers to execute arbitrary programs by sending msgsys.exe a UNC share pathname, which is used directly in a CreateProcessA (aka CreateProcess) call.
CVE-2010-4393
Heap-based buffer overflow in vidplin.dll in RealNetworks RealPlayer 11.0 through 11.1 and 14.0.x before 14.0.2, and RealPlayer SP 1.0 through 1.1.5, allows remote attackers to execute arbitrary code via a crafted header in an AVI file.
CVE-2010-4711
Double free vulnerability in the IMAP server component in GroupWise Internet Agent (GWIA) in Novell GroupWise before 8.02HP allows remote attackers to execute arbitrary code via a large parameter in a LIST command.
CVE-2010-4712
Multiple stack-based buffer overflows in gwia.exe in GroupWise Internet Agent (GWIA) in Novell GroupWise before 8.02HP allow remote attackers to execute arbitrary code via a Content-Type header containing (1) multiple items separated by ; (semicolon) characters or (2) crafted string data.
CVE-2010-4713
Integer signedness error in gwia.exe in GroupWise Internet Agent (GWIA) in Novell GroupWise before 8.02HP allows remote attackers to execute arbitrary code via a signed integer value in the Content-Type header.
CVE-2010-4714
Multiple stack-based buffer overflows in Novell GroupWise before 8.02HP allow remote attackers to execute arbitrary code via a long HTTP Host header to (1) gwpoa.exe in the Post Office Agent, (2) gwmta.exe in the Message Transfer Agent, (3) gwia.exe in the Internet Agent, (4) the WebAccess Agent, or (5) the Monitor Agent.
CVE-2010-4715
Multiple directory traversal vulnerabilities in the (1) WebAccess Agent and (2) Document Viewer Agent components in Novell GroupWise before 8.02HP allow remote attackers to read arbitrary files via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2010-4716
Cross-site scripting (XSS) vulnerability in the WebPublisher component in Novell GroupWise before 8.02HP allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-4717
Multiple stack-based buffer overflows in the IMAP server component in GroupWise Internet Agent (GWIA) in Novell GroupWise before 8.02HP allow remote attackers to execute arbitrary code via a long (1) LIST or (2) LSUB command.
CVE-2011-0096
The MHTML protocol handler in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 does not properly handle a MIME format in a request for content blocks in a document, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted web site that is visited in Internet Explorer, aka "MHTML Mime-Formatted Request Vulnerability."
CVE-2011-0413
The DHCPv6 server in ISC DHCP 4.0.x and 4.1.x before 4.1.2-P1, 4.0-ESV and 4.1-ESV before 4.1-ESV-R1, and 4.2.x before 4.2.1b1 allows remote attackers to cause a denial of service (assertion failure and daemon crash) by sending a message over IPv6 for a declined and abandoned address.
CVE-2011-0450
The downloads manager in Opera before 11.01 on Windows does not properly determine the pathname of the filesystem-viewing application, which allows user-assisted remote attackers to execute arbitrary code via a crafted web site that hosts an executable file.
CVE-2011-0680
data/WorkingMessage.java in the Mms application in Android before 2.2.2 and 2.3.x before 2.3.2 does not properly manage the draft cache, which allows remote attackers to read SMS messages intended for other recipients in opportunistic circumstances via a standard text messaging service.
CVE-2011-0681
The Cascading Style Sheets (CSS) Extensions for XML implementation in Opera before 11.01 recognizes links to javascript: URLs in the -o-link property, which makes it easier for remote attackers to bypass CSS filtering via a crafted URL.
CVE-2011-0682
Integer truncation error in opera.dll in Opera before 11.01 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via an HTML form with a select element that contains a large number of children.
CVE-2011-0683
Opera before 11.01 does not properly restrict the use of opera: URLs, which makes it easier for remote attackers to conduct clickjacking attacks via a crafted web site.
CVE-2011-0684
Opera before 11.01 does not properly handle redirections and unspecified other HTTP responses, which allows remote web servers to obtain sufficient access to local files to use these files as page resources, and consequently obtain potentially sensitive information from the contents of the files, via an unknown response manipulation.
CVE-2011-0685
The Delete Private Data feature in Opera before 11.01 does not properly implement the "Clear all email account passwords" option, which might allow physically proximate attackers to access an e-mail account via an unattended workstation.
CVE-2011-0686
Unspecified vulnerability in Opera before 11.01 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by vkontakte.ru.
CVE-2011-0687
Opera before 11.01 does not properly implement Wireless Application Protocol (WAP) dropdown lists, which allows user-assisted remote attackers to cause a denial of service (application crash) via a crafted WAP document.
CVE-2011-0688
Intel Alert Management System (aka AMS or AMS2), as used in Symantec Antivirus Corporate Edition (SAVCE) 10.x before 10.1 MR10, Symantec System Center (SSC) 10.x, and Symantec Quarantine Server 3.5 and 3.6, allows remote attackers to execute arbitrary commands via crafted messages over TCP, as discovered by Junaid Bohio, a different vulnerability than CVE-2010-0110 and CVE-2010-0111.  NOTE: some of these details are obtained from third party information.
