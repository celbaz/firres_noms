CVE-2012-0052
Red Hat JBoss Operations Network (JON) before 2.4.2 and 3.0.x before 3.0.1 does not check the JON agent key, which allows remote attackers to spoof the identity of arbitrary agents via the registered agent name.
CVE-2012-0062
Red Hat JBoss Operations Network (JON) before 2.4.2 and 3.0.x before 3.0.1 allows remote attackers to hijack agent sessions via an agent registration request without a security token.
CVE-2012-1100
Red Hat JBoss Operations Network (JON) 3.0.x before 3.0.1, 2.4.2, and earlier, when LDAP authentication is enabled and the LDAP bind account credentials are invalid, allows remote attackers to login to LDAP-based accounts via an arbitrary password in a login request.
CVE-2012-6149
Multiple cross-site scripting (XSS) vulnerabilities in systems/sdc/notes.jsp in Spacewalk and Red Hat Network (RHN) Satellite 5.6 allow remote attackers to inject arbitrary web script or HTML via the (1) subject or (2) content values of a note in a system.addNote XML-RPC call.
CVE-2013-1871
Cross-site scripting (XSS) vulnerability in account/EditAddress.do in Spacewalk and Red Hat Network (RHN) Satellite 5.6 allows remote attackers to inject arbitrary web script or HTML via the type parameter.
CVE-2013-2829
MatrikonOPC SCADA DNP3 OPC Server 1.2.2.0 and earlier allows remote attackers to cause a denial of service (infinite loop) via a malformed DNP3 packet.
CVE-2013-3978
The Meeting Server in IBM Sametime 8.5.2 through 8.5.2.1 and 9.x through 9.0.0.1 does not send the appropriate HTTP response headers to prevent unwanted caching by a web browser, which allows remote attackers to obtain sensitive information by leveraging an unattended workstation.
CVE-2013-3983
The Meeting Server in IBM Sametime 8.5.2 through 8.5.2.1 and 9.x through 9.0.0.1 does not validate URLs in Cookie headers before using them in redirects, which has unspecified impact and remote attack vectors.
CVE-2013-3988
The Meeting Server in IBM Sametime 8.5.2 through 8.5.2.1 and 9.x through 9.0.0.1 allows remote attackers to conduct clickjacking attacks via unspecified vectors.
CVE-2013-4415
Multiple cross-site scripting (XSS) vulnerabilities in Spacewalk and Red Hat Network (RHN) Satellite 5.6 allow remote attackers to inject arbitrary web script or HTML via the (1) whereCriteria variable in a software channels search; (2) end_year, (3) start_hour, (4) end_am_pm, (5) end_day, (6) end_hour, (7) end_minute, (8) end_month, (9) end_year, (10) optionScanDateSearch, (11) result_filter, (12) search_string, (13) show_as, (14) start_am_pm, (15) start_day, (16) start_hour, (17) start_minute, (18) start_month, (19) start_year, or (20) whereToSearch variable in an scap audit results search; (21) end_minute, (22) end_month, (23) end_year, (24) errata_type_bug, (25) errata_type_enhancement, (26) errata_type_security, (27) fineGrained, (28) list_1892635924_sortdir, (29) optionIssueDateSearch, (30) start_am_pm, (31) start_day, (32) start_hour, (33) start_minute, (34) start_month, (35) start_year, or (36) view_mode variable in an errata search; or (37) fineGrained variable in a systems search, related to PAGE_SIZE_LABEL_SELECTED.
CVE-2013-4499
Cross-site scripting (XSS) vulnerability in the Bean module 7.x-1.x before 7.x-1.5 for Drupal allows remote attackers to inject arbitrary web script or HTML via the bean title.
CVE-2013-5014
The management console in Symantec Endpoint Protection Manager (SEPM) 11.0 before 11.0.7405.1424 and 12.1 before 12.1.4023.4080, and Symantec Protection Center Small Business Edition 12.x before 12.1.4023.4080, allows remote attackers to read arbitrary files via XML data containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2013-5015
SQL injection vulnerability in the management console in Symantec Endpoint Protection Manager (SEPM) 11.0 before 11.0.7405.1424 and 12.1 before 12.1.4023.4080, and Symantec Protection Center Small Business Edition 12.x before 12.1.4023.4080, allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2013-5351
Heap-based buffer overflow in IrfanView before 4.37 allows remote attackers to execute arbitrary code via the LZW code stream in a GIF file.
CVE-2013-5400
An unspecified servlet in IBM Platform Symphony Developer Edition (DE) 5.2 and 6.1.x through 6.1.1 has hardcoded credentials, which allows remote attackers to bypass authentication and obtain "local environment" access via unknown vectors.
CVE-2013-6440
The (1) BasicParserPool, (2) StaticBasicParserPool, (3) XML Decrypter, and (4) SAML Decrypter in Shibboleth OpenSAML-Java before 2.6.1 set the expandEntityReferences property to true, which allows remote attackers to conduct XML external entity (XXE) attacks via a crafted XML DOCTYPE declaration.
CVE-2013-6441
The lxc-sshd template (templates/lxc-sshd.in) in LXC before 1.0.0.beta2 uses read-write permissions when mounting /sbin/init, which allows local users to gain privileges by modifying the init file.
CVE-2013-6492
The Piranha Configuration Tool in Piranha 0.8.6 does not properly restrict access to webpages, which allows remote attackers to bypass authentication and read or modify the LVS configuration via an HTTP POST request.
CVE-2013-6722
Unrestricted file upload vulnerability in the Registration/Edit My Profile portlet in IBM WebSphere Portal 7.x before 7.0.0.2 CF27 and 8.x through 8.0.0.1 CF09 allows remote attackers to cause a denial of service or modify data via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/434.html

"CWE-434: Unrestricted Upload of File with Dangerous Type"
CVE-2013-6728
The charting component in IBM WebSphere Dashboard Framework (WDF) 6.1.5 and 7.0.1 allows remote attackers to view or delete image files by leveraging incorrect security constraints for a temporary directory.
CVE-2013-6742
The Meeting Server in IBM Sametime 8.5.2 through 8.5.2.1 and 9.x through 9.0.0.1 do not have an off autocomplete attribute for a password field, which makes it easier for remote attackers to obtain access by leveraging an unattended workstation.
CVE-2013-6743
Cross-site scripting (XSS) vulnerability in the Meeting Server in IBM Sametime 8.5.2 through 8.5.2.1 and 9.x through 9.0.0.1 allows remote authenticated users to inject arbitrary web script or HTML via vectors involving an IMG element.
CVE-2013-7032
Multiple cross-site scripting (XSS) vulnerabilities in the web based operator client in LiveZilla before 5.1.2.1 allow remote attackers to inject arbitrary web script or HTML via the (1) name of an uploaded file or (2) customer name in a resource created from an uploaded file, a different vulnerability than CVE-2013-7003.
CVE-2013-7326
Cross-site scripting (XSS) vulnerability in vTiger CRM 5.4.0 allows remote attackers to inject arbitrary web script or HTML via the (1) return_url parameter to modules\com_vtiger_workflow\savetemplate.php, or unspecified vectors to (2) deletetask.php, (3) edittask.php, (4) savetask.php, or (5) saveworkflow.php.
CVE-2014-0018
Red Hat JBoss Enterprise Application Platform (JBEAP) 6.2.0 and JBoss WildFly Application Server, when run under a security manager, do not properly restrict access to the Modular Service Container (MSC) service registry, which allows local users to modify the server via a crafted deployment.
CVE-2014-0032
The get_resource function in repos.c in the mod_dav_svn module in Apache Subversion before 1.7.15 and 1.8.x before 1.8.6, when SVNListParentPath is enabled, allows remote attackers to cause a denial of service (crash) via vectors related to the server root and request methods other than GET, as demonstrated by the "svn ls http://svn.example.com" command.
CVE-2014-0322
Use-after-free vulnerability in Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code via vectors involving crafted JavaScript code, CMarkup, and the onpropertychange attribute of a script element, as exploited in the wild in January and February 2014.
CVE-2014-0332
Cross-site scripting (XSS) vulnerability in mainPage in Dell SonicWALL GMS before 7.1 SP2, SonicWALL Analyzer before 7.1 SP2, and SonicWALL UMA E5000 before 7.1 SP2 might allow remote attackers to inject arbitrary web script or HTML via the node_id parameter in a ScreenDisplayManager genNetwork action.
CVE-2014-0813
Cross-site request forgery (CSRF) vulnerability in phpMyFAQ before 2.8.6 allows remote attackers to hijack the authentication of arbitrary users for requests that modify settings.
CVE-2014-0814
Cross-site scripting (XSS) vulnerability in phpMyFAQ before 2.8.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-0855
Multiple cross-site scripting (XSS) vulnerabilities in IBM Connections Portlets 4.x before 4.5.1 FP1 for IBM WebSphere Portal 7.0.0.2 and 8.0.0.1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-1219
CA 2E Web Option r8.1.2 accepts a predictable substring of a W2E_SSNID session token in place of the entire token, which allows remote attackers to hijack sessions by changing characters at the end of this substring, as demonstrated by terminating a session via a modified SSNID parameter to web2edoc/close.htm.
CVE-2014-1253
AppleMNT.sys in Apple Boot Camp 5 before 5.1 allows local users to cause a denial of service (kernel memory corruption) or possibly have unspecified other impact via a malformed header in a Portable Executable (PE) file.
CVE-2014-1467
BlackBerry Enterprise Service 10 before 10.2.1, Universal Device Service 6, Enterprise Server Express for Domino through 5.0.4, Enterprise Server Express for Exchange through 5.0.4, Enterprise Server for Domino through 5.0.4 MR6, Enterprise Server for Exchange through 5.0.4 MR6, and Enterprise Server for GroupWise through 5.0.4 MR6 log cleartext credentials during exception handling, which might allow context-dependent attackers to obtain sensitive information by reading a log file.
CVE-2014-1680
Untrusted search path vulnerability in Bandisoft Bandizip before 3.10 allows local users to gain privileges via a Trojan horse dwmapi.dll file in the current working directory.
Per: http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2014-1921
parcimonie before 0.8.1, when using a large keyring, sleeps for the same amount of time between fetches, which allows attackers to correlate key fetches via unspecified vectors.
CVE-2014-1948
OpenStack Image Registry and Delivery Service (Glance) 2013.2 through 2013.2.1 and Icehouse before icehouse-2 logs a URL containing the Swift store backend password when authentication fails and WARNING level logging is enabled, which allows local users to obtain sensitive information by reading the log.
CVE-2014-1950
Use-after-free vulnerability in the xc_cpupool_getinfo function in Xen 4.1.x through 4.3.x, when using a multithreaded toolstack, does not properly handle a failure by the xc_cpumap_alloc function, which allows local users with access to management functions to cause a denial of service (heap corruption) and possibly gain privileges via unspecified vectors.
CVE-2014-1960
The Solution Manager in SAP NetWeaver does not properly restrict access, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2014-1961
Unspecified vulnerability in the Portal WebDynPro in SAP NetWeaver allows remote attackers to obtain sensitive path information via unknown attack vectors.
CVE-2014-1962
Gwsync in SAP CRM 7.02 EHP 2 allows remote attackers to obtain sensitive information via unspecified vectors, related to an XML External Entity (XXE) issue.
CVE-2014-1963
Unspecified vulnerability in Message Server in SAP NetWeaver 7.20 allows remote attackers to cause a denial of service via unknown attack vectors.
CVE-2014-1964
Cross-site scripting (XSS) vulnerability in the Integration Repository in the SAP Exchange Infrastructure (BC-XI) component in SAP NetWeaver allows remote attackers to inject arbitrary web script or HTML via vectors related to the ESR application and a DIR error.
CVE-2014-1965
Cross-site scripting (XSS) vulnerability in ISpeakAdapter in the Integration Repository in the SAP Exchange Infrastructure (BC-XI) component 3.0, 7.00 through 7.02, and 7.10 through 7.11 for SAP NetWeaver allows remote attackers to inject arbitrary web script or HTML via vectors related to PIP.
