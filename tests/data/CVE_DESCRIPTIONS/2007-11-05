CVE-2007-0011
The web portal interface in Citrix Access Gateway (aka Citrix Advanced Access Control) before Advanced Edition 4.5 HF1 places a session ID in the URL, which allows context-dependent attackers to hijack sessions by reading "residual information", including the a referer log, browser history, or browser cache.
CVE-2007-4217
Stack-based buffer overflow in the domacro function in ftp in IBM AIX 5.2 and 5.3 allows local users to gain privileges via a long parameter to a macro, as demonstrated by executing a macro via the '$' command.
CVE-2007-4513
Multiple stack-based buffer overflows in IBM AIX 5.2 and 5.3 allow local users to gain privileges via a long argument to the (1) "-p" option to lqueryvg or (2) the "-V" option to lquerypv.
CVE-2007-4621
Buffer overflow in crontab in IBM AIX 5.2 allows local users to gain privileges via long command line arguments.
CVE-2007-4622
Integer underflow in the dns_name_fromtext function in (1) libdns_nonsecure.a and (2) libdns_secure.a in IBM AIX 5.2 allows local users to gain privileges via a crafted "-y" (TSIG key) command line argument to dig.
CVE-2007-4623
Stack-based buffer overflow in the sendrmt function in bellmail in IBM AIX 5.2 and 5.3 allows local users to execute arbitrary code via a long parameter to the m command.
CVE-2007-5603
Stack-based buffer overflow in the SonicWall SSL-VPN NetExtender NELaunchCtrl ActiveX control before 2.1.0.51, and 2.5.x before 2.5.0.56, allows remote attackers to execute arbitrary code via a long string in the second argument to the AddRouteEntry method.
CVE-2007-5804
cfgcon in IBM AIX 5.2 and 5.3 does not properly validate the argument to the "-p" option to swcons, which allows local users in the system group to create or overwrite an arbitrary file, and enable world writability of this file, by using the file's name as the argument.
CVE-2007-5805
cfgcon in IBM AIX 5.2 and 5.3 does not properly validate the argument to the "-p" option to swcons, which allows local users in the system group to create an arbitrary file, and enable world writability of this file, via a symlink attack involving use of the file's name as the argument.  NOTE: this issue is due to an incomplete fix for CVE-2007-5804.
CVE-2007-5806
Cross-site scripting (XSS) vulnerability in Services/Utilities/classes/class.ilUtil.php in ILIAS 3.8.3 and earlier allows remote attackers to inject arbitrary web script or HTML via attributes inside a domain-name string in the (1) mailing or (2) forum component, as demonstrated using the style and onmouseover HTML attributes.
CVE-2007-5807
Buffer overflow in the register function in Ultra Star Reader ActiveX control in SSReader allows remote attackers to execute arbitrary code via unspecified vectors, as exploited in the wild.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5808
Unspecified vulnerability in the Groupmax Collaboration - Schedule component in Hitachi Groupmax Collaboration Portal 07-30 through 07-30-/F and 07-32 through 07-32-/C, uCosminexus Collaboration Portal 06-30 through 06-30-/F and 06-32 through 06-32-/C, and Groupmax Collaboration Web Client - Mail/Schedule 07-30 through 07-30-/F and 07-32 through 07-32-/B might allow remote attackers to obtain sensitive information via unspecified vectors related to schedule portlets.
CVE-2007-5809
Cross-site scripting (XSS) vulnerability in Hitachi Web Server 01-00 through 03-10, as used by certain Cosminexus products, allows remote attackers to inject arbitrary web script or HTML via unspecified HTTP requests that trigger creation of a server-status page.
CVE-2007-5810
Hitachi Web Server 01-00 through 03-00-01, as used by certain Cosminexus products, does not properly validate SSL client certificates, which might allow remote attackers to spoof authentication via a client certificate with a forged signature.
CVE-2007-5811
** DISPUTED **  Directory traversal vulnerability in PageTraiteDownload.php in phpMyConferences 8.0.2 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the dir parameter.  NOTE: this issue is disputed for 8.0.2 by a reliable third party, who notes that the PHP code is syntactically incorrect and cannot be executed.
CVE-2007-5812
Directory traversal vulnerability in modules/Builder/DownloadModule.php in ModuleBuilder 1.0 allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter.
CVE-2007-5813
Multiple directory traversal vulnerabilities in download.php in ISPworker 1.21 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) ticketid and (2) filename parameters.
CVE-2007-5814
Multiple buffer overflows in the SonicWall SSL-VPN NetExtender NELaunchCtrl ActiveX control before 2.1.0.51, and 2.5.x before 2.5.0.56, allow remote attackers to execute arbitrary code via a long (1) serverAddress, (2) sessionId, (3) clientIPLower, (4) clientIPHigher, (5) userName, (6) domainName, or (7) dnsSuffix Unicode property value.  NOTE: the AddRouteEntry vector is covered by CVE-2007-5603.
CVE-2007-5815
Absolute path traversal vulnerability in the WebCacheCleaner ActiveX control 1.3.0.3 in SonicWall SSL-VPN 200 before 2.1, and SSL-VPN 2000/4000 before 2.5, allows remote attackers to delete arbitrary files via a full pathname in the argument to the FileDelete method.
CVE-2007-5816
dialog.php in CONTENTCustomizer 3.1mp and earlier allows remote attackers to obtain sensitive author credentials by making a request with an editauthor action, then reading the value of the newlocalpassword password input field in the HTML source of the resulting page.
CVE-2007-5817
dialog.php in CONTENTCustomizer 3.1mp and earlier allows remote attackers to perform certain privileged actions via a (1) del, (2) delbackup, (3) res, or (4) ren action.  NOTE: this issue can be leveraged to conduct cross-site scripting (XSS) and possibly other attacks.
CVE-2007-5818
Cross-site request forgery (CSRF) vulnerability in blocks_edit_do.php in sBlog 0.7.3 Beta allows remote attackers to change arbitrary blocks as administrators.
CVE-2007-5819
IBM Tivoli Continuous Data Protection for Files (CDP) 3.1.0 uses weak permissions (unrestricted write) for the Central Admin Global download directory, which allows local users to place arbitrary files into a location used for updating CDP clients.
CVE-2007-5820
Directory traversal vulnerability in index.php in Ax Developer CMS (AxDCMS) 0.1.1 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the module parameter.
CVE-2007-5821
Multiple directory traversal vulnerabilities in DM Guestbook 0.4.1 and earlier allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in (1) the lng parameter to (a) guestbook.php, (b) admin/admin.guestbook.php, or (c) auto/glob_new.php; or (2) the lngdefault parameter to auto/ch_lng.php.
CVE-2007-5822
Direct static code injection vulnerability in forum.php in Ben Ng Scribe 0.2 and earlier allows remote attackers to inject arbitrary PHP code into a certain file in regged/ via the username parameter in a Register action, possibly related to the register function in forumfunctions.php.
CVE-2007-5823
Directory traversal vulnerability in forum.php in Ben Ng Scribe 0.2 and earlier allows remote attackers to create or overwrite arbitrary files via a .. (dot dot) in the username parameter in a Register action.
CVE-2007-5824
webserver.c in mt-dappd in Firefly Media Server 0.2.4 and earlier allows remote attackers to cause a denial of service (NULL dereference and daemon crash) via a stats method action to /xml-rpc with (1) an empty Authorization header line, which triggers a crash in the ws_decodepassword function; or (2) a header line without a ':' character, which triggers a crash in the ws_getheaders function.
CVE-2007-5825
Format string vulnerability in the ws_addarg function in webserver.c in mt-dappd in Firefly Media Server 0.2.4 and earlier allows remote attackers to execute arbitrary code via a stats method action to /xml-rpc with format string specifiers in the (1) username or (2) password portion of base64-encoded data on the "Authorization: Basic" HTTP header line.
CVE-2007-5826
Absolute path traversal vulnerability in the EDraw Flowchart ActiveX control in EDImage.ocx 2.0.2005.1104 allows remote attackers to create or overwrite arbitrary files with arbitrary contents via a full pathname in the second argument to the HttpDownloadFile method, a different product than CVE-2007-4420.
CVE-2007-5827
iSCSI Enterprise Target (iscsitarget) 0.4.15 uses weak permissions for /etc/ietd.conf, which allows local users to obtain passwords.
CVE-2007-5828
** DISPUTED **  Cross-site request forgery (CSRF) vulnerability in the admin panel in Django 0.96 allows remote attackers to change passwords of arbitrary users via a request to admin/auth/user/1/password/.  NOTE: this issue has been disputed by Debian, since product documentation includes a recommendation for a CSRF protection module that is included with the product.  However, CVE considers this an issue because the default configuration does not use this module.
CVE-2007-5829
The Disk Mount scanner in Symantec AntiVirus for Macintosh 9.x and 10.x, Norton AntiVirus for Macintosh 10.0 and 10.1, and Norton Internet Security for Macintosh 3.x, uses a directory with weak permissions (group writable), which allows local admin users to gain root privileges by replacing unspecified files, which are executed when a user with physical access inserts a disk and the "Show Progress During Mount Scans" option is enabled.
CVE-2007-5830
Unspecified vulnerability in the administrative interface in Avaya Messaging Storage Server (MSS) 3.1 before SP1, and Message Networking (MN) 3.1, allows remote attackers to cause a denial of service via unspecified vectors related to "input validation."
CVE-2007-5831
Directory traversal vulnerability in fileSystem.do in SSL-Explorer before 0.2.14 allows remote attackers to access arbitrary files via directory traversal sequences in the path parameter.  NOTE: some of these details are obtained from third party information.
CVE-2007-5832
Unspecified vulnerability in selectLanguage.do in SSL-Explorer before 0.2.15 allows remote attackers to inject (1) headers or (2) body data in an HTTP transaction, a different vulnerability than CVE-2007-2907. NOTE: some of these details are obtained from third party information.
CVE-2007-5833
Multiple cross-site scripting (XSS) vulnerabilities in BosDev BosMarket Business Directory System allow remote authenticated users to inject arbitrary web script or HTML via (1) user info (account details) or (2) a post.
CVE-2007-5834
Cross-site scripting (XSS) vulnerability in BosDev BosNews 4 allows remote attackers to inject arbitrary web script or HTML via a SCRIPT element in a news post.
CVE-2007-5835
Install.php in BosDev BosNews 4 and 5 does not require authentication for replacing an existing product installation or creating a new admin account, which allows remote attackers to cause a denial of service (overwritten files) and possibly obtain administrative access.
CVE-2007-5836
SQL injection vulnerability in Amazing Flash AFCommerce allows remote attackers to execute arbitrary SQL commands via the firstname parameter to an unspecified component, a different issue than CVE-2006-3794.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5837
GUI.pm in yarssr 0.2.2, when Gnome default URL handling is disabled, allows remote attackers to execute arbitrary commands via shell metacharacters in a link element in a feed.
