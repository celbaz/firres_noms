CVE-2007-1593
The administrative service in Symantec Veritas Volume Replicator (VVR) for Windows 3.1 through 4.3, and VVR for Unix 3.5 through 5.0, in Symantec Storage Foundation products allows remote attackers to cause a denial of service (memory consumption and service crash) via a crafted packet to the service port (8199/tcp) that triggers a request for more memory than available, which causes the service to write to an invalid pointer.
CVE-2007-1862
The recall_headers function in mod_mem_cache in Apache 2.2.4 does not properly copy all levels of header data, which can cause Apache to return HTTP headers containing previously used data, which could be used by remote attackers to obtain potentially sensitive information.
CVE-2007-2279
The Scheduler Service (VxSchedService.exe) in Symantec Storage Foundation for Windows 5.0 allows remote attackers to bypass authentication and execute arbitrary code via certain requests to the service socket that create (1) PreScript or (2) PostScript registry values under Veritas\VxSvc\CurrentVersion\Schedules specifying future command execution.
CVE-2007-2387
Apple Xserve Lights-Out Management before Firmware Update 1.0 on Intel hardware does not require a password for remote access to IPMI, which allows remote attackers to gain administrative access via unspecified requests with ipmitool.
CVE-2007-2452
Heap-based buffer overflow in the visit_old_format function in locate/locate.c in locate in GNU findutils before 4.2.31 might allow context-dependent attackers to execute arbitrary code via a long pathname in a locate database that has the old format, a different vulnerability than CVE-2001-1036.
CVE-2007-2513
Novell GroupWise 7 before SP2 20070524, and GroupWise 6 before 6.5 post-SP6 20070522, allows remote attackers to obtain credentials via a man-in-the-middle attack.
CVE-2007-2872
Multiple integer overflows in the chunk_split function in PHP 5 before 5.2.3 and PHP 4 before 4.4.8 allow remote attackers to cause a denial of service (crash) or execute arbitrary code via the (1) chunks, (2) srclen, and (3) chunklen arguments.
CVE-2007-2991
Cross-site scripting (XSS) vulnerability in includes/send.inc.php in Evenzia CMS allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2007-2992
Multiple SQL injection vulnerabilities in OmegaMw7.asp in OMEGA (aka Omegasoft) INterneSErvicesLosungen (INSEL) allow remote attackers to execute arbitrary SQL commands via (1) user-created text fields; the (2) F05003, (3) F05005, and (4) F05015 fields; and other unspecified standard fields.
CVE-2007-2993
Multiple cross-site scripting (XSS) vulnerabilities in OmegaMw7.asp in OMEGA (aka Omegasoft) INterneSErvicesLosungen (INSEL) allow remote attackers to inject arbitrary web script or HTML via (1) user-created text fields; the (2) F05003, (3) F05005, and (4) F05015 fields; and other unspecified standard fields.
CVE-2007-2994
SQL injection vulnerability in news.php in DGNews 2.1 allows remote attackers to execute arbitrary SQL commands via the newsid parameter in a fullnews action, a different vector than CVE-2007-0693.
CVE-2007-2995
Unspecified vulnerability in sysmgt.websm.rte in IBM AIX 5.2.0 and 5.3.0 has unknown impact and attack vectors.
CVE-2007-2996
Unspecified vulnerability in perl.rte 5.8.0.10 through 5.8.0.95 on IBM AIX 5.2, and 5.8.2.10 through 5.8.2.50 on AIX 5.3, allows local users to gain privileges via unspecified vectors related to the installation and "waiting for a legitimate user to execute a binary that ships with Perl."
CVE-2007-2997
** DISPUTED **  Multiple SQL injection vulnerabilities in cgi-bin/reorder2.asp in SalesCart Shopping Cart allow remote attackers to execute arbitrary SQL commands via the password field and other unspecified vectors. NOTE: the vendor disputes this issue, stating "We were able to reproduce this sql injection on an old out-of-date demo on the website but not on the released product."
CVE-2007-2998
The Pascal run-time library (PAS$RTL.EXE) before 20070418 on OpenVMS for Integrity Servers 8.3, and PAS$RTL.EXE before 20070419 on OpenVMS Alpha 8.3, does not properly restore PC and PSL values, which allows local users to cause a denial of service (system crash) via certain Pascal code.
CVE-2007-2999
Microsoft Windows Server 2003, when time restrictions are in effect for user accounts, generates different error messages for failed login attempts with a valid user name than for those with an invalid user name, which allows context-dependent attackers to determine valid Active Directory account names.
CVE-2007-3000
Multiple SQL injection vulnerabilities in PHP JackKnife (PHPJK) allow remote attackers to execute arbitrary SQL commands via (1) the iCategoryUnq parameter to G_Display.php or (2) the iSearchID parameter to Search/DisplayResults.php.
CVE-2007-3001
Multiple cross-site scripting (XSS) vulnerabilities in PHP JackKnife (PHPJK) allow remote attackers to inject arbitrary web script or HTML via (1) the sUName parameter to UserArea/Authenticate.php, (2) the sAccountUnq parameter to UserArea/NewAccounts/index.php, or the (3) iCategoryUnq, (4) iDBLoc, (5) iTtlNumItems, (6) iNumPerPage, or (7) sSort parameter to G_Display.php, different vectors than CVE-2005-4239.
CVE-2007-3002
PHP JackKnife (PHPJK) allows remote attackers to obtain sensitive information via (1) a request to index.php with an invalid value of the iParentUnq[] parameter, or a request to G_Display.php with an invalid (2) iCategoryUnq[] or (3) sSort[] array parameter, which reveals the path in various error messages.
CVE-2007-3003
Multiple SQL injection vulnerabilities in myBloggie 2.1.6 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) cat_id or (2) year parameter to index.php in a viewuser action, different vectors than CVE-2005-1500 and CVE-2005-4225.
CVE-2007-3004
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-2788.  Reason: This candidate is a duplicate of CVE-2007-2788.  Notes: All CVE users should reference CVE-2007-2788 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2007-3005
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-2789.  Reason: This candidate is a duplicate of CVE-2007-2789.  Notes: All CVE users should reference CVE-2007-2789 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2007-3006
Buffer overflow in Acoustica MP3 CD Burner 4.32 allows user-assisted remote attackers to execute arbitrary code via a .asx playlist file with a REF element containing a long string in the HREF attribute. NOTE: it was later claimed that 4.51 Build 147 is also affected.
CVE-2007-3007
PHP 5 before 5.2.3 does not enforce the open_basedir or safe_mode restriction in certain cases, which allows context-dependent attackers to determine the existence of arbitrary files by checking if the readfile function returns a string.  NOTE: this issue might also involve the realpath function.
CVE-2007-3008
Mbedthis AppWeb before 2.2.2 enables the HTTP TRACE method, which has unspecified impact probably related to remote information leaks and cross-site tracing (XST) attacks, a related issue to CVE-2004-2320 and CVE-2005-3398.
CVE-2007-3009
Format string vulnerability in the MprLogToFile::logEvent function in Mbedthis AppWeb 2.0.5-4, when the build supports logging but the configuration disables logging, allows remote attackers to cause a denial of service (daemon crash) via format string specifiers in the HTTP scheme, as demonstrated by a "GET %n://localhost:80/" request.
