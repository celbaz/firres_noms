CVE-2013-7330
Jenkins before 1.502 allows remote authenticated users to configure an otherwise restricted project via vectors related to post-build actions.
CVE-2014-2058
BuildTrigger in Jenkins before 1.551 and LTS before 1.532.2 allows remote authenticated users to bypass access restrictions and execute arbitrary jobs by configuring a job to trigger another job. NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-7330.
CVE-2014-2060
The Winstone servlet container in Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to hijack sessions via unspecified vectors.
CVE-2014-2061
The input control in PasswordParameterDefinition in Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to obtain passwords by reading the HTML source code, related to the default value.
CVE-2014-2062
Jenkins before 1.551 and LTS before 1.532.2 does not invalidate the API token when a user is deleted, which allows remote authenticated users to retain access via the token.
CVE-2014-2063
Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to conduct clickjacking attacks via unspecified vectors.
CVE-2014-2064
The loadUserByUsername function in hudson/security/HudsonPrivateSecurityRealm.java in Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to determine whether a user exists via vectors related to failed login attempts.
CVE-2014-2065
Cross-site scripting (XSS) vulnerability in Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to inject arbitrary web script or HTML via the iconSize cookie.
CVE-2014-2066
Session fixation vulnerability in Jenkins before 1.551 and LTS before 1.532.2 allows remote attackers to hijack web sessions via vectors involving the "override" of Jenkins cookies.
CVE-2014-2068
The doIndex function in hudson/util/RemotingDiagnostics.java in CloudBees Jenkins before 1.551 and LTS before 1.532.2 allows remote authenticated users with the ADMINISTER permission to obtain sensitive information via vectors related to heapDump.
CVE-2014-2278
Unrestricted file upload vulnerability in op/op.AddFile2.php in SeedDMS (formerly LetoDMS and MyDMS) before 4.3.4 allows remote attackers to execute arbitrary code by uploading a file with an executable extension specified by the partitionIndex parameter and leveraging CVE-2014-2279.2 to access it via the directory specified by the fileId parameter.
CVE-2014-2279
Multiple directory traversal vulnerabilities in SeedDMS (formerly LetoDMS and MyDMS) before 4.3.4 allow (1) remote authenticated users with access to the LogManagement functionality to read arbitrary files via a .. (dot dot) in the logname parameter to out/out.LogManagement.php or (2) remote attackers to write to arbitrary files via a .. (dot dot) in the fileId parameter to op/op.AddFile2.php.  NOTE: vector 2 can be leveraged to execute arbitrary code by using CVE-2014-2278.
CVE-2014-2559
Multiple cross-site request forgery (CSRF) vulnerabilities in twitget.php in the Twitget plugin before 3.3.3 for WordPress allow remote attackers to hijack the authentication of administrators for requests that change unspecified plugin options via a request to wp-admin/options-general.php.
CVE-2014-2995
Multiple cross-site scripting (XSS) vulnerabilities in twitget.php in the Twitget plugin before 3.3.3 for WordPress allow remote authenticated administrators to inject arbitrary web script or HTML via unspecified vectors, as demonstrated by the twitget_consumer_key parameter to wp-admin/options-general.php.
CVE-2014-6283
SAP Adaptive Server Enterprise (ASE) 15.7 before SP122 or SP63, 15.5 before ESD#5.4, and 15.0.3 before ESD#4.4 does not properly restrict access, which allows remote authenticated database users to (1) overwrite the master encryption key or (2) trigger a buffer overflow via a crafted RPC message to the hacmpmsgxchg function, and possibly other vectors.
CVE-2014-7960
OpenStack Object Storage (Swift) before 2.2.0 allows remote authenticated users to bypass the max_meta_count and other metadata constraints via multiple crafted requests which exceed the limit when combined.
CVE-2014-8074
Buffer overflow in the SetLogFile method in Foxit.FoxitPDFSDKProCtrl.5 in Foxit PDF SDK ActiveX 2.3 through 5.0.1820 before 5.0.2.924 allows remote attackers to execute arbitrary code via a long string, related to global variables.
CVE-2014-8317
Cross-site scripting (XSS) vulnerability in the Webform Validation module 6.x-1.x before 6.x-1.6 and 7.x-1.x before 7.x-1.4 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via a component name text.
CVE-2014-8318
Cross-site scripting (XSS) vulnerability in the Webform module 6.x-3.x before 6.x-3.20, 7.x-3.x before 7.x-3.20, and 7.x-4.x before 7.x-4.0-beta2 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via a field label title, when two fields have the same form_key.
CVE-2014-8319
Cross-site scripting (XSS) vulnerability in the easy_social_admin_summary function in the Easy Social module 7.x-2.x before 7.x-2.11 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via a block title.
CVE-2014-8320
Cross-site scripting (XSS) vulnerability in the Custom Search module 6.x-1.x before 6.x-1.12 and 7.x-1.x before 7.x-1.14 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via the "Label text" field to the results configuration page.
CVE-2014-8755
Panasonic Network Camera View 3 and 4 allows remote attackers to execute arbitrary code via a crafted page, which triggers an invalid pointer dereference, related to "the ability to nullify an arbitrary address in memory."
CVE-2014-8756
The NcrCtl4.NcrNet.1 control in Panasonic Network Camera Recorder before 4.04R03 allows remote attackers to execute arbitrary code via a crafted GetVOLHeader method call, which writes null bytes to an arbitrary address.
