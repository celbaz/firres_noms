CVE-2007-4350
Cross-site scripting (XSS) vulnerability in the management interface in HP SiteScope 9.0 build 911 allows remote attackers to inject arbitrary web script or HTML via an SNMP trap message.
CVE-2008-1547
Open redirect vulnerability in exchweb/bin/redir.asp in Microsoft Outlook Web Access (OWA) for Exchange Server 2003 SP2 (aka build 6.5.7638) allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the URL parameter.
CVE-2008-3248
qiomkfile in the Quick I/O for Database feature in Symantec Veritas File System (VxFS) on HP-UX, and before 5.0 MP3 on Solaris, Linux, and AIX, does not initialize filesystem blocks during creation of a file, which allows local users to obtain sensitive information by creating and then reading files.
CVE-2008-4121
Multiple cross-site scripting (XSS) vulnerabilities in cpCommerce before 1.2.4 allow remote attackers to inject arbitrary web script or HTML via (1) the search parameter in a search.quick action to search.php and (2) the name parameter in a sendtofriend action to sendtofriend.php.
Patch Information: http://cpcommerce.cpradio.org/downloads.php
CVE-2008-4618
The Stream Control Transmission Protocol (sctp) implementation in the Linux kernel before 2.6.27 does not properly handle a protocol violation in which a parameter has an invalid length, which allows attackers to cause a denial of service (panic) via unspecified vectors, related to sctp_sf_violation_paramlen, sctp_sf_abort_violation, sctp_make_abort_violation, and incorrect data types in function calls.
CVE-2008-4619
The RPC subsystem in Sun Solaris 9 allows remote attackers to cause a denial of service (daemon crash) via a crafted request to procedure 8 in program 100000 (rpcbind), related to the XDR_DECODE operation and the taddr2uaddr function.  NOTE: this might be a duplicate of CVE-2007-0165.
CVE-2008-4620
SQL injection vulnerability in Meeting Room Booking System (MRBS) before 1.4 allows remote attackers to execute arbitrary SQL commands via the area parameter to (1) month.php, and possibly (2) day.php and (3) week.php.
CVE-2008-4621
SQL injection vulnerability in bannerclick.php in ZeeScripts Zeeproperty allows remote attackers to execute arbitrary SQL commands via the adid parameter.
CVE-2008-4622
The isLoggedIn function in fastnews-code.php in phpFastNews 1.0.0 allows remote attackers to bypass authentication and gain administrative access by setting the fn-loggedin cookie to 1.
CVE-2008-4623
SQL injection vulnerability in the DS-Syndicate (com_ds-syndicate) component 1.1.1 for Joomla allows remote attackers to execute arbitrary SQL commands via the feed_id parameter to index2.php.
CVE-2008-4624
PHP remote file inclusion vulnerability in init.php in Fast Click SQL Lite 1.1.7, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the CFG[CDIR] parameter.
CVE-2008-4625
SQL injection vulnerability in stnl_iframe.php in the ShiftThis Newsletter (st_newsletter) plugin for WordPress allows remote attackers to execute arbitrary SQL commands via the newsletter parameter, a different vector than CVE-2008-0683.
CVE-2008-4626
Directory traversal vulnerability in index.php in Fritz Berger yet another php photo album - next generation (yappa-ng) 2.3.2 and possibly other versions through 2.3.3-beta0, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the album parameter.
CVE-2008-4627
SQL injection vulnerability in the rGallery plugin 1.09 for WoltLab Burning Board (WBB) allows remote attackers to execute arbitrary SQL commands via the itemID parameter in the RGalleryImageWrapper page in index.php.
CVE-2008-4628
SQL injection vulnerability in del.php in myWebland miniBloggie 1.0 allows remote attackers to execute arbitrary SQL commands via the post_id parameter.
CVE-2008-4629
Cross-site scripting (XSS) vulnerability in Usagi Project MyNETS 1.2.0 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-4630
Multiple unspecified vulnerabilities in Midgard Components (MidCOM) Framework before 8.09.1 have unknown impact and attack vectors.
CVE-2008-4631
Stack-based buffer overflow in the Message::AddToString function in message/Message.cpp in MUSCLE before 4.40 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted message.  NOTE: some of these details are obtained from third party information.
CVE-2008-4632
Multiple directory traversal vulnerabilities in index.php in Kure 0.6.3, when magic_quotes_gpc is disabled, allow remote attackers to read and possibly execute arbitrary local files via a .. (dot dot) in the (1) post and (2) doc parameters.
CVE-2008-4633
SQL injection vulnerability in Node Vote 5.x before 5.x-1.1 and 6.x before 6.x-1.0, a module for Drupal, when "Allow user to vote again" is enabled, allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors related to a "previously cast vote."
CVE-2008-4634
Cross-site scripting (XSS) vulnerability in Movable Type 4 through 4.21 allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to the administrative page, a different vulnerability than CVE-2008-4079.
CVE-2008-4635
Unspecified vulnerability in Hisanaga Electric Co, Ltd. hisa_cart 1.29 and earlier, a module for XOOPS, allows remote attackers to obtain sensitive user information via unknown vectors.
CVE-2008-4637
Cross-site scripting (XSS) vulnerability in cpCommerce before 1.2.4 allows remote attackers to inject arbitrary web script or HTML via unknown vectors in the advanced search feature.  NOTE: this is probably a variant of CVE-2008-4121.
Patch Information: http://cpcommerce.cpradio.org/downloads.php
CVE-2008-4638
qioadmin in the Quick I/O for Database feature in Symantec Veritas File System (VxFS) on HP-UX, and before 5.0 MP3 on Solaris, Linux, and AIX, allows local users to read arbitrary files by causing qioadmin to write a file's content to standard error in an error message.
CVE-2008-4639
jhead.c in Matthias Wandel jhead 2.84 and earlier allows local users to overwrite arbitrary files via a symlink attack on a temporary file.
CVE-2008-4640
The DoCommand function in jhead.c in Matthias Wandel jhead 2.84 and earlier allows local users to delete arbitrary files via vectors involving a modified input filename in which (1) a final "z" character is replaced by a "t" character or (2) a final "t" character is replaced by a "z" character.
CVE-2008-4641
The DoCommand function in jhead.c in Matthias Wandel jhead 2.84 and earlier allows attackers to execute arbitrary commands via shell metacharacters in unspecified input.
CVE-2008-4642
SQL injection vulnerability in profile.php in AstroSPACES 1.1.1 allows remote attackers to execute arbitrary SQL commands via the id parameter in a view action.
