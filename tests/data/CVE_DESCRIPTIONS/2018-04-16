CVE-2014-1686
MediaWiki 1.18.0 allows remote attackers to obtain the installation path via vectors related to thumbnail creation.
CVE-2014-2069
Absolute path traversal vulnerability in Eshtery CMS allows remote attackers to read arbitrary files via a full pathname in the file parameter to FileManager.aspx.
CVE-2015-1952
Cross-site scripting (XSS) vulnerability in IBM AppScan Enterprise Edition 9.0.x before 9.0.2 iFix 001 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors. IBM X-Force ID: 103416.
CVE-2016-9093
A version of the SymEvent Driver that shipped with Symantec Endpoint Protection 12.1 RU6 MP6 and earlier fails to properly sanitize logged-in user input. SEP 14.0 and later are not impacted by this issue. A non-admin user would need to be able to save an executable file to disk and then be able to successfully run that file. If properly constructed, the file could access the driver interface and potentially manipulate certain system calls. On all 32-bit systems and in most cases on 64-bit systems, this will result in a denial of service that will crash the system. In very narrow circumstances, and on 64-bit systems only, this could allow the user to run arbitrary code on the local machine with kernel-level privileges. This could result in a non-privileged user gaining privileged access on the local machine.
CVE-2016-9094
Symantec Endpoint Protection clients place detected malware in quarantine as part of the intended product functionality. The quarantine logs can be exported for review by the user in a variety of formats including .CSV files. Prior to 14.0 MP1 and 12.1 RU6 MP7, the potential exists for file metadata to be interpreted and evaluated as a formula. Successful exploitation of an attack of this type requires considerable direct user-interaction from the user exporting and then opening the log files on the intended target client.
CVE-2016-9592
openshift before versions 3.3.1.11, 3.2.1.23, 3.4 is vulnerable to a flaw when a volume fails to detach, which causes the delete operation to fail with 'VolumeInUse' error. Since the delete operation is retried every 30 seconds for each volume, this could lead to a denial of service attack as the number of API requests being sent to the cloud-provider exceeds the API's rate-limit.
CVE-2016-9593
foreman-debug before version 1.15.0 is vulnerable to a flaw in foreman-debug's logging. An attacker with access to the foreman log file would be able to view passwords, allowing them to access those systems.
CVE-2017-10140
Postfix before 2.11.10, 3.0.x before 3.0.10, 3.1.x before 3.1.6, and 3.2.x before 3.2.2 might allow local users to gain privileges by leveraging undocumented functionality in Berkeley DB 2.x and later, related to reading settings from DB_CONFIG in the current directory.
CVE-2017-6323
The Symantec Management Console prior to ITMS 8.1 RU1, ITMS 8.0_POST_HF6, and ITMS 7.6_POST_HF7 has an issue whereby XML input containing a reference to an external entity is processed by a weakly configured XML parser. This attack may lead to the disclosure of confidential data, denial of service, server side request forgery, port scanning from the perspective of the machine where the parser is located, and other system impacts.
CVE-2018-0530
SQL injection vulnerability in the Cybozu Garoon 3.5.0 to 4.2.6 allows remote authenticated attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2018-0531
Cybozu Garoon 3.0.0 to 4.2.6 allows remote authenticated attackers to bypass access restriction to view or alter an access privilege of a folder and/or notification settings via unspecified vectors.
CVE-2018-0532
Cybozu Garoon 3.0.0 to 4.2.6 allows remote authenticated attackers to bypass access restriction to alter setting data of the Standard database via unspecified vectors.
CVE-2018-0533
Cybozu Garoon 3.0.0 to 4.2.6 allows remote authenticated attackers to bypass access restriction to alter setting data of session authentication via unspecified vectors.
CVE-2018-0548
Cybozu Garoon 4.0.0 to 4.6.0 allows remote authenticated attackers to bypass access restriction to view the closed title of "Space" via unspecified vectors.
CVE-2018-0549
Cross-site scripting vulnerability in Cybozu Garoon 3.0.0 to 4.6.0 allows remote authenticated attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2018-0550
Cybozu Garoon 3.5.0 to 4.6.1 allows remote authenticated attackers to bypass access restriction to view the closed title of "Cabinet" via unspecified vectors.
CVE-2018-0551
Cross-site scripting vulnerability in Cybozu Garoon 3.0.0 to 4.6.1 allows remote authenticated attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2018-0560
Hatena Bookmark App for iOS Version 3.0 to 3.70 allows remote attackers to spoof the address bar via vectors related to URL display.
CVE-2018-0561
Untrusted search path vulnerability in The installer of PhishWall Client Internet Explorer edition Ver. 3.7.15 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2018-0562
Untrusted search path vulnerability in Installer of SoundEngine Free ver.5.21 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2018-0737
The OpenSSL RSA Key generation algorithm has been shown to be vulnerable to a cache timing side channel attack. An attacker with sufficient access to mount cache timing attacks during the RSA key generation process could recover the private key. Fixed in OpenSSL 1.1.0i-dev (Affected 1.1.0-1.1.0h). Fixed in OpenSSL 1.0.2p-dev (Affected 1.0.2b-1.0.2o).
CVE-2018-1000169
An exposure of sensitive information vulnerability exists in Jenkins 2.115 and older, LTS 2.107.1 and older, in CLICommand.java and ViewOptionHandler.java that allows unauthorized attackers to confirm the existence of agents or views with an attacker-specified name by sending a CLI command to Jenkins.
CVE-2018-1000170
A cross-site scripting vulnerability exists in Jenkins 2.115 and older, LTS 2.107.1 and older, in confirmationList.jelly and stopButton.jelly that allows attackers with Job/Configure and/or Job/Create permission to create an item name containing JavaScript that would be executed in another user's browser when that other user performs some UI actions.
CVE-2018-10070
A vulnerability in MikroTik Version 6.41.4 could allow an unauthenticated remote attacker to exhaust all available CPU and all available RAM by sending a crafted FTP request on port 21 that begins with many '\0' characters, preventing the affected router from accepting new FTP connections. The router will reboot after 10 minutes, logging a "router was rebooted without proper shutdown" message.
CVE-2018-10097
XSS exists in Domain Trader 2.5.3 via the recoverlogin.php email_address parameter.
CVE-2018-10100
Before WordPress 4.9.5, the redirection URL for the login page was not validated or sanitized if forced to use HTTPS.
CVE-2018-10101
Before WordPress 4.9.5, the URL validator assumed URLs with the hostname localhost were on the same host as the WordPress server.
CVE-2018-10102
Before WordPress 4.9.5, the version string was not escaped in the get_the_generator function, and could lead to XSS in a generator tag.
CVE-2018-10106
D-Link DIR-815 REV. B (with firmware through DIR-815_REVB_FIRMWARE_PATCH_2.07.B01) devices have permission bypass and information disclosure in /htdocs/web/getcfg.php, as demonstrated by a /getcfg.php?a=%0a_POST_SERVICES%3DDEVICE.ACCOUNT%0aAUTHORIZED_GROUP%3D1 request.
CVE-2018-10107
D-Link DIR-815 REV. B (with firmware through DIR-815_REVB_FIRMWARE_PATCH_2.07.B01) devices have XSS in the RESULT parameter to /htdocs/webinc/js/info.php.
CVE-2018-10108
D-Link DIR-815 REV. B (with firmware through DIR-815_REVB_FIRMWARE_PATCH_2.07.B01) devices have XSS in the Treturn parameter to /htdocs/webinc/js/bsc_sms_inbox.php.
CVE-2018-10109
Monstra CMS 3.0.4 has a stored XSS vulnerability when an attacker has access to the editor role, and enters the payload in the content section of a new page in the blog catalog.
CVE-2018-10111
An issue was discovered in GEGL through 0.3.32. The render_rectangle function in process/gegl-processor.c has unbounded memory allocation, leading to a denial of service (application crash) upon allocation failure.
CVE-2018-10112
An issue was discovered in GEGL through 0.3.32. The gegl_tile_backend_swap_constructed function in buffer/gegl-tile-backend-swap.c allows remote attackers to cause a denial of service (write access violation) or possibly have unspecified other impact via a malformed PNG file that is mishandled during a call to the babl_format_get_bytes_per_pixel function in babl-format.c in babl 0.1.46.
CVE-2018-10113
An issue was discovered in GEGL through 0.3.32. The process function in operations/external/ppm-load.c has unbounded memory allocation, leading to a denial of service (application crash) upon allocation failure.
CVE-2018-10114
An issue was discovered in GEGL through 0.3.32. The gegl_buffer_iterate_read_simple function in buffer/gegl-buffer-access.c allows remote attackers to cause a denial of service (write access violation) or possibly have unspecified other impact via a malformed PPM file, related to improper restrictions on memory allocation in the ppm_load_read_header function in operations/external/ppm-load.c.
CVE-2018-10117
An issue was discovered in idreamsoft iCMS V7.0.7. There is a CSRF vulnerability that can add an admin account via admincp.php?app=members&do=save&frame=iPHP.
CVE-2018-10118
Monstra CMS 3.0.4 has Stored XSS via the Name field on the Create New Page screen under the admin/index.php?id=pages URI, related to plugins/box/pages/pages.admin.php.
CVE-2018-10119
sot/source/sdstor/stgstrms.cxx in LibreOffice before 5.4.5.1 and 6.x before 6.0.1.1 uses an incorrect integer data type in the StgSmallStrm class, which allows remote attackers to cause a denial of service (use-after-free with write access) or possibly have unspecified other impact via a crafted document that uses the structured storage ole2 wrapper file format.
CVE-2018-10120
The SwCTBWrapper::Read function in sw/source/filter/ww8/ww8toolbar.cxx in LibreOffice before 5.4.6.1 and 6.x before 6.0.2.1 does not validate a customizations index, which allows remote attackers to cause a denial of service (heap-based buffer overflow with write access) or possibly have unspecified other impact via a crafted document that contains a certain Microsoft Word record.
CVE-2018-10121
plugins/box/pages/pages.admin.php in Monstra CMS 3.0.4 has a stored XSS vulnerability when an attacker has access to the editor role, and enters the payload in the title section of an admin/index.php?id=pages&action=edit_page&name=error404 (aka Edit 404 page) action.
CVE-2018-10122
QingDao Nature Easy Soft Chanzhi Enterprise Portal System (aka chanzhieps) pro1.6 allows remote attackers to read arbitrary files via directory traversal sequences in the pathname parameter to www/file.php.
CVE-2018-10124
The kill_something_info function in kernel/signal.c in the Linux kernel before 4.13, when an unspecified architecture and compiler is used, might allow local users to cause a denial of service via an INT_MIN argument.
CVE-2018-10127
An issue was discovered in XYHCMS 3.5. It has CSRF via an index.php?g=Manage&m=Rbac&a=addUser request, resulting in addition of an account with the administrator role.
CVE-2018-10128
An issue was discovered in XYHCMS 3.5. It has XSS via the test parameter to index.php.
CVE-2018-10132
PbootCMS v0.9.8 has CSRF via an admin.php/Message/mod/id/19.html?backurl=/index.php request, resulting in PHP code injection in the recontent parameter.
CVE-2018-10133
PbootCMS v0.9.8 allows PHP code injection via an IF label in index.php/About/6.html or admin.php/Site/index.html, related to the parserIfLabel function in \apps\home\controller\ParserController.php.
CVE-2018-10135
iScripts eSwap v2.4 has Reflected XSS via the "catwiseproducts.php" catid parameter in the User Panel.
CVE-2018-10136
iScripts UberforX 2.2 has Stored XSS in the "manage_settings" section of the Admin Panel via a value field to the /cms?section=manage_settings&action=edit URI.
CVE-2018-10137
iScripts UberforX 2.2 has CSRF in the "manage_settings" section of the Admin Panel via the /cms?section=manage_settings&action=edit URI.
CVE-2018-10138
The CATALooK.netStore module through 7.2.8 for DNN (formerly DotNetNuke) allows XSS via the /ViewEditGoogleMaps.aspx PortalID or CATSkin parameter, or the /ImageViewer.aspx link or desc parameter.
CVE-2018-10169
ProtonVPN 1.3.3 for Windows suffers from a SYSTEM privilege escalation vulnerability through the "ProtonVPN Service" service. This service establishes an NetNamedPipe endpoint that allows arbitrary installed applications to connect and call publicly exposed methods. The "Connect" method accepts a class instance argument that provides attacker control of the OpenVPN command line. An attacker can specify a dynamic library plugin that should run for every new VPN connection. This plugin will execute code in the context of the SYSTEM user.
CVE-2018-10170
NordVPN 6.12.7.0 for Windows suffers from a SYSTEM privilege escalation vulnerability through the "nordvpn-service" service. This service establishes an NetNamedPipe endpoint that allows arbitrary installed applications to connect and call publicly exposed methods. The "Connect" method accepts a class instance argument that provides attacker control of the OpenVPN command line. An attacker can specify a dynamic library plugin that should run for every new VPN connection attempt. This plugin will execute code in the context of the SYSTEM user.
CVE-2018-10172
7-Zip through 18.01 on Windows implements the "Large memory pages" option by calling the LsaAddAccountRights function to add the SeLockMemoryPrivilege privilege to the user's account, which makes it easier for attackers to bypass intended access restrictions by using this privilege in the context of a sandboxed process.
CVE-2018-10177
In ImageMagick 7.0.7-28, there is an infinite loop in the ReadOneMNGImage function of the coders/png.c file. Remote attackers could leverage this vulnerability to cause a denial of service via a crafted mng file.
CVE-2018-3846
In the ffgphd and ffgtkn functions in NASA CFITSIO 3.42, specially crafted images parsed via the library can cause a stack-based buffer overflow overwriting arbitrary data. An attacker can deliver an FIT image to trigger this vulnerability and potentially gain code execution.
CVE-2018-3848
In the ffghbn function in NASA CFITSIO 3.42, specially crafted images parsed via the library can cause a stack-based buffer overflow overwriting arbitrary data. An attacker can deliver an FIT image to trigger this vulnerability and potentially gain code execution.
CVE-2018-3849
In the ffghtb function in NASA CFITSIO 3.42, specially crafted images parsed via the library can cause a stack-based buffer overflow overwriting arbitrary data. An attacker can deliver an FIT image to trigger this vulnerability and potentially gain code execution.
CVE-2018-5382
Bouncy Castle BKS version 1 keystore (BKS-V1) files use an HMAC that is only 16 bits long, which can allow an attacker to compromise the integrity of a BKS-V1 keystore. All BKS-V1 keystores are vulnerable. Bouncy Castle release 1.47 introduces BKS version 2, which uses a 160-bit MAC.
CVE-2018-9153
The plugin upload component in Z-BlogPHP 1.5.1 allows remote attackers to execute arbitrary PHP code via the app_id parameter to zb_users/plugin/AppCentre/plugin_edit.php because of an unanchored regular expression, a different vulnerability than CVE-2018-8893. The component must be accessed directly by an administrator, or through CSRF.
CVE-2018-9169
Z-BlogPHP 1.5.1 has XSS via the zb_users/plugin/AppCentre/plugin_edit.php app_id parameter. The component must be accessed directly by an administrator, or through CSRF.
