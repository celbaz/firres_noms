CVE-2010-1325
Cross-site request forgery (CSRF) vulnerability in the apache2-slms package in SUSE Lifecycle Management Server (SLMS) 1.0 on SUSE Linux Enterprise (SLE) 11 allows remote attackers to hijack the authentication of unspecified victims via vectors related to improper parameter quoting.  NOTE: some sources report that this is a vulnerability in a product named "Apache SLMS," but that is incorrect.
CVE-2010-1507
WebYaST in yast2-webclient in SUSE Linux Enterprise (SLE) 11 on the WebYaST appliance uses a fixed secret key that is embedded in the appliance's image, which allows remote attackers to spoof session cookies by leveraging knowledge of this key.
CVE-2010-2226
The xfs_swapext function in fs/xfs/xfs_dfrag.c in the Linux kernel before 2.6.35 does not properly check the file descriptors passed to the SWAPEXT ioctl, which allows local users to leverage write access and obtain read access by swapping one file into another file.
CVE-2010-2240
The do_anonymous_page function in mm/memory.c in the Linux kernel before 2.6.27.52, 2.6.32.x before 2.6.32.19, 2.6.34.x before 2.6.34.4, and 2.6.35.x before 2.6.35.2 does not properly separate the stack and the heap, which allows context-dependent attackers to execute arbitrary code by writing to the bottom page of a shared memory segment, as demonstrated by a memory-exhaustion attack against the X.Org X server.
CVE-2010-2532
** DISPUTED **  lxsession-logout in lxsession in LXDE, as used on SUSE openSUSE 11.3 and other platforms, does not lock the screen when the Suspend or Hibernate button is pressed, which might make it easier for physically proximate attackers to access an unattended laptop via a resume action.  NOTE: there is no general agreement that this is a vulnerability, because separate control over locking can be an equally secure, or more secure, behavior in some threat environments.
CVE-2010-2954
The irda_bind function in net/irda/af_irda.c in the Linux kernel before 2.6.36-rc3-next-20100901 does not properly handle failure of the irda_open_tsap function, which allows local users to cause a denial of service (NULL pointer dereference and panic) and possibly have unspecified other impact via multiple unsuccessful calls to bind on an AF_IRDA (aka PF_IRDA) socket.
CVE-2010-3203
Directory traversal vulnerability in the PicSell (com_picsell) component 1.0 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the dflink parameter in a prevsell dwnfree action to index.php.
CVE-2010-3204
Multiple PHP remote file inclusion vulnerabilities in Pecio CMS 2.0.5 allow remote attackers to execute arbitrary PHP code via a URL in the template parameter to (1) post.php, (2) article.php, (3) blog.php, or (4) home.php in pec_templates/nova-blue/.
CVE-2010-3205
PHP remote file inclusion vulnerability in index.php in Textpattern CMS 4.2.0 allows remote attackers to execute arbitrary PHP code via a URL in the inc parameter.
CVE-2010-3206
Multiple PHP remote file inclusion vulnerabilities in DiY-CMS 1.0 allow remote attackers to execute arbitrary PHP code via a URL in the (1) lang parameter to modules/guestbook/blocks/control.block.php, (2) main_module parameter to index.php, and (3) getFile parameter to includes/general.functions.php.
CVE-2010-3207
SQL injection vulnerability in index.php in GaleriaSHQIP 1.0, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the album_id parameter.  NOTE: some of these details are obtained from third party information.
CVE-2010-3208
Cross-site scripting (XSS) vulnerability in ajax.php in Wiccle Web Builder (WWB) 1.00 and 1.0.1 allows remote attackers to inject arbitrary web script or HTML via the post_text parameter in a site custom_search action to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2010-3209
Multiple PHP remote file inclusion vulnerabilities in Seagull 0.6.7 allow remote attackers to execute arbitrary PHP code via a URL in the includeFile parameter to (1) Config/Container.php and (2) HTML/QuickForm.php in fog/lib/pear/, the (3) driverpath parameter to fog/lib/pear/DB/NestedSet.php, and the (4) path parameter to fog/lib/pear/DB/NestedSet/Output.php.
CVE-2010-3210
Multiple PHP remote file inclusion vulnerabilities in Multi-lingual E-Commerce System 0.2 allow remote attackers to execute arbitrary PHP code via a URL in the include_path parameter to (1) checkout2-CYM.php, (2) checkout2-EN.php, (3) checkout2-FR.php, (4) cat-FR.php, (5) cat-EN.php, (6) cat-CYM.php, (7) checkout1-CYM.php, (8) checkout1-EN.php, (9) checkout1-FR.php, (10) prod-CYM.php, (11) prod-EN.php, and (12) prod-FR.php in inc/.
CVE-2010-3211
Multiple SQL injection vulnerabilities in the JE FAQ Pro (com_jefaqpro) component 1.5.0 for Joomla! allow remote attackers to execute arbitrary SQL commands via category categorylist operations with (1) the catid parameter or (2) the catid parameter in a lists action.
CVE-2010-3212
SQL injection vulnerability in index.php in Seagull 0.6.7 and earlier allows remote attackers to execute arbitrary SQL commands via the frmQuestion parameter in a retrieve action, in conjunction with a user/password PATH_INFO.
