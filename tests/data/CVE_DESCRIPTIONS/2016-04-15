CVE-2010-5325
Heap-based buffer overflow in the unhtmlify function in foomatic-rip in foomatic-filters before 4.0.6 allows remote attackers to cause a denial of service (memory corruption and crash) or possibly execute arbitrary code via a long job title.
CVE-2015-5271
The TripleO Heat templates (tripleo-heat-templates) do not properly order the Identity Service (keystone) before the OpenStack Object Storage (Swift) staticweb middleware in the swiftproxy pipeline when the staticweb middleware is enabled, which might allow remote attackers to obtain sensitive information from private containers via unspecified vectors.
CVE-2015-5348
Apache Camel 2.6.x through 2.14.x, 2.15.x before 2.15.5, and 2.16.x before 2.16.1, when using (1) camel-jetty or (2) camel-servlet as a consumer in Camel routes, allow remote attackers to execute arbitrary commands via a crafted serialized Java object in an HTTP request.
CVE-2015-7676
Ipswitch MOVEit File Transfer (formerly DMZ) 8.1 and earlier, when configured to support file view on download, allows remote authenticated users to conduct cross-site scripting (XSS) attacks by uploading HTML files.
CVE-2016-0889
An HTTP servlet in vApp Manager in EMC Unisphere for VMAX Virtual Appliance before 8.2.0 allows remote attackers to write to arbitrary files via a crafted pathname.
CVE-2016-1264
Race condition in the Op command in Juniper Junos OS before 12.1X44-D55, 12.1X46 before 12.1X46-D40, 12.1X47 before 12.1X47-D25, 12.3 before 12.3R11, 12.3X48 before 12.3X48-D20, 12.3X50 before 12.3X50-D50, 13.2 before 13.2R8, 13.2X51 before 13.2X51-D39, 13.2X52 before 13.2X52-D30, 13.3 before 13.3R7, 14.1 before 14.1R6, 14.1X53 before 14.1X53-D30, 14.2 before 14.2R4, 15.1 before 15.1F2 or 15.1R2, 15.1X49 before 15.1X49-D10 or 15.1X49-D20, and 16.1 before 16.1R1 allows remote authenticated users to gain privileges via the URL option.
CVE-2016-1267
Race condition in the RPC functionality in Juniper Junos OS before 12.1X44-D55, 12.1X46 before 12.1X46-D40, 12.1X47 before 12.1X47-D25, 12.3 before 12.3R11, 12.3X48 before 12.3X48-D20, 13.2 before 13.2R8, 13.2X51 before 13.2X51-D39, 13.3 before 13.3R7, 14.1 before 14.1R6, 14.1X53 before 14.1X53-D30, 14.2 before 14.2R3-S4, 15.1 before 15.1F2, or 15.1R2, 15.1X49 before 15.1X49-D20, and 16.1 before 16.1R1 allows local users to read, delete, or modify arbitrary files via unspecified vectors.
CVE-2016-1268
The administrative web services interface in Juniper ScreenOS before 6.3.0r21 allows remote attackers to cause a denial of service (reboot) via a crafted SSL packet.
CVE-2016-1269
Juniper Junos OS before 12.1X44-D60, 12.1X46 before 12.1X46-D40, 12.1X47 before 12.1X47-D30, 12.3 before 12.3R11, 12.3X48 before 12.3X48-D20, 13.2 before 13.2R9, 13.2X51 before 13.2X51-D39, 13.3 before 13.3R8, 14.1 before 14.1R6, 14.1X53 before 14.1X53-D30, 14.2 before 14.2R4-S1, 15.1 before 15.1R2, 15.1X49 before 15.1X49-D30, and 16.1 before 16.1R1 allow remote attackers to cause a denial of service (socket consumption) via crafted TCP timestamps.
CVE-2016-1270
The rpd daemon in Juniper Junos OS before 12.1X44-D60, 12.1X46 before 12.1X46-D45, 12.1X47 before 12.1X47-D30, 12.3 before 12.3R9, 12.3X48 before 12.3X48-D20, 13.2 before 13.2R7, 13.2X51 before 13.2X51-D40, 13.3 before 13.3R6, 14.1 before 14.1R4, and 14.2 before 14.2R2, when configured with BGP-based L2VPN or VPLS, allows remote attackers to cause a denial of service (daemon restart) via a crafted L2VPN family BGP update.
CVE-2016-1271
Juniper Junos OS before 12.1X46-D45, 12.1X47 before 12.1X47-D30, 12.3 before 12.3R11, 12.3X48 before 12.3X48-D25, 13.2 before 13.2R8, 13.3 before 13.3R7, 14.1 before 14.1R6, 14.2 before 14.2R4, 15.1 before 15.1R1 or 15.1F2, and 15.1X49 before 15.1X49-D15 allow local users to gain privileges via crafted combinations of CLI commands and arguments, a different vulnerability than CVE-2015-3003, CVE-2014-3816, and CVE-2014-0615.
CVE-2016-1273
Juniper Junos OS before 13.2X51-D40, 14.x before 14.1X53-D30, and 15.x before 15.1X53-D20 on QFX5100 and QFX10002 switches do not have sufficient entropy, which makes it easier for remote attackers to defeat cryptographic encryption and authentication protection mechanisms via unspecified vectors.
CVE-2016-1274
Juniper Junos OS 14.1X53 before 14.1X53-D30 on QFX Series switches allows remote attackers to cause a denial of service (PFE panic) via a high rate of unspecified VXLAN packets.
CVE-2016-2076
Client Integration Plugin (CIP) in VMware vCenter Server 5.5 U3a, U3b, and U3c and 6.0 before U2; vCloud Director 5.5.5; and vRealize Automation Identity Appliance 6.2.4 before 6.2.4.1 mishandles session content, which allows remote attackers to hijack sessions via a crafted web site.
CVE-2016-2145
The am_read_post_data function in mod_auth_mellon before 0.11.1 does not check if the ap_get_client_block function returns an error, which allows remote attackers to cause a denial of service (segmentation fault and process crash) via a crafted POST data.
CVE-2016-2146
The am_read_post_data function in mod_auth_mellon before 0.11.1 does not limit the amount of data read, which allows remote attackers to cause a denial of service (worker process crash, web server deadlock, or memory consumption) via a large amount of POST data.
CVE-2016-2212
The getOrderByStatusUrlKey function in the Mage_Rss_Helper_Order class in app/code/core/Mage/Rss/Helper/Order.php in Magento Enterprise Edition before 1.14.2.3 and Magento Community Edition before 1.9.2.3 allows remote attackers to obtain sensitive order information via the order_id in a JSON object in the data parameter in an RSS feed request to index.php/rss/order/status.
CVE-2016-3144
Cross-site scripting (XSS) vulnerability in the Block Class module 7.x-2.x before 7.x-2.2 for Drupal allows remote authenticated users with the "Administer block classes" permission to inject arbitrary web script or HTML via a class name.
CVE-2016-3961
Xen and the Linux kernel through 4.5.x do not properly suppress hugetlbfs support in x86 PV guests, which allows local PV guest OS users to cause a denial of service (guest OS crash) by attempting to access a hugetlbfs mapped area.
