CVE-2008-2934
Mozilla Firefox 3 before 3.0.1 on Mac OS X allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted GIF file that triggers a free of an uninitialized pointer.
CVE-2008-3206
SQL injection vulnerability in browse.groups.php in Yuhhu Pubs Black Cat allows remote attackers to execute arbitrary SQL commands via the category parameter.
CVE-2008-3207
PHP remote file inclusion vulnerability in cms/modules/form.lib.php in Pragyan CMS 2.6.2, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the (1) sourceFolder or (2) moduleFolder parameter.
CVE-2008-3208
Simple DNS Plus 4.1, 5.0, and possibly other versions before 5.1.101 allows remote attackers to cause a denial of service via multiple DNS reply packets.
CVE-2008-3209
Heap-based buffer overflow in the OpenGifFile function in BiGif.dll in Black Ice Document Imaging SDK 10.95 allows remote attackers to execute arbitrary code via a long string argument to the GetNumberOfImagesInGifFile method in the BIImgFrm Control ActiveX control in biimgfrm.ocx.  NOTE: some of these details are obtained from third party information.
CVE-2008-3210
rutil/dns/DnsStub.cxx in ReSIProcate 1.3.2, as used by repro, allows remote attackers to cause a denial of service (daemon crash) via a SIP (1) INVITE or (2) OPTIONS message with a long domain name in a request URI, which triggers an assert error.
CVE-2008-3211
Scripteen Free Image Hosting Script 1.2 and 1.2.1 allows remote attackers to bypass authentication and gain administrative access by setting the cookid cookie value to 1.
CVE-2008-3212
Multiple SQL injection vulnerabilities in Scripteen Free Image Hosting Script 1.2.1 allow remote attackers to execute arbitrary SQL commands via the (1) username or (2) password parameter to admin/login.php, or the (3) uname or (4) pass parameter to login.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3213
SQL injection vulnerability in secciones/tablon/tablon.php in WebCMS Portal Edition allows remote attackers to execute arbitrary SQL commands via the id parameter to portal/index.php in a tablon action. NOTE: some of these details are obtained from third party information.
CVE-2008-3214
dnsmasq 2.25 allows remote attackers to cause a denial of service (daemon crash) by (1) renewing a nonexistent lease or (2) sending a DHCPREQUEST for an IP address that is not in the same network, related to the DHCP NAK response from the daemon.
CVE-2008-3215
libclamav/petite.c in ClamAV before 0.93.3 allows remote attackers to cause a denial of service via a malformed Petite file that triggers an out-of-bounds memory access.  NOTE: this issue exists because of an incomplete fix for CVE-2008-2713.
CVE-2008-3216
The save function in br/prefmanager.d in projectl 1.001 creates a projectL.prf file in the current working directory, which allows local users to overwrite arbitrary files via a symlink attack.
CVE-2008-3217
PowerDNS Recursor before 3.1.6 does not always use the strongest random number generator for source port selection, which makes it easier for remote attack vectors to conduct DNS cache poisoning.  NOTE: this is related to incomplete integration of security improvements associated with addressing CVE-2008-1637.
CVE-2008-3218
Multiple cross-site scripting (XSS) vulnerabilities in Drupal 6.x before 6.3 allow remote attackers to inject arbitrary web script or HTML via vectors related to (1) free tagging taxonomy terms, which are not properly handled on node preview pages, and (2) unspecified OpenID values.
CVE-2008-3219
The Drupal filter_xss_admin function in 5.x before 5.8 and 6.x before 6.3 does not "prevent use of the object HTML tag in administrator input," which has unknown impact and attack vectors, probably related to an insufficient cross-site scripting (XSS) protection mechanism.
CVE-2008-3220
Cross-site request forgery (CSRF) vulnerability in Drupal 5.x before 5.8 and 6.x before 6.3 allows remote attackers to perform administrative actions via vectors involving deletion of "translated strings."
CVE-2008-3221
Cross-site request forgery (CSRF) vulnerability in Drupal 6.x before 6.3 allows remote attackers to perform administrative actions via vectors involving deletion of OpenID identities.
CVE-2008-3222
Session fixation vulnerability in Drupal 5.x before 5.9 and 6.x before 6.3, when contributed modules "terminate the current request during a login event," allows remote attackers to hijack web sessions via unknown vectors.
CVE-2008-3223
SQL injection vulnerability in the Schema API in Drupal 6.x before 6.3 allows remote attackers to execute arbitrary SQL commands via vectors related to "an inappropriate placeholder for 'numeric' fields."
CVE-2008-3224
Unspecified vulnerability in phpBB before 3.0.1 has unknown impact and attack vectors related to "urls gone through redirect() being used within login_box()."
CVE-2008-3225
Joomla! before 1.5.4 allows attackers to access administration functionality, which has unknown impact and attack vectors related to a missing "LDAP security fix."
CVE-2008-3226
The file caching implementation in Joomla! before 1.5.4 allows attackers to access cached pages via unknown attack vectors.
CVE-2008-3227
Unspecified vulnerability in Joomla! before 1.5.4 has unknown impact and attack vectors related to a "User Redirect Spam fix," possibly an open redirect vulnerability.
CVE-2008-3228
Joomla! before 1.5.4 does not configure .htaccess to apply certain security checks that "block common exploits" to SEF URLs, which has unknown impact and remote attack vectors.
CVE-2008-3229
Stack-based buffer overflow in op before Changeset 563, when xauth support is enabled, allows local users to gain privileges via a long XAUTHORITY environment variable.
CVE-2008-3230
The ffmpeg lavf demuxer allows user-assisted attackers to cause a denial of service (application crash) via a crafted GIF file, possibly related to gstreamer, as demonstrated by lol-giftopnm.gif.
CVE-2008-3231
xine-lib before 1.1.15 allows remote attackers to cause a denial of service (crash) via a crafted OGG file, as demonstrated by playing lol-ffplay.ogg with xine.
CVE-2008-3232
Unrestricted file upload vulnerability in ecrire/images.php in Dotclear 1.2.7.1 and earlier allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in images.
CVE-2008-3233
Cross-site scripting (XSS) vulnerability in WordPress before 2.6, SVN development versions only, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-3234
sshd in OpenSSH 4 on Debian GNU/Linux, and the 20070303 OpenSSH snapshot, allows remote authenticated users to obtain access to arbitrary SELinux roles by appending a :/ (colon slash) sequence, followed by the role name, to the username.
