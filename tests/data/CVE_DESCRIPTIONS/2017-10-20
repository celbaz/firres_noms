CVE-2010-3659
Multiple cross-site scripting (XSS) vulnerabilities in TYPO3 CMS 4.1.x before 4.1.14, 4.2.x before 4.2.13, 4.3.x before 4.3.4, and 4.4.x before 4.4.1 allow remote authenticated backend users to inject arbitrary web script or HTML via unspecified parameters to the extension manager, or unspecified parameters to unknown backend forms.
CVE-2011-1935
pcap-linux.c in libpcap 1.1.1 before commit ea9432fabdf4b33cbc76d9437200e028f1c47c93 when snaplen is set may truncate packets, which might allow remote attackers to send arbitrary data while avoiding detection via crafted packets.
CVE-2013-6049
apt-listbugs before 0.1.10 creates temporary files insecurely, which allows attackers to have unspecified impact via unknown vectors.
CVE-2017-12628
The JMX server embedded in Apache James, also used by the command line client is exposed to a java de-serialization issue, and thus can be used to execute arbitrary commands. As James exposes JMX socket by default only on local-host, this vulnerability can only be used for privilege escalation. Release 3.0.1 upgrades the incriminated library.
CVE-2017-13127
The VIP.com application for IOS and Android allows remote attackers to obtain sensitive information and hijack the authentication of users via a rogue access point and a man-in-the-middle attack.
CVE-2017-14937
The airbag detonation algorithm allows injury to passenger-car occupants via predictable Security Access (SA) data to the internal CAN bus (or the OBD connector). This affects the airbag control units (aka pyrotechnical control units or PCUs) of unspecified passenger vehicles manufactured in 2014 or later, when the ignition is on and the speed is less than 6 km/h. Specifically, there are only 256 possible key pairs, and authentication attempts have no rate limit. In addition, at least one manufacturer's interpretation of the ISO 26021 standard is that it must be possible to calculate the key directly (i.e., the other 255 key pairs must not be used). Exploitation would typically involve an attacker who has already gained access to the CAN bus, and sends a crafted Unified Diagnostic Service (UDS) message to detonate the pyrotechnical charges, resulting in the same passenger-injury risks as in any airbag deployment.
CVE-2017-15291
Cross-site scripting (XSS) vulnerability in the Wireless MAC Filtering page in TP-LINK TL-MR3220 wireless routers allows remote attackers to inject arbitrary web script or HTML via the Description field.
CVE-2017-15651
PRTG Network Monitor 17.3.33.2830 allows remote authenticated administrators to execute arbitrary code by uploading a .exe file and then proceeding in spite of the error message.
CVE-2017-15670
The GNU C Library (aka glibc or libc6) before 2.27 contains an off-by-one error leading to a heap-based buffer overflow in the glob function in glob.c, related to the processing of home directories using the ~ operator followed by a long string.
CVE-2017-15671
The glob function in glob.c in the GNU C Library (aka glibc or libc6) before 2.27, when invoked with GLOB_TILDE, could skip freeing allocated memory when processing the ~ operator with a long user name, potentially leading to a denial of service (memory leak).
CVE-2017-2131
Panasonic KX-HJB1000 Home unit devices with firmware GHX1YG 14.50 or HJB1000_4.47 allow an attacker to bypass access restrictions to view the configuration menu via unspecified vectors.
CVE-2017-2132
Panasonic KX-HJB1000 Home unit devices with firmware GHX1YG 14.50 or HJB1000_4.47 allow an attacker to delete arbitrary files in a specific directory via unspecified vectors.
CVE-2017-2133
SQL injection vulnerability in Panasonic KX-HJB1000 Home unit devices with firmware GHX1YG 14.50 or HJB1000_4.47 allows authenticated attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2017-6141
In F5 BIG-IP LTM, AAM, AFM, APM, ASM, Link Controller, PEM, and WebSafe 12.1.0 through 12.1.2, certain values in a TLS abbreviated handshake when using a client SSL profile with the Session Ticket option enabled may cause disruption of service to the Traffic Management Microkernel (TMM). The Session Ticket option is disabled by default.
CVE-2017-6144
In F5 BIG-IP PEM 12.1.0 through 12.1.2 when downloading the Type Allocation Code (TAC) database file via HTTPS, the server's certificate is not verified. Attackers in a privileged network position may be able to launch a man-in-the-middle attack against these connections. TAC databases are used in BIG-IP PEM for Device Type and OS (DTOS) and Tethering detection. Customers not using BIG-IP PEM, not configuring downloads of TAC database files, or not using HTTP for that download are not affected.
CVE-2017-6145
iControl REST in F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, Link Controller, PEM, and WebSafe 12.0.0 through 12.1.2 and 13.0.0 includes a service to convert authorization BIGIPAuthCookie cookies to X-F5-Auth-Token tokens. This service does not properly re-validate cookies when making that conversion, allowing once-valid but now expired cookies to be converted to valid tokens.
CVE-2017-6165
In F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, GTM, Link Controller, PEM, and WebSafe 11.5.1 HF6 through 11.5.4 HF4, 11.6.0 through 11.6.1 HF1, and 12.0.0 through 12.1.2 on VIPRION platforms only, the script which synchronizes SafeNet External Network HSM configuration elements between blades in a clustered deployment will log the HSM partition password in cleartext to the "/var/log/ltm" log file.
