CVE-2008-0062
KDC in MIT Kerberos 5 (krb5kdc) does not set a global variable for some krb4 message types, which allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via crafted messages that trigger a NULL pointer dereference or double-free.
CVE-2008-0063
The Kerberos 4 support in KDC in MIT Kerberos 5 (krb5kdc) does not properly clear the unused portion of a buffer when generating an error message, which might allow remote attackers to obtain sensitive information, aka "Uninitialized stack values."
CVE-2008-0947
Buffer overflow in the RPC library used by libgssrpc and kadmind in MIT Kerberos 5 (krb5) 1.4 through 1.6.3 allows remote attackers to execute arbitrary code by triggering a large number of open file descriptors.
CVE-2008-0948
Buffer overflow in the RPC library (lib/rpc/rpc_dtablesize.c) used by libgssrpc and kadmind in MIT Kerberos 5 (krb5) 1.2.2, and probably other versions before 1.3, when running on systems whose unistd.h does not define the FD_SETSIZE macro, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code by triggering a large number of open file descriptors.
CVE-2008-1001
Cross-site scripting (XSS) vulnerability in Apple Safari before 3.1, when running on Windows XP or Vista, allows remote attackers to inject arbitrary web script or HTML via a crafted URL that is not properly handled in the error page.
CVE-2008-1002
Cross-site scripting (XSS) vulnerability in Apple Safari before 3.1 allows remote attackers to inject arbitrary web script or HTML via a crafted javascript: URL.
CVE-2008-1003
Cross-site scripting (XSS) vulnerability in WebCore, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to sites that set the document.domain property or have the same document.domain.
CVE-2008-1004
Cross-site scripting (XSS) vulnerability in WebCore, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to the Web Inspector.
CVE-2008-1005
WebCore, as used in Apple Safari before 3.1, does not properly mask the password field when reverse conversion is used with the Kotoeri input method, which allows physically proximate attackers to read the password.
CVE-2008-1006
Cross-site scripting (XSS) vulnerability in WebCore, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary web script or HTML by using the window.open function to change the security context of a web page.
CVE-2008-1007
WebCore, as used in Apple Safari before 3.1, does not enforce the frame navigation policy for Java applets, which allows remote attackers to conduct cross-site scripting (XSS) attacks.
CVE-2008-1008
Cross-site scripting (XSS) vulnerability in WebCore, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary web script or HTML via the document.domain property.
CVE-2008-1009
Cross-site scripting (XSS) vulnerability in WebCore, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary JavaScript by modifying the history object.
CVE-2008-1010
Buffer overflow in WebKit, as used in Apple Safari before 3.1, allows remote attackers to execute arbitrary code via crafted regular expressions in JavaScript.
CVE-2008-1011
Cross-site scripting (XSS) vulnerability in WebKit, as used in Apple Safari before 3.1, allows remote attackers to inject arbitrary web script or HTML via a frame that calls a method instance in another frame.
