CVE-2013-0211
Integer signedness error in the archive_write_zip_data function in archive_write_set_format_zip.c in libarchive 3.1.2 and earlier, when running on 64-bit machines, allows context-dependent attackers to cause a denial of service (crash) via unspecified vectors, which triggers an improper conversion between unsigned and signed types, leading to a buffer overflow.
CVE-2013-1442
Xen 4.0 through 4.3.x, when using AVX or LWP capable CPUs, does not properly clear previous data from registers when using an XSAVE or XRSTOR to extend the state components of a saved or restored vCPU after touching other restored extended registers, which allows local guest OSes to obtain sensitive information by reading the registers.
CVE-2013-1444
A certain Debian patch for txt2man 1.5.5, as used in txt2man 1.5.5-2, 1.5.5-4, and others, allows local users to overwrite arbitrary files via a symlink attack on /tmp/2222.
CVE-2013-1839
The strHdrAcptLangGetItem function in errorpage.cc in Squid 3.2.x before 3.2.9 and 3.3.x before 3.3.3 allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a "," character in an Accept-Language header.
CVE-2013-2218
Double free vulnerability in the virConnectListAllInterfaces method in interface/interface_backend_netcf.c in libvirt 1.0.6 allows remote attackers to cause a denial of service (libvirtd crash) via a filtering flag that causes an interface to be skipped, as demonstrated by the "virsh iface-list --inactive" command.
CVE-2013-2230
The qemu driver (qemu/qemu_driver.c) in libvirt before 1.1.1 allows remote authenticated users to cause a denial of service (daemon crash) via unspecified vectors involving "multiple events registration."
CVE-2013-2238
Multiple buffer overflows in the switch_perform_substitution function in switch_regex.c in FreeSWITCH 1.2 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via vectors related to the index and substituted variables.
CVE-2013-3417
The administrative web interface in Cisco Video Surveillance Operations Manager does not properly perform authentication, which allows remote attackers to watch video feeds via a crafted URL, aka Bug ID CSCtg72262.
CVE-2013-4136
ext/common/ServerInstanceDir.h in Phusion Passenger gem before 4.0.6 for Ruby allows local users to gain privileges or possibly change the ownership of arbitrary directories via a symlink attack on a directory with a predictable name in /tmp/.
CVE-2013-4153
Double free vulnerability in the qemuAgentGetVCPUs function in qemu/qemu_agent.c in libvirt 1.0.6 through 1.1.0 allows remote attackers to cause a denial of service (daemon crash) via a cpu count request, as demonstrated by the "virsh vcpucount dom --guest" command.
CVE-2013-4154
The qemuAgentCommand function in libvirt before 1.1.1, when a guest agent is not configured, allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via vectors related to "agent based cpu (un)plug," as demonstrated by the "virsh vcpucount foobar --guest" command.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2013-4222
OpenStack Identity (Keystone) Folsom, Grizzly 2013.1.3 and earlier, and Havana before havana-3 does not properly revoke user tokens when a tenant is disabled, which allows remote authenticated users to retain access via the token.
CVE-2013-4239
The xenDaemonListDefinedDomains function in xen/xend_internal.c in libvirt 1.1.1 allows remote authenticated users to cause a denial of service (memory corruption and crash) via vectors involving the virConnectListDefinedDomains API function.
CVE-2013-4291
The virSecurityManagerSetProcessLabel function in libvirt 0.10.2.7, 1.0.5.5, and 1.1.1, when the domain has read an uid:gid label, does not properly set group memberships, which allows local users to gain privileges.
CVE-2013-4292
libvirt 1.1.0 and 1.1.1 allows local users to cause a denial of service (memory consumption) via a large number of domain migrate parameters in certain RPC calls in (1) daemon/remote.c and (2) remote/remote_driver.c.
CVE-2013-4296
The remoteDispatchDomainMemoryStats function in daemon/remote.c in libvirt 0.9.1 through 0.10.1.x, 0.10.2.x before 0.10.2.8, 1.0.x before 1.0.5.6, and 1.1.x before 1.1.2 allows remote authenticated users to cause a denial of service (uninitialized pointer dereference and crash) via a crafted RPC call.
CVE-2013-4297
The virFileNBDDeviceAssociate function in util/virfile.c in libvirt 1.1.2 and earlier allows remote authenticated users to cause a denial of service (uninitialized pointer dereference and crash) via unspecified vectors.
CVE-2013-4310
Apache Struts 2.0.0 through 2.3.15.1 allows remote attackers to bypass access controls via a crafted action: prefix.
CVE-2013-4314
The X509Extension in pyOpenSSL before 0.13.1 does not properly handle a '\0' character in a domain name in the Subject Alternative Name field of an X.509 certificate, which allows man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority.
CVE-2013-4316
Apache Struts 2.0.0 through 2.3.15.1 enables Dynamic Method Invocation by default, which has unknown impact and attack vectors.
CVE-2013-4359
Integer overflow in kbdint.c in mod_sftp in ProFTPD 1.3.4d and 1.3.5r3 allows remote attackers to cause a denial of service (memory consumption) via a large response count value in an authentication request, which triggers a large memory allocation.
CVE-2013-4362
WEB-DAV Linux File System (davfs2) 1.4.6 and 1.4.7 allow local users to gain privileges via unknown attack vectors in (1) kernel_interface.c and (2) mount_davfs.c, related to the "system" function.
CVE-2013-4372
Multiple cross-site scripting (XSS) vulnerabilities in Fuse Management Console in Red Hat JBoss Fuse 6.0.0 before patch 3 and JBoss A-MQ 6.0.0 before patch 3 allow remote attackers to inject arbitrary web script or HTML via the (1) user field in the create user page or (2) profile version to the create profile page.
CVE-2013-4378
Cross-site scripting (XSS) vulnerability in HtmlSessionInformationsReport.java in JavaMelody 1.46 and earlier allows remote attackers to inject arbitrary web script or HTML via a crafted X-Forwarded-For header.
CVE-2013-4623
The x509parse_crt function in x509.h in PolarSSL 1.1.x before 1.1.7 and 1.2.x before 1.2.8 does not properly parse certificate messages during the SSL/TLS handshake, which allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a certificate message that contains a PEM encoded certificate.
CVE-2013-5504
Cross-site scripting (XSS) vulnerability in the Mobile Device Management (MDM) portal in Cisco Identity Services Engine (ISE) allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter, aka Bug ID CSCui30266.
CVE-2013-5505
Cross-site scripting (XSS) vulnerability in an administration page in Cisco Identity Services Engine (ISE) allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter, aka Bug ID CSCui30275.
CVE-2013-5651
The virBitmapParse function in util/virbitmap.c in libvirt before 1.1.2 allows context-dependent attackers to cause a denial of service (out-of-bounds read and crash) via a crafted bitmap, as demonstrated by a large nodeset value to numatune.
CVE-2013-5679
The authenticated-encryption feature in the symmetric-encryption implementation in the OWASP Enterprise Security API (ESAPI) for Java 2.x before 2.1.0 does not properly resist tampering with serialized ciphertext, which makes it easier for remote attackers to bypass intended cryptographic protection mechanisms via an attack against authenticity in the default configuration, involving a null MAC and a zero MAC length.
CVE-2013-5692
Directory traversal vulnerability in X2Engine X2CRM before 3.5 allows remote authenticated administrators to include and execute arbitrary local files via a .. (dot dot) in the file parameter to index.php/admin/translationManager.
CVE-2013-5693
Cross-site scripting (XSS) vulnerability in X2Engine X2CRM before 3.5 allows remote attackers to inject arbitrary web script or HTML via the model parameter to index.php/admin/editor.
CVE-2013-5697
SQL injection vulnerability in mod_accounting.c in the mod_accounting module 0.5 and earlier for Apache allows remote attackers to execute arbitrary SQL commands via a Host header.
CVE-2013-5960
The authenticated-encryption feature in the symmetric-encryption implementation in the OWASP Enterprise Security API (ESAPI) for Java 2.x before 2.1.0.1 does not properly resist tampering with serialized ciphertext, which makes it easier for remote attackers to bypass intended cryptographic protection mechanisms via an attack against the intended cipher mode in a non-default configuration, a different vulnerability than CVE-2013-5679.
CVE-2013-5961
Unrestricted file upload vulnerability in lazyseo.php in the Lazy SEO plugin 1.1.9 for WordPress allows remote attackers to execute arbitrary PHP code by uploading a PHP file, then accessing it via a direct request to the file in lazy-seo/.
CVE-2013-5962
Unrestricted file upload vulnerability in frames/upload-images.php in the Complete Gallery Manager plugin before 3.3.4 rev40279 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in wp-content/[year]/[month]/.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2013-5963
Unrestricted file upload vulnerability in multi.php in Simple Dropbox Upload plugin before 1.8.8.1 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in wp-content/uploads/wpdb/.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'


CVE-2013-5964
Cross-site scripting (XSS) vulnerability in the administration page in the Flag module 7.x-3.x before 7.x-3.1 for Drupal allows remote authenticated users with the "Administer flags" permission to inject arbitrary web script or HTML via the flag title.
CVE-2013-5965
The Node View Permissions module 7.x-1.x before 7.x-1.2 for Drupal does not properly implement the hook_query_alter function, which might allow remote attackers to obtain sensitive information by reading a node listing.
