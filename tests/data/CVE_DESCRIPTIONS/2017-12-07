CVE-2017-1000410
The Linux kernel version 3.3-rc1 and later is affected by a vulnerability lies in the processing of incoming L2CAP commands - ConfigRequest, and ConfigResponse messages. This info leak is a result of uninitialized stack variables that may be returned to an attacker in their uninitialized state. By manipulating the code flows that precede the handling of these configuration messages, an attacker can also gain some control over which data will be held in the uninitialized stack variables. This can allow him to bypass KASLR, and stack canaries protection - as both pointers and stack canaries may be leaked in this manner. Combining this vulnerability (for example) with the previously disclosed RCE vulnerability in L2CAP configuration parsing (CVE-2017-1000251) may allow an attacker to exploit the RCE against kernels which were built with the above mitigations. These are the specifics of this vulnerability: In the function l2cap_parse_conf_rsp and in the function l2cap_parse_conf_req the following variable is declared without initialization: struct l2cap_conf_efs efs; In addition, when parsing input configuration parameters in both of these functions, the switch case for handling EFS elements may skip the memcpy call that will write to the efs variable: ... case L2CAP_CONF_EFS: if (olen == sizeof(efs)) memcpy(&efs, (void *)val, olen); ... The olen in the above if is attacker controlled, and regardless of that if, in both of these functions the efs variable would eventually be added to the outgoing configuration request that is being built: l2cap_add_conf_opt(&ptr, L2CAP_CONF_EFS, sizeof(efs), (unsigned long) &efs); So by sending a configuration request, or response, that contains an L2CAP_CONF_EFS element, but with an element length that is not sizeof(efs) - the memcpy to the uninitialized efs variable can be avoided, and the uninitialized variable would be returned to the attacker (16 bytes).
CVE-2017-11937
The Microsoft Malware Protection Engine running on Microsoft Forefront and Microsoft Defender on Windows 7 SP1, Windows 8.1, Windows RT 8.1, Windows 10 Gold, 1511, 1607, and 1703, 1709 and Windows Server 2016, Windows Server, version 1709, Microsoft Exchange Server 2013 and 2016, does not properly scan a specially crafted file leading to remote code execution. aka "Microsoft Malware Protection Engine Remote Code Execution Vulnerability".
CVE-2017-1271
IBM Security Guardium 9.0, 9.1, and 9.5 supports interaction between multiple actors and allows those actors to negotiate which algorithm should be used as a protection mechanism such as encryption or authentication, but it does not select the strongest algorithm that is available to both parties. IBM X-Force ID: 124746.
CVE-2017-1336
IBM Infosphere BigInsights 4.2.0 could allow an attacker to inject code that could allow access to restricted data and files. IBM X-Force ID: 126244.
CVE-2017-1341
IBM WebSphere MQ 8.0 and 9.0 could allow, under special circumstances, an unauthorized user to access an object which they should have been denied access. IBM X-Force ID: 126456.
CVE-2017-1342
IBM Insights Foundation for Energy 2.0 could reveal sensitive information in error messages to authenticated users that could e used to conduct further attacks. IBM X-Force ID: 126457.
CVE-2017-1353
IBM Atlas eDiscovery Process Management 6.0.3 could allow an authenticated attacker to obtain sensitive information when an unsuspecting user clicks on unsafe third-party links. IBM X-Force ID: 126680.
CVE-2017-1354
IBM Atlas eDiscovery Process Management 6.0.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126681.
CVE-2017-1355
IBM Atlas eDiscovery Process Management 6.0.3 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 126682.
CVE-2017-1356
IBM Atlas eDiscovery Process Management 6.0.3 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database. IBM X-Force ID: 126683.
CVE-2017-1433
IBM WebSphere MQ 7.5, 8.0, and 9.0 could allow an authenticated user to insert messages with a corrupt RFH header into the channel which would cause it to restart. IBM X-Force ID: 127803.
CVE-2017-14386
The web user interface of Dell 2335dn and 2355dn Multifunction Laser Printers, firmware versions prior to V2.70.06.26 A13 and V2.70.45.34 A10 respectively, are affected by a cross-site scripting vulnerability. Attackers could potentially exploit this vulnerability to execute arbitrary HTML or JavaScript code in the user's browser session in the context of the affected website.
CVE-2017-1465
IBM TRIRIGA 3.2, 3.3, 3.4, and 3.5 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 128464.
CVE-2017-1481
IBM Sterling B2B Integrator Standard Edition 5.2 allows a user to view sensitive information that belongs to another user. IBM X-Force ID: 128619.
CVE-2017-1482
IBM Sterling B2B Integrator Standard Edition 5.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128620.
CVE-2017-1487
IBM Sterling File Gateway 2.2 could allow an authenticated attacker to obtain sensitive information such as login ids on the system. IBM X-Force ID: 128626.
CVE-2017-1497
IBM Sterling File Gateway 2.2 could allow an unauthorized user to view files they should not have access to providing they know the directory location of the file. IBM X-Force ID: 128695.
CVE-2017-1498
IBM Connections 5.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 129020.
CVE-2017-15121
A non-privileged user is able to mount a fuse filesystem on RHEL 6 or 7 and crash a system if an application punches a hole in a file that does not end aligned to a page boundary.
CVE-2017-16884
Cross-site scripting (XSS) vulnerability in MistServer before 2.13 allows remote attackers to inject arbitrary web script or HTML via vectors related to failed authentication requests alerts.
CVE-2017-17055
Artica Web Proxy before 3.06.112911 allows remote attackers to execute arbitrary code as root by conducting a cross-site scripting (XSS) attack involving the username-form-id parameter to freeradius.users.php.
CVE-2017-17381
The Virtio Vring implementation in QEMU allows local OS guest users to cause a denial of service (divide-by-zero error and QEMU process crash) by unsetting vring alignment while updating Virtio rings.
CVE-2017-17384
ISPConfig 3.x before 3.1.9 allows remote authenticated users to obtain root access by creating a crafted cron job.
CVE-2017-17430
Sangoma NetBorder / Vega Session Controller before 2.3.12-80-GA allows remote attackers to execute arbitrary commands via the web interface.
CVE-2017-17435
An issue was discovered in the software on Vaultek Gun Safe VT20i products, aka BlueSteal. An attacker can remotely unlock any safe in this product line without a valid PIN code. Even though the phone application requires it and there is a field to supply the PIN code in an authorization request, the safe does not check the PIN code, so an attacker can obtain authorization using any value. Once an attacker sees the Bluetooth Low Energy (BLE) advertisement for the safe, they need only to write a BLE characteristic to enable notifications, and send a crafted getAuthor packet that returns a temporary key, and an unlock packet including that temporary key. The safe then opens after the unlock packet is processed, with no verification of PIN or other credentials.
CVE-2017-17436
An issue was discovered in the software on Vaultek Gun Safe VT20i products. There is no encryption of the session between the Android application and the safe. The website and marketing materials advertise that this communication channel is encrypted with "Highest Level Bluetooth Encryption" and "Data transmissions are secure via AES256 bit encryption." These claims, however, are not true. Moreover, AES256 bit encryption is not supported in the Bluetooth Low Energy (BLE) standard, so it would have to be at the application level. This lack of encryption allows an individual to learn the passcode by eavesdropping on the communications between the application and the safe.
CVE-2017-17448
net/netfilter/nfnetlink_cthelper.c in the Linux kernel through 4.14.4 does not require the CAP_NET_ADMIN capability for new, get, and del operations, which allows local users to bypass intended access restrictions because the nfnl_cthelper_list data structure is shared across all net namespaces.
CVE-2017-17449
The __netlink_deliver_tap_skb function in net/netlink/af_netlink.c in the Linux kernel through 4.14.4, when CONFIG_NLMON is enabled, does not restrict observations of Netlink messages to a single net namespace, which allows local users to obtain sensitive information by leveraging the CAP_NET_ADMIN capability to sniff an nlmon interface for all Netlink activity on the system.
CVE-2017-17450
net/netfilter/xt_osf.c in the Linux kernel through 4.14.4 does not require the CAP_NET_ADMIN capability for add_callback and remove_callback operations, which allows local users to bypass intended access restrictions because the xt_osf_fingers data structure is shared across all net namespaces.
CVE-2017-17451
The WP Mailster plugin before 1.5.5 for WordPress has XSS in the unsubscribe handler via the mes parameter to view/subscription/unsubscribe2.php.
CVE-2017-17456
The function d2alaw_array() in alaw.c of libsndfile 1.0.29pre1 may lead to a remote DoS attack (SEGV on unknown address 0x000000000000), a different vulnerability than CVE-2017-14245.
CVE-2017-17457
The function d2ulaw_array() in ulaw.c of libsndfile 1.0.29pre1 may lead to a remote DoS attack (SEGV on unknown address 0x000000000000), a different vulnerability than CVE-2017-14246.
CVE-2017-17458
In Mercurial before 4.4.1, it is possible that a specially malformed repository can cause Git subrepositories to run arbitrary code in the form of a .git/hooks/post-update script checked into the repository. Typical use of Mercurial prevents construction of such repositories, but they can be created programmatically.
CVE-2017-17459
http_transport.c in Fossil before 2.4, when the SSH sync protocol is used, allows user-assisted remote attackers to execute arbitrary commands via an ssh URL with an initial dash character in the hostname, a related issue to CVE-2017-9800, CVE-2017-12836, CVE-2017-12976, CVE-2017-14176, CVE-2017-16228, CVE-2017-1000116, and CVE-2017-1000117.
CVE-2017-3737
OpenSSL 1.0.2 (starting from version 1.0.2b) introduced an "error state" mechanism. The intent was that if a fatal error occurred during a handshake then OpenSSL would move into the error state and would immediately fail if you attempted to continue the handshake. This works as designed for the explicit handshake functions (SSL_do_handshake(), SSL_accept() and SSL_connect()), however due to a bug it does not work correctly if SSL_read() or SSL_write() is called directly. In that scenario, if the handshake fails then a fatal error will be returned in the initial function call. If SSL_read()/SSL_write() is subsequently called by the application for the same SSL object then it will succeed and the data is passed without being decrypted/encrypted directly from the SSL/TLS record layer. In order to exploit this issue an application bug would have to be present that resulted in a call to SSL_read()/SSL_write() being issued after having already received a fatal error. OpenSSL version 1.0.2b-1.0.2m are affected. Fixed in OpenSSL 1.0.2n. OpenSSL 1.1.0 is not affected.
CVE-2017-3738
There is an overflow bug in the AVX2 Montgomery multiplication procedure used in exponentiation with 1024-bit moduli. No EC algorithms are affected. Analysis suggests that attacks against RSA and DSA as a result of this defect would be very difficult to perform and are not believed likely. Attacks against DH1024 are considered just feasible, because most of the work necessary to deduce information about a private key may be performed offline. The amount of resources required for such an attack would be significant. However, for an attack on TLS to be meaningful, the server would have to share the DH1024 private key among multiple clients, which is no longer an option since CVE-2016-0701. This only affects processors that support the AVX2 but not ADX extensions like Intel Haswell (4th generation). Note: The impact from this issue is similar to CVE-2017-3736, CVE-2017-3732 and CVE-2015-3193. OpenSSL version 1.0.2-1.0.2m and 1.1.0-1.1.0g are affected. Fixed in OpenSSL 1.0.2n. Due to the low severity of this issue we are not issuing a new release of OpenSSL 1.1.0 at this time. The fix will be included in OpenSSL 1.1.0h when it becomes available. The fix is also available in commit e502cc86d in the OpenSSL git repository.
