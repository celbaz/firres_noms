CVE-2015-5059
The "Project Documentation" feature in MantisBT 1.2.19 and earlier, when the threshold to access files ($g_view_proj_doc_threshold) is set to ANYBODY, allows remote authenticated users to download attachments linked to arbitrary private projects via a file id number in the file_id parameter to file_download.php.
CVE-2017-11129
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android. The keystore is locked with a hard-coded password. Therefore, everyone with access to the keystore can read the content out, for example the private key of the user.
CVE-2017-11130
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android, through 0.0.80w for Web, and through 0.0.86 for Desktop. The product's protocol only tries to ensure confidentiality. In the whole protocol, no integrity or authenticity checks are done. Therefore man-in-the-middle attackers can conduct replay attacks.
CVE-2017-11131
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android, through 0.0.80w for Web, and through 0.0.86 for Desktop. For authentication, the user password is hashed directly with SHA-512 without a salt or another key-derivation mechanism to enable a secure secret for authentication. Moreover, only the first 32 bytes of the hash are used. This allows for easy dictionary and rainbow-table attacks if an attacker has access to the password hash.
CVE-2017-11132
An issue was discovered in heinekingmedia StashCat before 1.5.18 for Android. No certificate pinning is implemented; therefore the attacker could issue a certificate for the backend and the application would not notice it.
CVE-2017-11133
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android, through 0.0.80w for Web, and through 0.0.86 for Desktop. To encrypt messages, AES in CBC mode is used with a pseudo-random secret. This secret and the IV are generated with math.random() in previous versions and with CryptoJS.lib.WordArray.random() in newer versions, which uses math.random() internally. This is not cryptographically strong.
CVE-2017-11134
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android. The login credentials are written into a log file on the device. Hence, an attacker with access to the logs can read them.
CVE-2017-11135
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android, through 0.0.80w for Web, and through 0.0.86 for Desktop. The logout mechanism does not check for authorization. Therefore, an attacker only needs to know the device ID. This causes a denial of service. This might be interpreted as a vulnerability in customer-controlled software, in the sense that the StashCat client side has no secure way to signal that it is ending a session and that data should be deleted.
CVE-2017-11136
An issue was discovered in heinekingmedia StashCat through 1.7.5 for Android, through 0.0.80w for Web, and through 0.0.86 for Desktop. It uses RSA to exchange a secret for symmetric encryption of messages. However, the private RSA key is not only stored on the client but transmitted to the backend, too. Moreover, the key to decrypt the private key is composed of the first 32 bytes of the SHA-512 hash of the user password. But this hash is stored on the backend, too. Therefore, everyone with access to the backend database can read the transmitted secret for symmetric encryption, hence can read the communication.
CVE-2017-11379
Configuration and database backup archives are not signed or validated in Trend Micro Deep Discovery Director 1.1.
CVE-2017-11380
Backup archives were found to be encrypted with a static password across different installations, which suggest the same password may be used in all virtual appliance instances of Trend Micro Deep Discovery Director 1.1.
CVE-2017-11381
A command injection vulnerability exists in Trend Micro Deep Discovery Director 1.1 that allows an attacker to restore accounts that can access the pre-configuration console.
CVE-2017-11552
mpg321.c in mpg321 0.3.2-1 does not properly manage memory for use with libmad 0.15.1b, which allows remote attackers to cause a denial of service (memory corruption seen in a crash in the mad_decoder_run function in decoder.c in libmad) via a crafted MP3 file.
CVE-2017-12061
An XSS issue was discovered in admin/install.php in MantisBT before 1.3.12 and 2.x before 2.5.2. Some variables under user control in the MantisBT installation script are not properly sanitized before being output, allowing remote attackers to inject arbitrary JavaScript code, as demonstrated by the $f_database, $f_db_username, and $f_admin_username variables. This is mitigated by the fact that the admin/ folder should be deleted after installation, and also prevented by CSP.
CVE-2017-12062
An XSS issue was discovered in manage_user_page.php in MantisBT 2.x before 2.5.2. The 'filter' field is not sanitized before being rendered in the Manage User page, allowing remote attackers to execute arbitrary JavaScript code if CSP is disabled.
CVE-2017-12064
The csv_log_html function in library/edihistory/edih_csv_inc.php in OpenEMR 5.0.0 and prior allows attackers to bypass intended access restrictions via a crafted name.
CVE-2017-12065
spikekill.php in Cacti before 1.1.16 might allow remote attackers to execute arbitrary code via the avgnan, outlier-start, or outlier-end parameter.
CVE-2017-12066
Cross-site scripting (XSS) vulnerability in aggregate_graphs.php in Cacti before 1.1.16 allows remote authenticated users to inject arbitrary web script or HTML via specially crafted HTTP Referer headers, related to the $cancel_url variable. NOTE: this vulnerability exists because of an incomplete fix (lack of the htmlspecialchars ENT_QUOTES flag) for CVE-2017-11163.
CVE-2017-12067
Potrace 1.14 has a heap-based buffer over-read in the interpolate_cubic function in mkbitmap.c.
CVE-2017-12068
The Event List plugin 0.7.9 for WordPress has XSS in the slug array parameter to wp-admin/admin.php in an el_admin_categories delete_bulk action.
CVE-2017-12131
The Easy Testimonials plugin 3.0.4 for WordPress has XSS in include/settings/display.options.php, as demonstrated by the Default Testimonials Width, View More Testimonials Link, and Testimonial Excerpt Options screens.
CVE-2017-12132
The DNS stub resolver in the GNU C Library (aka glibc or libc6) before version 2.26, when EDNS support is enabled, will solicit large UDP responses from name servers, potentially simplifying off-path DNS spoofing attacks due to IP fragmentation.
CVE-2017-1500
A Reflected Cross Site Scripting (XSS) vulnerability exists in the authorization function exposed by RESTful Web Api of IBM Worklight Framework 6.1, 6.2, 6.3, 7.0, 7.1, and 8.0. The vulnerable parameter is "scope"; if you set as its value a "realm" not defined in authenticationConfig.xml, you get an HTTP 403 Forbidden response and the value will be reflected in the body of the HTTP response. By setting it to arbitrary JavaScript code it is possible to modify the flow of the authorization function, potentially leading to credential disclosure within a trusted session.
CVE-2017-4921
VMware vCenter Server (6.5 prior to 6.5 U1) contains an insecure library loading issue that occurs due to the use of LD_LIBRARY_PATH variable in an unsafe manner. Successful exploitation of this issue may allow unprivileged host users to load a shared library that may lead to privilege escalation.
CVE-2017-4922
VMware vCenter Server (6.5 prior to 6.5 U1) contains an information disclosure issue due to the service startup script using world writable directories as temporary storage for critical information. Successful exploitation of this issue may allow unprivileged host users to access certain critical information when the service gets restarted.
CVE-2017-4923
VMware vCenter Server (6.5 prior to 6.5 U1) contains an information disclosure vulnerability. This issue may allow plaintext credentials to be obtained when using the vCenter Server Appliance file-based backup feature.
CVE-2017-8571
Microsoft Outlook 2007 SP3, Outlook 2010 SP2, Outlook 2013 SP1, Outlook 2013 RT SP1, and Outlook 2016 as packaged in Microsoft Office allows a security feature bypass vulnerability due to the way that it handles input, aka "Microsoft Office Outlook Security Feature Bypass Vulnerability".
CVE-2017-8572
Microsoft Outlook 2007 SP3, Outlook 2010 SP2, Outlook 2013 SP1, Outlook 2013 RT SP1, and Outlook 2016 as packaged in Microsoft Office allows an information disclosure vulnerability due to the way that it discloses the contents of its memory, aka "Microsoft Office Outlook Information Disclosure Vulnerability".
CVE-2017-8663
Microsoft Outlook 2007 SP3, Outlook 2010 SP2, Outlook 2013 SP1, Outlook 2013 RT SP1, and Outlook 2016 as packaged in Microsoft Office allows a remote code execution vulnerability due to the way Microsoft Outlook parses specially crafted email messages, aka "Microsoft Office Outlook Memory Corruption Vulnerability"
