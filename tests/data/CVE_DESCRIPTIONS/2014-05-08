CVE-2012-5477
The smart proxy in Foreman before 1.1 uses a umask set to 0, which allows local users to modify files created by the daemon via unspecified vectors.
CVE-2013-0171
Foreman before 1.1 allows remote attackers to execute arbitrary code via a crafted YAML object to the (1) fact or (2) report import API.
CVE-2013-0173
Foreman before 1.1 uses a salt of "foreman" to hash root passwords, which makes it easier for attackers to guess the password via a brute force attack.
CVE-2013-0174
The external node classifier (ENC) API in Foreman before 1.1 allows remote attackers to obtain the hashed root password via an API request.
CVE-2013-0187
Foreman before 1.1 allows remote authenticated users to gain privileges via a (1) XMLHttpRequest or (2) AJAX request.
CVE-2013-0210
The smart proxy Puppet run API in Foreman before 1.2.0 allows remote attackers to execute arbitrary commands via vectors related to escaping and Puppet commands.
CVE-2013-0345
varnish 3.0.3 uses world-readable permissions for the /var/log/varnish/ directory and the log files in the directory, which allows local users to obtain sensitive information by reading the files.  NOTE: some of these details are obtained from third party information.
CVE-2013-3571
socat 1.2.0.0 before 1.7.2.2 and 2.0.0-b1 before 2.0.0-b6, when used for a listen type address and the fork option is enabled, allows remote attackers to cause a denial of service (file descriptor consumption) via multiple request that are refused based on the (1) sourceport, (2) lowport, (3) range, or (4) tcpwrap restrictions.
CVE-2013-4544
hw/net/vmxnet3.c in QEMU 2.0.0-rc0, 1.7.1, and earlier allows local guest users to cause a denial of service or possibly execute arbitrary code via vectors related to (1) RX or (2) TX queue numbers or (3) interrupt indices.  NOTE: some of these details are obtained from third party information.
CVE-2013-5016
Symantec Critical System Protection (SCSP) before 5.2.9, when installed on an unpatched Windows Server 2003 R2 platform, allows remote attackers to bypass policy settings via unspecified vectors.
CVE-2013-5916
Cross-site scripting (XSS) vulnerability in falha.php in the Bradesco Gateway plugin 2.0 for Wordpress, as used in the WP e-Commerce plugin, allows remote attackers to inject arbitrary web script or HTML via the QUERY_STRING.
CVE-2013-6372
The Subversion plugin before 1.54 for Jenkins stores credentials using base64 encoding, which allows local users to obtain passwords and SSH private keys by reading a subversion.credentials file.
CVE-2013-6889
GNU Rush 1.7 does not properly drop privileges, which allows local users to read arbitrary files via the --lint option.
CVE-2013-7041
The pam_userdb module for Pam uses a case-insensitive method to compare hashed passwords, which makes it easier for attackers to guess the password via a brute force attack.
CVE-2014-0056
The l3-agent in OpenStack Neutron 2012.2 before 2013.2.3 does not check the tenant id when creating ports, which allows remote authenticated users to plug ports into the routers of arbitrary tenants via the device id in a port-create command.
CVE-2014-0090
Session fixation vulnerability in Foreman before 1.4.2 allows remote attackers to hijack web sessions via the session id cookie.
CVE-2014-0109
Apache CXF before 2.6.14 and 2.7.x before 2.7.11 allows remote attackers to cause a denial of service (memory consumption) via a large request with the Content-Type set to text/html to a SOAP endpoint, which triggers an error.
CVE-2014-0110
Apache CXF before 2.6.14 and 2.7.x before 2.7.11 allows remote attackers to cause a denial of service (/tmp disk consumption) via a large invalid SOAP message.
CVE-2014-0116
CookieInterceptor in Apache Struts 2.x before 2.3.16.3, when a wildcard cookiesName value is used, does not properly restrict access to the getClass method, which allows remote attackers to "manipulate" the ClassLoader and modify session state via a crafted request.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-0113.
CVE-2014-0134
The instance rescue mode in OpenStack Compute (Nova) 2013.2 before 2013.2.3 and Icehouse before 2014.1, when using libvirt to spawn images and use_cow_images is set to false, allows remote authenticated users to read certain compute host files by overwriting an instance disk with a crafted image.
CVE-2014-0135
Kafo before 0.3.17 and 0.4.x before 0.5.2, as used by Foreman, uses world-readable permissions for default_values.yaml, which allows local users to obtain passwords and other sensitive information by reading the file.
CVE-2014-0190
The GIF decoder in QtGui in Qt before 5.3 allows remote attackers to cause a denial of service (NULL pointer dereference) via invalid width and height values in a GIF image.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"
CVE-2014-0192
Foreman 1.4.0 before 1.5.0 does not properly restrict access to provisioning template previews, which allows remote attackers to obtain sensitive information via the hostname parameter, related to "spoof."
CVE-2014-0362
Cross-site scripting (XSS) vulnerability on Google Search Appliance (GSA) devices before 7.0.14.G.216 and 7.2 before 7.2.0.G.114, when dynamic navigation is configured, allows remote attackers to inject arbitrary web script or HTML via input included in a SCRIPT element.
CVE-2014-0595
/opt/novell/ncl/bin/nwrights in Novell Client for Linux in Novell Open Enterprise Server (OES) 11 Linux SP2 does not properly manage a certain array, which allows local users to obtain the S permission in opportunistic circumstances by leveraging the granting of the F permission by an administrator.
CVE-2014-0930
The ptrace system call in IBM AIX 5.3, 6.1, and 7.1, and VIOS 2.2.x, allows local users to cause a denial of service (system crash) or obtain sensitive information from kernel memory via a crafted PT_LDINFO operation.
CVE-2014-0963
The Reverse Proxy feature in IBM Global Security Kit (aka GSKit) in IBM Security Access Manager (ISAM) for Web 7.0 before 7.0.0-ISS-SAM-IF0006 and 8.0 before 8.0.0.3-ISS-WGA-IF0002 allows remote attackers to cause a denial of service (infinite loop) via crafted SSL messages.
Per: http://www-01.ibm.com/support/docview.wss?uid=swg21672192

"Affected Products and Versions

All versions of IBM Security Access Manager for Web, both software and appliance: 7.0, 8.0"
CVE-2014-1682
The API in Zabbix before 1.8.20rc1, 2.0.x before 2.0.11rc1, and 2.2.x before 2.2.2rc1 allows remote authenticated users to spoof arbitrary users via the user name in a user.login request.
CVE-2014-1685
The Frontend in Zabbix before 1.8.20rc2, 2.0.x before 2.0.11rc2, and 2.2.x before 2.2.2rc1 allows remote "Zabbix Admin" users to modify the media of arbitrary users via unspecified vectors.
CVE-2014-1934
tag.py in eyeD3 (aka python-eyed3) 7.0.3, 0.6.18, and earlier for Python allows local users to modify arbitrary files via a symlink attack on a temporary file.
CVE-2014-2132
Cisco WebEx Recording Format (WRF) player and Advanced Recording Format (ARF) player T27 LD before SP32 EP16, T28 before T28.12, and T29 before T29.2 allow remote attackers to cause a denial of service (application crash) via a crafted (1) .wrf or (2) .arf file that triggers a buffer over-read, aka Bug ID CSCuh52768.
CVE-2014-2133
Buffer overflow in Cisco Advanced Recording Format (ARF) player T27 LD before SP32 EP16, T28 before T28.12, and T29 before T29.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted .arf file that triggers improper LZW decompression, aka Bug ID CSCuj87565.
CVE-2014-2134
Heap-based buffer overflow in Cisco WebEx Recording Format (WRF) player T27 LD before SP32 EP16, T28 before T28.12, and T29 before T29.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted audio channel in a .wrf file, aka Bug ID CSCuc39458.
CVE-2014-2135
Buffer overflow in Cisco Advanced Recording Format (ARF) player T27 LD before SP32 EP16, T28 before T28.12, and T29 before T29.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted .arf file, aka Bug IDs CSCul87216 and CSCuj07603.
CVE-2014-2136
Buffer overflow in Cisco Advanced Recording Format (ARF) player T27 LD before SP32 EP16, T28 before T28.12, and T29 before T29.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted .arf file, aka Bug IDs CSCui72223, CSCul01163, and CSCul01166.
CVE-2014-2602
Unspecified vulnerability in HP OneView 1.0 and 1.01 allows remote authenticated users to gain privileges via unknown vectors.
CVE-2014-2689
Cross-site scripting (XSS) vulnerability in Offiria 2.1.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to installer/index.php.
CVE-2014-2854
Cross-site scripting (XSS) vulnerability in the SemanticTitle extension before 1.1.0 for MediaWiki allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-2933
Directory traversal vulnerability in dirmng/index.php in Caldera 9.20 allows remote attackers to access arbitrary directories via a crafted pathname.
CVE-2014-2934
Multiple SQL injection vulnerabilities in Caldera 9.20 allow remote attackers to execute arbitrary SQL commands via the tr parameter to (1) costview2/jobs.php or (2) costview2/printers.php.
CVE-2014-2935
costview3/xmlrpc_server/xmlrpc.php in CostView in Caldera 9.20 allows remote attackers to execute arbitrary commands via shell metacharacters in a methodCall element in a PHP XMLRPC request.
CVE-2014-2936
The directory manager in Caldera 9.20 allows remote attackers to conduct variable-injection attacks in the global scope via (1) the maindir_hotfolder parameter to dirmng/index.php, or an unspecified parameter to (2) PPD/index.php, (3) dirmng/docmd.php, or (4) dirmng/param.php.
CVE-2014-3115
Multiple cross-site request forgery (CSRF) vulnerabilities in the web administration console in Fortinet FortiWeb before 5.2.0 allow remote attackers to hijack the authentication of administrators via system/config/adminadd and other unspecified vectors.
CVE-2014-3123
Cross-site scripting (XSS) vulnerability in admin/manage-images.php in the NextCellent Gallery plugin before 1.19.18 for WordPress allows remote authenticated users with the NextGEN Upload images, NextGEN Manage gallery, or NextGEN Manage others gallery permission to inject arbitrary web script or HTML via the "Alt & Title Text" field.
CVE-2014-3207
Cross-site scripting (XSS) vulnerability in wserver.ml in SKS Keyserver before 1.1.5 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to pks/lookup/undefined1.
CVE-2014-3215
seunshare in policycoreutils 2.2.5 is owned by root with 4755 permissions, and executes programs in a way that changes the relationship between the setuid system call and the getresuid saved set-user-ID value, which makes it easier for local users to gain privileges by leveraging a program that mistakenly expected that it could permanently drop privileges.
CVE-2014-3421
lisp/gnus/gnus-fun.el in GNU Emacs 24.3 and earlier allows local users to overwrite arbitrary files via a symlink attack on the /tmp/gnus.face.ppm temporary file.
CVE-2014-3422
lisp/emacs-lisp/find-gc.el in GNU Emacs 24.3 and earlier allows local users to overwrite arbitrary files via a symlink attack on a temporary file under /tmp/esrc/.
CVE-2014-3423
lisp/net/browse-url.el in GNU Emacs 24.3 and earlier allows local users to overwrite arbitrary files via a symlink attack on a /tmp/Mosaic.##### temporary file.
CVE-2014-3424
lisp/net/tramp-sh.el in GNU Emacs 24.3 and earlier allows local users to overwrite arbitrary files via a symlink attack on a /tmp/tramp.##### temporary file.
CVE-2014-3425
NCSA Mosaic 2.0 and earlier allows local users to cause a denial of service ("remote control" outage) by creating a /tmp/xmosaic.pid file for every possible PID.
CVE-2014-3426
NCSA Mosaic 2.1 through 2.7b5 allows local users to cause a denial of service ("remote control" outage) by creating a /tmp/Mosaic.pid file for every possible PID.
