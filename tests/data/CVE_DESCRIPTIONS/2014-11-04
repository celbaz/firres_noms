CVE-2013-4148
Integer signedness error in the virtio_net_load function in hw/net/virtio-net.c in QEMU 1.x before 1.7.2 allows remote attackers to execute arbitrary code via a crafted savevm image, which triggers a buffer overflow.
CVE-2013-4149
Buffer overflow in virtio_net_load function in net/virtio-net.c in QEMU 1.3.0 through 1.7.x before 1.7.2 might allow remote attackers to execute arbitrary code via a large MAC table.
CVE-2013-4150
The virtio_net_load function in hw/net/virtio-net.c in QEMU 1.5.0 through 1.7.x before 1.7.2 allows remote attackers to cause a denial of service or possibly execute arbitrary code via vectors in which the value of curr_queues is greater than max_queues, which triggers an out-of-bounds write.
CVE-2013-4151
The virtio_load function in virtio/virtio.c in QEMU 1.x before 1.7.2 allows remote attackers to execute arbitrary code via a crafted savevm image, which triggers an out-of-bounds write.
CVE-2013-4526
Buffer overflow in hw/ide/ahci.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service and possibly execute arbitrary code via vectors related to migrating ports.
CVE-2013-4527
Buffer overflow in hw/timer/hpet.c in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via vectors related to the number of timers.
CVE-2013-4529
Buffer overflow in hw/pci/pcie_aer.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service and possibly execute arbitrary code via a large log_num value in a savevm image.
CVE-2013-4530
Buffer overflow in hw/ssi/pl022.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service or possibly execute arbitrary code via crafted tx_fifo_head and rx_fifo_head values in a savevm image.
CVE-2013-4531
Buffer overflow in target-arm/machine.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service and possibly execute arbitrary code via a negative value in cpreg_vmstate_array_len in a savevm image.
CVE-2013-4533
Buffer overflow in the pxa2xx_ssp_load function in hw/arm/pxa2xx.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service or possibly execute arbitrary code via a crafted s->rx_level value in a savevm image.
CVE-2013-4534
Buffer overflow in hw/intc/openpic.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service or possibly execute arbitrary code via vectors related to IRQDest elements.
CVE-2013-4537
The ssi_sd_transfer function in hw/sd/ssi-sd.c in QEMU before 1.7.2 allows remote attackers to execute arbitrary code via a crafted arglen value in a savevm image.
CVE-2013-4538
Multiple buffer overflows in the ssd0323_load function in hw/display/ssd0323.c in QEMU before 1.7.2 allow remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via crafted (1) cmd_len, (2) row, or (3) col values; (4) row_start and row_end values; or (5) col_star and col_end values in a savevm image.
CVE-2013-4539
Multiple buffer overflows in the tsc210x_load function in hw/input/tsc210x.c in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via a crafted (1) precision, (2) nextprecision, (3) function, or (4) nextfunction value in a savevm image.
CVE-2013-4540
Buffer overflow in scoop_gpio_handler_update in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via a large (1) prev_level, (2) gpio_level, or (3) gpio_dir value in a savevm image.
CVE-2013-4541
The usb_device_post_load function in hw/usb/bus.c in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via a crafted savevm image, related to a negative setup_len or setup_index value.
CVE-2013-4542
The virtio_scsi_load_request function in hw/scsi/scsi-bus.c in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via a crafted savevm image, which triggers an out-of-bounds array access.
CVE-2013-6399
Array index error in the virtio_load function in hw/virtio/virtio.c in QEMU before 1.7.2 allows remote attackers to execute arbitrary code via a crafted savevm image.
CVE-2013-7057
Cross-site request forgery (CSRF) vulnerability in Axway SecureTransport 5.1 SP2 and earlier allows remote attackers to hijack the authentication of unspecified users for requests that upload arbitrary files via a crafted request to api/v1.0/files/.
CVE-2014-0182
Heap-based buffer overflow in the virtio_load function in hw/virtio/virtio.c in QEMU before 1.7.2 might allow remote attackers to execute arbitrary code via a crafted config length in a savevm image.
CVE-2014-0222
Integer overflow in the qcow_open function in block/qcow.c in QEMU before 1.7.2 allows remote attackers to cause a denial of service (crash) via a large L2 table in a QCOW version 1 image.
CVE-2014-0223
Integer overflow in the qcow_open function in block/qcow.c in QEMU before 1.7.2 allows local users to cause a denial of service (crash) and possibly execute arbitrary code via a large image size, which triggers a buffer overflow or out-of-bounds read.
CVE-2014-2718
ASUS RT-AC68U, RT-AC66R, RT-AC66U, RT-AC56R, RT-AC56U, RT-N66R, RT-N66U, RT-N56R, RT-N56U, and possibly other RT-series routers before firmware 3.0.0.4.376.x do not verify the integrity of firmware (1) update information or (2) downloaded updates, which allows man-in-the-middle (MITM) attackers to execute arbitrary code via a crafted image.
CVE-2014-3461
hw/usb/bus.c in QEMU 1.6.2 allows remote attackers to execute arbitrary code via crafted savevm data, which triggers a heap-based buffer overflow, related to "USB post load checks."
CVE-2014-3660
parser.c in libxml2 before 2.9.2 does not properly prevent entity expansion even when entity substitution has been disabled, which allows context-dependent attackers to cause a denial of service (CPU consumption) via a crafted XML document containing a large number of nested entity references, a variant of the "billion laughs" attack.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-4311
Epicor Enterprise 7.4 before FS74SP6_HotfixTL054181 allows attackers to obtain the (1) Database Connection and (2) E-mail Connection passwords by reading HTML source code of the database connection and email settings page.
CVE-2014-4974
The ESET Personal Firewall NDIS filter (EpFwNdis.sys) kernel mode driver, aka Personal Firewall module before Build 1212 (20140609), as used in multiple ESET products 5.0 through 7.0, allows local users to obtain sensitive information from kernel memory via crafted IOCTL calls.
CVE-2014-5387
Multiple SQL injection vulnerabilities in EllisLab ExpressionEngine before 2.9.1 allow remote authenticated users to execute arbitrary SQL commands via the (1) column_filter or (2) category[] parameter to system/index.php or the (3) tbl_sort[0][] parameter in the comment module to system/index.php.
CVE-2014-6130
The IBM Notes Traveler application before 9.0.1.3 for Android lacks a warning message during selection of an HTTP session, which makes it easier for remote attackers to obtain sensitive information by sniffing the network during a session in which the user had intended to use HTTPS.
CVE-2014-7176
SQL injection vulnerability in Enalean Tuleap before 7.5.99.4 allows remote authenticated users to execute arbitrary SQL commands via the lobal_txt parameter to plugins/docman.
CVE-2014-7875
Unspecified vulnerability on the HP LaserJet CM3530 Multifunction Printer CC519A and CC520A with firmware before 53.236.2 allows remote attackers to obtain sensitive information, modify data, or cause a denial of service via unknown vectors.
CVE-2014-8339
SQL injection vulnerability in midroll.php in Nuevolab Nuevoplayer for ClipShare 8.0 and earlier allows remote attackers to execute arbitrary SQL commands via the ch parameter.
CVE-2014-8471
CA Cloud Service Management (CSM) before Summer 2014 allows remote attackers to conduct replay attacks via unspecified vectors.
CVE-2014-8472
CA Cloud Service Management (CSM) before Summer 2014 does not properly verify authentication tokens from an Identity Provider, which allows user-assisted remote attackers to bypass intended access restrictions via unspecified vectors.
CVE-2014-8473
Cross-site request forgery (CSRF) vulnerability in CA Cloud Service Management (CSM) before Summer 2014 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2014-8474
CA Cloud Service Management (CSM) before Summer 2014 allows remote attackers to read arbitrary files, send HTTP requests to intranet servers, or cause a denial of service (CPU and memory consumption) via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-8584
Cross-site scripting (XSS) vulnerability in the Web Dorado Spider Video Player (aka WordPress Video Player) plugin before 1.5.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-8585
Directory traversal vulnerability in the WordPress Download Manager plugin for WordPress allows remote attackers to read arbitrary files via a .. (dot dot) in the fname parameter to (1) views/file_download.php or (2) file_download.php.
CVE-2014-8586
SQL injection vulnerability in the CP Multi View Event Calendar plugin 1.01 for WordPress allows remote attackers to execute arbitrary SQL commands via the calid parameter.
CVE-2014-8587
SAPCRYPTOLIB before 5.555.38, SAPSECULIB, and CommonCryptoLib before 8.4.30, as used in SAP NetWeaver AS for ABAP and SAP HANA, allows remote attackers to spoof Digital Signature Algorithm (DSA) signatures via unspecified vectors.
CVE-2014-8588
SQL injection vulnerability in metadata.xsjs in SAP HANA 1.00.60.379371 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-8589
Integer overflow in SAP Network Interface Router (SAProuter) 40.4 allows remote attackers to cause a denial of service (resource consumption) via crafted requests.
CVE-2014-8590
XML external entity (XXE) vulnerability in the Web Service Navigator in SAP NetWeaver Application Server (AS) Java allows remote attackers to access arbitrary files via a crafted request.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-8591
Unspecified vulnerability in SAP Internet Communication Manager (ICM), as used in SAP NetWeaver 7.02 and 7.3, allows remote attackers to cause a denial of service (process termination) via unknown vectors.
CVE-2014-8592
Unspecified vulnerability in SAP Host Agent, as used in SAP NetWeaver 7.02 and 7.3, allows remote attackers to cause a denial of service (process termination) via a crafted request.
CVE-2014-8593
Multiple cross-site scripting (XSS) vulnerabilities in Allomani Weblinks 1.0 allow remote attackers to inject arbitrary web script or HTML via the (1) default URI to admin.php or the (2) id parameter to admin.php or (3) go.php.
