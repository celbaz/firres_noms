CVE-2010-5289
Buffer overflow in the Authenticate method in the INCREDISPOOLERLib.Pop ActiveX control in ImSpoolU.dll in IncrediMail 2.0 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a long string in the first argument.
CVE-2012-6584
Multiple SQL injection vulnerabilities in MYRE Realty Manager allow remote attackers to execute arbitrary SQL commands via the bathrooms1 parameter to (1) demo2/search.php or (2) search.php.
CVE-2012-6585
Cross-site scripting (XSS) vulnerability in search.php in MYRE Realty Manager allows remote attackers to inject arbitrary web script or HTML via the cat_id1 parameter.
CVE-2012-6586
Multiple SQL injection vulnerabilities in MYRE Vacation Rental Software allow remote attackers to execute arbitrary SQL commands via the (1) garage1 or (2) bathrooms1 parameter to vacation/1_mobile/search.php, or (3) unspecified input to vacation/widgate/request_more_information.php.
CVE-2012-6587
Cross-site scripting (XSS) vulnerability in vacation/1_mobile/alert_members.php in MYRE Vacation Rental Software allows remote attackers to inject arbitrary web script or HTML via the link_idd parameter in a login action.
CVE-2012-6588
SQL injection vulnerability in links.php in MYRE Business Directory allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2012-6589
Cross-site scripting (XSS) vulnerability in search.php in MYRE Business Directory allows remote attackers to inject arbitrary web script or HTML via the look parameter.
CVE-2013-3387
Cisco Prime Central for Hosted Collaboration Solution (HCS) Assurance 8.6 and 9.x before 9.2(1) allows remote attackers to cause a denial of service (disk consumption) via a flood of TCP packets to port 5400, leading to large error-log files, aka Bug ID CSCua42724.
CVE-2013-3388
Cisco Prime Central for Hosted Collaboration Solution (HCS) Assurance 8.6 and 9.x before 9.2(1) allows remote attackers to cause a denial of service (memory consumption) via a flood of TCP packets to port 44444, aka Bug ID CSCtz92776.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20130821-hcm

"Vulnerable Products
The following products are affected by the vulnerabilities that are described in this advisory:

    Cisco Prime Central for HCS Assurance 8.6
    Cisco Prime Central for HCS Assurance 9.0
    Cisco Prime Central for HCS Assurance 9.1"
CVE-2013-3389
Cisco Prime Central for Hosted Collaboration Solution (HCS) Assurance 8.6 and 9.x before 9.2(1) allows remote attackers to cause a denial of service (memory consumption) via a flood of TCP packets to port (1) 61615 or (2) 61616, aka Bug ID CSCtz90114.
CVE-2013-3390
Memory leak in Cisco Prime Central for Hosted Collaboration Solution (HCS) Assurance 8.6 and 9.x before 9.2(1) allows remote attackers to cause a denial of service (memory consumption) via a flood of TCP packets, aka Bug ID CSCub59158.
CVE-2013-3459
Cisco Unified Communications Manager (Unified CM) 7.1(x) before 7.1(5b)su6a does not properly handle errors, which allows remote attackers to cause a denial of service (service disruption) via malformed registration messages, aka Bug ID CSCuf93466.
CVE-2013-3460
Memory leak in Cisco Unified Communications Manager (Unified CM) 8.5(x) before 8.5(1)su6, 8.6(x) before 8.6(2a)su3, and 9.x before 9.1(1) allows remote attackers to cause a denial of service (service disruption) via a high rate of UDP packets, aka Bug ID CSCub85597.
CVE-2013-3461
Cisco Unified Communications Manager (Unified CM) 8.5(x) and 8.6(x) before 8.6(2a)su3 and 9.x before 9.1(1) does not properly restrict the rate of SIP packets, which allows remote attackers to cause a denial of service (memory and CPU consumption, and service disruption) via a flood of UDP packets to port 5060, aka Bug ID CSCub35869.
CVE-2013-3462
Buffer overflow in Cisco Unified Communications Manager (Unified CM) 7.1(x) before 7.1(5b)su6, 8.5(x) before 8.5(1)su6, 8.6(x) before 8.6(2a)su3, and 9.x before 9.1(2) allows remote authenticated users to execute arbitrary code via unspecified vectors, aka Bug ID CSCud54358.
CVE-2013-4205
Memory leak in the unshare_userns function in kernel/user_namespace.c in the Linux kernel before 3.10.6 allows local users to cause a denial of service (memory consumption) via an invalid CLONE_NEWUSER unshare call.
CVE-2013-4216
The Trace_OpenLogFile function in InfraStack/OSDependent/Linux/InfraStackModules/TraceModule/TraceModule.c in the Trace module in the Intel WiMAX Network Service through 1.5.2 for Intel Wireless WiMAX Connection 2400 devices uses world-writable permissions for wimaxd.log, which allows local users to cause a denial of service (data corruption) by modifying this file.
CVE-2013-4217
The OSAL_Crypt_SetEncryptedPassword function in InfraStack/OSDependent/Linux/OSAL/Services/wimax_osal_crypt_services.c in the OSAL crypt module in the Intel WiMAX Network Service through 1.5.2 for Intel Wireless WiMAX Connection 2400 devices logs a cleartext password during certain attempts to set a password, which allows local users to obtain sensitive information by reading a log file.
CVE-2013-4218
The InitMethodAndPassword function in InfraStack/OSAgnostic/WiMax/Agents/Supplicant/Source/SupplicantAgent.c in the Intel WiMAX Network Service through 1.5.2 for Intel Wireless WiMAX Connection 2400 devices uses the same RSA private key in supplicant_key.pem on all systems, which allows local users to obtain sensitive information via unspecified decryption operations.
CVE-2013-4219
Multiple integer overflows in the Intel WiMAX Network Service through 1.5.2 for Intel Wireless WiMAX Connection 2400 devices allow remote attackers to cause a denial of service (component crash) or possibly execute arbitrary code via an L5 connection with a crafted PDU value that triggers a heap-based buffer overflow within (1) L5SocketsDispatcher.c or (2) L5Connector.c.
CVE-2013-4220
The bad_mode function in arch/arm64/kernel/traps.c in the Linux kernel before 3.9.5 on the ARM64 platform allows local users to cause a denial of service (system crash) via vectors involving an attempted register access that triggers an unexpected value in the Exception Syndrome Register (ESR).
CVE-2013-4247
Off-by-one error in the build_unc_path_to_root function in fs/cifs/connect.c in the Linux kernel before 3.9.6 allows remote attackers to cause a denial of service (memory corruption and system crash) via a DFS share mount operation that triggers use of an unexpected DFS referral name length.
CVE-2013-4254
The validate_event function in arch/arm/kernel/perf_event.c in the Linux kernel before 3.10.8 on the ARM platform allows local users to gain privileges or cause a denial of service (NULL pointer dereference and system crash) by adding a hardware event to an event group led by a software event.
CVE-2013-5578
Buffer overflow in the ToDot method in the WINGRAPHVIZLib.NEATO ActiveX control in WinGraphviz.dll in StarUML allows remote attackers to execute arbitrary code via a long argument.
