CVE-2015-2005
IBM Security QRadar SIEM 7.1.x before 7.1 MR2 Patch 12 and 7.2.x before 7.2.5 Patch 6 does not properly expire sessions, which allows remote attackers to obtain sensitive information by leveraging an unattended workstation.
CVE-2015-2008
IBM Security QRadar SIEM 7.1.x before 7.1 MR2 Patch 12 and 7.2.x before 7.2.6 includes SSH private keys during backup operations, which allows remote authenticated administrators to obtain sensitive information by reading a backup archive.
CVE-2015-3197
ssl/s2_srvr.c in OpenSSL 1.0.1 before 1.0.1r and 1.0.2 before 1.0.2f does not prevent use of disabled ciphers, which makes it easier for man-in-the-middle attackers to defeat cryptographic protection mechanisms by performing computations on SSLv2 traffic, related to the get_client_master_key and get_client_hello functions.
CVE-2015-4956
The Web UI in IBM Security QRadar SIEM 7.1.x before 7.1 MR2 Patch 12 allows remote authenticated users to execute unspecified OS commands via unknown vectors.
CVE-2015-4957
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Security QRadar SIEM 7.1.x before 7.1 MR2 Patch 12 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-4991
IBM SPSS Modeler 14.2 through FP3 IF027, 15 through FP3 IF015, 16 through FP2 IF012, 17 through FP1 IF018, and 17.1 through IF008 includes unspecified cleartext data in memory dumps, which allows local users to obtain sensitive information by reading a dump file.
CVE-2015-5010
IBM Security Access Manager for Web 7.0 before 7.0.0 IF21, 8.0 before 8.0.1.3 IF4, and 9.0 before 9.0.0.1 IF1 does not have a lockout mechanism for invalid login attempts, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2015-5012
The SSH implementation on IBM Security Access Manager for Web appliances 7.0 before 7.0.0 FP19, 8.0 before 8.0.1.3 IF3, and 9.0 before 9.0.0.0 IF1 does not properly restrict the set of MAC algorithms, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via unspecified vectors.
CVE-2015-5042
IBM Emptoris Contract Management 9.5.0.x before 9.5.0.6 iFix15, 10.0.0.x and 10.0.1.x before 10.0.1.5 iFix5, 10.0.2.x before 10.0.2.7 iFix4, and 10.0.4.x before 10.0.4.0 iFix3 allows remote attackers to execute arbitrary code by including a crafted Flash file.
CVE-2015-5050
Cross-site request forgery (CSRF) vulnerability in IBM Emptoris Contract Management 9.5.0.x before 9.5.0.6 iFix15, 10.0.0.x and 10.0.1.x before 10.0.1.5 iFix5, 10.0.2.x before 10.0.2.7 iFix4, and 10.0.4.x before 10.0.4.0 iFix3 allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2015-7398
Cross-site scripting (XSS) vulnerability in IBM Emptoris Contract Management 9.5.0.x before 9.5.0.6 iFix15, 10.0.0.x and 10.0.1.x before 10.0.1.5 iFix5, 10.0.2.x before 10.0.2.7 iFix4, and 10.0.4.x before 10.0.4.0 iFix3 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-7408
The server in IBM Spectrum Protect (aka Tivoli Storage Manager) 5.5 and 6.x before 6.3.5.1 and 7.x before 7.1.4 does not properly restrict use of the ASNODENAME option, which allows remote attackers to read or write to backup data by leveraging proxy authority.
CVE-2015-7444
The Update Installer in IBM WebSphere Commerce Enterprise 7.0.0.8 and 7.0.0.9 does not properly replicate the search index, which allows attackers to obtain sensitive information via unspecified vectors.
CVE-2015-7472
IBM WebSphere Portal 6.1.0 through 6.1.0.6 CF27, 6.1.5 through 6.1.5.3 CF27, 7.0.0 through 7.0.0.2 CF29, 8.0.0 before 8.0.0.1 CF20, and 8.5.0 before CF10 allows remote attackers to conduct LDAP injection attacks, and consequently read or write to repository data, via unspecified vectors.
CWE-90: Improper Neutralization of Special Elements used in an LDAP Query ('LDAP Injection') - https://cwe.mitre.org/data/definitions/90.html
CVE-2015-7492
Cross-site scripting (XSS) vulnerability in Reference Data Management (RDM) in IBM InfoSphere Master Data Management 10.1, 11.0 before FP5, 11.3, 11.4, and 11.5 before FP1 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-8531
Cross-site scripting (XSS) vulnerability in IBM Security Access Manager for Web 8.0 before 8.0.1.3 IF4 and 9.0 before 9.0.0.1 IF1 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-8795
Multiple cross-site scripting (XSS) vulnerabilities in the Admin UI in Apache Solr before 5.1 allow remote attackers to inject arbitrary web script or HTML via crafted fields that are mishandled during the rendering of the (1) Analysis page, related to webapp/web/js/scripts/analysis.js or (2) Schema-Browser page, related to webapp/web/js/scripts/schema-browser.js.
CVE-2015-8796
Cross-site scripting (XSS) vulnerability in webapp/web/js/scripts/schema-browser.js in the Admin UI in Apache Solr before 5.3 allows remote attackers to inject arbitrary web script or HTML via a crafted schema-browse URL.
CVE-2015-8797
Cross-site scripting (XSS) vulnerability in webapp/web/js/scripts/plugins.js in the stats page in the Admin UI in Apache Solr before 5.3.1 allows remote attackers to inject arbitrary web script or HTML via the entry parameter to a plugins/cache URI.
CVE-2016-0231
IBM Financial Transaction Manager (FTM) for ACH Services, Check Services and Corporate Payment Services (CPS) 3.0.0 before FP12 allows remote authenticated users to obtain sensitive information by reading exception details in error logs.
CVE-2016-0232
IBM Financial Transaction Manager (FTM) for ACH Services, Check Services and Corporate Payment Services (CPS) 3.0.0 before FP12 allows remote authenticated users to obtain sensitive information by reading README files.
CVE-2016-0701
The DH_check_pub_key function in crypto/dh/dh_check.c in OpenSSL 1.0.2 before 1.0.2f does not ensure that prime numbers are appropriate for Diffie-Hellman (DH) key exchange, which makes it easier for remote attackers to discover a private DH exponent by making multiple handshakes with a peer that chose an inappropriate number, as demonstrated by a number in an X9.42 file.
CVE-2016-0742
The resolver in nginx before 1.8.1 and 1.9.x before 1.9.10 allows remote attackers to cause a denial of service (invalid pointer dereference and worker process crash) via a crafted UDP DNS response.
<a href="https://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-0746
Use-after-free vulnerability in the resolver in nginx 0.6.18 through 1.8.0 and 1.9.x before 1.9.10 allows remote attackers to cause a denial of service (worker process crash) or possibly have unspecified other impact via a crafted DNS response related to CNAME response processing.
<a href="http://cwe.mitre.org/data/definitions/416.html" rel="nofollow">CWE-416: Use After Free</a>
CVE-2016-0747
The resolver in nginx before 1.8.1 and 1.9.x before 1.9.10 does not properly limit CNAME resolution, which allows remote attackers to cause a denial of service (worker process resource consumption) via vectors related to arbitrary name resolution.
CVE-2016-1321
Cisco Universal Small Cell devices with firmware R2.12 through R3.5 contain an image-decryption key in flash memory, which allows remote attackers to bypass a certain certificate-validation feature and obtain sensitive firmware-image and IP address data via a request to an unspecified Cisco server, aka Bug ID CSCut98082.
CVE-2016-1330
Cisco IOS 15.2(4)E on Industrial Ethernet 2000 devices allows remote attackers to cause a denial of service (device reload) via crafted Cisco Discovery Protocol (CDP) packets, aka Bug ID CSCuy27746.
CVE-2016-1331
Multiple cross-site scripting (XSS) vulnerabilities in Cisco Emergency Responder 11.5(0.99833.5) allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCuy10766.
CVE-2016-2231
The Windows-based Host Interface Program (WHIP) service on Huawei SmartAX MT882 devices V200R002B022 Arg relies on the client to send a length field that is consistent with a buffer size, which allows remote attackers to cause a denial of service (device outage) or possibly have unspecified other impact via crafted traffic on TCP port 8701.
CVE-2016-2314
GlobespanVirata ftpd 1.0, as used on Huawei SmartAX MT882 devices V200R002B022 Arg, allows remote authenticated users to cause a denial of service (device outage) by using the FTP MKD command to create a directory with a long name, and then using certain other commands.
