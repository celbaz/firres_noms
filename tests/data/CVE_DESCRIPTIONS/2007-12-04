CVE-2007-6206
The do_coredump function in fs/exec.c in Linux kernel 2.4.x and 2.6.x up to 2.6.24-rc3, and possibly other versions, does not change the UID of a core dump file if it exists before a root process creates a core dump in the same location, which might allow local users to obtain sensitive information.
CVE-2007-6207
Xen 3.x, possibly before 3.1.2, when running on IA64 systems, does not check the RID value for mov_to_rr, which allows a VTi domain to read memory of other domains.
CVE-2007-6208
sylprint.pl in claws mail tools (claws-mail-tools) allows local users to overwrite arbitrary files via a symlink attack on the sylprint.[USER].[PID] temporary file.
CVE-2007-6209
Util/difflog.pl in zsh 4.3.4 allows local users to overwrite arbitrary files via a symlink attack on temporary files.
CVE-2007-6210
zabbix_agentd 1.1.4 in ZABBIX before 1.4.3 runs "UserParameter" scripts with gid 0, which might allow local users to gain privileges.
CVE-2007-6211
Send ICMP Nasty Garbage (sing) on Debian GNU/Linux allows local users to append to arbitrary files and gain privileges via the -L (output log file) option.  NOTE: this issue is only a vulnerability in limited environments, since sing is not installed setuid, and the administrator would need to override a non-setuid default during installation.
CVE-2007-6212
Directory traversal vulnerability in region.php in KML share 1.1 allows remote attackers to read arbitrary files via a .. (dot dot) in the layer parameter.
CVE-2007-6213
Multiple directory traversal vulnerabilities in mod/chat/index.php in WebED 0.0.9 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) Root and (2) Path parameters.
CVE-2007-6214
Directory traversal vulnerability in include/file_download.php in LearnLoop 2.0 beta7 allows remote attackers to read arbitrary files via a .. (dot dot) in the sFilePath parameter.  NOTE: exploitation requires that the product is configured, but has zero files in the database.
CVE-2007-6215
Multiple directory traversal vulnerabilities in play.php in Web-MeetMe 3.0.3 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) roomNo and possibly the (2) bookid parameter.
CVE-2007-6216
Race condition in the Fibre Channel protocol (fcp) driver and Devices filesystem (devfs) in Sun Solaris 10 allows local users to cause a denial of service (system hang) via some programs that access hardware resources, as demonstrated by the (1) cfgadm and (2) format programs.
CVE-2007-6217
Multiple SQL injection vulnerabilities in login.asp in Irola My-Time (aka Timesheet) 3.5 allow remote attackers to execute arbitrary SQL commands via the (1) login (aka Username) and (2) password parameters. NOTE: some of these details are obtained from third party information.
CVE-2007-6218
Multiple PHP remote file inclusion vulnerabilities in Ossigeno CMS 2.2 pre1 allow remote attackers to execute arbitrary PHP code via a URL in the (1) level parameter to (a) install_module.php and (b) uninstall_module.php in upload/xax/admin/modules/, (c) upload/xax/admin/patch/index.php, and (d) install_module.php and (e) uninstall_module.php in upload/xax/ossigeno/admin/; and the (2) ossigeno parameter to (f) ossigeno_modules/ossigeno-catalogo/xax/ossigeno/catalogo/common.php, different vectors than CVE-2007-5234.
CVE-2007-6219
Cross-site scripting (XSS) vulnerability in IBM Tivoli Netcool Security Manager 1.3.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-6220
typespeed before 0.6.4 allows remote attackers to cause a denial of service (application crash) via unspecified network behavior that triggers a divide-by-zero error.
CVE-2007-6221
TuMusika Evolution 1.7R5 allows remote attackers to obtain configuration information via a direct request to phpinfo.php, which calls the phpinfo function.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-6222
The CheckCustomerAccess function in functions.php in CRM-CTT Interleave before 4.2.0 (formerly CRM-CTT) does not properly verify user privileges, which allows remote authenticated users with the LIMITTOCUSTOMERS privilege to bypass intended access restrictions and edit non-active user settings.  NOTE: some of these details are obtained from third party information.
CVE-2007-6223
SQL injection vulnerability in garage.php in phpBB Garage 1.2.0 Beta3 allows remote attackers to execute arbitrary SQL commands via the make_id parameter in a search action in browse mode.
CVE-2007-6224
The RealNetworks RealAudioObjects.RealAudio ActiveX control in rmoc3260.dll, as shipped with RealPlayer 11, allows remote attackers to cause a denial of service (browser crash) via a certain argument to the GetSourceTransport method.
CVE-2007-6225
Unspecified vulnerability in Sun Solaris 10, when 64bit mode is used on the x86 platform, allows local users in a Linux (lx) branded zone to cause a denial of service (panic) via unspecified vectors.
CVE-2007-6226
The American Power Conversion (APC) AP7932 0u 30amp Switched Rack Power Distribution Unit (PDU), with rpdu 3.5.5 and aos 3.5.6, allows remote attackers to bypass authentication and obtain login access by making a login attempt while a different client is logged in, and then resubmitting the login attempt once the other client exits.
CVE-2007-6227
QEMU 0.9.0 allows local users of a Windows XP SP2 guest operating system to overwrite the TranslationBlock (code_gen_buffer) buffer, and probably have unspecified other impacts related to an "overflow," via certain Windows executable programs, as demonstrated by qemu-dos.com.
CVE-2007-6228
Stack-based buffer overflow in the Helper class in the yt.ythelper.2 ActiveX control in Yahoo! Toolbar 1.4.1 allows remote attackers to cause a denial of service (browser crash) via a long argument to the c method.
CVE-2007-6229
PHP remote file inclusion vulnerability in common/classes/class_HeaderHandler.lib.php in Rayzz Script 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the CFG[site][project_path] parameter.
CVE-2007-6230
Directory traversal vulnerability in common/classes/class_HeaderHandler.lib.php in Rayzz Script 2.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the CFG[site][project_path] parameter.
CVE-2007-6231
Multiple PHP remote file inclusion vulnerabilities in tellmatic 1.0.7 allow remote attackers to execute arbitrary PHP code via a URL in the tm_includepath parameter to (1) Classes.inc.php, (2) statistic.inc.php, (3) status.inc.php, (4) status_top_x.inc.php, or (5) libchart-1.1/libchart.php in include/.  NOTE: access to include/ is blocked by .htaccess in most deployments that use Apache HTTP Server.
CVE-2007-6232
Cross-site scripting (XSS) vulnerability in index.php in FTP Admin 0.1.0 allows remote attackers to inject arbitrary web script or HTML via the error parameter in an error page action.
CVE-2007-6233
Directory traversal vulnerability in index.php in FTP Admin 0.1.0 allows remote authenticated users to include and execute arbitrary local files via a .. (dot dot) in the page parameter.  NOTE: in some environments, this can be leveraged for remote file inclusion by using a UNC share pathname or an ftp, ftps, or ssh2.sftp URL.
CVE-2007-6234
index.php in FTP Admin 0.1.0 allows remote attackers to bypass authentication and obtain administrative access via a loggedin parameter with a value of true, as demonstrated by adding a user account.
CVE-2007-6235
A certain ActiveX control in RealNetworks RealPlayer 11 allows remote attackers to cause a denial of service (application crash) via a malformed .au file that triggers a divide-by-zero error.  NOTE: this might be related to CVE-2007-4904.
CVE-2007-6236
Microsoft Windows Media Player (WMP) allows remote attackers to cause a denial of service (application crash) via a certain AIFF file that triggers a divide-by-zero error, as demonstrated by kr.aiff.
CVE-2007-6237
cp.php in DeluxeBB 1.09 does not verify that the membercookie parameter corresponds to the authenticated member during a profile update, which allows remote authenticated users to change the e-mail addresses of arbitrary accounts via a modified membercookie parameter, a different vector than CVE-2006-4078.  NOTE: this can be leveraged for administrative access by requesting password-reset e-mail through a lostpw action to misc.php.
CVE-2007-6238
Unspecified vulnerability in Apple QuickTime 7.2 on Windows XP allows remote attackers to execute arbitrary code via unknown attack vectors, probably a different vulnerability than CVE-2007-6166.  NOTE: this information is based upon a vague advisory by a vulnerability information sales organization that does not coordinate with vendors or release advisories with actionable information.  A CVE has been assigned for tracking purposes, but duplicates with other CVEs are difficult to determine.  However, the organization has stated that this is different than CVE-2007-6166.
CVE-2007-6239
The "cache update reply processing" functionality in Squid 2.x before 2.6.STABLE17 and Squid 3.0 allows remote attackers to cause a denial of service (crash) via unknown vectors related to HTTP headers and an Array memory leak during requests for cached objects.
