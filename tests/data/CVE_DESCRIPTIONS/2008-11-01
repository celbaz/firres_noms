CVE-2008-4863
Untrusted search path vulnerability in BPY_interface in Blender 2.46 allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to an erroneous setting of sys.path by the PySys_SetArgv function.
CVE-2008-4864
Multiple integer overflows in imageop.c in the imageop module in Python 1.5.2 through 2.5.1 allow context-dependent attackers to break out of the Python VM and execute arbitrary code via large integer values in certain arguments to the crop function, leading to a buffer overflow, a different vulnerability than CVE-2007-4965 and CVE-2008-1679.
CVE-2008-4865
Untrusted search path vulnerability in valgrind before 3.4.0 allows local users to execute arbitrary programs via a Trojan horse .valgrindrc file in the current working directory, as demonstrated using a malicious --db-command options.  NOTE: the severity of this issue has been disputed, but CVE is including this issue because execution of a program from an untrusted directory is a common scenario.
CVE-2008-4866
Multiple buffer overflows in libavformat/utils.c in FFmpeg 0.4.9 before r14715, as used by MPlayer, allow context-dependent attackers to have an unknown impact via vectors related to execution of DTS generation code with a delay greater than MAX_REORDER_DELAY.
CVE-2008-4867
Buffer overflow in libavcodec/dca.c in FFmpeg 0.4.9 before r14917, as used by MPlayer, allows context-dependent attackers to have an unknown impact via vectors related to an incorrect DCA_MAX_FRAME_SIZE value.
CVE-2008-4868
Unspecified vulnerability in the avcodec_close function in libavcodec/utils.c in FFmpeg 0.4.9 before r14787, as used by MPlayer, has unknown impact and attack vectors, related to a free "on random pointers."
CVE-2008-4869
FFmpeg 0.4.9, as used by MPlayer, allows context-dependent attackers to cause a denial of service (memory consumption) via unknown vectors, aka a "Tcp/udp memory leak."
CVE-2008-4870
dovecot 1.0.7 in Red Hat Enterprise Linux (RHEL) 5, and possibly Fedora, uses world-readable permissions for dovecot.conf, which allows local users to obtain the ssl_key_password parameter value.
CVE-2008-4871
Cross-site scripting (XSS) vulnerability in My Little Forum 1.75 and 2.0 Beta 23 allows remote attackers to inject arbitrary web script or HTML via BBcode IMG tags.
CVE-2008-4872
Cross-site scripting (XSS) vulnerability in bidhistory.php in iTechBids Gold 5.0 allows remote attackers to inject arbitrary web script or HTML via the item_id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4873
board.cgi in Sepal SPBOARD 4.5 allows remote attackers to execute arbitrary commands via shell metacharacters in the file parameter during a down_file action.
CVE-2008-4874
The web component in Philips Electronics VOIP841 DECT Phone with firmware 1.0.4.50 and 1.0.4.80 has a back door "service" account with "service" as its password, which makes it easier for remote attackers to obtain access.
CVE-2008-4875
Directory traversal vulnerability in the web server in Philips Electronics VOIP841 DECT Phone with firmware 1.0.4.50 and 1.0.4.80 allows remote authenticated users to read arbitrary files via a .. (dot dot) in a GET request.  NOTE: this can be leveraged with CVE-2008-4874 for unauthenticated access to sensitive files such as (1) save.dat and (2) apply.log, which can contain other credentials such as the Skype username and password.
CVE-2008-4876
Cross-site scripting (XSS) vulnerability in the web server component in Philips Electronics VOIP841 DECT Phone with firmware 1.0.4.50 and 1.0.4.80 allows remote attackers to inject arbitrary web script or HTML via the request URL, which is not properly handled in a 404 web error page.
CVE-2008-4877
SQL injection vulnerability in admin.php in WebCards 1.3, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the user parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-4878
Unrestricted file upload vulnerability in the "Add Image Macro" feature in WebCards 1.3 allows remote authenticated administrators to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the uploaded file.
