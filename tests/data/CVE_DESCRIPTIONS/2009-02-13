CVE-2008-6124
SQL injection vulnerability in the hotpot_delete_selected_attempts function in report.php in the HotPot module in Moodle 1.6 before 1.6.7, 1.7 before 1.7.5, 1.8 before 1.8.6, and 1.9 before 1.9.2 allows remote attackers to execute arbitrary SQL commands via a crafted selected attempt.
CVE-2008-6125
Unspecified vulnerability in the user editing interface in Moodle 1.5.x, 1.6 before 1.6.6, and 1.7 before 1.7.3 allows remote authenticated users to gain privileges via unknown vectors.
CVE-2008-6126
Multiple directory traversal vulnerabilities in moziloCMS 1.10.2 and earlier allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) file parameter to download.php and the (2) page parameter to index.php, a different vector than CVE-2008-3589.
CVE-2008-6127
Multiple cross-site scripting (XSS) vulnerabilities in moziloCMS 1.10.2 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) page and (2) query parameters to (a) index.php, (3) cat and (4) file parameters to (b) download.php, (5) gal parameter to gallery.php, and the (6) URL to admin/login.php.
CVE-2008-6128
Session fixation vulnerability in moziloCMS 1.10.2 and earlier allows remote attackers to hijack web sessions by setting the PHPSESSID parameter.
CVE-2008-6129
Directory traversal vulnerability in print.php in moziloWiki 1.0.1 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the page parameter.
CVE-2008-6130
Cross-site scripting (XSS) vulnerability in index.php in moziloWiki 1.0.1 and earlier allows remote attackers to inject arbitrary web script or HTML via the (1) action and (2) page parameters.
CVE-2008-6131
Session fixation vulnerability in moziloWiki 1.0.1 and earlier allows remote attackers to hijack web sessions by setting the PHPSESSID parameter.
CVE-2008-6132
Eval injection vulnerability in reserve.php in phpScheduleIt 1.2.10 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary PHP code via the start_date parameter.
CVE-2008-6133
SQL injection vulnerability in arsaprint.php in Full PHP Emlak Script allows remote attackers to execute arbitrary SQL commands via the id parameter, a different vector than CVE-2008-3942.
CVE-2009-0009
Unspecified vulnerability in the Pixlet codec in Apple Mac OS X 10.4.11 and 10.5.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a crafted movie file that triggers memory corruption.
CVE-2009-0011
Certificate Assistant in Apple Mac OS X 10.5.6 allows local users to overwrite arbitrary files via unknown vectors related to an "insecure file operation" on a temporary file.
CVE-2009-0012
Heap-based buffer overflow in CoreText in Apple Mac OS X 10.5.6 allows remote attackers to execute arbitrary code via a crafted Unicode string.
CVE-2009-0013
dscl in DS Tools in Apple Mac OS X 10.4.11 and 10.5.6 requires that passwords must be provided as command line arguments, which allows local users to gain privileges by listing process information.
CVE-2009-0014
Folder Manager in Apple Mac OS X 10.5.6 uses insecure default permissions when recreating a Downloads folder after it has been deleted, which allows local users to bypass intended access restrictions and read the Downloads folder.
CVE-2009-0015
Unspecified vulnerability in fseventsd in the FSEvents framework in Apple Mac OS X 10.5.6 allows local users to obtain sensitive information (filesystem activities and directory names) via unknown vectors related to "credential management."
CVE-2009-0017
csregprinter in the Printing component in Apple Mac OS X 10.4.11 and 10.5.6 does not properly handle error conditions, which allows local users to execute arbitrary code via unknown vectors that trigger a heap-based buffer overflow.
CVE-2009-0018
The Remote Apple Events server in Apple Mac OS X 10.4.11 and 10.5.6 does not properly initialize a buffer, which allows remote attackers to read portions of memory.
CVE-2009-0019
Remote Apple Events in Apple Mac OS X 10.4.11 and 10.5.6 allows remote attackers to cause a denial of service (application termination) or obtain sensitive information via unspecified vectors that trigger an out-of-bounds memory access.
CVE-2009-0020
Unspecified vulnerability in CarbonCore in Apple Mac OS X 10.4.11 and 10.5.6 allows remote attackers to cause a denial of service (application termination) and execute arbitrary code via a crafted resource fork that triggers memory corruption.
CVE-2009-0137
Multiple unspecified vulnerabilities in Safari RSS in Apple Mac OS X 10.4.11 and 10.5.6, and Windows XP and Vista, allow remote attackers to execute arbitrary JavaScript in the local security zone via a crafted feed: URL, related to "input validation issues."
CVE-2009-0138
servermgrd (Server Manager) in Apple Mac OS X 10.5.6 does not properly validate authentication credentials, which allows remote attackers to modify the system configuration.
CVE-2009-0139
Integer overflow in the SMB component in Apple Mac OS X 10.5.6 allows remote SMB servers to cause a denial of service (system shutdown) or execute arbitrary code via a crafted SMB file system that triggers a heap-based buffer overflow.
CVE-2009-0140
Unspecified vulnerability in the SMB component in Apple Mac OS X 10.4.11 and 10.5.6 allows remote SMB servers to cause a denial of service (memory exhaustion and system shutdown) via a crafted file system name.
CVE-2009-0141
XTerm in Apple Mac OS X 10.4.11 and 10.5.6, when used with luit, creates tty devices with insecure world-writable permissions, which allows local users to write to the Xterm of another user.
CVE-2009-0216
GE Fanuc iFIX 5.0 and earlier relies on client-side authentication involving a weakly encrypted local password file, which allows remote attackers to bypass intended access restrictions and start privileged server login sessions by recovering a password or by using a modified program module.
CVE-2009-0360
Russ Allbery pam-krb5 before 3.13, when linked against MIT Kerberos, does not properly initialize the Kerberos libraries for setuid use, which allows local users to gain privileges by pointing an environment variable to a modified Kerberos configuration file, and then launching a PAM-based setuid application.
Per vendor advisory:
 http://www.eyrie.org/~eagle/software/pam-krb5/security/2009-02-11.html

"This advisory is only for my pam-krb5 module, as distributed from my web site and packaged by Debian, Ubuntu, and Gentoo."
CVE-2009-0361
Russ Allbery pam-krb5 before 3.13, as used by libpam-heimdal, su in Solaris 10, and other software, does not properly handle calls to pam_setcred when running setuid, which allows local users to overwrite and change the ownership of arbitrary files by setting the KRB5CCNAME environment variable, and then launching a setuid application that performs certain pam_setcred operations.
CVE-2009-0362
filter.d/wuftpd.conf in Fail2ban 0.8.3 uses an incorrect regular expression that allows remote attackers to cause a denial of service (forced authentication failures) via a crafted reverse-resolved DNS name (rhost) entry that contains a substring that is interpreted as an IP address, a different vulnerability than CVE-2007-4321.
CVE-2009-0503
IBM WebSphere Message Broker 6.1.x before 6.1.0.2 writes a database connection password to the Event Log and System Log during exception handling for a JDBC error, which allows local users to obtain sensitive information by reading these logs.
CVE-2009-0569
Buffer overflow in Becky! Internet Mail 2.48.02 and earlier allows remote attackers to execute arbitrary code via a mail message with a crafted return receipt request.
CVE-2009-0570
Directory traversal vulnerability in send.php in Ninja Designs Mailist 3.0, when register_globals is enabled and magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the load parameter.  NOTE: some of these details are obtained from third party information.
CVE-2009-0571
admin.php in Ninja Designs Mailist 3.0 stores backup copies of maillist.php under the web root with insufficient access control, which allows remote attackers to obtain sensitive information via a direct request to the backup directory.
CVE-2009-0572
PHP remote file inclusion vulnerability in include/flatnux.php in FlatnuX CMS (aka Flatnuke3) 2009-01-27 and 2009-02-04, when register_globals is enabled and magic_quotes_gpc disabled, allows remote attackers to execute arbitrary PHP code via a URL in the _FNROOTPATH parameter to (1) index.php and (2) filemanager.php.
CVE-2009-0573
Multiple cross-site scripting (XSS) vulnerabilities in FotoWeb 6.0 (Build 273) allow remote attackers to inject arbitrary web script or HTML via the (1) s parameter to cmdrequest/Login.fwx and the (2) search parameter to Grid.fwx.
CVE-2009-0574
SQL injection vulnerability in index.php in Easy CafeEngine allows remote attackers to execute arbitrary SQL commands via the catid parameter, a different vector than CVE-2008-4604.
CVE-2009-0575
Cross-site scripting (XSS) vulnerability in the theme_views_bulk_operations_confirmation function in views_bulk_operations.module in Views Bulk Operations 5.x before 5.x-1.3 and 6.x before 6.x-1.4, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to node titles.  NOTE: some of these details are obtained from third party information.
CVE-2009-0576
Unspecified vulnerability in Sun Java System Directory Server 5.2 p6 and earlier, and Enterprise Edition 5, allows remote attackers to cause a denial of service (daemon crash) via crafted LDAP requests.
