CVE-2011-0284
Double free vulnerability in the prepare_error_as function in do_as_req.c in the Key Distribution Center (KDC) in MIT Kerberos 5 (aka krb5) 1.7 through 1.9, when the PKINIT feature is enabled, allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via an e_data field containing typed data.
CVE-2011-0421
The _zip_name_locate function in zip_name_locate.c in the Zip extension in PHP before 5.3.6 does not properly handle a ZIPARCHIVE::FL_UNCHANGED argument, which might allow context-dependent attackers to cause a denial of service (NULL pointer dereference) via an empty ZIP archive that is processed with a (1) locateName or (2) statName operation.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2011-0708
exif.c in the Exif extension in PHP before 5.3.6 on 64-bit platforms performs an incorrect cast, which allows remote attackers to cause a denial of service (application crash) via an image with a crafted Image File Directory (IFD) that triggers a buffer over-read.
CVE-2011-1024
chain.c in back-ldap in OpenLDAP 2.4.x before 2.4.24, when a master-slave configuration with a chain overlay and ppolicy_forward_updates (aka authentication-failure forwarding) is used, allows remote authenticated users to bypass external-program authentication by sending an invalid password to a slave server.
CVE-2011-1025
bind.cpp in back-ndb in OpenLDAP 2.4.x before 2.4.24 does not require authentication for the root Distinguished Name (DN), which allows remote attackers to bypass intended access restrictions via an arbitrary password.
CVE-2011-1027
Off-by-one error in the convert_query_hexchar function in html.c in cgit.cgi in cgit before 0.8.3.5 allows remote attackers to cause a denial of service (infinite loop) via a string composed of a % (percent) character followed by invalid hex characters, as demonstrated by a %gg sequence.
CVE-2011-1081
modrdn.c in slapd in OpenLDAP 2.4.x before 2.4.24 allows remote attackers to cause a denial of service (daemon crash) via a relative Distinguished Name (DN) modification request (aka MODRDN operation) that contains an empty value for the OldDN field.
CVE-2011-1464
Buffer overflow in the strval function in PHP before 5.3.6, when the precision configuration option has a large value, might allow context-dependent attackers to cause a denial of service (application crash) via a small numerical value in the argument.
CVE-2011-1465
The SPDY implementation in net/http/http_network_transaction.cc in Google Chrome before 11.0.696.14 drains the bodies from SPDY responses, which might allow remote SPDY servers to cause a denial of service (application exit) by canceling a stream.
CVE-2011-1466
Integer overflow in the SdnToJulian function in the Calendar extension in PHP before 5.3.6 allows context-dependent attackers to cause a denial of service (application crash) via a large integer in the first argument to the cal_from_jd function.
CVE-2011-1467
Unspecified vulnerability in the NumberFormatter::setSymbol (aka numfmt_set_symbol) function in the Intl extension in PHP before 5.3.6 allows context-dependent attackers to cause a denial of service (application crash) via an invalid argument, a related issue to CVE-2010-4409.
CVE-2011-1468
Multiple memory leaks in the OpenSSL extension in PHP before 5.3.6 might allow remote attackers to cause a denial of service (memory consumption) via (1) plaintext data to the openssl_encrypt function or (2) ciphertext data to the openssl_decrypt function.
CVE-2011-1469
Unspecified vulnerability in the Streams component in PHP before 5.3.6 allows context-dependent attackers to cause a denial of service (application crash) by accessing an ftp:// URL during use of an HTTP proxy with the FTP wrapper.
CVE-2011-1470
The Zip extension in PHP before 5.3.6 allows context-dependent attackers to cause a denial of service (application crash) via a ziparchive stream that is not properly handled by the stream_get_contents function.
CVE-2011-1471
Integer signedness error in zip_stream.c in the Zip extension in PHP before 5.3.6 allows context-dependent attackers to cause a denial of service (CPU consumption) via a malformed archive file that triggers errors in zip_fread function calls.
