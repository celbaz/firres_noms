CVE-2009-5115
McAfee Common Management Agent (CMA) 3.5.5 through 3.5.5.588 and 3.6.0 through 3.6.0.608, and McAfee Agent 4.0 before Patch 3, allows remote authenticated users to overwrite arbitrary files by accessing a report-writing ActiveX control COM object.
CVE-2009-5116
McAfee LinuxShield 1.5.1 and earlier does not properly implement client authentication, which allows remote authenticated users to obtain Admin access to the statistics server by leveraging a client account.
CVE-2009-5117
The Web Post Protection feature in McAfee Host Data Loss Prevention (DLP) 3.x before 3.0.100.10 and 9.x before 9.0.0.422, when HTTP Capture mode is enabled, allows local users to obtain sensitive information from web traffic by reading unspecified files.
CVE-2009-5118
Untrusted search path vulnerability in McAfee VirusScan Enterprise before 8.7i allows local users to gain privileges via a Trojan horse DLL in an unspecified directory, as demonstrated by scanning a document located on a remote share.
Per: https://kc.mcafee.com/corporate/index?page=content&id=SB10013

'Access Vector Remote'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426 Untrusted Search Path'

CVE-2010-3496
McAfee VirusScan Enterprise 8.5i and 8.7i does not properly interact with the processing of hcp:// URLs by the Microsoft Help and Support Center, which makes it easier for remote attackers to execute arbitrary code via malware that is correctly detected by this product, but with a detection approach that occurs too late to stop the code execution.
CVE-2010-3497
Symantec Norton AntiVirus 2011 does not properly interact with the processing of hcp:// URLs by the Microsoft Help and Support Center, which makes it easier for remote attackers to execute arbitrary code via malware that is correctly detected by this product, but with a detection approach that occurs too late to stop the code execution. NOTE: the researcher indicates that a vendor response was received, stating that this issue "falls into the work of our Firewall and not our AV (per our methodology of layers of defense)."
CVE-2010-3498
AVG Anti-Virus does not properly interact with the processing of hcp:// URLs by the Microsoft Help and Support Center, which makes it easier for remote attackers to execute arbitrary code via malware that is correctly detected by this product, but with a detection approach that occurs too late to stop the code execution.
CVE-2010-3499
F-Secure Anti-Virus does not properly interact with the processing of hcp:// URLs by the Microsoft Help and Support Center, which makes it easier for remote attackers to execute arbitrary code via malware that is correctly detected by this product, but with a detection approach that occurs too late to stop the code execution.  NOTE: the researcher indicates that a vendor response was received, stating that "the inability to catch these files are caused by lacking functionality rather than programming errors."
CVE-2010-5143
McAfee VirusScan Enterprise before 8.8 allows local users to disable the product by leveraging administrative privileges to execute an unspecified Metasploit Framework module.
CVE-2011-5100
The web interface in McAfee Firewall Reporter before 5.1.0.13 does not properly implement cookie authentication, which allows remote attackers to obtain access, and disable anti-virus functionality, via an HTTP request.
CVE-2011-5101
The Rumor technology in McAfee SaaS Endpoint Protection before 5.2.4 allows remote attackers to relay e-mail messages via unspecified vectors, as demonstrated by relaying spam.
CVE-2012-0681
Apple Remote Desktop before 3.6.1 does not recognize the "Encrypt all network data" setting during connections to third-party VNC servers, which allows remote attackers to obtain cleartext VNC session content by sniffing the network.
CVE-2012-2687
Multiple cross-site scripting (XSS) vulnerabilities in the make_variant_list function in mod_negotiation.c in the mod_negotiation module in the Apache HTTP Server 2.4.x before 2.4.3, when the MultiViews option is enabled, allow remote attackers to inject arbitrary web script or HTML via a crafted filename that is not properly handled during construction of a variant list.
Per http://httpd.apache.org/security/vulnerabilities_22.html versions 2.2.x before 2.2.23 are also vulnerable.
CVE-2012-2864
Mesa, as used in Google Chrome before 21.0.1183.0 on the Acer AC700, Cr-48, and Samsung Series 5 and 5 550 Chromebook platforms, and the Samsung Chromebox Series 3, allows remote attackers to execute arbitrary code via unspecified vectors that trigger an "array overflow."
CVE-2012-3502
The proxy functionality in (1) mod_proxy_ajp.c in the mod_proxy_ajp module and (2) mod_proxy_http.c in the mod_proxy_http module in the Apache HTTP Server 2.4.x before 2.4.3 does not properly determine the situations that require closing a back-end connection, which allows remote attackers to obtain sensitive information in opportunistic circumstances by reading a response that was intended for a different client.
CVE-2012-4580
Cross-site scripting (XSS) vulnerability in McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, allows remote attackers to inject arbitrary web script or HTML via vectors related to the McAfee Security Appliance Management Console/Dashboard.
CVE-2012-4581
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, does not disable the server-side session token upon the closing of the Management Console/Dashboard, which makes it easier for remote attackers to hijack sessions by capturing a session cookie and then modifying the response to a login attempt, related to a "Logout Failure" issue.
CVE-2012-4582
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, allows remote authenticated users to reset the passwords of arbitrary administrative accounts via unspecified vectors.
CVE-2012-4583
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, allows remote authenticated users to obtain the session tokens of arbitrary users by navigating within the Dashboard.
CVE-2012-4584
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, does not properly encrypt system-backup data, which makes it easier for remote authenticated users to obtain sensitive information by reading a backup file, as demonstrated by obtaining password hashes.
CVE-2012-4585
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, allows remote authenticated users to read arbitrary files via a crafted URL.
CVE-2012-4586
McAfee Email and Web Security (EWS) 5.x before 5.5 Patch 6 and 5.6 before Patch 3, and McAfee Email Gateway (MEG) 7.0 before Patch 1, accesses files with the privileges of the root user, which allows remote authenticated users to bypass intended permission settings by requesting a file.
CVE-2012-4587
McAfee Enterprise Mobility Manager (EMM) Agent before 4.8 and Server before 10.1, when one-time provisioning (OTP) mode is enabled, have an improper dependency on DNS SRV records, which makes it easier for remote attackers to discover user passwords by spoofing the EMM server, as demonstrated by a password entered on an iOS device.
CVE-2012-4588
McAfee Enterprise Mobility Manager (EMM) Agent before 4.8 and Server before 10.1 record all invalid usernames presented in failed login attempts, and place them on a list of accounts that an administrator may wish to unlock, which allows remote attackers to cause a denial of service (excessive list size in the EMM Database) via a long sequence of login attempts with different usernames.
CVE-2012-4589
Login.aspx in the Portal in McAfee Enterprise Mobility Manager (EMM) before 10.0 does not have an off autocomplete attribute for unspecified form fields, which makes it easier for remote attackers to obtain access by leveraging an unattended workstation.
CVE-2012-4590
Multiple cross-site scripting (XSS) vulnerabilities in About.aspx in the Portal in McAfee Enterprise Mobility Manager (EMM) before 10.0 might allow remote attackers to inject arbitrary web script or HTML via the (1) User Agent or (2) Connection variable.
CVE-2012-4591
About.aspx in the Portal in McAfee Enterprise Mobility Manager (EMM) before 10.0 discloses the name of the user account for an IIS worker process, which allows remote attackers to obtain potentially sensitive information by visiting this page.
CVE-2012-4592
The Portal in McAfee Enterprise Mobility Manager (EMM) before 10.0 does not set the secure flag for the ASP.NET session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2012-4593
McAfee Application Control and Change Control 5.1.x and 6.0.0 do not enforce an intended password requirement in certain situations involving attributes of the password file, which allows local users to bypass authentication by executing a command.
CVE-2012-4594
McAfee ePolicy Orchestrator (ePO) 4.6.1 and earlier allows remote authenticated users to bypass intended access restrictions, and obtain sensitive information from arbitrary reporting panels, via a modified ID value in a console URL.
CVE-2012-4595
McAfee Email and Web Security (EWS) 5.5 through Patch 6 and 5.6 through Patch 3, and McAfee Email Gateway (MEG) 7.0.0 and 7.0.1, allows remote attackers to bypass authentication and obtain an admin session ID via unspecified vectors.
CVE-2012-4596
Directory traversal vulnerability in McAfee Email Gateway (MEG) 7.0.0 and 7.0.1 allows remote authenticated users to bypass intended access restrictions and download arbitrary files via a crafted URL.
CVE-2012-4597
Cross-site scripting (XSS) vulnerability in McAfee Email and Web Security (EWS) 5.5 through Patch 6 and 5.6 through Patch 3, and McAfee Email Gateway (MEG) 7.0.0 and 7.0.1, allows remote attackers to inject arbitrary web script or HTML via vectors related to the McAfee Security Appliance Management Console/Dashboard.
CVE-2012-4598
An unspecified ActiveX control in McAfee Virtual Technician (MVT) before 6.4, and ePO-MVT, allows remote attackers to execute arbitrary code or cause a denial of service (Internet Explorer crash) via a crafted web site.
CVE-2012-4599
McAfee SmartFilter Administration, and SmartFilter Administration Bess Edition, before 4.2.1.01 does not require authentication for access to the JBoss Remote Method Invocation (RMI) interface, which allows remote attackers to execute arbitrary code via a crafted .war file.
