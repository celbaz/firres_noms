CVE-2016-7035
An authorization flaw was found in Pacemaker before 1.1.16, where it did not properly guard its IPC interface. An attacker with an unprivileged account on a Pacemaker node could use this flaw to, for example, force the Local Resource Manager daemon to execute a script as root and thereby gain root access on the machine.
CVE-2016-7041
Drools Workbench contains a path traversal vulnerability. The vulnerability allows a remote, authenticated attacker to bypass the directory restrictions and retrieve arbitrary files from the affected host.
CVE-2016-7056
A timing attack flaw was found in OpenSSL 1.0.1u and before that could allow a malicious user with local access to recover ECDSA P-256 private keys.
CVE-2016-7061
An information disclosure vulnerability was found in JBoss Enterprise Application Platform before 7.0.4. It was discovered that when configuring RBAC and marking information as sensitive, users with a Monitor role are able to view the sensitive information.
CVE-2016-7067
Monit before version 5.20.0 is vulnerable to a cross site request forgery attack. Successful exploitation will enable an attacker to disable/enable all monitoring for a particular host or disable/enable monitoring for a specific service.
CVE-2016-7071
It was found that the CloudForms before 5.6.2.2, and 5.7.0.7 did not properly apply permissions controls to VM IDs passed by users. A remote, authenticated attacker could use this flaw to execute arbitrary VMs on systems managed by CloudForms if they know the ID of the VM.
CVE-2016-7072
An issue has been found in PowerDNS Authoritative Server before 3.4.11 and 4.0.2 allowing a remote, unauthenticated attacker to cause a denial of service by opening a large number of TCP connections to the web server. If the web server runs out of file descriptors, it triggers an exception and terminates the whole PowerDNS process. While it's more complicated for an unauthorized attacker to make the web server run out of file descriptors since its connection will be closed just after being accepted, it might still be possible.
CVE-2016-7075
It was found that Kubernetes as used by Openshift Enterprise 3 did not correctly validate X.509 client intermediate certificate host name fields. An attacker could use this flaw to bypass authentication requirements by using a specially crafted X.509 certificate.
CVE-2016-7077
foreman before 1.14.0 is vulnerable to an information leak. It was found that Foreman form helper does not authorize options for associated objects. Unauthorized user can see names of such objects if their count is less than 6.
CVE-2016-7078
foreman before version 1.15.0 is vulnerable to an information leak through organizations and locations feature. When a user is assigned _no_ organizations/locations, they are able to view all resources instead of none (mirroring an administrator's view). The user's actions are still limited by their assigned permissions, e.g. to control viewing, editing and deletion.
CVE-2016-9048
Multiple exploitable SQL Injection vulnerabilities exists in ProcessMaker Enterprise Core 3.0.1.7-community. Specially crafted web requests can cause SQL injections. An attacker can send a web request with parameters containing SQL injection attacks to trigger this vulnerability, potentially allowing exfiltration of the database, user credentials and in certain setups access the underlying operating system.
CVE-2017-1679
IBM OpenPages GRC Platform 7.2, 7.3, 7.4, and 8.0 could allow an attacker to obtain sensitive information from error log files. IBM X-Force ID: 134001.
CVE-2018-11775
TLS hostname verification when using the Apache ActiveMQ Client before 5.15.6 was missing which could make the client vulnerable to a MITM attack between a Java application using the ActiveMQ client and the ActiveMQ server. This is now enabled by default.
CVE-2018-12608
An issue was discovered in Docker Moby before 17.06.0. The Docker engine validated a client TLS certificate using both the configured client CA root certificate and all system roots on non-Windows systems. This allowed a client with any domain validated certificate signed by a system-trusted root CA (as opposed to one signed by the configured CA root certificate) to authenticate.
CVE-2018-14620
The OpenStack RabbitMQ container image insecurely retrieves the rabbitmq_clusterer component over HTTP during the build stage. This could potentially allow an attacker to serve malicious code to the image builder and install in the resultant container image. Version of openstack-rabbitmq-container and openstack-containers as shipped with Red Hat Openstack 12, 13, 14 are believed to be vulnerable.
CVE-2018-14625
A flaw was found in the Linux Kernel where an attacker may be able to have an uncontrolled read to kernel-memory from within a vm guest. A race condition between connect() and close() function may allow an attacker using the AF_VSOCK protocol to gather a 4 byte information leak or possibly intercept or corrupt AF_VSOCK messages destined to other clients.
CVE-2018-14635
When using the Linux bridge ml2 driver, non-privileged tenants are able to create and attach ports without specifying an IP address, bypassing IP address validation. A potential denial of service could occur if an IP address, conflicting with existing guests or routers, is then assigned from outside of the allowed allocation pool. Versions of openstack-neutron before 13.0.0.0b2, 12.0.3 and 11.0.5 are vulnerable.
CVE-2018-14636
Live-migrated instances are briefly able to inspect traffic for other instances on the same hypervisor. This brief window could be extended indefinitely if the instance's port is set administratively down prior to live-migration and kept down after the migration is complete. This is possible due to the Open vSwitch integration bridge being connected to the instance during migration. When connected to the integration bridge, all traffic for instances using the same Open vSwitch instance would potentially be visible to the migrated guest, as the required Open vSwitch VLAN filters are only applied post-migration. Versions of openstack-neutron before 13.0.0.0b2, 12.0.3, 11.0.5 are vulnerable.
CVE-2018-15886
Monstra CMS 3.0.4 does not properly restrict modified Snippet content, as demonstrated by the admin/index.php?id=snippets&action=edit_snippet&filename=google-analytics URI, which allows attackers to execute arbitrary PHP code by placing this code after a <?php substring.
CVE-2018-16591
FURUNO FELCOM 250 and 500 devices allow unauthenticated users to change the password for the Admin, Log and Service accounts, as well as the password for the protected "SMS" panel via /cgi-bin/sm_changepassword.cgi and /cgi-bin/sm_sms_changepasswd.cgi.
CVE-2018-16608
In Monstra CMS 3.0.4, an attacker with 'Editor' privileges can change the password of the administrator via an admin/index.php?id=users&action=edit&user_id=1, Insecure Direct Object Reference (IDOR).
CVE-2018-16705
FURUNO FELCOM 250 and 500 devices allow unauthenticated access to the xml/permission.xml file containing all of the system's usernames and passwords. This includes the Admin and Service user accounts and their unsalted MD5 hashes, as well as the SMS server password in cleartext.
CVE-2018-16764
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because of an IR::FunctionValidationContext::catch_all heap-based buffer over-read.
CVE-2018-16765
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because of an unspecified "heap-buffer-overflow" condition in FunctionValidationContext::else_.
CVE-2018-16766
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because Errors::unreachable() is reached.
CVE-2018-16767
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because of an unspecified "heap-buffer-overflow" condition in FunctionValidationContext::popAndValidateOperand.
CVE-2018-16768
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because of an unspecified "heap-buffer-overflow" condition in IR::FunctionValidationContext::end.
CVE-2018-16769
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because libRuntime.so!llvm::InstructionCombiningPass::runOnFunction is mishandled.
CVE-2018-16770
In WAVM through 2018-07-26, a crafted file sent to the WebAssembly Virtual Machine may cause a denial of service (application crash) or possibly have unspecified other impact because a certain new_allocator allocate call fails.
CVE-2018-16771
Hoosk v1.7.0 allows PHP code execution via a SiteUrl that is provided during installation and mishandled in config.php.
CVE-2018-16772
Hoosk v1.7.0 allows XSS via the Navigation Title of a new page entered at admin/pages/new.
CVE-2018-16773
EasyCMS 1.5 allows XSS via the index.php?s=/admin/fields/update/navTabId/listfields/callbackType/closeCurrent content field.
CVE-2018-16774
HongCMS 3.0.0 allows arbitrary file deletion via a ../ in the file parameter to admin/index.php/language/ajax?action=delete.
CVE-2018-16775
An issue was discovered in Victor CMS through 2018-05-10. There is XSS via the site name in the "Categories" menu.
CVE-2018-16776
wityCMS 0.6.2 has XSS via the "Site Name" field found in the "Contact" "Configuration" page.
CVE-2018-16779
BlogCMS through 2016-10-25 has XSS via a comment.
CVE-2018-16780
Complete Responsive CMS Blog through 2018-05-20 has XSS via a comment.
CVE-2018-16781
ffjpeg.dll in ffjpeg before 2018-08-22 allows remote attackers to cause a denial of service (FPE signal) via a progressive JPEG file that lacks an AC Huffman table.
CVE-2018-16782
libimageworsener.a in ImageWorsener 1.3.2 has a buffer overflow in the bmpr_read_rle_internal function in imagew-bmp.c.
CVE-2018-16790
_bson_iter_next_internal in bson-iter.c in libbson 1.12.0, as used in MongoDB mongo-c-driver and other products, has a heap-based buffer over-read via a crafted bson buffer.
CVE-2018-16797
A heap-based buffer overflow in PotPlayerMini.exe in PotPlayer 1.7.8556 allows remote attackers to execute arbitrary code via a .wav file with large BytesPerSec and SamplesPerSec values, and a small Data_Chunk_Size value.
CVE-2018-16802
An issue was discovered in Artifex Ghostscript before 9.25. Incorrect "restoration of privilege" checking when running out of stack during exception handling could be used by attackers able to supply crafted PostScript to execute code using the "pipe" instruction. This is due to an incomplete fix for CVE-2018-16509.
CVE-2018-16805
In b3log Solo 2.9.3, XSS in the Input page under the Publish Articles menu, with an ID of linkAddress stored in the link JSON field, allows remote attackers to inject arbitrary Web scripts or HTML via a crafted site name provided by an administrator.
CVE-2018-16806
A Pektron Passive Keyless Entry and Start (PKES) system, as used on the Tesla Model S and possibly other vehicles, relies on the DST40 cipher, which makes it easier for attackers to obtain access via an approach involving a 5.4 TB precomputation, followed by wake-frame reception and two challenge/response operations, to clone a key fob within a few seconds.
CVE-2018-3875
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. The strncpy overflows the destination buffer, which has a size of 2,000 bytes. An attacker can send an arbitrarily long "sessionToken" value in order to exploit this vulnerability.
CVE-2018-3896
An exploitable buffer overflow vulnerabilities exist in the /cameras/XXXX/clips handler of video-core's HTTP server of Samsung SmartThings Hub with Firmware version 0.20.17. The video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. The strncpy call overflows the destination buffer, which has a size of 52 bytes. An attacker can send an arbitrarily long "correlationId" value in order to exploit this vulnerability.
CVE-2018-3897
An exploitable buffer overflow vulnerabilities exist in the /cameras/XXXX/clips handler of video-core's HTTP server of Samsung SmartThings Hub with Firmware version 0.20.17. The video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. The strncpy call overflows the destination buffer, which has a size of 52 bytes. An attacker can send an arbitrarily long "callbackUrl" value in order to exploit this vulnerability.
