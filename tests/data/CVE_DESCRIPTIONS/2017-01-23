CVE-2013-7451
The validator module before 1.1.0 for Node.js allows remote attackers to bypass the XSS filter via a nested tag.
CVE-2013-7452
The validator module before 1.1.0 for Node.js allows remote attackers to bypass the cross-site scripting (XSS) filter via a crafted javascript URI.
CVE-2013-7453
The validator module before 1.1.0 for Node.js allows remote attackers to bypass the cross-site scripting (XSS) filter via vectors related to UI redressing.
CVE-2013-7454
The validator module before 1.1.0 for Node.js allows remote attackers to bypass the cross-site scripting (XSS) filter via nested forbidden strings.
CVE-2014-8362
Vivint Sky Control Panel 1.1.1.9926 allows remote attackers to enable and disable the alarm system and modify other security settings via the Web-enabled interface.
CVE-2014-9772
The validator package before 2.0.0 for Node.js allows remote attackers to bypass the cross-site scripting (XSS) filter via hex-encoded characters.
CVE-2015-4626
B.A.S C2Box before 4.0.0 (r19171) relies on client-side validation, which allows remote attackers to "corrupt the business logic" via a negative value in an overdraft.
CVE-2015-7743
XML external entity vulnerability in PRTG Network Monitor before 16.2.23.3077/3078 allows remote authenticated users to read arbitrary files by creating a new HTTP XML/REST Value sensor that accesses a crafted XML file.
CVE-2015-8315
The ms package before 0.7.1 for Node.js allows attackers to cause a denial of service (CPU consumption) via a long version string, aka a "regular expression denial of service (ReDoS)."
CVE-2015-8854
The marked package before 0.3.4 for Node.js allows attackers to cause a denial of service (CPU consumption) via unspecified vectors that trigger a "catastrophic backtracking issue for the em inline rule," aka a "regular expression denial of service (ReDoS)."
CVE-2015-8855
The semver package before 4.3.2 for Node.js allows attackers to cause a denial of service (CPU consumption) via a long version string, aka a "regular expression denial of service (ReDoS)."
CVE-2015-8856
Cross-site scripting (XSS) vulnerability in the serve-index package before 1.6.3 for Node.js allows remote attackers to inject arbitrary web script or HTML via a crafted file or directory name.
CVE-2015-8857
The uglify-js package before 2.4.24 for Node.js does not properly account for non-boolean values when rewriting boolean expressions, which might allow attackers to bypass security mechanisms or possibly have unspecified other impact by leveraging improperly rewritten Javascript.
CVE-2015-8858
The uglify-js package before 2.6.0 for Node.js allows attackers to cause a denial of service (CPU consumption) via crafted input in a parse call, aka a "regular expression denial of service (ReDoS)."
CVE-2015-8859
The send package before 0.11.1 for Node.js allows attackers to obtain the root path via unspecified vectors.
CVE-2015-8860
The tar package before 2.0.0 for Node.js allows remote attackers to write to arbitrary files via a symlink attack in an archive.
CVE-2015-8861
The handlebars package before 4.0.0 for Node.js allows remote attackers to conduct cross-site scripting (XSS) attacks by leveraging a template with an attribute that is not quoted.
CVE-2015-8862
mustache package before 2.2.1 for Node.js allows remote attackers to conduct cross-site scripting (XSS) attacks by leveraging a template with an attribute that is not quoted.
CVE-2015-8971
Terminology 0.7.0 allows remote attackers to execute arbitrary commands via escape sequences that modify the window title and then are written to the terminal, a similar issue to CVE-2003-0063.
CVE-2015-8972
Stack-based buffer overflow in the ValidateMove function in frontend/move.cc in GNU Chess (aka gnuchess) before 6.2.4 might allow context-dependent attackers to execute arbitrary code via a large input, as demonstrated when in UCI mode.
CVE-2016-0765
Multiple cross-site scripting (XSS) vulnerabilities in eshop-orders.php in the eShop plugin 6.3.14 for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) page or (2) action parameter.
CVE-2016-0769
Multiple SQL injection vulnerabilities in eshop-orders.php in the eShop plugin 6.3.14 for WordPress allow (1) remote administrators to execute arbitrary SQL commands via the delid parameter or remote authenticated users to execute arbitrary SQL commands via the (2) view, (3) mark, or (4) change parameter.
CVE-2016-10101
Information Disclosure can occur in Hitek Software's Automize 10.x and 11.x passManager.jsd. Users have the Read attribute, which allows an attacker to recover the encrypted password to access the Password Manager.
CVE-2016-10102
hitek.jar in Hitek Software's Automize uses weak encryption when encrypting SSH/SFTP and Encryption profile passwords. This allows an attacker to retrieve the encrypted passwords from sshProfiles.jsd and encryptionProfiles.jsd and decrypt them to recover cleartext passwords. All 10.x up to and including 10.25 and all 11.x up to and including 11.14 are verified to be affected.
CVE-2016-10103
Information Disclosure can occur in encryptionProfiles.jsd in Hitek Software's Automize because of the Read attribute being set for Users. This allows an attacker to recover encrypted passwords for GPG Encryption profiles. Verified in all 10.x versions up to and including 10.25, and all 11.x versions up to and including 11.14.
CVE-2016-10104
Information Disclosure can occur in sshProfiles.jsd in Hitek Software's Automize because of the Read attribute being set for Users. This allows an attacker to recover encrypted passwords for SSH/SFTP profiles. Verified in all 10.x versions up to and including 10.25, and all 11.x versions up to and including 11.14.
CVE-2016-10156
A flaw in systemd v228 in /src/basic/fs-util.c caused world writable suid files to be created when using the systemd timers features, allowing local attackers to escalate their privileges to root. This is fixed in v229.
CVE-2016-10157
Akamai NetSession 1.9.3.1 is vulnerable to DLL Hijacking: it tries to load CSUNSAPI.dll without supplying the complete path. The issue is aggravated because the mentioned DLL is missing from the installation, thus making it possible to hijack the DLL and subsequently inject code within the Akamai NetSession process space.
CVE-2016-1281
Untrusted search path vulnerability in the installer for TrueCrypt 7.2 and 7.1a, VeraCrypt before 1.17-BETA, and possibly other products allows local users to execute arbitrary code with administrator privileges and conduct DLL hijacking attacks via a Trojan horse DLL in the "application directory", as demonstrated with the USP10.dll, RichEd20.dll, NTMarta.dll and SRClient.dll DLLs.
CVE-2016-1417
Untrusted search path vulnerability in Snort 2.9.7.0-WIN32 allows remote attackers to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse tcapi.dll that is located in the same folder on a remote file share as a pcap file that is being processed.
CVE-2016-1925
Integer underflow in header.c in lha allows remote attackers to have unspecified impact via a large header size value for the (1) level0 or (2) level1 header in a lha archive, which triggers a buffer overflow.
CVE-2016-2242
Exponent CMS 2.x before 2.3.7 Patch 3 allows remote attackers to execute arbitrary code via the sc parameter to install/index.php.
CVE-2016-2783
Avaya Fabric Connect Virtual Services Platform (VSP) Operating System Software (VOSS) before 4.2.3.0 and 5.x before 5.0.1.0 does not properly handle VLAN and I-SIS indexes, which allows remote attackers to obtain unauthorized access via crafted Ethernet frames.
CVE-2016-3147
Buffer overflow in the collector.exe listener of the Landesk Management Suite 10.0.0.271 and earlier allows remote attackers to cause a denial of service and possibly execute arbitrary code via a large packet.
CVE-2016-3177
Multiple use-after-free and double-free vulnerabilities in gifcolor.c in GIFLIB 5.1.2 have unspecified impact and attack vectors.
CVE-2016-4010
Magento CE and EE before 2.0.6 allows remote attackers to conduct PHP objection injection attacks and execute arbitrary PHP code via crafted serialized shopping cart data.
CVE-2016-4055
The duration function in the moment package before 2.11.2 for Node.js allows remote attackers to cause a denial of service (CPU consumption) via a long string, aka a "regular expression Denial of Service (ReDoS)."
CVE-2016-4056
Cross-site scripting (XSS) vulnerability in the Backend component in TYPO3 6.2.x before 6.2.19 allows remote attackers to inject arbitrary web script or HTML via the module parameter when creating a bookmark.
CVE-2016-4338
The mysql user parameter configuration script (userparameter_mysql.conf) in the agent in Zabbix before 2.0.18, 2.2.x before 2.2.13, and 3.0.x before 3.0.3, when used with a shell other than bash, allows context-dependent attackers to execute arbitrary code or SQL commands via the mysql.size parameter.
CVE-2016-4340
The impersonate feature in Gitlab 8.7.0, 8.6.0 through 8.6.7, 8.5.0 through 8.5.11, 8.4.0 through 8.4.9, 8.3.0 through 8.3.8, and 8.2.0 through 8.2.4 allows remote authenticated users to "log in" as any other user via unspecified vectors.
CVE-2016-4484
The Debian initrd script for the cryptsetup package 2:1.7.3-2 and earlier allows physically proximate attackers to gain shell access via many log in attempts with an invalid password.
CVE-2016-4793
The clientIp function in CakePHP 3.2.4 and earlier allows remote attackers to spoof their IP via the CLIENT-IP HTTP header.
CVE-2016-5091
Extbase in TYPO3 4.3.0 before 6.2.24, 7.x before 7.6.8, and 8.1.1 allows remote attackers to obtain sensitive information or possibly execute arbitrary code via a crafted Extbase action.
CVE-2016-5119
The automatic update feature in KeePass 2.33 and earlier allows man-in-the-middle attackers to execute arbitrary code by spoofing the version check response and supplying a crafted update.
CVE-2016-5237
Valve Steam 3.42.16.13 uses weak permissions for the files in the Steam program directory, which allows local users to modify the files and possibly gain privileges as demonstrated by a Trojan horse Steam.exe file.
CVE-2016-5697
Ruby-saml before 1.3.0 allows attackers to perform XML signature wrapping attacks via unspecified vectors.
CVE-2016-5720
Multiple untrusted search path vulnerabilities in Microsoft Skype allow local users to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse (1) msi.dll, (2) dpapi.dll, or (3) cryptui.dll that is located in the current working directory.
CVE-2016-5742
SQL injection vulnerability in the XML-RPC interface in Movable Type Pro and Advanced 6.x before 6.1.3 and 6.2.x before 6.2.6 and Movable Type Open Source 5.2.13 and earlier allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2016-5873
Buffer overflow in the HTTP URL parsing functions in pecl_http before 3.0.1 might allow remote attackers to execute arbitrary code via non-printable characters in a URL.
CVE-2016-5876
ownCloud server before 8.2.6 and 9.x before 9.0.3, when the gallery app is enabled, allows remote attackers to download arbitrary images via a direct request.
CVE-2016-6160
tcprewrite in tcpreplay before 4.1.2 allows remote attackers to cause a denial of service (segmentation fault) via a large frame, a related issue to CVE-2017-14266.
CVE-2016-6164
Integer overflow in the mov_build_index function in libavformat/mov.c in FFmpeg before 2.8.8, 3.0.x before 3.0.3 and 3.1.x before 3.1.1 allows remote attackers to have unspecified impact via vectors involving sample size.
CVE-2016-6223
The TIFFReadRawStrip1 and TIFFReadRawTile1 functions in tif_read.c in libtiff before 4.0.7 allows remote attackers to cause a denial of service (crash) or possibly obtain sensitive information via a negative index in a file-content buffer.
CVE-2016-6484
CRLF injection vulnerability in Infoblox Network Automation NetMRI before 7.1.1 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via the contentType parameter in a login action to config/userAdmin/login.tdf.
CVE-2016-6517
Directory traversal vulnerability in Liferay 5.1.0 allows remote attackers to have unspecified impact via a %2E%2E (encoded dot dot) in the minifierBundleDir parameter to barebone.jsp.
CVE-2016-6521
Cross-site request forgery (CSRF) vulnerability in Grails console (aka Grails Debug Console and Grails Web Console) 2.0.7, 1.5.10, and earlier allows remote attackers to hijack the authentication of users for requests that execute arbitrary Groovy code via unspecified vectors.
CVE-2016-6582
The Doorkeeper gem before 4.2.0 for Ruby might allow remote attackers to conduct replay attacks or revoke arbitrary tokens by leveraging failure to implement the OAuth 2.0 Token Revocation specification.
CVE-2016-6600
Directory traversal vulnerability in the file upload functionality in ZOHO WebNMS Framework 5.2 and 5.2 SP1 allows remote attackers to upload and execute arbitrary JSP files via a .. (dot dot) in the fileName parameter to servlets/FileUploadServlet.
CVE-2016-6601
Directory traversal vulnerability in the file download functionality in ZOHO WebNMS Framework 5.2 and 5.2 SP1 allows remote attackers to read arbitrary files via a .. (dot dot) in the fileName parameter to servlets/FetchFile.
CVE-2016-6602
ZOHO WebNMS Framework 5.2 and 5.2 SP1 use a weak obfuscation algorithm to store passwords, which allows context-dependent attackers to obtain cleartext passwords by leveraging access to WEB-INF/conf/securitydbData.xml.  NOTE: this issue can be combined with CVE-2016-6601 for a remote exploit.
CVE-2016-6603
ZOHO WebNMS Framework 5.2 and 5.2 SP1 allows remote attackers to bypass authentication and impersonate arbitrary users via the UserName HTTP header.
CVE-2016-6668
The Atlassian Hipchat Integration Plugin for Bitbucket Server 6.26.0 before 6.27.5, 6.28.0 before 7.3.7, and 7.4.0 before 7.8.17; Confluence HipChat plugin 6.26.0 before 7.8.17; and HipChat for JIRA plugin 6.26.0 before 7.8.17 allows remote attackers to obtain the secret key for communicating with HipChat instances by reading unspecified pages.
CVE-2016-6920
Heap-based buffer overflow in the decode_block function in libavcodec/exr.c in FFmpeg before 3.1.3 allows remote attackers to cause a denial of service (application crash) via vectors involving tile positions.
CVE-2016-7036
python-jose before 1.3.2 allows attackers to have unspecified impact by leveraging failure to use a constant time comparison for HMAC keys.
CVE-2016-7037
The verify function in Encryption/Symmetric.php in Malcolm Fell jwt before 1.0.3 does not use a timing-safe function for hash comparison, which allows attackers to spoof signatures via a timing attack.
CVE-2016-7102
ownCloud Desktop before 2.2.3 allows local users to execute arbitrary code and possibly gain privileges via a Trojan library in a "special path" in the C: drive.
CVE-2016-7410
The _dwarf_read_loc_section function in dwarf_loc.c in libdwarf 20160613 allows attackers to cause a denial of service (buffer over-read) via a crafted file.
CVE-2016-7567
Buffer overflow in the SLPFoldWhiteSpace function in common/slp_compare.c in OpenSLP 2.0 allows remote attackers to have unspecified impact via a crafted string.
CVE-2016-7792
Ubiquiti Networks UniFi 5.2.7 does not restrict access to the database, which allows remote attackers to modify the database by directly connecting to it.
CVE-2016-8213
EMC Documentum WebTop Version 6.8, prior to P18 and Version 6.8.1, prior to P06; and EMC Documentum TaskSpace version 6.7SP3, prior to P02; and EMC Documentum Capital Projects Version 1.9, prior to P30 and Version 1.10, prior to P17; and EMC Documentum Administrator Version 7.0, Version 7.1, and Version 7.2 prior to P18 contain a Stored Cross-Site Scripting Vulnerability that could potentially be exploited by malicious users to compromise the affected system.
CVE-2016-9012
CloudVision Portal (CVP) before 2016.1.2.1 allows remote authenticated users to gain access to the internal configuration mechanisms via the management plane, related to a request to /web/system/console/bundle.
CVE-2016-9081
Joomla! 3.4.4 through 3.6.3 allows attackers to reset username, password, and user group assignments and possibly perform other user account modifications via unspecified vectors.
CVE-2016-9379
The pygrub boot loader emulator in Xen, when S-expression output format is requested, allows local pygrub-using guest OS administrators to read or delete arbitrary files on the host via string quotes and S-expressions in the bootloader configuration file.
CVE-2016-9380
The pygrub boot loader emulator in Xen, when nul-delimited output format is requested, allows local pygrub-using guest OS administrators to read or delete arbitrary files on the host via NUL bytes in the bootloader configuration file.
CVE-2016-9381
Race condition in QEMU in Xen allows local x86 HVM guest OS administrators to gain privileges by changing certain data on shared rings, aka a "double fetch" vulnerability.
CVE-2016-9382
Xen 4.0.x through 4.7.x mishandle x86 task switches to VM86 mode, which allows local 32-bit x86 HVM guest OS users to gain privileges or cause a denial of service (guest OS crash) by leveraging a guest operating system that uses hardware task switching and allows a new task to start in VM86 mode.
CVE-2016-9383
Xen, when running on a 64-bit hypervisor, allows local x86 guest OS users to modify arbitrary memory and consequently obtain sensitive information, cause a denial of service (host crash), or execute arbitrary code on the host by leveraging broken emulation of bit test instructions.
CVE-2016-9385
The x86 segment base write emulation functionality in Xen 4.4.x through 4.7.x allows local x86 PV guest OS administrators to cause a denial of service (host crash) by leveraging lack of canonical address checks.
CVE-2016-9386
The x86 emulator in Xen does not properly treat x86 NULL segments as unusable when accessing memory, which might allow local HVM guest users to gain privileges via vectors involving "unexpected" base/limit values.
CVE-2016-9401
popd in bash might allow local users to bypass the restricted shell and cause a use-after-free via a crafted address.
CVE-2016-9445
Integer overflow in the vmnc decoder in the gstreamer allows remote attackers to cause a denial of service (crash) via large width and height values, which triggers a buffer overflow.
CVE-2016-9446
The vmnc decoder in the gstreamer does not initialize the render canvas, which allows remote attackers to obtain sensitive information as demonstrated by thumbnailing a simple 1 frame vmnc movie that does not draw to the allocated render canvas.
CVE-2016-9447
The ROM mappings in the NSF decoder in gstreamer 0.10.x allow remote attackers to cause a denial of service (out-of-bounds read or write) and possibly execute arbitrary code via a crafted NSF music file.
CVE-2016-9870
EMC Isilon OneFS 8.0.0.0, EMC Isilon OneFS 7.2.1.0 - 7.2.1.2, EMC Isilon OneFS 7.2.0.x, EMC Isilon OneFS 7.1.1.0 - 7.1.1.10, and EMC Isilon OneFS 7.1.0.x is affected by an LDAP injection vulnerability that could potentially be exploited by a malicious user to compromise the system.
CVE-2017-5182
Remote Manager in Open Enterprise Server (OES) allows unauthenticated remote attackers to read any arbitrary file, via a specially crafted URL, that allows complete directory traversal and total information disclosure. This vulnerability is present on all versions of OES for linux, it applies to OES2015 SP1 before Maintenance Update 11080, OES2015 before Maintenance Update 11079, OES11 SP3 before Maintenance Update 11078, OES11 SP2 before Maintenance Update 11077).
CVE-2017-5371
Odata Server in SAP Adaptive Server Enterprise (ASE) 16 allows remote attackers to cause a denial of service (process crash) via a series of crafted requests, aka SAP Security Note 2330422.
CVE-2017-5372
The function msp (aka MSPRuntimeInterface) in the P4 SERVERCORE component in SAP AS JAVA allows remote attackers to obtain sensitive system information by leveraging a missing authorization check for the (1) getInformation, (2) getParameters, (3) getServiceInfo, (4) getStatistic, or (5) getClientStatistic function, aka SAP Security Note 2331908.
CVE-2017-5539
The patch for directory traversal (CVE-2017-5480) in b2evolution version 6.8.4-stable has a bypass vulnerability. An attacker can use ..\/ to bypass the filter rule. Then, this attacker can exploit this vulnerability to delete or read any files on the server. It can also be used to determine whether a file exists.
CVE-2017-5544
An issue was discovered on FiberHome Fengine S5800 switches V210R240. An unauthorized attacker can access the device's SSH service, using a password cracking tool to establish SSH connections quickly. This will trigger an increase in the SSH login timeout (each of the login attempts will occupy a connection slot for a longer time). Once this occurs, legitimate login attempts via SSH/telnet will be refused, resulting in a denial of service; you must restart the device.
CVE-2017-5553
Cross-site scripting (XSS) vulnerability in plugins/markdown_plugin/_markdown.plugin.php in b2evolution before 6.8.5 allows remote authenticated users to inject arbitrary web script or HTML via a javascript: URL.
CVE-2017-5554
An issue was discovered in ABOOT in OnePlus 3 and 3T OxygenOS before 4.0.2. The attacker can reboot the device into the fastboot mode, which could be done without any authentication. A physical attacker can press the "Volume Up" button during device boot, where an attacker with ADB access can issue the adb reboot bootloader command. Then, the attacker can put the platform's SELinux in permissive mode, which severely weakens it, by issuing: fastboot oem selinux permissive.
CVE-2017-5556
The ConvertToPDF plugin in Foxit Reader before 8.2 and PhantomPDF before 8.2 on Windows, when the gflags app is enabled, allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted JPEG image. The vulnerability could lead to information disclosure; an attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process.
CVE-2017-5563
LibTIFF version 4.0.7 is vulnerable to a heap-based buffer over-read in tif_lzw.c resulting in DoS or code execution via a crafted bmp image to tools/bmp2tiff.
CVE-2017-5569
An issue was discovered in eClinicalWorks Patient Portal 7.0 build 13. This is a blind SQL injection within the template.jsp, which can be exploited without the need of authentication and via an HTTP POST request, and which can be used to dump database data out to a malicious server, using an out-of-band technique such as select_loadfile().
CVE-2017-5570
An issue was discovered in eClinicalWorks Patient Portal 7.0 build 13. This is a blind SQL injection within the messageJson.jsp, which can only be exploited by authenticated users via an HTTP POST request and which can be used to dump database data out to a malicious server, using an out-of-band technique such as select_loadfile().
CVE-2017-5574
SQL injection vulnerability in register.php in GeniXCMS before 1.0.0 allows unauthenticated users to execute arbitrary SQL commands via the activation parameter.
CVE-2017-5575
SQL injection vulnerability in inc/lib/Options.class.php in GeniXCMS before 1.0.0 allows remote attackers to execute arbitrary SQL commands via the modules parameter.
