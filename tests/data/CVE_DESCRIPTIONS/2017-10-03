CVE-2014-0043
In Apache Wicket 1.5.10 or 6.13.0, by issuing requests to special urls handled by Wicket, it is possible to check for the existence of particular classes in the classpath and thus check whether a third party library with a known security vulnerability is in use.
CVE-2015-3321
Services and files in Lenovo Fingerprint Manager before 8.01.42 have incorrect ACLs, which allows local users to invalidate local checks and gain privileges via standard filesystem operations.
CVE-2015-6576
Bamboo 2.2 before 5.8.5 and 5.9.x before 5.9.7 allows remote attackers with access to the Bamboo web interface to execute arbitrary Java code via an unspecified resource.
CVE-2015-6971
Lenovo System Update (formerly ThinkVantage System Update) before 5.07.0013 allows local users to submit commands to the System Update service (SUService.exe) and gain privileges by launching signed Lenovo executables.
CVE-2015-7357
Cross-site scripting (XSS) vulnerability in the uDesign (aka U-Design) theme 2.3.0 before 2.7.10 for WordPress allows remote attackers to inject arbitrary web script or HTML via a fragment identifier, as demonstrated by #<svg onload=alert(1)>.
CVE-2015-7358
The IsDriveLetterAvailable method in Driver/Ntdriver.c in TrueCrypt 7.0, VeraCrypt before 1.15, and CipherShed, when running on Windows, does not properly validate drive letter symbolic links, which allows local users to mount an encrypted volume over an existing drive letter and gain privileges via an entry in the /GLOBAL?? directory.
CVE-2015-7359
The (1) IsVolumeAccessibleByCurrentUser and (2) MountDevice methods in Ntdriver.c in TrueCrypt 7.0, VeraCrypt before 1.15, and CipherShed, when running on Windows, do not check the impersonation level of impersonation tokens, which allows local users to impersonate a user at SecurityIdentify level and gain access to other users' mounted encrypted volumes.
CVE-2015-7841
The login page of the server on Huawei FusionServer rack servers RH2288 V3 with software before V100R003C00SPC603, RH2288H V3 with software before V100R003C00SPC503, XH628 V3 with software before V100R003C00SPC602, RH1288 V3 with software before V100R003C00SPC602, RH2288A V2 with software before V100R002C00SPC701, RH1288A V2 with software before V100R002C00SPC502, RH8100 V3 with software before V100R003C00SPC110, CH222 V3 with software before V100R001C00SPC161, CH220 V3 with software before V100R001C00SPC161, and CH121 V3 with software before V100R001C00SPC161 allows remote attackers to bypass access restrictions and enter commands via unspecified parameters, as demonstrated by a "user creation command."
CVE-2015-7843
The management interface on Huawei FusionServer rack servers RH2288 V3 with software before V100R003C00SPC603, RH2288H V3 with software before V100R003C00SPC503, XH628 V3 with software before V100R003C00SPC602, RH1288 V3 with software before V100R003C00SPC602, RH2288A V2 with software before V100R002C00SPC701, RH1288A V2 with software before V100R002C00SPC502, RH8100 V3 with software before V100R003C00SPC110, CH222 V3 with software before V100R001C00SPC161, CH220 V3 with software before V100R001C00SPC161, and CH121 V3 with software before V100R001C00SPC161 does not limit the number of query attempts, which allows remote authenticated users to obtain credentials of higher-level users via a brute force attack.
CVE-2015-7980
Cross-site scripting (XSS) vulnerability in the Compass Rose module 6.x-1.x before 6.x-1.1 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, related to "embedding a JavaScript library from an external source that was not reliable."
CVE-2016-6806
Apache Wicket 6.x before 6.25.0, 7.x before 7.5.0, and 8.0.0-M1 provide a CSRF prevention measure that fails to discover some cross origin requests. The mitigation is to not only check the Origin HTTP header, but also take the Referer HTTP header into account when no Origin was provided. Furthermore, not all Wicket server side targets were subjected to the CSRF check. This was also fixed.
CVE-2017-11321
The restricted shell interface in UCOPIA Wireless Appliance before 5.1.8 allows remote authenticated users to gain 'admin' privileges via shell metacharacters in the less command.
CVE-2017-11322
The chroothole_client executable in UCOPIA Wireless Appliance before 5.1.8 allows remote attackers to gain root privileges via a dollar sign ($) metacharacter in the argument to chroothole_client.
CVE-2017-11496
Stack buffer overflow in hasplms in Gemalto ACC (Admin Control Center), all versions ranging from HASP SRM 2.10 to Sentinel LDK 7.50, allows remote attackers to execute arbitrary code via malformed ASN.1 streams in V2C and similar input files.
CVE-2017-11497
Stack buffer overflow in hasplms in Gemalto ACC (Admin Control Center), all versions ranging from HASP SRM 2.10 to Sentinel LDK 7.50, allows remote attackers to execute arbitrary code via language packs containing filenames longer than 1024 characters.
CVE-2017-11498
Buffer overflow in hasplms in Gemalto ACC (Admin Control Center), all versions ranging from HASP SRM 2.10 to Sentinel LDK 7.50, allows remote attackers to shut down the remote process (a denial of service) via a language pack (ZIP file) with invalid HTML files.
CVE-2017-12620
When loading models or dictionaries that contain XML it is possible to perform an XXE attack, since Apache OpenNLP is a library, this only affects applications that load models or dictionaries from untrusted sources. The versions 1.5.0 to 1.5.3, 1.6.0, 1.7.0 to 1.7.2, 1.8.0 to 1.8.1 of Apache OpenNLP are affected.
CVE-2017-12638
Stack based buffer overflow in Ipswitch IMail server up to and including 12.5.5 allows remote attackers to execute arbitrary code via unspecified vectors in IMmailSrv, aka ETBL or ETCETERABLUE.
CVE-2017-12639
Stack based buffer overflow in Ipswitch IMail server up to and including 12.5.5 allows remote attackers to execute arbitrary code via unspecified vectors in IMmailSrv, aka ETRE or ETCTERARED.
CVE-2017-12792
Multiple cross-site request forgery (CSRF) vulnerabilities in NexusPHP 1.5 allow remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via the (1) linkname, (2) url, or (3) title parameter in an add action to linksmanage.php.
CVE-2017-1311
IBM Insights Foundation for Energy 2.0 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database. IBM X-Force ID: 125719.
CVE-2017-1324
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 125975.
CVE-2017-1334
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126242.
CVE-2017-1335
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126243.
CVE-2017-1345
IBM Insights Foundation for Energy 2.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126460.
CVE-2017-1359
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126686.
CVE-2017-1364
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126857.
CVE-2017-1369
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126862.
CVE-2017-13704
In dnsmasq before 2.78, if the DNS packet size does not match the expected size, the size parameter in a memset call gets a negative value. As it is an unsigned value, memset ends up writing up to 0xffffffff zero's (0xffffffffffffffff in 64 bit platforms), making dnsmasq crash.
CVE-2017-13997
A Missing Authentication for Critical Function issue was discovered in Schneider Electric InduSoft Web Studio v8.0 SP2 or prior, and InTouch Machine Edition v8.0 SP2 or prior. InduSoft Web Studio provides the capability for an HMI client to trigger script execution on the server for the purposes of performing customized calculations or actions. A remote malicious entity could bypass the server authentication and trigger the execution of an arbitrary command. The command is executed under high privileges and could lead to a complete compromise of the server.
CVE-2017-1429
IBM RELM 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 127587.
CVE-2017-14492
Heap-based buffer overflow in dnsmasq before 2.78 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a crafted IPv6 router advertisement request.
CVE-2017-14493
Stack-based buffer overflow in dnsmasq before 2.78 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a crafted DHCPv6 request.
CVE-2017-14494
dnsmasq before 2.78, when configured as a relay, allows remote attackers to obtain sensitive memory information via vectors involving handling DHCPv6 forwarded requests.
CVE-2017-14495
Memory leak in dnsmasq before 2.78, when the --add-mac, --add-cpe-id or --add-subnet option is specified, allows remote attackers to cause a denial of service (memory consumption) via vectors involving DNS response creation.
CVE-2017-14496
Integer underflow in the add_pseudoheader function in dnsmasq before 2.78 , when the --add-mac, --add-cpe-id or --add-subnet option is specified, allows remote attackers to cause a denial of service via a crafted DNS request.
CVE-2017-14754
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to Arbitrary File Read: /xAdmin/html/cm_datasource_group_xsd.jsp, parameter: xsd_datasource_schema_file filename. In order for this vulnerability to be exploited, an attacker must authenticate to the application first.
CVE-2017-14755
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to Cross-Site Scripting: /xAdmin/html/XPressoDoc, parameter: categoryId.
CVE-2017-14756
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to Cross-Site Scripting: /xAdmin/html/Deployment (cat_id).
CVE-2017-14757
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to SQL Injection: /xDashboard/html/jobhistory/downloadSupportFile.action, parameter: jobRunId. In order for this vulnerability to be exploited, an attacker must authenticate to the application first.
CVE-2017-14758
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to SQL Injection: /xAdmin/html/cm_doclist_view_uc.jsp, parameter: documentId. In order for this vulnerability to be exploited, an attacker must authenticate to the application first.
CVE-2017-14759
OpenText Document Sciences xPression (formerly EMC Document Sciences xPression) v4.5SP1 Patch 13 (older versions might be affected as well) is prone to an XML External Entity vulnerability: /xFramework/services/QuickDoc.QuickDocHttpSoap11Endpoint/. An unauthenticated user is able to read directory listings or system files, or cause SSRF or Denial of Service.
CVE-2017-14770
Skybox Manager Client Application prior to 8.5.501 is prone to an information disclosure vulnerability of user password hashes. A local authenticated attacker can access the password hashes in a debugger-pause state during the authentication process.
CVE-2017-14771
Skybox Manager Client Application prior to 8.5.501 is prone to an arbitrary file upload vulnerability due to insufficient input validation of user-supplied files path when uploading files via the application. During a debugger-pause state, a local authenticated attacker can upload an arbitrary file and overwrite existing files within the scope of the affected application.
CVE-2017-14772
Skybox Manager Client Application is prone to information disclosure via a username enumeration attack. A local unauthenticated attacker could exploit the flaw to obtain valid usernames, by analyzing error messages upon valid and invalid account login attempts.
CVE-2017-14773
Skybox Manager Client Application prior to 8.5.501 is prone to an elevation of privileges vulnerability during authentication of a valid user in a debugger-pause state. The vulnerability can only be exploited by a local authenticated attacker.
CVE-2017-14848
WPHRM Human Resource Management System for WordPress 1.0 allows SQL Injection via the employee_id parameter.
CVE-2017-14979
Gxlcms uses an unsafe character-replacement approach in an attempt to restrict access, which allows remote attackers to read arbitrary files via modified pathnames in the s parameter to index.php, related to Lib/Admin/Action/TplAction.class.php and Lib/Admin/Common/function.php.
CVE-2017-14981
Cross-Site Scripting (XSS) was discovered in ATutor before 2.2.3. The vulnerability exists due to insufficient filtration of data (url in /mods/_standard/rss_feeds/edit_feed.php). An attacker could inject arbitrary HTML and script code into a browser in the context of the vulnerable website.
CVE-2017-14983
Cross-site scripting (XSS) vulnerability in the EyesOfNetwork web interface (aka eonweb) 5.1-0 allows remote authenticated administrators to inject arbitrary web script or HTML via the object parameter to module/admin_conf/index.php.
CVE-2017-14984
Cross-site scripting (XSS) vulnerability in the EyesOfNetwork web interface (aka eonweb) 5.1-0 allows remote authenticated users to inject arbitrary web script or HTML via the bp_name parameter to /module/admin_bp/add_services.php.
CVE-2017-14985
Cross-site scripting (XSS) vulnerability in the EyesOfNetwork web interface (aka eonweb) 5.1-0 allows remote authenticated users to inject arbitrary web script or HTML via the url parameter to module/module_frame/index.php.
CVE-2017-14988
Header::readfrom in IlmImf/ImfHeader.cpp in OpenEXR 2.2.0 allows remote attackers to cause a denial of service (excessive memory allocation) via a crafted file that is accessed with the ImfOpenInputFile function in IlmImf/ImfCRgbaFile.cpp.
CVE-2017-14989
A use-after-free in RenderFreetype in MagickCore/annotate.c in ImageMagick 7.0.7-4 Q16 allows attackers to crash the application via a crafted font file, because the FT_Done_Glyph function (from FreeType 2) is called at an incorrect place in the ImageMagick code.
CVE-2017-14990
WordPress 4.8.2 stores cleartext wp_signups.activation_key values (but stores the analogous wp_users.user_activation_key values as hashes), which might make it easier for remote attackers to hijack unactivated user accounts by leveraging database read access (such as access gained through an unspecified SQL injection vulnerability).
CVE-2017-1569
IBM WebSphere Commerce 7.0 and 8.0 contains an unspecified vulnerability in Marketing ESpot's that could cause a denial of service. IBM X-Force ID: 131779.
CVE-2017-6089
SQL injection vulnerability in PhpCollab 2.5.1 and earlier allows remote attackers to execute arbitrary SQL commands via the (1) project or id parameters to topics/deletetopics.php; the (2) id parameter to bookmarks/deletebookmarks.php; or the (3) id parameter to calendar/deletecalendar.php.
CVE-2017-6090
Unrestricted file upload vulnerability in clients/editclient.php in PhpCollab 2.5.1 and earlier allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in logos_clients/.
CVE-2017-8018
EMC AppSync host plug-in versions 3.5 and below (Windows platform only) includes a denial of service (DoS) vulnerability that could potentially be exploited by malicious users to compromise the affected system.
CVE-2017-8021
EMC Elastic Cloud Storage (ECS) before 3.1 is affected by an undocumented account vulnerability that could potentially be leveraged by malicious users to compromise the affected system.
CVE-2017-9537
Persistent cross-site scripting (XSS) in the Add Node function of SolarWinds Network Performance Monitor version 12.0.15300.90 allows remote attackers to introduce arbitrary JavaScript into various vulnerable parameters.
CVE-2017-9538
The 'Upload logo from external path' function of SolarWinds Network Performance Monitor version 12.0.15300.90 allows remote attackers to cause a denial of service (permanent display of a "Cannot exit above the top directory" error message throughout the entire web application) via a ".." in the path field. In other words, the denial of service is caused by an incorrect implementation of a directory-traversal protection mechanism.
CVE-2017-9797
When an Apache Geode cluster before v1.2.1 is operating in secure mode, an unauthenticated client can enter multi-user authentication mode and send metadata messages. These metadata operations could leak information about application data types. In addition, an attacker could perform a denial of service attack on the cluster.
