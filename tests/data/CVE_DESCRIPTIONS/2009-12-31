CVE-2009-4498
The node_process_command function in Zabbix Server before 1.8 allows remote attackers to execute arbitrary commands via a crafted request.
CVE-2009-4499
SQL injection vulnerability in the get_history_lastid function in the nodewatcher component in Zabbix Server before 1.6.8 allows remote attackers to execute arbitrary SQL commands via a crafted request, possibly related to the send_history_last_id function in zabbix_server/trapper/nodehistory.c.
CVE-2009-4500
The process_trap function in trapper/trapper.c in Zabbix Server before 1.6.6 allows remote attackers to cause a denial of service (crash) via a crafted request with data that lacks an expected : (colon) separator, which triggers a NULL pointer dereference.
CVE-2009-4501
The zbx_get_next_field function in libs/zbxcommon/str.c in Zabbix Server before 1.6.8 allows remote attackers to cause a denial of service (crash) via a request that lacks expected separators, which triggers a NULL pointer dereference, as demonstrated using the Command keyword.
CVE-2009-4502
The NET_TCP_LISTEN function in net.c in Zabbix Agent before 1.6.7, when running on FreeBSD or Solaris, allows remote attackers to bypass the EnableRemoteCommands setting and execute arbitrary commands via shell metacharacters in the argument to net.tcp.listen.  NOTE: this attack is limited to attacks from trusted IP addresses.
CVE-2009-4512
Directory traversal vulnerability in index.php in Oscailt 3.3, when Use Friendly URL's is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the obj_id parameter.
CVE-2009-4513
Multiple cross-site scripting (XSS) vulnerabilities in the Workflow module 5.x before 5.x-2.4 and 6.x before 6.x-1.2, a module for Drupal, allow remote authenticated users, with "administer workflow" privileges, to inject arbitrary web script or HTML via the name of a (1) workflow or (2) workflow state.
CVE-2009-4514
Cross-site scripting (XSS) vulnerability in the OpenSocial Shindig-Integrator module 5.x and 6.x before 6.x-2.1, a module for Drupal, allows remote authenticated users, with "create application" privileges, to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4515
The Storm module 6.x before 6.x-1.25 for Drupal does not enforce privilege requirements for storminvoiceitem nodes, which allows remote attackers to read node titles via unspecified vectors.
CVE-2009-4516
Cross-site scripting (XSS) vulnerability in the FAQ Ask module 5.x and 6.x before 6.x-2.0, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4517
Cross-site request forgery (CSRF) vulnerability in the FAQ Ask module 5.x and 6.x before 6.x-2.0, a module for Drupal, allows remote attackers to hijack the authentication of arbitrary users for requests that access unpublished content.
CVE-2009-4518
Cross-site scripting (XSS) vulnerability in the Insert Node module 5.x before 5.x-1.2 for Drupal allows remote attackers to inject arbitrary web script or HTML via an inserted node.
CVE-2009-4519
Multiple unspecified vulnerabilities in Ortro before 1.3.4 have unknown impact and attack vectors.
CVE-2009-4520
The CCK Comment Reference module 5.x before 5.x-1.2 and 6.x before 6.x-1.3, a module for Drupal, allows remote attackers to bypass intended access restrictions and read comments by using the autocomplete path.
CVE-2009-4521
Cross-site scripting (XSS) vulnerability in birt-viewer/run in Eclipse Business Intelligence and Reporting Tools (BIRT) before 2.5.0, as used in KonaKart and other products, allows remote attackers to inject arbitrary web script or HTML via the __report parameter.
CVE-2009-4522
Cross-site scripting (XSS) vulnerability in search.5.html in BloofoxCMS 0.3.5 allows remote attackers to inject arbitrary web script or HTML via the search parameter to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-4523
Cross-site scripting (XSS) vulnerability in index.php in Zainu 1.0 allows remote attackers to inject arbitrary web script or HTML via the searchSongKeyword parameter in a SearchSong action.
CVE-2009-4524
Cross-site scripting (XSS) vulnerability in the RealName module 6.x-1.x before 6.x-1.3 for Drupal allows remote attackers to inject arbitrary web script or HTML via a realname (aka real name) element.
CVE-2009-4525
Cross-site scripting (XSS) vulnerability in the Print (aka Printer, e-mail and PDF versions) module 5.x before 5.x-4.9 and 6.x before 6.x-1.9, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via crafted data in a list of links.
CVE-2009-4526
The Send by e-mail sub-module in the Print (aka Printer, e-mail and PDF versions) module 5.x before 5.x-4.9 and 6.x before 6.x-1.9, a module for Drupal, does not properly enforce privilege requirements, which allows remote attackers to read page titles by requesting a "Send to friend" form.
CVE-2009-4527
The Shibboleth authentication module 5.x before 5.x-3.4 and 6.x before 6.x-3.2, a module for Drupal, does not properly remove statically granted privileges after a logout or other session change, which allows physically proximate attackers to gain privileges by using an unattended web browser.
CVE-2009-4528
The Organic Groups (OG) Vocabulary module 6.x before 6.x-1.0 for Drupal allows remote authenticated group members to bypass intended access restrictions, and create, modify, or read a vocabulary, via unspecified vectors.
CVE-2009-4529
InterVations NaviCOPA Web Server 3.0.1.2 and earlier allows remote attackers to obtain the source code for a web page via a trailing encoded space character in a URI, as demonstrated by /index.html%20 and /index.php%20 URIs.
CVE-2009-4530
Mongoose 2.8.0 and earlier allows remote attackers to obtain the source code for a web page by appending ::$DATA to the URI.
CVE-2009-4531
httpdx 1.4.4 and earlier allows remote attackers to obtain the source code for a web page by appending a . (dot) character to the URI.
CVE-2009-4532
Cross-site scripting (XSS) vulnerability in the Webform module 5.x before 5.x-2.8 and 6.x before 6.x-2.8, a module for Drupal, allows remote authenticated users, with webform creation privileges, to inject arbitrary web script or HTML via a field label.
CVE-2009-4533
The Webform module 5.x before 5.x-2.8 and 6.x before 6.x-2.8, a module for Drupal, does not prevent caching of a page that contains token placeholders for a default value, which allows remote attackers to read session variables via unspecified vectors.
CVE-2009-4534
Open redirect vulnerability in the FAQ Ask module 5.x and 6.x before 6.x-2.0, a module for Drupal, allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2009-4535
Mongoose 2.8.0 and earlier allows remote attackers to obtain the source code for a web page by appending a / (slash) character to the URI.
