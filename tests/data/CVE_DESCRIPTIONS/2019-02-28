CVE-2018-12388
Mozilla developers and community members reported memory safety bugs present in Firefox 62. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Firefox < 63.
CVE-2018-12389
Mozilla developers and community members reported memory safety bugs present in Firefox ESR 60.2. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Firefox ESR < 60.3 and Thunderbird < 60.3.
CVE-2018-12390
Mozilla developers and community members reported memory safety bugs present in Firefox 62 and Firefox ESR 60.2. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Firefox < 63, Firefox ESR < 60.3, and Thunderbird < 60.3.
CVE-2018-12391
During HTTP Live Stream playback on Firefox for Android, audio data can be accessed across origins in violation of security policies. Because the problem is in the underlying Android service, this issue is addressed by treating all HLS streams as cross-origin and opaque to access. *Note: this issue only affects Firefox for Android. Desktop versions of Firefox are unaffected.*. This vulnerability affects Firefox < 63, Firefox ESR < 60.3, and Thunderbird < 60.3.
CVE-2018-12392
When manipulating user events in nested loops while opening a document through script, it is possible to trigger a potentially exploitable crash due to poor event handling. This vulnerability affects Firefox < 63, Firefox ESR < 60.3, and Thunderbird < 60.3.
CVE-2018-12393
A potential vulnerability was found in 32-bit builds where an integer overflow during the conversion of scripts to an internal UTF-16 representation could result in allocating a buffer too small for the conversion. This leads to a possible out-of-bounds write. *Note: 64-bit builds are not vulnerable to this issue.*. This vulnerability affects Firefox < 63, Firefox ESR < 60.3, and Thunderbird < 60.3.
CVE-2018-12395
By rewriting the Host: request headers using the webRequest API, a WebExtension can bypass domain restrictions through domain fronting. This would allow access to domains that share a host that are otherwise restricted. This vulnerability affects Firefox ESR < 60.3 and Firefox < 63.
CVE-2018-12396
A vulnerability where a WebExtension can run content scripts in disallowed contexts following navigation or other events. This allows for potential privilege escalation by the WebExtension on sites where content scripts should not be run. This vulnerability affects Firefox ESR < 60.3 and Firefox < 63.
CVE-2018-12397
A WebExtension can request access to local files without the warning prompt stating that the extension will "Access your data for all websites" being displayed to the user. This allows extensions to run content scripts in local pages without permission warnings when a local file is opened. This vulnerability affects Firefox ESR < 60.3 and Firefox < 63.
CVE-2018-12398
By using the reflected URL in some special resource URIs, such as chrome:, it is possible to inject stylesheets and bypass Content Security Policy (CSP). This vulnerability affects Firefox < 63.
CVE-2018-12399
When a new protocol handler is registered, the API accepts a title argument which can be used to mislead users about which domain is registering the new protocol. This may result in the user approving a protocol handler that they otherwise would not have. This vulnerability affects Firefox < 63.
CVE-2018-12400
In private browsing mode on Firefox for Android, favicons are cached in the cache/icons folder as they are in non-private mode. This allows information leakage of sites visited during private browsing sessions. *Note: this issue only affects Firefox for Android. Desktop versions of Firefox are unaffected.*. This vulnerability affects Firefox < 63.
CVE-2018-12401
Some special resource URIs will cause a non-exploitable crash if loaded with optional parameters following a '?' in the parsed string. This could lead to denial of service (DOS) attacks. This vulnerability affects Firefox < 63.
CVE-2018-12402
The internal WebBrowserPersist code does not use correct origin context for a resource being saved. This manifests when sub-resources are loaded as part of "Save Page As..." functionality. For example, a malicious page could recover a visitor's Windows username and NTLM hash by including resources otherwise unreachable to the malicious page, if they can convince the visitor to save the complete web page. Similarly, SameSite cookies are sent on cross-origin requests when the "Save Page As..." menu item is selected to save a page, which can result in saving the wrong version of resources based on those cookies. This vulnerability affects Firefox < 63.
CVE-2018-12403
If a site is loaded over a HTTPS connection but loads a favicon resource over HTTP, the mixed content warning is not displayed to users. This vulnerability affects Firefox < 63.
CVE-2018-12405
Mozilla developers and community members reported memory safety bugs present in Firefox 63 and Firefox ESR 60.3. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Thunderbird < 60.4, Firefox ESR < 60.4, and Firefox < 64.
CVE-2018-12406
Mozilla developers and community members reported memory safety bugs present in Firefox 63. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Firefox < 64.
CVE-2018-12407
A buffer overflow occurs when drawing and validating elements with the ANGLE graphics library, used for WebGL content, when working with the VertexBuffer11 module. This results in a potentially exploitable crash. This vulnerability affects Firefox < 64.
CVE-2018-18492
A use-after-free vulnerability can occur after deleting a selection element due to a weak reference to the select element in the options collection. This results in a potentially exploitable crash. This vulnerability affects Thunderbird < 60.4, Firefox ESR < 60.4, and Firefox < 64.
CVE-2018-18493
A buffer overflow can occur in the Skia library during buffer offset calculations with hardware accelerated canvas 2D actions due to the use of 32-bit calculations instead of 64-bit. This results in a potentially exploitable crash. This vulnerability affects Thunderbird < 60.4, Firefox ESR < 60.4, and Firefox < 64.
CVE-2018-18494
A same-origin policy violation allowing the theft of cross-origin URL entries when using the Javascript location property to cause a redirection to another site using performance.getEntries(). This is a same-origin policy violation and could allow for data theft. This vulnerability affects Thunderbird < 60.4, Firefox ESR < 60.4, and Firefox < 64.
CVE-2018-18495
WebExtension content scripts can be loaded into about: pages in some circumstances, in violation of the permissions granted to extensions. This could allow an extension to interfere with the loading and usage of these pages and use capabilities that were intended to be restricted from extensions. This vulnerability affects Firefox < 64.
CVE-2018-18496
When the RSS Feed preview about:feeds page is framed within another page, it can be used in concert with scripted content for a clickjacking attack that confuses users into downloading and executing an executable file from a temporary directory. *Note: This issue only affects Windows operating systems. Other operating systems are not affected.*. This vulnerability affects Firefox < 64.
CVE-2018-18497
Limitations on the URIs allowed to WebExtensions by the browser.windows.create API can be bypassed when a pipe in the URL field is used within the extension to load multiple pages as a single argument. This could allow a malicious WebExtension to open privileged about: or file: locations. This vulnerability affects Firefox < 64.
CVE-2018-18498
A potential vulnerability leading to an integer overflow can occur during buffer size calculations for images when a raw value is used instead of the checked value. This leads to a possible out-of-bounds write. This vulnerability affects Thunderbird < 60.4, Firefox ESR < 60.4, and Firefox < 64.
CVE-2018-18499
A same-origin policy violation allowing the theft of cross-origin URL entries when using a meta http-equiv="refresh" on a page to cause a redirection to another site using performance.getEntries(). This is a same-origin policy violation and could allow for data theft. This vulnerability affects Firefox < 62, Firefox ESR < 60.2, and Thunderbird < 60.2.1.
