CVE-2015-0718
Cisco NX-OS 4.0 through 6.1 on Nexus 1000V 3000, 4000, 5000, 6000, and 7000 devices and Unified Computing System (UCS) platforms allows remote attackers to cause a denial of service (TCP stack reload) by sending crafted TCP packets to a device that has a TIME_WAIT TCP session, aka Bug ID CSCub70579.
CVE-2015-6260
Cisco NX-OS 7.1(1)N1(1) on Nexus 5500, 5600, and 6000 devices does not properly validate PDUs in SNMP packets, which allows remote attackers to cause a denial of service (SNMP application restart) via a crafted packet, aka Bug ID CSCut84645.
CVE-2015-7490
IBM InfoSphere Information Server 8.5 through FP3, 8.7 through FP2, 9.1 through 9.1.2.0, 11.3 through 11.3.1.2, and 11.5 allows remote authenticated users to bypass intended access restrictions via a modified cookie.
CVE-2016-0227
Cross-site scripting (XSS) vulnerability in the document-list control implementation in IBM Business Process Manager (BPM) 8.0 through 8.0.1.3, 8.5.0 through 8.5.0.2, and 8.5.5 and 8.5.6 through 8.5.6.2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0702
The MOD_EXP_CTIME_COPY_FROM_PREBUF function in crypto/bn/bn_exp.c in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g does not properly consider cache-bank access times during modular exponentiation, which makes it easier for local users to discover RSA keys by running a crafted application on the same Intel Sandy Bridge CPU core as a victim and leveraging cache-bank conflicts, aka a "CacheBleed" attack.
CVE-2016-0705
Double free vulnerability in the dsa_priv_decode function in crypto/dsa/dsa_ameth.c in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via a malformed DSA private key.
<a href="http://cwe.mitre.org/data/definitions/415.html" rel="nofollow">CWE-415: Double Free</a>
CVE-2016-0797
Multiple integer overflows in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g allow remote attackers to cause a denial of service (heap memory corruption or NULL pointer dereference) or possibly have unspecified other impact via a long digit string that is mishandled by the (1) BN_dec2bn or (2) BN_hex2bn function, related to crypto/bn/bn.h and crypto/bn/bn_print.c.
<a href="http://cwe.mitre.org/data/definitions/190.html">CWE-190: Integer Overflow or Wraparound</a>
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-0798
Memory leak in the SRP_VBASE_get_by_user implementation in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g allows remote attackers to cause a denial of service (memory consumption) by providing an invalid username in a connection attempt, related to apps/s_server.c and crypto/srp/srp_vfy.c.
CVE-2016-0799
The fmtstr function in crypto/bio/b_print.c in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g improperly calculates string lengths, which allows remote attackers to cause a denial of service (overflow and out-of-bounds read) or possibly have unspecified other impact via a long string, as demonstrated by a large amount of ASN.1 data, a different vulnerability than CVE-2016-2842.
CVE-2016-1158
Cross-site request forgery (CSRF) vulnerability on Corega CG-WLBARGMH and CG-WLBARGNL devices allows remote attackers to hijack the authentication of administrators for requests that perform administrative functions.
CVE-2016-1288
The HTTPS Proxy feature in Cisco AsyncOS before 8.5.3-051 and 9.x before 9.0.0-485 on Web Security Appliance (WSA) devices allows remote attackers to cause a denial of service (service outage) by leveraging certain intranet connectivity and sending a malformed HTTPS request, aka Bug ID CSCuu24840.
CVE-2016-1329
Cisco NX-OS 6.0(2)U6(1) through 6.0(2)U6(5) on Nexus 3000 devices and 6.0(2)A6(1) through 6.0(2)A6(5) and 6.0(2)A7(1) on Nexus 3500 devices has hardcoded credentials, which allows remote attackers to obtain root privileges via a (1) TELNET or (2) SSH session, aka Bug ID CSCuy25800.
CVE-2016-1354
Cross-site scripting (XSS) vulnerability in Cisco Unified Communications Domain Manager (UCDM) 8.x before 8.1.1 allows remote attackers to inject arbitrary web script or HTML via crafted markup data, aka Bug ID CSCud41176.
CVE-2016-1355
Cross-site scripting (XSS) vulnerability in the Device Management UI in the management interface in Cisco FireSIGHT System Software 6.1.0 allows remote attackers to inject arbitrary web script or HTML via a crafted value, aka Bug ID CSCuy41687.
CVE-2016-1356
Cisco FireSIGHT System Software 6.1.0 does not use a constant-time algorithm for verifying credentials, which makes it easier for remote attackers to enumerate valid usernames by measuring timing differences, aka Bug ID CSCuy41615.
CVE-2016-1357
The password-management administration component in Cisco Policy Suite (CPS) 7.0.1.3, 7.0.2, 7.0.2-att, 7.0.3-att, 7.0.4-att, and 7.5.0 allows remote attackers to bypass intended RBAC restrictions and read unspecified data via unknown vectors, aka Bug ID CSCut85211.
CVE-2016-1358
Cisco Prime Infrastructure 2.2, 3.0, and 3.1(0.0) allows remote authenticated users to read arbitrary files or cause a denial of service via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue, aka Bug ID CSCuw81497.
CVE-2016-1359
Cisco Prime Infrastructure 3.0 allows remote authenticated users to execute arbitrary code via a crafted HTTP request that is mishandled during viewing of a log file, aka Bug ID CSCuw81494.
CVE-2016-2842
The doapr_outch function in crypto/bio/b_print.c in OpenSSL 1.0.1 before 1.0.1s and 1.0.2 before 1.0.2g does not verify that a certain memory allocation succeeds, which allows remote attackers to cause a denial of service (out-of-bounds write or memory consumption) or possibly have unspecified other impact via a long string, as demonstrated by a large amount of ASN.1 data, a different vulnerability than CVE-2016-0799.
CVE-2016-8000
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-0800.  Reason: This candidate is a duplicate of CVE-2016-0800.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2016-0800 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
