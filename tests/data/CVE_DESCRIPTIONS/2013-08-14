CVE-2013-2078
Xen 4.0.2 through 4.0.4, 4.1.x, and 4.2.x allows local PV guest users to cause a denial of service (hypervisor crash) via certain bit combinations to the XSETBV instruction.
CVE-2013-2126
Multiple double free vulnerabilities in the LibRaw::unpack function in libraw_cxx.cpp in LibRaw before 0.15.2 allow context-dependent attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a malformed full-color (1) Foveon or (2) sRAW image file.
CVE-2013-2127
Buffer overflow in the exposure correction code in LibRaw before 0.15.1 allows context-dependent attackers to cause a denial of service (crash) and possibly execute arbitrary code via unspecified vectors.
CVE-2013-3175
Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT allow remote attackers to execute arbitrary code via a malformed asynchronous RPC request, aka "Remote Procedure Call Vulnerability."
CVE-2013-3181
usp10.dll in the Unicode Scripts Processor in Microsoft Windows XP SP2 and SP3 and Windows Server 2003 SP2 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "Uniscribe Font Parsing Engine Memory Corruption Vulnerability."
CVE-2013-3182
The Windows NAT Driver (aka winnat) service in Microsoft Windows Server 2012 does not properly validate memory addresses during the processing of ICMP packets, which allows remote attackers to cause a denial of service (memory corruption and system hang) via crafted packets, aka "Windows NAT Denial of Service Vulnerability."
CVE-2013-3183
The TCP/IP implementation in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly perform memory allocation for inbound ICMPv6 packets, which allows remote attackers to cause a denial of service (system hang) via crafted packets, aka "ICMPv6 Vulnerability."
CVE-2013-3184
Microsoft Internet Explorer 7 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3185
Microsoft Active Directory Federation Services (AD FS) 1.x through 2.1 on Windows Server 2003 R2 SP2, Windows Server 2008 SP2 and R2 SP1, and Windows Server 2012 allows remote attackers to obtain sensitive information about the service account, and possibly conduct account-lockout attacks, by connecting to an endpoint, aka "AD FS Information Disclosure Vulnerability."
CVE-2013-3186
The Protected Mode feature in Microsoft Internet Explorer 7 through 10 on Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly implement the Integrity Access Level (aka IL) protection mechanism, which allows remote attackers to obtain medium-integrity privileges by leveraging access to a low-integrity process, aka "Process Integrity Level Assignment Vulnerability."
CVE-2013-3187
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3191 and CVE-2013-3193.
CVE-2013-3188
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3189.
CVE-2013-3189
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3188.
CVE-2013-3190
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3191
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3187 and CVE-2013-3193.
CVE-2013-3192
Cross-site scripting (XSS) vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to inject arbitrary web script or HTML via crafted character sequences with EUC-JP encoding, aka "EUC-JP Character Encoding Vulnerability."
CVE-2013-3193
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3187 and CVE-2013-3191.
CVE-2013-3194
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3196
The NT Virtual DOS Machine (NTVDM) subsystem in the kernel in Microsoft Windows XP SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, Windows 7 SP1, and Windows 8 on 32-bit platforms does not properly validate kernel-memory addresses, which allows local users to gain privileges or cause a denial of service (memory corruption) via a crafted application, aka "Windows Kernel Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3197 and CVE-2013-3198.
CVE-2013-3197
The NT Virtual DOS Machine (NTVDM) subsystem in the kernel in Microsoft Windows XP SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, Windows 7 SP1, and Windows 8 on 32-bit platforms does not properly validate kernel-memory addresses, which allows local users to gain privileges or cause a denial of service (memory corruption) via a crafted application, aka "Windows Kernel Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3196 and CVE-2013-3198.
CVE-2013-3198
The NT Virtual DOS Machine (NTVDM) subsystem in the kernel in Microsoft Windows XP SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, Windows 7 SP1, and Windows 8 on 32-bit platforms does not properly validate kernel-memory addresses, which allows local users to gain privileges or cause a denial of service (memory corruption) via a crafted application, aka "Windows Kernel Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3196 and CVE-2013-3197.
CVE-2013-3199
Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-4879
SQL injection vulnerability in core/inc/bigtree/cms.php in BigTree CMS 4.0 RC2 and earlier allows remote attackers to execute arbitrary SQL commands via the PATH_INFO to index.php.
CVE-2013-4880
Cross-site scripting (XSS) vulnerability in core/admin/modules/developer/modules/views/add.php in BigTree CMS 4.0 RC2 and earlier allows remote attackers to inject arbitrary web script or HTML via the module parameter.
CVE-2013-5120
SQL injection vulnerability in PHPFox before 3.6.0 (build4) allows remote attackers to execute arbitrary SQL commands via the search[gender] parameter to user/browse/view_/.
CVE-2013-5121
SQL injection vulnerability in PHPFox before 3.6.0 (build6) allows remote attackers to execute arbitrary SQL commands via the search[sort_by] parameter to user/browse/view_/.
