CVE-2017-10868
H2O version 2.2.2 and earlier allows remote attackers to cause a denial of service in the server via specially crafted HTTP/1 header.
CVE-2017-10869
Buffer overflow in H2O version 2.2.2 and earlier allows remote attackers to cause a denial-of-service in the server via unspecified vectors.
CVE-2017-10872
H2O version 2.2.3 and earlier allows remote attackers to cause a denial of service in the server via unspecified vectors.
CVE-2017-10907
Directory traversal vulnerability in OneThird CMS Show Off v1.85 and earlier. Show Off v1.85 en and earlier allows an attacker to read arbitrary files via unspecified vectors.
CVE-2017-10908
H2O version 2.2.3 and earlier allows remote attackers to cause a denial of service in the server via specially crafted HTTP/2 header.
CVE-2017-10909
Untrusted search path vulnerability in Music Center for PC version 1.0.01 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2017-15307
Huawei Honor 8 smartphone with software versions earlier than FRD-L04C567B389 and earlier than FRD-L14C567B389 have a permission control vulnerability due to improper authorization configuration on specific device information.
CVE-2017-15308
Huawei iReader app before 8.0.2.301 has an input validation vulnerability due to insufficient validation on the URL used for loading network data. An attacker can control app access and load malicious websites created by the attacker, and the code in webpages would be loaded and run.
CVE-2017-15309
Huawei iReader app before 8.0.2.301 has a path traversal vulnerability due to insufficient validation on file storage paths. An attacker can exploit this vulnerability to store downloaded malicious files in an arbitrary directory.
CVE-2017-15310
Huawei iReader app before 8.0.2.301 has an arbitrary file deletion vulnerability due to the lack of input validation. An attacker can exploit this vulnerability to delete specific files from the SD card.
CVE-2017-15311
The baseband modules of Mate 10, Mate 10 Pro, Mate 9, Mate 9 Pro Huawei smart phones with software before ALP-AL00 8.0.0.120(SP2C00), before BLA-AL00 8.0.0.120(SP2C00), before MHA-AL00B 8.0.0.334(C00), and before LON-AL00B 8.0.0.334(C00) have a stack overflow vulnerability due to the lack of parameter validation. An attacker could send malicious packets to the smart phones within radio range by special wireless device, which leads stack overflow when the baseband module handles these packets. The attacker could exploit this vulnerability to perform a denial of service attack or remote code execution in baseband module.
CVE-2017-15312
Huawei SmartCare V200R003C10 has a stored XSS (cross-site scripting) vulnerability in the dashboard module. A remote authenticated attacker could exploit this vulnerability to inject malicious scripts in the affected device.
CVE-2017-15313
Huawei SmartCare V200R003C10 has a CSV injection vulnerability. An remote authenticated attacker could inject malicious CSV expression to the affected device.
CVE-2017-15316
The GPU driver of Mate 9 Huawei smart phones with software before MHA-AL00B 8.0.0.334(C00) and Mate 9 Pro Huawei smart phones with software before LON-AL00B 8.0.0.334(C00) has a memory double free vulnerability. An attacker tricks a user into installing a malicious application, and the application can call special API, which triggers double free and causes a system crash or arbitrary code execution.
CVE-2017-15317
AR120-S V200R006C10, V200R007C00, V200R008C20, V200R008C30; AR1200 V200R006C10, V200R006C13, V200R007C00, V200R007C01, V200R007C02, V200R008C20, V200R008C30; AR1200-S V200R006C10, V200R007C00, V200R008C20, V200R008C30; AR150 V200R006C10, V200R007C00, V200R007C01, V200R007C02, V200R008C20, V200R008C30; AR150-S V200R006C10, V200R007C00, V200R008C20, V200R008C30; AR160 V200R006C10, V200R006C12, V200R007C00, V200R007C01, V200R007C02, V200R008C20, V200R008C30; AR200 V200R006C10, V200R007C00, V200R007C01, V200R008C20, V200R008C30; AR200-S V200R006C10, V200R007C00, V200R008C20, V200R008C30; AR2200 V200R006C10, V200R006C13, V200R006C16, V200R007C00, V200R007C01, V200R007C02, V200R008C20, V200R008C30; AR2200-S V200R006C10, V200R007C00, V200R008C20, V200R008C30; AR3200 V200R006C10, V200R006C11, V200R007C00, V200R007C01, V200R007C02, V200R008C00, V200R008C10, V200R008C20, V200R008C30; AR510 V200R006C10, V200R006C12, V200R006C13, V200R006C15, V200R006C16, V200R006C17, V200R007C00, V200R008C20, V200R008C30; SRG1300 V200R006C10, V200R007C00, V200R007C02, V200R008C20, V200R008C30; SRG2300 V200R006C10, V200R007C00, V200R007C02, V200R008C20, V200R008C30; SRG3300 V200R006C10, V200R007C00, V200R008C20, V200R008C30 have an input validation vulnerability in Huawei multiple products. Due to the insufficient input validation, an unauthenticated, remote attacker may craft a malformed Stream Control Transmission Protocol (SCTP) packet and send it to the device, causing the device to read out of bounds and restart.
CVE-2017-15318
RP200 V500R002C00, V600R006C00; TE30 V100R001C10, V500R002C00, V600R006C00; TE40 V500R002C00, V600R006C00; TE50 V500R002C00, V600R006C00; TE60 V100R001C10, V500R002C00, V600R006C00 have an out-of-bounds read vulnerabilities in some Huawei products. Due to insufficient input validation, a remote attacker could exploit these vulnerabilities by sending specially crafted SS7 related packets to the target devices. Successful exploit will cause out-of-bounds read and possibly crash the system.
CVE-2017-15319
RP200 V500R002C00, V600R006C00; TE30 V100R001C10, V500R002C00, V600R006C00; TE40 V500R002C00, V600R006C00; TE50 V500R002C00, V600R006C00; TE60 V100R001C10, V500R002C00, V600R006C00 have an out-of-bounds read vulnerabilities in some Huawei products. Due to insufficient input validation, a remote attacker could exploit these vulnerabilities by sending specially crafted SS7 related packets to the target devices. Successful exploit will cause out-of-bounds read and possibly crash the system.
CVE-2017-15320
RP200 V500R002C00, V600R006C00; TE30 V100R001C10, V500R002C00, V600R006C00; TE40 V500R002C00, V600R006C00; TE50 V500R002C00, V600R006C00; TE60 V100R001C10, V500R002C00, V600R006C00 have an out-of-bounds read vulnerabilities in some Huawei products. Due to insufficient input validation, a remote attacker could exploit these vulnerabilities by sending specially crafted SS7 related packets to the target devices. Successful exploit will cause out-of-bounds read and possibly crash the system.
CVE-2017-15321
Huawei FusionSphere OpenStack V100R006C000SPC102 (NFV) has an information leak vulnerability due to the use of a low version transmission protocol by default. An attacker could intercept packets transferred by a target device. Successful exploit could cause an information leak.
CVE-2017-15322
Some Huawei smartphones with software of BGO-L03C158B003CUSTC158D001 and BGO-L03C331B009CUSTC331D001 have a DoS vulnerability due to insufficient input validation. An attacker could exploit this vulnerability by sending specially crafted NFC messages to the target device. Successful exploit could make a service crash.
CVE-2017-15324
Huawei S5700 and S6700 with software of V200R005C00 have a DoS vulnerability due to insufficient validation of the Network Quality Analysis (NQA) packets. A remote attacker could exploit this vulnerability by sending malformed NQA packets to the target device. Successful exploitation could make the device restart.
CVE-2017-15328
Huawei HG8245H version earlier than V300R018C00SPC110 has an authentication bypass vulnerability. An attacker can access a specific URL of the affect product. Due to improper verification of the privilege, successful exploitation may cause information leak.
CVE-2017-16727
A Credentials Management issue was discovered in Moxa NPort W2150A versions prior to 1.11, and NPort W2250A versions prior to 1.11. The default password is empty on the device. An unauthorized user can access the device without a password. An unauthorized user has the ability to completely compromise the confidentiality and integrity of the wireless traffic.
CVE-2017-16766
An improper access control vulnerability in synodsmnotify in Synology DiskStation Manager (DSM) before 6.1.4-15217 and before 6.0.3-8754-6 allows local users to inject arbitrary web script or HTML via the -fn option.
