CVE-2009-4925
Multiple SQL injection vulnerabilities in Portale e-commerce Creasito (aka creasito e-commerce content manager) 1.3.16, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the username parameter to (1) admin/checkuser.php and (2) checkuser.php.
CVE-2009-4926
Multiple cross-site scripting (XSS) vulnerabilities in Online Contact Manager (formerly EContact PRO) 3.0 allow remote attackers to inject arbitrary web script or HTML via the (1) showGroup parameter to (a) index.php and the (2) id parameter to (b) view.php, (c) email.php, (d) edit.php, and (e) delete.php.
CVE-2009-4927
WB News 2.1.2 allows remote attackers to bypass authentication and gain administrative access via a modified WBNEWS cookie, as demonstrated by setting this cookie to 1.
CVE-2009-4928
PHP remote file inclusion vulnerability in config.php in TotalCalendar 2.4 allows remote attackers to execute arbitrary PHP code via a URL in the inc_dir parameter, a different vector than CVE-2006-1922 and CVE-2006-7055.
CVE-2009-4929
admin/manage_users.php in TotalCalendar 2.4 does not require administrative authentication, which allows remote attackers to change arbitrary passwords via the newPW1 and newPW2 parameters.
CVE-2009-4930
Cross-site scripting (XSS) vulnerability in the twbkwbis.P_SecurityQuestion (aka Change Security Question) page in SunGard Banner Student System 7.4 allows remote attackers to inject arbitrary web script or HTML via the New Question field.
CVE-2009-4931
Stack-based buffer overflow in Groovy Media Player 1.1.0 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long string in a .m3u playlist file.
CVE-2009-4932
Stack-based buffer overflow in 1by1 1.67 (aka 1.6.7.0) allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long string in a .m3u playlist file.
CVE-2009-4933
Multiple SQL injection vulnerabilities in login.php in EZ Webitor allow remote attackers to execute arbitrary SQL commands via the (1) txtUserId (Username) and (2) txtPassword (Password) parameters.  NOTE: some of these details are obtained from third party information.
CVE-2009-4934
Cross-site scripting (XSS) vulnerability in index.php in Online Photo Pro 2.0 allows remote attackers to inject arbitrary web script or HTML via the section parameter.
CVE-2009-4935
SQL injection vulnerability in ogp_show.php in Online Guestbook Pro allows remote attackers to execute arbitrary SQL commands via the display parameter.
CVE-2010-0832
pam_motd (aka the MOTD module) in libpam-modules before 1.1.0-2ubuntu1.1 in PAM on Ubuntu 9.10 and libpam-modules before 1.1.1-2ubuntu5 in PAM on Ubuntu 10.04 LTS allows local users to change the ownership of arbitrary files via a symlink attack on .cache in a user's home directory, related to "user file stamps" and the motd.legal-notice file.
CVE-2010-2448
znc.cpp in ZNC before 0.092 allows remote authenticated users to cause a denial of service (crash) by requesting traffic statistics when there is an active unauthenticated connection, which triggers a NULL pointer dereference, as demonstrated using (1) a traffic link in the web administration pages or (2) the traffic command in the /znc shell.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2010-2489
Buffer overflow in Ruby 1.9.x before 1.9.1-p429 on Windows might allow local users to gain privileges via a crafted ARGF.inplace_mode value that is not properly handled when constructing the filenames of the backup files.
CVE-2010-2680
Directory traversal vulnerability in the JExtensions JE Section/Property Finder (jesectionfinder) component for Joomla! allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the view parameter to index.php.
CVE-2010-2681
PHP remote file inclusion vulnerability in the SEF404x (com_sef) component for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig.absolute.path parameter to index.php.
CVE-2010-2682
Directory traversal vulnerability in the Realtyna Translator (com_realtyna) component 1.0.15 for Joomla! allows remote attackers to read arbitrary files and possibly have unspecified other impact via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-2683
SQL injection vulnerability in result.php in Customer Paradigm PageDirector CMS allows remote attackers to execute arbitrary SQL commands via the sub_catid parameter.
CVE-2010-2684
SQL injection vulnerability in index.php in Customer Paradigm PageDirector CMS allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-2685
siteadmin/adduser.php in Customer Paradigm PageDirector CMS does not properly restrict access, which allows remote attackers to bypass intended restrictions and add administrative users via a direct request.
CVE-2010-2686
Multiple SQL injection vulnerabilities in clientes.asp in the TopManage OLK module 1.91.30 for SAP allow remote attackers to execute arbitrary SQL commands via the (1) PriceFrom, (2) PriceTo, and (3) InvFrom parameters, as reachable from olk/c_p/searchCart.asp, and other unspecified vectors when performing an advanced search.  NOTE: some of these details are obtained from third party information.
CVE-2010-2687
SQL injection vulnerability in printdetail.asp in Site2Nite Boat Classifieds allows remote attackers to execute arbitrary SQL commands via the Id parameter.
CVE-2010-2688
SQL injection vulnerability in detail.asp in Site2Nite Boat Classifieds allows remote attackers to execute arbitrary SQL commands via the ID parameter.
CVE-2010-2689
SQL injection vulnerability in cont_form.php in Internet DM WebDM CMS allows remote attackers to execute arbitrary SQL commands via the cf_id parameter.
CVE-2010-2690
SQL injection vulnerability in the JOOFORGE Gamesbox (com_gamesbox) component 1.0.2, and possibly earlier, for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a consoles action to index.php.
CVE-2010-2691
Multiple SQL injection vulnerabilities in 2daybiz Custom T-Shirt Design Script allow remote attackers to execute arbitrary SQL commands via the (1) sbid parameter to products_details.php, (2) pid parameter to products/products.php, and (3) designid parameter to designview.php.
CVE-2010-2692
Cross-site scripting (XSS) vulnerability in 2daybiz Custom T-Shirt Design Script allows remote attackers to inject arbitrary web script or HTML via a review comment.
CVE-2010-2694
SQL injection vulnerability in the redSHOP Component (com_redshop) 1.0 for Joomla! allows remote attackers to execute arbitrary SQL commands via the pid parameter to index.php.
CVE-2010-2695
Directory traversal vulnerability in the SFTP/SSH2 virtual server in Xlight FTP Server 3.5.0, 3.5.5, and possibly other versions before 3.6 allows remote authenticated users to read, overwrite, or delete arbitrary files via .. (dot dot) sequences in the (1) ls, (2) rm, (3) rename, and other unspecified commands.
CVE-2010-2696
SQL injection vulnerability in gallery/index.php in Sijio Community Software allows remote attackers to execute arbitrary SQL commands via the parent parameter.
CVE-2010-2697
Cross-site scripting (XSS) vulnerability in Sijio Community Software allows remote authenticated users to inject arbitrary web script or HTML via the title parameter when adding a new blog, related to edit_blog/index.php.  NOTE: some of these details are obtained from third party information.
CVE-2010-2698
Multiple cross-site scripting (XSS) vulnerabilities in Sijio Community Software allow remote authenticated users to inject arbitrary web script or HTML via the title parameter when (1) editing a new blog, (2) adding an album, or (3) editing an album.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-2699
SQL injection vulnerability in index.php in Edge PHP Clickbank Affiliate Marketplace Script (CBQuick) allows remote attackers to execute arbitrary SQL commands via the search parameter.
CVE-2010-2700
Cross-site scripting (XSS) vulnerability in index.php in Edge PHP Clickbank Affiliate Marketplace Script (CBQuick) allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2010-2701
Multiple buffer overflows in the FathFTP ActiveX control 1.7 allow remote attackers to execute arbitrary code via (1) the GetFromURL member or (2) a long argument to the RasIsConnected method.
CVE-2010-2702
Buffer overflow in the UGameEngine::UpdateConnectingMessage function in the Unreal engine 1, 2, and 2.5, as used in multiple games including Unreal Tournament 2004, Unreal tournament 2003, Postal 2, Raven Shield, and SWAT4, when downloads are enabled, allows remote attackers to execute arbitrary code via a long LEVEL field in a WELCOME response to a download request.
