CVE-2011-4133
Cross-site request forgery (CSRF) vulnerability in Moodle 1.9.x before 1.9.11 allows remote attackers to hijack the authentication of unspecified victims for requests that modify an RSS feed in an RSS block.
CVE-2011-4278
Cross-site scripting (XSS) vulnerability in the tag autocomplete functionality in Moodle 1.9.x before 1.9.11 and 2.0.x before 2.0.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-4279
Moodle 2.0.x before 2.0.2 does not use the forceloginforprofiles setting for course-profiles access control, which makes it easier for remote attackers to obtain potentially sensitive information via vectors involving use of a search engine, as demonstrated by the search functionality of Google, Yahoo!, Wrensoft Zoom, MSN, Yandex, and AltaVista.
CVE-2011-4280
Cross-site scripting (XSS) vulnerability in the Spike PHPCoverage (aka spikephpcoverage) library, as used in Moodle 2.0.x before 2.0.2 and other products, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-4281
Multiple cross-site request forgery (CSRF) vulnerabilities in Moodle 2.0.x before 2.0.2 allow remote attackers to hijack the authentication of arbitrary users for requests that mark the completion of (1) an activity or (2) a course.
CVE-2011-4282
Multiple cross-site scripting (XSS) vulnerabilities in the course-tags functionality in tag/coursetags_more.php in Moodle 2.0.x before 2.0.2 allow remote attackers to inject arbitrary web script or HTML via the (1) sort or (2) show parameter.
CVE-2011-4283
Moodle 1.9.x before 1.9.11 and 2.0.x before 2.0.2 places an IMS enterprise enrolment file in the course-files area, which allows remote attackers to obtain sensitive information via a request for imsenterprise-enrol.xml.
CVE-2011-4284
Moodle 2.0.x before 2.0.2 allows remote attackers to obtain sensitive information from a myprofile (aka My profile) block by visiting a user-context page.
CVE-2011-4285
The default configuration of Moodle 2.0.x before 2.0.2 has an incorrect setting of the moodle/course:delete capability, which allows remote authenticated users to delete arbitrary courses by leveraging the teacher role.
CVE-2011-4286
Multiple cross-site scripting (XSS) vulnerabilities in the media-filter implementation in filter/mediaplugin/filter.php in Moodle 1.9.x before 1.9.11 and 2.0.x before 2.0.2 allow remote attackers to inject arbitrary web script or HTML via vectors involving (1) Flash Video (aka FLV) files and (2) YouTube videos.
CVE-2011-4287
admin/uploaduser_form.php in Moodle 2.0.x before 2.0.3 does not force password changes for autosubscribed users, which makes it easier for remote attackers to obtain access by leveraging knowledge of the initial password of a new user.
CVE-2011-4288
Moodle 1.9.x before 1.9.12 and 2.0.x before 2.0.3 does not properly implement associations between teachers and groups, which allows remote authenticated users to read quiz reports of arbitrary students by leveraging the teacher role.
CVE-2011-4289
Moodle 2.0.x before 2.0.3 does not recognize the configuration setting that makes e-mail addresses visible only to course members, which allows remote authenticated users to obtain sensitive address information by reading a full profile page.
CVE-2011-4290
Multiple cross-site scripting (XSS) vulnerabilities in lib/weblib.php in Moodle 1.9.x before 1.9.12 allow remote attackers to inject arbitrary web script or HTML via vectors related to URL encoding.
CVE-2011-4291
Moodle 2.0.x before 2.0.3 allows remote authenticated users to cause a denial of service (invalid database records) via a series of crafted ratings operations.
CVE-2011-4292
Moodle 2.0.x before 2.0.3 allows remote authenticated users to cause a denial of service (invalid database records) via a series of crafted comments operations.
CVE-2011-4293
The theme implementation in Moodle 2.0.x before 2.0.4 and 2.1.x before 2.1.1 triggers duplicate caching of Cascading Style Sheets (CSS) and JavaScript content, which allows remote attackers to bypass intended access restrictions and write to an operating-system temporary directory via unspecified vectors.
CVE-2011-4294
The error-message functionality in Moodle 1.9.x before 1.9.13, 2.0.x before 2.0.4, and 2.1.x before 2.1.1 does not ensure that a continuation link refers to an http or https URL for the local Moodle instance, which might allow attackers to trick users into visiting arbitrary web sites via unspecified vectors.
CVE-2011-4295
The moodle_enrol_external:role_assign function in enrol/externallib.php in Moodle 2.0.x before 2.0.4 and 2.1.x before 2.1.1 does not have an authorization check, which allows remote authenticated users to gain privileges by making a role assignment.
CVE-2011-4296
lib/db/access.php in Moodle 2.0.x before 2.0.4 and 2.1.x before 2.1.1 assigns incorrect capabilities to the course-creator role, which allows remote authenticated users to modify course filters by leveraging this role.
CVE-2011-4297
comment/lib.php in Moodle 2.0.x before 2.0.4 and 2.1.x before 2.1.1 does not properly restrict comment capabilities, which allows remote attackers to post a comment by leveraging the guest role and operating on a front-page activity.
CVE-2012-2021
Multiple cross-site scripting (XSS) vulnerabilities in HP AssetManager 5.20, 5.21, 5.22, and 9.30 allow remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2282
EMC Celerra Network Server 6.x before 6.0.61.0, VNX 7.x before 7.0.53.2, and VNXe 2.0 and 2.1 before 2.1.3.19077 (aka MR1 SP3.2) and 2.2 before 2.2.0.19078 (aka MR2 SP0.2) do not properly implement NFS access control, which allows remote authenticated users to read or modify files via a (1) NFSv2, (2) NFSv3, or (3) NFSv4 request.
CVE-2012-2607
The Johnson Controls CK721-A controller with firmware before SSM4388_03.1.0.14_BB allows remote attackers to perform arbitrary actions via crafted packets to TCP port 41014 (aka the download port).
CVE-2012-2645
The Yahoo! Japan Yahoo! Browser application 1.2.0 and earlier for Android does not properly implement the WebView class, which allows remote attackers to obtain sensitive information via a crafted application.
CVE-2012-4026
The Johnson Controls Pegasys P2000 server with software before 3.11 allows remote attackers to trigger false alerts via crafted packets to TCP port 41013 (aka the upload port), a different vulnerability than CVE-2012-2607.
CVE-2012-4027
Directory traversal vulnerability in Tridium Niagara AX Framework allows remote attackers to read files outside of the intended images, nav, and px folders by leveraging incorrect permissions, as demonstrated by reading the config.bog file.
CVE-2012-4028
Tridium Niagara AX Framework does not properly store credential data, which allows context-dependent attackers to bypass intended access restrictions by using the stored information for authentication.
