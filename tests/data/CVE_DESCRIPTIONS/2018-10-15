CVE-2017-5934
Cross-site scripting (XSS) vulnerability in the link dialogue in GUI editor in MoinMoin before 1.9.10 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2018-12154
Denial of Service in Unified Shader Compiler in Intel Graphics Drivers before 10.18.x.5056 (aka 15.33.x.5056), 10.18.x.5057 (aka 15.36.x.5057) and 20.19.x.5058 (aka 15.40.x.5058) may allow an unprivileged user to potentially create an infinite loop and crash an application via local access.
CVE-2018-15378
A vulnerability in ClamAV versions prior to 0.100.2 could allow an attacker to cause a denial of service (DoS) condition. The vulnerability is due to an error related to the MEW unpacker within the "unmew11()" function (libclamav/mew.c), which can be exploited to trigger an invalid read memory access via a specially crafted EXE file.
CVE-2018-15538
Agentejo Cockpit has multiple Cross-Site Scripting vulnerabilities.
CVE-2018-15539
Agentejo Cockpit lacks an anti-CSRF protection mechanism. Thus, an attacker is able to change API tokens, passwords, etc.
CVE-2018-15540
Agentejo Cockpit performs actions on files without appropriate validation and therefore allows an attacker to traverse the file system to unintended locations and/or access arbitrary files, aka /media/api Directory Traversal.
CVE-2018-15590
An issue was discovered in Ivanti Workspace Control before 10.3.0.0 and RES One Workspace, when file and folder security are configured. A local authenticated user can bypass file and folder security restriction by leveraging an unspecified attack vector.
CVE-2018-15591
An issue was discovered in Ivanti Workspace Control before 10.3.10.0 and RES One Workspace. A local authenticated user can bypass Application Whitelisting restrictions to execute arbitrary code by leveraging multiple unspecified attack vectors.
CVE-2018-15592
An issue was discovered in Ivanti Workspace Control before 10.3.10.0 and RES One Workspace. A local authenticated user can execute processes with elevated privileges via an unspecified attack vector.
CVE-2018-15593
An issue was discovered in Ivanti Workspace Control before 10.3.10.0 and RES One Workspace. A local authenticated user can decrypt the encrypted datastore or relay server password by leveraging an unspecified attack vector.
CVE-2018-1744
IBM Security Key Lifecycle Manager 2.5, 2.6, 2.7, and 3.0 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system. IBM X-Force ID: 148423.
CVE-2018-1747
IBM Security Key Lifecycle Manager 2.5, 2.6, 2.7, and 3.0 is vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 148428.
CVE-2018-17532
Teltonika RUT9XX routers with firmware before 00.04.233 are prone to multiple unauthenticated OS command injection vulnerabilities in autologin.cgi and hotspotlogin.cgi due to insufficient user input sanitization. This allows remote attackers to execute arbitrary commands with root privileges.
CVE-2018-17533
Teltonika RUT9XX routers with firmware before 00.05.01.1 are prone to cross-site scripting vulnerabilities in hotspotlogin.cgi due to insufficient user input sanitization.
CVE-2018-17534
Teltonika RUT9XX routers with firmware before 00.04.233 provide a root terminal on a serial interface without proper access control. This allows attackers with physical access to execute arbitrary commands with root privileges.
CVE-2018-17961
Artifex Ghostscript 9.25 and earlier allows attackers to bypass a sandbox protection mechanism via vectors involving errorhandler setup. NOTE: this issue exists because of an incomplete fix for CVE-2018-17183.
CVE-2018-17980
NoMachine before 5.3.27 and 6.x before 6.3.6 allows attackers to gain privileges via a Trojan horse wintab32.dll file located in the same directory as a .nxs file, as demonstrated by a scenario where the .nxs file and the DLL are in the current working directory, and the Trojan horse code is executed. (The directory could, in general, be on a local filesystem or a network share.).
CVE-2018-18073
Artifex Ghostscript allows attackers to bypass a sandbox protection mechanism by leveraging exposure of system operators in the saved execution stack in an error object.
CVE-2018-18259
Stored XSS has been discovered in version 1.0.12 of the LUYA CMS software via /admin/api-cms-nav/create-page.
CVE-2018-18260
In the 2.4 version of Camaleon CMS, Stored XSS has been discovered. The profile image in the User settings section can be run in the update / upload area via /admin/media/upload?actions=false.
CVE-2018-18296
MetInfo 6.1.2 has XSS via the /admin/index.php bigclass parameter in an n=column&a=doadd action.
CVE-2018-18309
An issue was discovered in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.31. An invalid memory address dereference was discovered in read_reloc in reloc.c. The vulnerability causes a segmentation fault and application crash, which leads to denial of service, as demonstrated by objdump, because of missing _bfd_clear_contents bounds checking.
CVE-2018-18310
An invalid memory address dereference was discovered in dwfl_segment_report_module.c in libdwfl in elfutils through v0.174. The vulnerability allows attackers to cause a denial of service (application crash) with a crafted ELF file, as demonstrated by consider_notes.
CVE-2018-18315
com/mossle/cdn/CdnController.java in lemon 1.9.0 allows attackers to upload arbitrary files because the copyMultipartFileToFile method in CdnUtils only checks for a ../ substring, and does not validate the file type and spaceName parameter.
CVE-2018-18316
emlog v6.0.0 has CSRF via the admin/user.php?action=new URI.
CVE-2018-18317
DESHANG DSCMS 1.1 has CSRF via the public/index.php/admin/admin/add.html URI.
CVE-2018-18318
The /dev/block/mmcblk0rpmb driver kernel module on Qiku 360 Phone N6 Pro 1801-A01 devices allows attackers to cause a denial of service (NULL pointer dereference and device crash) via a crafted 0xc0d8b300 ioctl call.
CVE-2018-18319
** DISPUTED ** An issue was discovered in the Merlin.PHP component 0.6.6 for Asuswrt-Merlin devices. An attacker can execute arbitrary commands because api.php has an eval call, as demonstrated by the /6/api.php?function=command&class=remote&Cc='ls' URI. NOTE: the vendor indicates that Merlin.PHP is designed only for use on a trusted intranet network, and intentionally allows remote code execution.
CVE-2018-18320
** DISPUTED ** An issue was discovered in the Merlin.PHP component 0.6.6 for Asuswrt-Merlin devices. An attacker can execute arbitrary commands because exec.php has a popen call. NOTE: the vendor indicates that Merlin.PHP is designed only for use on a trusted intranet network, and intentionally allows remote code execution.
CVE-2018-18322
CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.480 has Command Injection via shell metacharacters in the admin/index.php service_start, service_restart, service_fullstatus, or service_stop parameter.
CVE-2018-18323
CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.480 has Local File Inclusion via directory traversal with an admin/index.php?module=file_editor&file=/../ URI.
CVE-2018-18324
CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.480 has XSS via the admin/fileManager2.php fm_current_dir parameter, or the admin/index.php module, service_start, service_fullstatus, service_restart, service_stop, or file (within the file_editor) parameter.
CVE-2018-18361
An issue was discovered in nc-cms through 2017-03-10. index.php?action=edit_html allows XSS via the name parameter, as demonstrated by a value beginning with home_content and containing a crafted SRC attribute of an IMG element.
