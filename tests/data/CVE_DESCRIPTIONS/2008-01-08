CVE-2007-0066
The kernel in Microsoft Windows 2000 SP4, XP SP2, and Server 2003, when ICMP Router Discovery Protocol (RDP) is enabled, allows remote attackers to cause a denial of service via fragmented router advertisement ICMP packets that trigger an out-of-bounds read, aka "Windows Kernel TCP/IP/ICMP Vulnerability."
CVE-2007-0069
Unspecified vulnerability in the kernel in Microsoft Windows XP SP2, Server 2003, and Vista allows remote attackers to cause a denial of service (CPU consumption) and possibly execute arbitrary code via crafted (1) IGMPv3 and (2) MLDv2 packets that trigger memory corruption, aka "Windows Kernel TCP/IP/IGMPv3 and MLDv2 Vulnerability."
CVE-2007-5352
Unspecified vulnerability in Local Security Authority Subsystem Service (LSASS) in Microsoft Windows 2000 SP4, XP SP2, and Server 2003 SP1 and SP2 allows local users to gain privileges via a crafted local procedure call (LPC) request.
CVE-2007-5360
Buffer overflow in OpenPegasus Management server, when compiled to use PAM and with PEGASUS_USE_PAM_STANDALONE_PROC defined, as used in VMWare ESX Server 3.0.1 and 3.0.2, might allow remote attackers to execute arbitrary code via vectors related to PAM authentication, a different vulnerability than CVE-2008-0003.
CVE-2007-5965
QSslSocket in Trolltech Qt 4.3.0 through 4.3.2 does not properly verify SSL certificates, which might make it easier for remote attackers to trick a user into accepting an invalid server certificate for a spoofed service, or trick a service into accepting an invalid client certificate for a user.
CVE-2007-6388
Cross-site scripting (XSS) vulnerability in mod_status in the Apache HTTP Server 2.2.0 through 2.2.6, 2.0.35 through 2.0.61, and 1.3.2 through 1.3.39, when the server-status page is enabled, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-6421
Cross-site scripting (XSS) vulnerability in balancer-manager in mod_proxy_balancer in the Apache HTTP Server 2.2.0 through 2.2.6 allows remote attackers to inject arbitrary web script or HTML via the (1) ss, (2) wr, or (3) rr parameters, or (4) the URL.
CVE-2007-6422
The balancer_handler function in mod_proxy_balancer in the Apache HTTP Server 2.2.0 through 2.2.6, when a threaded Multi-Processing Module is used, allows remote authenticated users to cause a denial of service (child process crash) via an invalid bb variable.
CVE-2007-6668
admin/uploadgames.php in MySpace Content Zone (MCZ) 3.x does not require administrative privileges, which allows remote attackers to perform unrestricted file uploads, as demonstrated by uploading (1) a .php file and (2) a .php%00.jpeg file.
CVE-2007-6669
Cross-site scripting (XSS) vulnerability in search.php in PHCDownload 1.1.0 allows remote attackers to inject arbitrary web script or HTML via the string parameter.
CVE-2007-6670
SQL injection vulnerability in search.php in PHCDownload 1.1.0 allows remote attackers to execute arbitrary SQL commands via the string parameter.
CVE-2007-6671
SQL injection vulnerability in login_form.asp in Instant Softwares Dating Site allows remote attackers to execute arbitrary SQL commands via the Password parameter, a different product than CVE-2006-6021. NOTE: some of these details are obtained from third party information.
CVE-2007-6672
Mortbay Jetty 6.1.5 and 6.1.6 allows remote attackers to bypass protection mechanisms and read the source of files via multiple '/' (slash) characters in the URI.
CVE-2007-6673
Cross-site scripting (XSS) vulnerability in Makale Scripti allows remote attackers to inject arbitrary web script or HTML via the ara parameter to the default URI under Ara/ in a search action.
CVE-2007-6674
Cross-site scripting (XSS) vulnerability in Default.asp in RapidShare Database allows remote attackers to inject arbitrary web script or HTML via the Arayalim parameter.
CVE-2007-6675
The b_system_comments_show function in htdocs/modules/system/blocks/system_blocks.php in XOOPS before 2.0.18 does not check permissions, which allows remote attackers to read the comments in restricted modules.
CVE-2007-6676
The default configuration of Uber Uploader (UU) 5.3.6 and earlier does not block uploads of (1) .html, (2) .asp, and other possibly dangerous extensions, which allows remote attackers to use these extensions in uploads via (a) uu_file_upload.php, related to uu_file_upload.js and (b) uber_uploader_file.php, related to uber_uploader_file.js, a different issue than CVE-2007-0123.  NOTE: the vendor disputes the severity of the issue, noting that it is the administrator's responsibility to "add file extensions that you may or may not want uploaded."
CVE-2008-0003
Stack-based buffer overflow in the PAMBasicAuthenticator::PAMCallback function in OpenPegasus CIM management server (tog-pegasus), when compiled to use PAM and without PEGASUS_USE_PAM_STANDALONE_PROC defined, might allow remote attackers to execute arbitrary code via unknown vectors, a different vulnerability than CVE-2007-5360.
CVE-2008-0093
Multiple cross-site scripting (XSS) vulnerabilities in newticket.php in eTicket 1.5.5.2, and 1.5.6 RC2 and RC3, allow remote attackers to inject arbitrary web script or HTML via the (1) Name and (2) Subject parameters.
CVE-2008-0094
Multiple directory traversal vulnerabilities in MODx Content Management System 0.9.6.1 allow remote attackers to (1) include and execute arbitrary local files via a .. (dot dot) in the as_language parameter to assets/snippets/AjaxSearch/AjaxSearch.php, reached through index-ajax.php; and (2) read arbitrary local files via a .. (dot dot) in the file parameter to assets/js/htcmime.php.
CVE-2008-0095
The SIP channel driver in Asterisk Open Source 1.4.x before 1.4.17, Business Edition before C.1.0-beta8, AsteriskNOW before beta7, Appliance Developer Kit before Asterisk 1.4 revision 95946, and Appliance s800i 1.0.x before 1.0.3.4 allows remote attackers to cause a denial of service (daemon crash) via a BYE message with an Also (Also transfer) header, which triggers a NULL pointer dereference.
CVE-2008-0096
Multiple buffer overflows in Georgia SoftWorks SSH2 Server (GSW_SSHD) 7.01.0003 and earlier allow remote attackers to execute arbitrary code via a (1) a long username, which triggers an overflow in the log function; or (2) a long password.
CVE-2008-0097
Format string vulnerability in the log function in Georgia SoftWorks SSH2 Server (GSW_SSHD) 7.01.0003 and earlier allows remote attackers to execute arbitrary code via format string specifiers in the username field, as demonstrated by a certain LoginPassword message.
CVE-2008-0098
Buffer overflow in RealPlayer 11 build 6.0.14.748 allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: As of 20080103, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2008-0099
Multiple SQL injection vulnerabilities in MyPHP Forum 3.0 and earlier allow remote attackers to execute arbitrary SQL commands via the searchtext parameter to search.php, and unspecified other vectors.
CVE-2008-0100
Stack-based buffer overflow in the Scene::errorf function in Scene.cpp in White_Dune 0.29 beta791 and earlier allows remote attackers to execute arbitrary code via a long string in a .WRL file.
CVE-2008-0101
Format string vulnerability in the swDebugf function in DuneApp.cpp in White_Dune 0.29 beta791 and earlier allows remote attackers to execute arbitrary code via format string specifiers in a .WRL file.
CVE-2008-0129
SQL injection vulnerability in starnet/addons/slideshow_full.php in Site@School 2.3.10 and earlier allows remote attackers to execute arbitrary SQL commands via the album_name parameter.
CVE-2008-0130
SQL injection vulnerability in login_form.asp in Instant Softwares Dating Site allows remote attackers to execute arbitrary SQL commands via the Username parameter, a different vulnerability than CVE-2007-6671.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0131
Cross-site scripting (XSS) vulnerability in login_form.asp in Instant Softwares Dating Site allows remote attackers to inject arbitrary web script or HTML via the msg parameter, a different product than CVE-2006-6022.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0132
Pragma FortressSSH 5.0 Build 4 Revision 293 and earlier handles long input to sshd.exe by creating an error-message window and waiting for the administrator to click in this window before terminating the sshd.exe process, which allows remote attackers to cause a denial of service (connection slot exhaustion) via a flood of SSH connections with long data objects, as demonstrated by (1) a long list of keys and (2) a long username.
CVE-2008-0133
Multiple SQL injection vulnerabilities in Tribisur 2.1 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) id parameter to cat_main.php and the (2) cat parameter to forum.php in a liste action.
CVE-2008-0134
Cross-site scripting (XSS) vulnerability in Forums/setup.asp in Snitz Forums 2000 3.4.06 and earlier allows remote attackers to inject arbitrary web script or HTML via the MAIL parameter.
CVE-2008-0135
Snitz Forums 2000 3.4.06 and earlier stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for forum/snitz_forums_2000.mdb.
CVE-2008-0136
Snitz Forums 2000 3.4.05 allows remote attackers to obtain sensitive information via a direct request to forum/whereami.asp, which reveals the database path.
CVE-2008-0137
PHP remote file inclusion vulnerability in config.inc.php in SNETWORKS PHP CLASSIFIEDS 5.0 allows remote attackers to execute arbitrary PHP code via a URL in the path_escape parameter.
CVE-2008-0138
PHP remote file inclusion vulnerability in xoopsgallery/init_basic.php in the mod_gallery module for XOOPS, when register_globals is disabled, allows remote attackers to execute arbitrary PHP code via a URL in the GALLERY_BASEDIR parameter.
CVE-2008-0139
Eval injection vulnerability in loudblog/inc/parse_old.php in Loudblog 0.8.0 and earlier allows remote attackers to execute arbitrary PHP code via the template parameter.
CVE-2008-0140
Directory traversal vulnerability in error.php in Uebimiau Webmail 2.7.10 and 2.7.2 allows remote authenticated users to read arbitrary files via a .. (dot dot) in the selected_theme parameter, a different vector than CVE-2007-3172.
CVE-2008-0141
actions.php in WebPortal CMS 0.6-beta generates predictable passwords containing only the time of day, which makes it easier for remote attackers to obtain access to any account via a lostpass action.
CVE-2008-0142
Multiple SQL injection vulnerabilities in WebPortal CMS 0.6-beta allow remote attackers to execute arbitrary SQL commands via the user_name parameter to actions.php, and unspecified other vectors.
CVE-2008-0143
PHP remote file inclusion vulnerability in common/db.php in samPHPweb, possibly 4.2.2 and others, as provided with SAM Broadcaster, allows remote attackers to execute arbitrary PHP code via a URL in the commonpath parameter.
CVE-2008-0144
PHP remote file inclusion vulnerability in index.php in NetRisk 1.9.7 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.  NOTE: this can also be leveraged for local file inclusion using directory traversal sequences.
CVE-2008-0145
Unspecified vulnerability in glob in PHP before 4.4.8, when open_basedir is enabled, has unknown impact and attack vectors.  NOTE: this issue reportedly exists because of a regression related to CVE-2007-4663.
CVE-2008-0146
Cross-site scripting (XSS) vulnerability in the error page in W3-mSQL allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to the top-level URI.
