CVE-2007-3336
Multiple "pointer overwrite" vulnerabilities in Ingres database server 2006 9.0.4, r3, 2.6, and 2.5, as used in multiple CA (formerly Computer Associates) products, allow remote attackers to execute arbitrary code by sending certain TCP data at different times to the Ingres Communications Server Process (iigcc), which calls the (1) QUinsert or (2) QUremove functions with attacker-controlled input.
CVE-2007-3337
wakeup in Ingres database server 2006 9.0.4, r3, 2.6, and 2.5, as used in multiple CA (Computer Associates) products, allows local users to truncate arbitrary files via a symlink attack on the alarmwkp.def file.
CVE-2007-3338
Multiple stack-based buffer overflows in Ingres database server 2006 9.0.4, r3, 2.6, and 2.5, as used in multiple CA (Computer Associates) products, allow remote attackers to execute arbitrary code via the (1) uuid_from_char or (2) duve_get_args functions.
CVE-2007-3343
Cross-site scripting (XSS) vulnerability in RaidenHTTPD before 2.0.14 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-3344
Multiple cross-site scripting (XSS) vulnerabilities in netjukebox 4.01b allow remote attackers to inject arbitrary web script or HTML via the (1) album_id, (2) order, (3) sort, (4) filter, and (5) genre_id parameters to (a) index.php; and the (6) url parameter to (b) ridirect.php.  NOTE: the attack also reveals the installation path.
CVE-2007-3345
Multiple SQL injection vulnerabilities in index.php in PHPAccounts 0.5 allow remote attackers to execute arbitrary SQL commands via the (1) Outgoing_Type_ID, (2) Outgoing_ID, (3) Project_ID, (4) Client_ID, (5) Invoice_ID, or (6) Vendor_ID parameter.
CVE-2007-3346
Directory traversal vulnerability in index.php in PHPAccounts 0.5 allows remote attackers to include arbitrary local files via unspecified manipulations of the page parameter.
CVE-2007-3347
The D-Link DPH-540/DPH-541 phone accepts SIP INVITE messages that are not from the Call Server's IP address, which allows remote attackers to engage in arbitrary SIP communication with the phone, as demonstrated by communication with forged caller ID.
CVE-2007-3348
The D-Link DPH-540/DPH-541 phone allows remote attackers to cause a denial of service (device outage) via a malformed SDP header in a SIP INVITE message.
CVE-2007-3349
The Aastra 9112i SIP Phone with firmware 1.4.0.1048 and boot version 1.1.0.10 allows remote attackers to (1) cause a denial of service (device freeze) via a malformed SIP message of a certain length or (2) cause a denial of service (continuous ring) via a malformed SIP message of a certain other length.
CVE-2007-3350
AOL Instant Messenger (AIM) 6.1.32.1 on Windows XP allows remote attackers to cause a denial of service (application hang) via a flood of spoofed SIP INVITE requests.
CVE-2007-3351
The SJPhone SIP soft phone 1.60.303c, when installed on the Dell Axim X3 running Windows Mobile 2003, allows remote attackers to cause a denial of service (device hang and traffic amplification) via a direct crafted INVITE transaction, which causes the phone to transmit many RTP packets.
CVE-2007-3352
Cross-site scripting (XSS) vulnerability in the preview form in Stephen Ostermiller Contact Form before 2.00.02 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors that contain an apostrophe.
CVE-2007-3353
** DISPUTED **  PHP remote file inclusion vulnerability in includes/template.php in MyEvent 1.6 allows remote attackers to execute arbitrary PHP code via a URL in the myevent_path parameter.  NOTE: a reliable third party disputes this issue, saying "the entire file is a class."
CVE-2007-3354
Multiple SQL injection vulnerabilities in NetClassifieds Premium Edition allow remote attackers to execute arbitrary SQL commands via the s_user_id parameter to ViewCat.php and other unspecified vectors. NOTE: the CatID/ViewCat.php, CatID/gallery.php, and ItemNum/ViewItem.php vectors are already covered by CVE-2005-3978.
CVE-2007-3355
Multiple cross-site scripting (XSS) vulnerabilities in NetClassifieds Premium Edition allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-3356
NetClassifieds Premium Edition allows remote attackers to obtain sensitive information via certain requests that reveal the path in an error message, related to the display_errors setting in (1) Common.php and (2) imageresizer.php, and (3) the use of __FILE__ in error reporting by imageresizer.php; and (4) via certain requests that reveal the table name and complete query, related to the Halt_On_Error setting in Mysql_db.php.
CVE-2007-3357
NetClassifieds Premium Edition does not use encryption for (1) stored passwords or (2) sensitive data, which might allow attackers to obtain information via certain vectors.
CVE-2007-3358
PHP remote file inclusion vulnerability in html/load_lang.php in SerWeb 0.9.6 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the _SERWEB[serwebdir] parameter.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-3359
Multiple PHP remote file inclusion vulnerabilities in SerWeb 0.9.6 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the _SERWEB[serwebdir] parameter to (1) html/load_apu.php or (2) html/mail_prepend.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-3360
hook.c in BitchX 1.1-final allows remote IRC servers to execute arbitrary commands by sending a client certain data containing NICK and EXEC strings, which exceeds the bounds of a hash table, and injects an EXEC hook function that receives and executes shell commands.
CVE-2007-3361
The Nortel PC Client SIP Soft Phone 4.1 3.5.208[20051015] allows remote attackers to cause a denial of service (device crash) via a SIP message with a malformed header.
CVE-2007-3362
ageet AGEphone before 1.6.2, running on Windows Mobile 5 on the HTC HyTN Pocket PC device, allows remote attackers to (1) cause a denial of service (call disruption and device hang) via a SIP message with a malformed header and (2) cause a denial of service (call disruption, false ring indication, and device outage) via a SIP message with a malformed SDP delimiter.
CVE-2007-3363
Multiple unspecified vulnerabilities in ageet AGEphone before 1.6.3 allow remote attackers to have an unknown impact via malformed SIP packets.
CVE-2007-3364
Cross-site scripting (XSS) vulnerability in the cgi-bin/post.mscgi sample page in MyServer 0.8.9 allows remote attackers to inject arbitrary web script or HTML via the body content.
CVE-2007-3365
MyServer 0.8.9 and earlier does not properly handle uppercase characters in filename extensions, which allows remote attackers to obtain sensitive information (script source code) via a modified extension, as demonstrated by post.mscgI.
CVE-2007-3366
Cross-site scripting (XSS) vulnerability in Simple CGI Wrapper (scgiwrap) in cPanel before 10.9.1, and 11.x before 11.4.19-R14378, allows remote attackers to inject arbitrary web script or HTML via the URI.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3367
Simple CGI Wrapper (scgiwrap) in cPanel before 10.9.1, and 11.x before 11.4.19-R14378, allows remote attackers to obtain sensitive information via a direct request, which reveals the path in an error message.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3368
Buffer overflow in the HTTP server on the Polycom SoundPoint IP 601 SIP phone with BootROM 3.0.x+ allows remote attackers to cause a denial of service (device reboot) via a malformed CGI parameter.
CVE-2007-3369
Buffer overflow in the Polycom SoundPoint IP 601 SIP phone with BootROM 3.0.x+ and SIP version 1.6.3.0067 allows remote attackers to cause a denial of service (device hang or reboot) via an INVITE message with a long Via header.
CVE-2007-3370
Multiple PHP remote file inclusion vulnerabilities in Sun Board 1.00.00 Alpha allow remote attackers to execute arbitrary PHP code via a URL in (1) the sunPath parameter to include.php or (2) the dir parameter to skin/board/default/doctype.php.
CVE-2007-3371
PHP remote file inclusion vulnerability in plugins/widgets/htmledit/htmledit.php in Powl 0.94 allows remote attackers to execute arbitrary PHP code via a URL in the _POWL[installPath] parameter.
CVE-2007-3372
The Avahi daemon in Avahi before 0.6.20 allows attackers to cause a denial of service (exit) via empty TXT data over D-Bus, which triggers an assert error.
