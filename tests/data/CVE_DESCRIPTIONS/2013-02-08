CVE-2012-2686
crypto/evp/e_aes_cbc_hmac_sha1.c in the AES-NI functionality in the TLS 1.1 and 1.2 implementations in OpenSSL 1.0.1 before 1.0.1d allows remote attackers to cause a denial of service (application crash) via crafted CBC data.
CVE-2012-4700
Multiple buffer overflows in an ActiveX control in PE3DO32A.ocx in IntegraXor SCADA Server 4.00 build 4250.0 and earlier allow remote attackers to execute arbitrary code via a crafted HTML document.
CVE-2013-0166
OpenSSL before 0.9.8y, 1.0.0 before 1.0.0k, and 1.0.1 before 1.0.1d does not properly perform signature verification for OCSP responses, which allows remote OCSP servers to cause a denial of service (NULL pointer dereference and application crash) via an invalid key.
CVE-2013-0169
The TLS protocol 1.1 and 1.2 and the DTLS protocol 1.0 and 1.2, as used in OpenSSL, OpenJDK, PolarSSL, and other products, do not properly consider timing side-channel attacks on a MAC check requirement during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, aka the "Lucky Thirteen" issue.
Per http://www.openssl.org/news/vulnerabilities.html:
Fixed in OpenSSL 1.0.1d (Affected 1.0.1c, 1.0.1b, 1.0.1a, 1.0.1) 
Fixed in OpenSSL 1.0.0k (Affected 1.0.0j, 1.0.0i, 1.0.0g, 1.0.0f, 1.0.0e, 1.0.0d, 1.0.0c, 1.0.0b, 1.0.0a, 1.0.0) 
Fixed in OpenSSL 0.9.8y (Affected 0.9.8x, 0.9.8w, 0.9.8v, 0.9.8u, 0.9.8t, 0.9.8s, 0.9.8r, 0.9.8q, 0.9.8p, 0.9.8o, 0.9.8n, 0.9.8m, 0.9.8l, 0.9.8k, 0.9.8j, 0.9.8i, 0.9.8h, 0.9.8g, 0.9.8f, 0.9.8d, 0.9.8c, 0.9.8b, 0.9.8a, 0.9.8)
CVE-2013-0170
Use-after-free vulnerability in the virNetMessageFree function in rpc/virnetserverclient.c in libvirt 1.0.x before 1.0.2, 0.10.2 before 0.10.2.3, 0.9.11 before 0.9.11.9, and 0.9.6 before 0.9.6.4 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code by triggering certain errors during an RPC connection, which causes a message to be freed without being removed from the message queue.
CVE-2013-0189
cachemgr.cgi in Squid 3.1.x and 3.2.x, possibly 3.1.22, 3.2.4, and other versions, allows remote attackers to cause a denial of service (resource consumption) via a crafted request.  NOTE: this issue is due to an incorrect fix for CVE-2012-5643, possibly involving an incorrect order of arguments or incorrect comparison.
Per http://www.ubuntu.com/usn/USN-1713-1/
A security issue affects these releases of Ubuntu and its derivatives:
Ubuntu 12.10
Ubuntu 12.04 LTS
Ubuntu 11.10
Ubuntu 10.04 LTS

CVE-2013-0242
Buffer overflow in the extend_buffers function in the regular expression matcher (posix/regexec.c) in glibc, possibly 2.17 and earlier, allows context-dependent attackers to cause a denial of service (memory corruption and crash) via crafted multibyte characters.
CVE-2013-0262
rack/file.rb (Rack::File) in Rack 1.5.x before 1.5.2 and 1.4.x before 1.4.5 allows attackers to access arbitrary files outside the intended root directory via a crafted PATH_INFO environment variable, probably a directory traversal vulnerability that is remotely exploitable, aka "symlink path traversals."
CVE-2013-0263
Rack::Session::Cookie in Rack 1.5.x before 1.5.2, 1.4.x before 1.4.5, 1.3.x before 1.3.10, 1.2.x before 1.2.8, and 1.1.x before 1.1.6 allows remote attackers to guess the session cookie, gain privileges, and execute arbitrary code via a timing attack involving an HMAC comparison function that does not run in constant time.
CVE-2013-0633
Buffer overflow in Adobe Flash Player before 10.3.183.51 and 11.x before 11.5.502.149 on Windows and Mac OS X, before 10.3.183.51 and 11.x before 11.2.202.262 on Linux, before 11.1.111.32 on Android 2.x and 3.x, and before 11.1.115.37 on Android 4.x allows remote attackers to execute arbitrary code via crafted SWF content, as exploited in the wild in February 2013.
CVE-2013-0634
Adobe Flash Player before 10.3.183.51 and 11.x before 11.5.502.149 on Windows and Mac OS X, before 10.3.183.51 and 11.x before 11.2.202.262 on Linux, before 11.1.111.32 on Android 2.x and 3.x, and before 11.1.115.37 on Android 4.x allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted SWF content, as exploited in the wild in February 2013.
CVE-2013-1465
The Cubecart::_basket method in classes/cubecart.class.php in CubeCart 5.0.0 through 5.2.0 allows remote attackers to unserialize arbitrary PHP objects via a crafted shipping parameter, as demonstrated by modifying the application configuration using the Config object.
CVE-2013-1618
The TLS implementation in Opera before 12.13 does not properly consider timing side-channel attacks on a MAC check operation during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, a related issue to CVE-2013-0169.
CVE-2013-1619
The TLS implementation in GnuTLS before 2.12.23, 3.0.x before 3.0.28, and 3.1.x before 3.1.7 does not properly consider timing side-channel attacks on a noncompliant MAC check operation during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, a related issue to CVE-2013-0169.
CVE-2013-1620
The TLS implementation in Mozilla Network Security Services (NSS) does not properly consider timing side-channel attacks on a noncompliant MAC check operation during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, a related issue to CVE-2013-0169.
CVE-2013-1621
Array index error in the SSL module in PolarSSL before 1.2.5 might allow remote attackers to cause a denial of service via vectors involving a crafted padding-length value during validation of CBC padding in a TLS session, a different vulnerability than CVE-2013-0169.
CVE-2013-1622
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate is not a security issue.  Further investigation showed that, because of RFC noncompliance, no version or configuration of the product had the vulnerability previously associated with this ID. Notes: none.
CVE-2013-1623
The TLS and DTLS implementations in wolfSSL CyaSSL before 2.5.0 do not properly consider timing side-channel attacks on a noncompliant MAC check operation during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, a related issue to CVE-2013-0169.
CVE-2013-1624
The TLS implementation in the Bouncy Castle Java library before 1.48 and C# library before 1.8 does not properly consider timing side-channel attacks on a noncompliant MAC check operation during the processing of malformed CBC padding, which allows remote attackers to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data for crafted packets, a related issue to CVE-2013-0169.
CVE-2013-1637
Opera before 12.13 allows remote attackers to execute arbitrary code via vectors involving DOM events.
CVE-2013-1638
Opera before 12.13 allows remote attackers to execute arbitrary code via crafted clipPaths in an SVG document.
CVE-2013-1639
Opera before 12.13 does not send CORS preflight requests in all required cases, which allows remote attackers to bypass a CSRF protection mechanism via a crafted web site that triggers a CORS request.
