CVE-2014-6362
Use-after-free vulnerability in Microsoft Office 2007 SP3, 2010 SP2, and 2013 Gold and SP1 allows remote attackers to bypass the ASLR protection mechanism via a crafted document, aka "Microsoft Office Component Use After Free Vulnerability."
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-0003
win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to gain privileges or cause a denial of service (NULL pointer dereference) via a crafted application, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2015-0008
The UNC implementation in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not include authentication from the server to the client, which allows remote attackers to execute arbitrary code by making crafted data available on a UNC share, as demonstrated by Group Policy data from a spoofed domain controller, aka "Group Policy Remote Code Execution Vulnerability."
CVE-2015-0009
The Group Policy Security Configuration policy implementation in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows man-in-the-middle attackers to disable a signing requirement and trigger a revert-to-default action by spoofing domain-controller responses, aka "Group Policy Security Feature Bypass Vulnerability."
CVE-2015-0010
The CryptProtectMemory function in cng.sys (aka the Cryptography Next Generation driver) in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1, when the CRYPTPROTECTMEMORY_SAME_LOGON option is used, does not check an impersonation token's level, which allows local users to bypass intended decryption restrictions by leveraging a service that (1) has a named-pipe planting vulnerability or (2) uses world-readable shared memory for encrypted data, aka "CNG Security Feature Bypass Vulnerability" or MSRC ID 20707.
CVE-2015-0012
Microsoft System Center Virtual Machine Manager (VMM) 2012 R2 Update Rollup 4 does not properly validate the roles of users, which allows local users to obtain server and virtual-machine administrative privileges by establishing a server session with Active Directory credentials, aka "Virtual Machine Manager Elevation of Privilege Vulnerability."
CVE-2015-0017
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0020, CVE-2015-0022, CVE-2015-0026, CVE-2015-0030, CVE-2015-0031, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0018
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0037, CVE-2015-0040, and CVE-2015-0066.
CVE-2015-0019
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0020
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0022, CVE-2015-0026, CVE-2015-0030, CVE-2015-0031, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0021
Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0022
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0026, CVE-2015-0030, CVE-2015-0031, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0023
Microsoft Internet Explorer 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0025.
CVE-2015-0025
Microsoft Internet Explorer 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0023.
CVE-2015-0026
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0022, CVE-2015-0030, CVE-2015-0031, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0027
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0035, CVE-2015-0039, CVE-2015-0052, and CVE-2015-0068.
CVE-2015-0028
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0048.
CVE-2015-0029
Microsoft Internet Explorer 6 and 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0030
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0022, CVE-2015-0026, CVE-2015-0031, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0031
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0022, CVE-2015-0026, CVE-2015-0030, CVE-2015-0036, and CVE-2015-0041.
CVE-2015-0035
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0027, CVE-2015-0039, CVE-2015-0052, and CVE-2015-0068.
CVE-2015-0036
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0022, CVE-2015-0026, CVE-2015-0030, CVE-2015-0031, and CVE-2015-0041.
CVE-2015-0037
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0018, CVE-2015-0040, and CVE-2015-0066.
CVE-2015-0038
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0042 and CVE-2015-0046.
CVE-2015-0039
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0027, CVE-2015-0035, CVE-2015-0052, and CVE-2015-0068.
CVE-2015-0040
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0018, CVE-2015-0037, and CVE-2015-0066.
CVE-2015-0041
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0017, CVE-2015-0020, CVE-2015-0022, CVE-2015-0026, CVE-2015-0030, CVE-2015-0031, and CVE-2015-0036.
CVE-2015-0042
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0038 and CVE-2015-0046.
CVE-2015-0043
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0044
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-8967 and CVE-2015-0050.
CVE-2015-0045
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0053.
CVE-2015-0046
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0038 and CVE-2015-0042.
CVE-2015-0048
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0028.
CVE-2015-0049
Microsoft Internet Explorer 8 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0050
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-8967 and CVE-2015-0044.
CVE-2015-0051
Microsoft Internet Explorer 8 allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "Internet Explorer ASLR Bypass Vulnerability."
CVE-2015-0052
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0027, CVE-2015-0035, CVE-2015-0039, and CVE-2015-0068.
CVE-2015-0053
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0045.
CVE-2015-0054
Microsoft Internet Explorer 7 through 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2015-0055
Microsoft Internet Explorer 10 and 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2015-0057
win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to gain privileges via a crafted application, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2015-0058
Double free vulnerability in win32k.sys in the kernel-mode drivers in Microsoft Windows 8.1, Windows Server 2012 R2, and Windows RT 8.1 allows local users to gain privileges via a crafted application, aka "Windows Cursor Object Double Free Vulnerability."
CVE-2015-0059
win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2008 R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to gain privileges via a crafted TrueType font, aka "TrueType Font Parsing Remote Code Execution Vulnerability."
CVE-2015-0060
The font mapper in win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly scale fonts, which allows local users to cause a denial of service (system hang) via a crafted application, aka "Windows Font Driver Denial of Service Vulnerability."
CVE-2015-0061
Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 do not properly initialize memory for TIFF images, which allows remote attackers to obtain sensitive information from process memory via a crafted image file, aka "TIFF Processing Information Disclosure Vulnerability."
CVE-2015-0062
Microsoft Windows Server 2008 R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to gain privileges via a crafted application that leverages incorrect impersonation handling in a process that uses the SeAssignPrimaryTokenPrivilege privilege, aka "Windows Create Process Elevation of Privilege Vulnerability."
CVE-2015-0063
Microsoft Excel 2007 SP3; the proofing tools in Office 2010 SP2; Excel 2010 SP2; Excel 2013 Gold, SP1, and RT; Excel Viewer; and Office Compatibility Pack SP3 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Office document, aka "Excel Remote Code Execution Vulnerability."
CVE-2015-0064
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, Word Automation Services in SharePoint Server 2010, Web Applications 2010 SP2, Word Viewer, and Office Compatibility Pack SP3 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Office document, aka "Office Remote Code Execution Vulnerability."
CVE-2015-0065
Microsoft Word 2007 SP3 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Office document, aka "OneTableDocumentStream Remote Code Execution Vulnerability."
CVE-2015-0066
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0018, CVE-2015-0037, and CVE-2015-0040.
CVE-2015-0067
Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-0068
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-0027, CVE-2015-0035, CVE-2015-0039, and CVE-2015-0052.
CVE-2015-0069
Microsoft Internet Explorer 10 and 11 allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "Internet Explorer ASLR Bypass Vulnerability."
CVE-2015-0070
Microsoft Internet Explorer 6 through 11 allows remote attackers to read content from a different (1) domain or (2) zone via a crafted web site, aka "Internet Explorer Cross-domain Information Disclosure Vulnerability."
CVE-2015-0071
Microsoft Internet Explorer 9 through 11 allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "Internet Explorer ASLR Bypass Vulnerability."
CVE-2015-1172
Unrestricted file upload vulnerability in admin/upload-file.php in the Holding Pattern theme (aka holding_pattern) 0.6 and earlier for WordPress allows remote attackers to execute arbitrary PHP code by uploading a file with a PHP extension, then accessing it via a direct request to the file in an unspecified directory.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-1518
SQL injection vulnerability in the search_post function in includes/search.php in Redaxscript before 2.3.0 allows remote attackers to execute arbitrary SQL commands via the search_terms parameter.
CVE-2015-1575
Multiple cross-site scripting (XSS) vulnerabilities in u5CMS before 3.9.4 allow remote attackers to inject arbitrary web script or HTML via the (1) c, (2) i, (3) l, or (4) p parameter to index.php; the (5) a or (6) b parameter to u5admin/cookie.php; the name parameter to (7) copy.php or (8) delete.php in u5admin/; the (9) f or (10) typ parameter to u5admin/deletefile.php; the (11) n parameter to u5admin/done.php; the (12) c parameter to u5admin/editor.php; the (13) uri parameter to u5admin/meta2.php; the (14) n parameter to u5admin/notdone.php; the (15) newname parameter to u5admin/rename2.php; the (16) l parameter to u5admin/sendfile.php; the (17) s parameter to u5admin/characters.php; the (18) page parameter to u5admin/savepage.php; or the (19) name parameter to u5admin/new2.php.
CVE-2015-1576
Multiple SQL injection vulnerabilities in u5CMS before 3.9.4 allow remote attackers to execute arbitrary SQL commands via the name parameter to (1) copy2.php, (2) localize.php, (3) metai.php, (4) nc.php, (5) new2.php, or (6) rename2.php in u5admin/; (7) c parameter to u5admin/editor.php; (8) typ parameter to u5admin/meta2.php; or (9) newname parameter to u5admin/rename2.php.
CVE-2015-1577
Directory traversal vulnerability in u5admin/deletefile.php in u5CMS before 3.9.4 allows remote attackers to write to arbitrary files via a (1) .. (dot dot) or (2) full pathname in the f parameter.
CVE-2015-1578
Multiple open redirect vulnerabilities in u5CMS before 3.9.4 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the (1) pidvesa cookie to u5admin/pidvesa.php or (2) uri parameter to u5admin/meta2.php.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-1579
Directory traversal vulnerability in the Elegant Themes Divi theme for WordPress allows remote attackers to read arbitrary files via a .. (dot dot) in the img parameter in a revslider_show_image action to wp-admin/admin-ajax.php.  NOTE: this vulnerability may be a duplicate of CVE-2014-9734.
CVE-2015-1580
Multiple cross-site request forgery (CSRF) vulnerabilities in the Redirection Page plugin 1.2 for WordPress allow remote attackers to hijack the authentication of administrators for requests that (1) change plugin settings or conduct cross-site scripting (XSS) attacks via the (2) source or (3) redir parameter in an add action in the redirection-page to wp-admin/options-general.php.
CVE-2015-1581
Multiple cross-site request forgery (CSRF) vulnerabilities in the Mobile Domain plugin 1.5.2 for WordPress allow remote attackers to hijack the authentication of administrators for requests that (1) change plugin settings or conduct cross-site scripting (XSS) attacks via the (2) domain, (3) text, (4) font, (5) fontcolor, (6) color, or (7) padding parameter in an add-domain action in the mobile-domain page to wp-admin/options-general.php.
CVE-2015-1582
Multiple cross-site scripting (XSS) vulnerabilities in the Spider Facebook plugin before 1.0.11 for WordPress allow (1) remote attackers to inject arbitrary web script or HTML via the appid parameter in a registration task to the default URI or remote administrators to inject arbitrary web script or HTML via the (2) asc_or_desc, (3) order_by, (4) page_number, (5) serch_or_not, or (6) search_events_by_title parameter in (a) the Spider_Facebook_manage page to wp-admin/admin.php or a (b) selectpagesforfacebook or (c) selectpostsforfacebook action to wp-admin/admin-ajax.php.
