CVE-2015-0787
XSS in NetIQ Designer for Identity Manager before 4.5.3 allows remote attackers to inject arbitrary HTML code via the accessMgrDN value of the forgotUser.do CGI.
CVE-2016-1000120
SQLi and XSS in Huge IT catalog extension v1.0.4 for Joomla
CVE-2016-1000121
XSS and SQLi in Huge IT Joomla Slider v1.0.9 extension
CVE-2016-1000122
XSS and SQLi in Huge IT Joomla Slider v1.0.9 extension
CVE-2016-1592
XSS in NetIQ Designer for Identity Manager before 4.5.3 allows remote attackers to inject arbitrary HTML code via the nrfEntitlementReport.do CGI.
CVE-2016-1598
XSS in NetIQ IDM 4.5 Identity Applications before 4.5.4 allows attackers able to change their username to inject arbitrary HTML code into the Role Assignment administrator HTML pages.
CVE-2016-5764
Micro Focus Rumba FTP 4.X client buffer overflow makes it possible to corrupt the stack and allow arbitrary code execution. Fixed in: Rumba FTP 4.5 (HF 14668). This can only occur if a client connects to a malicious server.
CVE-2016-6431
A vulnerability in the local Certificate Authority (CA) feature of Cisco ASA Software before 9.6(1.5) could allow an unauthenticated, remote attacker to cause a reload of the affected system. The vulnerability is due to improper handling of crafted packets during the enrollment operation. An attacker could exploit this vulnerability by sending a crafted enrollment request to the affected system. An exploit could allow the attacker to cause the reload of the affected system. Note: Only HTTPS packets directed to the Cisco ASA interface, where the local CA is allowing user enrollment, can be used to trigger this vulnerability. This vulnerability affects systems configured in routed firewall mode and in single or multiple context mode.
CVE-2016-6432
A vulnerability in the Identity Firewall feature of Cisco ASA Software before 9.6(2.1) could allow an unauthenticated, remote attacker to cause a reload of the affected system or to remotely execute code. The vulnerability is due to a buffer overflow in the affected code area. An attacker could exploit this vulnerability by sending a crafted NetBIOS packet in response to a NetBIOS probe sent by the ASA software. An exploit could allow the attacker to execute arbitrary code and obtain full control of the system or cause a reload of the affected system. Note: Only traffic directed to the affected system can be used to exploit this vulnerability. This vulnerability affects systems configured in routed and transparent firewall mode and in single or multiple context mode. This vulnerability can be triggered by IPv4 traffic.
CVE-2016-6437
A vulnerability in the SSL session cache management of Cisco Wide Area Application Services (WAAS) could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition due to high consumption of disk space. The user would see a performance degradation. More Information: CSCva03095. Known Affected Releases: 5.3(5), 6.1(1), 6.2(1). Known Fixed Releases: 5.3(5g)1, 6.2(2.32).
CVE-2016-6438
A vulnerability in Cisco IOS XE Software running on Cisco cBR-8 Converged Broadband Routers could allow an unauthenticated, remote attacker to cause a configuration integrity change to the vty line configuration on an affected device. This vulnerability affects the following releases of Cisco IOS XE Software running on Cisco cBR-8 Converged Broadband Routers: All 3.16S releases, All 3.17S releases, Release 3.18.0S, Release 3.18.1S, Release 3.18.0SP. More Information: CSCuz62815. Known Affected Releases: 15.5(3)S2.9, 15.6(2)SP. Known Fixed Releases: 15.6(1.7)SP1, 16.4(0.183), 16.5(0.1).
CVE-2016-6439
A vulnerability in the detection engine reassembly of HTTP packets for Cisco Firepower System Software before 6.0.1 could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition due to the Snort process unexpectedly restarting. The vulnerability is due to improper handling of an HTTP packet stream. An attacker could exploit this vulnerability by sending a crafted HTTP packet stream to the detection engine on the targeted device. An exploit could allow the attacker to cause a DoS condition if the Snort process restarts and traffic inspection is bypassed or traffic is dropped.
CVE-2016-6440
The Cisco Unified Communications Manager (CUCM) may be vulnerable to data that can be displayed inside an iframe within a web page, which in turn could lead to a clickjacking attack. More Information: CSCuz64683 CSCuz64698. Known Affected Releases: 11.0(1.10000.10), 11.5(1.10000.6), 11.5(0.99838.4). Known Fixed Releases: 11.0(1.22048.1), 11.5(0.98000.1070), 11.5(0.98000.284)11.5(0.98000.346), 11.5(0.98000.768), 11.5(1.10000.3), 11.5(1.10000.6), 11.5(2.10000.2).
CVE-2016-6442
A vulnerability in Cisco Finesse Agent and Supervisor Desktop Software could allow an unauthenticated, remote attacker to conduct a cross-site request forgery (CSRF) attack against the user of the web interface. More Information: CSCvb57213. Known Affected Releases: 11.0(1).
CVE-2016-6443
A vulnerability in the Cisco Prime Infrastructure and Evolved Programmable Network Manager SQL database interface could allow an authenticated, remote attacker to impact system confidentiality by executing a subset of arbitrary SQL queries that can cause product instability. More Information: CSCva27038, CSCva28335. Known Affected Releases: 3.1(0.128), 1.2(400), 2.0(1.0.34A).
CVE-2016-6444
A vulnerability in Cisco Meeting Server could allow an unauthenticated, remote attacker to conduct a cross-site request forgery (CSRF) attack against a Web Bridge user. More Information: CSCvb03308. Known Affected Releases: 1.8, 1.9, 2.0.
CVE-2016-6445
A vulnerability in the Extensible Messaging and Presence Protocol (XMPP) service of the Cisco Meeting Server (CMS) before 2.0.6 and Acano Server before 1.8.18 and 1.9.x before 1.9.6 could allow an unauthenticated, remote attacker to masquerade as a legitimate user. This vulnerability is due to the XMPP service incorrectly processing a deprecated authentication scheme. A successful exploit could allow an attacker to access the system as another user.
CVE-2016-6446
A vulnerability in Web Bridge for Cisco Meeting Server could allow an unauthenticated, remote attacker to retrieve memory from a connected server. More Information: CSCvb03308. Known Affected Releases: 1.8, 1.9, 2.0.
