CVE-2017-1000367
Todd Miller's sudo version 1.8.20 and earlier is vulnerable to an input validation (embedded spaces) in the get_process_ttyname() function resulting in information disclosure and command execution.
CVE-2017-1000368
Todd Miller's sudo version 1.8.20p1 and earlier is vulnerable to an input validation (embedded newlines) in the get_process_ttyname() function resulting in information disclosure and command execution.
CVE-2017-7669
In Apache Hadoop 2.8.0, 3.0.0-alpha1, and 3.0.0-alpha2, the LinuxContainerExecutor runs docker commands as root with insufficient input validation. When the docker feature is enabled, authenticated users can run commands as root.
CVE-2017-8438
Elastic X-Pack Security versions 5.0.0 to 5.4.0 contain a privilege escalation bug in the run_as functionality. This bug prevents transitioning into the specified user specified in a run_as request. If a role has been created using a template that contains the _user properties, the behavior of run_as will be incorrect. Additionally if the run_as user specified does not exist, the transition will not happen.
CVE-2017-8439
Kibana version 5.4.0 was affected by a Cross Site Scripting (XSS) bug in the Time Series Visual Builder. This bug could allow an attacker to obtain sensitive information from Kibana users.
CVE-2017-8440
Starting in version 5.3.0, Kibana had a cross-site scripting (XSS) vulnerability in the Discover page that could allow an attacker to obtain sensitive information from or perform destructive actions on behalf of other Kibana users.
CVE-2017-8441
Elastic X-Pack Security versions prior to 5.4.1 and 5.3.3 did not always correctly apply Document Level Security to index aliases. This bug could allow a user with restricted permissions to view data they should not have access to when performing certain operations against an index alias.
CVE-2017-8835
SQL injection exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. An attack vector is the bauth cookie to cgi-bin/MANGA/admin.cgi. One impact is enumeration of user accounts by observing whether a session ID can be retrieved from the sessions database.
CVE-2017-8836
CSRF exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. The CGI scripts in the administrative interface are affected. This allows an attacker to execute commands, if a logged in user visits a malicious website. This can for example be used to change the credentials of the administrative webinterface.
CVE-2017-8837
Cleartext password storage exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. The files in question are /etc/waipass and /etc/roapass. In case one of these devices is compromised, the attacker can gain access to passwords and abuse them to compromise further systems.
CVE-2017-8838
XSS via syncid exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. The affected script is cgi-bin/HASync/hasync.cgi.
CVE-2017-8839
XSS via orig_url exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. The affected script is guest/preview.cgi.
CVE-2017-8840
Debug information disclosure exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. A direct request to cgi-bin/HASync/hasync.cgi?debug=1 shows Master LAN Address, Serial Number, HA Group ID, Virtual IP, and Submitted syncid.
CVE-2017-8841
Arbitrary file deletion exists on Peplink Balance 305, 380, 580, 710, 1350, and 2500 devices with firmware before fw-b305hw2_380hw6_580hw2_710hw3_1350hw2_2500-7.0.1-build2093. The attack methodology is absolute path traversal in cgi-bin/MANGA/firmware_process.cgi via the upfile.path parameter.
CVE-2017-9420
Cross site scripting (XSS) vulnerability in the Spiffy Calendar plugin before 3.3.0 for WordPress allows remote attackers to inject arbitrary JavaScript via the yr parameter.
CVE-2017-9430
Stack-based buffer overflow in dnstracer through 1.9 allows attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a command line with a long name argument that is mishandled in a strcpy call for argv[0]. An example threat model is a web application that launches dnstracer with an untrusted name string.
CVE-2017-9431
Google gRPC before 2017-04-05 has an out-of-bounds write caused by a heap-based buffer overflow related to core/lib/iomgr/error.c.
CVE-2017-9432
Document Liberation Project libstaroffice before 2017-04-07 has an out-of-bounds write caused by a stack-based buffer overflow related to the DatabaseName::read function in lib/StarWriterStruct.cxx.
CVE-2017-9433
Document Liberation Project libmwaw before 2017-04-08 has an out-of-bounds write caused by a heap-based buffer overflow related to the MsWrd1Parser::readFootnoteCorrespondance function in lib/MsWrd1Parser.cxx.
CVE-2017-9434
Crypto++ (aka cryptopp) through 5.6.5 contains an out-of-bounds read vulnerability in zinflate.cpp in the Inflator filter.
CVE-2017-9435
Dolibarr ERP/CRM before 5.0.3 is vulnerable to a SQL injection in user/index.php (search_supervisor and search_statut parameters).
CVE-2017-9436
TeamPass before 2.1.27.4 is vulnerable to a SQL injection in users.queries.php.
CVE-2017-9437
Openbravo Business Suite 3.0 is affected by SQL injection. This vulnerability could allow remote authenticated attackers to inject arbitrary SQL code.
CVE-2017-9438
libyara/re.c in the regexp module in YARA 3.5.0 allows remote attackers to cause a denial of service (stack consumption) via a crafted rule (involving hex strings) that is mishandled in the _yr_re_emit function, a different vulnerability than CVE-2017-9304.
CVE-2017-9439
In ImageMagick 7.0.5-5, a memory leak was found in the function ReadPDBImage in coders/pdb.c, which allows attackers to cause a denial of service via a crafted file.
CVE-2017-9440
In ImageMagick 7.0.5-5, a memory leak was found in the function ReadPSDChannel in coders/psd.c, which allows attackers to cause a denial of service via a crafted file.
CVE-2017-9441
** DISPUTED ** Multiple cross-site scripting (XSS) vulnerabilities in BigTree CMS through 4.2.18 allow remote authenticated users to inject arbitrary web script or HTML by uploading a crafted package, triggering mishandling of the (1) title or (2) version or (3) author_name parameter in manifest.json. This issue exists in core\admin\modules\developer\extensions\install\unpack.php and core\admin\modules\developer\packages\install\unpack.php. NOTE: the vendor states "You must implicitly trust any package or extension you install as they all have the ability to write PHP files."
CVE-2017-9442
** DISPUTED ** BigTree CMS through 4.2.18 allows remote authenticated users to execute arbitrary code by uploading a crafted package containing a PHP web shell, related to extraction of a ZIP archive to filename patterns such as cache/package/xxx/yyy.php. This issue exists in core\admin\modules\developer\extensions\install\unpack.php and core\admin\modules\developer\packages\install\unpack.php. NOTE: the vendor states "You must implicitly trust any package or extension you install as they all have the ability to write PHP files."
CVE-2017-9443
** DISPUTED ** BigTree CMS through 4.2.18 allows remote authenticated users to conduct SQL injection attacks via a crafted tables object in manifest.json in an uploaded package. This issue exists in core\admin\modules\developer\extensions\install\process.php and core\admin\modules\developer\packages\install\process.php. NOTE: the vendor states "You must implicitly trust any package or extension you install as they all have the ability to write PHP files."
CVE-2017-9444
BigTree CMS through 4.2.18 has CSRF related to the core\admin\modules\users\profile\update.php script (modify user information), the index.php/admin/developer/packages/delete/ URI (remove packages), the index.php/admin/developer/upgrade/ignore/?versions= URI, and the index.php/admin/developer/upgrade/set-ftp-directory/ URI.
