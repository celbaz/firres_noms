CVE-2010-5193
Stack-based buffer overflow in the TIFMergeMultiFiles function in the SCRIBBLE.ScribbleCtrl.1 ActiveX control (ImageViewer2.ocx) in Viscom Image Viewer CP Pro 8.0 and Gold 6.0 allows remote attackers to execute arbitrary code via a long strDelimit parameter.
CVE-2010-5194
Stack-based buffer overflow in the Image2PDF function in the SCRIBBLE.ScribbleCtrl.1 ActiveX control (ImageViewer2.ocx) in Viscom Image Viewer CP Pro 8.0, Gold 5.5, Gold 6.0, and earlier allows remote attackers to execute arbitrary code via a long strPDFFile parameter.
CVE-2011-4946
SQL injection vulnerability in e107_admin/users_extended.php in e107 before 0.7.26 allows remote attackers to execute arbitrary SQL commands via the user_field parameter.
CVE-2011-4947
Cross-site request forgery (CSRF) vulnerability in e107_admin/users_extended.php in e107 before 0.7.26 allows remote attackers to hijack the authentication of administrators for requests that insert cross-site scripting (XSS) sequences via the user_include parameter.
CVE-2011-4948
Directory traversal vulnerability in admin/remote.php in EGroupware Enterprise Line (EPL) before 11.1.20110804-1 and EGroupware Community Edition before 1.8.001.20110805 allows remote attackers to read arbitrary files via a ..%2f (encoded dot dot slash) in the type parameter.
CVE-2011-4949
SQL injection vulnerability in phpgwapi/js/dhtmlxtree/samples/with_db/loaddetails.php in EGroupware Enterprise Line (EPL) before 11.1.20110804-1 and EGroupware Community Edition before 1.8.001.20110805 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2011-4950
Cross-site scripting (XSS) vulnerability in phpgwapi/js/jscalendar/test.php in EGroupware Enterprise Line (EPL) before 11.1.20110804-1 and EGroupware Community Edition before 1.8.001.20110805 allows remote attackers to inject arbitrary web script or HTML via the lang parameter.
CVE-2011-4951
Open redirect vulnerability in phpgwapi/ntlm/index.php in EGroupware Enterprise Line (EPL) before 11.1.20110804-1 and EGroupware Community Edition before 1.8.001.20110805 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the forward parameter.
Per: http://cwe.mitre.org/data/definitions/601.html

'CWE-601: URL Redirection to Untrusted Site ('Open Redirect')'
CVE-2011-5137
Multiple SQL injection vulnerabilities in tForum b0.915 allow remote attackers to execute arbitrary SQL commands via the (1) TopicID parameter to viewtopic.php, the (2) BoardID parameter to viewboard.php, or (3) CatID parameter to viewcat.php.
CVE-2011-5138
Cross-site scripting (XSS) vulnerability in member.php in tForum b0.915 allows remote attackers to inject arbitrary web script or HTML via the username parameter in a viewprofile action.
CVE-2011-5139
SQL injection vulnerability in page.php in Pre Studio Business Cards Designer allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2011-5140
Multiple SQL injection vulnerabilities in the blog module 1.0 for DiY-CMS allow remote attackers to execute arbitrary SQL commands via the (1) start parameter to (a) tags.php, (b) list.php, (c) index.php, (d) main_index.php, (e) viewpost.php, (f) archive.php, (g) control/approve_comments.php, (h) control/approve_posts.php, and (i) control/viewcat.php; and the (2) month and (3) year parameters to archive.php.
CVE-2011-5141
Directory traversal vulnerability in exportcsv/exportcsv_index.php in Open Business Management (OBM) 2.4.0-rc13 and earlier allows remote authenticated users to include and execute arbitrary local files via a .. (dot dot) in the module parameter in an export_page action.
CVE-2011-5142
Multiple cross-site scripting (XSS) vulnerabilities in Open Business Management (OBM) 2.4.0-rc13 and probably earlier allow remote attackers to inject arbitrary web script or HTML via the (1) tf_delegation, (2) tf_ip, or (3) tf_name parameter in a search action to host/host_index.php; (4) login parameter to obm.php; or (5) tf_user parameter in a search action to group/group_index.php.
CVE-2011-5143
Multiple cross-site scripting (XSS) vulnerabilities in Open Business Management (OBM) 2.3.20 and probably earlier allow remote attackers to inject arbitrary web script or HTML via the (1) tf_name, (2) tf_delegation, and (3) tf_ip parameters to index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2011-5144
Open Business Management (OBM) 2.4.0-rc13 and earlier allows remote attackers to obtain configuration information via a direct request to test.php, which calls the phpinfo function.
CVE-2011-5145
Multiple SQL injection vulnerabilities in Open Business Management (OBM) 2.4.0-rc13 and probably earlier allow remote authenticated users to execute arbitrary SQL commands via the (1) sel_domain_id or (2) action parameter to obm.php; (3) tf_user parameter in a search action to group/group_index.php; (4) tf_delegation, (5) tf_ip, (6) tf_name to host/host_index.php; or (7) lang, (8) theme, (9) cal_alert, (10) cal_first_hour, (11) cal_interval, (12) cal_last_hour, (13) commentorder, (14) csv_sep, (15) date, (16) date_upd, (17) debug_exe, (18) debug_id, (19) debug_param, (20) debug_sess, (21) debug_solr, (22) debug_sql, (23) dsrc, (24) menu, (25) rows, (26) sel_display_days, (27) timeformat, (28) timezone, or (29) todo parameter to settings/settings_index.php.
CVE-2011-5146
Bokken before 1.6 and 1.5-x before 1.5-3 for Debian allows local users to overwrite arbitrary files via a symlink attack on /tmp/graph.dot.
CVE-2011-5147
Static code injection vulnerability in ajax_save_name.php in the Ajax File Manager module in the tinymce plugin in FreeWebshop 2.2.9 R2 and earlier allows remote attackers to inject arbitrary PHP code into data.php via the selected document, as demonstrated by a call to ajax_file_cut.php and then to ajax_save_name.php.
CVE-2011-5148
Multiple incomplete blacklist vulnerabilities in the Simple File Upload (mod_simplefileuploadv1.3) module before 1.3.5 for Joomla! allow remote attackers to execute arbitrary code by uploading a file with a (1) php5, (2) php6, or (3) double (e.g. .php.jpg) extension, then accessing it via a direct request to the file in images/, as exploited in the wild in January 2012.
Per: http://cwe.mitre.org/data/definitions/184.html

'CWE-184: Incomplete Blacklist'
CVE-2011-5149
Multiple cross-site scripting (XSS) vulnerabilities in SpamTitan 5.08 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) testaddr or (2) testpass parameter to auth-settings.php; (3) hostname, (4) domainname, or (5) mailserver parameter to setup-relay.php; or (6) subnetmask or (7) defaultroute parameter to setup-network.php.
CVE-2011-5150
Multiple cross-site scripting (XSS) vulnerabilities in SpamTitan 5.07 and possibly earlier allow remote attackers or authenticated users to inject arbitrary web script or HTML via the (1) ipaddress or (2) domain parameter to setup-network.php, different vectors than CVE-2011-5149.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2012-2083
Cross-site scripting (XSS) vulnerability in the fusion_core_preprocess_page function in fusion_core/template.php in the Fusion module before 6.x-1.13 for Drupal allows remote attackers to inject arbitrary web script or HTML via the q parameter.
CVE-2012-2114
Stack-based buffer overflow in fprintf in musl before 0.8.8 and earlier allows context-dependent attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long string to an unbuffered stream such as stderr.
CVE-2012-2116
Cross-site request forgery (CSRF) vulnerability in the Commerce Reorder module before 7.x-1.1 for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that add items to the shopping cart.
CVE-2012-2117
Cross-site scripting (XSS) vulnerability in the Gigya - Social optimization module 6.x before 6.x-3.2 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2186
Incomplete blacklist vulnerability in main/manager.c in Asterisk Open Source 1.8.x before 1.8.15.1 and 10.x before 10.7.1, Certified Asterisk 1.8.11 before 1.8.11-cert6, Asterisk Digiumphones 10.x.x-digiumphones before 10.7.1-digiumphones, and Asterisk Business Edition C.3.x before C.3.7.6 allows remote authenticated users to execute arbitrary commands by leveraging originate privileges and providing an ExternalIVR value in an AMI Originate action.
CVE-2012-2657
** DISPUTED **  Buffer overflow in the SQLDriverConnect function in unixODBC 2.0.10, 2.3.1, and earlier allows local users to cause a denial of service (crash) via a long string in the FILEDSN option.  NOTE: this issue might not be a vulnerability, since the ability to set this option typically implies that the attacker already has legitimate access to cause a DoS or execute code, and therefore the issue would not cross privilege boundaries. There may be limited attack scenarios if isql command-line options are exposed to an attacker, although it seems likely that other, more serious issues would also be exposed, and this issue might not cross privilege boundaries in that context.
CVE-2012-2658
** DISPUTED **  Buffer overflow in the SQLDriverConnect function in unixODBC 2.3.1 allows local users to cause a denial of service (crash) via a long string in the DRIVER option.  NOTE: this issue might not be a vulnerability, since the ability to set this option typically implies that the attacker already has legitimate access to cause a DoS or execute code, and therefore the issue would not cross privilege boundaries. There may be limited attack scenarios if isql command-line options are exposed to an attacker, although it seems likely that other, more serious issues would also be exposed, and this issue might not cross privilege boundaries in that context.
CVE-2012-2704
The Advertisement module 6.x-2.x before 6.x-2.3 for Drupal does not properly restrict access to debug information, which allows remote attackers to obtain sensitive site configuration information that is specified by the $conf variable in settings.php.
CVE-2012-2865
Google Chrome before 21.0.1180.89 does not properly perform line breaking, which allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted document.
CVE-2012-2866
Google Chrome before 21.0.1180.89 does not properly perform a cast of an unspecified variable during handling of run-in elements, which allows remote attackers to cause a denial of service or possibly have unknown other impact via a crafted document.
CVE-2012-2867
The SPDY implementation in Google Chrome before 21.0.1180.89 allows remote attackers to cause a denial of service (application crash) via unspecified vectors.
CVE-2012-2868
Race condition in Google Chrome before 21.0.1180.89 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors involving improper interaction between worker processes and an XMLHttpRequest (aka XHR) object.
CVE-2012-2869
Google Chrome before 21.0.1180.89 does not properly load URLs, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors that trigger a "stale buffer."
CVE-2012-2870
libxslt 1.1.26 and earlier, as used in Google Chrome before 21.0.1180.89, does not properly manage memory, which might allow remote attackers to cause a denial of service (application crash) via a crafted XSLT expression that is not properly identified during XPath navigation, related to (1) the xsltCompileLocationPathPattern function in libxslt/pattern.c and (2) the xsltGenerateIdFunction function in libxslt/functions.c.
CVE-2012-2871
libxml2 2.9.0-rc1 and earlier, as used in Google Chrome before 21.0.1180.89, does not properly support a cast of an unspecified variable during handling of XSL transforms, which allows remote attackers to cause a denial of service or possibly have unknown other impact via a crafted document, related to the _xmlNs data structure in include/libxml/tree.h.
Per: http://cwe.mitre.org/data/definitions/704.html

'CWE-704: Incorrect Type Conversion or Cast'
CVE-2012-2872
Cross-site scripting (XSS) vulnerability in an SSL interstitial page in Google Chrome before 21.0.1180.89 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-3378
The register_application function in atk-adaptor/bridge.c in GNOME at-spi2-atk 2.5.2 does not seed the random number generator and generates predictable temporary file names, which makes it easier for local users to create or truncate files via a symlink attack on a temporary socket file in /tmp/at-spi2.
CVE-2012-3379
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2012-0808.  Reason: This candidate is a duplicate of CVE-2012-0808.  Notes: All CVE users should reference CVE-2012-0808 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2012-3380
Directory traversal vulnerability in naxsi-ui/nx_extract.py in the Naxsi module before 0.46-1 for Nginx allows local users to read arbitrary files via unspecified vectors.
CVE-2012-3478
rssh 2.3.3 and earlier allows local users to bypass intended restricted shell access via crafted environment variables in the command line.
CVE-2012-3533
The python SDK before 3.1.0.6 and CLI before 3.1.0.8 for oVirt 3.1 does not check the server SSL certificate against the client keys, which allows remote attackers to spoof a server via a man-in-the-middle (MITM) attack.
CVE-2012-3534
GNU Gatekeeper before 3.1 does not limit the number of connections to the status port, which allows remote attackers to cause a denial of service (connection and thread consumption) via a large number of connections.
CVE-2012-4008
The Cybozu Live application 1.0.4 and earlier for Android allows remote attackers to execute arbitrary Java methods, and obtain sensitive information or execute arbitrary commands, via a crafted web site.
CVE-2012-4009
The WebView class in the Cybozu Live application 1.0.4 and earlier for Android allows remote attackers to execute arbitrary JavaScript code, and obtain sensitive information, via a crafted application that places this code into a local file associated with a file: URL.
CVE-2012-4170
Buffer overflow in Adobe Photoshop CS6 13.x before 13.0.1 allows remote attackers to execute arbitrary code via a crafted file.
CVE-2012-4171
Adobe Flash Player before 10.3.183.23 and 11.x before 11.4.402.265 on Windows and Mac OS X, before 10.3.183.23 and 11.x before 11.2.202.238 on Linux, before 11.1.111.16 on Android 2.x and 3.x, and before 11.1.115.17 on Android 4.x; Adobe AIR before 3.4.0.2540; and Adobe AIR SDK before 3.4.0.2540 allow attackers to cause a denial of service (application crash) by leveraging a logic error during handling of Firefox dialogs.
CVE-2012-4245
The scriptfu network server in GIMP 2.6 does not require authentication, which allows remote attackers to execute arbitrary commands via the python-fu-eval command.
CVE-2012-4600
Cross-site scripting (XSS) vulnerability in Open Ticket Request System (OTRS) Help Desk 2.4.x before 2.4.14, 3.0.x before 3.0.16, and 3.1.x before 3.1.10, when Firefox or Opera is used, allows remote attackers to inject arbitrary web script or HTML via an e-mail message body with nested HTML tags.
CVE-2012-4737
channels/chan_iax2.c in Asterisk Open Source 1.8.x before 1.8.15.1 and 10.x before 10.7.1, Certified Asterisk 1.8.11 before 1.8.11-cert7, Asterisk Digiumphones 10.x.x-digiumphones before 10.7.1-digiumphones, and Asterisk Business Edition C.3.x before C.3.7.6 does not enforce ACL rules during certain uses of peer credentials, which allows remote authenticated users to bypass intended outbound-call restrictions by leveraging the availability of these credentials.
CVE-2012-4739
Multiple cross-site scripting (XSS) vulnerabilities in Barracuda SSL VPN before 2.2.2.203 (2012-07-05) allow remote attackers to inject arbitrary web script or HTML via the (1) policyLaunching, (2) resourcePrefix, or (3) actionPath parameter in showUserResourceCategories.do; (4) list or (5) path parameter to fileSystem.do; or (6) return-To parameter to launchAgent.do.
CVE-2012-4740
Cross-site scripting (XSS) vulnerability in the captive portal in PacketFence before 3.3.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-4741
The RADIUS extension in PacketFence before 3.3.0 uses a different user name than is used for authentication for users with custom VLAN assignment extensions, which allows remote attackers to spoof user identities via the User-Name RADIUS attribute.
CVE-2012-4742
The web_node_register function in web.pm in PacketFence before 3.0.2 might allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2012-4743
Multiple SQL injection vulnerabilities in ssearch.php in Siche search module 0.5 for Zeroboard allow remote attackers to execute arbitrary SQL commands via the (1) ss, (2) sm, (3) align, or (4) category parameters.
CVE-2012-4744
Cross-site scripting (XSS) vulnerability in ssearch.php in the Siche search module 0.5 for Zeroboard allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2012-4745
Cross-site scripting (XSS) vulnerability in admin/login.asp in Acuity CMS 2.6.2 allows remote attackers to inject arbitrary web script or HTML via the UserName parameter.
CVE-2012-4746
Cross-site request forgery (CSRF) vulnerability in accessaccount.cgi in ZTE ZXDSL 831IIV7.5.0a_Z29_OV allows remote attackers to hijack the authentication of administrators for requests that change the administrator password via the sysPassword parameter.
