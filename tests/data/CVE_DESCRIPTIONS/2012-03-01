CVE-2011-4486
Cisco Unified Communications Manager (CUCM) with software 6.x and 7.x before 7.1(5b)su5, 8.0 before 8.0(3a)su3, and 8.5 and 8.6 before 8.6(2a)su1 and Cisco Business Edition 3000 with software before 8.6.3 and 5000 and 6000 with software before 8.6(2a)su1 allow remote attackers to cause a denial of service (device reload) via a crafted SCCP registration, aka Bug ID CSCtu73538.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20120229-cucm

'The following products are affected by the vulnerabilities that are described in this advisory:

    * Cisco Unified Communications Manager Software versions 6.x 
    * Cisco Unified Communications Manager Software versions 7.x 
    * Cisco Unified Communications Manager Software versions 8.x'
CVE-2011-4487
SQL injection vulnerability in Cisco Unified Communications Manager (CUCM) with software 6.x and 7.x before 7.1(5b)su5, 8.0 before 8.0(3a)su3, and 8.5 and 8.6 before 8.6(2a)su1 and Cisco Business Edition 3000 with software before 8.6.3 and 5000 and 6000 with software before 8.6(2a)su1 allows remote attackers to execute arbitrary SQL commands via a crafted SCCP registration, aka Bug ID CSCtu73538.
CVE-2012-0330
Cisco TelePresence Video Communication Server with software before X7.0.1 allows remote attackers to cause a denial of service (device crash) via a malformed SIP message, aka Bug ID CSCtr20426.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20120229-vcs

'Vulnerable Products
These vulnerabilities affect all three variants (Control, Expressway, and Starter Pack Express) of Cisco TelePresence Video Communication Server.'
CVE-2012-0331
Cisco TelePresence Video Communication Server with software before X7.0.1 allows remote attackers to cause a denial of service (device crash) via a crafted SIP packet, as demonstrated by a SIP INVITE message from a Tandberg device, aka Bug ID CSCtq73319.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20120229-vcs

'Vulnerable Products
These vulnerabilities affect all three variants (Control, Expressway, and Starter Pack Express) of Cisco TelePresence Video Communication Server.'
CVE-2012-0359
The Cisco Cius with software before 9.2(1) SR2 allows remote attackers to cause a denial of service (device crash or hang) via malformed network traffic, aka Bug ID CSCto71445.
Per: http://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20120229-cius

'Vulnerable Products
The following products are affected by the vulnerability detailed in this advisory:

    * Cius Wifi devices running Cius Software Version 9.2(1) SR1 and prior'
CVE-2012-0366
Cisco Unity Connection before 7.1.3b(Su2) allows remote authenticated users to change the administrative password by leveraging the Help Desk Administrator role, aka Bug ID CSCtd45141.
CVE-2012-0367
Cisco Unity Connection before 7.1.5b(Su5), 8.0 and 8.5 before 8.5.1(Su3), and 8.6 before 8.6.2 allows remote attackers to cause a denial of service (services crash) via a series of crafted TCP segments, aka Bug ID CSCtq67899.
CVE-2012-0368
The administrative management interface on Cisco Wireless LAN Controller (WLC) devices with software 4.x, 5.x, 6.0, and 7.0 before 7.0.220.0, 7.1 before 7.1.91.0, and 7.2 before 7.2.103.0 allows remote attackers to cause a denial of service (device crash) via a malformed URL in an HTTP request, aka Bug ID CSCts81997.
CVE-2012-0369
Cisco Wireless LAN Controller (WLC) devices with software 6.0 and 7.0 before 7.0.220.0, 7.1 before 7.1.91.0, and 7.2 before 7.2.103.0 allow remote attackers to cause a denial of service (device reload) via a sequence of IPv6 packets, aka Bug ID CSCtt07949.
CVE-2012-0370
Cisco Wireless LAN Controller (WLC) devices with software 4.x, 5.x, 6.0, and 7.0 before 7.0.220.0 and 7.1 before 7.1.91.0, when WebAuth is enabled, allow remote attackers to cause a denial of service (device reload) via a sequence of (1) HTTP or (2) HTTPS packets, aka Bug ID CSCtt47435.
CVE-2012-0371
Cisco Wireless LAN Controller (WLC) devices with software 4.x, 5.x, 6.0, and 7.0 before 7.0.220.4, when CPU-based ACLs are enabled, allow remote attackers to read or modify the configuration via unspecified vectors, aka Bug ID CSCtu56709.
