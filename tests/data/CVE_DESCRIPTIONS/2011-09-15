CVE-2011-0653
Cross-site scripting (XSS) vulnerability in Microsoft Office SharePoint Server 2010 Gold and SP1, and SharePoint Foundation 2010, allows remote attackers to inject arbitrary web script or HTML via the URI, aka "XSS in SharePoint Calendar Vulnerability."
CVE-2011-1353
Unspecified vulnerability in Adobe Reader 10.x before 10.1.1 on Windows allows local users to gain privileges via unknown vectors.
CVE-2011-1890
Cross-site scripting (XSS) vulnerability in EditForm.aspx in Microsoft Office SharePoint Server 2010 and SharePoint Foundation 2010 allows remote attackers to inject arbitrary web script or HTML via a post, aka "Editform Script Injection Vulnerability."
CVE-2011-1891
Cross-site scripting (XSS) vulnerability in Microsoft Windows SharePoint Services 3.0 SP2, and SharePoint Foundation 2010 Gold and SP1, allows remote attackers to inject arbitrary web script or HTML via unspecified parameters in a request to a script, aka "Contact Details Reflected XSS Vulnerability."
CVE-2011-1892
Microsoft Office Groove 2007 SP2, SharePoint Workspace 2010 Gold and SP1, Office Forms Server 2007 SP2, Office SharePoint Server 2007 SP2, Office SharePoint Server 2010 Gold and SP1, Office Groove Data Bridge Server 2007 SP2, Office Groove Management Server 2007 SP2, Groove Server 2010 Gold and SP1, Windows SharePoint Services 3.0 SP2, SharePoint Foundation 2010, and Office Web Apps 2010 Gold and SP1 do not properly handle Web Parts containing XML classes referencing external entities, which allows remote authenticated users to read arbitrary files via a crafted XML and XSL file, aka "SharePoint Remote File Disclosure Vulnerability."
CVE-2011-1893
Cross-site scripting (XSS) vulnerability in Microsoft Office SharePoint Server 2010, Windows SharePoint Services 2.0 and 3.0 SP2, and SharePoint Foundation 2010 allows remote attackers to inject arbitrary web script or HTML via the URI, aka "SharePoint XSS Vulnerability."
CVE-2011-1980
Untrusted search path vulnerability in Microsoft Office 2003 SP3 and 2007 SP2 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a .doc, .ppt, or .xls file, aka "Office Component Insecure Library Loading Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/MS11-073
Access Vector: Network per "This is a remote code execution vulnerability"
Per: http://cwe.mitre.org/data/definitions/426.html
'CWE-426: Untrusted Search Path'
CVE-2011-1982
Microsoft Office 2007 SP2, and 2010 Gold and SP1, does not initialize an unspecified object pointer during the opening of Word documents, which allows remote attackers to execute arbitrary code via a crafted document, aka "Office Uninitialized Object Pointer Vulnerability."
CVE-2011-1984
WINS in Microsoft Windows Server 2003 SP2 and Server 2008 SP2, R2, and R2 SP1 allows local users to gain privileges by sending crafted packets over the loopback interface, aka "WINS Local Elevation of Privilege Vulnerability."
CVE-2011-1986
Use-after-free vulnerability in Microsoft Excel 2003 SP3 allows remote attackers to execute arbitrary code via a crafted spreadsheet, aka "Excel Use after Free WriteAV Vulnerability."
CVE-2011-1987
Array index error in Microsoft Excel 2003 SP3 and 2007 SP2; Excel in Office 2007 SP2; Excel 2010 Gold and SP1; Excel in Office 2010 Gold and SP1; Office 2004, 2008, and 2011 for Mac; Open XML File Format Converter for Mac; Excel Viewer SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2 allows remote attackers to execute arbitrary code via a crafted spreadsheet, aka "Excel Out of Bounds Array Indexing Vulnerability."
CVE-2011-1988
Microsoft Excel 2003 SP3 and 2007 SP2; Excel in Office 2007 SP2; Office 2004 and 2008 for Mac; Open XML File Format Converter for Mac; Excel Viewer SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2 do not properly parse records in Excel spreadsheets, which allows remote attackers to execute arbitrary code via a crafted spreadsheet, aka "Excel Heap Corruption Vulnerability."
CVE-2011-1989
Microsoft Excel 2003 SP3 and 2007 SP2; Excel in Office 2007 SP2; Excel 2010 Gold and SP1; Excel in Office 2010 Gold and SP1; Office 2004, 2008, and 2011 for Mac; Open XML File Format Converter for Mac; Excel Viewer SP2; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2; Excel Services on Office SharePoint Server 2007 SP2; Excel Services on Office SharePoint Server 2010 Gold and SP1; and Excel Web App 2010 Gold and SP1 do not properly parse conditional expressions associated with formatting requirements, which allows remote attackers to execute arbitrary code via a crafted spreadsheet, aka "Excel Conditional Expression Parsing Vulnerability."
CVE-2011-1990
Microsoft Excel 2007 SP2; Excel in Office 2007 SP2; Excel Viewer SP2; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2; and Excel Services on Office SharePoint Server 2007 SP2 do not properly validate the sign of an unspecified array index, which allows remote attackers to execute arbitrary code via a crafted spreadsheet, aka "Excel Out of Bounds Array Indexing Vulnerability."
CVE-2011-1991
Multiple untrusted search path vulnerabilities in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allow local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a .doc, .rtf, or .txt file, related to (1) deskpan.dll in the Display Panning CPL Extension, (2) EAPHost Authenticator Service, (3) Folder Redirection, (4) HyperTerminal, (5) the Japanese Input Method Editor (IME), and (6) Microsoft Management Console (MMC), aka "Windows Components Insecure Library Loading Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/MS11-071
Access Vector: Network per "This is a remote code execution vulnerability"
Per: http://cwe.mitre.org/data/definitions/426.html'CWE-426: Untrusted Search Path'
CVE-2011-2431
Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allow attackers to execute arbitrary code via unspecified vectors, related to a "security bypass vulnerability."
CVE-2011-2432
Buffer overflow in the U3D TIFF Resource in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2433
Heap-based buffer overflow in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2011-2434 and CVE-2011-2437.
CVE-2011-2434
Heap-based buffer overflow in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2011-2433 and CVE-2011-2437.
CVE-2011-2435
Buffer overflow in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2436
Heap-based buffer overflow in the image-parsing library in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2437
Heap-based buffer overflow in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2011-2433 and CVE-2011-2434.
CVE-2011-2438
Multiple stack-based buffer overflows in the image-parsing library in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allow attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2439
Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allow attackers to execute arbitrary code via unspecified vectors, related to a "memory leakage condition vulnerability."
CVE-2011-2440
Use-after-free vulnerability in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2441
Multiple stack-based buffer overflows in CoolType.dll in Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allow attackers to execute arbitrary code via unspecified vectors.
CVE-2011-2442
Adobe Reader and Acrobat 8.x before 8.3.1, 9.x before 9.4.6, and 10.x before 10.1.1 allow attackers to execute arbitrary code via unspecified vectors, related to a "logic error vulnerability."
CVE-2011-2671
Unspecified vulnerability in Megalith 12th edition through 27th edition allows remote attackers to gain administrative privileges via unknown vectors.
CVE-2011-3322
Core Server HMI Service (Coreservice.exe) in Scadatec Limited Procyon SCADA 1.06, and other versions before 1.14, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long password to the Telnet (TCP/23) port, which triggers an out-of-bounds read or write, leading to a stack-based buffer overflow.
CVE-2011-3393
Multiple cross-site scripting (XSS) vulnerabilities in findagent.php in MYRE Real Estate Software allow remote attackers to inject arbitrary web script or HTML via the (1) country1, (2) state1, or (3) city1 parameter.
CVE-2011-3394
SQL injection vulnerability in findagent.php in MYRE Real Estate Software allows remote attackers to execute arbitrary SQL commands via the page parameter.
