CVE-2017-15720
In Apache Airflow 1.8.2 and earlier, an authenticated user can execute code remotely on the Airflow webserver by creating a special object.
CVE-2017-17835
In Apache Airflow 1.8.2 and earlier, a CSRF vulnerability allowed for a remote command injection on a default install of Airflow.
CVE-2017-17836
In Apache Airflow 1.8.2 and earlier, an experimental Airflow feature displayed authenticated cookies, as well as passwords to databases used by Airflow. An attacker who has limited access to airflow, whether it be via XSS or by leaving a machine unlocked can exfiltrate all credentials from the system.
CVE-2018-0187
A vulnerability in the Admin portal of Cisco Identity Services Engine (ISE) could allow an authenticated, remote attacker to obtain confidential information for privileged accounts. The vulnerability is due to the improper handling of confidential information. An attacker could exploit this vulnerability by logging into the web interface on a vulnerable system. An exploit could allow an attacker to obtain confidential information for privileged accounts. This information could then be used to impersonate or negatively impact the privileged account on the affected system.
CVE-2018-1000997
A path traversal vulnerability exists in the Stapler web framework used by Jenkins 2.145 and earlier, LTS 2.138.1 and earlier in core/src/main/java/org/kohsuke/stapler/Facet.java, groovy/src/main/java/org/kohsuke/stapler/jelly/groovy/GroovyFacet.java, jelly/src/main/java/org/kohsuke/stapler/jelly/JellyFacet.java, jruby/src/main/java/org/kohsuke/stapler/jelly/jruby/JRubyFacet.java, jsp/src/main/java/org/kohsuke/stapler/jsp/JSPFacet.java that allows attackers to render routable objects using any view in Jenkins, exposing internal information about those objects not intended to be viewed, such as their toString() representation.
CVE-2018-15455
A vulnerability in the logging component of Cisco Identity Services Engine could allow an unauthenticated, remote attacker to conduct cross-site scripting attacks. The vulnerability is due to the improper validation of requests stored in the system's logging database. An attacker could exploit this vulnerability by sending malicious requests to the targeted system. An exploit could allow the attacker to conduct cross-site scripting attacks when an administrator views the logs in the Admin Portal.
CVE-2018-15459
A vulnerability in the administrative web interface of Cisco Identity Services Engine (ISE) could allow an authenticated, remote attacker to gain additional privileges on an affected device. The vulnerability is due to improper controls on certain pages in the web interface. An attacker could exploit this vulnerability by authenticating to the device with an administrator account and sending a crafted HTTP request. A successful exploit could allow the attacker to create additional Admin accounts with different user roles. An attacker could then use these accounts to perform actions within their scope. The attacker would need valid Admin credentials for the device. This vulnerability cannot be exploited to add a Super Admin account.
CVE-2018-15614
A vulnerability in the one-x Portal component of IP Office could allow an authenticated user to perform stored cross site scripting attacks via fields in the Conference Scheduler Service that could affect other application users. Affected versions of IP Office include 10.0 through 10.1 SP3 and 11.0 versions prior to 11.0 SP1.
CVE-2018-1751
IBM Security Key Lifecycle Manager 3.0 through 3.0.0.2 uses weaker than expected cryptographic algorithms that could allow an attacker to decrypt highly sensitive information. IBM X-Force ID: 148512.
CVE-2018-20245
The LDAP auth backend (airflow.contrib.auth.backends.ldap_auth) prior to Apache Airflow 1.10.1 was misconfigured and contained improper checking of exceptions which disabled server certificate checking.
CVE-2018-2026
IBM Financial Transaction Manager 3.2.1 for Digital Payments could allow an authenticated user to obtain a directory listing of internal product files. IBM X-Force ID: 155552.
