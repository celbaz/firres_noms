CVE-2012-6582
Cross-site scripting (XSS) vulnerability in the Spambot module 6.x-3.x before 6.x-3.2 and 7.x-1.x before 7.x-1.1 for Drupal allows certain remote attackers to inject arbitrary web script or HTML via a stopforumspam.com API response, which is logged by the watchdog.
CVE-2013-2153
The XML digital signature functionality (xsec/dsig/DSIGReference.cpp) in Apache Santuario XML Security for C++ (aka xml-security-c) before 1.7.1 allows context-dependent attackers to reuse signatures and spoof arbitrary content via crafted Reference elements in the Signature, aka "XML Signature Bypass issue."
CVE-2013-2154
Stack-based buffer overflow in the XML Signature Reference functionality (xsec/dsig/DSIGReference.cpp) in Apache Santuario XML Security for C++ (aka xml-security-c) before 1.7.1 allows context-dependent attackers to cause a denial of service (crash) and possibly execute arbitrary code via malformed XPointer expressions, probably related to the DSIGReference::getURIBaseTXFM function.
CVE-2013-2155
Apache Santuario XML Security for C++ (aka xml-security-c) before 1.7.1 does not properly validate length values, which allows remote attackers to cause a denial of service or bypass the CVE-2009-0217 protection mechanism and spoof a signature via crafted length values to the (1) compareBase64StringToRaw, (2) DSIGAlgorithmHandlerDefault, or (3) DSIGAlgorithmHandlerDefault::verify functions.
CVE-2013-2156
Heap-based buffer overflow in the Exclusive Canonicalization functionality (xsec/canon/XSECC14n20010315.cpp) in Apache Santuario XML Security for C++ (aka xml-security-c) before 1.7.1 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted PrefixList attribute.
CVE-2013-2157
OpenStack Keystone Folsom, Grizzly before 2013.1.3, and Havana, when using LDAP with Anonymous binding, allows remote attackers to bypass authentication via an empty password.
CVE-2013-2161
XML injection vulnerability in account/utils.py in OpenStack Swift Folsom, Grizzly, and Havana allows attackers to trigger invalid or spoofed Swift responses via an account name.
CVE-2013-2172
jcp/xml/dsig/internal/dom/DOMCanonicalizationMethod.java in Apache Santuario XML Security for Java 1.4.x before 1.4.8 and 1.5.x before 1.5.5 allows context-dependent attackers to spoof an XML Signature by using the CanonicalizationMethod parameter to specify an arbitrary weak "canonicalization algorithm to apply to the SignedInfo part of the Signature."
CVE-2013-2210
Heap-based buffer overflow in the XML Signature Reference functionality in Apache Santuario XML Security for C++ (aka xml-security-c) before 1.7.2 allows context-dependent attackers to cause a denial of service (crash) and possibly execute arbitrary code via malformed XPointer expressions.  NOTE: this is due to an incorrect fix for CVE-2013-2154.
CVE-2013-4130
The (1) red_channel_pipes_add_type and (2) red_channel_pipes_add_empty_msg functions in server/red_channel.c in SPICE before 0.12.4 do not properly perform ring loops, which might allow remote attackers to cause a denial of service (reachable assertion and server exit) by triggering a network error.
CVE-2013-4155
OpenStack Swift before 1.9.1 in Folsom, Grizzly, and Havana allows authenticated users to cause a denial of service ("superfluous" tombstone consumption and Swift cluster slowdown) via a DELETE request with a timestamp that is older than expected.
CVE-2013-4653
Multiple cross-site scripting (XSS) vulnerabilities in the signin functionality of ics in MyTeamwork services in Alcatel-Lucent Omnitouch 8660 My Teamwork before 6.7, Omnitouch 8670 Automated Message Delivery System (AMDS) before 6.7, Omnitouch 8460 Advanced Communication Server before 9.1, and OmniTouch 8400 Instant Communications Suite before 6.7.3 (1) allow remote attackers to inject arbitrary web script or HTML via a crafted URL that results in a reflected XSS or (2) allow user-assisted remote attackers to inject arbitrary web script or HTML via a user's personal bookmark entry that results in a stored XSS via unspecified vectors.
CVE-2013-4761
Unspecified vulnerability in Puppet 2.7.x before 2.7.23 and 3.2.x before 3.2.4, and Puppet Enterprise 2.8.x before 2.8.3 and 3.0.x before 3.0.1, allows remote attackers to execute arbitrary Ruby programs from the master via the resource_type service.  NOTE: this vulnerability can only be exploited utilizing unspecified "local file system access" to the Puppet Master.
CVE-2013-4762
Puppet Enterprise before 3.0.1 does not sufficiently invalidate a session when a user logs out, which might allow remote attackers to hijack sessions by obtaining an old session ID.
CVE-2013-4955
Open redirect vulnerability in the login page in Puppet Enterprise before 3.0.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the service parameter.
CVE-2013-4956
Puppet Module Tool (PMT), as used in Puppet 2.7.x before 2.7.23 and 3.2.x before 3.2.4, and Puppet Enterprise 2.8.x before 2.8.3 and 3.0.x before 3.0.1, installs modules with weak permissions if those permissions were used when the modules were originally built, which might allow local users to read or modify those modules depending on the original permissions.
CVE-2013-4958
Puppet Enterprise before 3.0.1 does not use a session timeout, which makes it easier for attackers to gain privileges by leveraging an unattended workstation.
CVE-2013-4959
Puppet Enterprise before 3.0.1 uses HTTP responses that contain sensitive information without the "no-cache" setting, which might allow local users to obtain sensitive information such as (1) host name, (2) MAC address, and (3) SSH keys via the web browser cache.
CVE-2013-4961
Puppet Enterprise before 3.0.1 includes version information for the Apache and Phusion Passenger products in its HTTP response headers, which allows remote attackers to obtain sensitive information.
CVE-2013-4962
The reset password page in Puppet Enterprise before 3.0.1 does not force entry of the current password, which allows attackers to modify user passwords by leveraging session hijacking, an unattended workstation, or other vectors.
CVE-2013-4964
Puppet Enterprise before 3.0.1 does not set the secure flag for the session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2013-4967
Puppet Enterprise before 3.0.1 allows remote attackers to obtain the database password via vectors related to how the password is "seeded as a console parameter," External Node Classifiers, and the lack of access control for /nodes.
CVE-2013-5316
Cross-site request forgery (CSRF) vulnerability in RiteCMS 1.0.0 allows remote attackers to hijack the authentication of administrators for requests that change the administrator password via an edit user action to cms/index.php.
CVE-2013-5317
Cross-site scripting (XSS) vulnerability in RiteCMS 1.0.0 allows remote authenticated users to inject arbitrary web script or HTML via the mode parameter to cms/index.php.
CVE-2013-5318
SQL injection vulnerability in Ginkgo CMS 5.0 allows remote attackers to execute arbitrary SQL commands via the rang parameter to index.php.
CVE-2013-5319
Cross-site scripting (XSS) vulnerability in secure/admin/user/views/deleteuserconfirm.jsp in the Admin Panel in Atlassian JIRA before 6.0.5 allows remote attackers to inject arbitrary web script or HTML via the name parameter to secure/admin/user/DeleteUser!default.jspa.
CVE-2013-5320
Cross-site scripting (XSS) vulnerability in Forums/EditPost.aspx in mojoPortal before 2.3.9.8 allows remote attackers to inject arbitrary web script or HTML via the txtSubject parameter.
CVE-2013-5321
Multiple SQL injection vulnerabilities in AlienVault Open Source Security Information Management (OSSIM) 4.1 allow remote attackers to execute arbitrary SQL commands via the (1) sensor parameter in a Query action to forensics/base_qry_main.php; the (2) tcp_flags[] or (3) tcp_port[0][4] parameter to forensics/base_stat_alerts.php; the (4) ip_addr[1][8] or (5) port_type parameter to forensics/base_stat_ports.php; or the (6) sortby or (7) rvalue parameter in a search action to vulnmeter/index.php.
CVE-2013-5322
SQL injection vulnerability in the CoolURI extension before 1.0.30 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2013-5323
Cross-site scripting (XSS) vulnerability in the Static Info Tables (static_info_tables) extension before 2.3.1 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
