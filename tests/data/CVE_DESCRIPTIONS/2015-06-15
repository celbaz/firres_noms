CVE-2015-3209
Heap-based buffer overflow in the PCNET controller in QEMU allows remote attackers to execute arbitrary code by sending a packet with TXSTATUS_STARTPACKET set and then a crafted packet with TXSTATUS_DEVICEOWNS set.
CVE-2015-4093
Cross-site scripting (XSS) vulnerability in Elasticsearch Kibana 4.x before 4.0.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4118
SQL injection vulnerability in monitor/show_sys_state.php in ISPConfig before 3.0.5.4p7 allows remote authenticated users with monitor permissions to execute arbitrary SQL commands via the server parameter.  NOTE: this can be leveraged by remote attackers using CVE-2015-4119.2.
CVE-2015-4119
Multiple cross-site request forgery (CSRF) vulnerabilities in ISPConfig before 3.0.5.4p7 allow remote attackers to hijack the authentication of (1) administrators for requests that create an administrator account via a request to admin/users_edit.php or (2) arbitrary users for requests that conduct SQL injection attacks via the server parameter to monitor/show_sys_state.php.
CVE-2015-4141
The WPS UPnP function in hostapd, when using WPS AP, and wpa_supplicant, when using WPS external registrar (ER), 0.7.0 through 2.4 allows remote attackers to cause a denial of service (crash) via a negative chunk length, which triggers an out-of-bounds read or heap-based buffer overflow.
CVE-2015-4142
Integer underflow in the WMM Action frame parser in hostapd 0.5.5 through 2.4 and wpa_supplicant 0.7.0 through 2.4, when used for AP mode MLME/SME functionality, allows remote attackers to cause a denial of service (crash) via a crafted frame, which triggers an out-of-bounds read.
CVE-2015-4143
The EAP-pwd server and peer implementation in hostapd and wpa_supplicant 1.0 through 2.4 allows remote attackers to cause a denial of service (out-of-bounds read and crash) via a crafted (1) Commit or (2) Confirm message payload.
CVE-2015-4144
The EAP-pwd server and peer implementation in hostapd and wpa_supplicant 1.0 through 2.4 does not validate that a message is long enough to contain the Total-Length field, which allows remote attackers to cause a denial of service (crash) via a crafted message.
CVE-2015-4145
The EAP-pwd server and peer implementation in hostapd and wpa_supplicant 1.0 through 2.4 does not validate a fragment is already being processed, which allows remote attackers to cause a denial of service (memory leak) via a crafted message.
CVE-2015-4146
The EAP-pwd peer implementation in hostapd and wpa_supplicant 1.0 through 2.4 does not clear the L (Length) and M (More) flags before determining if a response should be fragmented, which allows remote attackers to cause a denial of service (crash) via a crafted message.
CVE-2015-4152
Directory traversal vulnerability in the file output plugin in Elasticsearch Logstash before 1.4.3 allows remote attackers to write to arbitrary files via vectors related to dynamic field references in the path option.
CVE-2015-4163
GNTTABOP_swap_grant_ref in Xen 4.2 through 4.5 does not check the grant table operation version, which allows local guest domains to cause a denial of service (NULL pointer dereference) via a hypercall without a GNTTABOP_setup_table or GNTTABOP_set_version.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-4164
The compat_iret function in Xen 3.1 through 4.5 iterates the wrong way through a loop, which allows local 32-bit PV guest administrators to cause a denial of service (large loop and system hang) via a hypercall_iret call with EFLAGS.VM set.
CVE-2015-4344
The Services Basic Authentication module 7.x-1.x through 7.x-1.3 for Drupal allows remote attackers to bypass intended resource restrictions via vectors related to page caching.
CVE-2015-4345
The RESTWS Basic Auth submodule in the RESTful Web Services module 7.x-1.x before 7.x-1.5 and 7.x-2.x before 7.x-2.3 for Drupal caches pages for authenticated requests, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2015-4346
Cross-site scripting (XSS) vulnerability in the SMS Framework module 6.x-1.x before 6.x-1.1 for Drupal, when the "Send to phone" submodule is enabled, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to message previews.
CVE-2015-4347
Cross-site scripting (XSS) vulnerability in the inLinks Integration module for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified path arguments.
CVE-2015-4348
SQL injection vulnerability in the Spider Contacts module for Drupal allows remote authenticated users with the "access Spider Contacts category administration" permission to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-4349
Cross-site request forgery (CSRF) vulnerability in the Spider Contacts module for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete contact categories via unspecified vectors.
CVE-2015-4350
Multiple cross-site request forgery (CSRF) vulnerabilities in the Spider Catalog module for Drupal allow remote attackers to hijack the authentication of administrators for requests that delete (1) products, (2) ratings, or (3) categories via unspecified vectors.
CVE-2015-4351
The Spider Video Player module for Drupal allows remote authenticated users with the "access Spider Video Player administration" permission to delete arbitrary files via a crafted URL.
CVE-2015-4352
Cross-site request forgery (CSRF) vulnerability in the Spider Video Player module for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete videos via unspecified vectors.
CVE-2015-4353
Cross-site request forgery (CSRF) vulnerability in the Custom Sitemap module for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete sitemaps via unspecified vectors.
CVE-2015-4354
Cross-site scripting (XSS) vulnerability in the Ubercart Webform Integration module before 6.x-1.8 and 7.x before 7.x-2.4 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4355
Cross-site request forgery (CSRF) vulnerability in the Watchdog Aggregator module for Drupal allows remote attackers to hijack the authentication of administrators for requests that enable or disable monitoring sites via unspecified vectors.
CVE-2015-4356
Cross-site scripting (XSS) vulnerability in the view-based webform results table in the Webform module 7.x-4.x before 7.x-4.4 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via a webform.
CVE-2015-4357
Cross-site scripting (XSS) vulnerability in the Webform module before 6.x-3.22, 7.x-3.x before 7.x-3.22, and 7.x-4.x before 7.x-4.4 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via a node title, which is used as the default title of a webform block.
CVE-2015-4358
Cross-site scripting (XSS) vulnerability in unspecified administration pages in the Ubercart Discount Coupons module 6.x-1.x before 6.x-1.8 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via vectors related to taxonomy terms.
CVE-2015-4359
Multiple cross-site scripting (XSS) vulnerabilities in the Registration codes module before 6.x-1.6, 6.x-2.x before 6.x-2.8, and 7.x-1.x before 7.x-1.2 for Drupal allow remote authenticated users with permission to create or edit taxonomy terms or nodes to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4360
Cross-site request forgery (CSRF) vulnerability in the Registration codes module before 6.x-1.6, 6.x-2.x before 6.x-2.8, and 7.x-1.x before 7.x-1.2 for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete role-rules via unspecified vectors.
CVE-2015-4361
Cross-site request forgery (CSRF) vulnerability in the Registration codes module before 6.x-1.6 for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete registration codes via unspecified vectors.
CVE-2015-4362
Cross-site request forgery (CSRF) vulnerability in tracking_code.admin.inc in the Tracking Code module 7.x-1.x before 7.x-1.6 for Drupal allows remote attackers to hijack the authentication of administrators for requests that disable tracking codes via unspecified vectors.
CVE-2015-4363
Open redirect vulnerability in the finder_form_goto function in the Finder module for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-4364
Multiple cross-site request forgery (CSRF) vulnerabilities in includes/campaignmonitor_lists.admin.inc in the Campaign Monitor module 7.x-1.0 for Drupal allow remote attackers to hijack the authentication of users for requests that (1) enable list subscriptions via a request to admin/config/services/campaignmonitor/lists/%/enable or (2) disable list subscriptions via a request to admin/config/services/campaignmonitor/lists/%/disable. NOTE: this refers to an issue in an independently developed Drupal module, and NOT an issue in the Campaign Monitor software itself (described on the campaignmonitor.com web site).
CVE-2015-4365
Cross-site scripting (XSS) vulnerability in the Taxonomy Accordion module for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via vectors related to taxonomy terms.
CVE-2015-4366
Cross-site scripting (XSS) vulnerability in the Mover module 6.x-1.0 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4367
Cross-site scripting (XSS) vulnerability in the Simple Subscription module before 6.x-1.1 and 7.x-1.x before 7.x-1.1 for Drupal allows remote authenticated users with the "administer blocks" permission to inject arbitrary web script or HTML via vectors related to block content.
CVE-2015-4368
The Commerce Ogone module 7.x-1.x before 7.x-1.5 for Drupal allows remote attackers to complete the checkout for an order without paying via unspecified vectors.
CVE-2015-4369
Cross-site scripting (XSS) vulnerability in the Trick Question module before 6.x-1.5 and 7.x-1.x before 7.x-1.5 for Drupal allows remote authenticated users with the "Administer Trick Question" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4370
Cross-site scripting (XSS) vulnerability in the Site Documentation module before 6.x-1.5 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via vectors related to taxonomy terms.
CVE-2015-4371
Open redirect vulnerability in the Perfecto module before 7.x-1.2 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in an unspecified parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-4372
Cross-site scripting (XSS) vulnerability in the Image Title module before 7.x-1.1 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4373
Cross-site scripting (XSS) vulnerability in the OG tabs module before 7.x-1.1 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via vectors related to nodes posted in an Organic Groups group.
CVE-2015-4375
The Chaos tool suite (ctools) module 7.x-1.x before 7.x-1.7 for Drupal allows remote attackers to obtain sensitive node titles via (1) an autocomplete search on custom entities without an access query tag or (2) leveraging knowledge of the ID of an entity.
CVE-2015-4376
Cross-site scripting (XSS) vulnerability in the Profile2 Privacy module 7.x-1.x before 7.x-1.5 for Drupal allows remote authenticated users with the "Administer Profile2 Privacy Levels" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4377
Cross-site scripting (XSS) vulnerability in unspecified administration pages in the Petition module 6.x-1.x before 6.x-1.3 for Drupal allows remote authenticated users with the "create petition" permission to inject arbitrary web script or HTML via unknown vectors.
CVE-2015-4378
Cross-site scripting (XSS) vulnerability in the Crumbs module 7.x-2.x before 7.x-2.3 for Drupal allows remote authenticated users with the "Administer Crumbs" permission to inject arbitrary web script or HTML via a custom breadcrumb separator.
CVE-2015-4379
Cross-site request forgery (CSRF) vulnerability in the Webform Multiple File Upload module 6.x-1.x before 6.x-1.3 and 7.x-1.x before 7.x-1.3 for Drupal allows remote attackers to hijack the authentication of certain users for requests that delete files via unspecified vectors.
CVE-2015-4380
Cross-site scripting (XSS) vulnerability in the Linear Case module 6.x-1.x before 6.x-1.3 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4381
Cross-site scripting (XSS) vulnerability in the Invoice module 6.x-1.x before 6.x-1.2 and 7.x-1.x before 7.x-1.3 for Drupal allows remote authenticated users with the "Administer own invoices" permission to inject arbitrary web script or HTML via unspecified vectors involving nodes of the "Invoice" content type.
CVE-2015-4382
Multiple cross-site request forgery (CSRF) vulnerabilities in the Invoice module 6.x-1.x before 6.x-1.2 and 7.x-1.x before 7.x-1.3 for Drupal allow remote attackers to hijack the authentication of arbitrary users for requests that (1) create, (2) delete, or (3) alter invoices via unspecified vectors.
CVE-2015-4383
Cross-site request forgery (CSRF) vulnerability in the Decisions module for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that remove individual voters via unspecified vectors.
CVE-2015-4384
Cross-site scripting (XSS) vulnerability in the Ubercart Webform Checkout Pane module 6.x-3.x before 6.x-3.10 and 7.x-3.x before 7.x-3.11 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4385
Cross-site scripting (XSS) vulnerability in unspecified administration pages in the Imagefield Info module 7.x-1.x before 7.x-1.2 for Drupal allows remote authenticated users with the "Administer image styles" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4386
Multiple cross-site scripting (XSS) vulnerabilities in unspecified administration pages in the EntityBulkDelete module 7.x-1.0 for Drupal allow remote attackers to inject arbitrary web script or HTML via unknown vectors involving creating or editing (1) comments, (2) taxonomy terms, or (3) nodes.
CVE-2015-4387
Cross-site scripting (XSS) vulnerability in unspecified administration pages in the Password Policy module 6.x-1.x before 6.x-1.11 and 7.x-1.x before 7.x-1.11 for Drupal, when a site has a policy that uses the username constraint, allows remote attackers to inject arbitrary web script or HTML via a crafted username that is imported from an external source.
CVE-2015-4388
Cross-site scripting (XSS) vulnerability in the Current Search Links module 7.x-1.x before 7.x-1.1 for Drupal, when the "Append the keywords passed by the user to the list" option is disabled, allows remote attackers to inject arbitrary web script or HTML via a crafted search query.
CVE-2015-4389
The Open Graph Importer (og_tag_importer) 7.x-1.x for Drupal does not properly check the create permission for content types created during import, which allows remote authenticated users to bypass intended restrictions by leveraging the "import og_tag_importer" permission.
CVE-2015-4390
Multiple cross-site request forgery (CSRF) vulnerabilities in the User Import module 6.x-4.x before 6.x-4.4 and 7.x-2.x before 7.x-2.3 for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) continue or (2) delete an ongoing import via unspecified vectors.
CVE-2015-4391
Cross-site request forgery (CSRF) vulnerability in the CiviCRM private report module 6.x-1.x before 6.x-1.2 and 7.x-1.x before 7.x-1.3 for Drupal allows remote attackers to hijack the authentication of users for requests that delete reports via unspecified vectors.
CVE-2015-4392
Cross-site scripting (XSS) vulnerability in the Display Suite module 7.x-2.7 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors related to field display settings.
CVE-2015-4393
The resource/endpoint for uploading files in the Services module 7.x-3.x before 7.x-3.12 for Drupal allows remote authenticated users with the "Save file information" permission to execute arbitrary code via a crafted filename.
CVE-2015-4394
The Services module 7.x-3.x before 7.x-3.12 for Drupal allows remote attackers to bypass the field_access restriction and obtain sensitive private field information via unspecified vectors.
CVE-2015-4395
The HybridAuth Social Login module 7.x-2.x before 7.x-2.10 for Drupal stores passwords in plaintext when the "Ask user for a password when registering" option is enabled, which allows remote authenticated users with certain permissions to obtain sensitive information by leveraging access to the database.
CVE-2015-4396
Multiple cross-site request forgery (CSRF) vulnerabilities in the Keyword Research module 6.x-1.x before 6.x-1.2 for Drupal allow remote attackers to hijack the authentication of users with the "kwresearch admin site keywords" permission for requests that (1) create, (2) delete, or (3) set priorities to keywords via unspecified vectors.
CVE-2015-4397
Cross-site request forgery (CSRF) vulnerability in the Node Template module for Drupal allows remote attackers to hijack the authentication of users with the "access node template" permission for requests that delete node templates via unspecified vectors.
CVE-2015-4559
Cross-site scripting (XSS) vulnerability in the product deployment feature in the Java core web services in Intel McAfee ePolicy Orchestrator (ePO) before 5.1.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
