CVE-2007-2835
Multiple stack-based buffer overflows in (1) CCE_pinyin.c and (2) xl_pinyin.c in ImmModules/cce/ in unicon-imc2 3.0.4, as used by zhcon and other applications, allow local users to gain privileges via a long HOME environment variable.
CVE-2007-2837
The (1) getRule and (2) getChains functions in server/rules.cpp in fireflierd (fireflier-server) in FireFlier 1.1.6 allow local users to overwrite arbitrary files via a symlink attack on the /tmp/fireflier.rules temporary file.
CVE-2007-2838
The populate_conns function in src/populate_conns.c in GSAMBAD 0.1.4 allows local users to overwrite arbitrary files via a symlink attack on the /tmp/gsambadtmp temporary file.
CVE-2007-3508
** DISPUTED **  Integer overflow in the process_envvars function in elf/rtld.c in glibc before 2.5-rc4 might allow local users to execute arbitrary code via a large LD_HWCAP_MASK environment variable value.  NOTE: the glibc maintainers state that they do not believe that this issue is exploitable for code execution.
CVE-2007-3511
The focus handling for the onkeydown event in Mozilla Firefox 1.5.0.12, 2.0.0.4 and other versions before 2.0.0.8, and SeaMonkey before 1.1.5 allows remote attackers to change field focus and copy keystrokes via the "for" attribute in a label, which bypasses the focus prevention, as demonstrated by changing focus from a textarea to a file upload field.
CVE-2007-3512
Stack-based buffer overflow in Lhaca File Archiver before 1.22 allows user-assisted remote attackers to execute arbitrary code via a large LHA "Extended Header Size" value in an LZH archive, a different issue than CVE-2007-3375.
CVE-2007-3513
The lcd_write function in drivers/usb/misc/usblcd.c in the Linux kernel before 2.6.22-rc7 does not limit the amount of memory used by a caller, which allows local users to cause a denial of service (memory consumption).
CVE-2007-3514
Cross-domain vulnerability in Apple Safari for Windows 3.0.2 allows remote attackers to bypass the Same Origin Policy and access restricted information from other domains via JavaScript that overwrites the document variable and statically sets the document.domain attribute to a file:// location, a different vector than CVE-2007-3482.
CVE-2007-3515
SQL injection vulnerability in view_event.php in TotalCalendar 2.402 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-3516
Multiple cross-site scripting (XSS) vulnerabilities in kayit.asp in Gorki Online Santrac Sitesi allow remote attackers to inject arbitrary web script or HTML via the (1) kullanici, (2) posta, or (3) takim_adi parameter to uyeler.asp.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3517
Multiple cross-site scripting (XSS) vulnerabilities in Claroline 1.8.3 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO (PHP_SELF) to (1) index.php, (2) demo/claroline170/index.php, and possibly other scripts.
CVE-2007-3518
SQL injection vulnerability in msg.php in HispaH YouTube Clone Script (youtubeclone) allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-3519
SQL injection vulnerability in eventdisplay.php in phpEventCalendar 0.2.3 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-3520
SQL injection vulnerability in process.php in Easybe 1-2-3 Music Store allows remote attackers to execute arbitrary SQL commands via the CategoryID parameter.
CVE-2007-3521
SQL injection vulnerability in ArcadeBuilder Game Portal Manager 1.7 allows remote attackers to execute arbitrary SQL commands via a usercookie cookie.
CVE-2007-3522
Multiple PHP remote file inclusion vulnerabilities in sPHPell 1.01 allow remote attackers to execute arbitrary PHP code via a URL in the SpellIncPath parameter to (1) spellcheckpageinc.php, (2) spellchecktext.php, (3) spellcheckwindow.php, or (4) spellcheckwindowframeset.php.
CVE-2007-3523
Multiple directory traversal vulnerabilities in Module/Galerie.php in XCMS 1.1 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the (1) Ent or (2) Lang parameter.
CVE-2007-3524
Multiple PHP remote file inclusion vulnerabilities in Ripe Website Manager 0.8.9 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the level parameter to (1) admin/includes/author_panel_header.php or (2) admin/includes/admin_header.php.
Successful exploitation of this vulnerability requires that "register_globals" is enabled.
CVE-2007-3525
Ripe Website Manager 0.8.9 and earlier allows remote attackers to obtain configuration information via a direct request to includes/phpinfo.php, which calls the phpinfo function.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3526
Multiple SQL injection vulnerabilities in Buddy Zone 1.5 and earlier allow remote attackers to execute arbitrary SQL commands via (1) the news_id parameter to view_news.php, (2) the cat_id parameter to view_events.php, or (3) the member_id parameter to video_gallery.php.
CVE-2007-3527
Integer overflow in Firebird 2.0.0 allows remote authenticated users to cause a denial of service (CPU consumption) via certain database operations with multi-byte character sets that trigger an attempt to use the value 65536 for a 16-bit integer, which is treated as 0 and causes an infinite loop on zero-length data.
CVE-2007-3528
The blowfish mode in DAR before 2.3.4 uses weak Blowfish-CBC cryptography by (1) discarding random bits by the blowfish::make_ivec function in libdar/crypto.cpp that results in predictable and repeating IV values, and (2) direct use of a password for keying, which makes it easier for context-dependent attackers to decrypt files.
CVE-2007-3529
videos.php in PHPDirector 0.21 and earlier allows remote attackers to obtain sensitive information via an empty value of the id[] parameter, which reveals the path in an error message.
CVE-2007-3530
PHPDirector 0.21 and earlier stores the admin account name and password in config.php, which allows local users to gain privileges by reading this file.
CVE-2007-3533
The 3Com IntelliJack Switch NJ220 before 2.0.23 allows remote attackers to cause a denial of service (reboot and reporting outage) via a loopback packet with zero in the length field.
CVE-2007-3534
SQL injection vulnerability in login.php in WebChat 0.78 allows remote attackers to execute arbitrary SQL commands via the rid parameter.
CVE-2007-3535
Multiple directory traversal vulnerabilities in GL-SH Deaf Forum 6.4.4 and earlier allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the (1) FORUM_LANGUAGE parameter to functions.php or the (2) style parameter to bottom.php.
Successful exploitation of this vulnerability requires that "magic_quotes_gpc" is disabled.
CVE-2007-3536
Multiple buffer overflows in the AMX NetLinx VNC (AmxVnc) ActiveX control in AmxVnc.dll 1.0.13.0 allow remote attackers to execute arbitrary code via long (1) Host, (2) Password, or (3) LogFile property values.
CVE-2007-3537
IBM OS/400 (aka i5/OS) V4R2M0 through V5R3M0 on iSeries machines sends responses to TCP SYN-FIN packets, which allows remote attackers to obtain system information and possibly bypass firewall rules.
CVE-2007-3538
SQL injection vulnerability in qtg_msg_view.php in QuickTalk guestbook 1.2 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-3539
Multiple SQL injection vulnerabilities in QuickTicket 1.2 build:20070621 and QuickTalk Forum 1.3 allow remote attackers to execute arbitrary SQL commands via the (1) t and (2) f parameters in (a) qti_ind_post.php and (b) qti_ind_post_prt.php; (3) dir and (4) order parameters in qti_ind_member.php; (5) id parameter in qti_usr.php; and the (6) f parameter in qti_ind_topic.php.  NOTE: it was later reported that vector 5 also affects 1.4, 1.5, and 1.5.0.3.
CVE-2007-3540
Multiple cross-site scripting (XSS) vulnerabilities in search.asp in rwAuction Pro 5.0 allow remote attackers to inject arbitrary web script or HTML via the (1) search, (2) show, (3) searchtype, (4) catid, and (5) searchtxt parameters, a different version and vectors than CVE-2005-4060.
CVE-2007-3541
Cross-site scripting (XSS) vulnerability in Kurinton sHTTPd 20070408 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-3542
Cross-site scripting (XSS) vulnerability in admin/auth.php in Pluxml 0.3.1 allows remote attackers to inject arbitrary web script or HTML via the msg parameter.
CVE-2007-3543
Unrestricted file upload vulnerability in WordPress before 2.2.1 and WordPress MU before 1.2.3 allows remote authenticated users to upload and execute arbitrary PHP code by making a post that specifies a .php filename in the _wp_attached_file metadata field; and then sending this file's content, along with its post_ID value, to (1) wp-app.php or (2) app.php.
Successful exploitation requires valid Editor credentials and that the system is configured to allow uploads.
CVE-2007-3544
Unrestricted file upload vulnerability in (1) wp-app.php and (2) app.php in WordPress 2.2.1 and WordPress MU 1.2.3 allows remote authenticated users to upload and execute arbitrary PHP code via unspecified vectors, possibly related to the wp_postmeta table and the use of custom fields in normal (non-attachment) posts.  NOTE: this issue reportedly exists because of an incomplete fix for CVE-2007-3543.
CVE-2007-3545
Buffer overflow in Warzone 2100 Resurrection before 2.0.7 allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via a long filename when setting background music.
CVE-2007-3546
Cross-site scripting (XSS) vulnerability in the Windows GUI in Nessus Vulnerability Scanner before 3.0.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-3547
Directory traversal vulnerability in qti_checkname.php in QuickTicket 1.2 allows remote attackers to include and execute arbitrary local files a .. (dot dot) in the lang parameter.
CVE-2007-3548
Stack-based buffer overflow in W3Filer 2.1.3 allows remote FTP servers to cause a denial of service (application hang or crash) and possibly execute arbitrary code by sending a large banner to a client that is sending a file.
CVE-2007-3549
SQL injection vulnerability in view_sub_cat.php in Buddy Zone 1.5 allows remote attackers to execute arbitrary SQL commands via the cat_id parameter.
CVE-2007-3550
** DISPUTED **  Microsoft Internet Explorer 6.0 and 7.0 allows remote attackers to fill Zones with arbitrary domains using certain metacharacters such as wildcards via JavaScript, which results in a denial of service (website suppression and resource consumption), aka "Internet Explorer Zone Domain Specification Dos and Page Suppressing".  NOTE: this issue has been disputed by a third party, who states that the zone settings cannot be manipulated.
CVE-2007-3551
Buffer overflow in bbs100 before 3.2 allows remote attackers to cause a denial of service (crash) by attempting to login as the Guest user when another Guest user is already logged in, possibly related to the state_login_prompt function in state_login.c.
CVE-2007-3552
Multiple unspecified vulnerabilities in bbs100 before 3.2 allow remote attackers to cause a denial of service (crash) via unspecified vectors, possibly involving certain v*printf and shift_StringIO functions.  NOTE: some details were obtained from third party information.
CVE-2007-3553
Cross-site scripting (XSS) vulnerability in Rapid Install Web Server in Oracle Application Server 11i allows remote attackers to inject arbitrary web script or HTML via a URL to the "Secondary Login Page", as demonstrated using (1) pls/ and (2) pls/MSBEP004/.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
