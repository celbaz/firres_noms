CVE-2016-6019
IBM Emptoris Strategic Supply Management Platform 10.0.0.x through 10.1.1.x is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 116739.
CVE-2016-8951
IBM Emptoris Strategic Supply Management Platform 10.0.0.x through 10.1.1.x is vulnerable to a denial of service attack. An attacker can exploit a vulnerability in the authentication features that could log out users and flood user accounts with emails. IBM X-Force ID: 118838.
CVE-2016-8952
IBM Emptoris Strategic Supply Management Platform 10.0.0.x through 10.1.1.x is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 118839.
CVE-2016-8964
IBM BigFix Inventory v9 9.2 uses an inadequate account lockout setting that could allow a remote attacker to brute force account credentials. IBM X-Force ID: 118853.
CVE-2017-11103
Heimdal before 7.4 allows remote attackers to impersonate services with Orpheus' Lyre attacks because it obtains service-principal names in a way that violates the Kerberos 5 protocol specification. In _krb5_extract_ticket() the KDC-REP service name must be obtained from the encrypted version stored in 'enc_part' instead of the unencrypted version stored in 'ticket'. Use of the unencrypted version provides an opportunity for successful server impersonation and other attacks. NOTE: this CVE is only for Heimdal and other products that embed Heimdal code; it does not apply to other instances in which this part of the Kerberos 5 protocol specification is violated.
CVE-2017-11173
Missing anchor in generated regex for rack-cors before 0.4.1 allows a malicious third-party site to perform CORS requests. If the configuration were intended to allow only the trusted example.com domain name and not the malicious example.net domain name, then example.com.example.net (as well as example.com-example.net) would be inadvertently allowed.
CVE-2017-11198
Cross-site scripting (XSS) vulnerability in /application/lib/ajax/get_image.php in FineCMS through 2017-07-12 allows remote attackers to inject arbitrary web script or HTML via the folder, id, or name parameter.
CVE-2017-11200
SQL Injection exists in FineCMS through 2017-07-12 via the application/core/controller/excludes.php visitor_ip parameter.
CVE-2017-11201
application/core/controller/images.php in FineCMS through 2017-07-12 allows remote authenticated admins to conduct XSS attacks by uploading an image via a route=images action.
CVE-2017-11202
FineCMS through 2017-07-12 allows XSS in visitors.php because JavaScript in visited URLs is not restricted either during logging or during the reading of logs, a different vulnerability than CVE-2017-11180.
CVE-2017-11310
The read_user_chunk_callback function in coders\png.c in ImageMagick 7.0.6-1 Q16 2017-06-21 (beta) has memory leak vulnerabilities via crafted PNG files.
CVE-2017-1308
IBM Daeja ViewONE Professional, Standard & Virtual 4.1.5.1 and 5.0 could allow an authenticated attacker to download files they should not have access to due to improper access controls. IBM X-Force ID: 125462.
CVE-2017-6249
An elevation of privilege vulnerability in the NVIDIA sound driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-34373711. References: N-CVE-2017-6249.
CVE-2017-7529
Nginx versions since 0.5.6 up to and including 1.13.2 are vulnerable to integer overflow vulnerability in nginx range filter module resulting into leak of potentially sensitive information triggered by specially crafted request.
CVE-2017-7672
If an application allows enter an URL in a form field and built-in URLValidator is used, it is possible to prepare a special URL which will be used to overload server process when performing validation of the URL. Solution is to upgrade to Apache Struts version 2.5.12.
CVE-2017-9787
When using a Spring AOP functionality to secure Struts actions it is possible to perform a DoS attack. Solution is to upgrade to Apache Struts version 2.5.12 or 2.3.33.
CVE-2017-9788
In Apache httpd before 2.2.34 and 2.4.x before 2.4.27, the value placeholder in [Proxy-]Authorization headers of type 'Digest' was not initialized or reset before or between successive key=value assignments by mod_auth_digest. Providing an initial key with no '=' assignment could reflect the stale value of uninitialized pool memory used by the prior request, leading to leakage of potentially confidential information, and a segfault in other cases resulting in denial of service.
CVE-2017-9789
When under stress, closing many connections, the HTTP/2 handling code in Apache httpd 2.4.26 would sometimes access memory after it has been freed, resulting in potentially erratic behaviour.
