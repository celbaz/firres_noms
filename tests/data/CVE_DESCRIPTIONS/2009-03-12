CVE-2009-0366
The uncompress_buffer function in src/server/simple_wml.cpp in Wesnoth before r33069 allows remote attackers to cause a denial of service via a large compressed WML document.
CVE-2009-0632
The IP Phone Personal Address Book (PAB) Synchronizer feature in Cisco Unified Communications Manager (aka CUCM, formerly CallManager) 4.1, 4.2 before 4.2(3)SR4b, 4.3 before 4.3(2)SR1b, 5.x before 5.1(3e), 6.x before 6.1(3), and 7.0 before 7.0(2) sends privileged directory-service account credentials to the client in cleartext, which allows remote attackers to modify the CUCM configuration and perform other privileged actions by intercepting these credentials, and then using them in requests unrelated to the intended synchronization task, as demonstrated by (1) DC Directory account credentials in CUCM 4.x and (2) TabSyncSysUser account credentials in CUCM 5.x through 7.x.
Per: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a8643c.shtml

"Impact

Successful exploitation of this vulnerability may allow an attacker to intercept user credentials that allow the attacker to escalate their privilege level and obtain complete administrative access to a vulnerable Cisco Unified Communications Manager system. If integrated with an external directory service, the intercepted user credentials may allow an attacker to gain access to additional systems configured to use the directory service for authentication."
CVE-2009-0778
The icmp_send function in net/ipv4/icmp.c in the Linux kernel before 2.6.25, when configured as a router with a REJECT route, does not properly manage the Protocol Independent Destination Cache (aka DST) in some situations involving transmission of an ICMP Host Unreachable message, which allows remote attackers to cause a denial of service (connectivity outage) by sending a large series of packets to many destination IP addresses within this REJECT route, related to an "rt_cache leak."
CVE-2009-0874
Multiple unspecified vulnerabilities in the Doors subsystem in the kernel in Sun Solaris 8 through 10, and OpenSolaris before snv_94, allow local users to cause a denial of service (process hang), or possibly bypass file permissions or gain kernel-context privileges, via vectors including ones related to (1) an argument handling deadlock in a door server and (2) watchpoint problems in the door_call function.
CVE-2009-0875
Race condition in the Doors subsystem in the kernel in Sun Solaris 8 through 10, and OpenSolaris before snv_94, allows local users to cause a denial of service (process hang), or possibly bypass file permissions or gain kernel-context privileges, via vectors involving the time at which control is transferred from a caller to a door server.
CVE-2009-0876
Sun xVM VirtualBox 2.0.0, 2.0.2, 2.0.4, 2.0.6r39760, 2.1.0, 2.1.2, and 2.1.4r42893 on Linux allows local users to gain privileges via a hardlink attack, which preserves setuid/setgid bits on Linux, related to DT_RPATH:$ORIGIN.
Per: http://sunsolve.sun.com/search/document.do?assetkey=1-66-254568-1

"5. Resolution

This issue is addressed in the following releases:

Linux

    * Sun xVM VirtualBox 2.0.6r43001
    * Sun xVM VirtualBox 2.1.4r43001"
CVE-2009-0877
Multiple cross-site scripting (XSS) vulnerabilities in Sun Java System Communications Express allow remote attackers to inject arbitrary web script or HTML via the (1) Full Name or (2) Subject field.
CVE-2009-0878
The read_game_map function in src/terrain_translation.cpp in Wesnoth before r32987 allows remote attackers to cause a denial of service (memory consumption and daemon hang) via a map with a large (1) width or (2) height.
CVE-2009-0879
The CIM server in IBM Director before 5.20.3 Service Update 2 on Windows allows remote attackers to cause a denial of service (daemon crash) via a long consumer name, as demonstrated by an M-POST request to a long /CIMListener/ URI.
CVE-2009-0880
Directory traversal vulnerability in the CIM server in IBM Director before 5.20.3 Service Update 2 on Windows allows remote attackers to load and execute arbitrary local DLL code via a .. (dot dot) in a /CIMListener/ URI in an M-POST request.
Per: http://www.securityfocus.com/archive/1/archive/1/501639/100/0/threaded

"The vendor has adressed this vulnerability in service update 2 for IBM
Director agent 5.20.3. Download link:

https://www14.software.ibm.com/webapp/iwm/web/reg/download.do?source=dmp
&S_PKG=director_x_520&S_TACT=sms<=en_US&cp=UTF-8"
CVE-2009-0881
SQL injection vulnerability in ejemplo/paises.php in isiAJAX 1 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-0882
Multiple SQL injection vulnerabilities in nForum 1.5 allow remote attackers to execute arbitrary SQL commands via the (1) id parameter to showtheme.php and the (2) user parameter to userinfo.php.
CVE-2009-0883
SQL injection vulnerability in Blue Eye CMS 1.0.0 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the BlueEyeCMS_login cookie parameter.
CVE-2009-0884
Buffer overflow in FileZilla Server before 0.9.31 allows remote attackers to cause a denial of service via unspecified vectors related to SSL/TLS packets.
CVE-2009-0885
Multiple heap-based buffer overflows in Media Commands 1.0 allow remote attackers to execute arbitrary code or cause a denial of service (application crash) via a long string in a (1) M3U, (2) M3l, (3) TXT, and (4) LRC playlist file.
CVE-2009-0886
Directory traversal vulnerability in login.php in OneOrZero Helpdesk 1.6.5.7 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the default_language parameter.
CVE-2009-0887
Integer signedness error in the _pam_StrTok function in libpam/pam_misc.c in Linux-PAM (aka pam) 1.0.3 and earlier, when a configuration file contains non-ASCII usernames, might allow remote attackers to cause a denial of service, and might allow remote authenticated users to obtain login access with a different user's non-ASCII username, via a login attempt.
