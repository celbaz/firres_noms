CVE-2013-0570
The Fibre Channel over Ethernet (FCoE) feature in IBM System Networking and Blade Network Technology (BNT) switches running IBM Networking Operating System (aka NOS, formerly BLADE Operating System) floods data frames with unknown MAC addresses out on all interfaces on the same VLAN, which might allow remote attackers to obtain sensitive information in opportunistic circumstances by eavesdropping on the broadcast domain. IBM X-Force ID: 83166.
CVE-2016-6542
The iTrack device tracking ID number, also called "LosserID" in the web API, can be obtained by being in the range of an iTrack device. The tracker ID is the device's BLE MAC address.
CVE-2016-6543
A captured MAC/device ID of an iTrack Easy can be registered under multiple user accounts allowing access to getgps GPS data, which can allow unauthenticated parties to track the device.
CVE-2016-6544
getgps data in iTrack Easy can be modified without authentication by setting the data using the parametercmd:setothergps. This vulnerability can be exploited to alter the GPS data of a lost device.
CVE-2016-6545
Session cookies are not used for maintaining valid sessions in iTrack Easy. The user's password is passed as a POST parameter over HTTPS using a base64 encoded passwd field on every request. In this implementation, sessions can only be terminated when the user changes the associated password.
CVE-2016-6546
The iTrack Easy mobile application stores the account password used to authenticate to the cloud API in base64-encoding in the cache.db file. The base64 encoding format is considered equivalent to cleartext.
CVE-2016-6547
The Zizai Tech Nut mobile app stores the account password used to authenticate to the cloud API in cleartext in the cache.db file.
CVE-2016-6548
The Zizai Tech Nut mobile app makes requests via HTTP instead of HTTPS. These requests contain the user's authenticated session token with the URL. An attacker can capture these requests and reuse the session token to gain full access the user's account.
CVE-2016-6549
The Zizai Tech Nut device allows unauthenticated Bluetooth pairing, which enables unauthenticated connected applications to write data to the device name attribute.
CVE-2016-6551
Intellian Satellite TV antennas t-Series and v-Series, firmware version 1.07, uses non-random default credentials of: ftp/ftp or intellian:12345678. A remote network attacker can gain elevated access to a vulnerable device.
CVE-2016-6552
Green Packet DX-350 uses non-random default credentials of: root:wimax. A remote network attacker can gain privileged access to a vulnerable device.
CVE-2016-6553
Nuuo NT-4040 Titan, firmware NT-4040_01.07.0000.0015_1120, uses non-random default credentials of: admin:admin and localdisplay:111111. A remote network attacker can gain privileged access to a vulnerable device.
CVE-2016-6554
Synology NAS servers DS107, firmware version 3.1-1639 and prior, and DS116, DS213, firmware versions prior to 5.2-5644-1, use non-random default credentials of: guest:(blank) and admin:(blank) . A remote network attacker can gain privileged access to a vulnerable device.
CVE-2016-6557
In ASUS RP-AC52 access points with firmware version 1.0.1.1s and possibly earlier, the web interface, the web interface does not sufficiently verify whether a valid request was intentionally provided by the user. An attacker can perform actions with the same permissions as a victim user, provided the victim has an active session and is induced to trigger the malicious request.
CVE-2016-6558
A command injection vulnerability exists in apply.cgi on the ASUS RP-AC52 access point, firmware version 1.0.1.1s and possibly earlier, web interface specifically in the action_script parameter. The action_script parameter specifies a script to be executed if the action_mode parameter does not contain a valid state. If the input provided by action_script does not match one of the hard coded options, then it will be executed as the argument of either a system() or an eval() call allowing arbitrary commands to be executed.
CVE-2016-6559
Improper bounds checking of the obuf variable in the link_ntoa() function in linkaddr.c of the BSD libc library may allow an attacker to read or write from memory. The full impact and severity depends on the method of exploit and how the library is used by applications. According to analysis by FreeBSD developers, it is very unlikely that applications exist that utilize link_ntoa() in an exploitable manner, and the CERT/CC is not aware of any proof of concept. A blog post describes the functionality of link_ntoa() and points out that none of the base utilities use this function in an exploitable manner. For more information, please see FreeBSD Security Advisory SA-16:37.
CVE-2016-6562
On iOS and Android devices, the ShoreTel Mobility Client app version 9.1.3.109 fails to properly validate SSL certificates provided by HTTPS connections, which means that an attacker in the position to perform MITM attacks may be able to obtain sensitive account information such as login credentials.
CVE-2016-6563
Processing malformed SOAP messages when performing the HNAP Login action causes a buffer overflow in the stack in some D-Link DIR routers. The vulnerable XML fields within the SOAP body are: Action, Username, LoginPassword, and Captcha. The following products are affected: DIR-823, DIR-822, DIR-818L(W), DIR-895L, DIR-890L, DIR-885L, DIR-880L, DIR-868L, and DIR-850L.
CVE-2016-6564
Android devices with code from Ragentek contain a privileged binary that performs over-the-air (OTA) update checks. Additionally, there are multiple techniques used to hide the execution of this binary. This behavior could be described as a rootkit. This binary, which resides as /system/bin/debugs, runs with root privileges and does not communicate over an encrypted channel. The binary has been shown to communicate with three hosts via HTTP: oyag[.]lhzbdvm[.]com oyag[.]prugskh[.]net oyag[.]prugskh[.]com Server responses to requests sent by the debugs binary include functionalities to execute arbitrary commands as root, install applications, or update configurations. Examples of a request sent by the client binary: POST /pagt/agent?data={"name":"c_regist","details":{...}} HTTP/1. 1 Host: 114.80.68.223 Connection: Close An example response from the server could be: HTTP/1.1 200 OK {"code": "01", "name": "push_commands", "details": {"server_id": "1" , "title": "Test Command", "comments": "Test", "commands": "touch /tmp/test"}} This binary is reported to be present in the following devices: BLU Studio G BLU Studio G Plus BLU Studio 6.0 HD BLU Studio X BLU Studio X Plus BLU Studio C HD Infinix Hot X507 Infinix Hot 2 X510 Infinix Zero X506 Infinix Zero 2 X509 DOOGEE Voyager 2 DG310 LEAGOO Lead 5 LEAGOO Lead 6 LEAGOO Lead 3i LEAGOO Lead 2S LEAGOO Alfa 6 IKU Colorful K45i Beeline Pro 2 XOLO Cube 5.0
CVE-2016-6565
The Imagely NextGen Gallery plugin for Wordpress prior to version 2.1.57 does not properly validate user input in the cssfile parameter of a HTTP POST request, which may allow an authenticated user to read arbitrary files from the server, or execute arbitrary code on the server in some circumstances (dependent on server configuration).
CVE-2016-6566
The valueAsString parameter inside the JSON payload contained by the ucLogin_txtLoginId_ClientStat POST parameter of the Sungard eTRAKiT3 software version 3.2.1.17 is not properly validated. An unauthenticated remote attacker may be able to modify the POST request and insert a SQL query which may then be executed by the backend server. eTRAKiT 3.2.1.17 was tested, but other versions may also be vulnerable.
CVE-2016-6567
SHDesigns' Resident Download Manager provides firmware update capabilities for Rabbit 2000/3000 CPU boards, which according to the reporter may be used in some industrial control and embedded applications. The Resident Download Manager does not verify that the firmware is authentic before executing code and deploying the firmware to devices. A remote attacker with the ability to send UDP traffic to the device may be able to execute arbitrary code on the device. According to SHDesigns' website, the Resident Download Manager and other Rabbit Tools have been discontinued since June 2011.
CVE-2016-6578
CodeLathe FileCloud, version 13.0.0.32841 and earlier, contains a global cross-site request forgery (CSRF) vulnerability. An attacker can perform actions with the same permissions as a victim user, provided the victim has an active session and is induced to trigger the malicious request.
CVE-2016-9482
Code generated by PHP FormMail Generator may allow a remote unauthenticated user to bypass authentication in the to access the administrator panel by navigating directly to /admin.php?mod=admin&func=panel
CVE-2016-9483
The PHP form code generated by PHP FormMail Generator deserializes untrusted input as part of the phpfmg_filman_download() function. A remote unauthenticated attacker may be able to use this vulnerability to inject PHP code, or along with CVE-2016-9484 to perform local file inclusion attacks and obtain files from the server.
CVE-2016-9484
The generated PHP form code does not properly validate user input folder directories, allowing a remote unauthenticated attacker to perform a path traversal and access arbitrary files on the server. The PHP FormMail Generator website does not use version numbers and is updated continuously. Any PHP form code generated by this website prior to 2016-12-06 may be vulnerable.
CVE-2016-9485
On Windows endpoints, the SecureConnector agent must run under the local SYSTEM account or another administrator account in order to enable full functionality of the agent. The typical configuration is for the agent to run as a Windows service under the local SYSTEM account. The SecureConnector agent runs various plugin scripts and executables on the endpoint in order to gather and report information about the host to the CounterACT management appliance. The SecureConnector agent downloads these scripts and executables as needed from the CounterACT management appliance and runs them on the endpoint. The SecureConnector agent fails to set any permissions on downloaded file objects. This allows a malicious user to take ownership of any of these files and make modifications to it, regardless of where the files are saved. These files are then executed under SYSTEM privileges. A malicious unprivileged user can overwrite these executable files with malicious code before the SecureConnector agent executes them, causing the malicious code to be run under the SYSTEM account.
CVE-2016-9486
On Windows endpoints, the SecureConnector agent must run under the local SYSTEM account or another administrator account in order to enable full functionality of the agent. The typical configuration is for the agent to run as a Windows service under the local SYSTEM account. The SecureConnector agent runs various plugin scripts and executables on the endpoint in order to gather and report information about the host to the CounterACT management appliance. The SecureConnector agent downloads these scripts and executables as needed from the CounterACT management appliance and runs them on the endpoint. By default, these executable files are downloaded to and run from the %TEMP% directory of the currently logged on user, despite the fact that the SecureConnector agent is running as SYSTEM. Aside from the downloaded scripts, the SecureConnector agent runs a batch file with SYSTEM privileges from the temp directory of the currently logged on user. If the naming convention of this script can be derived, which is made possible by placing it in a directory to which the user has read access, it may be possible overwrite the legitimate batch file with a malicious one before SecureConnector executes it. It is possible to change this directory by setting the the configuration property config.script_run_folder.value in the local.properties configuration file on the CounterACT management appliance, however the batch file which is run does not follow this property.
CVE-2016-9487
EpubCheck 4.0.1 does not properly restrict resolving external entities when parsing XML in EPUB files during validation. An attacker who supplies a specially crafted EPUB file may be able to exploit this behavior to read arbitrary files, or have the victim execute arbitrary requests on his behalf, abusing the victim's trust relationship with other entities.
CVE-2016-9489
In ManageEngine Applications Manager 12 and 13 before build 13200, an authenticated user is able to alter all of their own properties, including own group, i.e. changing their group to one with higher privileges like "ADMIN". A user is also able to change properties of another user, e.g. change another user's password.
CVE-2016-9491
ManageEngine Applications Manager 12 and 13 before build 13690 allows an authenticated user, who is able to access /register.do page (most likely limited to administrator), to browse the filesystem and read the system files, including Applications Manager configuration, stored private keys, etc. By default Application Manager is running with administrative privileges, therefore it is possible to access every directory on the underlying operating system.
CVE-2016-9492
The code generated by PHP FormMail Generator prior to 17 December 2016 is vulnerable to unrestricted upload of dangerous file types. In the generated form.lib.php file, upload file types are checked against a hard-coded list of dangerous extensions. This list does not include all variations of PHP files, which may lead to execution of the contained PHP code if the attacker can guess the uploaded filename. The form by default appends a short random string to the end of the filename.
CVE-2016-9493
The code generated by PHP FormMail Generator prior to 17 December 2016 is vulnerable to stored cross-site scripting. In the generated form.lib.php file, upload file types are checked against a hard-coded list of dangerous extensions. This list does not include all variations of PHP files, which may lead to execution of the contained PHP code if the attacker can guess the uploaded filename. The form by default appends a short random string to the end of the filename.
CVE-2016-9494
Hughes high-performance broadband satellite modems, models HN7740S DW7000 HN7000S/SM, are potentially vulnerable to improper input validation. The device's advanced status web page that is linked to from the basic status web page does not appear to properly parse malformed GET requests. This may lead to a denial of service.
CVE-2016-9495
Hughes high-performance broadband satellite modems, models HN7740S DW7000 HN7000S/SM, uses hard coded credentials. Access to the device's default telnet port (23) can be obtained through using one of a few default credentials shared among all devices.
CVE-2016-9496
Hughes high-performance broadband satellite modems, models HN7740S DW7000 HN7000S/SM, lacks authentication. An unauthenticated user may send an HTTP GET request to http://[ip]/com/gatewayreset or http://[ip]/cgi/reboot.bin to cause the modem to reboot.
CVE-2016-9497
Hughes high-performance broadband satellite modems, models HN7740S DW7000 HN7000S/SM, is vulnerable to an authentication bypass using an alternate path or channel. By default, port 1953 is accessible via telnet and does not require authentication. An unauthenticated remote user can access many administrative commands via this interface, including rebooting the modem.
CVE-2016-9498
ManageEngine Applications Manager 12 and 13 before build 13200, allows unserialization of unsafe Java objects. The vulnerability can be exploited by remote user without authentication and it allows to execute remote code compromising the application as well as the operating system. As Application Manager's RMI registry is running with privileges of system administrator, by exploiting this vulnerability an attacker gains highest privileges on the underlying operating system.
CVE-2016-9499
Accellion FTP server prior to version FTA_9_12_220 only returns the username in the server response if the username is invalid. An attacker may use this information to determine valid user accounts and enumerate them.
CVE-2016-9500
Accellion FTP server prior to version FTA_9_12_220 uses the Accusoft Prizm Content flash component, which contains multiple parameters (customTabCategoryName, customButton1Image) that are vulnerable to cross-site scripting.
CVE-2017-13091
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including improperly specified padding in CBC mode allows use of an EDA tool as a decryption oracle. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007: The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts. 
<br />
Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable. 
The CPE configuration provided is likely not inclusive of all vulnerable products. Please contact the vendor of your product for more information.
CVE-2017-13092
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including improperly specified HDL syntax allows use of an EDA tool as a decryption oracle. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-13093
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including modification of encrypted IP cyphertext to insert hardware trojans. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-13094
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including modification of the encryption key and insertion of hardware trojans in any IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-13095
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including modification of a license-deny response to a license grant. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-13096
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including modification of Rights Block to remove or relax access control. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-13097
The P1735 IEEE standard describes flawed methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP, including modification of Rights Block to remove or relax license requirement. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
According to https://www.kb.cert.org/vuls/id/739007:
The P1735 IEEE standard describes methods for encrypting electronic-design intellectual property (IP), as well as the management of access rights for such IP. The methods are flawed and, in the most egregious cases, enable attack vectors that allow recovery of the entire underlying plaintext IP. Implementations of IEEE P1735 may be weak to cryptographic attacks that allow an attacker to obtain plaintext intellectual property without the key, among other impacts.
<br />

Commercial electronic design automation (EDA) tools that utilize the P1735 standard, or products designed with such EDA tools may be vulnerable.
<br />
The CPE configuration provided is likely not inclusive of all vulnerable products.  Please contact the vendor of your product for more information.
CVE-2017-1367
IBM Security Identity Governance and Intelligence Virtual Appliance 5.2 through 5.2.3.2 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 126860.
CVE-2017-1395
IBM Security Identity Governance and Intelligence Virtual Appliance 5.2 through 5.2.3.2 could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques. IBM X-Force ID: 127341.
CVE-2018-1000206
JFrog Artifactory version since 5.11 contains a Cross ite Request Forgery (CSRF) vulnerability in UI rest endpoints that can result in Classic CSRF attack allowing an attacker to perform actions as logged in user. This attack appear to be exploitable via The victim must run maliciously crafted flash component. This vulnerability appears to have been fixed in 6.1.
CVE-2018-1000207
MODX Revolution version <=2.6.4 contains a Incorrect Access Control vulnerability in Filtering user parameters before passing them into phpthumb class that can result in Creating file with custom a filename and content. This attack appear to be exploitable via Web request. This vulnerability appears to have been fixed in commit 06bc94257408f6a575de20ddb955aca505ef6e68.
CVE-2018-1000208
MODX Revolution version <=2.6.4 contains a Directory Traversal vulnerability in /core/model/modx/modmanagerrequest.class.php that can result in remove files. This attack appear to be exploitable via web request via security/login processor. This vulnerability appears to have been fixed in pull 13980.
CVE-2018-1000209
Sensu, Inc. Sensu Core version Before version 1.4.2-3 contains a Insecure Permissions vulnerability in Sensu Core on Windows platforms that can result in Unprivileged users may execute code in context of Sensu service account. This attack appear to be exploitable via Unprivileged user may place an arbitrary DLL in the c:\opt\sensu\embedded\bin directory in order to exploit standard Windows DLL load order behavior. This vulnerability appears to have been fixed in 1.4.2-3 and later.
CVE-2018-1000210
YamlDotNet version 4.3.2 and earlier contains a Insecure Direct Object Reference vulnerability in The default behavior of Deserializer.Deserialize() will deserialize user-controlled types in the line "currentType = Type.GetType(nodeEvent.Tag.Substring(1), throwOnError: false);" and blindly instantiates them. that can result in Code execution in the context of the running process. This attack appear to be exploitable via Victim must parse a specially-crafted YAML file. This vulnerability appears to have been fixed in 5.0.0.
CVE-2018-1000211
Doorkeeper version 4.2.0 and later contains a Incorrect Access Control vulnerability in Token revocation API's authorized method that can result in Access tokens are not revoked for public OAuth apps, leaking access until expiry.
CVE-2018-10018
The GDASPAMLib.AntiSpam ActiveX control ASK\GDASpam.dll in G DATA Total Security 25.4.0.3 has a buffer overflow via a long IsBlackListed argument.
CVE-2018-10098
In MicroWorld eScan Internet Security Suite (ISS) for Business 14.0.1400.2029, the driver econceal.sys allows a non-privileged user to send a 0x830020E0 IOCTL request to \\.\econceal to cause a denial of service (BSOD).
CVE-2018-10631
Medtronic N'Vision Clinician Programmer 8840 N'Vision Clinician Programmer, all versions, and 8870 N'Vision removable Application Card, all versions. The 8840 Clinician Programmer executes the application program from the 8870 Application Card. An attacker with physical access to an 8870 Application Card and sufficient technical capability can modify the contents of this card, including the binary executables. If modified to bypass protection mechanisms, this malicious code will be run when the card is inserted into an 8840 Clinician Programmer.
CVE-2018-10875
A flaw was found in ansible. ansible.cfg is read from the current working directory which can be altered to make it point to a plugin or a module path under the control of an attacker, thus allowing the attacker to execute arbitrary code.
CVE-2018-1245
RSA Identity Lifecycle and Governance versions 7.0.1, 7.0.2 and 7.1.0 contains an authorization bypass vulnerability within the workflow architect component (ACM). A remote authenticated malicious user with non-admin privileges could potentially bypass the Java Security Policies. Once bypassed, a malicious user could potentially run arbitrary system commands at the OS level with application owner privileges on the affected system.
CVE-2018-1255
RSA Identity Lifecycle and Governance versions 7.0.1, 7.0.2 and 7.1.0 contains a reflected cross-site scripting vulnerability. A remote unauthenticated attacker could potentially exploit this vulnerability by tricking a victim application user to supply malicious HTML or JavaScript code to a vulnerable web application, which is then reflected back to the victim and executed by the web browser.
CVE-2018-14029
CSRF vulnerability in admin/user/edit in Creatiwity wityCMS 0.6.2 allows an attacker to take over a user account, as demonstrated by modifying the account's email field.
CVE-2018-14031
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer over-read in the function H5T_copy in H5T.c.
CVE-2018-14032
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-11206. Reason: This candidate is a reservation duplicate of CVE-2018-11206. Notes: All CVE users should reference CVE-2018-11206 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-14033
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer over-read in the function H5O_layout_decode in H5Olayout.c, related to HDmemcpy.
CVE-2018-14034
An issue was discovered in the HDF HDF5 1.8.20 library. There is an out of bounds read in the function H5O_pline_reset in H5Opline.c.
CVE-2018-14035
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer over-read in the function H5VM_memcpyvv in H5VM.c.
CVE-2018-14036
Directory Traversal with ../ sequences occurs in AccountsService before 0.6.50 because of an insufficient path check in user_change_icon_file_authorized_cb() in user.c.
CVE-2018-14040
In Bootstrap before 4.1.2, XSS is possible in the collapse data-parent attribute.
CVE-2018-14041
In Bootstrap before 4.1.2, XSS is possible in the data-target property of scrollspy.
CVE-2018-14042
In Bootstrap before 4.1.2, XSS is possible in the data-container property of tooltip.
CVE-2018-14043
mstdlib (aka the M Standard Library for C) 1.2.0 has incorrect file access control in situations where M_fs_perms_can_access attempts to delete an existing file (that lacks public read/write access) during a copy operation, related to fs/m_fs.c and fs/m_fs_path.c. An attacker could create the file and then would have access to the data.
CVE-2018-14044
The RateTransposer::setChannels function in RateTransposer.cpp in libSoundTouch.a in Olli Parviainen SoundTouch 2.0 allows remote attackers to cause a denial of service (assertion failure and application exit), as demonstrated by SoundStretch.
CVE-2018-14045
The FIRFilter::evaluateFilterMulti function in FIRFilter.cpp in libSoundTouch.a in Olli Parviainen SoundTouch 2.0 allows remote attackers to cause a denial of service (assertion failure and application exit), as demonstrated by SoundStretch.
CVE-2018-14046
Exiv2 0.26 has a heap-based buffer over-read in WebPImage::decodeChunks in webpimage.cpp.
CVE-2018-14047
** DISPUTED ** An issue has been found in PNGwriter 0.7.0. It is a SEGV in pngwriter::readfromfile in pngwriter.cc. NOTE: there is a "Warning: PNGwriter was never designed for reading untrusted files with it. Do NOT use this in sensitive environments, especially DO NOT read PNGs from unknown sources with it!" statement in the master/README.md file.
CVE-2018-14048
An issue has been found in libpng 1.6.34. It is a SEGV in the function png_free_data in png.c, related to the recommended error handling for png_read_image.
CVE-2018-14049
An issue has been found in libwav through 2017-04-20. It is a SEGV in the function print_info in wav_info/wav_info.c.
CVE-2018-14050
An issue has been found in libwav through 2017-04-20. It is a SEGV in the function wav_free in libwav.c.
CVE-2018-14051
The function wav_read in libwav.c in libwav through 2017-04-20 has an infinite loop.
CVE-2018-14052
An issue has been found in libwav through 2017-04-20. It is a SEGV in the function apply_gain in wav_gain/wav_gain.c.
CVE-2018-14054
A double free exists in the MP4StringProperty class in mp4property.cpp in MP4v2 2.0.0. A dangling pointer is freed again in the destructor once an exception is triggered.
CVE-2018-6969
VMware Tools (10.x and prior before 10.3.0) contains an out-of-bounds read vulnerability in HGFS. Successful exploitation of this issue may lead to information disclosure or may allow attackers to escalate their privileges on the guest VMs. In order to be able to exploit this issue, file sharing must be enabled.
CVE-2018-7535
An issue was discovered in TotalAV v4.1.7. An unprivileged user could modify or overwrite all of the product's files because of weak permissions (Everyone:F) under %PROGRAMFILES%, which allows local users to gain privileges or obtain maximum control over the product.
CVE-2018-8847
Eaton 9000X DriveA versions 2.0.29 and prior has a stack-based buffer overflow vulnerability, which may allow remote code execution.
CVE-2018-9067
The Lenovo Help Android app versions earlier than 6.1.2.0327 had insufficient access control for some functions which, if exploited, could have led to exposure of approximately 400 email addresses and 8,500 IMEI.
CVE-2018-9070
For the Lenovo Smart Assistant Android app versions earlier than 12.1.82, an attacker with physical access to the smart speaker can, by pressing a specific button sequence, enter factory test mode and enable a web service intended for testing the device. As with most test modes, this provides extra privileges, including changing settings and running code. Lenovo Smart Assistant is an Amazon Alexa-enabled smart speaker developed by Lenovo.
