CVE-2008-0001
VFS in the Linux kernel before 2.6.22.16, and 2.6.23.x before 2.6.23.14, performs tests of access mode by using the flag variable instead of the acc_mode variable, which might allow local users to bypass intended permissions and remove directories.
CVE-2008-0173
SQL injection vulnerability in Gforge 4.6.99 and earlier allows remote attackers to execute arbitrary SQL commands via unspecified parameters, related to RSS exports.
CVE-2008-0253
SQL injection vulnerability in full_text.php in Binn SBuilder allows remote attackers to execute arbitrary SQL commands via the nid parameter.
CVE-2008-0254
SQL injection vulnerability in activate.php in TutorialCMS (aka Photoshop Tutorials) 1.02, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the userName parameter.
CVE-2008-0255
SQL injection vulnerability in archive.php in iGaming 1.5, and 1.3.1 and earlier, allows remote attackers to execute arbitrary SQL commands via the section parameter.
CVE-2008-0256
Multiple SQL injection vulnerabilities in Matteo Binda ASP Photo Gallery 1.0 allow remote attackers to execute arbitrary SQL commands via the (1) id parameter to (a) Imgbig.asp, (b) thumb.asp, and (c) thumbricerca.asp and the (2) ricerca parameter to (d) thumbricerca.asp.
CVE-2008-0257
Cross-site scripting (XSS) vulnerability in search.pl in Dansie Search Engine 2.7 allows remote attackers to inject arbitrary web script or HTML via the keywords parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0258
Cross-site scripting (XSS) vulnerability in index.php in PHP Running Management (phpRunMan) before 1.0.3 allows remote attackers to inject arbitrary web script or HTML via the message parameter.
CVE-2008-0259
Multiple directory traversal vulnerabilities in _mg/php/mg_thumbs.php in minimal Gallery 0.8 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) thumbcat and (2) thumb parameters.
CVE-2008-0260
minimal Gallery 0.8 allows remote attackers to obtain configuration information via a direct request to php_info.php, which calls the phpinfo function.
CVE-2008-0261
Unspecified vulnerability in the search component and module in Mambo 4.5.x and 4.6.x allows remote attackers to cause a denial of service (query flood) via unspecified vectors.
CVE-2008-0262
SQL injection vulnerability in includes/articleblock.php in Agares PhpAutoVideo 2.21 allows remote attackers to execute arbitrary SQL commands via the articlecat parameter.
CVE-2008-0263
The SIP module in Ingate Firewall before 4.6.1 and SIParator before 4.6.1 does not reuse SIP media ports in unspecified call hold and send-only stream scenarios, which allows remote attackers to cause a denial of service (port exhaustion) via unspecified vectors.
CVE-2008-0264
Unspecified vulnerability in the Meta Tags (aka Nodewords) 5.x-1.6 module for Drupal, when images are permitted in node bodies, allows remote authenticated users to execute arbitrary code via unspecified vectors involving creation of a node.
CVE-2008-0265
Multiple cross-site scripting (XSS) vulnerabilities in the Search function in the web management interface in F5 BIG-IP 9.4.3 allow remote attackers to inject arbitrary web script or HTML via the SearchString parameter to (1) list_system.jsp, (2) list_pktfilter.jsp, (3) list_ltm.jsp, (4) resources_audit.jsp, and (5) list_asm.jsp in tmui/Control/jspmap/tmui/system/log/; and (6) list.jsp in certain directories.
CVE-2008-0266
Cross-site request forgery (CSRF) vulnerability in admin.php in eTicket 1.5.5.2 allows remote attackers to change the administrative password and possibly perform other administrative tasks.  NOTE: either the old password must be known, or the attacker must leverage a separate SQL injection vulnerability.
CVE-2008-0267
Multiple SQL injection vulnerabilities in eTicket 1.5.5.2 allow remote authenticated users to execute arbitrary SQL commands via the (1) status, (2) sort, and (3) way parameters to search.php; and allow remote authenticated administrators to execute arbitrary SQL commands via the (4) msg and (5) password parameters to admin.php.
CVE-2008-0268
Cross-site scripting (XSS) vulnerability in view.php in eTicket 1.5.5.2 allows remote attackers to inject arbitrary web script or HTML via the s parameter.
CVE-2008-0269
Unspecified vulnerability in the dotoprocs function in Sun Solaris 10 allows local users to cause a denial of service (panic) via unspecified vectors.
CVE-2008-0270
SQL injection vulnerability in index.php in TaskFreak! 0.6.1 and earlier allows remote authenticated users to execute arbitrary SQL commands via the sContext parameter.
CVE-2008-0271
The editor deletion form in BUEditor 4.7.x before 4.7.x-1.0 and 5.x before 5.x-1.1, a module for Drupal, does not follow Drupal's Forms API submission model, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks and delete custom editor interfaces.
CVE-2008-0272
Cross-site request forgery (CSRF) vulnerability in the aggregator module in Drupal 4.7.x before 4.7.11 and 5.x before 5.6 allows remote attackers to delete items from a feed as privileged users.
CVE-2008-0273
Interpretation conflict in Drupal 4.7.x before 4.7.11 and 5.x before 5.6, when Internet Explorer 6 is used, allows remote attackers to conduct cross-site scripting (XSS) attacks via invalid UTF-8 byte sequences, which are not processed as UTF-8 by Drupal's HTML filtering, but are processed as UTF-8 by Internet Explorer, effectively removing characters from the document and defeating the HTML protection mechanism.
CVE-2008-0274
Cross-site scripting (XSS) vulnerability in Drupal 4.7.x and 5.x, when certain .htaccess protections are disabled, allows remote attackers to inject arbitrary web script or HTML via crafted links involving theme .tpl.php files.
CVE-2008-0275
The Atom 4.7 before 4.7.x-1.0 and 5.x before 5.x-1.0 module for Drupal does not properly manage permissions for node (1) titles, (2) teasers, and (3) bodies, which might allow remote attackers to gain access to syndicated content.
CVE-2008-0276
Cross-site scripting (XSS) vulnerability in the Devel module before 5.x-0.1 for Drupal allows remote attackers to inject arbitrary web script or HTML via a site variable, related to lack of escaping of the variable table.
CVE-2008-0277
Unspecified vulnerability in the Fileshare module for Drupal allows remote authenticated users with node-creation privileges to execute arbitrary code via unspecified vectors.
CVE-2008-0278
SQL injection vulnerability in index.php in X7 Chat 2.0.5 and possibly earlier allows remote attackers to execute arbitrary SQL commands via the day parameter in a sm_window action.
CVE-2008-0279
SQL injection vulnerability in liretopic.php in Xforum 1.4 and possibly others allows remote attackers to execute arbitrary SQL commands via the topic parameter.  NOTE: the categorie parameter might also be affected.
CVE-2008-0280
SQL injection vulnerability in index.php in MTCMS 2.0 and possibly earlier versions allows remote attackers to execute arbitrary SQL commands via the (1) a or (2) cid parameter.
CVE-2008-0281
SQL injection vulnerability in liste.php in ID-Commerce 2.0 and earlier allows remote attackers to execute arbitrary SQL commands via the idFamille parameter.
CVE-2008-0282
SQL injection vulnerability in welcome/inscription.php in DomPHP 0.81 and earlier allows remote attackers to execute arbitrary SQL commands via the mail parameter.
CVE-2008-0283
PHP remote file inclusion vulnerability in /aides/index.php in DomPHP 0.81 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.
CVE-2008-0284
Cross-site scripting (XSS) vulnerability in Simple Machines Forum (SMF) 1.1.4 and earlier allows remote attackers to inject arbitrary web script or HTML via (1) Itemid or (2) topic arguments.
