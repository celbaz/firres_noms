CVE-2013-4352
The cache_invalidate function in modules/cache/cache_storage.c in the mod_cache module in the Apache HTTP Server 2.4.6, when a caching forward proxy is enabled, allows remote HTTP servers to cause a denial of service (NULL pointer dereference and daemon crash) via vectors that trigger a missing hostname value.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-0117
The mod_proxy module in the Apache HTTP Server 2.4.x before 2.4.10, when a reverse proxy is enabled, allows remote attackers to cause a denial of service (child-process crash) via a crafted HTTP Connection header.
Per vendor advisory http://httpd.apache.org/security/vulnerabilities_24.html
"A flaw was found in mod_proxy in httpd versions 2.4.6 to 2.4.9."
CVE-2014-0118
The deflate_in_filter function in mod_deflate.c in the mod_deflate module in the Apache HTTP Server before 2.4.10, when request body decompression is enabled, allows remote attackers to cause a denial of service (resource consumption) via crafted request data that decompresses to a much larger size.
CVE-2014-0226
Race condition in the mod_status module in the Apache HTTP Server before 2.4.10 allows remote attackers to cause a denial of service (heap-based buffer overflow), or possibly obtain sensitive credential information or execute arbitrary code, via a crafted request that triggers improper scoreboard handling within the status_handler function in modules/generators/mod_status.c and the lua_ap_scoreboard_worker function in modules/lua/lua_request.c.
CVE-2014-0231
The mod_cgid module in the Apache HTTP Server before 2.4.10 does not have a timeout mechanism, which allows remote attackers to cause a denial of service (process hang) via a request to a CGI script that does not read from its stdin file descriptor.
CVE-2014-1973
Directory traversal vulnerability in the NextApp File Explorer application before 2.1.0.3 for Android allows remote attackers to overwrite or create arbitrary files via a crafted filename.
CVE-2014-1987
The CGI component in Cybozu Garoon 3.1.0 through 3.7 SP3 allows remote attackers to execute arbitrary commands via unspecified vectors.
CVE-2014-1992
Cross-site scripting (XSS) vulnerability in the Messages functionality in Cybozu Garoon 3.1.x, 3.5.x, and 3.7.x before 3.7 SP4 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-1993
The Portlets subsystem in Cybozu Garoon 2.x and 3.x before 3.7 SP4 allows remote authenticated users to bypass intended access restrictions via unspecified vectors.
CVE-2014-1994
Cross-site scripting (XSS) vulnerability in the Notices portlet in Cybozu Garoon 2.x and 3.x before 3.7 SP4 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-1995
Cross-site scripting (XSS) vulnerability in the Map search functionality in Cybozu Garoon 2.x and 3.x before 3.7 SP4 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-1996
Cybozu Garoon 3.7 before SP4 allows remote authenticated users to bypass intended access restrictions, and execute arbitrary code or cause a denial of service, via an API call.
CVE-2014-1999
The auto-format feature in the Request_Curl class in FuelPHP 1.1 through 1.7.1 allows remote attackers to execute arbitrary code via a crafted response.
CVE-2014-3159
The WebContentsDelegateAndroid::OpenURLFromTab function in components/web_contents_delegate_android/web_contents_delegate_android.cc in Google Chrome before 36.0.1985.122 on Android does not properly restrict URL loading, which allows remote attackers to spoof the URL in the Omnibox via unspecified vectors.
CVE-2014-3160
The ResourceFetcher::canRequest function in core/fetch/ResourceFetcher.cpp in Blink, as used in Google Chrome before 36.0.1985.125, does not properly restrict subresource requests associated with SVG files, which allows remote attackers to bypass the Same Origin Policy via a crafted file.
CVE-2014-3161
The WebMediaPlayerAndroid::load function in content/renderer/media/android/webmediaplayer_android.cc in Google Chrome before 36.0.1985.122 on Android does not properly interact with redirects, which allows remote attackers to bypass the Same Origin Policy via a crafted web site that hosts a video stream.
CVE-2014-3162
Multiple unspecified vulnerabilities in Google Chrome before 36.0.1985.125 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2014-3523
Memory leak in the winnt_accept function in server/mpm/winnt/child.c in the WinNT MPM in the Apache HTTP Server 2.4.x before 2.4.10 on Windows, when the default AcceptFilter is enabled, allows remote attackers to cause a denial of service (memory consumption) via crafted requests.
CVE-2014-3884
Cross-site scripting (XSS) vulnerability in Usermin before 1.600 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this might overlap CVE-2014-3924.
CVE-2014-3885
Cross-site scripting (XSS) vulnerability in Webmin before 1.690 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this might overlap CVE-2014-3924.
CVE-2014-3886
Cross-site scripting (XSS) vulnerability in Webmin before 1.690, when referrer checking is disabled, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this might overlap CVE-2014-3924.
CVE-2014-3892
Cross-site scripting (XSS) vulnerability in Nexa Meridian before 2014 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-3894
Cross-site scripting (XSS) vulnerability in PHP Kobo Multifunctional MailForm Free 2014/1/28 and earlier allows remote attackers to inject arbitrary web script or HTML via an HTTP Referer header.
CVE-2014-4341
MIT Kerberos 5 (aka krb5) before 1.12.2 allows remote attackers to cause a denial of service (buffer over-read and application crash) by injecting invalid tokens into a GSSAPI application session.
CVE-2014-4342
MIT Kerberos 5 (aka krb5) 1.7.x through 1.12.x before 1.12.2 allows remote attackers to cause a denial of service (buffer over-read or NULL pointer dereference, and application crash) by injecting invalid tokens into a GSSAPI application session.
CVE-2014-4954
Cross-site scripting (XSS) vulnerability in the PMA_getHtmlForActionLinks function in libraries/structure.lib.php in phpMyAdmin 4.2.x before 4.2.6 allows remote authenticated users to inject arbitrary web script or HTML via a crafted table comment that is improperly handled during construction of a database structure page.
CVE-2014-4955
Cross-site scripting (XSS) vulnerability in the PMA_TRI_getRowForList function in libraries/rte/rte_list.lib.php in phpMyAdmin 4.0.x before 4.0.10.1, 4.1.x before 4.1.14.2, and 4.2.x before 4.2.6 allows remote authenticated users to inject arbitrary web script or HTML via a crafted trigger name that is improperly handled on the database triggers page.
CVE-2014-4986
Multiple cross-site scripting (XSS) vulnerabilities in js/functions.js in phpMyAdmin 4.0.x before 4.0.10.1, 4.1.x before 4.1.14.2, and 4.2.x before 4.2.6 allow remote authenticated users to inject arbitrary web script or HTML via a crafted (1) table name or (2) column name that is improperly handled during construction of an AJAX confirmation message.
CVE-2014-4987
server_user_groups.php in phpMyAdmin 4.1.x before 4.1.14.2 and 4.2.x before 4.2.6 allows remote authenticated users to bypass intended access restrictions and read the MySQL user list via a viewUsers request.
