CVE-2015-5664
Cross-site scripting (XSS) vulnerability in File Station in QNAP QTS before 4.2.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-6931
Cross-site scripting (XSS) vulnerability in the vSphere Web Client in VMware vCenter Server 5.0 before U3g, 5.1 before U3d, and 5.5 before U2d allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-7029
Apple AirPort Base Station Firmware before 7.6.7 and 7.7.x before 7.7.7 misparses DNS data, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2016-0221
Cross-site scripting (XSS) vulnerability in IBM Cognos TM1, as used in IBM Cognos Business Intelligence 10.2 before IF20, 10.2.1 before IF17, 10.2.1.1 before IF16, 10.2.2 before IF12, and 10.1.1 before IF19, allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0346
Cross-site scripting (XSS) vulnerability in IBM Cognos Business Intelligence 10.2 before IF20, 10.2.1 before IF17, 10.2.1.1 before IF16, 10.2.2 before IF12, and 10.1.1 before IF19 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0359
CRLF injection vulnerability in IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.43, 8.0 before 8.0.0.13, 8.5 Full before 8.5.5.10, and 8.5 Liberty before Liberty Fix Pack 16.0.0.2 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a crafted URL.
<a href="https://cwe.mitre.org/data/definitions/93.html">CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')</a>
CVE-2016-1227
NTT EAST Hikari Denwa routers with firmware PR-400MI, RT-400MI, and RV-440MI 07.00.1006 and earlier and NTT WEST Hikari Denwa routers with firmware PR-400MI, RT-400MI, and RV-440MI 07.00.1005 and earlier allow remote authenticated users to execute arbitrary OS commands via unspecified vectors.
CVE-2016-1228
Cross-site request forgery (CSRF) vulnerability on NTT EAST Hikari Denwa routers with firmware PR-400MI, RT-400MI, and RV-440MI 07.00.1006 and earlier and NTT WEST Hikari Denwa routers with firmware PR-400MI, RT-400MI, and RV-440MI 07.00.1005 and earlier allows remote attackers to hijack the authentication of arbitrary users.
CVE-2016-1328
goform/WClientMACList on Cisco EPC3928 devices allows remote attackers to cause a denial of service (device crash) via a long h_sortWireless parameter, related to a "Gateway Client List Denial of Service" issue, aka Bug ID CSCux24948.
CVE-2016-1336
goform/Docsis_system on Cisco EPC3928 devices allows remote attackers to cause a denial of service (device crash) via a long LanguageSelect parameter, related to a "Gateway HTTP Corruption Denial of Service" issue, aka Bug ID CSCuy28100.
CVE-2016-1337
Cisco EPC3928 devices allow remote attackers to obtain sensitive configuration and credential information by making requests during the early part of the boot process, related to a "Boot Information Disclosure" issue, aka Bug ID CSCux17178.
CVE-2016-1394
Cisco Firepower System Software 6.0.0 through 6.1.0 has a hardcoded account, which allows remote attackers to obtain CLI access by leveraging knowledge of the password, aka Bug ID CSCuz56238.
CVE-2016-1398
Buffer overflow in the web-based management interface on Cisco RV110W devices with firmware through 1.2.1.4, RV130W devices with firmware through 1.0.2.7, and RV215W devices with firmware through 1.3.0.7 allows remote authenticated users to cause a denial of service (device reload) via a crafted HTTP request, aka Bug ID CSCux86669.
CVE-2016-1425
Cisco IOS 15.0(2)SG5, 15.1(2)SG3, 15.2(1)E, 15.3(3)S, and 15.4(1.13)S allows remote attackers to cause a denial of service (device crash) via a crafted LLDP packet, aka Bug ID CSCun66735.
CVE-2016-1441
Cisco Cloud Network Automation Provisioner (CNAP) 1.0(0) in Cisco Configuration Assistant (CCA) allows remote attackers to bypass intended filesystem and administrative-endpoint restrictions via GET API calls, aka Bug ID CSCuy77145.
CVE-2016-1606
Multiple stack-based buffer overflows in COM objects in Micro Focus Rumba 9.4.x before 9.4 HF 13960 allow remote attackers to execute arbitrary code via (1) the NetworkName property value to ObjectXSNAConfig.ObjectXSNAConfig in iconfig.dll, (2) the CPName property value to ObjectXSNAConfig.ObjectXSNAConfig in iconfig.dll, (3) the PrinterName property value to ProfileEditor.PrintPasteControl in ProfEdit.dll, (4) the Data argument to the WriteRecords function in FTXBIFFLib.AS400FtxBIFF in FtxBIFF.dll, (5) the Serialized property value to NMSECCOMPARAMSLib.SSL3 in NMSecComParams.dll, (6) the UserName property value to NMSECCOMPARAMSLib.FirewallProxy in NMSecComParams.dll, (7) the LUName property value to ProfileEditor.MFSNAControl in ProfEdit.dll, (8) the newVal argument to the Load function in FTPSFTPLib.SFtpSession in FTPSFtp.dll, or (9) a long Host field in the FTP Client.
CVE-2016-1704
Multiple unspecified vulnerabilities in Google Chrome before 51.0.2704.103 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2016-2074
Buffer overflow in lib/flow.c in ovs-vswitchd in Open vSwitch 2.2.x and 2.3.x before 2.3.3 and 2.4.x before 2.4.1 allows remote attackers to execute arbitrary code via crafted MPLS packets, as demonstrated by a long string in an ovs-appctl command.
CVE-2016-2079
VMware NSX Edge 6.1 before 6.1.7 and 6.2 before 6.2.3 and vCNS Edge 5.5 before 5.5.4.3, when the SSL-VPN feature is configured, allow remote attackers to obtain sensitive information via unspecified vectors.
CVE-2016-2081
Cross-site scripting (XSS) vulnerability in VMware vRealize Log Insight 2.x and 3.x before 3.3.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-2082
Cross-site request forgery (CSRF) vulnerability in VMware vRealize Log Insight 2.x and 3.x before 3.3.2 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2016-2862
Cross-site scripting (XSS) vulnerability in IBM WebSphere Commerce 6.0 through 6.0.0.11, 7.0 before 7.0.0.9 cumulative iFix 3, and 8.0 before 8.0.0.5 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-2863
Cross-site request forgery (CSRF) vulnerability in IBM WebSphere Commerce 7.0 Feature Pack 8, 8.0.0.x before 8.0.0.10, and 8.0.1.x before 8.0.1.2 allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2016-2894
IBM Spectrum Protect (formerly Tivoli Storage Manager) 5.5 through 6.3 before 6.3.2.6, 6.4 before 6.4.3.3, and 7.1 before 7.1.6 allows local users to obtain sensitive retrieved data from arbitrary accounts in opportunistic circumstances by leveraging previous use of a symlink during archive and retrieve actions.
CVE-2016-3955
The usbip_recv_xbuff function in drivers/usb/usbip/usbip_common.c in the Linux kernel before 4.5.3 allows remote attackers to cause a denial of service (out-of-bounds write) or possibly have unspecified other impact via a crafted length value in a USB/IP packet.
CVE-2016-3962
Stack-based buffer overflow in the NTP time-server interface on Meinberg IMS-LANTIME M3000, IMS-LANTIME M1000, IMS-LANTIME M500, LANTIME M900, LANTIME M600, LANTIME M400, LANTIME M300, LANTIME M200, LANTIME M100, SyncFire 1100, and LCES devices with firmware before 6.20.004 allows remote attackers to obtain sensitive information, modify data, or cause a denial of service via a crafted parameter in a POST request.
CVE-2016-3988
Multiple stack-based buffer overflows in the NTP time-server interface on Meinberg IMS-LANTIME M3000, IMS-LANTIME M1000, IMS-LANTIME M500, LANTIME M900, LANTIME M600, LANTIME M400, LANTIME M300, LANTIME M200, LANTIME M100, SyncFire 1100, and LCES devices with firmware before 6.20.004 allow remote attackers to obtain sensitive information, modify data, or cause a denial of service via a crafted parameter in a POST request.
CVE-2016-3989
The NTP time-server interface on Meinberg IMS-LANTIME M3000, IMS-LANTIME M1000, IMS-LANTIME M500, LANTIME M900, LANTIME M600, LANTIME M400, LANTIME M300, LANTIME M200, LANTIME M100, SyncFire 1100, and LCES devices with firmware before 6.20.004 allows remote authenticated users to obtain root privileges for writing to unspecified scripts, and consequently obtain sensitive information or modify data, by leveraging access to the nobody account.
CVE-2016-4509
Heap-based buffer overflow in elcsoft.exe in Eaton ELCSoft 2.4.01 and earlier allows remote authenticated users to execute arbitrary code via a crafted file.
CVE-2016-4512
Stack-based buffer overflow in ELCSimulator in Eaton ELCSoft 2.4.01 and earlier allows remote attackers to execute arbitrary code via a long packet.
CVE-2016-4997
The compat IPT_SO_SET_REPLACE and IP6T_SO_SET_REPLACE setsockopt implementations in the netfilter subsystem in the Linux kernel before 4.6.3 allow local users to gain privileges or cause a denial of service (memory corruption) by leveraging in-container root access to provide a crafted offset value that triggers an unintended decrement.
CVE-2016-4998
The IPT_SO_SET_REPLACE setsockopt implementation in the netfilter subsystem in the Linux kernel before 4.6 allows local users to cause a denial of service (out-of-bounds read) or possibly obtain sensitive information from kernel heap memory by leveraging in-container root access to provide a crafted offset value that leads to crossing a ruleset blob boundary.
CVE-2016-5228
Stack-based buffer overflow in the PlayMacro function in ObjectXMacro.ObjectXMacro in WdMacCtl.ocx in Micro Focus Rumba 9.x before 9.3 HF 11997 and 9.4.x before 9.4 HF 12815 allows remote attackers to execute arbitrary code via a long MacroName argument. NOTE: some references mention CVE-2016-5226 but that is not a correct ID for any Rumba vulnerability.
CVE-2016-5701
setup/frames/index.inc.php in phpMyAdmin 4.0.10.x before 4.0.10.16, 4.4.15.x before 4.4.15.7, and 4.6.x before 4.6.3 allows remote attackers to conduct BBCode injection attacks against HTTP sessions via a crafted URI.
CVE-2016-5702
phpMyAdmin 4.6.x before 4.6.3, when the environment lacks a PHP_SELF value, allows remote attackers to conduct cookie-attribute injection attacks via a crafted URI.
CVE-2016-5703
SQL injection vulnerability in libraries/central_columns.lib.php in phpMyAdmin 4.4.x before 4.4.15.7 and 4.6.x before 4.6.3 allows remote attackers to execute arbitrary SQL commands via a crafted database name that is mishandled in a central column query.
CVE-2016-5704
Cross-site scripting (XSS) vulnerability in the table-structure page in phpMyAdmin 4.6.x before 4.6.3 allows remote attackers to inject arbitrary web script or HTML via vectors involving a comment.
CVE-2016-5705
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 4.4.x before 4.4.15.7 and 4.6.x before 4.6.3 allow remote attackers to inject arbitrary web script or HTML via vectors involving (1) server-privileges certificate data fields on the user privileges page, (2) an "invalid JSON" error message in the error console, (3) a database name in the central columns implementation, (4) a group name, or (5) a search name in the bookmarks implementation.
CVE-2016-5706
js/get_scripts.js.php in phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 allows remote attackers to cause a denial of service via a large array in the scripts parameter.
CVE-2016-5730
phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 allows remote attackers to obtain sensitive information via vectors involving (1) an array value to FormDisplay.php, (2) incorrect data to validate.php, (3) unexpected data to Validator.php, (4) a missing config directory during setup, or (5) an incorrect OpenID identifier data type, which reveals the full path in an error message.
CVE-2016-5731
Cross-site scripting (XSS) vulnerability in examples/openid.php in phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 allows remote attackers to inject arbitrary web script or HTML via vectors involving an OpenID error message.
CVE-2016-5732
Multiple cross-site scripting (XSS) vulnerabilities in the partition-range implementation in templates/table/structure/display_partitions.phtml in the table-structure page in phpMyAdmin 4.6.x before 4.6.3 allow remote attackers to inject arbitrary web script or HTML via crafted table parameters.
CVE-2016-5733
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 allow remote attackers to inject arbitrary web script or HTML via vectors involving (1) a crafted table name that is mishandled during privilege checking in table_row.phtml, (2) a crafted mysqld log_bin directive that is mishandled in log_selector.phtml, (3) the Transformation implementation, (4) AJAX error handling in js/ajax.js, (5) the Designer implementation, (6) the charts implementation in js/tbl_chart.js, or (7) the zoom-search implementation in rows_zoom.phtml.
CVE-2016-5734
phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 does not properly choose delimiters to prevent use of the preg_replace e (aka eval) modifier, which might allow remote attackers to execute arbitrary PHP code via a crafted string, as demonstrated by the table search-and-replace implementation.
CVE-2016-5739
The Transformation implementation in phpMyAdmin 4.0.x before 4.0.10.16, 4.4.x before 4.4.15.7, and 4.6.x before 4.6.3 does not use the no-referrer Content Security Policy (CSP) protection mechanism, which makes it easier for remote attackers to conduct CSRF attacks by reading an authentication token in a Referer header, related to libraries/Header.php.
CVE-2016-6130
Race condition in the sclp_ctl_ioctl_sccb function in drivers/s390/char/sclp_ctl.c in the Linux kernel before 4.6 allows local users to obtain sensitive information from kernel memory by changing a certain length value, aka a "double fetch" vulnerability.
