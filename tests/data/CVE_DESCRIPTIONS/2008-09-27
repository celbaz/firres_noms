CVE-2008-3528
The error-reporting functionality in (1) fs/ext2/dir.c, (2) fs/ext3/dir.c, and possibly (3) fs/ext4/dir.c in the Linux kernel 2.6.26.5 does not limit the number of printk console messages that report directory corruption, which allows physically proximate attackers to cause a denial of service (temporary system hang) by mounting a filesystem that has corrupted dir->i_size and dir->i_blocks values and performing (a) read or (b) write operations.  NOTE: there are limited scenarios in which this crosses privilege boundaries.
CVE-2008-4070
Heap-based buffer overflow in Mozilla Thunderbird before 2.0.0.17 and SeaMonkey before 1.1.12 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long header in a news article, related to "canceling [a] newsgroup message" and "cancelled newsgroup messages."
CVE-2008-4119
Multiple cross-site scripting (XSS) vulnerabilities in CA Service Desk 11.2 and CMDB 11.0 through 11.2 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving "multiple web forms."
CVE-2008-4195
Opera before 9.52 does not properly restrict the ability of a framed web page to change the address associated with a different frame, which allows remote attackers to trigger the display of an arbitrary address in a frame via unspecified use of web script.
CVE-2008-4196
Cross-site scripting (XSS) vulnerability in Opera before 9.52 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-4197
Opera before 9.52 on Windows, Linux, FreeBSD, and Solaris, when processing custom shortcut and menu commands, can produce argument strings that contain uninitialized memory, which might allow user-assisted remote attackers to execute arbitrary code or conduct other attacks via vectors related to activation of a shortcut.
CVE-2008-4198
Opera before 9.52, when rendering an http page that has loaded an https page into a frame, displays a padlock icon and offers a security information dialog reporting a secure connection, which might allow remote attackers to trick a user into performing unsafe actions on the http page.
CVE-2008-4199
Opera before 9.52 does not prevent use of links from web pages to feed source files on the local disk, which might allow remote attackers to determine the validity of local filenames via vectors involving "detection of JavaScript events and appropriate manipulation."
CVE-2008-4200
Opera before 9.52 does not ensure that the address field of a news feed represents the feed's actual URL, which allows remote attackers to change this field to display the URL of a page containing web script controlled by the attacker.
CVE-2008-4292
Opera before 9.52 does not check the CRL override upon encountering a certificate that lacks a CRL, which has unknown impact and attack vectors.  NOTE: it is not clear whether this is a vulnerability, but the vendor included it in a security section of the advisory.
CVE-2008-4293
Unspecified vulnerability in Opera before 9.52 on Windows, when registered as a protocol handler, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unknown vectors in which Opera is launched by other applications.
CVE-2008-4294
IBM Tivoli Netcool/Webtop 2.1 before 2.1.0.5 preserves cached user privileges after logout, which allows physically proximate attackers to hijack a session by visiting an unattended workstation, as demonstrated by a root session that is still valid after a subsequent read-only session has begun.
CVE-2008-4295
Microsoft Windows Mobile 6.0 on HTC Wiza 200 and HTC MDA 8125 devices does not properly handle the first attempt to establish a Bluetooth connection to a peer with a long name, which allows remote attackers to cause a denial of service (device reboot) by configuring a Bluetooth device with a long hci name and (1) connecting directly to the Windows Mobile system or (2) waiting for the Windows Mobile system to scan for nearby devices.
CVE-2008-4296
The Cisco Linksys WRT350N with firmware 1.0.3.7 has "admin" as its default password for the "admin" account, which makes it easier for remote attackers to obtain access.
CVE-2008-4297
Mercurial before 1.0.2 does not enforce the allowpull permission setting for a pull operation from hgweb, which allows remote attackers to read arbitrary files from a repository via an "hg pull" request.
CVE-2008-4298
Memory leak in the http_request_parse function in request.c in lighttpd before 1.4.20 allows remote attackers to cause a denial of service (memory consumption) via a large number of requests with duplicate request headers.
