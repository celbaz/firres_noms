CVE-2018-10622
A vulnerability was discovered in all versions of Medtronic MyCareLink 24950 and 24952 Patient Monitor. The affected products use per-product credentials that are stored in a recoverable format. An attacker can use these credentials for network authentication and encryption of local data at rest.
CVE-2018-10626
A vulnerability was discovered in all versions of Medtronic MyCareLink 24950 and 24952 Patient Monitor. The affected product's update service does not sufficiently verify the authenticity of the data uploaded. An attacker who obtains per-product credentials from the monitor and paired implantable cardiac device information can potentially upload invalid data to the Medtronic CareLink network.
CVE-2018-10630
For Crestron TSW-X60 version prior to 2.001.0037.001 and MC3 version prior to 1.502.0047.001, The devices are shipped with authentication disabled, and there is no indication to users that they need to take steps to enable it. When compromised, the access to the CTP console is left open.
CVE-2018-10769
The transferProxy and approveProxy functions of a smart contract implementation for SmartMesh (SMT), an Ethereum ERC20 token, allow attackers to accomplish an unauthorized transfer of digital assets because replay attacks can occur with the same-named functions (with the same signatures) in other tokens: First (FST), GG Token (GG), M2C Mesh Network (MTC), M2C Mesh Network (mesh), and UG Token (UGT).
CVE-2018-11048
Dell EMC Data Protection Advisor, versions 6.2, 6,3, 6.4, 6.5 and Dell EMC Integrated Data Protection Appliance (IDPA) versions 2.0, 2.1 contain a XML External Entity (XXE) Injection vulnerability in the REST API. An authenticated remote malicious user could potentially exploit this vulnerability to read certain system files in the server or cause denial of service by supplying specially crafted Document Type Definitions (DTDs) in an XML request.
CVE-2018-11063
Dell WMS versions 1.1 and prior are impacted by multiple unquoted service path vulnerabilities. Affected software installs multiple services incorrectly by specifying the paths to the service executables without quotes. This could potentially allow a low-privileged local user to execute arbitrary executables with elevated privileges.
CVE-2018-11492
ASUS HG100 devices allow denial of service via an IPv4 packet flood.
CVE-2018-13341
Crestron TSW-X60 all versions prior to 2.001.0037.001 and MC3 all versions prior to 1.502.0047.00, The passwords for special sudo accounts may be calculated using information accessible to those with regular user privileges. Attackers could decipher these passwords, which may allow them to execute hidden API calls and escape the CTP console sandbox environment with elevated privileges.
CVE-2018-13390
Unauthenticated access to cloudtoken daemon on Linux via network from version 0.1.1 before version 0.1.24 allows attackers on the same subnet to gain temporary AWS credentials for the users' roles.
CVE-2018-14028
In WordPress 4.9.7, plugins uploaded via the admin area are not verified as being ZIP files. This allows for PHP files to be uploaded. Once a PHP file is uploaded, the plugin extraction fails, but the PHP file remains in a predictable wp-content/uploads location, allowing for an attacker to then execute the file. This represents a security risk in limited scenarios where an attacker (who does have the required capabilities for plugin uploads) cannot simply place arbitrary PHP code into a valid plugin ZIP file and upload that plugin, because a machine's wp-content/plugins directory permissions were set up to block all new plugins.
CVE-2018-14503
Cross-site scripting (XSS) vulnerability in intervalCheck.jsp in Coremail XT 3.0 allows remote attackers to inject arbitrary web script or HTML via the sid parameter.
CVE-2018-14782
NetComm Wireless G LTE Light Industrial M2M Router (NWL-25) with firmware 2.0.29.11 and prior. The device allows access to configuration files and profiles without authenticating the user.
CVE-2018-14783
NetComm Wireless G LTE Light Industrial M2M Router (NWL-25) with firmware 2.0.29.11 and prior. A cross-site request forgery condition can occur, allowing an attacker to change passwords of the device remotely.
CVE-2018-14784
NetComm Wireless G LTE Light Industrial M2M Router (NWL-25) with firmware 2.0.29.11 and prior. The device is vulnerable to several cross-site scripting attacks, allowing a remote attacker to run arbitrary code on the device.
CVE-2018-14785
NetComm Wireless G LTE Light Industrial M2M Router (NWL-25) with firmware 2.0.29.11 and prior. The directory of the device is listed openly without authentication.
CVE-2018-14837
Wolf CMS 0.8.3.1 has XSS in the Snippets tab, as demonstrated by a ?/admin/snippet/edit/1 URI.
CVE-2018-15185
PHP Scripts Mall Naukri / Shine / Jobsite Clone Script 3.0.4 allows remote attackers to cause a denial of service (page update outage) via crafted PHP and JavaScript code in the "Current Position" field.
CVE-2018-15186
PHP Scripts Mall Chartered Accountant : Auditor Website 2.0.1 has CSRF via client/auditor/updprofile.php.
CVE-2018-15187
PHP Scripts Mall advanced-real-estate-script 4.0.9 has CSRF via edit-profile.php.
CVE-2018-15188
PHP Scripts Mall advanced-real-estate-script 4.0.9 allows remote attackers to cause a denial of service (page structure loss) via crafted JavaScript code in the Name field of a profile.
CVE-2018-15189
PHP Scripts Mall advanced-real-estate-script has XSS via the Name field of a profile.
CVE-2018-15190
PHP Scripts Mall hotel-booking-script 2.0.4 allows XSS via the First Name, Last Name, or Address field.
CVE-2018-15191
PHP Scripts Mall hotel-booking-script 2.0.4 allows remote attackers to cause a denial of service via crafted JavaScript code in the First Name, Last Name, or Address field.
CVE-2018-3110
A vulnerability was discovered in the Java VM component of Oracle Database Server. Supported versions that are affected are 11.2.0.4, 12.1.0.2, 12.2.0.1 and 18. Easily exploitable vulnerability allows low privileged attacker having Create Session privilege with network access via Oracle Net to compromise Java VM. While the vulnerability is in Java VM, attacks may significantly impact additional products. Successful attacks of this vulnerability can result in takeover of Java VM. CVSS 3.0 Base Score 9.9 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H).
CVE-2018-3779
active-support ruby gem 5.2.0 could allow a remote attacker to execute arbitrary code on the system, caused by containing a malicious backdoor. An attacker could exploit this vulnerability to execute arbitrary code on the system.
CVE-2018-6553
The CUPS AppArmor profile incorrectly confined the dnssd backend due to use of hard links. A local attacker could possibly use this issue to escape confinement. This flaw affects versions prior to 2.2.7-1ubuntu2.1 in Ubuntu 18.04 LTS, prior to 2.2.4-7ubuntu3.1 in Ubuntu 17.10, prior to 2.1.3-4ubuntu0.5 in Ubuntu 16.04 LTS, and prior to 1.7.2-0ubuntu1.10 in Ubuntu 14.04 LTS.
CVE-2018-6556
lxc-user-nic when asked to delete a network interface will unconditionally open a user provided path. This code path may be used by an unprivileged user to check for the existence of a path which they wouldn't otherwise be able to reach. It may also be used to trigger side effects by causing a (read-only) open of special kernel files (ptmx, proc, sys). Affected releases are LXC: 2.0 versions above and including 2.0.9; 3.0 versions above and including 3.0.0, prior to 3.0.2.
CVE-2018-7754
The aoedisk_debugfs_show function in drivers/block/aoe/aoeblk.c in the Linux kernel through 4.16.4rc4 allows local users to obtain sensitive address information by reading "ffree: " lines in a debugfs file.
