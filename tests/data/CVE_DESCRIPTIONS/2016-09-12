CVE-2016-0331
Cross-site scripting (XSS) vulnerability in IBM Rational Team Concert 6.0.1 and 6.0.2 before 6.0.2 iFix2 and Rational Collaborative Lifecycle Management 6.0.1 and 6.0.2 before 6.0.2 iFix2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-1469
The HTTP framework on Cisco SPA300, SPA500, and SPA51x devices allows remote attackers to cause a denial of service (device outage) via a series of malformed HTTP requests, aka Bug ID CSCut67385.
CVE-2016-4852
YoruFukurou (NightOwl) before 2.85 relies on support for emoji skin-tone modifiers even though this support is missing from the CoreText CTFramesetter API on OS X 10.9, which allows remote attackers to cause a denial of service (application crash) via a crafted emoji character sequence.
CVE-2016-5927
IBM Tivoli Storage Manager for Space Management (aka Spectrum Protect for Space Management) 6.3.x before 6.3.2.6, 6.4.x before 6.4.3.3, and 7.1.x before 7.1.6, when certain dsmsetpw tracing is configured, allows local users to discover an encrypted password by reading application-trace output.
CVE-2016-5954
IBM WebSphere Portal 6.1.0 through 6.1.0.6 CF27, 6.1.5 through 6.1.5.3 CF27, 7.0.0 through 7.0.0.2 CF30, 8.0.0 through 8.0.0.1 CF21, and 8.5.0 before CF12 allows remote authenticated users to cause a denial of service by uploading temporary files.
CVE-2016-6370
Directory traversal vulnerability in the web interface in Cisco Hosted Collaboration Mediation Fulfillment (HCM-F) 10.6(3) and earlier allows remote authenticated users to read arbitrary files via a crafted pathname in an HTTP request, aka Bug ID CSCuz27255.
CVE-2016-6371
Directory traversal vulnerability in the web interface in Cisco Hosted Collaboration Mediation Fulfillment (HCM-F) 10.6(3) and earlier allows remote attackers to write to arbitrary files via a crafted URL, aka Bug ID CSCuz64717.
CVE-2016-6375
Cisco Wireless LAN Controller (WLC) devices before 8.0.140.0, 8.1.x and 8.2.x before 8.2.121.0, and 8.3.x before 8.3.102.0 allow remote attackers to cause a denial of service (device reload) by sending crafted Inter-Access Point Protocol (IAPP) packets and then sending a traffic stream metrics (TSM) information request over SNMP, aka Bug ID CSCuz40221.
CVE-2016-6394
Session fixation vulnerability in Cisco Firepower Management Center and Cisco FireSIGHT System Software through 6.1.0 allows remote attackers to hijack web sessions via a session identifier, aka Bug ID CSCuz80503.
CVE-2016-6395
Cross-site scripting (XSS) vulnerability in the web-based management interface in Cisco Firepower Management Center before 6.1 and FireSIGHT System Software before 6.1 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuz58658.
CVE-2016-6396
Cisco Firepower Management Center before 6.1 and FireSIGHT System Software before 6.1, when certain malware blocking options are enabled, allow remote attackers to bypass malware detection via crafted fields in HTTP headers, aka Bug ID CSCuz44482.
CVE-2016-6398
The PPTP server in Cisco IOS 15.5(3)M does not properly initialize packet buffers, which allows remote attackers to obtain sensitive information from earlier network communication by reading packet data, aka Bug ID CSCvb16274.
CVE-2016-6399
Cisco ACE30 Application Control Engine Module through A5 3.3 and ACE 4700 Application Control Engine appliances through A5 3.3 allow remote attackers to cause a denial of service (device reload) via crafted (1) SSL or (2) TLS packets, aka Bug ID CSCvb16317.
CVE-2016-7124
ext/standard/var_unserializer.c in PHP before 5.6.25 and 7.x before 7.0.10 mishandles certain invalid objects, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via crafted serialized data that leads to a (1) __destruct call or (2) magic method call.
CVE-2016-7125
ext/session/session.c in PHP before 5.6.25 and 7.x before 7.0.10 skips invalid session names in a way that triggers incorrect parsing, which allows remote attackers to inject arbitrary-type session data by leveraging control of a session name, as demonstrated by object injection.
CVE-2016-7126
The imagetruecolortopalette function in ext/gd/gd.c in PHP before 5.6.25 and 7.x before 7.0.10 does not properly validate the number of colors, which allows remote attackers to cause a denial of service (select_colors allocation error and out-of-bounds write) or possibly have unspecified other impact via a large value in the third argument.
CVE-2016-7127
The imagegammacorrect function in ext/gd/gd.c in PHP before 5.6.25 and 7.x before 7.0.10 does not properly validate gamma values, which allows remote attackers to cause a denial of service (out-of-bounds write) or possibly have unspecified other impact by providing different signs for the second and third arguments.
CVE-2016-7128
The exif_process_IFD_in_TIFF function in ext/exif/exif.c in PHP before 5.6.25 and 7.x before 7.0.10 mishandles the case of a thumbnail offset that exceeds the file size, which allows remote attackers to obtain sensitive information from process memory via a crafted TIFF image.
CVE-2016-7129
The php_wddx_process_data function in ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a denial of service (segmentation fault) or possibly have unspecified other impact via an invalid ISO 8601 time value, as demonstrated by a wddx_deserialize call that mishandles a dateTime element in a wddxPacket XML document.
CVE-2016-7130
The php_wddx_pop_element function in ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) or possibly have unspecified other impact via an invalid base64 binary value, as demonstrated by a wddx_deserialize call that mishandles a binary element in a wddxPacket XML document.
CVE-2016-7131
ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) or possibly have unspecified other impact via a malformed wddxPacket XML document that is mishandled in a wddx_deserialize call, as demonstrated by a tag that lacks a < (less than) character.
CVE-2016-7132
ext/wddx/wddx.c in PHP before 5.6.25 and 7.x before 7.0.10 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) or possibly have unspecified other impact via an invalid wddxPacket XML document that is mishandled in a wddx_deserialize call, as demonstrated by a stray element inside a boolean element, leading to incorrect pop processing.
CVE-2016-7133
Zend/zend_alloc.c in PHP 7.x before 7.0.10, when open_basedir is enabled, mishandles huge realloc operations, which allows remote attackers to cause a denial of service (integer overflow) or possibly have unspecified other impact via a long pathname.
CVE-2016-7134
ext/curl/interface.c in PHP 7.x before 7.0.10 does not work around a libcurl integer overflow, which allows remote attackers to cause a denial of service (allocation error and heap-based buffer overflow) or possibly have unspecified other impact via a long string that is mishandled in a curl_escape call.
