CVE-2007-0455
Buffer overflow in the gdImageStringFTEx function in gdft.c in GD Graphics Library 2.0.33 and earlier allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted string with a JIS encoded font.
CVE-2007-0464
The _CFNetConnectionWillEnqueueRequests function in CFNetwork 129.19 on Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to cause a denial of service (application crash) via a crafted HTTP 301 response, which results in a NULL pointer dereference.
CVE-2007-0558
PHP remote file inclusion vulnerability in modules/mail/main.php in Inter7 vHostAdmin 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the MODULES_DIR parameter.
CVE-2007-0559
PHP remote file inclusion vulnerability in config.php in RPW 1.0.2 allows remote attackers to execute arbitrary PHP code via a URL in the sql_language parameter.
CVE-2007-0560
SQL injection vulnerability in user.asp in ASP EDGE 1.2b and earlier allows remote attackers to execute arbitrary SQL commands via the user parameter.
CVE-2007-0561
Multiple PHP remote file inclusion vulnerabilities in Xero Portal 1.2 allow remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter to (1) admin_linkdb.php, (2) admin_forum_prune.php, (3) admin_extensions.php, (4) admin_board.php, (5) admin_attachments.php, or (6) admin_users.php in admin/.
CVE-2007-0562
Windows Explorer (explorer.exe) 6.0.2900.2180 in Microsoft Windows XP SP2 allows user-assisted remote attackers to cause a denial of service (application crash) via a crafted .avi file, which triggers the crash when the user right clicks on the file.
CVE-2007-0563
Multiple cross-site scripting (XSS) vulnerabilities in Symantec Web Security (SWS) before 3.0.1.85 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to (1) error messages and (2) blocked page messages produced by SWS.
CVE-2007-0564
The license registering interface in Symantec Web Security (SWS) before 3.0.1.85 allows attackers to cause a denial of service (CPU consumption) by submitting a large file.
This vulnerablity is addressed in the following product release:
Symantec, Symantec Web Security, 3.0.1.85
CVE-2007-0565
CGI-Rescue Shopping Basket Professional 7.50 and earlier allows remote attackers to inject arbitrary operating system commands via unspecified vectors.
CVE-2007-0566
SQL injection vulnerability in news_detail.asp in ASP NEWS 3 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0567
Cross-site scripting (XSS) vulnerability in admin.php in Interactive-Scripts.Com PHP Membership Manager 1.5 allows remote attackers to inject arbitrary web script or HTML via the _p parameter.
CVE-2007-0568
PHP remote file inclusion vulnerability in system/lib/package.php in MyPHPCommander 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the gl_root parameter.
CVE-2007-0569
SQL injection vulnerability in xNews.php in xNews 1.3 allows remote attackers to execute arbitrary SQL commands via the id parameter in a shownews action.
CVE-2007-0570
PHP remote file inclusion vulnerability in ains_main.php in Johannes Gijsbers (aka Taradino) Ad Fundum Integratable News Script (AINS) 0.02b allows remote attackers to execute arbitrary PHP code via a URL in the ains_path parameter.
CVE-2007-0571
PHP remote file inclusion vulnerability in include/lib/lib_head.php in phpMyReports 3.0.11 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the cfgPathModule parameter.
CVE-2007-0572
PHP remote file inclusion vulnerability in include/irc/phpIRC.php in Drunken:Golem Gaming Portal 0.5.1 Alpha 2 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-0573
PHP remote file inclusion vulnerability in includes/config.inc.php in nsGalPHP 0.41 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the racineTBS parameter.
CVE-2007-0574
SQL injection vulnerability in rss/show_webfeed.php in SpoonLabs Vivvo Article Management CMS (aka phpWordPress) 3.40 allows remote attackers to execute arbitrary SQL commands via the wcHeadlines parameter, a different vector than CVE-2006-4715.  NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0575
Multiple SQL injection vulnerabilities in the administrative login page (admin/login.asp) in ASPCode.net AdMentor allow remote attackers to execute arbitrary SQL commands via the (1) Userid and (2) Password fields.
CVE-2007-0576
PHP remote file inclusion vulnerability in xt_counter.php in Xt-Stats 2.3.x up to 2.4.0.b3 allows remote attackers to execute arbitrary PHP code via a URL in the server_base_dir parameter.
CVE-2007-0577
PHP remote file inclusion vulnerability in function.inc.php in ACGVclick 0.2.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the path parameter.
CVE-2007-0578
The http_open function in httpget.c in mpg123 before 0.64 allows remote attackers to cause a denial of service (infinite loop) by closing the HTTP connection early.
CVE-2007-0579
Unspecified vulnerability in the calendar component in Horde Groupware Webmail Edition before 1.0, and Groupware before 1.0, allows remote attackers to include certain files via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2007-0580
PHP remote file inclusion vulnerability in menu.php in Foro Domus 2.10 allows remote attackers to execute arbitrary PHP code via a URL in the sesion_idioma parameter.
CVE-2007-0581
PHP remote file inclusion vulnerability in functions.php in EclipseBB 0.5.0 Lite allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-0582
SQL injection vulnerability in default.asp in ChernobiLe 1.0 allows remote attackers to execute arbitrary SQL commands via the User (username) field.
CVE-2007-0583
Multiple cross-site scripting (XSS) vulnerabilities in HTTP Commander 6.0, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via the (1) LogoffMessage parameter to logofflast.aspx or the (2) txtUsername parameter to Default.aspx. NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0584
PHP remote file inclusion vulnerability in membres/membreManager.php in PhP Generic Library & Framework for comm (g-neric) allows remote attackers to execute arbitrary PHP code via a URL in the include_path parameter.
CVE-2007-0585
include/debug.php in Webfwlog 0.92 and earlier, when register_globals is enabled, allows remote attackers to obtain source code of files via the conffile parameter.  NOTE: some of these details are obtained from third party information.  It is likely that this issue can be exploited to conduct directory traversal attacks.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-0588
The InternalUnpackBits function in Apple QuickDraw, as used by Quicktime 7.1.3 and other applications on Mac OS X 10.4.8 and earlier, allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted PICT file that triggers memory corruption in the _GetSrcBits32ARGB function. NOTE: this issue might overlap CVE-2007-0462.
CVE-2007-0589
SQL injection vulnerability in Forum Livre 1.0 allows remote attackers to execute arbitrary SQL commands via the user parameter to info_user.asp.
CVE-2007-0590
Cross-site scripting (XSS) vulnerability in busca2.asp in Forum Livre 1.0 remote attackers to inject arbitrary web script or HTML via the palavra parameter.
CVE-2007-0591
PHP remote file inclusion vulnerability in configure.php in Vu Le An Virtual Path (VirtualPath) 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-0592
Cross-site scripting (XSS) vulnerability in EzDatabase 2.1.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to admin/login.php and the Admin Panel Database.
CVE-2007-0593
Siteman 1.1.11 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing password hashes via a direct request for data/members.txt.
CVE-2007-0594
Siteman 2.0.x2 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing password hashes via a direct request for db/siteman/users.MYD.
CVE-2007-0595
Cross-site scripting (XSS) vulnerability in search in High 5 Review Site allows remote attackers to inject arbitrary web script or HTML via the q parameter (aka the search box).
CVE-2007-0596
PHP remote file inclusion vulnerability in index/main.php in Aztek Forum 4.00 allows remote authenticated administrators to execute arbitrary PHP code via a URL in the PF[top_url] parameter.
CVE-2007-0597
Aztek Forum 4.00 allows remote attackers to obtain sensitive information via a direct request to forum.php with the fid=XD query string, which reveals the path in an error message.
CVE-2007-0598
SQL injection vulnerability in forum/load.php in Aztek Forum 4.00 allows remote attackers to execute arbitrary SQL commands via the fid cookie to forum.php.
CVE-2007-0599
Variable overwrite vulnerability in common/config.php in Aztek Forum 4.00 allows remote attackers to overwrite arbitrary program variables and conduct other unauthorized activities, such as copying arbitrary files using index/common_actions.php, via vectors associated with extract operations on the (1) POST, (2) GET, (3) COOKIE, and (4) SERVER superglobal arrays.
CVE-2007-0600
SQL injection vulnerability in news_page.asp in Martyn Kilbryde Newsposter Script (aka makit news/blog poster) 3 and earlier allows remote attackers to execute arbitrary SQL commands via the uid parameter.
CVE-2007-0601
common/safety.php in Aztek Forum 4.00 allows remote attackers to enter certain data containing %22 sequences (URL encoded double quotes) and other potentially dangerous manipulations by sending a cookie, which bypasses the blacklist matching against the GET and PUT superglobal arrays.
CVE-2007-0602
Buffer overflow in libvsapi.so in the VSAPI library in Trend Micro VirusWall 3.81 for Linux, as used by IScan.BASE/vscan, allows local users to gain privileges via a long command line argument, a different vulnerability than CVE-2005-0533.
CVE-2007-0603
PGP Desktop before 9.5.1 does not validate data objects received over the (1) \pipe\pgpserv named pipe for PGPServ.exe or the (2) \pipe\pgpsdkserv named pipe for PGPsdkServ.exe, which allows remote authenticated users to gain privileges by sending a data object representing an absolute pointer, which causes code execution at the corresponding address.
CVE-2007-0604
Cross-site scripting (XSS) vulnerability in Movable Type (MT) before 3.34 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to the MTCommentPreviewIsStatic tag, which can open the "comment entry screen," a different vulnerability than CVE-2007-0231.
