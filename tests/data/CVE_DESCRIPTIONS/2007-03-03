CVE-2007-1231
Multiple cross-site scripting (XSS) vulnerabilities in SQLiteManager 1.2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) database name, (2) table name, (3) ViewName, (4) view, (5) trigger, and (6) function fields in main.php and certain other files.
CVE-2007-1232
Directory traversal vulnerability in SQLiteManager 1.2.0 allows remote attackers to read arbitrary files via a .. (dot dot) in a SQLiteManager_currentTheme cookie.
Successful exploitation requires that "magic_quotes_gpc" is disabled.  Additionally, in order to exploit this vulnerability to execute arbitrary code, the attacker would first be required to upload a malicious file or inject arbitrary commands into an existing file.
CVE-2007-1233
PHP remote file inclusion vulnerability in downloadcounter.php in STWC-Counter 3.4.0.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the stwc_counter_verzeichniss parameter.
CVE-2007-1234
Multiple cross-site scripting (XSS) vulnerabilities in sitex allow remote attackers to inject arbitrary web script or HTML via (1) the sxYear parameter to calendar.php, (2) the search parameter to search.php, (3) the linkid parameter to redirect.php, or (4) the page parameter to calendar_events.php.
CVE-2007-1235
Unrestricted file upload vulnerability in sitex allows remote attackers to upload arbitrary PHP code via an avatar filename with a double extension such as .php.jpg, which fails verification and is saved as a .php file.
CVE-2007-1236
sitex allows remote attackers to obtain sensitive information via a request with a numerical value for the (1) sxMonth[] or (2) sxYear[] parameter to calendar.php, or the (3) page[] parameter to calendar_events.php, which reveals the path in various error messages.
CVE-2007-1237
sitex allows remote attackers to obtain potentially sensitive information via a ' (quote) value for certain parameters, as demonstrated by parameters used in forum and search, which forces a SQL error.
CVE-2007-1238
Microsoft Office 2003 allows user-assisted remote attackers to cause a denial of service (application crash) by attempting to insert a corrupted WMF file.
CVE-2007-1239
Microsoft Excel 2003 does not properly parse .XLS files, which allows remote attackers to cause a denial of service (application crash) via a file with a (1) corrupted XML format or a (2) corrupted XLS format, which triggers a NULL pointer dereference.
CVE-2007-1240
Multiple cross-site scripting (XSS) vulnerabilities in Docebo CMS 3.0.3 through 3.0.5 allow remote attackers to inject arbitrary web script or HTML via (1) the searchkey parameter to index.php, or the (2) sn or (3) ri parameter to modules/htmlframechat/index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1241
Cross-site scripting (XSS) vulnerability in setup.php in Audins Audiens 3.3 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1242
SQL injection vulnerability in system/index.php in Audins Audiens 3.3 allows remote attackers to execute arbitrary SQL commands via the PHPSESSID cookie.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1243
Audins Audiens 3.3 allows remote attackers to bypass authentication and perform certain privileged actions, possibly an uninstall of the product, by calling unistall.php with the values cnf=disinstalla and status=on.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1244
Cross-site request forgery (CSRF) vulnerability in the AdminPanel in WordPress 2.1.1 and earlier allows remote attackers to perform privileged actions as administrators, as demonstrated using the delete action in wp-admin/post.php.  NOTE: this issue can be leveraged to perform cross-site scripting (XSS) attacks and steal cookies via the post parameter.
CVE-2007-1245
IrfanView 3.99 allows remote attackers to cause a denial of service (application crash) via a malformed WMF file.
CVE-2007-1246
The DMO_VideoDecoder_Open function in loader/dmo/DMO_VideoDecoder.c in MPlayer 1.0rc1 and earlier, as used in xine-lib, does not set the biSize before use in a memcpy, which allows user-assisted remote attackers to cause a buffer overflow and possibly execute arbitrary code, a different vulnerability than CVE-2007-1387.
Failed exploit attempts will likely result in a denial-of-service condition.
CVE-2007-1247
Multiple PHP remote file inclusion vulnerabilities in aWeb Labs aWebNews 1.5 allow remote attackers to execute arbitrary PHP code via a URL in the path_to_news parameter to (1) listing.php or (2) visview.php.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-1248
Multiple cross-site scripting (XSS) vulnerabilities in built2go News Manager Blog 1.0 allow remote attackers to inject arbitrary web script or HTML via the (1) cid, (2) uid, and (3) nid parameters to (a) news.php, and the nid parameter to (b) rating.php.
CVE-2007-1249
MoveSortedContentAction in C1 Financial Services Contelligent 9.1.4 does not check "the additional environment security configuration," which allows remote attackers with write permissions to reorder components.
CVE-2007-1250
SQL injection vulnerability in section/default.asp in ANGEL Learning Management Suite (LMS) 7.1 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-1251
Format string vulnerability in the new_warning function in ntserv/warning.c for Netrek Vanilla Server 2.12.0, when EVENTLOG is enabled, allows remote attackers to cause a denial of service (crash) or execute arbitrary code via format string specifiers in the message handling.
This vulnerability is addressed in the following product update: 
http://sourceforge.net/project/shownotes.php?release_id=490561
CVE-2007-1252
Buffer overflow in Symantec Mail Security for SMTP 5.0 before Patch 175 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via crafted headers in an e-mail message.  NOTE: some information was obtained from third party sources.
CVE-2007-1253
Eval injection vulnerability in the (a) kmz_ImportWithMesh.py Script for Blender 0.1.9h, as used in (b) Blender before 2.43, allows user-assisted remote attackers to execute arbitrary Python code by importing a crafted (1) KML or (2) KMZ file.
This vulnerability is addressed in the following product update:
http://www.blender.org/download/get-blender/
CVE-2007-1254
SQL injection vulnerability in part.userprofile.php in Connectix Boards 0.7 and earlier allows remote authenticated users to execute arbitrary SQL commands and obtain privileges via the p_skin parameter to index.php.
CVE-2007-1255
Unrestricted file upload vulnerability in admin.bbcode.php in Connectix Boards 0.7 and earlier allows remote authenticated administrators to execute arbitrary PHP code by uploading a crafted GIF smiley image with a .php extension via the uploadimage parameter to admin.php, which can be later accessed via a direct request for the file in smileys/.  NOTE: this can be leveraged with a separate SQL injection issue for remote unauthenticated attacks.
CVE-2007-1256
Mozilla Firefox 2.0.0.2 allows remote attackers to spoof the address bar, favicons, and document source, and perform updates in the context of arbitrary websites, by repeatedly setting document.location in the onunload attribute when linking to another website, a variant of CVE-2007-1092.
CVE-2007-1257
The Network Analysis Module (NAM) in Cisco Catalyst Series 6000, 6500, and 7600 allows remote attackers to execute arbitrary commands via certain SNMP packets that are spoofed from the NAM's own IP address.
Per: http://www.cisco.com/warp/public/707/cisco-sa-20070228-nam.shtml#@ID

"Only Cisco Catalyst systems that have a NAM on them are affected. This vulnerability affects systems that run Internetwork Operating System (IOS) or Catalyst Operating System (CatOS). "
CVE-2007-1258
Unspecified vulnerability in Cisco IOS 12.2SXA, SXB, SXD, and SXF; and the MSFC2, MSFC2a and MSFC3 running in Hybrid Mode on Cisco Catalyst 6000, 6500 and Cisco 7600 series systems; allows remote attackers on a local network segment to cause a denial of service (software reload) via a certain MPLS packet.
CVE-2007-1259
Multiple unspecified vulnerabilities in WebAPP before 0.9.9.6 have unknown impact and attack vectors.
CVE-2007-1260
Stack-based buffer overflow in the connectHandle function in server.cpp in WebMod 0.48 allows remote attackers to execute arbitrary code via a long string in the Content-Length HTTP header.
CVE-2007-1261
Unspecified vulnerability in the reports system in OpenBiblio before 0.6.0 allows attackers to gain privileges via unspecified vectors.
This vulnerability is addressed in the following product update:
http://sourceforge.net/project/showfiles.php?group_id=50071
