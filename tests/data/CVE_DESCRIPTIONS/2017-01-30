CVE-2015-2180
The DBMail driver in the Password plugin in Roundcube before 1.1.0 allows remote attackers to execute arbitrary commands via shell metacharacters in the password.
CVE-2015-2181
Multiple buffer overflows in the DBMail driver in the Password plugin in Roundcube before 1.1.0 allow remote attackers to have unspecified impact via the (1) password or (2) username.
CVE-2015-7331
The mcollective-puppet-agent plugin before 1.11.1 for Puppet allows remote attackers to execute arbitrary code via vectors involving the --server argument.
CVE-2015-7973
NTP before 4.2.8p6 and 4.3.x before 4.3.90, when configured in broadcast mode, allows man-in-the-middle attackers to conduct replay attacks by sniffing the network.
CVE-2015-7975
The nextvar function in NTP before 4.2.8p6 and 4.3.x before 4.3.90 does not properly validate the length of its input, which allows an attacker to cause a denial of service (application crash).
CVE-2015-7976
The ntpq saveconfig command in NTP 4.1.2, 4.2.x before 4.2.8p6, 4.3, 4.3.25, 4.3.70, and 4.3.77 does not properly filter special characters, which allows attackers to cause unspecified impact via a crafted filename.
CVE-2015-7977
ntpd in NTP before 4.2.8p6 and 4.3.x before 4.3.90 allows remote attackers to cause a denial of service (NULL pointer dereference) via a ntpdc reslist command.
CVE-2015-7978
NTP before 4.2.8p6 and 4.3.0 before 4.3.90 allows a remote attackers to cause a denial of service (stack exhaustion) via an ntpdc relist command, which triggers recursive traversal of the restriction list.
CVE-2015-7979
NTP before 4.2.8p6 and 4.3.x before 4.3.90 allows remote attackers to cause a denial of service (client-server association tear down) by sending broadcast packets with invalid authentication to a broadcast client.
CVE-2015-8034
The state.sls function in Salt before 2015.8.3 uses weak permissions on the cache data, which allows local users to obtain sensitive information by reading the file.
CVE-2015-8138
NTP before 4.2.8p6 and 4.3.x before 4.3.90 allows remote attackers to bypass the origin timestamp validation via a packet with an origin timestamp set to zero.
CVE-2015-8139
ntpq in NTP before 4.2.8p7 allows remote attackers to obtain origin timestamps and then impersonate peers via unspecified vectors.
CVE-2015-8140
The ntpq protocol in NTP before 4.2.8p7 allows remote attackers to conduct replay attacks by sniffing the network.
CVE-2015-8158
The getresponse function in ntpq in NTP versions before 4.2.8p9 and 4.3.x before 4.3.90 allows remote attackers to cause a denial of service (infinite loop) via crafted packets with incorrect values.
<a href="http://cwe.mitre.org/data/definitions/835.html">CWE-835: Loop with Unreachable Exit Condition ('Infinite Loop')</a>
CVE-2016-10087
The png_set_text_2 function in libpng 0.71 before 1.0.67, 1.2.x before 1.2.57, 1.4.x before 1.4.20, 1.5.x before 1.5.28, and 1.6.x before 1.6.27 allows context-dependent attackers to cause a NULL pointer dereference vectors involving loading a text chunk into a png structure, removing the text, and then adding another text chunk to the structure.
CVE-2016-10174
The NETGEAR WNR2000v5 router contains a buffer overflow in the hidden_lang_avi parameter when invoking the URL /apply.cgi?/lang_check.html. This buffer overflow can be exploited by an unauthenticated attacker to achieve remote code execution.
CVE-2016-10175
The NETGEAR WNR2000v5 router leaks its serial number when performing a request to the /BRS_netgear_success.html URI. This serial number allows a user to obtain the administrator username and password, when used in combination with the CVE-2016-10176 vulnerability that allows resetting the answers to the password-recovery questions.
CVE-2016-10176
The NETGEAR WNR2000v5 router allows an administrator to perform sensitive actions by invoking the apply.cgi URL on the web server of the device. This special URL is handled by the embedded web server (uhttpd) and processed accordingly. The web server also contains another URL, apply_noauth.cgi, that allows an unauthenticated user to perform sensitive actions on the device. This functionality can be exploited to change the router settings (such as the answers to the password-recovery questions) and achieve remote code execution.
CVE-2016-10177
An issue was discovered on the D-Link DWR-932B router. Undocumented TELNET and SSH services provide logins to admin with the password admin and root with the password 1234.
CVE-2016-10178
An issue was discovered on the D-Link DWR-932B router. HELODBG on port 39889 (UDP) launches the "/sbin/telnetd -l /bin/sh" command.
CVE-2016-10179
An issue was discovered on the D-Link DWR-932B router. There is a hardcoded WPS PIN of 28296607.
CVE-2016-10180
An issue was discovered on the D-Link DWR-932B router. WPS PIN generation is based on srand(time(0)) seeding.
CVE-2016-10181
An issue was discovered on the D-Link DWR-932B router. qmiweb provides sensitive information for CfgType=get_homeCfg requests.
CVE-2016-10182
An issue was discovered on the D-Link DWR-932B router. qmiweb allows command injection with ` characters.
CVE-2016-10183
An issue was discovered on the D-Link DWR-932B router. qmiweb allows directory listing with ../ traversal.
CVE-2016-10184
An issue was discovered on the D-Link DWR-932B router. qmiweb allows file reading with ..%2f traversal.
CVE-2016-10185
An issue was discovered on the D-Link DWR-932B router. A secure_mode=no line exists in /var/miniupnpd.conf.
CVE-2016-10186
An issue was discovered on the D-Link DWR-932B router. /var/miniupnpd.conf has no deny rules.
CVE-2016-2217
The OpenSSL address implementation in Socat 1.7.3.0 and 2.0.0-b8 does not use a prime number for the DH, which makes it easier for remote attackers to obtain the shared secret.
CVE-2016-2399
Integer overflow in the quicktime_read_pascal function in libquicktime 1.2.4 and earlier allows remote attackers to cause a denial of service or possibly have other unspecified impact via a crafted hdlr MP4 atom.
CVE-2016-2402
OkHttp before 2.7.4 and 3.x before 3.1.2 allows man-in-the-middle attackers to bypass certificate pinning by sending a certificate chain with a certificate from a non-pinned trusted CA and the pinned certificate.
CVE-2016-2516
NTP before 4.2.8p7 and 4.3.x before 4.3.92, when mode7 is enabled, allows remote attackers to cause a denial of service (ntpd abort) by using the same IP address multiple times in an unconfig directive.
CVE-2016-2517
NTP before 4.2.8p7 and 4.3.x before 4.3.92 allows remote attackers to cause a denial of service (prevent subsequent authentication) by leveraging knowledge of the controlkey or requestkey and sending a crafted packet to ntpd, which changes the value of trustedkey, controlkey, or requestkey.  NOTE: this vulnerability exists because of a CVE-2016-2516 regression.
CVE-2016-2518
The MATCH_ASSOC function in NTP before version 4.2.8p9 and 4.3.x before 4.3.92 allows remote attackers to cause an out-of-bounds reference via an addpeer request with a large hmode value.
CVE-2016-2519
ntpd in NTP before 4.2.8p7 and 4.3.x before 4.3.92 allows remote attackers to cause a denial of service (ntpd abort) by a large request data value, which triggers the ctl_getitem function to return a NULL value.
CVE-2016-5026
hs.py in OnionShare before 0.9.1 allows local users to modify the hiddenservice by pre-creating the /tmp/onionshare directory.
CVE-2016-5434
libalpm, as used in pacman 5.0.1, allows remote attackers to cause a denial of service (infinite loop or out-of-bounds read) via a crafted signature file.
CVE-2016-6167
Multiple untrusted search path vulnerabilities in Putty beta 0.67 allow local users to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse (1) UxTheme.dll or (2) ntmarta.dll file in the current working directory.
CVE-2016-6266
ccca_ajaxhandler.php in Trend Micro Smart Protection Server 2.5 before build 2200, 2.6 before build 2106, and 3.0 before build 1330 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the (1) host or (2) apikey parameter in a register action, (3) enable parameter in a save_stting action, or (4) host or (5) apikey parameter in a test_connection action.
CVE-2016-6267
SnmpUtils in Trend Micro Smart Protection Server 2.5 before build 2200, 2.6 before build 2106, and 3.0 before build 1330 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the (1) spare_Community, (2) spare_AllowGroupIP, or (3) spare_AllowGroupNetmask parameter to admin_notification.php.
CVE-2016-6268
Trend Micro Smart Protection Server 2.5 before build 2200, 2.6 before build 2106, and 3.0 before build 1330 allows local webserv users to execute arbitrary code with root privileges via a Trojan horse .war file in the Solr webapps directory.
CVE-2016-6269
Multiple directory traversal vulnerabilities in Trend Micro Smart Protection Server 2.5 before build 2200, 2.6 before build 2106, and 3.0 before build 1330 allow remote attackers to read and delete arbitrary files via the tmpfname parameter to (1) log_mgt_adhocquery_ajaxhandler.php, (2) log_mgt_ajaxhandler.php, (3) log_mgt_ajaxhandler.php or (4) tf parameter to wcs_bwlists_handler.php.
CVE-2016-6270
The handle_certificate function in /vmi/manager/engine/management/commands/apns_worker.py in Trend Micro Virtual Mobile Infrastructure before 5.1 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the password to api/v1/cfg/oauth/save_identify_pfx/.
CVE-2016-6604
NULL pointer dereference in Samsung Exynos fimg2d driver for Android L(5.0/5.1) and M(6.0) allows attackers to have unspecified impact via unknown vectors.
CVE-2016-7544
Crypto++ 5.6.4 incorrectly uses Microsoft's stack-based _malloca and _freea functions. The library will request a block of memory to align a table in memory. If the table is later reallocated, then the wrong pointer could be freed.
CVE-2016-7798
The openssl gem for Ruby uses the same initialization vector (IV) in GCM Mode (aes-*-gcm) when the IV is set before the key, which makes it easier for context-dependent attackers to bypass the encryption protection mechanism.
CVE-2016-9119
Cross-site scripting (XSS) vulnerability in the link dialogue in GUI editor in MoinMoin before 1.9.8 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-9132
In Botan 1.8.0 through 1.11.33, when decoding BER data an integer overflow could occur, which would cause an incorrect length field to be computed. Some API callers may use the returned (incorrect and attacker controlled) length field in a way which later causes memory corruption or other failure.
CVE-2016-9939
Crypto++ (aka cryptopp and libcrypto++) 5.6.4 contained a bug in its ASN.1 BER decoding routine. The library will allocate a memory block based on the length field of the ASN.1 object. If there is not enough content octets in the ASN.1 object, then the function will fail and the memory block will be zeroed even if its unused. There is a noticeable delay during the wipe for a large allocation.
CVE-2017-5572
An issue was discovered in Linux Foundation xapi in Citrix XenServer through 7.0. An authenticated read-only administrator can corrupt the host database.
CVE-2017-5573
An issue was discovered in Linux Foundation xapi in Citrix XenServer through 7.0. An authenticated read-only administrator can cancel tasks of other administrators.
CVE-2017-5610
wp-admin/includes/class-wp-press-this.php in Press This in WordPress before 4.7.2 does not properly restrict visibility of a taxonomy-assignment user interface, which allows remote attackers to bypass intended access restrictions by reading terms.
CVE-2017-5611
SQL injection vulnerability in wp-includes/class-wp-query.php in WP_Query in WordPress before 4.7.2 allows remote attackers to execute arbitrary SQL commands by leveraging the presence of an affected plugin or theme that mishandles a crafted post type name.
CVE-2017-5612
Cross-site scripting (XSS) vulnerability in wp-admin/includes/class-wp-posts-list-table.php in the posts list table in WordPress before 4.7.2 allows remote attackers to inject arbitrary web script or HTML via a crafted excerpt.
CVE-2017-5627
An issue was discovered in Artifex Software, Inc. MuJS before 4006739a28367c708dea19aeb19b8a1a9326ce08. The jsR_setproperty function in jsrun.c lacks a check for a negative array length. This leads to an integer overflow in the js_pushstring function in jsrun.c when parsing a specially crafted JS file.
CVE-2017-5628
An issue was discovered in Artifex Software, Inc. MuJS before 8f62ea10a0af68e56d5c00720523ebcba13c2e6a. The MakeDay function in jsdate.c does not validate the month, leading to an integer overflow when parsing a specially crafted JS file.
CVE-2017-5632
An issue was discovered on the ASUS RT-N56U Wireless Router with Firmware 3.0.0.4.374_979. When executing an "nmap -O" command that specifies an IP address of an affected device, one can crash the device's WAN connection, causing disconnection from the Internet, a Denial of Service (DoS). The attack is only possible from within the local area network.
