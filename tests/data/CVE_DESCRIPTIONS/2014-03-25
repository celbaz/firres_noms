CVE-2013-1604
Directory traversal vulnerability in MayGion IP Cameras with firmware before 2013.04.22 (05.53) allows remote attackers to read arbitrary files via a .. (dot dot) in the default URI.
Per: http://www.coresecurity.com/advisories/maygion-IP-cameras-multiple-vulnerabilities

"Multiple vulnerabilities have been found in MayGion IP cameras [1] based on firmware v09.27 and below"
CVE-2013-1605
Buffer overflow in MayGion IP Cameras with firmware before 2013.04.22 (05.53) allows remote attackers to execute arbitrary code via a long filename in a GET request.
Per: http://www.coresecurity.com/advisories/maygion-IP-cameras-multiple-vulnerabilities

"Multiple vulnerabilities have been found in MayGion IP cameras [1] based on firmware v09.27 and below"
CVE-2013-5443
Cross-site request forgery (CSRF) vulnerability in IBM Cognos Express 9.0 before IFIX 2, 9.5 before IFIX 2, 10.1 before IFIX 2, and 10.2.1 before FP1 allows remote attackers to hijack the authentication of arbitrary users.
CVE-2013-5444
The server in IBM Cognos Express 9.0 before IFIX 2, 9.5 before IFIX 2, 10.1 before IFIX 2, and 10.2.1 before FP1 allows remote attackers to read encrypted credentials via unspecified vectors.
CVE-2013-5445
IBM Cognos Express 9.0 before IFIX 2, 9.5 before IFIX 2, 10.1 before IFIX 2, and 10.2.1 before FP1 allows local users to obtain sensitive cleartext information by leveraging knowledge of a static decryption key.
Per: http://www-01.ibm.com/support/docview.wss?uid=swg21667626

"Encrypted credentials can be remotely retrieved from the IBM Cognos Express server."
CVE-2013-5951
Multiple cross-site scripting (XSS) vulnerabilities in eXtplorer 2.1.3, when used as a component for Joomla!, allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) application.js.php in scripts/ or (2) admin.php, (3) copy_move.php, (4) functions.php, (5) header.php, or (6) upload.php in include/.
CVE-2014-0076
The Montgomery ladder implementation in OpenSSL through 1.0.0l does not ensure that certain swap operations have a constant-time behavior, which makes it easier for local users to obtain ECDSA nonces via a FLUSH+RELOAD cache side-channel attack.
CVE-2014-0343
The web interface on Virtual Access GW6110A routers with software 9.00 before 9.09.27, 9.50 before 9.50.21, and 10.00 before 10.00.21 allows remote authenticated users to gain privileges via a modified JavaScript variable.
Per: http://cwe.mitre.org/data/definitions/472.html

"CWE-472: External Control of Assumed-Immutable Web Parameter"
CVE-2014-0628
The server in EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.5 does not properly process certificate chains, which allows remote attackers to cause a denial of service (daemon crash) via unspecified vectors.
CVE-2014-0884
Cross-site scripting (XSS) vulnerability in the Admin Web UI in IBM Lotus Protector for Mail Security 2.8.x before 2.8.1-22905 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-0885
Cross-site request forgery (CSRF) vulnerability in the Admin Web UI in IBM Lotus Protector for Mail Security 2.8.x before 2.8.1-22905 allows remote authenticated users to hijack the authentication of unspecified victims via unknown vectors.
CVE-2014-0886
The Admin Web UI in IBM Lotus Protector for Mail Security 2.8.x before 2.8.1-22905 allows remote authenticated users to bypass intended access restrictions and execute arbitrary commands via unspecified vectors.
CVE-2014-0887
The Admin Web UI in IBM Lotus Protector for Mail Security 2.8.x before 2.8.1-22905 allows remote authenticated users to execute arbitrary commands with root privileges via unspecified vectors.
CVE-2014-1492
The cert_TestHostName function in lib/certdb/certdb.c in the certificate-checking implementation in Mozilla Network Security Services (NSS) before 3.16 accepts a wildcard character that is embedded in an internationalized domain name's U-label, which might allow man-in-the-middle attackers to spoof SSL servers via a crafted certificate.
CVE-2014-1515
Mozilla Firefox before 28.0.1 on Android processes a file: URL by copying a local file onto the SD card, which allows attackers to obtain sensitive information from the Firefox profile directory via a crafted application.
CVE-2014-1761
Microsoft Word 2003 SP3, 2007 SP3, 2010 SP1 and SP2, 2013, and 2013 RT; Word Viewer; Office Compatibility Pack SP3; Office for Mac 2011; Word Automation Services on SharePoint Server 2010 SP1 and SP2 and 2013; Office Web Apps 2010 SP1 and SP2; and Office Web Apps Server 2013 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted RTF data, as exploited in the wild in March 2014.
CVE-2014-2016
Multiple cross-site scripting (XSS) vulnerabilities in OXID eShop Professional and Community Edition 4.6.8 and earlier, 4.7.x before 4.7.11, and 4.8.x before 4.8.4, and Enterprise Edition 4.6.8 and earlier, 5.0.x before 5.0.11 and 5.1.x before 5.1.4 allow remote attackers to inject arbitrary web script or HTML via the searchtag parameter to the getTag function in (1) application/controllers/details.php or (2) application/controllers/tag.php.
CVE-2014-2386
Multiple off-by-one errors in Icinga, possibly 1.10.2 and earlier, allow remote attackers to cause a denial of service (crash) via unspecified vectors to the (1) display_nav_table, (2) print_export_link, (3) page_num_selector, or (4) page_limit_selector function in cgi/cgiutils.c or (5) status_page_num_selector function in cgi/status.c, which triggers a stack-based buffer overflow.
CVE-2014-2526
Multiple cross-site scripting (XSS) vulnerabilities in BarracudaDrive before 6.7 allow remote attackers to inject arbitrary web script or HTML via the (1) sForumName or (2) sDescription parameter to Forum/manage/ForumManager.lsp; (3) sHint, (4) sWord, or (5) nId parameter to Forum/manage/hangman.lsp; (6) user parameter to rtl/protected/admin/wizard/setuser.lsp; (7) name or (8) email parameter to feedback.lsp; (9) lname or (10) url parameter to private/manage/PageManager.lsp; (11) cmd parameter to fs; (12) newname, (13) description, (14) firstname, (15) lastname, or (16) id parameter to rtl/protected/mail/manage/list.lsp; or (17) PATH_INFO to fs/.
CVE-2014-2538
Cross-site scripting (XSS) vulnerability in lib/rack/ssl.rb in the rack-ssl gem before 1.4.0 for Ruby allows remote attackers to inject arbitrary web script or HTML via a URI, which might not be properly handled by third-party adapters such as JRuby-Rack.
CVE-2014-2573
The VMWare driver in OpenStack Compute (Nova) 2013.2 through 2013.2.2 does not properly put VMs into RESCUE status, which allows remote authenticated users to bypass the quota limit and cause a denial of service (resource consumption) by requesting the VM be put into rescue and then deleting the image.
