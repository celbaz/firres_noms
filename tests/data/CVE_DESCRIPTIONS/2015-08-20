CVE-2015-0533
EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.8 and 4.1.x before 4.1.3 and RSA BSAFE SSL-C 2.8.9 and earlier allow remote SSL servers to conduct ECDHE-to-ECDH downgrade attacks and trigger a loss of forward secrecy by omitting the ServerKeyExchange message, a similar issue to CVE-2014-3572.
CVE-2015-0534
EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.8 and 4.1.x before 4.1.3, RSA BSAFE Crypto-J before 6.2, RSA BSAFE SSL-J before 6.2, and RSA BSAFE SSL-C 2.8.9 and earlier do not enforce certain constraints on certificate data, which allows remote attackers to defeat a fingerprint-based certificate-blacklist protection mechanism by including crafted data within a certificate's unsigned portion, a similar issue to CVE-2014-8275.
CVE-2015-0535
EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.8 and 4.1.x before 4.1.3 and RSA BSAFE SSL-C 2.8.9 and earlier do not properly restrict TLS state transitions, which makes it easier for remote attackers to conduct cipher-downgrade attacks to EXPORT_RSA ciphers via crafted TLS traffic, related to the "FREAK" issue, a similar issue to CVE-2015-0204.
CVE-2015-0536
EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.8 and 4.1.x before 4.1.3 and RSA BSAFE SSL-C 2.8.9 and earlier, when client authentication and an ephemeral Diffie-Hellman ciphersuite are enabled, allow remote attackers to cause a denial of service (daemon crash) via a ClientKeyExchange message with a length of zero, a similar issue to CVE-2015-1787.
CVE-2015-0537
Integer underflow in the base64-decoding implementation in EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.8 and 4.1.x before 4.1.3, RSA BSAFE Crypto-C Micro Edition (Crypto-C ME) before 4.0.4 and 4.1, and RSA BSAFE SSL-C 2.8.9 and earlier allows remote attackers to cause a denial of service (memory corruption or segmentation fault) or possibly have unspecified other impact via crafted base64 data, a similar issue to CVE-2015-0292.
CVE-2015-0542
Multiple cross-site request forgery (CSRF) vulnerabilities in EMC RSA Archer GRC 5.5 SP1 before P3 allow remote attackers to hijack the authentication of arbitrary users.
CVE-2015-3219
Cross-site scripting (XSS) vulnerability in the Orchestration/Stack section in OpenStack Dashboard (Horizon) 2014.2 before 2014.2.4 and 2015.1.x before 2015.1.1 allows remote attackers to inject arbitrary web script or HTML via the description parameter in a heat template, which is not properly handled in the help_text attribute in the Field class.
CVE-2015-4303
Cisco TelePresence Video Communication Server (VCS) X8.5.2 allows remote authenticated users to execute arbitrary commands in the context of the nobody user account via an unspecified web-page parameter, aka Bug ID CSCuv12333.
CVE-2015-4314
The System Snapshot feature in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.1 allows remote authenticated users to obtain sensitive password-hash information by reading the snapshot file, aka Bug ID CSCuv40422.
CVE-2015-4315
The Call Policy Configuration page in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.3 improperly validates external DTDs, which allows remote authenticated users to read arbitrary files or cause a denial of service via a crafted XML document, aka Bug ID CSCuv31853.
CVE-2015-4316
The Mobile and Remote Access (MRA) endpoint-validation feature in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 improperly validates the phone line used for registration, which allows remote authenticated users to conduct impersonation attacks via a crafted registration, aka Bug ID CSCuv40396.
CVE-2015-4317
Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 allows remote attackers to cause a denial of service via invalid variables in an authentication packet, aka Bug ID CSCuv40469.
CVE-2015-4318
Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 allows remote attackers to cause a denial of service via invalid variables in a GET request, aka Bug ID CSCuv40528.
CVE-2015-4319
The password-change feature in the administrative web interface in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.1 improperly performs authorization, which allows remote authenticated users to reset arbitrary active-user passwords via unspecified vectors, aka Bug ID CSCuv12338.
CVE-2015-4320
The Configuration Log File component in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 allows remote authenticated users to obtain sensitive information by reading a log file, aka Bug ID CSCuv12340.
CVE-2015-4321
The Unicast Reverse Path Forwarding (uRPF) implementation in Cisco Adaptive Security Appliance (ASA) Software 9.3(1.50), 9.3(2.100), 9.3(3), and 9.4(1) mishandles cases where an IP address belongs to an internal interface but is also in the ASA routing table, which allows remote attackers to bypass uRPF validation via spoofed packets, aka Bug ID CSCuv60724.
CVE-2015-4327
The CLI in Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 allows local users to obtain root privileges by writing script arguments to an unspecified file, aka Bug ID CSCuv12542.
CVE-2015-4328
Cisco TelePresence Video Communication Server (VCS) Expressway X8.5.2 improperly checks for a user account's read-only attribute, which allows remote authenticated users to execute arbitrary OS commands via crafted HTTP requests, as demonstrated by read or write operations on the Unified Communications lookup page, aka Bug ID CSCuv12552.
CVE-2015-4329
The administrator web interface in Cisco TelePresence Video Communication Server (VCS) X8.5.2 allows remote authenticated users to execute arbitrary OS commands via crafted HTTP requests, aka Bug ID CSCuv11796.
CVE-2015-4530
Cross-site request forgery (CSRF) vulnerability in EMC Documentum WebTop before 6.8P01, Documentum Administrator through 7.2, Documentum Digital Assets Manager through 6.5SP6, Documentum Web Publishers through 6.5SP7, and Documentum Task Space through 6.7SP2 allows remote attackers to hijack the authentication of arbitrary users.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-2518.
CVE-2015-4531
EMC Documentum Content Server before 6.7SP1 P32, 6.7SP2 before P25, 7.0 before P19, 7.1 before P16, and 7.2 before P02 does not properly check authorization for subgroups of privileged groups, which allows remote authenticated sysadmins to gain super-user privileges, and bypass intended restrictions on data access and server actions, via unspecified vectors.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-4622.
CVE-2015-4532
EMC Documentum Content Server before 6.7SP1 P32, 6.7SP2 before P25, 7.0 before P19, 7.1 before P16, and 7.2 before P02 does not properly check authorization and does not properly restrict object types, which allows remote authenticated users to run save RPC commands with super-user privileges, and consequently execute arbitrary code, via unspecified vectors.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-2514.
CVE-2015-4533
EMC Documentum Content Server before 6.7SP1 P32, 6.7SP2 before P25, 7.0 before P19, 7.1 before P16, and 7.2 before P02 does not properly check authorization after creation of an object, which allows remote authenticated users to execute arbitrary code with super-user privileges via a custom script.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-2513.
CVE-2015-4534
Java Method Server (JMS) in EMC Documentum Content Server before 6.7SP1 P32, 6.7SP2 before P25, 7.0 before P19, 7.1 before P16, and 7.2 before P02 allows remote authenticated users to execute arbitrary code by forging a signature for a query string that lacks the method_verb parameter.
CVE-2015-4535
Java Method Server (JMS) in EMC Documentum Content Server before 6.7SP1 P32, 6.7SP2 before P25, 7.0 before P19, 7.1 before P16, and 7.2 before P02, when __debug_trace__ is configured, allows remote authenticated users to gain super-user privileges by leveraging the ability to read a log file containing a login ticket.
CVE-2015-4536
EMC Documentum Content Server before 7.0 P20, 7.1 before P18, and 7.2 before P02, when RPC tracing is configured, stores certain obfuscated password data in a log file, which allows remote authenticated users to obtain sensitive information by reading this file.
CVE-2015-5192
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2015-5195.  Reason: This candidate is a reservation duplicate of CVE-2015-5195.  Notes: All CVE users should reference CVE-2015-5195 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2015-5193
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2015-7703.  Reason: This candidate is a reservation duplicate of CVE-2015-7703.  Notes: All CVE users should reference CVE-2015-7703 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2015-6528
Multiple cross-site scripting (XSS) vulnerabilities in install_classic.php in Coppermine Photo Gallery (CPG) 1.5.36 allow remote attackers to inject arbitrary web script or HTML via the (1) admin_username, (2) admin_password, (3) admin_email, (4) dbserver, (5) dbname, (6) dbuser, (7) dbpass, (8) table_prefix, or (9) impath parameter.
CVE-2015-6529
Multiple cross-site scripting (XSS) vulnerabilities in phpipam 1.1.010 allow remote attackers to inject arbitrary web script or HTML via the (1) section parameter to site/error.php or (2) ip parameter to site/tools/searchResults.php.
CVE-2015-6530
Cross-site scripting (XSS) vulnerability in OpenText Secure MFT 2013 before 2013 R3 P6 and 2014 before 2014 R2 P2 allows remote attackers to inject arbitrary web script or HTML via the querytext parameter to userdashboard.jsp.
