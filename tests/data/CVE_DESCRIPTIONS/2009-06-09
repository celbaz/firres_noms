CVE-2008-2475
eBay Enhanced Picture Uploader ActiveX control (EPUWALcontrol.dll) before 1.0.27 allows remote attackers to execute arbitrary commands via the PictureUrls property.
Per http://www.kb.cert.org/vuls/id/983731

This update is addressed in version 1.0.27 of the Ebay Enhanced Picture Control software.
CVE-2009-0791
Multiple integer overflows in Xpdf 2.x and 3.x and Poppler 0.x, as used in the pdftops filter in CUPS 1.1.17, 1.1.22, and 1.3.7, GPdf, and kdegraphics KPDF, allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted PDF file that triggers a heap-based buffer overflow, possibly related to (1) Decrypt.cxx, (2) FoFiTrueType.cxx, (3) gmem.c, (4) JBIG2Stream.cxx, and (5) PSOutputDev.cxx in pdftops/. NOTE: the JBIG2Stream.cxx vector may overlap CVE-2009-1179.
CVE-2009-0949
The ippReadIO function in cups/ipp.c in cupsd in CUPS before 1.3.10 does not properly initialize memory for IPP request packets, which allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via a scheduler request with two consecutive IPP_TAG_UNSUPPORTED tags.
CVE-2009-1196
The directory-services functionality in the scheduler in CUPS 1.1.17 and 1.1.22 allows remote attackers to cause a denial of service (cupsd daemon outage or crash) via manipulations of the timing of CUPS browse packets, related to a "pointer use-after-delete flaw."
CVE-2009-1296
The eCryptfs support utilities (ecryptfs-utils) 73-0ubuntu6.1 on Ubuntu 9.04 stores the mount passphrase in installation logs, which might allow local users to obtain access to the filesystem by reading the log files from disk.  NOTE: the log files are only readable by root.
CVE-2009-2012
Unspecified vulnerability in idmap in Sun OpenSolaris snv_88 through snv_110, when a CIFS server is enabled, allows local users to cause a denial of service (idpmapd daemon crash and idmapd outage) via unknown vectors.
CVE-2009-2013
SQL injection vulnerability in bin/aps_browse_sources.php in Frontis 3.9.01.24 allows remote attackers to execute arbitrary SQL commands via the source_class parameter in a browse_classes action.
CVE-2009-2014
SQL injection vulnerability in the ComSchool (com_school) component 1.4 for Joomla! allows remote attackers to execute arbitrary SQL commands via the classid parameter in a showclass action to index.php.
CVE-2009-2015
Directory traversal vulnerability in includes/file_includer.php in the Ideal MooFAQ (com_moofaq) component 1.0 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter.
CVE-2009-2016
SQL injection vulnerability in products.php in Virtue Shopping Mall allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2009-2017
SQL injection vulnerability in products.php in Virtue Book Store allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2009-2018
SQL injection vulnerability in admin/index.php in Jared Eckersley MyCars, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the authuserid parameter.
CVE-2009-2019
SQL injection vulnerability in news_detail.php in Virtue News Manager allows remote attackers to execute arbitrary SQL commands via the nid parameter.
CVE-2009-2020
Cross-site scripting (XSS) vulnerability in news_detail.php in Virtue News Manager allows remote attackers to inject arbitrary web script or HTML via the nid parameter.
CVE-2009-2021
SQL injection vulnerability in search.php in Virtue Classifieds allows remote attackers to execute arbitrary SQL commands via the category parameter.
CVE-2009-2022
fipsCMS Light 2.1 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file and obtain sensitive information via a direct request for _fipsdb/db.mdb.
CVE-2009-2023
SQL injection vulnerability in index.php in Shop-Script Pro 2.12, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the current_currency parameter.
CVE-2009-2024
Vlad Titarenko ASP VT Auth 1.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file and obtain usernames and passwords via a direct request for zHk8dEes3.txt.
CVE-2009-2025
admin/login.php in DM FileManager 3.9.2 allows remote attackers to bypass authentication and gain administrative access by setting the (1) USER, (2) GROUPID, (3) GROUP, and (4) USERID cookies to certain values.
