CVE-2007-0328
The DWUpdateService ActiveX control in the agent (agent.exe) in Macrovision FLEXnet Connect 6.0 and Update Service 3.x to 5.x allows remote attackers to execute arbitrary commands via (1) the Execute method, and obtain the exit status using (2) the GetExitCode method.
CVE-2007-1362
Mozilla Firefox 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, and SeaMonkey 1.0.9 and 1.1.2, allows remote attackers to cause a denial of service via (1) a large cookie path parameter, which triggers memory consumption, or (2) an internal delimiter within cookie path or name values, which could trigger a misinterpretation of cookie data, aka "Path Abuse in Cookies."
CVE-2007-2867
Multiple vulnerabilities in the layout engine for Mozilla Firefox 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, Thunderbird 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, and SeaMonkey 1.0.9 and 1.1.2 allow remote attackers to cause a denial of service (crash) via vectors related to dangling pointers, heap corruption, signed/unsigned, and other issues.
CVE-2007-2868
Multiple vulnerabilities in the JavaScript engine for Mozilla Firefox 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, Thunderbird 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, and SeaMonkey 1.0.9 and 1.1.2 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via vectors that trigger memory corruption.
CVE-2007-2869
The form autocomplete feature in Mozilla Firefox 1.5.x before 1.5.0.12, 2.x before 2.0.0.4, and possibly earlier versions, allows remote attackers to cause a denial of service (persistent temporary CPU consumption) via a large number of characters in a submitted form.
CVE-2007-2870
Mozilla Firefox 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, and SeaMonkey 1.0.9 and 1.1.2, allows remote attackers to bypass the same-origin policy and conduct cross-site scripting (XSS) and other attacks by using the addEventListener method to add an event listener for a site, which is executed in the context of that site.
CVE-2007-2871
Mozilla Firefox 1.5.x before 1.5.0.12 and 2.x before 2.0.0.4, and SeaMonkey 1.0.9 and 1.1.2, allows remote attackers to spoof or hide the browser chrome, such as the location bar, by placing XUL popups outside of the browser's content pane.  NOTE: this issue can be leveraged for phishing and other attacks.
CVE-2007-2917
Multiple buffer overflows in a certain ActiveX control in odapi.dll in Authentium Command Antivirus before 4.93.8 allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-2918
Multiple stack-based buffer overflows in ActiveX controls (1) VibeC in (a) vibecontrol.dll, (2) CallManager and (3) ViewerClient in (b) StarClient.dll, (4) ComLink in (c) uicomlink.dll, and (5) WebCamXMP in (d) wcamxmp.dll in Logitech VideoCall allow remote attackers to cause a denial of service (browser crash) and execute arbitrary code via unspecified vectors.
CVE-2007-2968
Cross-site scripting (XSS) vulnerability in register.php in cpCommerce 1.1.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the name parameter (Full Name field).
CVE-2007-2969
PHP remote file inclusion vulnerability in newsletter.php in WAnewsletter 2.1.3 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the waroot parameter.
CVE-2007-2970
Multiple cross-site scripting (XSS) vulnerabilities in cgi/block.cgi in 8e6 R3000 Internet Filter allow remote attackers to inject arbitrary web script or HTML via the (1) URL, (2) CAT, and (3) USER parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2971
SQL injection vulnerability in getnewsitem.php in gCards 1.46 and earlier allows remote attackers to execute arbitrary SQL commands via the newsid parameter.
CVE-2007-2972
The file parsing engine in Avira Antivir Antivirus before 7.04.00.24 allows remote attackers to cause a denial of service (application crash) via a crafted UPX compressed file, which triggers a divide-by-zero error.
CVE-2007-2973
Avira Antivir Antivirus before 7.03.00.09 allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a malformed TAR archive.
This vulnerability is reportedly resolved in the following product version: 7.03.00.09
CVE-2007-2974
Buffer overflow in the file parsing engine in Avira Antivir Antivirus before 7.03.00.09 allows remote attackers to execute arbitrary code via a crafted LZH archive file, resulting from an "integer cast around."
CVE-2007-2975
The admin console in Ignite Realtime Openfire 3.3.0 and earlier (formerly Wildfire) does not properly specify a filter mapping in web.xml, which allows remote attackers to gain privileges and execute arbitrary code by accessing functionality that is exposed through DWR, as demonstrated using the downloader.
The vendor has addressed this issue through the release of the following product updates:

Ignite Realtime openfire-3.3.1-1.i386.rpm
http://www.igniterealtime.org/downloads/download-landing.jsp?file=open fire/openfire-3.3.1-1.i386.rpm

Ignite Realtime openfire_3_3_1.dmg
http://www.igniterealtime.org/downloads/download-landing.jsp?file=open fire/openfire_3_3_1.dmg

Ignite Realtime openfire_3_3_1.exe
http://www.igniterealtime.org/downloads/download-landing.jsp?file=open fire/openfire_3_3_1.exe
CVE-2007-2976
Centrinity FirstClass 8.3 and earlier, and Server and Internet Services 8.0 and earlier, do not properly handle a URL with a null ("%00") character, which allows remote attackers to conduct cross-site scripting (XSS) attacks.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2977
Buffer overflow in the receive function in submit/submitcommon.c in the submit daemon in DOMjudge before 2.0.0RC1 allows remote attackers to cause a denial of service or have other unspecified impact.  NOTE: some of these details are obtained from third party information.
CVE-2007-2978
Session fixation vulnerability in eggblog 3.1.0 and earlier allows remote attackers to hijack web sessions by setting the PHPSESSID parameter.
CVE-2007-2979
Techno Dreams Web Directory / Search Engine 2.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for Database.mdb.
CVE-2007-2980
Heap-based buffer overflow in a certain ActiveX control in LEADTOOLS LEAD Raster ISIS Object (LTRIS14e.DLL) 14.5.0.44 allows remote attackers to cause a denial of service (Internet Explorer crash) or execute arbitrary code via a long DriverName property, a different ActiveX control than CVE-2007-2827.
CVE-2007-2981
Buffer overflow in a certain ActiveX control in LEAD Technologies LEADTOOLS Raster OCR Document Object Library (ltrdc14e.dll) 14.5.0.44 allows remote attackers to execute arbitrary code via a long DictionaryFileName property.
CVE-2007-2982
Multiple buffer overflows in the British Telecommunications Business Connect webhelper ActiveX control before 1.0.0.7 in btbconnectwebcontrol.dll allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-2984
Multiple stack-based buffer overflows in the Media Technology Group CDPass ActiveX control in CDPass.dll allow remote attackers to execute arbitrary code via unspecified vectors, possibly involving the GetTOC2 method.
CVE-2007-2985
Pheap 2.0 allows remote attackers to bypass authentication by setting a pheap_login cookie value to the administrator's username, which can be used to (1) obtain sensitive information, including the administrator password, via settings.php or (2) upload and execute arbitrary PHP code via an update_doc action in edit.php.
CVE-2007-2986
PHP remote file inclusion vulnerability in lib/live_status.lib.php in AdminBot MX 9.0.5 allows remote attackers to execute arbitrary PHP code via a URL in the ROOT parameter.
CVE-2007-2987
Multiple buffer overflows in certain ActiveX controls in sasatl.dll in Zenturi ProgramChecker allow remote attackers to execute arbitrary code via unspecified vectors, possibly involving the (1) DebugMsgLog or (2) DoFileProperties methods.
Failed exploit attempts will likely result in denial-of-service condition.
CVE-2007-2988
A certain admin script in Inout Meta Search Engine sends a redirect to the web browser but does not exit when administrative credentials are missing, which allows remote attackers to inject arbitrary PHP code, as demonstrated by a request to admin/create_engine.php followed by a request to admin/generate_tabs.php.
CVE-2007-2989
The libike library in Sun Solaris 9 before 20070529 contains a logic error related to a certain pointer, which allows remote attackers to cause a denial of service (in.iked daemon crash) by sending certain UDP packets with a source port different from 500.  NOTE: this issue might overlap CVE-2006-2298.
CVE-2007-2990
Unspecified vulnerability in inetd in Sun Solaris 10 before 20070529 allows local users to cause a denial of service (daemon termination) via unspecified manipulations of the /var/run/.inetd.uds Unix domain socket file.
