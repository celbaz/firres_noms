CVE-2008-6399
Unspecified vulnerability in DotNetNuke 4.5.2 through 4.9 allows remote attackers to "add additional roles to their user account" via unknown attack vectors.
CVE-2008-6400
Cross-site scripting (XSS) vulnerability in refbase before 0.9.5 allows remote attackers to inject arbitrary web script or HTML via the headerMsg parameter to (1) show.php and (2) search.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-0037
The redirect implementation in curl and libcurl 5.11 through 7.19.3, when CURLOPT_FOLLOWLOCATION is enabled, accepts arbitrary Location values, which might allow remote HTTP servers to (1) trigger arbitrary requests to intranet servers, (2) read or overwrite arbitrary files via a redirect to a file: URL, or (3) execute arbitrary commands via a redirect to an scp: URL.
CVE-2009-0186
Integer overflow in libsndfile 1.0.18, as used in Winamp and other products, allows context-dependent attackers to execute arbitrary code via crafted description chunks in a CAF audio file, leading to a heap-based buffer overflow.
CVE-2009-0365
nm-applet.conf in GNOME NetworkManager before 0.7.0.99 contains an incorrect deny setting, which allows local users to discover (1) network connection passwords and (2) pre-shared keys via calls to the GetSecrets method in the dbus request handler.
CVE-2009-0367
The Python AI module in Wesnoth 1.4.x and 1.5 before 1.5.11 allows remote attackers to escape the sandbox and execute arbitrary code by using a whitelisted module that imports an unsafe module, then using a hierarchical module name to access the unsafe module through the whitelisted module.
CVE-2009-0578
GNOME NetworkManager before 0.7.0.99 does not properly verify privileges for dbus (1) modify and (2) delete requests, which allows local users to change or remove the network connections of arbitrary users via unspecified vectors related to org.freedesktop.NetworkManagerUserSettings and at_console.
CVE-2009-0619
Unspecified vulnerability in the Session Border Controller (SBC) before 3.0(2) for Cisco 7600 series routers allows remote attackers to cause a denial of service (SBC card reload) via crafted packets to TCP port 2000.
CVE-2009-0771
The layout engine in Mozilla Firefox before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey 1.1.15 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via certain vectors that trigger memory corruption and assertion failures.
CVE-2009-0772
The layout engine in Mozilla Firefox 2 and 3 before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey 1.1.15 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via vectors related to nsCSSStyleSheet::GetOwnerNode, events, and garbage collection, which triggers memory corruption.
CVE-2009-0773
The JavaScript engine in Mozilla Firefox before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey 1.1.15 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via (1) a splice of an array that contains "some non-set elements," which causes jsarray.cpp to pass an incorrect argument to the ResizeSlots function, which triggers memory corruption; (2) vectors related to js_DecompileValueGenerator, jsopcode.cpp, __defineSetter__, and watch, which triggers an assertion failure or a segmentation fault; and (3) vectors related to gczeal, __defineSetter__, and watch, which triggers a hang.
CVE-2009-0774
The layout engine in Mozilla Firefox 2 and 3 before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey 1.1.15 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via vectors related to gczeal, a different vulnerability than CVE-2009-0773.
CVE-2009-0775
Double free vulnerability in Mozilla Firefox before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey before 1.1.15 allows remote attackers to execute arbitrary code via "cloned XUL DOM elements which were linked as a parent and child," which are not properly handled during garbage collection.
CVE-2009-0776
nsIRDFService in Mozilla Firefox before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey before 1.1.15 allows remote attackers to bypass the same-origin policy and read XML data from another domain via a cross-domain redirect.
CVE-2009-0777
Mozilla Firefox before 3.0.7, Thunderbird before 2.0.0.21, and SeaMonkey before 1.1.15 decode invisible characters when they are displayed in the location bar, which causes an incorrect address to be displayed and makes it easier for remote attackers to spoof URLs and conduct phishing attacks.
CVE-2009-0813
Insecure method vulnerability in the ImeraIEPlugin ActiveX control (ImeraIEPlugin.dll 1.0.2.54) in Imera TeamLinks Client allows remote attackers to force the download and execution of arbitrary URLs via modified DownloadProtocol, DownloadHost, DownloadPort, and DownloadURI parameters.
CVE-2009-0814
Cross-site scripting (XSS) vulnerability in Widgets.aspx in Blogsa 1.0 Beta 3 and earlier allows remote attackers to inject arbitrary web script or HTML via the searchText parameter.
CVE-2009-0815
The jumpUrl mechanism in class.tslib_fe.php in TYPO3 3.3.x through 3.8.x, 4.0 before 4.0.12, 4.1 before 4.1.10, 4.2 before 4.2.6, and 4.3alpha1 leaks a hash secret (juHash) in an error message, which allows remote attackers to read arbitrary files by including the hash in a request.
CVE-2009-0816
Multiple cross-site scripting (XSS) vulnerabilities in the backend user interface in TYPO3 3.3.x through 3.8.x, 4.0 before 4.0.12, 4.1 before 4.1.10, 4.2 before 4.2.6, and 4.3alpha1 allow remote attackers to inject arbitrary web script or HTML via unspecified fields.
CVE-2009-0817
Cross-site scripting (XSS) vulnerability in the Protected Node module 5.x before 5.x-1.4 and 6.x before 6.x-1.5, a module for Drupal, allows remote authenticated users with "administer site configuration" permissions to inject arbitrary web script or HTML via the Password page info field, which is not properly handled by the protected_node_enterpassword function in protected_node.module.
CVE-2009-0818
Cross-site scripting (XSS) vulnerability in the taxonomy_theme_admin_table_builder function (taxonomy_theme_admin.inc) in Taxonomy Theme module before 5.x-1.2, a module for Drupal, allows remote authenticated users with the "administer taxonomy" permission, or the ability to create pages when tagging is enabled, to inject arbitrary web script or HTML via the Vocabulary name (name parameter) to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-0819
sql/item_xmlfunc.cc in MySQL 5.1 before 5.1.32 and 6.0 before 6.0.10 allows remote authenticated users to cause a denial of service (crash) via "an XPath expression employing a scalar expression as a FilterExpr with ExtractValue() or UpdateXML()," which triggers an assertion failure.
CVE-2009-0820
Multiple eval injection vulnerabilities in phpScheduleIt before 1.2.11 allow remote attackers to execute arbitrary code via (1) the end_date parameter to reserve.php and (2) the start_date and end_date parameters to check.php.  NOTE: the start_date/reserve.php vector is already covered by CVE-2008-6132.
CVE-2009-0821
Mozilla Firefox 2.0.0.20 and earlier allows remote attackers to cause a denial of service (application crash) via nested calls to the window.print function, as demonstrated by a window.print(window.print()) in the onclick attribute of an INPUT element.
CVE-2009-0826
BlogHelper stores common_db.inc under the web root with insufficient access control, which allows remote attackers to download the database file containing user credentials via a direct request.
CVE-2009-0827
PollHelper stores poll.inc under the web root with insufficient access control, which allows remote attackers to download the database file containing user credentials via a direct request.
CVE-2009-0828
QuoteBook stores quotes.inc under the web root with insufficient access control, which allows remote attackers to obtain sensitive database information, including user credentials, via a direct request.
CVE-2009-0829
Multiple SQL injection vulnerabilities in QuoteBook allow remote attackers to execute arbitrary SQL commands via the (1) MyBox and (2) selectFavorites parameters to (a) quotes.php and the (3) QuoteName and (4) QuoteText parameters to (b) quotesadd.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-0830
Cross-site scripting (XSS) vulnerability in QuoteBook allows remote attackers to inject arbitrary web script or HTML via the (1) QuoteName and (2) QuoteText parameters to quotesadd.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-0831
SQL injection vulnerability in members.php in the Members CV (job) module 1.0 for PHP-Fusion, when magic_quotes_gpc is disabled, allows remote authenticated users to execute arbitrary SQL commands via the sortby parameter.
CVE-2009-0832
SQL injection vulnerability in items.php in the E-Cart module 1.3 for PHP-Fusion allows remote attackers to execute arbitrary SQL commands via the CA parameter.
CVE-2009-0833
Heap-based buffer overflow in gen_msn.dll in the gen_msn plugin 0.31 for Winamp 5.541 allows remote attackers to execute arbitrary code via a playlist (.pls) file with a long URL in the File1 field.  NOTE: some of these details are obtained from third party information.
