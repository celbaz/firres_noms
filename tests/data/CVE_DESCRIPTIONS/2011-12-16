CVE-2011-3834
Multiple integer overflows in the in_avi.dll plugin in Winamp before 5.623 allow remote attackers to execute arbitrary code via an AVI file with a crafted value for (1) the number of streams or (2) the size of the RIFF INFO chunk, leading to a heap-based buffer overflow.
CVE-2011-4369
Unspecified vulnerability in the PRC component in Adobe Reader and Acrobat 9.x before 9.4.7 on Windows, Adobe Reader and Acrobat 9.x through 9.4.6 on Mac OS X, Adobe Reader and Acrobat 10.x through 10.1.1 on Windows and Mac OS X, and Adobe Reader 9.x through 9.4.6 on UNIX allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unknown vectors, as exploited in the wild in December 2011.
CVE-2011-4725
Multiple SQL injection vulnerabilities in the Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 allow remote attackers to execute arbitrary SQL commands via crafted input to a PHP script, as demonstrated by login_up.php3 and certain other files.
CVE-2011-4726
Multiple cross-site scripting (XSS) vulnerabilities in the Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by admin/health/ and certain other files.
CVE-2011-4727
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 does not properly validate string data that is intended for storage in an XML document, which allows remote attackers to cause a denial of service (parsing error) or possibly have unspecified other impact via a crafted REST URL parameter, as demonstrated by parameters to admin/ and certain other files.
CVE-2011-4728
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 does not set the secure flag for a cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session, as demonstrated by cookies used by login_up.php3 and certain other files.
CVE-2011-4729
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 does not include the HTTPOnly flag in a Set-Cookie header for a cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie, as demonstrated by cookies used by login_up.php3 and certain other files.
CVE-2011-4730
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 generates a password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation, as demonstrated by forms in admin/reseller/login-info/ and certain other files.
CVE-2011-4731
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 includes an RFC 1918 IP address within a web page, which allows remote attackers to obtain potentially sensitive information by reading this page, as demonstrated by admin/home/admin and certain other files.
CVE-2011-4732
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 omits the Content-Type header's charset parameter for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving account/power-mode-logout and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4733
The Server Administration Panel in Parallels Plesk Panel 10.2.0_build1011110331.18 sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving smb/admin-home/disable-featured-applications-promo and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4734
Multiple SQL injection vulnerabilities in the Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 allow remote attackers to execute arbitrary SQL commands via crafted input to a PHP script, as demonstrated by file-manager/ and certain other files.
CVE-2011-4735
Multiple cross-site scripting (XSS) vulnerabilities in the Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by smb/user/create and certain other files.
CVE-2011-4736
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 receives cleartext password input over HTTP, which allows remote attackers to obtain sensitive information by sniffing the network, as demonstrated by forms in login_up.php3 and certain other files.
CVE-2011-4737
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 includes a submitted password within an HTTP response body, which allows remote attackers to obtain sensitive information by sniffing the network, as demonstrated by password handling in client@2/domain@1/odbc/dsn@1/properties/.
CVE-2011-4738
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 does not include the HTTPOnly flag in a Set-Cookie header for a cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie, as demonstrated by cookies used by get_password.php and certain other files.
CVE-2011-4739
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 generates a password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation, as demonstrated by forms in smb/my-profile and certain other files.
CVE-2011-4740
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 generates web pages containing external links in response to GET requests with query strings for smb/app/search-data/catalogId/marketplace and certain other files, which makes it easier for remote attackers to obtain sensitive information by reading (1) web-server access logs or (2) web-server Referer logs, related to a "cross-domain Referer leakage" issue.
CVE-2011-4741
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 includes a database connection string within a web page, which allows remote attackers to obtain potentially sensitive information by reading this page, as demonstrated by client@2/domain@1/hosting/aspdotnet/.
CVE-2011-4742
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 has web pages containing e-mail addresses that are not intended for correspondence about the local application deployment, which allows remote attackers to obtain potentially sensitive information by reading a page, as demonstrated by smb/user/list and certain other files.
CVE-2011-4743
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 omits the Content-Type header's charset parameter for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving smb/user/create and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4744
The Control Panel in Parallels Plesk Panel 10.2.0 build 20110407.20 sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving smb/admin-home/featured-applications/ and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4745
Multiple cross-site scripting (XSS) vulnerabilities in the billing system for Parallels Plesk Panel 10.3.1_build1013110726.09 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by admin/index.php/default and certain other files.
CVE-2011-4746
The billing system for Parallels Plesk Panel 10.3.1_build1013110726.09 does not disable the SSL 2.0 protocol, which makes it easier for remote attackers to conduct spoofing attacks by leveraging protocol weaknesses.
CVE-2011-4747
The billing system for Parallels Plesk Panel 10.3.1_build1013110726.09 does not prevent the use of weak ciphers for SSL sessions, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via a crafted CipherSuite list.
CVE-2011-4748
The billing system for Parallels Plesk Panel 10.3.1_build1013110726.09 has web pages containing e-mail addresses that are not intended for correspondence about the local application deployment, which allows remote attackers to obtain potentially sensitive information by reading a page, as demonstrated by js/ajax/core/ajax.inc.js and certain other files.
CVE-2011-4749
The billing system for Parallels Plesk Panel 10.3.1_build1013110726.09 generates a password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation, as demonstrated by forms on certain pages under admin/index.php/default.
CVE-2011-4750
Multiple cross-site scripting (XSS) vulnerabilities in SmarterTools SmarterStats 6.2.4100 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by Default.aspx and certain other files.
CVE-2011-4751
SmarterTools SmarterStats 6.2.4100 generates web pages containing external links in response to GET requests with query strings for frmGettingStarted.aspx, which makes it easier for remote attackers to obtain sensitive information by reading (1) web-server access logs or (2) web-server Referer logs, related to a "cross-domain Referer leakage" issue.
CVE-2011-4752
SmarterTools SmarterStats 6.2.4100 sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving frmCustomReport.aspx and certain other files.  NOTE: it is possible that only clients, not the SmarterStats product, could be affected by this issue.
CVE-2011-4753
Multiple SQL injection vulnerabilities in Parallels Plesk Small Business Panel 10.2.0 allow remote attackers to execute arbitrary SQL commands via crafted input to a PHP script, as demonstrated by domains/sitebuilder_edit.php and certain other files.
CVE-2011-4754
Multiple cross-site scripting (XSS) vulnerabilities in Parallels Plesk Small Business Panel 10.2.0 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by smb/app/available/id/apscatalog/ and certain other files.
CVE-2011-4755
Parallels Plesk Small Business Panel 10.2.0 does not properly validate string data that is intended for storage in an XML document, which allows remote attackers to cause a denial of service (parsing error) or possibly have unspecified other impact via a crafted cookie, as demonstrated by cookies to client@1/domain@1/hosting/file-manager/ and certain other files.
CVE-2011-4756
Parallels Plesk Small Business Panel 10.2.0 does not include the HTTPOnly flag in a Set-Cookie header for a cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie, as demonstrated by cookies used by domains/sitebuilder_edit.php and certain other files.
CVE-2011-4757
Parallels Plesk Small Business Panel 10.2.0 generates a password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation, as demonstrated by forms in smb/auth and certain other files.
CVE-2011-4758
Parallels Plesk Small Business Panel 10.2.0 receives cleartext password input over HTTP, which allows remote attackers to obtain sensitive information by sniffing the network, as demonstrated by forms in smb/auth and certain other files.
CVE-2011-4759
Parallels Plesk Small Business Panel 10.2.0 generates web pages containing external links in response to GET requests with query strings for client@1/domain@1/hosting/file-manager/ and certain other files, which makes it easier for remote attackers to obtain sensitive information by reading (1) web-server access logs or (2) web-server Referer logs, related to a "cross-domain Referer leakage" issue.
CVE-2011-4760
Parallels Plesk Small Business Panel 10.2.0 has web pages containing e-mail addresses that are not intended for correspondence about the local application deployment, which allows remote attackers to obtain potentially sensitive information by reading a page, as demonstrated by smb/email-address/list and certain other files.
CVE-2011-4761
Parallels Plesk Small Business Panel 10.2.0 omits the Content-Type header's charset parameter for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving domains/sitebuilder_edit.php and certain other files.  NOTE: it is possible that only clients, not the SmarterStats product, could be affected by this issue.
CVE-2011-4762
Parallels Plesk Small Business Panel 10.2.0 sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving smb/app/top-categories-data/ and certain other files.  NOTE: it is possible that only clients, not the SmarterStats product, could be affected by this issue.
CVE-2011-4763
Multiple SQL injection vulnerabilities in the Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 allow remote attackers to execute arbitrary SQL commands via crafted input to a PHP script, as demonstrated by Wizard/Edit/Html and certain other files.
CVE-2011-4764
Multiple cross-site scripting (XSS) vulnerabilities in the Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by Wizard/Edit/Modules/Image and certain other files.
CVE-2011-4765
The Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 does not include the HTTPOnly flag in a Set-Cookie header for a cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie, as demonstrated by cookies used by Wizard/Edit/Modules/ImageGallery/MultiImagesUpload and certain other files.
CVE-2011-4766
** DISPUTED ** The Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 allows remote attackers to obtain ASP source code via a direct request to wysiwyg/fckconfig.js.  NOTE: CVE disputes this issue because ASP is only used in a JavaScript comment.
CVE-2011-4767
The Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 has web pages containing e-mail addresses that are not intended for correspondence about the local application deployment, which allows remote attackers to obtain potentially sensitive information by reading a page, as demonstrated by js/Wizard/Status.js and certain other files.
CVE-2011-4768
The Site Editor (aka SiteBuilder) feature in Parallels Plesk Small Business Panel 10.2.0 omits the Content-Type header's charset parameter for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving Wizard/Edit/Modules/Image and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4776
Multiple cross-site scripting (XSS) vulnerabilities in the Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 allow remote attackers to inject arbitrary web script or HTML via crafted input to a PHP script, as demonstrated by admin/update/settings/ and certain other files.
CVE-2011-4777
Cross-site scripting (XSS) vulnerability in the Site Editor (aka SiteBuilder) feature in Parallels Plesk Panel 10.4.4_build20111103.18 allows remote attackers to inject arbitrary web script or HTML via the login parameter to preferences.html.
CVE-2011-4847
SQL injection vulnerability in the Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 allows remote attackers to execute arbitrary SQL commands via a certificateslist cookie to notification@/.
CVE-2011-4848
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 includes a submitted password within an HTTP response body, which allows remote attackers to obtain sensitive information by sniffing the network, as demonstrated by password handling in certain files under client@1/domain@1/backup/local-repository/.
CVE-2011-4849
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 does not set the secure flag for a cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session, as demonstrated by cookies used by help.php and certain other files.
CVE-2011-4850
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 does not include the HTTPOnly flag in a Set-Cookie header for a cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie, as demonstrated by cookies used by help.php and certain other files.
CVE-2011-4851
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 generates a password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation, as demonstrated by forms in server/google-tools/ and certain other files.
CVE-2011-4852
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 generates web pages containing external links in response to GET requests with query strings for enterprise/mobile-monitor/ and certain other files, which makes it easier for remote attackers to obtain sensitive information by reading (1) web-server access logs or (2) web-server Referer logs, related to a "cross-domain Referer leakage" issue.
CVE-2011-4853
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 includes an RFC 1918 IP address within a web page, which allows remote attackers to obtain potentially sensitive information by reading this page, as demonstrated by smb/user/list-data/items-per-page/ and certain other files.
CVE-2011-4854
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 does not ensure that Content-Type HTTP headers match the corresponding Content-Type data in HTML META elements, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving the get_enabled_product_icon program.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4855
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 omits the Content-Type header's charset parameter for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving admin/customer-service-plan/list/reset-search/true/ and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4856
The Control Panel in Parallels Plesk Panel 10.4.4_build20111103.18 sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving admin/health/parameters and certain other files.  NOTE: it is possible that only clients, not the Plesk product, could be affected by this issue.
CVE-2011-4857
Heap-based buffer overflow in the in_mod.dll plugin in Winamp before 5.623 allows remote attackers to execute arbitrary code via crafted song message data in an Impulse Tracker (IT) file.  NOTE: some of these details are obtained from third party information.
