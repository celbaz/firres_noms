CVE-2007-5494
Memory leak in the Red Hat Content Accelerator kernel patch in Red Hat Enterprise Linux (RHEL) 4 and 5 allows local users to cause a denial of service (memory consumption) via a large number of open requests involving O_ATOMICLOOKUP.
CVE-2007-5503
Multiple integer overflows in Cairo before 1.4.12 might allow remote attackers to execute arbitrary code, as demonstrated using a crafted PNG image with large width and height values, which is not properly handled by the read_png function.
CVE-2007-6150
The "internal state tracking" code for the random and urandom devices in FreeBSD 5.5, 6.1 through 6.3, and 7.0 beta 4 allows local users to obtain portions of previously-accessed random values, which could be leveraged to bypass protection mechanisms that rely on secrecy of those values.
CVE-2007-6170
SQL injection vulnerability in the Call Detail Record Postgres logging engine (cdr_pgsql) in Asterisk 1.4.x before 1.4.15, 1.2.x before 1.2.25, B.x before B.2.3.4, and C.x before C.1.0-beta6 allows remote authenticated users to execute arbitrary SQL commands via (1) ANI and (2) DNIS arguments.
CVE-2007-6171
SQL injection vulnerability in the Postgres Realtime Engine (res_config_pgsql) in Asterisk 1.4.x before 1.4.15 and C.x before C.1.0-beta6 allows remote attackers to execute arbitrary SQL commands via unknown vectors.
CVE-2007-6172
Multiple SQL injection vulnerabilities in wpQuiz 2.7 allow remote attackers to execute arbitrary SQL commands via the id parameter to (1) viewimage.php and (2) comments.php.
CVE-2007-6173
Cross-site scripting (XSS) vulnerability in c/portal/login in Liferay Enterprise Portal 4.3.1 allows remote attackers to inject arbitrary web script or HTML via the emailAddress parameter in a Send New Password action, a different vector than CVE-2007-6055.  NOTE: some of these details are obtained from third party information.
CVE-2007-6174
PHPDevShell before 0.7.0 allows remote authenticated users to gain privileges via a crafted request to update a user profile.  NOTE: some of these details are obtained from third party information.
CVE-2007-6175
Buffer overflow in Lhaplus 1.55 and earlier allows remote attackers to execute arbitrary code via a crafted LZH archive, a different vector than CVE-2007-5048.
CVE-2007-6176
kb_whois.cgi in K+B-Bestellsystem (aka KB-Bestellsystem) allows remote attackers to execute arbitrary commands via shell metacharacters in the (1) domain or (2) tld parameter in a check_owner action.
CVE-2007-6177
PHP remote file inclusion vulnerability in Exchange/include.php in PHP_CON 1.3 allows remote attackers to execute arbitrary PHP code via a URL in the webappcfg[APPPATH] parameter.
CVE-2007-6178
Multiple PHP remote file inclusion vulnerabilities in Easy Hosting Control Panel for Ubuntu (EHCP) 0.22.8 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the confdir parameter to (1) dbutil.bck.php and (2) dbutil.php in config/.
Additional Information - http://www.securityfocus.com/bid/26623/info
CVE-2007-6179
Multiple PHP remote file inclusion vulnerabilities in Charray's CMS 0.9.3 allow remote attackers to execute arbitrary PHP code via a URL in the ccms_library_path parameter to (1) markdown.php and (2) gallery.php in decoder/.
CVE-2007-6180
Race condition in the Remote Procedure Call kernel module (rpcmod) in Sun Solaris 8 through 10 allows local users to cause a denial of service (NULL dereference and panic) via unspecified vectors.
CVE-2007-6181
Heap-based buffer overflow in cygwin1.dll in Cygwin 1.5.7 and earlier allows context-dependent attackers to execute arbitrary code via a filename with a certain length, as demonstrated by a remote authenticated user who uses the SCP protocol to send a file to the Cygwin machine, and thereby causes scp.exe on this machine to execute, and then overwrite heap memory with characters from the filename. NOTE: it is also reported that a related issue might exist in 1.5.7 through 1.5.19.
CVE-2007-6182
The responder program in ISPsystem ISPmanager (aka ISPmgr) 4.2.15.1 allows local users to gain privileges via shell metacharacters in command line arguments.
CVE-2007-6183
Format string vulnerability in the mdiag_initialize function in gtk/src/rbgtkmessagedialog.c in Ruby-GNOME 2 (aka Ruby/Gnome2) 0.16.0, and SVN versions before 20071127, allows context-dependent attackers to execute arbitrary code via format string specifiers in the message parameter.
CVE-2007-6184
Directory traversal vulnerability in index.php in Project Alumni 1.0.9 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the act parameter.
CVE-2007-6185
Directory traversal vulnerability in users/files.php in Eurologon CMS allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter in a download action, as demonstrated by a certain PHP file containing database credentials.
CVE-2007-6186
Unspecified vulnerability in PHPDevShell before 0.7.0 has unknown impact and attack vectors, involving a "minor security bug in repair & optimize database."
CVE-2007-6187
Multiple directory traversal vulnerabilities in PHP Content Architect (aka NoAh) 0.9 pre 1.2 and earlier allow remote attackers to read arbitrary files via a .. (dot dot) in the filepath parameter to (1) css_file.php, (2) js_file.php, or (3) xml_file.php in noah/modules/nosystem/templates/.
CVE-2007-6188
Multiple directory traversal vulnerabilities in TuMusika Evolution 1.7R5 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the language parameter to (1) languages_n.php, (2) languages_f.php, or (3) languages.php in inc/; and (4) allow remote attackers to read arbitrary local files via a .. (dot dot) in the uri parameter to frames/nogui/sc_download.php.
CVE-2007-6189
A certain ActiveX control in (1) OScan8.ocx and (2) Oscan81.ocx in BitDefender Online Anti-Virus Scanner 8.0 allows remote attackers to execute arbitrary code via a long argument to the InitX method that begins with a "%%" sequence, which is misinterpreted as a Unicode string and decoded twice, leading to improper memory allocation and a heap-based buffer overflow.
CVE-2007-6190
The HTTP daemon in the Cisco Unified IP Phone, when the Extension Mobility feature is enabled, allows remote authenticated users of other phones associated with the same CUCM server to eavesdrop on the physical environment via a CiscoIPPhoneExecute message containing a URL attribute of an ExecuteItem element that specifies a Real-Time Transport Protocol (RTP) audio stream.
CVE-2007-6191
Multiple PHP remote file inclusion vulnerabilities in Armin Burger p.mapper 3.2.0 beta3 allow remote attackers to execute arbitrary PHP code via a URL in the _SESSION[PM_INCPHP] parameter to (1) incphp/globals.php or (2) plugins/export/mc_table.php.  NOTE: it could be argued that this vulnerability is caused by a problem in PHP and the proper fix should be in PHP; if so, then this should not be treated as a vulnerability in p.mapper.
CVE-2007-6192
The web management interface in Citrix NetScaler 8.0 build 47.8 uses weak encryption (XOR of unpadded data) to store credentials within a cookie, which makes it easier for remote attackers to obtain cleartext credentials when a cookie is captured via a known-plaintext attack.
CVE-2007-6193
The web management interface in Citrix NetScaler 8.0 build 47.8 stores the device's primary IP address in a cookie, which might allow remote attackers to obtain sensitive network configuration information if this address is not the same as the address being used by the web interface.
