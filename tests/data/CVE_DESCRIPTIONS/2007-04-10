CVE-2007-0734
fsck, as used by the AirPort Disk feature of the AirPort Extreme Base Station with 802.11n before Firmware Update 7.1, and by Apple Mac OS X 10.3.9 through 10.4.9, does not properly enforce password protection of a USB hard drive, which allows context-dependent attackers to list arbitrary directories or execute arbitrary code, resulting from memory corruption.
CVE-2007-0938
Microsoft Content Management Server (MCMS) 2001 SP1 and 2002 SP2 does not properly handle certain characters in a crafted HTTP GET request, which allows remote attackers to execute arbitrary code, aka the "CMS Memory Corruption Vulnerability."
CVE-2007-0939
Cross-site scripting (XSS) vulnerability in Microsoft Content Management Server (MCMS) 2001 SP1 and 2002 SP2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors involving HTML redirection queries, aka "Cross-site Scripting and Spoofing Vulnerability."
CVE-2007-1204
Stack-based buffer overflow in the Universal Plug and Play (UPnP) service in Microsoft Windows XP SP2 allows remote attackers on the same subnet to execute arbitrary code via crafted HTTP headers in request or notification messages, which trigger memory corruption.
CVE-2007-1205
Unspecified vulnerability in Microsoft Agent (msagent\agentsvr.exe) in Windows 2000 SP4, XP SP2, and Server 2003, 2003 SP1, and 2003 SP2 allows remote attackers to execute arbitrary code via crafted URLs, which result in memory corruption.
CVE-2007-1206
The Virtual DOS Machine (VDM) in the Windows Kernel in Microsoft Windows NT 4.0; 2000 SP4; XP SP2; Server 2003, 2003 SP1, and 2003 SP2; and Windows Vista before June 2006; uses insecure permissions (PAGE_READWRITE) for a physical memory view, which allows local users to gain privileges by modifying the "zero page" during a race condition before the view is unmapped.
CVE-2007-1209
Use-after-free vulnerability in the Client/Server Run-time Subsystem (CSRSS) in Microsoft Windows Vista does not properly handle connection resources when starting and stopping processes, which allows local users to gain privileges by opening and closing multiple ApiPort connections, which leaves a "dangling pointer" to a process data structure.
CVE-2007-1687
Multiple buffer overflows in the Internet Pictures Corporation iPIX Image Well ActiveX control (iPIX-ImageWell-ipix.dll) allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-1841
The isakmp_info_recv function in src/racoon/isakmp_inf.c in racoon in Ipsec-tools before 0.6.7 allows remote attackers to cause a denial of service (tunnel crash) via crafted (1) DELETE (ISAKMP_NPTYPE_D) and (2) NOTIFY (ISAKMP_NPTYPE_N) messages.
CVE-2007-1900
CRLF injection vulnerability in the FILTER_VALIDATE_EMAIL filter in ext/filter in PHP 5.2.0 and 5.2.1 allows context-dependent attackers to inject arbitrary e-mail headers via an e-mail address with a '\n' character, which causes a regular expression to ignore the subsequent part of the address string.
CVE-2007-1904
Directory traversal vulnerability in AOL Instant Messenger (AIM) 5.9 and earlier, and ICQ 5.1 and probably earlier, allows user-assisted remote attackers to write files to arbitrary locations via a .. (dot dot) in a filename in a file transfer operation.
CVE-2007-1905
Cross-site scripting (XSS) vulnerability in auth.php in Pineapple Technologies QuizShock 1.6.1 and earlier allows remote attackers to inject arbitrary web script or HTML via encoded special characters in the forward_to parameter, as demonstrated using "&lt;&quot;&lt;".
CVE-2007-1906
Directory traversal vulnerability in richedit/keyboard.php in eCardMAX HotEditor (Hot Editor) 4.0, and the HotEditor plugin for MyBB, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the first parameter.
CVE-2007-1907
PHP remote file inclusion vulnerability in warn.php in Pathos Content Management System (CMS) 0.92-2 allows remote attackers to execute arbitrary PHP code via a URL in the file parameter.
CVE-2007-1908
PHP file inclusion vulnerability in php121db.php in PHP121 Instant Messenger 2.2 allows remote attackers to execute arbitrary PHP code via a UNC share pathname or a local file pathname in the php121dir parameter, which is accessed by the file_exists function.
CVE-2007-1909
SQL injection vulnerability in login.php in Ryan Haudenschilt Battle.net Clan Script for PHP 1.5.1 and earlier allows remote attackers to execute arbitrary SQL commands via the (1) user or (2) pass parameter.
CVE-2007-1910
Buffer overflow in wwlib.dll in Microsoft Word 2007 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted document, as demonstrated by file789-1.doc.
CVE-2007-1911
Multiple unspecified vulnerabilities in Microsoft Word 2007 allow remote attackers to cause a denial of service (CPU consumption) via crafted documents, as demonstrated by (1) file798-1.doc and (2) file613-1.doc, possibly related to a buffer overflow.
CVE-2007-1912
Heap-based buffer overflow in Microsoft Windows allows user-assisted remote attackers to have an unknown impact via a crafted .HLP file.
CVE-2007-1913
The TRUSTED_SYSTEM_SECURITY function in the SAP RFC Library 6.40 and 7.00 before 20061211 allows remote attackers to verify the existence of users and groups on systems and domains via unspecified vectors, a different vulnerability than CVE-2006-6010.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1914
The RFC_START_PROGRAM function in the SAP RFC Library 6.40 and 7.00 before 20061211 allows remote attackers to obtain sensitive information (external RFC server configuration data) via unspecified vectors, a different vulnerability than CVE-2006-6010.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1915
Buffer overflow in the RFC_START_PROGRAM function in the SAP RFC Library 6.40 and 7.00 before 20061211 allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1916
Buffer overflow in the RFC_START_GUI function in the SAP RFC Library 6.40 and 7.00 before 20061211 allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1917
Buffer overflow in the SYSTEM_CREATE_INSTANCE function in the SAP RFC Library 6.40 and 7.00 before 20061211 allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1918
The RFC_SET_REG_SERVER_PROPERTY function in the SAP RFC Library 6.40 and 7.00 before 20070109 implements an option for exclusive access to an RFC server, which allows remote attackers to cause a denial of service (client lockout) via unspecified vectors.  NOTE: This information is based upon a vague initial disclosure. Details will be updated after the grace period has ended.
CVE-2007-1919
Cross-site scripting (XSS) vulnerability in index.php in Arizona Dream Livre d'or (livor) 2.5 allows remote attackers to inject arbitrary web script or HTML via the page parameter.
CVE-2007-1920
SQL injection vulnerability in index.php in the aktualnosci module in SmodBIP 1.06 and earlier allows remote attackers to execute arbitrary SQL commands via the zoom parameter, possibly related to home.php.
CVE-2007-1921
LIBSNDFILE.DLL, as used by AOL Nullsoft Winamp 5.33 and possibly other products, allows remote attackers to execute arbitrary code via a crafted .MAT file that contains a value that is used as an offset, which triggers memory corruption.
To exploit this issue, an attacker must entice an unsuspecting user to use the affected application to open a specially crafted file.
CVE-2007-1922
The Impulse Tracker (IT) and ScreamTracker 3 (S3M) modules in IN_MOD.DLL in AOL Nullsoft Winamp 5.33 allows remote attackers to execute arbitrary code via a crafted (1) .IT or (2) .S3M file containing integer values that are used as memory offsets, which triggers memory corruption.
CVE-2007-1923
(1) LedgerSMB and (2) DWS Systems SQL-Ledger implement access control lists by changing the set of URLs linked from menus, which allows remote attackers to access restricted functionality via direct requests.
CVE-2007-1924
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in phpContact allow remote attackers to execute arbitrary PHP code via a URL in the include_path parameter to (1) contact_business.php or (2) contact_person.php.  NOTE: this issue is disputed by CVE and a reliable third party, because include_path is initialized to a fixed value before use.
CVE-2007-1925
The borrado function in modules/Your_Account/index.php in Tru-Zone Nuke ET 3.4 before fix 7 does not verify that account deletion requests come from the account owner, which allows remote authenticated users to delete arbitrary accounts via a modified cookie.
CVE-2007-1926
Cross-site scripting (XSS) vulnerability in JBMC Software DirectAdmin before 1.293 does not properly display log files, which allows remote authenticated users to inject arbitrary web script or HTML via (1) http or (2) ftp requests logged in /var/log/directadmin/security.log; (3) allows context-dependent attackers to inject arbitrary web script or HTML into /var/log/messages via a PHP script that invokes /usr/bin/logger; (4) allows local users to inject arbitrary web script or HTML into /var/log/messages by invoking /usr/bin/logger at the command line; and allows remote attackers to inject arbitrary web script or HTML via remote requests logged in the (5) /var/log/exim/rejectlog, (6) /var/log/exim/mainlog, (7) /var/log/proftpd/auth.log, (8) /var/log/httpd/error_log, (9) /var/log/httpd/access_log, (10) /var/log/directadmin/error.log, and (11) /var/log/directadmin/security.log files.
CVE-2007-1927
Cross-site scripting (XSS) vulnerability in signup.asp in CmailServer WebMail 5.3.4 and earlier allows remote attackers to inject arbitrary web script or HTML via the POP3Mail parameter.
CVE-2007-1928
Directory traversal vulnerability in index.php in witshare 0.9 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the menu parameter.
CVE-2007-1929
Directory traversal vulnerability in downloadpic.php in Beryo 2.0, and possibly other versions including 2.4, allows remote attackers to read arbitrary files via a .. (dot dot) in the chemin parameter.
CVE-2007-1930
Directory traversal vulnerability in download2.php in cattaDoc 2.21, and possibly other versions including 3.0, allows remote attackers to read arbitrary files via a .. (dot dot) in the fn1 parameter.
CVE-2007-1931
SQL injection vulnerability in index.php in the slownik module in SmodCMS 2.10 and earlier allows remote attackers to execute arbitrary SQL commands via the ssid parameter.
CVE-2007-1932
Directory traversal vulnerability in scarnews.inc.php in ScarNews 1.2.1 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the sn_admin_dir parameter.
CVE-2007-1933
Multiple directory traversal vulnerabilities in PcP-Guestbook (PcP-Book) 3.0 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the lang parameter to (1) index.php, (2) gb.php, or (3) faq.php.
CVE-2007-1934
Directory traversal vulnerability in member.php in the eBoard 1.0.7 module for PHP-Nuke allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the GLOBALS[name] parameter.
CVE-2007-1935
PHP file inclusion vulnerability in admin/index.php in ScarAdControl (ScarAdController) 1.1 allows remote attackers to execute arbitrary PHP code via a UNC share pathname or a local file pathname in the site parameter, which is accessed by the file_exists function.
CVE-2007-1936
PHP remote file inclusion vulnerability in scaradcontrol.php in ScarAdControl (ScarAdController) 1.1 allows remote attackers to execute arbitrary PHP code via a URL in the sac_config_dir parameter.
CVE-2007-1937
PHP remote file inclusion vulnerability in smilies.php in Scorp Book 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the config parameter.
CVE-2007-1938
Ichitaro 2005 through 2007, and possibly related products, allows remote attackers to have an unknown impact via unspecified vectors in a document distributed through e-mail or a web site, possibly due to a buffer overflow or cross-site scripting (XSS).
CVE-2007-1939
Cross-site scripting (XSS) vulnerability in the embedded webserver in Daniel Naber LanguageTool before 0.8.9 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors involving an error message, possibly the demultiplex method in HTTPServer.java.
