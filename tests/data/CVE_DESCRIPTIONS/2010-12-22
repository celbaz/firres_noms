CVE-2009-2189
The ICMPv6 implementation on the Apple Time Capsule, AirPort Extreme Base Station, and AirPort Express Base Station with firmware before 7.5.2 does not limit the rate of (1) Router Advertisement and (2) Neighbor Discovery packets, which allows remote attackers to cause a denial of service (resource consumption and device restart) by sending many packets.
CVE-2010-0039
The Application-Level Gateway (ALG) on the Apple Time Capsule, AirPort Extreme Base Station, and AirPort Express Base Station with firmware before 7.5.2 modifies PORT commands in incoming FTP traffic, which allows remote attackers to use the device's IP address for arbitrary intranet TCP traffic by leveraging write access to an intranet FTP server.
CVE-2010-0114
fw_charts.php in the reporting module in the Manager (aka SEPM) component in Symantec Endpoint Protection (SEP) 11.x before 11 RU6 MP2 allows remote attackers to bypass intended restrictions on report generation, overwrite arbitrary PHP scripts, and execute arbitrary code via a crafted request.
CVE-2010-1676
Heap-based buffer overflow in Tor before 0.2.1.28 and 0.2.2.x before 0.2.2.20-alpha allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via unspecified vectors.
CVE-2010-1804
Unspecified vulnerability in the network bridge functionality on the Apple Time Capsule, AirPort Extreme Base Station, and AirPort Express Base Station with firmware before 7.5.2 allows remote attackers to cause a denial of service (networking outage) via a crafted DHCP reply.
CVE-2010-2590
Heap-based buffer overflow in the CrystalReports12.CrystalPrintControl.1 ActiveX control in PrintControl.dll 12.3.2.753 in SAP Crystal Reports 2008 SP3 Fix Pack 3.2 allows remote attackers to execute arbitrary code via a long ServerResourceVersion property value.
CVE-2010-2644
IBM WebSphere Service Registry and Repository (WSRR) 7.0.0 before FP1 does not properly implement access control, which allows remote attackers to perform governance actions via unspecified API requests to an EJB interface.
CVE-2010-3268
The GetStringAMSHandler function in prgxhndl.dll in hndlrsvc.exe in the Intel Alert Handler service (aka Symantec Intel Handler service) in Intel Alert Management System (AMS), as used in Symantec Antivirus Corporate Edition 10.1.4.4010 on Windows 2000 SP4 and Symantec Endpoint Protection before 11.x, does not properly validate the CommandLine field of an AMS request, which allows remote attackers to cause a denial of service (application crash) via a crafted request.
CVE-2010-3905
The password reset feature in the administrator interface for Eucalyptus 2.0.0 and 2.0.1 does not perform authentication, which allows remote attackers to gain privileges by sending password reset requests for other users.
CVE-2010-3970
Stack-based buffer overflow in the CreateSizedDIBSECTION function in shimgvw.dll in the Windows Shell graphics processor (aka graphics rendering engine) in Microsoft Windows XP SP2 and SP3, Server 2003 SP2, Vista SP1 and SP2, and Server 2008 Gold and SP2 allows remote attackers to execute arbitrary code via a crafted .MIC or unspecified Office document containing a thumbnail bitmap with a negative biClrUsed value, as reported by Moti and Xu Hao, aka "Windows Shell Graphics Processing Overrun Vulnerability."
CVE-2010-3971
Use-after-free vulnerability in the CSharedStyleSheet::Notify function in the Cascading Style Sheets (CSS) parser in mshtml.dll, as used in Microsoft Internet Explorer 6 through 8 and other products, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a self-referential @import rule in a stylesheet, aka "CSS Memory Corruption Vulnerability."
CVE-2010-4110
Unspecified vulnerability in HP OpenVMS 8.3, 8.3-1H1, and 8.4 on the Itanium platform on Integrity servers allows local users to gain privileges or cause a denial of service via unknown vectors.
CVE-2010-4111
Cross-site scripting (XSS) vulnerability in HP Insight Diagnostics Online Edition before 8.5.1.3712 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-4112
HP Insight Management Agents before 8.6 allows remote attackers to obtain sensitive information via an unspecified request that triggers disclosure of the full path.
CVE-2010-4113
Stack-based buffer overflow in HP Power Manager (HPPM) before 4.3.2 allows remote attackers to execute arbitrary code via a long Login variable to the management web server.
CVE-2010-4114
Cross-site scripting (XSS) vulnerability in HP Discovery & Dependency Mapping Inventory (DDMI) 2.5x, 7.5x, and 7.6x allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-4116
Unspecified vulnerability in HP StorageWorks Storage Mirroring 5.x before 5.2.2.1771.2 allows remote attackers to execute arbitrary code via unknown vectors.
CVE-2010-4275
Multiple cross-site scripting (XSS) vulnerabilities in Radius Manager 3.8.0 allow remote authenticated administrators to inject arbitrary web script or HTML via the (1) name or (2) descr parameter in an (a) update_usergroup or a (b) store_nas action to admin.php.
CVE-2010-4277
Cross-site scripting (XSS) vulnerability in lembedded-video.php in the Embedded Video plugin 4.1 for WordPress allows remote attackers to inject arbitrary web script or HTML via the content parameter to wp-admin/post.php.
CVE-2010-4332
Pointter PHP Content Management System 1.0 allows remote attackers to bypass authentication and obtain administrative privileges via arbitrary values of the auser and apass cookies.
CVE-2010-4333
Pointter PHP Micro-Blogging Social Network 1.8 allows remote attackers to bypass authentication and obtain administrative privileges via arbitrary values of the auser and apass cookies.
CVE-2010-4346
The install_special_mapping function in mm/mmap.c in the Linux kernel before 2.6.37-rc6 does not make an expected security_file_mmap function call, which allows local users to bypass intended mmap_min_addr restrictions and possibly conduct NULL pointer dereference attacks via a crafted assembly-language application.
CVE-2010-4347
The ACPI subsystem in the Linux kernel before 2.6.36.2 uses 0222 permissions for the debugfs custom_method file, which allows local users to gain privileges by placing a custom ACPI method in the ACPI interpreter tables, related to the acpi_debugfs_init function in drivers/acpi/debugfs.c.
CVE-2010-4573
The Update Installer in VMware ESXi 4.1, when a modified sfcb.cfg is present, does not properly configure the SFCB authentication mode, which allows remote attackers to obtain access via an arbitrary username and password.
CVE-2010-4574
The Pickle::Pickle function in base/pickle.cc in Google Chrome before 8.0.552.224 and Chrome OS before 8.0.552.343 on 64-bit Linux platforms does not properly perform pointer arithmetic, which allows remote attackers to bypass message deserialization validation, and cause a denial of service or possibly have unspecified other impact, via invalid pickle data.
CVE-2010-4575
The ThemeInstalledInfoBarDelegate::Observe function in browser/extensions/theme_installed_infobar_delegate.cc in Google Chrome before 8.0.552.224 and Chrome OS before 8.0.552.343 does not properly handle incorrect tab interaction by an extension, which allows user-assisted remote attackers to cause a denial of service (application crash) via a crafted extension.
CVE-2010-4576
browser/worker_host/message_port_dispatcher.cc in Google Chrome before 8.0.552.224 and Chrome OS before 8.0.552.343 does not properly handle certain postMessage calls, which allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via crafted JavaScript code that creates a web worker.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2010-4577
The CSSParser::parseFontFaceSrc function in WebCore/css/CSSParser.cpp in WebKit, as used in Google Chrome before 8.0.552.224, Chrome OS before 8.0.552.343, webkitgtk before 1.2.6, and other products does not properly parse Cascading Style Sheets (CSS) token sequences, which allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted local font, related to "Type Confusion."
CVE-2010-4578
Google Chrome before 8.0.552.224 and Chrome OS before 8.0.552.343 do not properly perform cursor handling, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to "stale pointers."
CVE-2010-4579
Opera before 11.00 does not properly constrain dialogs to appear on top of rendered documents, which makes it easier for remote attackers to trick users into interacting with a crafted web site that spoofs the (1) security information dialog or (2) download dialog.
CVE-2010-4580
Opera before 11.00 does not clear WAP WML form fields after manual navigation to a new web site, which allows remote attackers to obtain sensitive information via an input field that has the same name as an input field on a previously visited web site.
CVE-2010-4581
Unspecified vulnerability in Opera before 11.00 has unknown impact and attack vectors, related to "a high severity issue."
CVE-2010-4582
Opera before 11.00 does not properly handle security policies during updates to extensions, which might allow remote attackers to bypass intended access restrictions via unspecified vectors.
CVE-2010-4583
Opera before 11.00, when Opera Turbo is enabled, does not display a page's security indication, which makes it easier for remote attackers to spoof trusted content via a crafted web site.
CVE-2010-4584
Opera before 11.00, when Opera Turbo is used, does not properly present information about problematic X.509 certificates on https web sites, which might make it easier for remote attackers to spoof trusted content via a crafted web site.
CVE-2010-4585
Unspecified vulnerability in the auto-update functionality in Opera before 11.00 allows remote attackers to cause a denial of service (application crash) by triggering an Opera Unite update.
CVE-2010-4586
The default configuration of Opera before 11.00 enables WebSockets functionality, which has unspecified impact and remote attack vectors, possibly a related issue to CVE-2010-4508.
CVE-2010-4587
Opera before 11.00 on Windows does not properly implement the Insecure Third Party Module warning message, which might make it easier for user-assisted remote attackers to have an unspecified impact via a crafted module.
CVE-2010-4589
Cross-site scripting (XSS) vulnerability in IBM ENOVIA 6 allows remote attackers to inject arbitrary web script or HTML via vectors related to the emxFramework.FilterParameterPattern property.
CVE-2010-4590
Cross-site scripting (XSS) vulnerability in HTTP Access Services (HTTP-AS) in the Connection Manager in IBM Lotus Mobile Connect (LMC) before 6.1.4 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-4591
The Connection Manager in IBM Lotus Mobile Connect (LMC) before 6.1.4, when HTTP Access Services (HTTP-AS) is enabled, does not delete LTPA tokens in response to use of the iNotes Logoff button, which might allow physically proximate attackers to obtain access via an unattended client, related to a cookie domain mismatch.
CVE-2010-4592
The Mobile Network Connections functionality in the Connection Manager in IBM Lotus Mobile Connect before 6.1.4, when HTTP Access Services (HTTP-AS) is enabled, does not properly handle failed attempts at establishing HTTP-TCP sessions, which allows remote attackers to cause a denial of service (memory consumption and daemon crash) by making many TCP connection attempts.
CVE-2010-4593
The Connection Manager in IBM Lotus Mobile Connect before 6.1.4 does not properly maintain a certain reference count, which allows remote authenticated users to cause a denial of service (IP address exhaustion) by making invalid attempts to establish sessions with the same VPN ID from multiple devices.
CVE-2010-4594
The Connection Manager in IBM Lotus Mobile Connect before 6.1.4, when HTTP Access Services (HTTP-AS) is enabled, does not properly process TCP connection requests, which allows remote attackers to cause a denial of service (memory consumption and HTTP-AS hang) by making many connection requests that trigger "queue size delta errors," related to a "timing hole" issue.
CVE-2010-4595
The Connection Manager in IBM Lotus Mobile Connect before 6.1.4 disables the http.device.stanza blacklisting functionality for HTTP Access Services (HTTP-AS), which allows remote attackers to bypass intended access restrictions via an HTTP request that contains a disallowed User-Agent header.
