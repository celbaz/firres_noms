CVE-2014-7198
OMERO before 5.0.6 has multiple CSRF vulnerabilities because the framework for OMERO's web interface lacks CSRF protection.
CVE-2017-16774
Cross-site scripting (XSS) vulnerability in SYNO.Core.PersonalNotification.Event in Synology DiskStation Manager (DSM) before 6.1.4-15217-3 allows remote authenticated users to inject arbitrary web script or HTML via the package parameter.
CVE-2017-16775
Improper restriction of rendered UI layers or frames vulnerability in SSOOauth.cgi in Synology SSO Server before 2.1.3-0129 allows remote attackers to conduct clickjacking attacks via unspecified vectors.
CVE-2017-8023
EMC NetWorker may potentially be vulnerable to an unauthenticated remote code execution vulnerability in the Networker Client execution service (nsrexecd) when oldauth authentication method is used. An unauthenticated remote attacker could send arbitrary commands via RPC service to be executed on the host system with the privileges of the nsrexecd service, which runs with administrative privileges.
CVE-2018-13283
Lack of administrator control over security vulnerability in client.cgi in Synology SSL VPN Client before 1.2.5-0226 allows remote attackers to conduct man-in-the-middle attacks via the (1) command, (2) hostname, or (3) port parameter.
CVE-2018-13284
Command injection vulnerability in ftpd in Synology Diskstation Manager (DSM) before 6.2-23739-1 allows remote authenticated users to execute arbitrary OS commands via the (1) MKD or (2) RMD command.
CVE-2018-13285
Command injection vulnerability in ftpd in Synology Router Manager (SRM) before 1.1.7-6941-1 allows remote authenticated users to execute arbitrary OS commands via the (1) MKD or (2) RMD command.
CVE-2018-13286
Incorrect default permissions vulnerability in synouser.conf in Synology Diskstation Manager (DSM) before 6.2-23739-1 allows remote authenticated users to obtain sensitive information via the world readable configuration.
CVE-2018-13287
Incorrect default permissions vulnerability in synouser.conf in Synology Router Manager (SRM) before 1.1.7-6941-1 allows remote authenticated users to obtain sensitive information via the world readable configuration.
CVE-2018-13288
Information exposure vulnerability in SYNO.FolderSharing.List in Synology File Station before 1.2.3-0252 and before 1.1.5-0125 allows remote attackers to obtain sensitive information via the (1) folder_path or (2) real_path parameter.
CVE-2018-13289
Information exposure vulnerability in SYNO.FolderSharing.List in Synology Router Manager (SRM) before 1.1.7-6941-2 allows remote attackers to obtain sensitive information via the (1) folder_path or (2) real_path parameter.
CVE-2018-13290
Information exposure vulnerability in SYNO.Core.ACL in Synology Router Manager (SRM) before 1.1.7-6941-2 allows remote authenticated users to determine the existence of files or obtain sensitive information of files via the file_path parameter.
CVE-2018-13291
Information exposure vulnerability in /usr/syno/etc/mount.conf in Synology DiskStation Manager (DSM) before 6.2.1-23824 allows remote authenticated users to obtain sensitive information via the world readable configuration.
CVE-2018-13292
Information exposure vulnerability in /usr/syno/etc/mount.conf in Synology Router Manager (SRM) before 1.1.7-6941-2 allows remote authenticated users to obtain sensitive information via the world readable configuration.
CVE-2018-13293
Cross-site scripting (XSS) vulnerability in Control Panel SSO Settings in Synology DiskStation Manager (DSM) before 6.2.1-23824 allows remote authenticated users to inject arbitrary web script or HTML via the URL parameter.
CVE-2018-13294
Information exposure vulnerability in SYNO.Personal.Profile in Synology Application Service before 1.5.4-0320 allows remote authenticated users to obtain sensitive system information via the uid parameter.
CVE-2018-13295
Information exposure vulnerability in SYNO.Personal.Application.Info in Synology Application Service before 1.5.4-0320 allows remote authenticated users to obtain sensitive system information via the version parameter.
CVE-2018-13296
Uncontrolled resource consumption vulnerability in TLS configuration in Synology MailPlus Server before 2.0.5-0606 allows remote attackers to conduct denial-of-service attacks via client-initiated renegotiation.
CVE-2018-13297
Information exposure vulnerability in SYNO.SynologyDrive.Files in Synology Drive before 1.1.2-10562 allows remote attackers to obtain sensitive system information via the dsm_path parameter.
CVE-2018-13298
Channel accessible by non-endpoint vulnerability in privacy page in Synology Android Moments before 1.2.3-199 allows man-in-the-middle attackers to execute arbitrary code via unspecified vectors.
CVE-2018-13299
Relative path traversal vulnerability in Attachment Uploader in Synology Calendar before 2.2.2-0532 allows remote authenticated users to upload arbitrary files via the filename parameter.
CVE-2018-17563
A Malformed Input String to /cgi-bin/api-get_line_status on Grandstream GXP16xx VoIP 1.0.4.128 phones allows attackers to dump the device's configuration in cleartext.
CVE-2018-17564
A Malformed Input String to /cgi-bin/delete_CA on Grandstream GXP16xx VoIP 1.0.4.128 phones allows attackers to delete configuration parameters and gain admin access to the device.
CVE-2018-17565
Shell Metacharacter Injection in the SSH configuration interface on Grandstream GXP16xx VoIP 1.0.4.128 phones allows attackers to execute arbitrary system commands and gain a root shell.
CVE-2018-17989
A stored XSS vulnerability exists in the web interface on D-Link DSL-3782 devices with firmware 1.01 that allows authenticated attackers to inject a JavaScript or HTML payload inside the ACL page. The injected payload would be executed in a user's browser when "/cgi-bin/New_GUI/Acl.asp" is requested.
CVE-2018-17990
An issue was discovered on D-Link DSL-3782 devices with firmware 1.01. An OS command injection vulnerability in Acl.asp allows a remote authenticated attacker to execute arbitrary OS commands via the ScrIPaddrEndTXT parameter.
CVE-2018-19113
The Pronestor PNHM (aka Health Monitoring or HealthMonitor) add-in before 8.1.13.0 for Outlook has "BUILTIN\Users:(I)(F)" permissions for the "%PROGRAMFILES(X86)%\proNestor\Outlook add-in for Pronestor\PronestorHealthMonitor.exe" file, which allows local users to gain privileges via a Trojan horse PronestorHealthMonitor.exe file.
CVE-2018-3979
A remote denial-of-service vulnerability exists in the way the Nouveau Display Driver (the default Ubuntu Nvidia display driver) handles GPU shader execution. A specially crafted pixel shader can cause remote denial-of-service issues. An attacker can provide a specially crafted website to trigger this vulnerability. This vulnerability can be triggered remotely after the user visits a malformed website. No further user interaction is required. Vulnerable versions include Ubuntu 18.04 LTS (linux 4.15.0-29-generic x86_64), Nouveau Display Driver NV117 (vermagic: 4.15.0-29-generic SMP mod_unload).
CVE-2018-4050
An exploitable local privilege escalation vulnerability exists in the privileged helper tool of GOG Galaxy's Games, version 1.2.47 for macOS. An attacker can globally adjust folder permissions leading to execution of arbitrary code with elevated privileges.
CVE-2018-5757
An issue was discovered on AudioCodes 450HD IP Phone devices with firmware 3.0.0.535.106. The traceroute and ping functionality, which uses a parameter in a request to command.cgi from the Monitoring page in the web UI, unsafely puts user-alterable data directly into an OS command, leading to Remote Code Execution via shell metacharacters in the query string.
CVE-2018-8913
Missing custom error page vulnerability in Synology Web Station before 2.1.3-0139 allows remote attackers to conduct phishing attacks via a crafted URL.
