CVE-2007-0008
Integer underflow in the SSLv2 support in Mozilla Network Security Services (NSS) before 3.11.5, as used by Firefox before 1.5.0.10 and 2.x before 2.0.0.2, SeaMonkey before 1.0.8, Thunderbird before 1.5.0.10, and certain Sun Java System server products before 20070611, allows remote attackers to execute arbitrary code via a crafted SSLv2 server message containing a public key that is too short to encrypt the "Master Secret", which results in a heap-based overflow.
CVE-2007-0009
Stack-based buffer overflow in the SSLv2 support in Mozilla Network Security Services (NSS) before 3.11.5, as used by Firefox before 1.5.0.10 and 2.x before 2.0.0.2, Thunderbird before 1.5.0.10, SeaMonkey before 1.0.8, and certain Sun Java System server products before 20070611, allows remote attackers to execute arbitrary code via invalid "Client Master Key" length values.
CVE-2007-0775
Multiple unspecified vulnerabilities in the layout engine in Mozilla Firefox before 1.5.0.10 and 2.x before 2.0.0.2, Thunderbird before 1.5.0.10, and SeaMonkey before 1.0.8 allow remote attackers to cause a denial of service (crash) and potentially execute arbitrary code via certain vectors.
CVE-2007-0776
Heap-based buffer overflow in the _cairo_pen_init function in Mozilla Firefox 2.x before 2.0.0.2, Thunderbird before 1.5.0.10, and SeaMonkey before 1.0.8 allows remote attackers to execute arbitrary code via a large stroke-width attribute in the clipPath element in an SVG file.
CVE-2007-0777
The JavaScript engine in Mozilla Firefox before 1.5.0.10 and 2.x before 2.0.0.2, Thunderbird before 1.5.0.10, and SeaMonkey before 1.0.8 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via certain vectors that trigger memory corruption.
Successful exploitation in Thunderbird requires that JavaScript be enabled in mail which is not the default setting.
CVE-2007-0778
The page cache feature in Mozilla Firefox before 1.5.0.10 and 2.x before 2.0.0.2, and SeaMonkey before 1.0.8 can generate hash collisions that cause page data to be appended to the wrong page cache, which allows remote attackers to obtain sensitive information or enable further attack vectors when the target page is reloaded from the cache.
CVE-2007-0779
GUI overlay vulnerability in Mozilla Firefox 1.5.x before 1.5.0.10 and 2.x before 2.0.0.2, and SeaMonkey before 1.0.8 allows remote attackers to spoof certain user interface elements, such as the host name or security indicators, via the CSS3 hotspot property with a large, transparent, custom cursor.
CVE-2007-0780
browser.js in Mozilla Firefox 1.5.x before 1.5.0.10 and 2.x before 2.0.0.2, and SeaMonkey before 1.0.8 uses the requesting URI to identify child windows, which allows remote attackers to conduct cross-site scripting (XSS) attacks by opening a blocked popup originating from a javascript: URI in combination with multiple frames having the same data: URI.
CVE-2007-0995
Mozilla Firefox before 1.5.0.10 and 2.x before 2.0.0.2, and SeaMonkey before 1.0.8 ignores trailing invalid HTML characters in attribute names, which allows remote attackers to bypass content filters that use regular expressions.
CVE-2007-1090
Microsoft Windows Explorer on Windows XP and 2003 allows remote user-assisted attackers to cause a denial of service (crash) via a malformed WMF file, which triggers the crash when the user browses the folder.
CVE-2007-1091
Microsoft Internet Explorer 7 allows remote attackers to prevent users from leaving a site, spoof the address bar, and conduct phishing and other attacks via onUnload Javascript handlers.
CVE-2007-1092
Mozilla Firefox 1.5.0.9 and 2.0.0.1, and SeaMonkey before 1.0.8 allow remote attackers to execute arbitrary code via JavaScript onUnload handlers that modify the structure of a document, wich triggers memory corruption due to the lack of a finalize hook on DOM window objects.
CVE-2007-1093
Multiple unspecified vulnerabilities in JP1/Cm2/Network Node Manager (NNM) before 07-10-05, and before 08-00-02 in the 08-x series, allow remote attackers to execute arbitrary code, cause a denial of service, or trigger invalid Web utility behavior.
CVE-2007-1094
Microsoft Internet Explorer 7 allows remote attackers to cause a denial of service (NULL dereference and application crash) via JavaScript onUnload handlers that modify the structure of a document.
CVE-2007-1095
Mozilla Firefox before 2.0.0.8 and SeaMonkey before 1.1.5 do not properly implement JavaScript onUnload handlers, which allows remote attackers to run certain JavaScript code and access the location DOM hierarchy in the context of the next web site that is visited by a client.
CVE-2007-1096
Cross-site scripting (XSS) vulnerability in ps_cart.php in VirtueMart before 20070116 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this issue might overlap CVE-2007-0376.
CVE-2007-1097
Unrestricted file upload vulnerability in the onAttachFiles function in the upload tool (inc/lib/attachment.lib.php) in Wiclear before 0.11.1 allows remote attackers to upload and execute arbitrary PHP code via unspecified vectors related to filename validation.  NOTE: some details were obtained from third party information.
CVE-2007-1098
Multiple unspecified vulnerabilities in ScryMUD before 2.1.11 have unknown impact and attack vectors, possibly related to denial of service caused by a search that begins with a .* sequence.
CVE-2007-1099
dbclient in Dropbear SSH client before 0.49 does not sufficiently warn the user when it detects a hostkey mismatch, which might allow remote attackers to conduct man-in-the-middle attacks.
CVE-2007-1100
Directory traversal vulnerability in download.php in Ahmet Sacan Pickle before 20070301 allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter.
CVE-2007-1101
Multiple cross-site scripting (XSS) vulnerabilities in Photostand 1.2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) message ("comment") or (2) name field, or the (3) q parameter in a search action in index.php.
CVE-2007-1102
Photostand 1.2.0 allows remote attackers to obtain sensitive information via a ' (quote) character in (1) a PHPSESSID cookie or (2) the id parameter in an article action in index.php, which reveal the path in various error messages.
CVE-2007-1103
Tor does not verify a node's uptime and bandwidth advertisements, which allows remote attackers who operate a low resource node to make false claims of greater resources, which places the node into use for many circuits and compromises the anonymity of traffic sources and destinations.
CVE-2007-1104
PHP remote file inclusion vulnerability in top.php in PHP Module Implementation (PHP-MIP) 0.1 allows remote attackers to execute arbitrary PHP code via a URL in the laypath parameter.
CVE-2007-1105
PHP remote file inclusion vulnerability in functions.php in Extreme phpBB (aka phpBB Extreme) 3.0.1 allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-1106
PHP remote file inclusion vulnerability in includes/functions_nomoketos_rules.php in the NoMoKeTos Rules 0.0.1 module for phpBB allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-1107
SQL injection vulnerability in thumbnails.php in Coppermine Photo Gallery (CPG) 1.3.x allows remote authenticated users to execute arbitrary SQL commands via a cpg131_fav cookie.  NOTE: it was later reported that 1.4.10, 1.4.14, and other 1.4.x versions are also affected using similar cookies.
CVE-2007-1108
PHP remote file inclusion vulnerability in index.php in Christian Schneider CS-Gallery 2.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the album parameter during a securealbum todo action.
CVE-2007-1109
Multiple cross-site scripting (XSS) vulnerabilities in Phpwebgallery 1.4.1 allow remote attackers to inject arbitrary web script or HTML via the (1) login or (2) mail_address field in Register.php, or the (3) search_author, (4) mode, (5) start_year, (6) end_year, or (7) date_type field in Search.php, a different vulnerability than CVE-2006-1674.  NOTE: 1.6.2 and other versions might also be affected.
CVE-2007-1110
Directory traversal vulnerability in data/showcode.php in ActiveCalendar 1.2.0 allows remote attackers to read arbitrary files via a .. (dot dot) in the page parameter.
CVE-2007-1111
Multiple cross-site scripting (XSS) vulnerabilities in ActiveCalendar 1.2.0 allow remote attackers to inject arbitrary web script or HTML via the css parameter to (1) flatevents.php, (2) js.php, (3) mysqlevents.php, (4) m_2.php, (5) m_3.php, (6) m_4.php, (7) xmlevents.php, (8) y_2.php, or (9) y_3.php in data/.
CVE-2007-1114
The child frames in Microsoft Internet Explorer 7 inherit the default charset from the parent window when a charset is not specified in an HTTP Content-Type header or META tag, which allows remote attackers to conduct cross-site scripting (XSS) attacks, as demonstrated using the UTF-7 character set.
CVE-2007-1115
The child frames in Opera 9 before 9.20 inherit the default charset from the parent window when a charset is not specified in an HTTP Content-Type header or META tag, which allows remote attackers to conduct cross-site scripting (XSS) attacks, as demonstrated using the UTF-7 character set.
CVE-2007-1116
The CheckLoadURI function in Mozilla Firefox 1.8 lists the about: URI as a ChromeProtocol and can be loaded via JavaScript, which allows remote attackers to obtain sensitive information by querying the browser's session history.
Comments in the hyperlinks also pointed to Firefox 2.0.0.2 containing the vulnerability.
