CVE-2016-2983
IBM Tealeaf Customer Experience 8.7, 8.8, and 9.0.2 could allow a remote attacker under unusual circumstances to read operational data or TLS session state for any active sessions, cause denial of service, or bypass security. IBM X-Force ID: 113999.
CVE-2016-6217
Cross-site scripting (XSS) vulnerability in Sophos PureMessage for UNIX before 6.3.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2017-1000386
Jenkins Active Choices plugin version 1.5.3 and earlier allowed users with Job/Configure permission to provide arbitrary HTML to be shown on the 'Build With Parameters' page through the 'Active Choices Reactive Reference Parameter' type. This could include, for example, arbitrary JavaScript. Active Choices now sanitizes the HTML inserted on the 'Build With Parameters' page if and only if the script is executed in a sandbox. As unsandboxed scripts are subject to administrator approval, it is up to the administrator to allow or disallow problematic script output.
CVE-2017-1000387
Jenkins Build-Publisher plugin version 1.21 and earlier stores credentials to other Jenkins instances in the file hudson.plugins.build_publisher.BuildPublisher.xml in the Jenkins master home directory. These credentials were stored unencrypted, allowing anyone with local file system access to access them. Additionally, the credentials were also transmitted in plain text as part of the configuration form. This could result in exposure of the credentials through browser extensions, cross-site scripting vulnerabilities, and similar situations.
CVE-2017-1000388
Jenkins Dependency Graph Viewer plugin 0.12 and earlier did not perform permission checks for the API endpoint that modifies the dependency graph, allowing anyone with Overall/Read permission to modify this data.
CVE-2017-1000389
Some URLs provided by Jenkins global-build-stats plugin version 1.4 and earlier returned a JSON response that contained request parameters. These responses had the Content Type: text/html, so could have been interpreted as HTML by clients, resulting in a potential reflected cross-site scripting vulnerability. Additionally, some URLs provided by global-build-stats plugin that modify data did not require POST requests to be sent, resulting in a potential cross-site request forgery vulnerability.
CVE-2017-1000390
Jenkins Multijob plugin version 1.25 and earlier did not check permissions in the Resume Build action, allowing anyone with Job/Read permission to resume the build.
CVE-2017-1000391
Jenkins versions 2.88 and earlier and 2.73.2 and earlier stores metadata related to 'people', which encompasses actual user accounts, as well as users appearing in SCM, in directories corresponding to the user ID on disk. These directories used the user ID for their name without additional escaping, potentially resulting in problems like overwriting of unrelated configuration files.
CVE-2017-1000392
Jenkins 2.88 and earlier; 2.73.2 and earlier Autocompletion suggestions for text fields were not escaped, resulting in a persisted cross-site scripting vulnerability if the source for the suggestions allowed specifying text that includes HTML metacharacters like less-than and greater-than characters.
CVE-2017-1000393
Jenkins 2.73.1 and earlier, 2.83 and earlier users with permission to create or configure agents in Jenkins could configure a launch method called 'Launch agent via execution of command on master'. This allowed them to run arbitrary shell commands on the master node whenever the agent was supposed to be launched. Configuration of this launch method now requires the Run Scripts permission typically only granted to administrators.
CVE-2017-1000394
Jenkins 2.73.1 and earlier, 2.83 and earlier bundled a version of the commons-fileupload library with the denial-of-service vulnerability known as CVE-2016-3092. The fix for that vulnerability has been backported to the version of the library bundled with Jenkins.
CVE-2017-1000395
Jenkins 2.73.1 and earlier, 2.83 and earlier provides information about Jenkins user accounts which is generally available to anyone with Overall/Read permissions via the /user/(username)/api remote API. This included e.g. Jenkins users' email addresses if the Mailer Plugin is installed. The remote API now no longer includes information beyond the most basic (user ID and name) unless the user requesting it is a Jenkins administrator.
CVE-2017-1000396
Jenkins 2.73.1 and earlier, 2.83 and earlier bundled a version of the commons-httpclient library with the vulnerability CVE-2012-6153 that incorrectly verified SSL certificates, making it susceptible to man-in-the-middle attacks. This library is widely used as a transitive dependency in Jenkins plugins. The fix for CVE-2012-6153 was backported to the version of commons-httpclient that is bundled in core and made available to plugins.
CVE-2017-1000397
Jenkins Maven Plugin 2.17 and earlier bundled a version of the commons-httpclient library with the vulnerability CVE-2012-6153 that incorrectly verified SSL certificates, making it susceptible to man-in-the-middle attacks. Maven Plugin 3.0 no longer has a dependency on commons-httpclient.
CVE-2017-1000398
The remote API in Jenkins 2.73.1 and earlier, 2.83 and earlier at /computer/(agent-name)/api showed information about tasks (typically builds) currently running on that agent. This included information about tasks that the current user otherwise has no access to, e.g. due to lack of Item/Read permission. This has been fixed, and the API now only shows information about accessible tasks.
CVE-2017-1000399
The Jenkins 2.73.1 and earlier, 2.83 and earlier remote API at /queue/item/(ID)/api showed information about tasks in the queue (typically builds waiting to start). This included information about tasks that the current user otherwise has no access to, e.g. due to lack of Item/Read permission. This has been fixed, and the API endpoint is now only available for tasks that the current user has access to.
CVE-2017-1000400
The Jenkins 2.73.1 and earlier, 2.83 and earlier remote API at /job/(job-name)/api contained information about upstream and downstream projects. This included information about tasks that the current user otherwise has no access to, e.g. due to lack of Item/Read permission. This has been fixed, and the API now only lists upstream and downstream projects that the current user has access to.
CVE-2017-1000401
The Jenkins 2.73.1 and earlier, 2.83 and earlier default form control for passwords and other secrets, <f:password/>, supports form validation (e.g. for API keys). The form validation AJAX requests were sent via GET, which could result in secrets being logged to a HTTP access log in non-default configurations of Jenkins, and made available to users with access to these log files. Form validation for <f:password/> is now always sent via POST, which is typically not logged.
CVE-2017-1000402
Jenkins Swarm Plugin Client 3.4 and earlier bundled a version of the commons-httpclient library with the vulnerability CVE-2012-6153 that incorrectly verified SSL certificates, making it susceptible to man-in-the-middle attacks.
CVE-2017-1000403
Jenkins Speaks! Plugin, all current versions, allows users with Job/Configure permission to run arbitrary Groovy code inside the Jenkins JVM, effectively elevating privileges to Overall/Run Scripts.
CVE-2017-1000404
The Jenkins Delivery Pipeline Plugin version 1.0.7 and earlier used the unescaped content of the query parameter 'fullscreen' in its JavaScript, resulting in a cross-site scripting vulnerability through specially crafted URLs.
CVE-2017-1204
IBM Tealeaf Customer Experience 8.7, 8.8, and 9.0.2 contains hard-coded credentials. A remote attacker could exploit this vulnerability to gain access to the system. IBM X-Force ID: 123740.
CVE-2017-12374
The ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition on an affected device. The vulnerability is due to a lack of input validation checking mechanisms during certain mail parsing operations (mbox.c operations on bounce messages). If successfully exploited, the ClamAV software could allow a variable pointing to the mail body which could cause a used after being free (use-after-free) instance which may lead to a disruption of services on an affected device to include a denial of service condition.
CVE-2017-12375
The ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition on an affected device. The vulnerability is due to a lack of input validation checking mechanisms during certain mail parsing functions (the rfc2047 function in mbox.c). An unauthenticated, remote attacker could exploit this vulnerability by sending a crafted email to the affected device. This action could cause a buffer overflow condition when ClamAV scans the malicious email, allowing the attacker to potentially cause a DoS condition on an affected device.
CVE-2017-12376
ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition or potentially execute arbitrary code on an affected device. The vulnerability is due to improper input validation checking mechanisms when handling Portable Document Format (.pdf) files sent to an affected device. An unauthenticated, remote attacker could exploit this vulnerability by sending a crafted .pdf file to an affected device. This action could cause a handle_pdfname (in pdf.c) buffer overflow when ClamAV scans the malicious file, allowing the attacker to cause a DoS condition or potentially execute arbitrary code.
CVE-2017-12377
ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition or potentially execute arbitrary code on an affected device. The vulnerability is due to improper input validation checking mechanisms in mew packet files sent to an affected device. A successful exploit could cause a heap-based buffer over-read condition in mew.c when ClamAV scans the malicious file, allowing the attacker to cause a DoS condition or potentially execute arbitrary code on the affected device.
CVE-2017-12378
ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition on an affected device. The vulnerability is due to improper input validation checking mechanisms of .tar (Tape Archive) files sent to an affected device. A successful exploit could cause a checksum buffer over-read condition when ClamAV scans the malicious .tar file, potentially allowing the attacker to cause a DoS condition on the affected device.
CVE-2017-12379
ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition or potentially execute arbitrary code on an affected device. The vulnerability is due to improper input validation checking mechanisms in the message parsing function on an affected system. An unauthenticated, remote attacker could exploit this vulnerability by sending a crafted email to the affected device. This action could cause a messageAddArgument (in message.c) buffer overflow condition when ClamAV scans the malicious email, allowing the attacker to potentially cause a DoS condition or execute arbitrary code on an affected device.
CVE-2017-12380
ClamAV AntiVirus software versions 0.99.2 and prior contain a vulnerability that could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition on an affected device. The vulnerability is due to improper input validation checking mechanisms in mbox.c during certain mail parsing functions of the ClamAV software. An unauthenticated, remote attacker could exploit this vulnerability by sending a crafted email to the affected device. An exploit could trigger a NULL pointer dereference condition when ClamAV scans the malicious email, which may result in a DoS condition.
CVE-2017-1279
IBM Tealeaf Customer Experience 8.7, 8.8, and 9.0.2 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system. IBM X-Force ID: 124757.
quried
CVE-2017-14521
In WonderCMS 2.3.1, the upload functionality accepts random application extensions and leads to malicious File Upload.
CVE-2017-14522
** DISPUTED **  In WonderCMS 2.3.1, the application's input fields accept arbitrary user input resulting in execution of malicious JavaScript. NOTE: the vendor disputes this issue stating that this is a feature that enables only a logged in administrator to write execute JavaScript anywhere on their website.
CVE-2017-14523
** DISPUTED **  WonderCMS 2.3.1 is vulnerable to an HTTP Host header injection attack. It uses user-entered values to redirect pages. NOTE: the vendor reports that exploitation is unlikely because the attack can only come from a local machine or from the administrator as a self attack.
CVE-2017-14592
Sourcetree for macOS had several argument and command injection bugs in Mercurial and Git repository handling. An attacker with permission to commit to a repository linked in Sourcetree for macOS is able to exploit this issue to gain code execution on the system. From version 1.4.0 of Sourcetree for macOS, this vulnerability can be triggered from a webpage through the use of the Sourcetree URI handler. Versions of Sourcetree for macOS starting with 1.0b2 before version 2.7.0 are affected by this vulnerability.
CVE-2017-14593
Sourcetree for Windows had several argument and command injection bugs in Mercurial and Git repository handling. An attacker with permission to commit to a repository linked in Sourcetree for Windows is able to exploit this issue to gain code execution on the system. From version 0.8.4b of Sourcetree for Windows, this vulnerability can be triggered from a webpage through the use of the Sourcetree URI handler. Versions of Sourcetree for Windows starting with 0.5.1.0 before version 2.4.7.0 are affected by this vulnerability
CVE-2017-1506
IBM Cognos TM1 10.2 and 10.2.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 129617.
CVE-2017-1515
IBM Doors Web Access 9.5 and 9.6 could allow an authenticated user to obtain sensitive information from HTTP internal server error responses. IBM X-Force ID: 129825.
CVE-2017-1516
IBM Doors Web Access 9.5 and 9.6 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 129826.
CVE-2017-1532
IBM DOORS 9.5 and 9.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 130411.
CVE-2017-1540
IBM Doors Web Access 9.5 and 9.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 130808.
CVE-2017-1545
IBM Doors Web Access 9.5 and 9.6 could allow an attacker with physical access to the system to log into the application using previously stored credentials. IBM X-Force ID: 130914.
CVE-2017-1563
IBM Doors Web Access 9.5 and 9.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 131763.
CVE-2017-1567
IBM Doors Web Access 9.5 and 9.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 131769.
CVE-2017-1653
IBM Jazz Foundation (IBM Rational Collaborative Lifecycle Management 6.0.x) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 133268.
CVE-2017-17976
In Utilities.php in Perfex CRM 1.9.7, Unrestricted file upload can lead to remote code execution.
CVE-2017-18076
In strategy.rb in OmniAuth before 1.3.2, the authenticity_token value is improperly protected because POST (in addition to GET) parameters are stored in the session and become available in the environment of the callback phase.
CVE-2017-2166
Open redirect vulnerability in GroupSession version 4.7.0 and earlier allows an attacker to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2017-3762
Sensitive data stored by Lenovo Fingerprint Manager Pro, version 8.01.86 and earlier, including users' Windows logon credentials and fingerprint data, is encrypted using a weak algorithm, contains a hard-coded password, and is accessible to all users with local non-administrative access to the system in which it is installed.
CVE-2017-3768
An unprivileged attacker with connectivity to the IMM2 could cause a denial of service attack on the IMM2 (Versions earlier than 4.4 for Lenovo System x and earlier than 6.4 for IBM System x). Flooding the IMM2 with a high volume of authentication failures via the Common Information Model (CIM) used by LXCA and OneCLI and other tools can exhaust available system memory which can cause the IMM2 to reboot itself until the requests cease.
CVE-2018-0506
Nootka 1.4.4 and earlier allows remote attackers to execute arbitrary OS commands via unspecified vectors.
CVE-2018-0507
Untrusted search path vulnerability in FLET'S VIRUS CLEAR Easy Setup & Application Tool ver.11 and earlier versions, FLET'S VIRUS CLEAR v6 Easy Setup & Application Tool ver.11 and earlier versions allow an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2018-1000017
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2015-1142857.  Reason: This candidate is effectively a reservation duplicate of CVE-2015-1142857, originally it was thought that an incomplete fix occurred and a second CVE was needed, however this does not appear to be the case. Notes: All CVE users should reference CVE-2015-1142857 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-1342
A Vulnerability exists on Admin Console where an attacker can upload files to the Admin Console server, and potentially execute them. This impacts NetIQ Access Manager versions 4.3 and 4.4 as well as the Administrative console.
CVE-2018-5750
The acpi_smbus_hc_add function in drivers/acpi/sbshc.c in the Linux kernel through 4.14.15 allows local users to obtain sensitive address information by reading dmesg data from an SBS HC printk call.
CVE-2018-6015
An issue was discovered in the "Email Subscribers & Newsletters" plugin before 3.4.8 for WordPress. Sending an HTTP POST request to a URI with /?es=export at the end, and adding option=view_all_subscribers in the body, allows downloading of a CSV data file with all subscriber data.
CVE-2018-6323
The elf_object_p function in elfcode.h in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29.1, has an unsigned integer overflow because bfd_size_type multiplication is not used. A crafted ELF file allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact.
