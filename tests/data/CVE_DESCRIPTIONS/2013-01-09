CVE-2013-0001
The Windows Forms (aka WinForms) component in Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.0 SP2, 4, and 4.5 does not properly initialize memory arrays, which allows remote attackers to obtain sensitive information via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application that leverages a pointer to an unmanaged memory location, aka "System Drawing Information Disclosure Vulnerability."
Per http://technet.microsoft.com/en-us/security/bulletin/ms13-004 Microsoft .NET Framework 3.0 Service Pack 2 is not vulnerable.
CVE-2013-0002
Buffer overflow in the Windows Forms (aka WinForms) component in Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.0 SP2, 3.5, 3.5.1, 4, and 4.5 allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application that leverages improper counting of objects during a memory copy operation, aka "WinForms Buffer Overflow Vulnerability."
Per http://technet.microsoft.com/en-us/security/bulletin/ms13-004 Microsoft .NET Framework 3.0 Service Pack 2 is not vulnerable.
CVE-2013-0003
Buffer overflow in a System.DirectoryServices.Protocols (S.DS.P) namespace method in Microsoft .NET Framework 2.0 SP2, 3.0 SP2, 3.5, 3.5.1, 4, and 4.5 allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application that leverages a missing array-size check during a memory copy operation, aka "S.DS.P Buffer Overflow Vulnerability."
Per http://technet.microsoft.com/en-us/security/bulletin/ms13-004 Microsoft .NET Framework 3.0 Service Pack 2 is not vulnerable.
CVE-2013-0004
Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly validate the permissions of objects in memory, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application, aka "Double Construction Vulnerability."
CVE-2013-0005
The WCF Replace function in the Open Data (aka OData) protocol implementation in Microsoft .NET Framework 3.5, 3.5 SP1, 3.5.1, and 4, and the Management OData IIS Extension on Windows Server 2012, allows remote attackers to cause a denial of service (resource consumption and daemon restart) via crafted values in HTTP requests, aka "Replace Denial of Service Vulnerability."
CVE-2013-0006
Microsoft XML Core Services (aka MSXML) 3.0, 5.0, and 6.0 does not properly parse XML content, which allows remote attackers to execute arbitrary code via a crafted web page, aka "MSXML Integer Truncation Vulnerability."
CVE-2013-0007
Microsoft XML Core Services (aka MSXML) 4.0, 5.0, and 6.0 does not properly parse XML content, which allows remote attackers to execute arbitrary code via a crafted web page, aka "MSXML XSLT Vulnerability."
CVE-2013-0008
win32k.sys in the kernel-mode drivers in Microsoft Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle window broadcast messages, which allows local users to gain privileges via a crafted application, aka "Win32k Improper Message Handling Vulnerability."
CVE-2013-0009
Cross-site scripting (XSS) vulnerability in Microsoft System Center Operations Manager 2007 SP1 and R2 allows remote attackers to inject arbitrary web script or HTML via crafted input, aka "System Center Operations Manager Web Console XSS Vulnerability," a different vulnerability than CVE-2013-0010.
CVE-2013-0010
Cross-site scripting (XSS) vulnerability in Microsoft System Center Operations Manager 2007 SP1 and R2 allows remote attackers to inject arbitrary web script or HTML via crafted input, aka "System Center Operations Manager Web Console XSS Vulnerability," a different vulnerability than CVE-2013-0009.
CVE-2013-0011
The Print Spooler in Microsoft Windows Server 2008 R2 and R2 SP1 and Windows 7 Gold and SP1 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted print job, aka "Windows Print Spooler Components Vulnerability."
CVE-2013-0013
The SSL provider component in Microsoft Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle encrypted packets, which allows man-in-the-middle attackers to conduct SSLv2 downgrade attacks against (1) SSLv3 sessions or (2) TLS sessions by intercepting handshakes and injecting content, aka "Microsoft SSL Version 3 and TLS Protocol Security Feature Bypass Vulnerability."
CVE-2013-0625
Adobe ColdFusion 9.0, 9.0.1, and 9.0.2, when a password is not configured, allows remote attackers to bypass authentication and possibly execute arbitrary code via unspecified vectors, as exploited in the wild in January 2013.
CVE-2013-0629
Adobe ColdFusion 9.0, 9.0.1, 9.0.2, and 10, when a password is not configured, allows attackers to access restricted directories via unspecified vectors, as exploited in the wild in January 2013.
CVE-2013-0631
Adobe ColdFusion 9.0, 9.0.1, and 9.0.2 allows attackers to obtain sensitive information via unspecified vectors, as exploited in the wild in January 2013.
