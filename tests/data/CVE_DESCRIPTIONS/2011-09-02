CVE-2009-5086
Cross-site scripting (XSS) vulnerability in Appliance Configuration Manager (ACM) in Juniper IDP 4.1 before 4.1r3 and 4.2 before 4.2r1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-0311
The class file parser in IBM Java before 1.4.2 SR13 FP9, as used in IBM Runtimes for Java Technology 5.0.0 before SR13 and 6.0.0 before SR10, allows remote authenticated users to cause a denial of service (JVM segmentation fault, and possibly memory consumption or an infinite loop) via a crafted attribute length field in a class file, which triggers a buffer over-read.
CVE-2011-0342
Multiple buffer overflows in the InduSoft ISSymbol ActiveX control in ISSymbol.ocx 301.1104.601.0 in InduSoft Web Studio 7.0B2 hotfix 7.0.01.04 allow remote attackers to execute arbitrary code via a long parameter to the (1) Open, (2) Close, or (3) SetCurrentLanguage method.
CVE-2011-0541
fuse 2.8.5 and earlier does not properly handle when /etc/mtab cannot be updated, which allows local users to unmount arbitrary directories via a symlink attack.
CVE-2011-0542
fusermount in fuse 2.8.5 and earlier does not perform a chdir to / before performing a mount or umount, which allows local users to unmount arbitrary directories via unspecified vectors.
CVE-2011-0543
Certain legacy functionality in fusermount in fuse 2.8.5 and earlier, when util-linux does not support the --no-canonicalize option, allows local users to bypass intended access restrictions and unmount arbitrary directories via a symlink attack.
CVE-2011-1411
Shibboleth OpenSAML library 2.4.x before 2.4.3 and 2.5.x before 2.5.1, and IdP before 2.3.2, allows remote attackers to forge messages and bypass authentication via an "XML Signature wrapping attack."
CVE-2011-1944
Integer overflow in xpath.c in libxml2 2.6.x through 2.6.32 and 2.7.x through 2.7.8, and libxml 1.8.16 and earlier, allows context-dependent attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted XML file that triggers a heap-based buffer overflow when adding a new namespace node, related to handling of XPath expressions.
CVE-2011-2176
GNOME NetworkManager before 0.8.6 does not properly enforce the auth_admin element in PolicyKit, which allows local users to bypass intended wireless network sharing restrictions via unspecified vectors.
CVE-2011-2594
Heap-based buffer overflow in KMPlayer 3.0.0.1441, and possibly other versions, allows remote attackers to execute arbitrary code via a playlist (.KPL) file with a long Title field.
CVE-2011-2762
The web interface on the LifeSize Room appliance LS_RM1_3.5.3 (11) allows remote attackers to bypass authentication via unspecified data associated with a "true" authentication status, related to AMF data and the LSRoom_Remoting.authenticate function in gateway.php.
CVE-2011-2763
The web interface on the LifeSize Room appliance LS_RM1_3.5.3 (11) and 4.7.18 allows remote attackers to execute arbitrary commands via a modified request to the LSRoom_Remoting.doCommand function in gateway.php.
CVE-2011-2903
Heap-based buffer overflow in tcptrack before 1.4.2 might allow attackers to execute arbitrary code via a long command line argument. NOTE: this is only a vulnerability in limited scenarios in which tcptrack is "configured as a handler for other applications." This issue might not qualify for inclusion in CVE.
CVE-2011-3132
Cross-site scripting (XSS) vulnerability in TIBCO Spotfire Server 3.0.x before 3.0.2, 3.1.x before 3.1.2, 3.2.x before 3.2.1, and 3.3.x before 3.3.1, and Spotfire Analytics Server before 10.1.1, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-3133
Session fixation vulnerability in TIBCO Spotfire Server 3.0.x before 3.0.2, 3.1.x before 3.1.2, 3.2.x before 3.2.1, and 3.3.x before 3.3.1, and Spotfire Analytics Server before 10.1.1, allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html
'CWE-384: Session Fixation'
CVE-2011-3134
Unspecified vulnerability in TIBCO Spotfire Server 3.0.x before 3.0.2, 3.1.x before 3.1.2, 3.2.x before 3.2.1, and 3.3.x before 3.3.1, and Spotfire Analytics Server before 10.1.1, allows remote attackers to modify data or obtain sensitive information via a crafted URL.
CVE-2011-3385
Cross-site scripting (XSS) vulnerability in WebsiteBaker before 2.8, as used in LEPTON and possibly other products, allows remote attackers to inject arbitrary web script or HTML via unknown vectors, a different vulnerability than CVE-2006-2307.
CVE-2011-3386
Unspecified vulnerability in Medtronic Paradigm wireless insulin pump 512, 522, 712, and 722 allows remote attackers to modify the delivery of an insulin bolus dose and cause a denial of service (adverse human health effects) via unspecified vectors involving wireless communications and knowledge of the device's serial number, as demonstrated by Jerome Radcliffe at the Black Hat USA conference in August 2011.  NOTE: the vendor has disputed the severity of this issue, saying "we believe the risk of deliberate, malicious, or unauthorized manipulation of medical devices is extremely low... we strongly believe it would be extremely difficult for a third-party to wirelessly tamper with your insulin pump... you would be able to detect tones on the insulin pump that weren't intentionally programmed and could intervene accordingly."
CVE-2011-3387
The class file parser in IBM Java 1.4.2 SR13 FP9 allows remote authenticated users to cause a denial of service (memory consumption or an infinite loop) via a crafted attribute length field in a class file, related to validation of a length field at the wrong time, a different vulnerability than CVE-2011-0311.
