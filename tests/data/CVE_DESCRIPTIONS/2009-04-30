CVE-2009-0663
Heap-based buffer overflow in the DBD::Pg (aka DBD-Pg or libdbd-pg-perl) module 1.49 for Perl might allow context-dependent attackers to execute arbitrary code via unspecified input to an application that uses the getline and pg_getline functions to read database rows.
CVE-2009-1255
The process_stat function in (1) Memcached before 1.2.8 and (2) MemcacheDB 1.2.0 discloses (a) the contents of /proc/self/maps in response to a stats maps command and (b) memory-allocation statistics in response to a stats malloc command, which allows remote attackers to obtain sensitive information such as the locations of memory regions, and defeat ASLR protection, by sending a command to the daemon's TCP port.
CVE-2009-1291
Stack-based buffer overflow in TIBCO SmartSockets before 6.8.2, SmartSockets Product Family (aka RTworks) before 4.0.5, and Enterprise Message Service (EMS) 4.0.0 through 5.1.1, as used in SmartSockets Server and RTworks Server (aka RTserver), SmartSockets client libraries and add-on products, RTworks libraries and components, EMS Server (aka tibemsd), SmartMQ, iProcess Engine, ActiveMatrix products, and CA Enterprise Communicator, allows remote attackers to execute arbitrary code via "inbound data," as demonstrated by requests to the UDP interface of the RTserver component, and data injection into the TCP stream to tibemsd.
CVE-2009-1295
Apport before 0.108.4 on Ubuntu 8.04 LTS, before 0.119.2 on Ubuntu 8.10, and before 1.0-0ubuntu5.2 on Ubuntu 9.04 does not properly remove files from the application's crash-report directory, which allows local users to delete arbitrary files via unspecified vectors.
CVE-2009-1313
The nsTextFrame::ClearTextRun function in layout/generic/nsTextFrameThebes.cpp in Mozilla Firefox 3.0.9 allows remote attackers to cause a denial of service (memory corruption) and probably execute arbitrary code via unspecified vectors.  NOTE: this vulnerability reportedly exists because of an incorrect fix for CVE-2009-1302.
CVE-2009-1339
Cross-site request forgery (CSRF) vulnerability in TWiki before 4.3.1 allows remote authenticated users to hijack the authentication of arbitrary users for requests that update pages, as demonstrated by a URL for a save script in the SRC attribute of an IMG element, a related issue to CVE-2009-1434.
CVE-2009-1341
Memory leak in the dequote_bytea function in quote.c in the DBD::Pg (aka DBD-Pg or libdbd-pg-perl) module before 2.0.0 for Perl allows context-dependent attackers to cause a denial of service (memory consumption) by fetching data with BYTEA columns.
CVE-2009-1348
The AV engine before DAT 5600 in McAfee VirusScan, Total Protection, Internet Security, SecurityShield for Microsoft ISA Server, Security for Microsoft Sharepoint, Security for Email Servers, Email Gateway, and Active Virus Defense allows remote attackers to bypass virus detection via (1) an invalid Headflags field in a malformed RAR archive, (2) an invalid Packsize field in a malformed RAR archive, or (3) an invalid Filelength field in a malformed ZIP archive.
CVE-2009-1415
lib/pk-libgcrypt.c in libgnutls in GnuTLS before 2.6.6 does not properly handle invalid DSA signatures, which allows remote attackers to cause a denial of service (application crash) and possibly have unspecified other impact via a malformed DSA key that triggers a (1) free of an uninitialized pointer or (2) double free.
CVE-2009-1416
lib/gnutls_pk.c in libgnutls in GnuTLS 2.5.0 through 2.6.5 generates RSA keys stored in DSA structures, instead of the intended DSA keys, which might allow remote attackers to spoof signatures on certificates or have unspecified other impact by leveraging an invalid DSA key.
CVE-2009-1417
gnutls-cli in GnuTLS before 2.6.6 does not verify the activation and expiration times of X.509 certificates, which allows remote attackers to successfully present a certificate that is (1) not yet valid or (2) no longer valid, related to lack of time checks in the _gnutls_x509_verify_certificate function in lib/x509/verify.c in libgnutls_x509, as used by (a) Exim, (b) OpenLDAP, and (c) libsoup.
CVE-2009-1432
Symantec Reporting Server, as used in Symantec AntiVirus (SAV) Corporate Edition 10.1 before 10.1 MR8 and 10.2 before 10.2 MR2, Symantec Client Security (SCS) before 3.1 MR8, and the Symantec Endpoint Protection Manager (SEPM) component in Symantec Endpoint Protection (SEP) before 11.0 MR2, allows remote attackers to inject arbitrary text into the login screen, and possibly conduct phishing attacks, via vectors involving a URL that is not properly handled.
CVE-2009-1434
Cross-site request forgery (CSRF) vulnerability in Foswiki before 1.0.5 allows remote attackers to hijack the authentication of arbitrary users for requests that modify pages, change permissions, or change group memberships, as demonstrated by a URL for a (1) save or (2) view script in the SRC attribute of an IMG element, a related issue to CVE-2009-1339.
CVE-2009-1492
The getAnnots Doc method in the JavaScript API in Adobe Reader and Acrobat 9.1, 8.1.4, 7.1.1, and earlier allows remote attackers to cause a denial of service (memory corruption) or execute arbitrary code via a PDF file that contains an annotation, and has an OpenAction entry with JavaScript code that calls this method with crafted integer arguments.
CVE-2009-1493
The customDictionaryOpen spell method in the JavaScript API in Adobe Reader 9.1, 8.1.4, 7.1.1, and earlier on Linux and UNIX allows remote attackers to cause a denial of service (memory corruption) or execute arbitrary code via a PDF file that triggers a call to this method with a long string in the second argument.
CVE-2009-1494
The process_stat function in Memcached 1.2.8 discloses memory-allocation statistics in response to a stats malloc command, which allows remote attackers to obtain potentially sensitive information by sending this command to the daemon's TCP port.
