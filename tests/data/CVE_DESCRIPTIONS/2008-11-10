CVE-2008-4281
Directory traversal vulnerability in VMWare ESXi 3.5 before ESXe350-200810401-O-UG and ESX 3.5 before ESX350-200810201-UG allows administrators with the Datastore.FileManagement privilege to gain privileges via unknown vectors.
CVE-2008-4387
Unspecified vulnerability in the Simba MDrmSap ActiveX control in mdrmsap.dll in SAP SAPgui allows remote attackers to execute arbitrary code via unknown vectors involving instantiation by Internet Explorer.
Patch Information (SAP Login Required) = http://service.sap.com/sap/support/notes/1142431
CVE-2008-4818
Cross-site scripting (XSS) vulnerability in Adobe Flash Player 9.0.124.0 and earlier allows remote attackers to inject arbitrary web script or HTML via vectors involving HTTP response headers.
CVE-2008-4819
Unspecified vulnerability in Adobe Flash Player 9.0.124.0 and earlier makes it easier for remote attackers to conduct DNS rebinding attacks via unknown vectors.
CVE-2008-4820
Unspecified vulnerability in the Flash Player ActiveX control in Adobe Flash Player 9.0.124.0 and earlier on Windows allows attackers to obtain sensitive information via unknown vectors.
CVE-2008-4821
Adobe Flash Player 9.0.124.0 and earlier, when a Mozilla browser is used, does not properly interpret jar: URLs, which allows attackers to obtain sensitive information via unknown vectors.
CVE-2008-4822
Adobe Flash Player 9.0.124.0 and earlier does not properly interpret policy files, which allows remote attackers to bypass a non-root domain policy.
CVE-2008-4823
Cross-site scripting (XSS) vulnerability in Adobe Flash Player 9.0.124.0 and earlier allows remote attackers to inject arbitrary web script or HTML via vectors related to loose interpretation of an ActionScript attribute.
CVE-2008-4831
Unspecified vulnerability in Adobe ColdFusion 8 and 8.0.1 and ColdFusion MX 7.0.2 allows local users to bypass sandbox restrictions, and obtain sensitive information or possibly gain privileges, via unknown vectors.
CVE-2008-4915
The CPU hardware emulation in VMware Workstation 6.0.5 and earlier and 5.5.8 and earlier; Player 2.0.x through 2.0.5 and 1.0.x through 1.0.8; ACE 2.0.x through 2.0.5 and earlier, and 1.0.x through 1.0.7; Server 1.0.x through 1.0.7; ESX 2.5.4 through 3.5; and ESXi 3.5, when running 32-bit and 64-bit guest operating systems, does not properly handle the Trap flag, which allows authenticated guest OS users to gain privileges on the guest OS.
CVE-2008-5000
SQL injection vulnerability in admin/includes/news.inc.php in PHPX 3.5.16, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via uppercase characters in the news_id parameter.
CVE-2008-5001
Multiple stack-based buffer overflows in multiple functions in vncviewer/FileTransfer.cpp in vncviewer for UltraVNC 1.0.2 and 1.0.4 before 01252008, when in LISTENING mode or when using the DSM plugin, allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unspecified parameters, a different issue than CVE-2008-0610.
CVE-2008-5002
Insecure method vulnerability in the ChilkatCrypt2.ChilkatCrypt2.1 ActiveX control (ChilkatCrypt2.dll 4.3.2.1) in Chilkat Crypt ActiveX Component allows remote attackers to create and overwrite arbitrary files via the WriteFile method.  NOTE: this could be leveraged for code execution by creating executable files in Startup folders or by accessing files using hcp:// URLs.  NOTE: some of these details are obtained from third party information.
CVE-2008-5003
SQL injection vulnerability in ndetail.php in Shahrood allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5004
SQL injection vulnerability in genscode.php in myWebland Bloggie Lite 0.0.2 beta allows remote attackers to execute arbitrary SQL commands via a crafted cookie.
CVE-2008-5005
Multiple stack-based buffer overflows in (1) University of Washington IMAP Toolkit 2002 through 2007c, (2) University of Washington Alpine 2.00 and earlier, and (3) Panda IMAP allow (a) local users to gain privileges by specifying a long folder extension argument on the command line to the tmail or dmail program; and (b) remote attackers to execute arbitrary code by sending e-mail to a destination mailbox name composed of a username and '+' character followed by a long string, processed by the tmail or possibly dmail program.
CVE-2008-5006
smtp.c in the c-client library in University of Washington IMAP Toolkit 2007b allows remote SMTP servers to cause a denial of service (NULL pointer dereference and application crash) by responding to the QUIT command with a close of the TCP connection instead of the expected 221 response code.
CVE-2008-5007
create_lazarus_export_tgz.sh in lazarus 0.9.24 allows local users to overwrite or delete arbitrary files via a symlink attack on a (1) /tmp/lazarus.tgz temporary file or a (2) /tmp/lazarus temporary directory.
CVE-2008-5008
Buffer overflow in src/src_sinc.c in Secret Rabbit Code (aka SRC or libsamplerate) before 0.1.4, when "extreme low conversion ratios" are used, allows user-assisted attackers to have an unknown impact via a crafted audio file.
CVE-2008-5009
Race condition in the s_xout kernel module in Sun Solstice X.25 9.2, when running on a multiple CPU machine, allows local users to cause a denial of service (panic) via vectors involving reading the /dev/xty file.
CVE-2008-5010
in.dhcpd in the DHCP implementation in Sun Solaris 8 through 10, and OpenSolaris before snv_103, allows remote attackers to cause a denial of service (assertion failure and daemon exit) via unknown DHCP requests related to the "number of offers," aka Bug ID 6713805.
CVE-2008-5011
Multiple cross-site scripting (XSS) vulnerabilities in IBM Lotus Quickr 8.1 before 8.1.0.2 services for Lotus Domino allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, possibly related to qpconfig_sample.xml, aka SPR CWIR7KMPVP and THES7F9NVR, a different vulnerability than CVE-2008-2163 and CVE-2008-3860.
CVE-2008-5026
Microsoft SharePoint uses URLs with the same hostname and port number for a web site's primary files and individual users' uploaded files (aka attachments), which allows remote authenticated users to leverage same-origin relationships and conduct cross-site scripting (XSS) attacks by uploading HTML documents.
CVE-2008-5027
The Nagios process in (1) Nagios before 3.0.5 and (2) op5 Monitor before 4.0.1 allows remote authenticated users to bypass authorization checks, and trigger execution of arbitrary programs by this process, via an (a) custom form or a (b) browser addon.
CVE-2008-5028
Cross-site request forgery (CSRF) vulnerability in cmd.cgi in (1) Nagios 3.0.5 and (2) op5 Monitor before 4.0.1 allows remote attackers to send commands to the Nagios process, and trigger execution of arbitrary programs by this process, via unspecified HTTP requests.
CVE-2008-5029
The __scm_destroy function in net/core/scm.c in the Linux kernel 2.6.27.4, 2.6.26, and earlier makes indirect recursive calls to itself through calls to the fput function, which allows local users to cause a denial of service (panic) via vectors related to sending an SCM_RIGHTS message through a UNIX domain socket and closing file descriptors.
CVE-2008-5030
Heap-based buffer overflow in the cddb_read_disc_data function in cddb.c in libcdaudio 0.99.12p2 allows remote CDDB servers to execute arbitrary code via long CDDB data.
CVE-2008-5031
Multiple integer overflows in Python 2.2.3 through 2.5.1, and 2.6, allow context-dependent attackers to have an unknown impact via a large integer value in the tabsize argument to the expandtabs method, as implemented by (1) the string_expandtabs function in Objects/stringobject.c and (2) the unicode_expandtabs function in Objects/unicodeobject.c.  NOTE: this vulnerability reportedly exists because of an incomplete fix for CVE-2008-2315.
CVE-2008-5032
Stack-based buffer overflow in VideoLAN VLC media player 0.5.0 through 0.9.5 might allow user-assisted attackers to execute arbitrary code via the header of an invalid CUE image file, related to modules/access/vcd/cdrom.c.  NOTE: this identifier originally included an issue related to RealText, but that issue has been assigned a separate identifier, CVE-2008-5036.
CVE-2008-5033
The chip_command function in drivers/media/video/tvaudio.c in the Linux kernel 2.6.25.x before 2.6.25.19, 2.6.26.x before 2.6.26.7, and 2.6.27.x before 2.6.27.3 allows attackers to cause a denial of service (NULL function pointer dereference and OOPS) via unknown vectors.
CVE-2008-5034
** DISPUTED **  master-filter in printfilters-ppd 2.13 allows local users to overwrite arbitrary files via a symlink attack on the /tmp/filter.debug temporary file.  NOTE: the vendor disputes this vulnerability, stating 'this package does not have " possibility of attack with the help of symlinks"'.
CVE-2008-5035
The Resource Monitoring and Control (RMC) daemon in IBM Hardware Management Console (HMC) 7 release 3.2.0 SP1 and 3.3.0 SP2 allows remote attackers to cause a denial of service (daemon crash or hang) via a packet with an invalid length.
CVE-2008-5036
Stack-based buffer overflow in VideoLAN VLC media player 0.9.x before 0.9.6 might allow user-assisted attackers to execute arbitrary code via an an invalid RealText (rt) subtitle file, related to the ParseRealText function in modules/demux/subtitle.c.  NOTE: this issue was SPLIT from CVE-2008-5032 on 20081110.
