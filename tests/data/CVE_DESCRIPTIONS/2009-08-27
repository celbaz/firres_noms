CVE-2008-7095
The SNMP daemon in ArubaOS 3.3.2.6 in Aruba Mobility Controller does not restrict SNMP access, which allows remote attackers to (1) read all SNMP community strings via SNMP-COMMUNITY-MIB::snmpCommunityName (1.3.6.1.6.3.18.1.1.1.2) or SNMP-VIEW-BASED-ACM-MIB::vacmGroupName (1.3.6.1.6.3.16.1.2.1.3) with knowledge of one community string, and (2) read SNMPv3 user names via SNMP-USER-BASED-SM-MIB or SNMP-VIEW-BASED-ACM-MIB.
CVE-2008-7096
Intel Desktop and Intel Mobile Boards with BIOS firmware DQ35JO, DQ35MP, DP35DP, DG33FB, DG33BU, DG33TL, MGM965TW, D945GCPE, and DX38BT allows local administrators with ring 0 privileges to gain additional privileges and modify code that is running in System Management Mode, or access hypervisory memory as demonstrated at Black Hat 2008 by accessing certain remapping registers in Xen 3.3.
CVE-2008-7097
Multiple SQL injection vulnerabilities in Qsoft K-Rate Premium allow remote attackers to execute arbitrary SQL commands via (1) the $id variable in admin/includes/dele_cpac.php, (2) $ord[order_id] variable in payments/payment_received.php, (3) $id variable in includes/functions.php, and (4) unspecified variables in modules/chat.php, as demonstrated via the (a) show parameter in an online action to index.php; (b) PATH_INTO to the room/ handler; (c) image and (d) id parameters in a vote action to index.php; (e) PATH_INFO to the blog/ handler; and (f) id parameter in a blog_edit action to index.php.
CVE-2008-7098
Multiple cross-site scripting (XSS) vulnerabilities in Qsoft K-Rate Premium allow remote attackers to inject arbitrary web script or HTML via the blog, possibly the (1) Title and (2) Text fields; (3) the gallery, possibly the Description field in Your Pictures; (4) the forum, possibly the Your Message field when posting a new thread; or (5) the vote parameter in a view action to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-7099
Unspecified vulnerability in the Manage Templates feature in Qsoft K-Rate Premium allows remote attackers to execute arbitrary PHP code via unknown vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-7100
Unspecified vulnerability in DotNetNuke 4.4.1 through 4.8.4 allows remote authenticated users to bypass authentication and gain privileges via unknown vectors related to a "unique id" for user actions and improper validation of a "user identity."
CVE-2008-7101
Unspecified vulnerability in DotNetNuke 4.0 through 4.8.4 and 5.0 allows remote attackers to obtain sensitive information (portal number) by accessing the install wizard page via unknown vectors.
CVE-2008-7102
DotNetNuke 2.0 through 4.8.4 allows remote attackers to load .ascx files instead of skin files, and possibly access privileged functionality, via unknown vectors related to parameter validation.
CVE-2008-7103
Stack-based buffer overflow in an ActiveX control in najdisitoolbar.dll in Najdi.si Toolbar 2.0.4.1 allows remote attackers to cause a denial of service (browser crash) or execute arbitrary code via a long Document.Location property value.
CVE-2008-7104
Sophos PureMessage Scanner service (PMScanner.exe) in PureMessage for Microsoft Exchange 3.0 before 3.0.2 allows remote attackers to cause a denial of service (message queue delay and incomplete spam rule update) via a crafted (1) RTF or (2) PDF file.
CVE-2008-7105
Sophos PureMessage for Microsoft Exchange 3.0 before 3.0.2 allows remote attackers to cause a denial of service (EdgeTransport.exe termination) via a TNEF-encoded message with a crafted rich text body that is not properly handled during conversion to plain text.  NOTE: this might be related to CVE-2008-7104.
CVE-2008-7106
The installation of Sophos PureMessage for Microsoft Exchange 3.0 before 3.0.2, when both anti-virus and anti-spam are supported, does not create or launch the associated scan engines when the system is under heavy load, which has unspecified impact, probably remote bypass of scanner protection or a denial of service (message loss or delay).
CVE-2009-2050
Cisco Unified Communications Manager (aka CUCM, formerly CallManager) before 6.1(1) allows remote attackers to cause a denial of service (voice-services outage) via a malformed header in a SIP message, aka Bug ID CSCsi46466.
CVE-2009-2051
Cisco IOS 12.2 through 12.4 and 15.0 through 15.1, Cisco IOS XE 2.5.x and 2.6.x before 2.6.1, and Cisco Unified Communications Manager (aka CUCM, formerly CallManager) 4.x, 5.x before 5.1(3g), 6.x before 6.1(4), and 7.x before 7.1(2) allow remote attackers to cause a denial of service (device reload or voice-services outage) via a malformed SIP INVITE message that triggers an improper call to the sipSafeStrlen function, aka Bug IDs CSCsz40392 and CSCsz43987.
CVE-2009-2052
Cisco Unified Communications Manager (aka CUCM, formerly CallManager) 4.x, 5.x before 5.1(3g), 6.x before 6.1(4), 7.0 before 7.0(2), and 7.1 before 7.1(2); and Cisco Unified Presence 1.x, 6.x before 6.0(6), and 7.x before 7.0(4); allows remote attackers to cause a denial of service (TCP services outage) via a large number of TCP connections, related to "tracking of network connections," aka Bug IDs CSCsq22534 and CSCsw52371.
CVE-2009-2053
Cisco Unified Communications Manager (aka CUCM, formerly CallManager) 4.x, 5.x before 5.1(3g), 6.x before 6.1(4), 7.0 before 7.0(2a)su1, and 7.1 before 7.1(2) allows remote attackers to cause a denial of service (file-descriptor exhaustion and SCCP outage) via a flood of TCP packets, aka Bug ID CSCsx32236.
CVE-2009-2054
Cisco Unified Communications Manager (aka CUCM, formerly CallManager) 4.x, 5.x before 5.1(3g), 6.x before 6.1(4), 7.0 before 7.0(2a)su1, and 7.1 before 7.1(2a)su1 allows remote attackers to cause a denial of service (file-descriptor exhaustion and SIP outage) via a flood of TCP packets, aka Bug ID CSCsx23689.
CVE-2009-2698
The udp_sendmsg function in the UDP implementation in (1) net/ipv4/udp.c and (2) net/ipv6/udp.c in the Linux kernel before 2.6.19 allows local users to gain privileges or cause a denial of service (NULL pointer dereference and system crash) via vectors involving the MSG_MORE flag and a UDP socket.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2009-2861
The Over-the-Air Provisioning (OTAP) functionality on Cisco Aironet Lightweight Access Point 1100 and 1200 devices does not properly implement access-point association, which allows remote attackers to spoof a controller and cause a denial of service (service outage) via crafted remote radio management (RRM) packets, aka "SkyJack" or Bug ID CSCtb56664.
CVE-2009-2935
Google V8, as used in Google Chrome before 2.0.172.43, allows remote attackers to bypass intended restrictions on reading memory, and possibly obtain sensitive information or execute arbitrary code in the Chrome sandbox, via crafted JavaScript.
CVE-2009-2972
in.lpd in the print service in Sun Solaris 8 and 9 allows remote attackers to cause a denial of service (memory consumption) via unspecified vectors that trigger a "fork()/exec() bomb."
CVE-2009-2973
Google Chrome before 2.0.172.43 does not prevent SSL connections to a site with an X.509 certificate signed with the (1) MD2 or (2) MD4 algorithm, which makes it easier for man-in-the-middle attackers to spoof arbitrary HTTPS servers via a crafted certificate, a related issue to CVE-2009-2409.
CVE-2009-2974
Google Chrome 1.0.154.65, 1.0.154.48, and earlier allows remote attackers to (1) cause a denial of service (application hang) via vectors involving a chromehtml: URI value for the document.location property or (2) cause a denial of service (application hang and CPU consumption) via vectors involving a series of function calls that set a chromehtml: URI value for the document.location property.
CVE-2009-2975
Mozilla Firefox 3.5.2 on Windows XP, in some situations possibly involving an incompletely configured protocol handler, does not properly implement setting the document.location property to a value specifying a protocol associated with an external application, which allows remote attackers to cause a denial of service (memory consumption) via vectors involving a series of function calls that set this property, as demonstrated by (1) the chromehtml: protocol and (2) the aim: protocol.
CVE-2009-2976
Cisco Aironet Lightweight Access Point (AP) devices send the contents of certain multicast data frames in cleartext, which allows remote attackers to discover Wireless LAN Controller MAC addresses and IP addresses, and AP configuration details, by sniffing the wireless network.
CVE-2009-2977
The Cisco Security Monitoring, Analysis and Response System (CS-MARS) 6.0.4 and earlier stores cleartext passwords in log/sysbacktrace.## files within error-logs.tar.gz archives, which allows context-dependent attackers to obtain sensitive information by reading these files.
CVE-2009-2978
SQL injection vulnerability in SugarCRM 4.5.1o and earlier, 5.0.0k and earlier, and 5.2.0g and earlier, allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
