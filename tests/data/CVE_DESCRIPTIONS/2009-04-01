CVE-2007-4475
Stack-based buffer overflow in EAI WebViewer3D ActiveX control (webviewer3d.dll) in SAP AG SAPgui before 7.10 Patch Level 9 allows remote attackers to execute arbitrary code via a long argument to the SaveViewToSessionFile method.
CVE-2008-3871
Multiple format string vulnerabilities in UltraISO 9.3.1.2633, and possibly other versions before 9.3.3.2685, allow user-assisted attackers to execute arbitrary code via format string specifiers in the filename of a (1) DAA or (2) ISZ file.
CVE-2008-4825
Multiple buffer overflows in UltraISO 9.3.1.2633, and possibly other versions before 9.3.3.2685, allow user-assisted attackers to execute arbitrary code via a crafted (1) CIF, (2) C2D, or (3) GI file.
CVE-2008-6573
Multiple SQL injection vulnerabilities in Avaya SIP Enablement Services (SES) in Avaya Avaya Communication Manager 3.x, 4.0, and 5.0 (1) allow remote attackers to execute arbitrary SQL commands via unspecified vectors related to profiles in the SIP Personal Information Manager (SPIM) in the web interface; and allow remote authenticated users to execute arbitrary SQL commands via unspecified vectors related to (2) permissions for SPIM profiles in the web interface and (3) a crafted SIP request to the SIP server.
CVE-2008-6574
Unspecified vulnerability in SIP Enablement Services (SES) in Avaya Communication Manager 3.1.x and 4.x allows remote attackers to gain privileges and cause a denial of service via unknown vectors related to reuse of valid credentials.
CVE-2008-6575
Unspecified vulnerability in the SIP server in SIP Enablement Services (SES) in Avaya Communication Manager 3.1.x and 4.x allows remote authenticated users to cause a denial of service (resource consumption) via unknown vectors.
CVE-2008-6576
Unspecified vulnerability in the "session limitation technique" in the FTP service on Nortel Communications Server 1000 (CS1K) 4.50.x, when running on VGMC or signaling nodes, allows remote attackers to cause a denial of service (resource exhaustion and failed updates) via unknown vectors that causes consumption of all available sessions.
CVE-2008-6577
Nortel MG1000S, Signaling Server, and Call Server on the Communications Server 1000 (CS1K) 4.50.x contain multiple unspecified hard-coded accounts and passwords, which allows remote attackers to gain privileges.
CVE-2008-6578
Multiple unspecified vulnerabilities in Nortel Communication Server 1000 4.50.x allow remote attackers to execute arbitrary commands to gain privileges, obtain sensitive information, or cause a denial of service via unknown vectors.
CVE-2008-6579
Nortel Communication Server 1000 4.50.x allows remote attackers to obtain Web application structure via unknown vectors related to "web resources to phones and administrators."
CVE-2009-0686
The TrendMicro Activity Monitor Module (tmactmon.sys) 2.52.0.1002 in Trend Micro Internet Pro 2008 and 2009, and Security Pro 2008 and 2009, allows local users to gain privileges via a crafted IRP in a METHOD_NEITHER IOCTL request to \Device\tmactmon that overwrites memory.
CVE-2009-0790
The pluto IKE daemon in Openswan and Strongswan IPsec 2.6 before 2.6.21 and 2.4 before 2.4.14, and Strongswan 4.2 before 4.2.14 and 2.8 before 2.8.9, allows remote attackers to cause a denial of service (daemon crash and restart) via a crafted (1) R_U_THERE or (2) R_U_THERE_ACK Dead Peer Detection (DPD) IPsec IKE Notification message that triggers a NULL pointer dereference related to inconsistent ISAKMP state and the lack of a phase2 state association in DPD.
CVE-2009-1204
Cross-site scripting (XSS) vulnerability in TikiWiki (Tiki) CMS/Groupware 2.2 allows remote attackers to inject arbitrary web script or HTML via the PHP_SELF portion of a URI to (1) tiki-galleries.php, (2) tiki-list_file_gallery.php, (3) tiki-listpages.php, and (4) tiki-orphan_pages.php.
CVE-2009-1205
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-4475.  Reason: This candidate is a duplicate of CVE-2007-4475.  Notes: All CVE users should reference CVE-2007-4475 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2009-1206
Unspecified vulnerability in futomi's CGI Cafe Access Analyzer CGI Professional Version 4.11.5 and earlier allows remote attackers to gain administrative privileges via unknown vectors.
CVE-2009-1207
Race condition in the dircmp script in Sun Solaris 8 through 10, and OpenSolaris snv_01 through snv_111, allows local users to overwrite arbitrary files, probably involving a symlink attack on temporary files.
CVE-2009-1208
SQL injection vulnerability in auth2db 0.2.5, and possibly other versions before 0.2.7, uses the addslashes function instead of the mysql_real_escape_string function, which allows remote attackers to conduct SQL injection attacks using multibyte character encodings.
CVE-2009-1209
Stack-based buffer overflow in W3C Amaya Web Browser 11.1 allows remote attackers to execute arbitrary code via a script tag with a long defer attribute.
CVE-2009-1210
Format string vulnerability in the PROFINET/DCP (PN-DCP) dissector in Wireshark 1.0.6 and earlier allows remote attackers to execute arbitrary code via a PN-DCP packet with format string specifiers in the station name.  NOTE: some of these details are obtained from third party information.
CVE-2009-1211
Blue Coat ProxySG, when transparent interception mode is enabled, uses the HTTP Host header to determine the remote endpoint, which allows remote attackers to bypass access controls for Flash, Java, Silverlight, and probably other technologies, and possibly communicate with restricted intranet sites, via a crafted web page that causes a client to send HTTP requests with a modified Host header.
CVE-2009-1212
Multiple insecure method vulnerabilities in PRECIS~2.DLL in the PrecisionID Datamatrix ActiveX control (DMATRIXLib.Datamatrix) allow remote attackers to overwrite arbitrary files via the (1) SaveBarCode and (2) SaveEnhWMF methods.
CVE-2009-1213
Cross-site request forgery (CSRF) vulnerability in attachment.cgi in Bugzilla 3.2 before 3.2.3, 3.3 before 3.3.4, and earlier versions allows remote attackers to hijack the authentication of arbitrary users for requests that use attachment editing.
CVE-2009-1214
GNU screen 4.0.3 creates the /tmp/screen-exchange temporary file with world-readable permissions, which might allow local users to obtain sensitive session information.
CVE-2009-1215
Race condition in GNU screen 4.0.3 allows local users to create or overwrite arbitrary files via a symlink attack on the /tmp/screen-exchange temporary file.
CVE-2009-1216
Multiple unspecified vulnerabilities in (1) unlzh.c and (2) unpack.c in the gzip libraries in Microsoft Windows Server 2008, Windows Services for UNIX 3.0 and 3.5, and the Subsystem for UNIX-based Applications (SUA); as used in gunzip, gzip, pack, pcat, and unpack 7.x before 7.0.1701.48, 8.x before 8.0.1969.62, and 9.x before 9.0.3790.2076; allow remote attackers to execute arbitrary code via unknown vectors.
CVE-2009-1217
Off-by-one error in the GpFont::SetData function in gdiplus.dll in Microsoft GDI+ on Windows XP allows remote attackers to cause a denial of service (stack corruption and application termination) via a crafted EMF file that triggers an integer overflow, as demonstrated by voltage-exploit.emf, aka the "Microsoft GdiPlus EMF GpFont.SetData integer overflow."
CVE-2009-1218
Multiple cross-site scripting (XSS) vulnerabilities in Sun Calendar Express Web Server in Sun ONE Calendar Server 6.0 and Sun Java System Calendar Server 6 2004Q2 through 6.3-7.01 allow remote attackers to inject arbitrary web script or HTML via (1) the fmt-out parameter to login.wcap or (2) the date parameter to command.shtml.
CVE-2009-1219
Sun Calendar Express Web Server in Sun ONE Calendar Server 6.0 and Sun Java System Calendar Server 6 2004Q2 through 6.3-7.01 allows remote attackers to cause a denial of service (daemon crash) via multiple requests to the default URI with alphabetic characters in the tzid parameter.
CVE-2009-1220
Cross-site scripting (XSS) vulnerability in +webvpn+/index.html in WebVPN on the Cisco Adaptive Security Appliances (ASA) 5520 with software 7.2(4)30 and earlier 7.2 versions including 7.2(2)22, and 8.0(4)28 and earlier 8.0 versions, when clientless mode is enabled, allows remote attackers to inject arbitrary web script or HTML via the Host HTTP header.
