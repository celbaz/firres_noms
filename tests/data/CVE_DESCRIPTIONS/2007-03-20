CVE-2007-0607
W-Agora (Web-Agora) 4.2.1, when register_globals is enabled, stores globals.inc under the web document root with insufficient access control, which allows remote attackers to obtain application path information via a direct request.
CVE-2007-0998
The VNC server implementation in QEMU, as used by Xen and possibly other environments, allows local users of a guest operating system to read arbitrary files on the host operating system via unspecified vectors related to QEMU monitor mode, as demonstrated by mapping files to a CDROM device.  NOTE: some of these details are obtained from third party information.
CVE-2007-1507
The default configuration in OpenAFS 1.4.x before 1.4.4 and 1.5.x before 1.5.17 supports setuid programs within the local cell, which might allow attackers to gain privileges by spoofing a response to an AFS cache manager FetchStatus request, and setting setuid and root ownership for files in the cache.
CVE-2007-1508
Cross-site scripting (XSS) vulnerability in CMD_USER_STATS in DirectAdmin allows remote attackers to inject arbitrary web script or HTML via the RESULT parameter, a different vector than CVE-2006-5983.
CVE-2007-1509
Directory traversal vulnerability in enkrypt.php in Sascha Schroeder krypt (aka Holtstraeter Rot 13) allows remote attackers to read arbitrary files via a .. (dot dot) in the datei parameter.
CVE-2007-1510
SQL injection vulnerability in post.php in Particle Blogger 1.0.0 through 1.2.0 allows remote attackers to execute arbitrary SQL commands via the postid parameter.
CVE-2007-1511
Buffer overflow in FrontBase Relational Database Server 4.2.7 and earlier allows remote authenticated users, with privileges for creating a stored procedure, to execute arbitrary code via a CREATE PROCEDURE request with a long procedure name.
CVE-2007-1512
Stack-based buffer overflow in the AfxOleSetEditMenu function in the MFC component in Microsoft Windows 2000 SP4, XP SP2, and Server 2003 Gold and SP1, and Visual Studio .NET 2002 Gold and SP1, and 2003 Gold and SP1 allows user-assisted remote attackers to have an unknown impact (probably crash) via an RTF file with a malformed OLE object, which results in writing two 0x00 characters past the end of szBuffer, aka the "MFC42u.dll Off-by-Two Overflow." NOTE: this issue is due to an incomplete patch (MS07-012) for CVE-2007-0025.
CVE-2007-1513
PHP remote file inclusion vulnerability in comanda.php in GraFX Company WebSite Builder (CWB) PRO 1.9.8, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the INCLUDE_PATH parameter.
CVE-2007-1514
PHP remote file inclusion vulnerability in index.php in ViperWeb Portal alpha 0.1 allows remote attackers to execute arbitrary PHP code via a URL in the modpath parameter.
CVE-2007-1515
Multiple cross-site scripting (XSS) vulnerabilities in Horde IMP H3 4.1.3, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via (1) the email Subject header in thread.php, (2) the edit_query parameter in search.php, or other unspecified parameters in search.php.  NOTE: some of these details are obtained from third party information.
CVE-2007-1516
PHP remote file inclusion vulnerability in functions/update.php in Cicoandcico CcMail 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the functions_dir parameter.
CVE-2007-1517
SQL injection vulnerability in comments.php in WSN Guest 1.02 and 1.21 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-1518
SQL injection vulnerability in usergroups.php in Woltlab Burning Board (wBB) 2.x allows remote attackers to execute arbitrary SQL commands via the array index of the applicationids array.
CVE-2007-1519
Cross-site scripting (XSS) vulnerability in modules.php in PHP-Nuke 8.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the query parameter in a search operation in the Downloads module, a different product than CVE-2006-3948.
CVE-2007-1520
The cross-site request forgery (CSRF) protection in PHP-Nuke 8.0 and earlier does not ensure the SERVER superglobal is an array before validating the HTTP_REFERER, which allows remote attackers to conduct CSRF attacks.
CVE-2007-1521
Double free vulnerability in PHP before 4.4.7, and 5.x before 5.2.2, allows context-dependent attackers to execute arbitrary code by interrupting the session_regenerate_id function, as demonstrated by calling a userspace error handler or triggering a memory limit violation.
CVE-2007-1522
Double free vulnerability in the session extension in PHP 5.2.0 and 5.2.1 allows context-dependent attackers to execute arbitrary code via illegal characters in a session identifier, which is rejected by an internal session storage module, which calls the session identifier generator with an improper environment, leading to code execution when the generator is interrupted, as demonstrated by triggering a memory limit violation or certain PHP errors.
CVE-2007-1523
Heap-based buffer overflow in the kernel in NetBSD 3.0, certain versions of FreeBSD and OpenBSD, and possibly other BSD derived operating systems allows local users to have an unknown impact.  NOTE: this information is based upon a vague pre-advisory with no actionable information. Details will be updated after 20070329.
CVE-2007-1524
Directory traversal vulnerability in themes/default/ in ZomPlog 3.7.6 and earlier allows remote attackers to include arbitrary local files via a .. (dot dot) in the settings[skin] parameter, as demonstrated by injecting PHP code into an Apache HTTP Server log file, which can then be included via themes/default/.
CVE-2007-1525
Direct static code injection vulnerability in postpost.php in Dayfox Blog (dfblog) 4 allows remote attackers to execute arbitrary PHP code via the cat parameter, which can be executed via a request to posts.php.
CVE-2007-1526
Sun Java System Web Server 6.1 before 20070314 allows remote authenticated users with revoked client certificates to bypass the Certificate Revocation List (CRL) authorization control and access secure web server instances running under an account different from that used for the admin server via unspecified vectors.
CVE-2007-1527
The LLTD Mapper in Microsoft Windows Vista does not verify that an IP address in a TLV type 0x07 field in a HELLO packet corresponds to a valid IP address for the local network, which allows remote attackers to trick users into communicating with an external host by sending a HELLO packet with the MW characteristic and a spoofed TLV type 0x07 field, aka the "Spoof and Management URL IP Redirect" attack.
CVE-2007-1528
The LLTD Mapper in Microsoft Windows Vista allows remote attackers to spoof hosts, and nonexistent bridge relationships, into the network topology map by using a MAC address that differs from the MAC address provided in the Real Source field of the LLTD BASE header of a HELLO packet, aka the "Spoof on Bridge" attack.
CVE-2007-1529
The LLTD Responder in Microsoft Windows Vista does not send the Mapper a response to a DISCOVERY packet if another host has sent a spoofed response first, which allows remote attackers to spoof arbitrary hosts via a network-based race condition, aka the "Total Spoof" attack.
CVE-2007-1530
The LLTD Mapper in Microsoft Windows Vista does not properly gather responses to EMIT packets, which allows remote attackers to cause a denial of service (mapping failure) by omitting an ACK response, which triggers an XML syntax error.
CVE-2007-1531
Microsoft Windows XP and Vista overwrites ARP table entries included in gratuitous ARP, which allows remote attackers to cause a denial of service (loss of network access) by sending a gratuitous ARP for the address of the Vista host.
CVE-2007-1532
The neighbor discovery implementation in Microsoft Windows Vista allows remote attackers to conduct a redirect attack by (1) responding to queries by sending spoofed Neighbor Advertisements or (2) blindly sending Neighbor Advertisements.
CVE-2007-1533
The Teredo implementation in Microsoft Windows Vista uses the same nonce for communication with different UDP ports within a solicitation session, which makes it easier for remote attackers to spoof the nonce through brute force attacks.
CVE-2007-1534
DFSR.exe in Windows Meeting Space in Microsoft Windows Vista remains available for remote connections on TCP port 5722 for 2 minutes after Windows Meeting Space is closed, which allows remote attackers to have an unknown impact by connecting to this port during the time window.
CVE-2007-1535
Microsoft Windows Vista establishes a Teredo address without user action upon connection to the Internet, contrary to documentation that Teredo is inactive without user action, which increases the attack surface and allows remote attackers to communicate via Teredo.
CVE-2007-1536
Integer underflow in the file_printf function in the "file" program before 4.20 allows user-assisted attackers to execute arbitrary code via a file that triggers a heap-based buffer overflow.
CVE-2007-1537
\Device\NdisTapi (NDISTAPI.sys) in Microsoft Windows XP SP2 and 2003 SP1 uses weak permissions, which allows local users to write to the device and cause a denial of service, as demonstrated by using an IRQL to acquire a spinlock on paged memory via the NdisTapiDispatch function.
CVE-2007-1538
** DISPUTED **  McAfee VirusScan Enterprise 8.5.0.i uses insecure permissions for certain Windows Registry keys, which allows local users to bypass local password protection via the UIP value in (1) HKEY_LOCAL_MACHINE\SOFTWARE\McAfee\DesktopProtection or (2) HKEY_LOCAL_MACHINE\SOFTWARE\Network Associates\TVD\VirusScan Entreprise\CurrentVersion.  NOTE: this issue has been disputed by third-party researchers, stating that the default permissions for HKEY_LOCAL_MACHINE\SOFTWARE does not allow for write access and the product does not modify the inherited permissions. There might be an interaction error with another product.
CVE-2007-1539
Directory traversal vulnerability in inc/map.func.php in pragmaMX Landkarten 2.1 module allows remote attackers to include arbitrary files via a .. (dot dot) sequence in the module_name parameter, as demonstrated via a static PHP code injection attack in an Apache log file.
CVE-2007-1540
Directory traversal vulnerability in am.pl in (1) SQL-Ledger 2.6.27 and earlier, and (2) LedgerSMB before 1.2.0, allows remote attackers to run arbitrary executables and bypass authentication via a .. (dot dot) sequence and trailing NULL (%00) in the login parameter.  NOTE: this issue was reportedly addressed in SQL-Ledger 2.6.27, however third-party researchers claim that the file is still executed even though an error is generated.
CVE-2007-1541
Directory traversal vulnerability in am.pl in SQL-Ledger 2.6.27 only checks for the presence of a NULL (%00) character to protect against directory traversal attacks, which allows remote attackers to run arbitrary executables and bypass authentication via a .. (dot dot) sequence in the login parameter.
CVE-2007-1542
Unspecified vulnerability in the Cisco IP Phone 7940 and 7960 running firmware before POS8-6-0 allows remote attackers to cause a denial of service via the Remote-Party-ID sipURI field in a SIP INVITE request. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1543
Stack-based buffer overflow in the accept_att_local function in server/os/connection.c in Network Audio System (NAS) before 1.8a SVN 237 allows remote attackers to execute arbitrary code via a long path slave name in a USL socket connection.
CVE-2007-1544
Integer overflow in the ProcAuWriteElement function in server/dia/audispatch.c in Network Audio System (NAS) before 1.8a SVN 237 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large max_samples value.
CVE-2007-1545
The AddResource function in server/dia/resource.c in Network Audio System (NAS) before 1.8a SVN 237 allows remote attackers to cause a denial of service (server crash) via a nonexistent client ID.
CVE-2007-1546
Array index error in Network Audio System (NAS) before 1.8a SVN 237 allows remote attackers to cause a denial of service (crash) via (1) large num_action values in the ProcAuSetElements function in server/dia/audispatch.c or (2) a large inputNum parameter to the compileInputs function in server/dia/auutil.c.
CVE-2007-1547
The ReadRequestFromClient function in server/os/io.c in Network Audio System (NAS) before 1.8a SVN 237 allows remote attackers to cause a denial of service (crash) via multiple simultaneous connections, which triggers a NULL pointer dereference.
CVE-2007-1548
SQL injection vulnerability in functions/functions_filters.asp in Web Wiz Forums before 8.05a (MySQL version) does not properly filter certain characters in SQL commands, which allows remote attackers to execute arbitrary SQL commands via \"' (backslash double-quote quote) sequences, which are collapsed into \'', as demonstrated via the name parameter to forum/pop_up_member_search.asp.
CVE-2007-1549
Unrestricted file upload vulnerability in gallery.php in phpx 3.5.15 allows remote attackers to upload and execute arbitrary PHP scripts via an addImage action, which places scripts into the gallery/shelties/ directory.
CVE-2007-1550
Multiple SQL injection vulnerabilities in phpx 3.5.15 allow remote attackers to execute arbitrary SQL commands via the (1) image_id or (2) cat_id parameter to (a) gallery.php; the (3) news_id parameter to (b) news.php or (c) print.php; (4) the news_cat_id parameter to news.php; the (5) cat_id, (6) topic_id, or (7) post_id parameter to (d) forums.php; or (8) the user_id parameter to (e) users.php.
CVE-2007-1551
Multiple cross-site scripting (XSS) vulnerabilities in phpx 3.5.15 allow remote attackers to inject arbitrary web script or HTML via (1) the signature in "dans profile," or (2) search.php.
CVE-2007-1552
Unrestricted file upload vulnerability in usercp.php in MetaForum 0.513 Beta restricts file types based on the MIME type in the Content-type HTTP header, which allows remote attackers to upload and execute arbitrary scripts via an image MIME type with a filename containing an executable extension such as .php.
CVE-2007-1553
admin/configuration.php in Guestbara 1.2 and earlier allows remote attackers to modify the e-mail, name, and password of the admin account by setting the zapis parameter to "ok" and providing modified admin_mail, login, and pass parameters.
CVE-2007-1554
Direct static code injection vulnerability in admin/configuration.php in Guestbara 1.2 and earlier allows remote authenticated users to inject arbitrary PHP code into config.php via the (1) admin_mail, (2) emotpatch, (3) login, (4) pass, and unspecified other parameters. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1555
SQL injection vulnerability in forum.php in the Minerva mod 2.0.21 build 238a and earlier for phpBB allows remote attackers to execute arbitrary SQL commands via the c parameter.
