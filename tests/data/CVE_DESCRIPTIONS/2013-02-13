CVE-2012-3280
Multiple unspecified vulnerabilities on HP NonStop Servers H06.x and J06.x allow remote authenticated users to obtain sensitive information, modify data, or cause a denial of service via an OSS Remote Operation over an Expand connection.
https://h20566.www2.hp.com/portal/site/hpsc/template.PAGE/public/kb/docDisplay/?spf_p.tpst=kbDocDisplay&spf_p.prp_kbDocDisplay=wsrp-navigationalState%3DdocId%253Demr_na-c03654586-2%257CdocLocale%253D%257CcalledBy%253D&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken
CVE-2012-3363
Zend_XmlRpc in Zend Framework 1.x before 1.11.12 and 1.12.x before 1.12.0 does not properly handle SimpleXMLElement classes, which allows remote attackers to read arbitrary files or create TCP connections via an external entity reference in a DOCTYPE element in an XML-RPC request, aka an XML external entity (XXE) injection attack.
CVE-2012-6075
Buffer overflow in the e1000_receive function in the e1000 device driver (hw/e1000.c) in QEMU 1.3.0-rc2 and other versions, when the SBP and LPE flags are disabled, allows remote attackers to cause a denial of service (guest OS crash) and possibly execute arbitrary guest code via a large packet.
CVE-2012-6531
(1) Zend_Dom, (2) Zend_Feed, and (3) Zend_Soap in Zend Framework 1.x before 1.11.13 and 1.12.x before 1.12.0 do not properly handle SimpleXMLElement classes, which allow remote attackers to read arbitrary files or create TCP connections via an external entity reference in a DOCTYPE element in an XML-RPC request, aka an XML external entity (XXE) injection attack, a different vulnerability than CVE-2012-3363.
CVE-2012-6532
(1) Zend_Dom, (2) Zend_Feed, (3) Zend_Soap, and (4) Zend_XmlRpc in Zend Framework 1.x before 1.11.13 and 1.12.x before 1.12.0 allow remote attackers to cause a denial of service (CPU consumption) via recursive or circular references in an XML entity definition in an XML DOCTYPE declaration, aka an XML Entity Expansion (XEE) attack.
CVE-2013-0015
Microsoft Internet Explorer 6 through 9 does not properly perform auto-selection of the Shift JIS encoding, which allows remote attackers to read content from a different (1) domain or (2) zone via a crafted web site that triggers cross-domain scrolling events, aka "Shift JIS Character Encoding Vulnerability."
CVE-2013-0018
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer SetCapture Use After Free Vulnerability."
CVE-2013-0019
Use-after-free vulnerability in Microsoft Internet Explorer 7 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer COmWindowProxy Use After Free Vulnerability."
CVE-2013-0020
Use-after-free vulnerability in Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer CMarkup Use After Free Vulnerability."
CVE-2013-0021
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer vtable Use After Free Vulnerability."
CVE-2013-0022
Use-after-free vulnerability in Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer LsGetTrailInfo Use After Free Vulnerability."
CVE-2013-0023
Use-after-free vulnerability in Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer CDispNode Use After Free Vulnerability."
CVE-2013-0024
Use-after-free vulnerability in Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer pasteHTML Use After Free Vulnerability."
CVE-2013-0025
Use-after-free vulnerability in Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer SLayoutRun Use After Free Vulnerability."
CVE-2013-0026
Use-after-free vulnerability in Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer InsertElement Use After Free Vulnerability."
CVE-2013-0027
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer CPasteCommand Use After Free Vulnerability."
CVE-2013-0028
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer CObjectElement Use After Free Vulnerability."
CVE-2013-0029
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer CHTML Use After Free Vulnerability."
CVE-2013-0030
The Vector Markup Language (VML) implementation in Microsoft Internet Explorer 6 through 10 does not properly allocate buffers, which allows remote attackers to execute arbitrary code via a crafted web site, aka "VML Memory Corruption Vulnerability."
CVE-2013-0073
The Windows Forms (aka WinForms) component in Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly restrict the privileges of a callback function during object creation, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application, aka "WinForms Callback Elevation Vulnerability."
CVE-2013-0075
The TCP/IP implementation in Microsoft Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows remote attackers to cause a denial of service (reboot) via a crafted packet that terminates a TCP connection, aka "TCP FIN WAIT Vulnerability."
CVE-2013-0076
The Client/Server Run-time Subsystem (CSRSS) in Microsoft Windows Server 2008 R2 and R2 SP1 and Windows 7 Gold and SP1 does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Reference Count Vulnerability."
CVE-2013-0077
Quartz.dll in DirectShow in Microsoft Windows XP SP2 and SP3, Server 2003 SP2, Vista SP2, and Server 2008 SP2 allows remote attackers to execute arbitrary code via crafted media content in (1) a media file, (2) a media stream, or (3) a Microsoft Office document, aka "Media Decompression Vulnerability."
CVE-2013-0152
Memory leak in Xen 4.2 and unstable allows local HVM guests to cause a denial of service (host memory consumption) by performing nested virtualization in a way that triggers errors that are not properly handled.
CVE-2013-0190
The xen_failsafe_callback function in Xen for the Linux kernel 2.6.23 and other versions, when running a 32-bit PVOPS guest, allows local users to cause a denial of service (guest crash) by triggering an iret fault, leading to use of an incorrect stack pointer and stack corruption.
CVE-2013-0208
The boot-from-volume feature in OpenStack Compute (Nova) Folsom and Essex, when using nova-volumes, allows remote authenticated users to boot from other users' volumes via a volume id in the block_device_mapping parameter.
Per http://www.ubuntu.com/usn/USN-1709-1/
A security issue affects these releases of Ubuntu and its derivatives:
Ubuntu 12.10
Ubuntu 12.04 LTS
Ubuntu 11.10

CVE-2013-0231
The pciback_enable_msi function in the PCI backend driver (drivers/xen/pciback/conf_space_capability_msi.c) in Xen for the Linux kernel 2.6.18 and 3.8 allows guest OS users with PCI device access to cause a denial of service via a large number of kernel log messages. NOTE: some of these details are obtained from third party information.
CVE-2013-0238
The try_parse_v4_netmask function in hostmask.c in IRCD-Hybrid before 8.0.6 does not properly validate masks, which allows remote attackers to cause a denial of service (crash) via a mask that causes a negative number to be parsed.
CVE-2013-0241
The QXL display driver in QXL Virtual GPU 0.1.0 allows local users to cause a denial of service (guest crash or hang) via a SPICE connection that prevents other threads from obtaining the qemu_mutex mutex.  NOTE: some of these details are obtained from third party information.
Per https://rhn.redhat.com/errata/RHSA-2013-0218.html
Affected Products: 	
Red Hat Enterprise Linux Desktop (v. 6)
Red Hat Enterprise Linux Server (v. 6)
Red Hat Enterprise Linux Workstation (v. 6)

Per http://www.ubuntu.com/usn/USN-1714-1/
A security issue affects these releases of Ubuntu and its derivatives:
Ubuntu 12.04 LTS
Ubuntu 11.10

CVE-2013-0255
PostgreSQL 9.2.x before 9.2.3, 9.1.x before 9.1.8, 9.0.x before 9.0.12, 8.4.x before 8.4.16, and 8.3.x before 8.3.23 does not properly declare the enum_recv function in backend/utils/adt/enum.c, which causes it to be invoked with incorrect arguments and allows remote authenticated users to cause a denial of service (server crash) or read sensitive process memory via a crafted SQL command, which triggers an array index error and an out-of-bounds read.
CVE-2013-0265
The redirect_stderr function in xnbd_common.c in xnbd-server and xndb-wrapper in xNBD 0.1.0 allow local users to overwrite arbitrary files via a symlink attack on /tmp/xnbd.log.
CVE-2013-0269
The JSON gem before 1.5.5, 1.6.x before 1.6.8, and 1.7.x before 1.7.7 for Ruby allows remote attackers to cause a denial of service (resource consumption) or bypass the mass assignment protection mechanism via a crafted JSON document that triggers the creation of arbitrary Ruby symbols or certain internal objects, as demonstrated by conducting a SQL injection attack against Ruby on Rails, aka "Unsafe Object Creation Vulnerability."
CVE-2013-0276
ActiveRecord in Ruby on Rails before 2.3.17, 3.1.x before 3.1.11, and 3.2.x before 3.2.12 allows remote attackers to bypass the attr_protected protection mechanism and modify protected model attributes via a crafted request.
CVE-2013-0277
ActiveRecord in Ruby on Rails before 2.3.17 and 3.x before 3.1.0 allows remote attackers to cause a denial of service or execute arbitrary code via crafted serialized attributes that cause the +serialize+ helper to deserialize arbitrary YAML.
CVE-2013-0635
Adobe Shockwave Player before 12.0.0.112 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2013-0636
Stack-based buffer overflow in Adobe Shockwave Player before 12.0.0.112 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2013-1100
The HTTP server in Cisco IOS on Catalyst switches does not properly handle TCP socket events, which allows remote attackers to cause a denial of service (device crash) via crafted packets on TCP port (1) 80 or (2) 443, aka Bug ID CSCuc53853.
CVE-2013-1111
The Cisco ATA 187 Analog Telephone Adaptor with firmware 9.2.1.0 and 9.2.3.1 before ES build 4 does not properly implement access control, which allows remote attackers to execute operating-system commands via vectors involving a session on TCP port 7870, aka Bug ID CSCtz67038.
CVE-2013-1114
Multiple cross-site scripting (XSS) vulnerabilities in Cisco Unity Express before 8.0 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka Bug ID CSCud87527.
CVE-2013-1122
Cisco NX-OS on the Nexus 7000, when a certain Overlay Transport Virtualization (OTV) configuration is used, allows remote attackers to cause a denial of service (M1-Series module reload) via crafted packets, aka Bug ID CSCud15673.
CVE-2013-1131
Cisco Small Business Wireless Access Points WAP200, WAP2000, WAP200E, and WET200 allow remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted SSID that is not properly handled during a site survey, aka Bug IDs CSCua86182, CSCua91196, CSCud36155, and CSCua86190.
CVE-2013-1248
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1249
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1250
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1251
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1252
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1253
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1254
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1255
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1256
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1257
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1258
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1259
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1260
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1261
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1262
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1263
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1264
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1265
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1266
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1267
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1268
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1269
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1270
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1271
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1272
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1273
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1274
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1275
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1276
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1277
Race condition in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges, and consequently read the contents of arbitrary kernel memory locations, via a crafted application, a different vulnerability than other CVEs listed in MS13-016.
CVE-2013-1278
Race condition in the kernel in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges via a crafted application that leverages incorrect handling of objects in memory, aka "Kernel Race Condition Vulnerability," a different vulnerability than CVE-2013-1279.
CVE-2013-1279
Race condition in the kernel in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges via a crafted application that leverages incorrect handling of objects in memory, aka "Kernel Race Condition Vulnerability," a different vulnerability than CVE-2013-1278.
CVE-2013-1280
The kernel in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Windows Kernel Reference Count Vulnerability."
CVE-2013-1281
The NFS server in Microsoft Windows Server 2008 R2 and R2 SP1 and Server 2012 allows remote attackers to cause a denial of service (NULL pointer dereference and reboot) via an attempted renaming of a file or folder located on a read-only share, aka "NULL Dereference Vulnerability."
CVE-2013-1313
Object Linking and Embedding (OLE) Automation in Microsoft Windows XP SP3 does not properly allocate memory, which allows remote attackers to execute arbitrary code via a crafted RTF document, aka "OLE Automation Remote Code Execution Vulnerability."
CVE-2013-1453
plugins/system/highlight/highlight.php in Joomla! 3.0.x through 3.0.2 and 2.5.x through 2.5.8 allows attackers to unserialize arbitrary PHP objects to obtain sensitive information, delete arbitrary directories, conduct SQL injection attacks, and possibly have other impacts via the highlight parameter.  Note: it was originally reported that this issue only allowed attackers to obtain sensitive information, but later analysis demonstrated that other attacks exist.
CVE-2013-1454
Joomla! 3.0.x through 3.0.2 allows attackers to obtain sensitive information via unspecified vectors related to "Coding errors."
CVE-2013-1455
Joomla! 3.0.x through 3.0.2 allows attackers to obtain sensitive information via unspecified vectors related to an "Undefined variable."
