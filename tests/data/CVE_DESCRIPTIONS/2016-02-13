CVE-2015-8629
The xdr_nullstring function in lib/kadm5/kadm_rpc_xdr.c in kadmind in MIT Kerberos 5 (aka krb5) before 1.13.4 and 1.14.x before 1.14.1 does not verify whether '\0' characters exist as expected, which allows remote authenticated users to obtain sensitive information or cause a denial of service (out-of-bounds read) via a crafted string.
CVE-2015-8630
The (1) kadm5_create_principal_3 and (2) kadm5_modify_principal functions in lib/kadm5/srv/svr_principal.c in kadmind in MIT Kerberos 5 (aka krb5) 1.12.x and 1.13.x before 1.13.4 and 1.14.x before 1.14.1 allow remote authenticated users to cause a denial of service (NULL pointer dereference and daemon crash) by specifying KADM5_POLICY with a NULL policy name.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-8631
Multiple memory leaks in kadmin/server/server_stubs.c in kadmind in MIT Kerberos 5 (aka krb5) before 1.13.4 and 1.14.x before 1.14.1 allow remote authenticated users to cause a denial of service (memory consumption) via a request specifying a NULL principal name.
CVE-2016-0863
Cross-site request forgery (CSRF) vulnerability in Tollgrade SmartGrid LightHouse Sensor Management System (SMS) Software EMS before 5.1, and 4.1.0 Build 16, allows remote attackers to hijack the authentication of arbitrary users.
CVE-2016-0864
Tollgrade SmartGrid LightHouse Sensor Management System (SMS) Software EMS before 5.1, and 4.1.0 Build 16, allows remote attackers to obtain sensitive report and username information via unspecified vectors.
CVE-2016-0865
Tollgrade SmartGrid LightHouse Sensor Management System (SMS) Software EMS before 5.1, and 4.1.0 Build 16, allows remote authenticated users to change arbitrary passwords via unspecified vectors.
CVE-2016-0866
Cross-site scripting (XSS) vulnerability in Tollgrade SmartGrid LightHouse Sensor Management System (SMS) Software EMS before 5.1, and 4.1.0 Build 16, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1521
The directrun function in directmachine.cpp in Libgraphite in Graphite 2 1.2.4, as used in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.6.1, does not validate a certain skip operation, which allows remote attackers to execute arbitrary code, obtain sensitive information, or cause a denial of service (out-of-bounds read and application crash) via a crafted Graphite smart font.
CVE-2016-1522
Code.cpp in Libgraphite in Graphite 2 1.2.4, as used in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.6.1, does not consider recursive load calls during a size check, which allows remote attackers to cause a denial of service (heap-based buffer overflow) or possibly execute arbitrary code via a crafted Graphite smart font.
CVE-2016-1523
The SillMap::readFace function in FeatureMap.cpp in Libgraphite in Graphite 2 1.2.4, as used in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.6.1, mishandles a return value, which allows remote attackers to cause a denial of service (missing initialization, NULL pointer dereference, and application crash) via a crafted Graphite smart font.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-1524
Multiple unrestricted file upload vulnerabilities in NETGEAR Management System NMS300 1.5.0.11 and earlier allow remote attackers to execute arbitrary Java code by using (1) fileUpload.do or (2) lib-1.0/external/flash/fileUpload.do to upload a JSP file, and then accessing it via a direct request for a /null URI.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2016-1525
Directory traversal vulnerability in data/config/image.do in NETGEAR Management System NMS300 1.5.0.11 and earlier allows remote authenticated users to read arbitrary files via a .. (dot dot) in the realName parameter.
CVE-2016-1526
The TtfUtil:LocaLookup function in TtfUtil.cpp in Libgraphite in Graphite 2 1.2.4, as used in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.6.1, incorrectly validates a size value, which allows remote attackers to obtain sensitive information or cause a denial of service (out-of-bounds read and application crash) via a crafted Graphite smart font.
CVE-2016-1949
Mozilla Firefox before 44.0.2 does not properly restrict the interaction between Service Workers and plugins, which allows remote attackers to bypass the Same Origin Policy via a crafted web site that triggers spoofed responses to requests that use NPAPI, as demonstrated by a request for a crossdomain.xml file.
