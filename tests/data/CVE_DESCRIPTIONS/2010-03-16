CVE-2007-6733
The nfs_lock function in fs/nfs/file.c in the Linux kernel 2.6.9 does not properly remove POSIX locks on files that are setgid without group-execute permission, which allows local users to cause a denial of service (BUG and system crash) by locking a file on an NFS filesystem and then changing this file's permissions, a related issue to CVE-2010-0727.
CVE-2010-0397
The xmlrpc extension in PHP 5.3.1 does not properly handle a missing methodName element in the first argument to the xmlrpc_decode_request function, which allows context-dependent attackers to cause a denial of service (NULL pointer dereference and application crash) and possibly have unspecified other impact via a crafted argument.
Per: http://cwe.mitre.org/data/slices/2000.html

Improper Check for Unusual or Exceptional Conditions CWE-754
CVE-2010-0727
The gfs2_lock function in the Linux kernel before 2.6.34-rc1-next-20100312, and the gfs_lock function in the Linux kernel on Red Hat Enterprise Linux (RHEL) 5 and 6, does not properly remove POSIX locks on files that are setgid without group-execute permission, which allows local users to cause a denial of service (BUG and system crash) by locking a file on a (1) GFS or (2) GFS2 filesystem, and then changing this file's permissions.
CVE-2010-0729
A certain Red Hat patch for the Linux kernel in Red Hat Enterprise Linux (RHEL) 4 on the ia64 platform allows local users to use ptrace on an arbitrary process, and consequently gain privileges, via vectors related to a missing ptrace_check_attach call.
CVE-2010-0793
Buffer overflow in BarnOwl before 1.5.1 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted CC: header.
CVE-2010-0963
Cross-site scripting (XSS) vulnerability in index.php in dl Download Ticket Service before 0.7 allows remote attackers to inject arbitrary web script or HTML via the t parameter, related to an invalid ticket ID.  NOTE: some of these details are obtained from third party information.
CVE-2010-0964
SQL injection vulnerability in start.php in Eros Webkatalog allows remote attackers to execute arbitrary SQL commands via the id parameter in a rubrik action.
CVE-2010-0965
Jevci Siparis Formu Scripti stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for siparis.mdb.
CVE-2010-0966
PHP remote file inclusion vulnerability in inc/config.php in deV!L`z Clanportal (DZCP) 1.5.2, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the basePath parameter.
CVE-2010-0967
Multiple directory traversal vulnerabilities in Geekhelps ADMP 1.01, when magic_quotes_gpc is disabled, allow remote attackers to include and execute arbitrary local files via directory traversal sequences in the style parameter to (1) colorvoid/footer.php, (2) default-green/footer.php, (3) default-orange/footer.php, and (4) default/footer.php in themes/.  NOTE: some of these details are obtained from third party information.
CVE-2010-0968
SQL injection vulnerability in bannershow.php in Geekhelps ADMP 1.01 allows remote attackers to execute arbitrary SQL commands via the click parameter.
CVE-2010-0969
Unbound before 1.4.3 does not properly align structures on 64-bit platforms, which allows remote attackers to cause a denial of service (daemon crash) via unspecified vectors.
CVE-2010-0970
SQL injection vulnerability in phpmylogon.php in PhpMyLogon 2 allows remote attackers to execute arbitrary SQL commands via the username parameter.  NOTE: some of these details are obtained from third party information.
CVE-2010-0971
Multiple cross-site scripting (XSS) vulnerabilities in ATutor 1.6.4 allow remote authenticated users, with Instructor privileges, to inject arbitrary web script or HTML via the (1) Question and (2) Choice fields in tools/polls/add.php, the (3) Type and (4) Title fields in tools/groups/create_manual.php, and the (5) Title field in assignments/add_assignment.php.  NOTE: some of these details are obtained from third party information.
CVE-2010-0972
Directory traversal vulnerability in the GCalendar (com_gcalendar) component 2.1.5 for Joomla! allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-0973
SQL injection vulnerability in index.php in phppool media Domain Verkaus and Auktions Portal allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0974
Multiple SQL injection vulnerabilities in PHPCityPortal allow remote attackers to execute arbitrary SQL commands via the id parameter to (1) video_show.php, (2) spotlight_detail.php, (3) real_estate_details.php, and (4) auto_details.php.
CVE-2010-0975
PHP remote file inclusion vulnerability in external.php in PHPCityPortal allows remote attackers to execute arbitrary PHP code via a URL in the url parameter.
CVE-2010-0976
Acidcat CMS 3.5.x does not prevent access to install.asp after installation finishes, which might allow remote attackers to restart the installation process and have unspecified other impact via requests to install.asp and other install_*.asp scripts.  NOTE: the final installation screen states "Important: you must now delete all files beginning with 'install' from the root directory."
CVE-2010-0977
PD PORTAL 4.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for db/db.mdb.
CVE-2010-0978
KMSoft Guestbook (aka GBook) 1.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for db/db.mdb.
CVE-2010-0979
Cross-site scripting (XSS) vulnerability in display.php in Obsession-Design Image-Gallery (ODIG) 1.1 allows remote attackers to inject arbitrary web script or HTML via the folder parameter.
CVE-2010-0980
SQL injection vulnerability in player.php in Left 4 Dead (L4D) Stats 1.1 allows remote attackers to execute arbitrary SQL commands via the steamid parameter.
CVE-2010-0981
SQL injection vulnerability in the TPJobs (com_tpjobs) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the id_c[] parameter in a resadvsearch action to index.php.
CVE-2010-0982
Directory traversal vulnerability in the CARTwebERP (com_cartweberp) component 1.56.75 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-0983
PHP remote file inclusion vulnerability in include/mail.inc.php in Rezervi 3.0.2 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the root parameter, a different vector than CVE-2007-2156.
CVE-2010-0984
Acidcat CMS 3.5.3 and earlier stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing credentials via a direct request for databases/acidcat_3.mdb.
CVE-2010-0985
Directory traversal vulnerability in the Abbreviations Manager (com_abbrev) component 1.1 for Joomla! allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the controller parameter to index.php.  NOTE: some of these details are obtained from third party information.
