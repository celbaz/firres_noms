CVE-2011-4971
Multiple integer signedness errors in the (1) process_bin_sasl_auth, (2) process_bin_complete_sasl_auth, (3) process_bin_update, and (4) process_bin_append_prepend functions in Memcached 1.4.5 and earlier allow remote attackers to cause a denial of service (crash) via a large body length value in a packet.
CVE-2013-1447
OpenJPEG 1.3 and earlier allows remote attackers to cause a denial of service (memory consumption or crash) via unspecified vectors related to NULL pointer dereferences, division-by-zero, and other errors.
CVE-2013-1812
The ruby-openid gem before 2.2.2 for Ruby allows remote OpenID providers to cause a denial of service (CPU consumption) via (1) a large XRDS document or (2) an XML Entity Expansion (XEE) attack.
CVE-2013-1913
Integer overflow in the load_image function in file-xwd.c in the X Window Dump (XWD) plug-in in GIMP 2.6.9 and earlier, when used with glib before 2.24, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large color entries value in an X Window System (XWD) image dump.
CVE-2013-1978
Heap-based buffer overflow in the read_xwd_cols function in file-xwd.c in the X Window Dump (XWD) plug-in in GIMP 2.6.9 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via an X Window System (XWD) image dump with more colors than color map entries.
CVE-2013-2751
Eval injection vulnerability in frontview/lib/np_handler.pl in the FrontView web interface in NETGEAR ReadyNAS RAIDiator before 4.1.12 and 4.2.x before 4.2.24 allows remote attackers to execute arbitrary Perl code via a crafted request, related to the "forgot password workflow."
CVE-2013-2752
Cross-site request forgery (CSRF) vulnerability in frontview/lib/np_handler.pl in NETGEAR ReadyNAS RAIDiator before 4.1.12 and 4.2.x before 4.2.24 allows remote attackers to hijack the authentication of users.
CVE-2013-4458
Stack-based buffer overflow in the getaddrinfo function in sysdeps/posix/getaddrinfo.c in GNU C Library (aka glibc or libc6) 2.18 and earlier allows remote attackers to cause a denial of service (crash) via a (1) hostname or (2) IP address that triggers a large number of AF_INET6 address results.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-1914.
CVE-2013-4566
mod_nss 1.0.8 and earlier, when NSSVerifyClient is set to none for the server/vhost context, does not enforce the NSSVerifyClient setting in the directory context, which allows remote attackers to bypass intended access restrictions.
CVE-2013-5763
Unspecified vulnerability in the Oracle Outside In Technology component in Oracle Fusion Middleware 8.4.0 allows context-dependent attackers to affect availability via unknown vectors related to Outside In Maintenance.  NOTE: the original disclosure of this issue erroneously mapped it to CVE-2013-3624.
Per: http://www.oracle.com/technetwork/topics/security/cpuoct2013-1899837.html

"Outside In Technology is a suite of software development kits (SDKs). It does not have any particular associated protocol. If the hosting software passes data received over the network to Outside In Technology code, the CVSS Base Score would increase to 6.8."
CVE-2013-6045
Multiple heap-based buffer overflows in OpenJPEG 1.3 and earlier might allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2013-6052
OpenJPEG 1.3 and earlier allows remote attackers to obtain sensitive information via unspecified vectors that trigger a heap-based out-of-bounds read.
CVE-2013-6054
Heap-based buffer overflow in OpenJPEG 1.3 has unspecified impact and remote vectors, a different vulnerability than CVE-2013-6045.
CVE-2013-6421
The unpack_zip function in archive_unpacker.rb in the sprout gem 0.7.246 for Ruby allows context-dependent attackers to execute arbitrary commands via shell metacharacters in a (1) filename or (2) path.
CVE-2013-6810
The server in Brocade Network Advisor before 12.1.0, as used in EMC Connectrix Manager Converged Network Edition (CMCNE), HP B-series SAN Network Advisor, and possibly other products, allows remote attackers to execute arbitrary code by using a servlet to upload an executable file.
CVE-2013-6986
The ZippyYum Subway CA Kiosk app 3.4 for iOS uses cleartext storage in SQLite cache databases, which allows attackers to obtain sensitive information by reading data elements, as demonstrated by password elements.
CVE-2013-7030
** DISPUTED ** The TFTP service in Cisco Unified Communications Manager (aka CUCM or Unified CM) allows remote attackers to obtain sensitive information from a phone via an RRQ operation, as demonstrated by discovering a cleartext UseUserCredential field in an SPDefault.cnf.xml file.  NOTE: the vendor reportedly disputes the significance of this report, stating that this is an expected default behavior, and that the product's documentation describes use of the TFTP Encrypted Config option in addressing this issue.
