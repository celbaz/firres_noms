CVE-2008-7203
Valve Software Half-Life Counter-Strike 1.6 allows remote attackers to cause a denial of service (crash) via multiple crafted login packets.
CVE-2008-7204
Cross-site request forgery (CSRF) vulnerability in VirtueMart 1.0.13a and earlier allows remote attackers to hijack the authentication of administrators via unspecified vectors.
CVE-2008-7205
Unspecified vulnerability in the product view functionality in VirtueMart 1.0.13a and earlier allows remote attackers to read arbitrary files via vectors related to a template file.
CVE-2008-7206
Unspecified vulnerability in Electronic Logbook (ELOG) before 2.7.2 has unknown impact and attack vectors when the "logbook contains HTML code," probably cross-site scripting (XSS).
CVE-2008-7207
RivetTracker before 1.0 stores passwords in cleartext in config.php, which allows local users to discover passwords by reading config.php.
CVE-2008-7208
Multiple SQL injection vulnerabilities in OneCMS 2.4, and possibly earlier, allow remote attackers to execute arbitrary SQL commands via the (1) username parameter ($usernameb variable) to a_login.php or (2) user parameter to staff.php.
CVE-2008-7209
Unrestricted file upload vulnerability in the add2 action in a_upload.php in OneCMS 2.4, and possibly earlier, allows remote attackers to execute arbitrary code by uploading a file with an executable extension and using a safe content type such as image/gif, then accessing it via a direct request to the file in an unspecified directory.
CVE-2008-7210
directory.php in AJchat 0.10 allows remote attackers to bypass input validation and conduct SQL injection attacks via a numeric parameter with a value matching the s parameter's hash value, which prevents the associated $_GET["s"] variable from being unset.  NOTE: it could be argued that this vulnerability is due to a bug in the unset PHP command (CVE-2006-3017) and the proper fix should be in PHP; if so, then this should not be treated as a vulnerability in AJChat.
CVE-2008-7211
CreativeLabs es1371mp.sys 5.1.3612.0 WDM audio driver, as used in Ensoniq PCI 1371 sound cards and when running on Windows Vista, does not create a Functional Device Object (FDO) to prevent user-moade access to the Physical Device Object (PDO), which allows local users to gain SYSTEM privileges via a crafted IRP request that dereferences a NULL FsContext pointer.
CVE-2008-7212
MOStlyCE before 2.4, as used in Mambo 4.6.3 and earlier, allows remote attackers to obtain sensitive information via certain requests to mambots/editors/mostlyce/jscripts/tiny_mce/filemanager/connectors/php/connector.php, which reveals the installation path in an error message.
CVE-2008-7213
Cross-site scripting (XSS) vulnerability in mambots/editors/mostlyce/jscripts/tiny_mce/filemanager/connectors/php/connector.php in MOStlyCE before 2.4, as used in Mambo 4.6.3 and earlier, allows remote attackers to inject arbitrary web script or HTML via the Command parameter.
CVE-2008-7214
Cross-site request forgery (CSRF) vulnerability in administrator/index2.php in MOStlyCE before 2.4, as used in Mambo 4.6.3 and earlier, allows remote attackers to hijack the authentication of administrators for requests that add new administrator accounts via the save task in a com_users action, as demonstrated using a separate XSS vulnerability in mambots/editors/mostlyce/jscripts/tiny_mce/filemanager/connectors/php/connector.php.
CVE-2008-7215
The Image Manager in MOStlyCE before 2.4, as used in Mambo 4.6.3 and earlier, allows remote attackers to rename arbitrary files and cause a denial of service via modified file[NewFile][name], file[NewFile][tmp_name], and file[NewFile][size] parameters in a FileUpload command, which are used to modify equivalent variables in $_FILES that are accessed when the is_uploaded_file check fails.
CVE-2008-7216
Peter's Math Anti-Spam Spinoff plugin for WordPress generates audio CAPTCHA clips by concatenating static audio files without any additional distortion, which allows remote attackers to bypass CAPTCHA protection by reading certain bytes from the generated clip.
CVE-2009-2800
Buffer overflow in Alias Manager in Apple Mac OS X 10.4.11 and 10.5.8 allows attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted alias file.
CVE-2009-3167
Directory traversal vulnerability in index.php in Anantasoft Gazelle CMS 1.0, when magic_quotes_gpc is disabled, allows remote attackers to read arbitrary files via a .. (dot dot) in the template parameter.
CVE-2009-3168
Mevin Productions Basic PHP Events Lister 2.0 does not properly restrict access to (1) admin/reset.php and (2) admin/user_add.php, which allows remote authenticated users to reset administrative passwords or add administrators via a direct request.
CVE-2009-3169
Multiple unspecified vulnerabilities in Hitachi JP1/File Transmission Server/FTP before 09-00 allow remote attackers to execute arbitrary code via unknown attack vectors.
CVE-2009-3170
Stack-based buffer overflow in AIMP2 Audio Converter 2.53 (build 330) and earlier allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long File1 argument in a (1) .pls or (2) .m3u playlist file.
CVE-2009-3171
Multiple cross-site scripting (XSS) vulnerabilities in Anantasoft Gazelle CMS 1.0 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) user parameter to user.php or (2) lookup parameter to search.php.
CVE-2009-3172
Unspecified vulnerability in Hitachi Groupmax Groupware Server 07-00 through 07-50-/A, Groupmax Server Set 03-00 through 06-52, Groupware Server Set 03-00 through 06-52, and Scheduler Server Set 03-00 through 06-52 has unknown impact and attack vectors related to invalid access rights.
CVE-2009-3173
Unrestricted file upload vulnerability in admin/add_album.php in The Rat CMS Alpha 2 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in images/.
CVE-2009-3174
PHP remote file inclusion vulnerability in fonctions_racine.php in OBOphiX 2.7.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the chemin_lib parameter.
CVE-2009-3175
Multiple SQL injection vulnerabilities in Model Agency Manager PRO (formerly Modeling Agency Content Management Script) allow remote attackers to execute arbitrary SQL commands via the user_id parameter to (1) view.php, (2) photos.php, and (3) motm.php; and the (4) id parameter to forum_message.php.
CVE-2009-3176
Buffer overflow in the ActiveX control in Novell iPrint Client 4.38 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unknown attack vectors, as demonstrated by a certain module in VulnDisco Pack Professional 8.1, "Novell iPrint Client 4.38 ActiveX exploit." NOTE: as of 20090909, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-3177
Unspecified vulnerability in Kaspersky Online Scanner 7.0 has unknown impact and attack vectors, as demonstrated by a certain module in VulnDisco Pack Professional 8.8, (1) "Kaspersky Online Antivirus Scanner 7.0 exploit (Linux)" and (2) "Kaspersky Online Antivirus Scanner 7.0 exploit (Windows)." NOTE: as of 20090909, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-3178
Unspecified vulnerability in mm.exe in Symantec Altiris Deployment Solution 6.9 allows remote attackers to cause a denial of service via unknown attack vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.18, "Symantec Altiris Deployment Solution 6.9 DoS." NOTE: as of 20090909, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-3179
Multiple unspecified vulnerabilities in Symantec Altiris Deployment Solution 6.9 might allow remote attackers to execute arbitrary code via unknown client-side attack vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.17, as identified by (1) "Symantec Altiris Deployment Solution 6.9 exploit, (2) "Symantec Altiris Deployment Solution 6.9 exploit (II)," and (3) "Symantec Altiris Deployment Solution 6.9 exploit (III)." NOTE: as of 20090909, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-3180
Anantasoft Gazelle CMS 1.0 allows remote attackers to conduct a password reset for other users via a modified user parameter to renew.php.
CVE-2009-3181
Directory traversal vulnerability in Anantasoft Gazelle CMS 1.0 allows remote attackers to overwrite arbitrary files via a .. (dot dot) in the customizetemplate parameter in a direct request to admin/settemplate.php.
CVE-2009-3182
Unrestricted file upload vulnerability in admin/editor/filemanager/browser.html in Anantasoft Gazelle CMS 1.0 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in user/File/.
