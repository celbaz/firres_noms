CVE-2015-6023
ping.cgi in NetCommWireless HSPA 3G10WVE wireless routers with firmware before 3G10WVE-L101-S306ETS-C01_R05 allows remote attackers to bypass intended access restrictions via a direct request.  NOTE: this issue can be combined with CVE-2015-6024 to execute arbitrary commands.
CVE-2015-6024
ping.cgi in NetCommWireless HSPA 3G10WVE wireless routers with firmware before 3G10WVE-L101-S306ETS-C01_R05 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the DIA_IPADDRESS parameter.
CVE-2015-8831
Cross-site scripting (XSS) vulnerability in admin/comments.php in Dotclear before 2.8.2 allows remote attackers to inject arbitrary web script or HTML via the author name in a comment.
CVE-2015-8832
Multiple incomplete blacklist vulnerabilities in inc/core/class.dc.core.php in Dotclear before 2.8.2 allow remote authenticated users with "manage their own media items" and "manage their own entries and comments" permissions to execute arbitrary PHP code by uploading a file with a (1) .pht, (2) .phps, or (3) .phtml extension.
CVE-2015-8936
Cross-site scripting (XSS) vulnerability in squidGuard.cgi in squidGuard before 1.5 allows remote attackers to inject arbitrary web script or HTML via a blocked site link.
CVE-2016-10190
Heap-based buffer overflow in libavformat/http.c in FFmpeg before 2.8.10, 3.0.x before 3.0.5, 3.1.x before 3.1.6, and 3.2.x before 3.2.2 allows remote web servers to execute arbitrary code via a negative chunk size in an HTTP response.
CVE-2016-10191
Heap-based buffer overflow in libavformat/rtmppkt.c in FFmpeg before 2.8.10, 3.0.x before 3.0.5, 3.1.x before 3.1.6, and 3.2.x before 3.2.2 allows remote attackers to execute arbitrary code by leveraging failure to check for RTMP packet size mismatches.
CVE-2016-10192
Heap-based buffer overflow in ffserver.c in FFmpeg before 2.8.10, 3.0.x before 3.0.5, 3.1.x before 3.1.6, and 3.2.x before 3.2.2 allows remote attackers to execute arbitrary code by leveraging failure to check chunk size.
CVE-2016-10198
The gst_aac_parse_sink_setcaps function in gst/audioparsers/gstaacparse.c in gst-plugins-good in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (invalid memory read and crash) via a crafted audio file.
CVE-2016-10199
The qtdemux_tag_add_str_full function in gst/isomp4/qtdemux.c in gst-plugins-good in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (out-of-bounds read and crash) via a crafted tag value.
CVE-2016-2147
Integer overflow in the DHCP client (udhcpc) in BusyBox before 1.25.0 allows remote attackers to cause a denial of service (crash) via a malformed RFC1035-encoded domain name, which triggers an out-of-bounds heap write.
CVE-2016-2148
Heap-based buffer overflow in the DHCP client (udhcpc) in BusyBox before 1.25.0 allows remote attackers to have unspecified impact via vectors involving OPTION_6RD parsing.
CVE-2016-3101
Cross-site scripting (XSS) vulnerability in the Extra Columns plugin before 1.17 in Jenkins allows remote attackers to inject arbitrary web script or HTML by leveraging failure to filter tool tips through the configured markup formatter.
CVE-2016-3102
The Script Security plugin before 1.18.1 in Jenkins might allow remote attackers to bypass a Groovy sandbox protection mechanism via a plugin that performs (1) direct field access or (2) get/set array operations.
CVE-2016-4986
Directory traversal vulnerability in the TAP plugin before 1.25 in Jenkins allows remote attackers to read arbitrary files via an unspecified parameter.
CVE-2016-4987
Directory traversal vulnerability in the Image Gallery plugin before 1.4 in Jenkins allows remote attackers to list arbitrary directories and read arbitrary files via unspecified form fields.
CVE-2016-4988
Cross-site scripting (XSS) vulnerability in the Build Failure Analyzer plugin before 1.16.0 in Jenkins allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter.
CVE-2016-5726
Packages.php in Simple Machines Forum (SMF) 2.1 allows remote attackers to conduct PHP object injection attacks and execute arbitrary PHP code via the themechanges array parameter.
CVE-2016-5727
LogInOut.php in Simple Machines Forum (SMF) 2.1 allows remote attackers to conduct PHP object injection attacks and execute arbitrary PHP code via vectors related to variables derived from user input in a foreach loop.
CVE-2016-6171
Knot DNS before 2.3.0 allows remote DNS servers to cause a denial of service (memory exhaustion and slave server crash) via a large zone transfer for (1) DDNS, (2) AXFR, or (3) IXFR.
CVE-2016-6173
NSD before 4.1.11 allows remote DNS master servers to cause a denial of service (/tmp disk consumption and slave server crash) via a zone transfer with unlimited data.
CVE-2016-8494
Insufficient verification of uploaded files allows attackers with webui administrators privileges to perform arbitrary code execution by uploading a new webui theme.
CVE-2016-9244
A BIG-IP virtual server configured with a Client SSL profile that has the non-default Session Tickets option enabled may leak up to 31 bytes of uninitialized memory. A remote attacker may exploit this vulnerability to obtain Secure Sockets Layer (SSL) session IDs from other sessions. It is possible that other data from uninitialized memory may be returned as well.
CVE-2017-3807
A vulnerability in Common Internet Filesystem (CIFS) code in the Clientless SSL VPN functionality of Cisco ASA Software, Major Releases 9.0-9.6, could allow an authenticated, remote attacker to cause a heap overflow. The vulnerability is due to insufficient validation of user supplied input. An attacker could exploit this vulnerability by sending a crafted URL to the affected system. An exploit could allow the remote attacker to cause a reload of the affected system or potentially execute code. Note: Only traffic directed to the affected system can be used to exploit this vulnerability. This vulnerability affects systems configured in routed firewall mode only and in single or multiple context mode. This vulnerability can be triggered by IPv4 or IPv6 traffic. A valid TCP connection is needed to perform the attack. The attacker needs to have valid credentials to log in to the Clientless SSL VPN portal. Vulnerable Cisco ASA Software running on the following products may be affected by this vulnerability: Cisco ASA 5500 Series Adaptive Security Appliances, Cisco ASA 5500-X Series Next-Generation Firewalls, Cisco Adaptive Security Virtual Appliance (ASAv), Cisco ASA for Firepower 9300 Series, Cisco ASA for Firepower 4100 Series. Cisco Bug IDs: CSCvc23838.
CVE-2017-3813
A vulnerability in the Start Before Logon (SBL) module of Cisco AnyConnect Secure Mobility Client Software for Windows could allow an unauthenticated, local attacker to open Internet Explorer with the privileges of the SYSTEM user. The vulnerability is due to insufficient implementation of the access controls. An attacker could exploit this vulnerability by opening the Internet Explorer browser. An exploit could allow the attacker to use Internet Explorer with the privileges of the SYSTEM user. This may allow the attacker to execute privileged commands on the targeted system. This vulnerability affects versions prior to released versions 4.4.00243 and later and 4.3.05017 and later. Cisco Bug IDs: CSCvc43976.
CVE-2017-5180
Firejail before 0.9.44.4 and 0.9.38.x LTS before 0.9.38.8 LTS does not consider the .Xauthority case during its attempt to prevent accessing user files with an euid of zero, which allows local users to conduct sandbox-escape attacks via vectors involving a symlink and the --private option.
CVE-2017-5589
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for yaxim and Bruno (0.8.6 - 0.8.8; Android).
CVE-2017-5590
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for ChatSecure (3.2.0 - 4.0.0; only iOS) and Zom (all versions up to 1.0.11; only iOS).
CVE-2017-5591
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for SleekXMPP up to 1.3.1 and Slixmpp all versions up to 1.2.3, as bundled in poezio (0.8 - 0.10) and other products.
CVE-2017-5592
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for profanity (0.4.7 - 0.5.0).
CVE-2017-5593
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for Psi+ (0.16.563.580 - 0.16.571.627).
CVE-2017-5602
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for jappix 1.0.0 to 1.1.6.
CVE-2017-5603
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for Jitsi 2.5.5061 - 2.9.5544.
CVE-2017-5604
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for mcabber 1.0.0 - 1.0.4.
CVE-2017-5605
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for Movim 0.8 - 0.10.
CVE-2017-5606
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for Xabber (only if manually enabled: 1.0.30, 1.0.30 VIP, beta 1.0.3 - 1.0.74; Android).
CVE-2017-5634
The Norwegian Air Shuttle (aka norwegian.com) airline kiosk allows physically proximate attackers to bypass the intended "Please select booking identification" UI step, and obtain administrative privileges and network access on the underlying Windows OS, by accessing a touch-screen print icon to manipulate the print dialog.
CVE-2017-5837
The gst_riff_create_audio_caps function in gst-libs/gst/riff/riff-media.c in gst-plugins-base in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (floating point exception and crash) via a crafted video file.
CVE-2017-5838
The gst_date_time_new_from_iso8601_string function in gst/gstdatetime.c in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (out-of-bounds heap read) via a malformed datetime string.
CVE-2017-5839
The gst_riff_create_audio_caps function in gst-libs/gst/riff/riff-media.c in gst-plugins-base in GStreamer before 1.10.3 does not properly limit recursion, which allows remote attackers to cause a denial of service (stack overflow and crash) via vectors involving nested WAVEFORMATEX.
CVE-2017-5840
The qtdemux_parse_samples function in gst/isomp4/qtdemux.c in gst-plugins-good in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (out-of-bounds heap read) via vectors involving the current stts index.
CVE-2017-5841
The gst_avi_demux_parse_ncdt function in gst/avi/gstavidemux.c in gst-plugins-good in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (out-of-bounds heap read) via vectors involving ncdt tags.
CVE-2017-5842
The html_context_handle_element function in gst/subparse/samiparse.c in gst-plugins-base in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (out-of-bounds write) via a crafted SMI file, as demonstrated by OneNote_Manager.smi.
CVE-2017-5843
Multiple use-after-free vulnerabilities in the (1) gst_mini_object_unref, (2) gst_tag_list_unref, and (3) gst_mxf_demux_update_essence_tracks functions in GStreamer before 1.10.3 allow remote attackers to cause a denial of service (crash) via vectors involving stream tags, as demonstrated by 02785736.mxf.
CVE-2017-5844
The gst_riff_create_audio_caps function in gst-libs/gst/riff/riff-media.c in gst-plugins-base in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (floating point exception and crash) via a crafted ASF file.
CVE-2017-5845
The gst_avi_demux_parse_ncdt function in gst/avi/gstavidemux.c in gst-plugins-good in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (invalid memory read and crash) via a ncdt sub-tag that "goes behind" the surrounding tag.
CVE-2017-5846
The gst_asf_demux_process_ext_stream_props function in gst/asfdemux/gstasfdemux.c in gst-plugins-ugly in GStreamer before 1.10.3 allows remote attackers to cause a denial of service (invalid memory read and crash) via vectors related to the number of languages in a video file.
CVE-2017-5847
The gst_asf_demux_process_ext_content_desc function in gst/asfdemux/gstasfdemux.c in gst-plugins-ugly in GStreamer allows remote attackers to cause a denial of service (out-of-bounds heap read) via vectors involving extended content descriptors.
CVE-2017-5848
The gst_ps_demux_parse_psm function in gst/mpegdemux/gstmpegdemux.c in gst-plugins-bad in GStreamer allows remote attackers to cause a denial of service (invalid memory read and crash) via vectors involving PSM parsing.
CVE-2017-5858
An incorrect implementation of "XEP-0280: Message Carbons" in multiple XMPP clients allows a remote attacker to impersonate any user, including contacts, in the vulnerable application's display. This allows for various kinds of social engineering attacks. This CVE is for Converse.js (0.8.0 - 1.0.6, 2.0.0 - 2.0.4).
CVE-2017-5940
Firejail before 0.9.44.6 and 0.9.38.x LTS before 0.9.38.10 LTS does not comprehensively address dotfile cases during its attempt to prevent accessing user files with an euid of zero, which allows local users to conduct sandbox-escape attacks via vectors involving a symlink and the --private option. NOTE: this vulnerability exists because of an incomplete fix for CVE-2017-5180.
CVE-2017-5941
An issue was discovered in the node-serialize package 0.0.4 for Node.js. Untrusted data passed into the unserialize() function can be exploited to achieve arbitrary code execution by passing a JavaScript Object with an Immediately Invoked Function Expression (IIFE).
