CVE-2013-1067
Apport 2.12.5 and earlier uses weak permissions for core dump files created by setuid binaries, which allows local users to obtain sensitive information by reading the file.
CVE-2013-3280
EMC RSA Authentication Agent 7.1.x before 7.1.2 for Web for Internet Information Services has a fail-open design, which allows remote attackers to bypass intended access restrictions via vectors that trigger an agent crash.
CVE-2013-3989
IBM Security AppScan Enterprise 8.x before 8.8 sends a cleartext AppScan Source database password in a response, which allows remote authenticated users to obtain sensitive information, and subsequently conduct man-in-the-middle attacks, by examining the response content.
CVE-2013-4421
The buf_decompress function in packet.c in Dropbear SSH Server before 2013.59 allows remote attackers to cause a denial of service (memory consumption) via a compressed packet that has a large size when it is decompressed.
CVE-2013-4434
Dropbear SSH Server before 2013.59 generates error messages for a failed logon attempt with different time delays depending on whether the user account exists, which allows remote attackers to discover valid usernames.
CVE-2013-4465
Unrestricted file upload vulnerability in the avatar upload functionality in Simple Machines Forum before 2.0.6 and 2.1 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in an unspecified directory.
CWE-434: Unrestricted Upload of File with Dangerous Type per http://cwe.mitre.org/data/definitions/434.html
CVE-2013-4957
The dashboard report in Puppet Enterprise before 3.0.1 allows attackers to execute arbitrary YAML code via a crafted report-specific type.
CVE-2013-4965
Puppet Enterprise before 3.1.0 does not properly restrict the number of authentication attempts by a console account, which makes it easier for remote attackers to bypass intended access restrictions via a brute-force attack.
CVE-2013-5424
IBM Flex System Manager (FSM) 1.3.0 allows remote attackers to bypass intended access restrictions, and create new user accounts or execute tasks, by leveraging an expired password for the system-level account.
CVE-2013-5521
Cisco Identity Services Engine does not properly restrict the creation of guest accounts, which allows remote attackers to cause a denial of service (exhaustion of the account supply) via a series of requests within one session, aka Bug ID CSCue94287.
CVE-2013-5522
Cisco IOS on Catalyst 3750X switches has default Service Module credentials, which makes it easier for local users to gain privileges via a Service Module login, aka Bug ID CSCue92286.
CVE-2013-5530
The web framework in Cisco Identity Services Engine (ISE) 1.0 and 1.1.0 before 1.1.0.665-5, 1.1.1 before 1.1.1.268-7, 1.1.2 before 1.1.2.145-10, 1.1.3 before 1.1.3.124-7, 1.1.4 before 1.1.4.218-7, and 1.2 before 1.2.0.899-2 allows remote authenticated users to execute arbitrary commands via a crafted session on TCP port 443, aka Bug ID CSCuh81511.
CVE-2013-5531
Cisco Identity Services Engine (ISE) 1.x before 1.1.1 allows remote attackers to bypass authentication, and read support-bundle configuration and credentials data, via a crafted session on TCP port 443, aka Bug ID CSCty20405.
CVE-2013-5549
Cisco IOS XR 3.8.1 through 4.2.0 does not properly process fragmented packets within the RP-A, RP-B, PRP, and DRP-B route-processor components, which allows remote attackers to cause a denial of service (transmission outage) via (1) IPv4 or (2) IPv6 traffic, aka Bug ID CSCuh30380.
CVE-2013-6127
The SUPERGRIDLib.SuperGrid ActiveX control in SuperGrid.ocx before 65.30.30000.10002 in WellinTech KingView before 6.53 does not properly restrict ReplaceDBFile method calls, which allows remote attackers to create or overwrite arbitrary files, and subsequently execute arbitrary programs, via the two pathname arguments, as demonstrated by a directory traversal attack.
CVE-2013-6128
The KCHARTXYLib.KChartXY ActiveX control in KChartXY.ocx before 65.30.30000.10002 in WellinTech KingView before 6.53 does not properly restrict SaveToFile method calls, which allows remote attackers to create or overwrite arbitrary files, and subsequently execute arbitrary programs, via the single pathname argument, as demonstrated by a directory traversal attack.
CVE-2013-6280
Cross-site scripting (XSS) vulnerability in Social Sharing Toolkit plugin before 2.1.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-6281
Cross-site scripting (XSS) vulnerability in codebase/spreadsheet.php in the Spreadsheet (dhtmlxSpreadsheet) plugin 2.0 for WordPress allows remote attackers to inject arbitrary web script or HTML via the "page" parameter.
CVE-2013-6283
VideoLAN VLC Media Player 2.0.8 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long string in a URL in a m3u file.
