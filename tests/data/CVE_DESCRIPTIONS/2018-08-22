CVE-2016-9605
A flaw was found in cobbler software component version 2.6.11-1. It suffers from an invalid parameter validation vulnerability, leading the arbitrary file reading. The flaw is triggered by navigating to a vulnerable URL via cobbler-web on a default installation.
CVE-2017-2575
A vulnerability was found while fuzzing libbpg 0.9.7. It is a NULL pointer dereference issue due to missing check of the return value of function malloc in the BPG encoder. This vulnerability appeared while converting a malicious JPEG file to BPG.
CVE-2017-2627
A flaw was found in openstack-tripleo-common as shipped with Red Hat Openstack Enterprise 10 and 11. The sudoers file as installed with OSP's openstack-tripleo-common package is much too permissive. It contains several lines for the mistral user that have wildcards that allow directory traversal with '..' and it grants full passwordless root access to the validations user.
CVE-2017-2635
A NULL pointer deference flaw was found in the way libvirt from 2.5.0 to 3.0.0 handled empty drives. A remote authenticated attacker could use this flaw to crash libvirtd daemon resulting in denial of service.
CVE-2017-2662
A flaw was found in Foreman's katello plugin version 3.4.5. After setting a new role to allow restricted access on a repository with a filter (filter set on the Product Name), the filter is not respected when the actions are done via hammer using the repository id.
CVE-2017-7513
It was found that Satellite 5 configured with SSL/TLS for the PostgreSQL backend failed to correctly validate X.509 server certificate host name fields. A man-in-the-middle attacker could use this flaw to spoof a PostgreSQL server using a specially crafted X.509 certificate.
CVE-2017-7528
Ansible Tower as shipped with Red Hat CloudForms Management Engine 5 is vulnerable to CRLF Injection. It was found that X-Forwarded-For header allows internal servers to deploy other systems (using callback).
CVE-2018-10844
It was found that the GnuTLS implementation of HMAC-SHA-256 was vulnerable to a Lucky thirteen style attack. Remote attackers could use this flaw to conduct distinguishing attacks and plaintext-recovery attacks via statistical analysis of timing data using crafted packets.
CVE-2018-10845
It was found that the GnuTLS implementation of HMAC-SHA-384 was vulnerable to a Lucky thirteen style attack. Remote attackers could use this flaw to conduct distinguishing attacks and plain text recovery attacks via statistical analysis of timing data using crafted packets.
CVE-2018-10846
A cache-based side channel in GnuTLS implementation that leads to plain text recovery in cross-VM attack setting was found. An attacker could use a combination of "Just in Time" Prime+probe attack in combination with Lucky-13 attack to recover plain text using crafted packets.
CVE-2018-10858
A heap-buffer overflow was found in the way samba clients processed extra long filename in a directory listing. A malicious samba server could use this flaw to cause arbitrary code execution on a samba client. Samba versions before 4.6.16, 4.7.9 and 4.8.4 are vulnerable.
CVE-2018-10884
Ansible Tower before versions 3.1.8 and 3.2.6 is vulnerable to cross-site request forgery (CSRF) in awx/api/authentication.py. An attacker could exploit this by tricking already authenticated users into visiting a malicious site and hijacking the authtoken cookie.
CVE-2018-10918
A null pointer dereference flaw was found in the way samba checked database outputs from the LDB database layer. An authenticated attacker could use this flaw to crash a samba server in an Active Directory Domain Controller configuration. Samba versions before 4.7.9 and 4.8.4 are vulnerable.
CVE-2018-10919
The Samba Active Directory LDAP server was vulnerable to an information disclosure flaw because of missing access control checks. An authenticated attacker could use this flaw to extract confidential attribute values using LDAP search expressions. Samba versions before 4.6.16, 4.7.9 and 4.8.4 are vulnerable.
CVE-2018-1139
A flaw was found in the way samba before 4.7.9 and 4.8.4 allowed the use of weak NTLMv1 authentication even when NTLMv1 was explicitly disabled. A man-in-the-middle attacker could use this flaw to read the credential and other details passed between the samba server and client.
CVE-2018-1140
A missing input sanitization flaw was found in the implementation of LDP database used for the LDAP server. An attacker could use this flaw to cause a denial of service against a samba server, used as a Active Directory Domain Controller. All versions of Samba from 4.8.0 onwards are vulnerable
CVE-2018-11758
This affects Apache Cayenne 4.1.M1, 3.2.M1, 4.0.M2 to 4.0.M5, 4.0.B1, 4.0.B2, 4.0.RC1, 3.1, 3.1.1, 3.1.2. CayenneModeler is a desktop GUI tool shipped with Apache Cayenne and intended for editing Cayenne ORM models stored as XML files. If an attacker tricks a user of CayenneModeler into opening a malicious XML file, the attacker will be able to instruct the XML parser built into CayenneModeler to transfer files from a local machine to a remote machine controlled by the attacker. The cause of the issue is XML parser processing XML External Entity (XXE) declarations included in XML. The vulnerability is addressed in Cayenne by disabling XXE processing in all operations that require XML parsing.
CVE-2018-11776
Apache Struts versions 2.3 to 2.3.34 and 2.5 to 2.5.16 suffer from possible Remote Code Execution when alwaysSelectFullNamespace is true (either by user or a plugin like Convention Plugin) and then: results are used with no namespace and in same time, its upper package have no or wildcard namespace and similar to results, same possibility when using url tag which doesn't have value and action set and in same time, its upper package have no or wildcard namespace.
CVE-2018-14787
In Philips' IntelliSpace Cardiovascular (ISCV) products (ISCV Version 2.x or prior and Xcelera Version 4.1 or prior), an attacker with escalated privileges could access folders which contain executables where authenticated users have write permissions, and could then execute arbitrary code with local administrative permissions.
CVE-2018-14789
In Philips' IntelliSpace Cardiovascular (ISCV) products (ISCV Version 3.1 or prior and Xcelera Version 4.1 or prior), an unquoted search path or element vulnerability has been identified, which may allow an attacker to execute arbitrary code and escalate their level of privileges.
CVE-2018-14799
In Philips PageWriter TC10, TC20, TC30, TC50, TC70 Cardiographs, all versions prior to May 2018, the PageWriter device does not sanitize data entered by user. This can lead to buffer overflow or format string vulnerabilities.
CVE-2018-14801
In Philips PageWriter TC10, TC20, TC30, TC50, TC70 Cardiographs, all versions prior to May 2018, an attacker with both the superuser password and physical access can enter the superuser password that can be used to access and modify all settings on the device, as well as allow the user to reset existing passwords.
CVE-2018-1599
IBM API Connect 5.0.0.0 through 5.0.8.3 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 143744.
CVE-2018-5235
Norton Utilities (prior to 16.0.3.44) may be susceptible to a DLL Preloading vulnerability, which is a type of issue that can occur when an application looks to call a DLL for execution and an attacker provides a malicious DLL to use instead. Depending on how the application is configured, it will generally follow a specific search path to locate the DLL. The vulnerability can be exploited by a simple file write (or potentially an over-write) which results in a foreign DLL running under the context of the application.
CVE-2018-5238
Norton Power Eraser (prior to 5.3.0.24) and SymDiag (prior to 2.1.242) may be susceptible to a DLL Preloading vulnerability, which is a type of issue that can occur when an application looks to call a DLL for execution and an attacker provides a malicious DLL to use instead. Depending on how the application is configured, it will generally follow a specific search path to locate the DLL. The vulnerability can be exploited by a simple file write (or potentially an over-write) which results in a foreign DLL running under the context of the application.
