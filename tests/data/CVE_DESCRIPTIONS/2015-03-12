CVE-2015-0521
Cross-site scripting (XSS) vulnerability in EMC RSA Certificate Manager (RCM) before 6.9 build 558 and RSA Registration Manager (RRM) before 6.9 build 558 allows remote authenticated users to inject arbitrary web script or HTML via vectors related to the CMP shared secret parameter.
CVE-2015-0522
Cross-site scripting (XSS) vulnerability in EMC RSA Certificate Manager (RCM) before 6.9 build 558 and RSA Registration Manager (RRM) before 6.9 build 558 allows remote attackers to inject arbitrary web script or HTML via vectors related to the email address parameter.
CVE-2015-0523
EMC RSA Certificate Manager (RCM) before 6.9 build 558 and RSA Registration Manager (RRM) before 6.9 build 558 allow remote attackers to cause an Administration Server denial of service via an invalid MIME e-mail message with a multipart/* Content-Type header.
CVE-2015-0524
SQL injection vulnerability in the Gateway Provisioning service in EMC Secure Remote Services Virtual Edition (ESRS VE) 3.02 and 3.03 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-0525
The Gateway Provisioning service in EMC Secure Remote Services Virtual Edition (ESRS VE) 3.02 and 3.03 allows remote attackers to execute arbitrary OS commands via unspecified vectors.
CVE-2015-1061
IOSurface in Apple iOS before 8.2, Apple OS X through 10.10.2, and Apple TV before 7.1 allows attackers to execute arbitrary code in a privileged context via a crafted app that leverages "type confusion" during serialized-object handling.
CVE-2015-1062
MobileStorageMounter in Apple iOS before 8.2 and Apple TV before 7.1 does not delete invalid disk-image folders, which allows attackers to create folders in arbitrary filesystem locations via a crafted app.
CVE-2015-1063
CoreTelephony in Apple iOS before 8.2 allows remote attackers to cause a denial of service (NULL pointer dereference and device restart) via a Class 0 SMS message.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-1064
Springboard in Apple iOS before 8.2 allows physically proximate attackers to bypass an intended activation requirement and read the home screen by leveraging an application crash during the activation process.
CVE-2015-1065
Multiple buffer overflows in iCloud Keychain in Apple iOS before 8.2 and Apple OS X through 10.10.2 allow man-in-the-middle attackers to execute arbitrary code by modifying the client-server data stream during keychain recovery.
CVE-2015-1066
Off-by-one error in IOAcceleratorFamily in Apple OS X through 10.10.2 allows attackers to execute arbitrary code in a privileged context via a crafted app.
CVE-2015-2044
The emulation routines for unspecified X86 devices in Xen 3.2.x through 4.5.x does not properly initialize data, which allow local HVM guest users to obtain sensitive information via vectors involving an unsupported access size.
CVE-2015-2045
The HYPERVISOR_xen_version hypercall in Xen 3.2.x through 4.5.x does not properly initialize data structures, which allows local guest users to obtain sensitive information via unspecified vectors.
CVE-2015-2150
Xen 3.3.x through 4.5.x and the Linux kernel through 3.19.1 do not properly restrict access to PCI command registers, which might allow local guest OS users to cause a denial of service (non-maskable interrupt and host crash) by disabling the (1) memory or (2) I/O decoding for a PCI Express device and then accessing the device, which triggers an Unsupported Request (UR) response.
CVE-2015-2151
The x86 emulator in Xen 3.2.x through 4.5.x does not properly ignore segment overrides for instructions with register operands, which allows local guest users to obtain sensitive information, cause a denial of service (memory corruption), or possibly execute arbitrary code via unspecified vectors.
CVE-2015-2208
The saveObject function in moadmin.php in phpMoAdmin 1.1.2 allows remote attackers to execute arbitrary commands via shell metacharacters in the object parameter.
CVE-2015-2237
Multiple SQL injection vulnerabilities in Betster (aka PHP Betoffice) 1.0.4 allow remote attackers to execute arbitrary SQL commands via the id parameter to (1) showprofile.php or (2) categoryedit.php or (3) username parameter in a login to index.php.
CVE-2015-2241
Cross-site scripting (XSS) vulnerability in the contents function in admin/helpers.py in Django before 1.7.6 and 1.8 before 1.8b2 allows remote attackers to inject arbitrary web script or HTML via a model attribute in ModelAdmin.readonly_fields, as demonstrated by a @property.
CVE-2015-2275
Cross-site scripting (XSS) vulnerability in WoltLab Community Gallery 2.0 before 2014-12-26 allows remote attackers to inject arbitrary web script or HTML via the parameters[data][7][title] parameter in a saveImageData action to index.php/AJAXProxy.
CVE-2015-2285
The logrotation script (/etc/cron.daily/upstart) in the Ubuntu Upstart package before 1.13.2-0ubuntu9, as used in Ubuntu Vivid 15.04, allows local users to execute arbitrary commands and gain privileges via a crafted file in /run/user/*/upstart/sessions/.
