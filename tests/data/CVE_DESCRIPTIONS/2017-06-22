CVE-2012-6706
A VMSF_DELTA memory corruption was discovered in unrar before 5.5.5, as used in Sophos Anti-Virus Threat Detection Engine before 3.37.2 and other products, that can lead to arbitrary code execution. An integer overflow can be caused in DataSize+CurChannel. The result is a negative value of the "DestPos" variable, which allows the attacker to write out of bounds when setting Mem[DestPos].
CVE-2015-9098
In Redgate SQL Monitor before 3.10 and 4.x before 4.2, a remote attacker can gain unauthenticated access to the Base Monitor, resulting in the ability to execute arbitrary SQL commands on any monitored Microsoft SQL Server machines. If the Base Monitor is connecting to these machines using an account with SQL admin privileges, then code execution on the operating system can result in full system compromise (if Microsoft SQL Server is running with local administrator privileges).
CVE-2016-9747
IBM RELM 4.0, 5.0 and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-9982
IBM Sterling B2B Integrator Standard Edition 5.2 could allow an authenticated user to obtain sensitive information such as account lists due to improper access control. IBM X-Force ID: 120274.
CVE-2016-9983
IBM Sterling B2B Integrator Standard Edition 5.2 could allow an authenticated user with special privileges to view files that they should not have access to. IBM X-Force ID: 120275.
CVE-2017-0176
A buffer overflow in Smart Card authentication code in gpkcsp.dll in Microsoft Windows XP through SP3 and Server 2003 through SP2 allows a remote attacker to execute arbitrary code on the target computer, provided that the computer is joined in a Windows domain and has Remote Desktop Protocol connectivity (or Terminal Services) enabled.
CVE-2017-0897
ExpressionEngine version 2.x < 2.11.8 and version 3.x < 3.5.5 create an object signing token with weak entropy. Successfully guessing the token can lead to remote code execution.
CVE-2017-1326
IBM Sterling File Gateway does not properly restrict user requests based on permission level. This allows for users to update data related to other users, by manipulating the parameters passed in the POST request. IBM X-Force ID: 126060.
CVE-2017-2780
An exploitable heap buffer overflow vulnerability exists in the X509 certificate parsing functionality of InsideSecure MatrixSSL 3.8.7b. A specially crafted x509 certificate can cause a buffer overflow on the heap resulting in remote code execution. To trigger this vulnerability, a specially crafted x509 certificate must be presented to the vulnerable client or server application when initiating secure connection.
CVE-2017-2781
An exploitable heap buffer overflow vulnerability exists in the X509 certificate parsing functionality of InsideSecure MatrixSSL 3.8.7b. A specially crafted x509 certificate can cause a buffer overflow on the heap resulting in remote code execution. To trigger this vulnerability, a specially crafted x509 certificate must be presented to the vulnerable client or server application when initiating secure connection.
CVE-2017-2782
An integer overflow vulnerability exists in the X509 certificate parsing functionality of InsideSecure MatrixSSL 3.8.7b. A specially crafted x509 certificate can cause a length counter to overflow, leading to a controlled out of bounds copy operation. To trigger this vulnerability, a specially crafted x509 certificate must be presented to the vulnerable client or server application when initiating secure connection
CVE-2017-3629
Vulnerability in the Solaris component of Oracle Sun Systems Products Suite (subcomponent: Kernel). Supported versions that are affected are 10 and 11. Easily exploitable vulnerability allows low privileged attacker with logon to the infrastructure where Solaris executes to compromise Solaris. Successful attacks of this vulnerability can result in takeover of Solaris. CVSS 3.0 Base Score 7.8 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H).
CVE-2017-3630
Vulnerability in the Solaris component of Oracle Sun Systems Products Suite (subcomponent: Kernel). Supported versions that are affected are 10 and 11. Easily exploitable vulnerability allows low privileged attacker with logon to the infrastructure where Solaris executes to compromise Solaris. Successful attacks of this vulnerability can result in unauthorized update, insert or delete access to some of Solaris accessible data as well as unauthorized read access to a subset of Solaris accessible data and unauthorized ability to cause a partial denial of service (partial DOS) of Solaris. CVSS 3.0 Base Score 5.3 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L).
CVE-2017-3631
Vulnerability in the Solaris component of Oracle Sun Systems Products Suite (subcomponent: Kernel). The supported version that is affected is 11. Easily exploitable vulnerability allows low privileged attacker with logon to the infrastructure where Solaris executes to compromise Solaris. Successful attacks of this vulnerability can result in unauthorized update, insert or delete access to some of Solaris accessible data as well as unauthorized read access to a subset of Solaris accessible data and unauthorized ability to cause a partial denial of service (partial DOS) of Solaris. CVSS 3.0 Base Score 5.3 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L).
CVE-2017-9424
IdeaBlade Breeze Breeze.Server.NET before 1.6.5 allows remote attackers to execute arbitrary code, related to use of TypeNameHandling in JSON deserialization.
CVE-2017-9775
Stack buffer overflow in GfxState.cc in pdftocairo in Poppler before 0.56 allows remote attackers to cause a denial of service (application crash) via a crafted PDF document.
CVE-2017-9776
Integer overflow leading to Heap buffer overflow in JBIG2Stream.cc in pdftocairo in Poppler before 0.56 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted PDF document.
CVE-2017-9807
An issue was discovered in the OpenWebif plugin through 1.2.4 for E2 open devices. The saveConfig function of "plugin/controllers/models/config.py" performs an eval() call on the contents of the "key" HTTP GET parameter. This allows an unauthenticated remote attacker to execute arbitrary Python code or OS commands via api/saveconfig.
CVE-2017-9815
In LibTIFF 4.0.7, the TIFFReadDirEntryLong8Array function in libtiff/tif_dirread.c mishandles a malloc operation, which allows attackers to cause a denial of service (memory leak within the function _TIFFmalloc in tif_unix.c) via a crafted file.
