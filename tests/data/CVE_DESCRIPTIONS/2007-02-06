CVE-2007-0006
The key serial number collision avoidance code in the key_alloc_serial function in Linux kernel 2.6.9 up to 2.6.20 allows local users to cause a denial of service (crash) via vectors that trigger a null dereference, as originally reported as "spinlock CPU recursion."
The scheme for selecting serial numbers was changed from incrementing a counter to random number selection, increasing the likelihood of a serial number collision.
CVE-2007-0452
smbd in Samba 3.0.6 through 3.0.23d allows remote authenticated users to cause a denial of service (memory and CPU exhaustion) by renaming a file in a way that prevents a request from being removed from the deferred open queue, which triggers an infinite loop.
CVE-2007-0453
Buffer overflow in the nss_winbind.so.1 library in Samba 3.0.21 through 3.0.23d, as used in the winbindd daemon on Solaris, allows attackers to execute arbitrary code via the (1) gethostbyname and (2) getipnodebyname functions.
CVE-2007-0454
Format string vulnerability in the afsacl.so VFS module in Samba 3.0.6 through 3.0.23d allows context-dependent attackers to execute arbitrary code via format string specifiers in a filename on an AFS file system, which is not properly handled during Windows ACL mapping.
CVE-2007-0555
PostgreSQL 7.3 before 7.3.13, 7.4 before 7.4.16, 8.0 before 8.0.11, 8.1 before 8.1.7, and 8.2 before 8.2.2 allows attackers to disable certain checks for the data types of SQL function arguments, which allows remote authenticated users to cause a denial of service (server crash) and possibly access database content.
CVE-2007-0556
The query planner in PostgreSQL before 8.0.11, 8.1 before 8.1.7, and 8.2 before 8.2.2 does not verify that a table is compatible with a "previously made query plan," which allows remote authenticated users to cause a denial of service (server crash) and possibly access database content via an "ALTER COLUMN TYPE" SQL statement, which can be leveraged to read arbitrary memory from the server.
CVE-2007-0756
Chicken of the VNC (cotv) 2.0 allows remote attackers to cause a denial of service (application crash) via a large computer-name size value in a ServerInit packet, which triggers a failed malloc and a resulting NULL dereference.
CVE-2007-0757
PHP remote file inclusion vulnerability in index.php in Miguel Nunes Call of Duty 2 (CoD2) DreamStats System 4.2 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the rootpath parameter.
CVE-2007-0758
PHP remote file inclusion vulnerability in lang.php in PHPProbid 5.24 allows remote attackers to execute arbitrary PHP code via a URL in the SRC attribute of an HTML element in the lang parameter.  NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0759
Multiple SQL injection vulnerabilities in EasyMoblog 0.5.1 allow remote attackers to execute arbitrary SQL commands via the (1) i or (2) post_id parameter to add_comment.php, which triggers an injection in libraries.inc.php; or (3) the i parameter to list_comments.php, which triggers an injection in libraries.inc.php.
CVE-2007-0760
EQdkp 1.3.1 and earlier authenticates administrative requests by verifying that the HTTP Referer header specifies an admin/ URL, which allows remote attackers to read or modify account names and passwords via a spoofed Referer.
CVE-2007-0761
PHP remote file inclusion vulnerability in config.php in phpBB ezBoard converter (ezconvert) 0.2 allows remote attackers to execute arbitrary PHP code via a URL in the ezconvert_dir parameter.
CVE-2007-0762
PHP remote file inclusion vulnerability in includes/functions.php in phpBB++ Build 100 allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-0763
Cross-site scripting (XSS) vulnerability in the news comment functionality in F3Site 2.1 and earlier allows remote attackers to inject arbitrary web script or HTML via the Autor field.
CVE-2007-0764
Unrestricted file upload vulnerability in F3Site 2.1 and earlier allows remote authenticated administrators to upload and execute arbitrary PHP scripts via GIF86 header in a file in the uplf parameter, which can be later accessed via a relative pathname in the dir parameter in adm.php.
CVE-2007-0765
SQL injection vulnerability in news.php in dB Masters Curium CMS 1.03 and earlier allows remote attackers to execute arbitrary SQL commands via the c_id parameter.
CVE-2007-0766
Stack-based buffer overflow in Remotesoft .NET Explorer 2.0.1 allows user-assisted remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a long line in a .cpp file.
CVE-2007-0767
Cross-site scripting (XSS) vulnerability in the core in Phorum before 5.1.18 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-0768
Multiple cross-site scripting (XSS) vulnerabilities in the Contact Details functionality in Yahoo! Messenger 8.1.0.209 and earlier allow user-assisted remote attackers to inject arbitrary web script or HTML via a javascript: URI in the SRC attribute of an IMG element to the (1) First Name, (2) Last Name, and (3) Nickname fields.  NOTE: some of these details are obtained from third party information.
Access Complexity: Successful exploitation requires that the attacker is in the messenger list of the target.
CVE-2007-0769
** DISPUTED **  Cross-site scripting (XSS) vulnerability in register.php in Phorum 5.1.18 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: the vendor disputes this vulnerability, stating that "The characters are escaped properly."
CVE-2007-0784
SQL injection vulnerability in login.asp for tPassword in the Raymond BERTHOU script collection (aka RBL - ASP) allows remote attackers to execute arbitrary SQL commands via the (1) User and (2) Password parameters.
CVE-2007-0785
PHP remote file inclusion vulnerability in previewtheme.php in Flipsource Flip 2.01-final 1.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the inc_path parameter.
CVE-2007-0786
SQL injection vulnerability in view.php in Noname Media Photo Galerie Standard 1.1.1 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0787
PHP remote file inclusion vulnerability in controller.php in Simple Invoices before 20070202 allows remote attackers to execute arbitrary PHP code via a URL in the (1) module or (2) view parameter.  NOTE: some of these details are obtained from third party information.
Successful exploitation requires that "register_globals" is enabled and "magic_quotes_gpc" is disabled.
CVE-2007-0788
Cross-site scripting (XSS) vulnerability in MediaWiki 1.9.x before 1.9.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to "sortable tables JavaScript."
CVE-2007-0789
SQL injection vulnerability in Mambo before 4.5.5 allows remote attackers to execute arbitrary SQL commands via unspecified vectors in cancel edit functions, possibly related to the id parameter.
CVE-2007-0790
Heap-based buffer overflow in SmartFTP 2.0.1002 allows remote FTP servers to execute arbitrary code via a large banner.
CVE-2007-0791
Cross-site scripting (XSS) vulnerability in Atom feeds in Bugzilla 2.20.3, 2.22.1, and 2.23.3, and earlier versions down to 2.20.1, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-0792
The mod_perl initialization script in Bugzilla 2.23.3 does not set the Bugzilla Apache configuration to allow .htaccess permissions to override file permissions, which allows remote attackers to obtain the database username and password via a direct request for the localconfig file.
CVE-2007-0793
PHP remote file inclusion vulnerability in inc/common.php in GlobalMegaCorp dvddb 0.6 allows remote attackers to execute arbitrary PHP code via a URL in the config parameter.
CVE-2007-0794
** DISPUTED **  SQL injection vulnerability in inc/common.php in GlobalMegaCorp dvddb 0.6 allows remote attackers to execute arbitrary SQL commands via the user parameter.  NOTE: this issue has been disputed by a reliable third party, who states that inc/common.php only contains function definitions.
CVE-2007-0795
Multiple PHP remote file inclusion vulnerabilities in Wap Portal Server 1.x allow remote attackers to execute arbitrary PHP code via a URL in the language parameter to (1) index.php and (2) admin/index.php.
CVE-2007-0796
Blue Coat Systems WinProxy 6.1a and 6.0 r1c, and possibly earlier, allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via a long HTTP CONNECT request, which triggers heap corruption.
CVE-2007-0797
PHP remote file inclusion vulnerability in theme/settings.php in bluevirus-design SMA-DB 0.3.9 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the pfad_z parameter.
CVE-2007-0798
Multiple cross-site scripting (XSS) vulnerabilities in Ublog Reload 1.0.5 allow remote attackers to inject arbitrary web script or HTML via unspecified parameters to (1) login.asp; and allow remote authenticated users to inject arbitrary web script or HTML via unspecified parameters to (2) badword.asp, (3) polls.asp, and (4) users.asp.
CVE-2007-0799
SQL injection vulnerability in badword.asp in Ublog Reload 1.0.5 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
