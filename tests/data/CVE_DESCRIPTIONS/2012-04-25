CVE-2012-0467
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2012-0468
The browser engine in Mozilla Firefox 4.x through 11.0, Thunderbird 5.0 through 11.0, and SeaMonkey before 2.9 allows remote attackers to cause a denial of service (assertion failure and memory corruption) or possibly execute arbitrary code via vectors related to jsval.h and the js::array_shift function.
CVE-2012-0469
Use-after-free vulnerability in the mozilla::dom::indexedDB::IDBKeyRange::cycleCollection::Trace function in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allows remote attackers to execute arbitrary code via vectors related to crafted IndexedDB data.
CVE-2012-0470
Heap-based buffer overflow in the nsSVGFEDiffuseLightingElement::LightPixel function in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allows remote attackers to cause a denial of service (invalid gfxImageSurface free operation) or possibly execute arbitrary code by leveraging the use of "different number systems."
CVE-2012-0471
Cross-site scripting (XSS) vulnerability in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allows remote attackers to inject arbitrary web script or HTML via a multibyte character set.
CVE-2012-0472
The cairo-dwrite implementation in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9, when certain Windows Vista and Windows 7 configurations are used, does not properly restrict font-rendering attempts, which allows remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via unspecified vectors.
CVE-2012-0473
The WebGLBuffer::FindMaxUshortElement function in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 calls the FindMaxElementInSubArray function with incorrect template arguments, which allows remote attackers to obtain sensitive information from video memory via a crafted WebGL.drawElements call.
CVE-2012-0474
Cross-site scripting (XSS) vulnerability in the docshell implementation in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allows remote attackers to inject arbitrary web script or HTML via vectors related to short-circuited page loads, aka "Universal XSS (UXSS)."
CVE-2012-0475
Mozilla Firefox 4.x through 11.0, Thunderbird 5.0 through 11.0, and SeaMonkey before 2.9 do not properly construct the Origin and Sec-WebSocket-Origin HTTP headers, which might allow remote attackers to bypass an IPv6 literal ACL via a cross-site (1) XMLHttpRequest or (2) WebSocket operation involving a nonstandard port number and an IPv6 address that contains certain zero fields.
CVE-2012-0477
Multiple cross-site scripting (XSS) vulnerabilities in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allow remote attackers to inject arbitrary web script or HTML via the (1) ISO-2022-KR or (2) ISO-2022-CN character set.
CVE-2012-0478
The texImage2D implementation in the WebGL subsystem in Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 does not properly restrict JSVAL_TO_OBJECT casts, which might allow remote attackers to execute arbitrary code via a crafted web page.
CVE-2012-0479
Mozilla Firefox 4.x through 11.0, Firefox ESR 10.x before 10.0.4, Thunderbird 5.0 through 11.0, Thunderbird ESR 10.x before 10.0.4, and SeaMonkey before 2.9 allow remote attackers to spoof the address bar via an https URL for invalid (1) RSS or (2) Atom XML content.
CVE-2012-1126
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via crafted property data in a BDF font.
CVE-2012-1127
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via crafted glyph or bitmap data in a BDF font.
CVE-2012-1128
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (NULL pointer dereference and memory corruption) or possibly execute arbitrary code via a crafted TrueType font.
CVE-2012-1129
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via a crafted SFNT string in a Type 42 font.
CVE-2012-1130
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via crafted property data in a PCF font.
CVE-2012-1131
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, on 64-bit platforms allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via vectors related to the cell table of a font.
CVE-2012-1132
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via crafted dictionary data in a Type 1 font.
CVE-2012-1133
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap write operation and memory corruption) or possibly execute arbitrary code via crafted glyph or bitmap data in a BDF font.
CVE-2012-1134
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap write operation and memory corruption) or possibly execute arbitrary code via crafted private-dictionary data in a Type 1 font.
CVE-2012-1135
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via vectors involving the NPUSHB and NPUSHW instructions in a TrueType font.
CVE-2012-1136
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap write operation and memory corruption) or possibly execute arbitrary code via crafted glyph or bitmap data in a BDF font that lacks an ENCODING field.
CVE-2012-1137
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via a crafted header in a BDF font.
CVE-2012-1138
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via vectors involving the MIRP instruction in a TrueType font.
CVE-2012-1139
Array index error in FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid stack read operation and memory corruption) or possibly execute arbitrary code via crafted glyph data in a BDF font.
CVE-2012-1140
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via a crafted PostScript font object.
CVE-2012-1141
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap read operation and memory corruption) or possibly execute arbitrary code via a crafted ASCII string in a BDF font.
CVE-2012-1142
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap write operation and memory corruption) or possibly execute arbitrary code via crafted glyph-outline data in a font.
CVE-2012-1143
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (divide-by-zero error) via a crafted font.
CVE-2012-1144
FreeType before 2.4.9, as used in Mozilla Firefox Mobile before 10.0.4 and other products, allows remote attackers to cause a denial of service (invalid heap write operation and memory corruption) or possibly execute arbitrary code via a crafted TrueType font.
CVE-2012-2418
Heap-based buffer overflow in the intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, allows remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via a URI with a % (percent) character as its (1) last or (2) second-to-last character.
CVE-2012-2419
Memory leak in the intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, allows remote attackers to cause a denial of service (memory consumption) via a URI with multiple references to the same name-value pair.
CVE-2012-2420
The intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, might allow remote attackers to obtain sensitive information via a URI with a % (percent) character as its (1) last or (2) second-to-last character, in situations where a certain "post-URL data" buffer contains a 0x0000 character but a buffer overflow does not occur.
CVE-2012-2421
Absolute path traversal vulnerability in the intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, might allow remote attackers to read arbitrary files in ZIP archives via a full pathname in the URI.
CVE-2012-2422
Intuit QuickBooks 2009 through 2012 might allow remote attackers to obtain pathname information via the qbwc://docontrol/GetCompanyFile functionality.
CVE-2012-2423
The intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, provide different responses to remote requests depending on whether a ZIP pathname is valid, which allows remote attackers to obtain potentially sensitive information about the installation path and product version via a series of requests involving the Msxml2.XMLHTTP object.
CVE-2012-2424
The intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, allow remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a URI that lacks a required delimiter.
Per: http://cwe.mitre.org/data/definitions/476.html

CWE-476: NULL Pointer Dereference


CVE-2012-2425
The intu-help-qb (aka Intuit Help System Async Pluggable Protocol) handlers in HelpAsyncPluggableProtocol.dll in Intuit QuickBooks 2009 through 2012, when Internet Explorer is used, allow remote attackers to cause a denial of service (application crash) via a long URI.
