CVE-2008-7271
Multiple cross-site scripting (XSS) vulnerabilities in the Help Contents web application (aka the Help Server) in Eclipse IDE, possibly 3.3.2, allow remote attackers to inject arbitrary web script or HTML via (1) the searchWord parameter to help/advanced/searchView.jsp or (2) the workingSet parameter in an add action to help/advanced/workingSetManager.jsp, a different issue than CVE-2010-4647.
CVE-2010-2599
Unspecified vulnerability in Research In Motion (RIM) BlackBerry Device Software before 6.0.0 allows remote attackers to cause a denial of service (browser hang) via a crafted web page.
CVE-2010-2604
Multiple buffer overflows in the PDF Distiller in the BlackBerry Attachment Service component in Research In Motion (RIM) BlackBerry Enterprise Server 4.1.3 through 5.0.2, and Enterprise Server Express 5.0.1 and 5.0.2, allow remote attackers to execute arbitrary code via a crafted PDF file.
CVE-2010-3912
The supportconfig script in supportutils in SUSE Linux Enterprise 11 SP1 and 10 SP3 does not "disguise passwords" in configuration files, which has unknown impact and attack vectors.
CVE-2010-3924
SQL injection vulnerability in Aimluck Aipo before 5.1.0.1 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2010-3925
Contents-Mall before 15 does not properly handle passwords, which allows remote attackers to discover the administrative password, and consequently obtain sensitive information or modify data, via unspecified vectors.
CVE-2010-4051
The regcomp implementation in the GNU C Library (aka glibc or libc6) through 2.11.3, and 2.12.x through 2.12.2, allows context-dependent attackers to cause a denial of service (application crash) via a regular expression containing adjacent bounded repetitions that bypass the intended RE_DUP_MAX limitation, as demonstrated by a {10,}{10,}{10,}{10,}{10,} sequence in the proftpd.gnu.c exploit for ProFTPD, related to a "RE_DUP_MAX overflow."
CVE-2010-4052
Stack consumption vulnerability in the regcomp implementation in the GNU C Library (aka glibc or libc6) through 2.11.3, and 2.12.x through 2.12.2, allows context-dependent attackers to cause a denial of service (resource exhaustion) via a regular expression containing adjacent repetition operators, as demonstrated by a {10,}{10,}{10,}{10,} sequence in the proftpd.gnu.c exploit for ProFTPD.
CVE-2010-4527
The load_mixer_volumes function in sound/oss/soundcard.c in the OSS sound subsystem in the Linux kernel before 2.6.37 incorrectly expects that a certain name field ends with a '\0' character, which allows local users to conduct buffer overflow attacks and gain privileges, or possibly obtain sensitive information from kernel memory, via a SOUND_MIXER_SETLEVELS ioctl call.
CVE-2010-4529
Integer underflow in the irda_getsockopt function in net/irda/af_irda.c in the Linux kernel before 2.6.37 on platforms other than x86 allows local users to obtain potentially sensitive information from kernel heap memory via an IRLMP_ENUMDEVICES getsockopt call.
CVE-2010-4537
Unspecified vulnerability in CrawlTrack before 3.2.7, when a public stats page is provided, allows remote attackers to execute arbitrary PHP code via unknown vectors.
CVE-2010-4647
Multiple cross-site scripting (XSS) vulnerabilities in the Help Contents web application (aka the Help Server) in Eclipse IDE before 3.6.2 allow remote attackers to inject arbitrary web script or HTML via the query string to (1) help/index.jsp or (2) help/advanced/content.jsp.
CVE-2011-0261
Unspecified vulnerability in jovgraph.exe in jovgraph in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a malformed displayWidth option in the arg parameter.
CVE-2011-0262
Buffer overflow in the stringToSeconds function in ovutil.dll in ovwebsnmpsrv.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via large values of variables to jovgraph.exe.
CVE-2011-0263
Multiple stack-based buffer overflows in ovas.exe in the OVAS service in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allow remote attackers to execute arbitrary code via a long (1) Source Node or (2) Destination Node variable.
CVE-2011-0264
Stack-based buffer overflow in ovutil.dll in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long COOKIE variable.
CVE-2011-0265
Buffer overflow in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long data_select1 parameter.
CVE-2011-0266
Buffer overflow in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long nameParams parameter, a different vulnerability than CVE-2011-0267.2.
CVE-2011-0267
Multiple buffer overflows in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allow remote attackers to execute arbitrary code via a long (1) schdParams or (2) nameParams parameter, a different vulnerability than CVE-2011-0266.
CVE-2011-0268
Buffer overflow in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long text1 parameter.
CVE-2011-0269
Buffer overflow in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long schd_select1 parameter.
CVE-2011-0270
Format string vulnerability in nnmRptConfig.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via format string specifiers in input data that involves an invalid template name.
CVE-2011-0271
The CGI scripts in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 do not properly validate an unspecified parameter, which allows remote attackers to execute arbitrary commands by using a command string for this parameter's value, related to a "command injection vulnerability."
CVE-2011-0310
Buffer overflow in IBM WebSphere MQ 7.0 before 7.0.1.4 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted header field in a message.
CVE-2011-0443
SQL injection vulnerability in inc/tinybb-settings.php in tinyBB 1.2, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the id parameter in a profile action to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2011-0444
Buffer overflow in the MAC-LTE dissector (epan/dissectors/packet-mac-lte.c) in Wireshark 1.2.0 through 1.2.13 and 1.4.0 through 1.4.2 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large number of RARs.
CVE-2011-0445
The ASN.1 BER dissector in Wireshark 1.4.0 through 1.4.2 allows remote attackers to cause a denial of service (assertion failure) via crafted packets, as demonstrated by fuzz-2010-12-30-28473.pcap.
