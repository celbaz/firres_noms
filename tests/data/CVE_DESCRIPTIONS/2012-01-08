CVE-2011-3206
Multiple cross-site scripting (XSS) vulnerabilities in the administration interface in RHQ 4.2.0, as used in JBoss Operations Network (aka JON or JBoss ON) before 3.0, allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-4055
Buffer overflow in the WebClient ActiveX control in Siemens Tecnomatix FactoryLink 6.6.1 (aka 6.6 SP1), 7.5.217 (aka 7.5 SP2), and 8.0.2.54 allows remote attackers to execute arbitrary code via a long string in a parameter associated with the location URL.
CVE-2011-4056
An unspecified ActiveX control in ActBar.ocx in Siemens Tecnomatix FactoryLink 6.6.1 (aka 6.6 SP1), 7.5.217 (aka 7.5 SP2), and 8.0.2.54 allows remote attackers to create or overwrite arbitrary files via the save method.
CVE-2011-4360
MediaWiki before 1.17.1 allows remote attackers to obtain the page titles of all restricted pages via a series of requests involving the (1) curid or (2) oldid parameter.
CVE-2011-4361
MediaWiki before 1.17.1 does not check for read permission before handling action=ajax requests, which allows remote attackers to obtain sensitive information by (1) leveraging the SpecialUpload::ajaxGetExistsWarning function, or by (2) leveraging an extension, as demonstrated by the CategoryTree, ExtTab, and InlineEditor extensions.
CVE-2011-4529
Multiple buffer overflows in Siemens Automation License Manager (ALM) 4.0 through 5.1+SP1+Upd1 allow remote attackers to execute arbitrary code via a long serialid field in an _licensekey command, as demonstrated by the (1) check_licensekey or (2) read_licensekey command.
CVE-2011-4530
Siemens Automation License Manager (ALM) 4.0 through 5.1+SP1+Upd1 does not properly copy fields obtained from clients, which allows remote attackers to cause a denial of service (exception and daemon crash) via long fields, as demonstrated by fields to the (1) open_session->workstation->NAME or (2) grant->VERSION function.
CVE-2011-4531
Siemens Automation License Manager (ALM) 4.0 through 5.1+SP1+Upd1 allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via crafted content in a (1) get_target_ocx_param or (2) send_target_ocx_param command.
CVE-2011-4532
Absolute path traversal vulnerability in the ALMListView.ALMListCtrl ActiveX control in almaxcx.dll in the graphical user interface in Siemens Automation License Manager (ALM) 2.0 through 5.1+SP1+Upd2 allows remote attackers to overwrite arbitrary files via the Save method.
CVE-2011-4870
Multiple buffer overflows in the (1) GUIControls, (2) BatchObjSrv, and (3) BatchSecCtrl ActiveX controls in Invensys Wonderware InBatch 9.0 and 9.0 SP1, and InBatch 8.1 SP1, 9.0 SP2, and 9.5 Server and Runtime Clients, allow remote attackers to execute arbitrary code via a long string in a property value, a different issue than CVE-2011-3141.
CVE-2011-5055
MaraDNS 1.3.07.12 and 1.4.08 computes hash values for DNS data without properly restricting the ability to trigger hash collisions predictably, which allows remote attackers to cause a denial of service (CPU consumption) by sending many crafted queries with the Recursion Desired (RD) bit set.  NOTE: this issue exists because of an incomplete fix for CVE-2012-0024.
CVE-2011-5056
The authoritative server in MaraDNS through 2.0.04 computes hash values for DNS data without restricting the ability to trigger hash collisions predictably, which might allow local users to cause a denial of service (CPU consumption) via crafted records in zone files, a different vulnerability than CVE-2012-0024.
CVE-2011-5057
Apache Struts 2.3.1.1 and earlier provides interfaces that do not properly restrict access to collections such as the session and request collections, which might allow remote attackers to modify run-time data values via a crafted parameter to an application that implements an affected interface, as demonstrated by the SessionAware, RequestAware, ApplicationAware, ServletRequestAware, ServletResponseAware, and ParameterAware interfaces.  NOTE: the vendor disputes the significance of this report because of an "easy work-around in existing apps by configuring the interceptor."
CVE-2012-0024
MaraDNS before 1.3.07.12 and 1.4.x before 1.4.08 computes hash values for DNS data without restricting the ability to trigger hash collisions predictably, which allows remote attackers to cause a denial of service (CPU consumption) by sending many crafted queries with the Recursion Desired (RD) bit set.
CVE-2012-0391
The ExceptionDelegator component in Apache Struts before 2.2.3.1 interprets parameter values as OGNL expressions during certain exception handling for mismatched data types of properties, which allows remote attackers to execute arbitrary Java code via a crafted parameter.
CVE-2012-0392
The CookieInterceptor component in Apache Struts before 2.3.1.1 does not use the parameter-name whitelist, which allows remote attackers to execute arbitrary commands via a crafted HTTP Cookie header that triggers Java code execution through a static method.
CVE-2012-0393
The ParameterInterceptor component in Apache Struts before 2.3.1.1 does not prevent access to public constructors, which allows remote attackers to create or overwrite arbitrary files via a crafted parameter that triggers the creation of a Java object.
CVE-2012-0394
** DISPUTED ** The DebuggingInterceptor component in Apache Struts before 2.3.1.1, when developer mode is used, allows remote attackers to execute arbitrary commands via unspecified vectors.  NOTE: the vendor characterizes this behavior as not "a security vulnerability itself."
