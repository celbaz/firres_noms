CVE-2016-3048
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 114711.
CVE-2017-1000121
The UNIX IPC layer in WebKit, including WebKitGTK+ prior to 2.16.3, does not properly validate message size metadata, allowing a compromised secondary process to trigger an integer overflow and subsequent buffer overflow in the UI process. This vulnerability does not affect Apple products.
CVE-2017-1000122
The UNIX IPC layer in WebKit, including WebKitGTK+ prior to 2.16.3, does not properly validate certain message metadata, allowing a compromised secondary process to cause a denial of service (release assertion) of the UI process. This vulnerability does not affect Apple products.
CVE-2017-1000242
Jenkins Git Client Plugin 2.4.2 and earlier creates temporary file with insecure permissions resulting in information disclosure
CVE-2017-1000243
Jenkins Favorite Plugin 2.1.4 and older does not perform permission checks when changing favorite status, allowing any user to set any other user's favorites
CVE-2017-1000244
Jenkins Favorite Plugin version 2.2.0 and older is vulnerable to CSRF resulting in data modification
CVE-2017-1000245
The SSH Plugin stores credentials which allow jobs to access remote servers via the SSH protocol. User passwords and passphrases for encrypted SSH keys are stored in plaintext in a configuration file.
CVE-2017-1001001
PluXml version 5.6 is vulnerable to stored cross-site scripting vulnerability, within the article creation page, which can result in escalation of privileges.
CVE-2017-1147
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 122200.
CVE-2017-1148
IBM OpenPages GRC Platform 7.2 and 7.3 with OpenPages Loss Event Entry (LEE) application could allow a user to obtain sensitive information including private APIs that could be used in further attacks against the system. IBM X-Force ID: 122201.
CVE-2017-12625
Apache Hive 2.1.x before 2.1.2, 2.2.x before 2.2.1, and 2.3.x before 2.3.1 expose an interface through which masking policies can be defined on tables or views, e.g., using Apache Ranger. When a view is created over a given table, the policy enforcement does not happen correctly on the table for masked columns.
CVE-2017-1290
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 125151.
CVE-2017-1300
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 125162.
CVE-2017-1333
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 could allow an unauthenticated user to obtain sensitive information about the server that could be used in future attacks against the system. IBM X-Force ID: 126241.
CVE-2017-1340
IBM Jazz Reporting Service (JRS) 6.0.4 could allow an authenticated user to obtain information on another server that the current report builder interacts with. IBM X-Force ID: 126455.
CVE-2017-14021
A Use of Hard-coded Cryptographic Key issue was discovered in Korenix JetNet JetNet5018G version 1.4, JetNet5310G version 1.4a, JetNet5428G-2G-2FX version 1.4, JetNet5628G-R version 1.4, JetNet5628G version 1.4, JetNet5728G-24P version 1.4, JetNet5828G version 1.1d, JetNet6710G-HVDC version 1.1e, and JetNet6710G version 1.1. An attacker may gain access to hard-coded certificates and private keys allowing the attacker to perform man-in-the-middle attacks.
CVE-2017-14027
A Use of Hard-coded Credentials issue was discovered in Korenix JetNet JetNet5018G version 1.4, JetNet5310G version 1.4a, JetNet5428G-2G-2FX version 1.4, JetNet5628G-R version 1.4, JetNet5628G version 1.4, JetNet5728G-24P version 1.4, JetNet5828G version 1.1d, JetNet6710G-HVDC version 1.1e, and JetNet6710G version 1.1. The software uses undocumented hard-coded credentials that may allow an attacker to gain remote access.
CVE-2017-14375
EMC Unisphere for VMAX Virtual Appliance (vApp) versions prior to 8.4.0.15, EMC Solutions Enabler Virtual Appliance versions prior to 8.4.0.15, EMC VASA Virtual Appliance versions prior to 8.4.0.512, and EMC VMAX Embedded Management (eManagement) versions prior to and including 1.4 (Enginuity Release 5977.1125.1125 and earlier) contain an authentication bypass vulnerability that may potentially be exploited by malicious users to compromise the affected system.
CVE-2017-14376
EMC AppSync Server prior to 3.5.0.1 contains database accounts with hardcoded passwords that could potentially be exploited by malicious users to compromise the affected system.
CVE-2017-14992
Lack of content verification in Docker-CE (Also known as Moby) versions 1.12.6-0, 1.10.3, 17.03.0, 17.03.1, 17.03.2, 17.06.0, 17.06.1, 17.06.2, 17.09.0, and earlier allows a remote attacker to cause a Denial of Service via a crafted image layer payload, aka gzip bombing.
CVE-2017-1552
IBM Infosphere BigInsights 4.2.0 and 4.2.5 is vulnerable to link injection. By persuading a victim to click on a specially-crafted URL link, a remote attacker could exploit this vulnerability to conduct various attacks against the vulnerable system, including cross-site scripting, cache poisoning or session hijacking. IBM X-Force ID: 131396.
CVE-2017-1553
IBM Infosphere BigInsights 4.2.0 and 4.2.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 131397.
CVE-2017-15535
MongoDB 3.4.x before 3.4.10, and 3.5.x-development, has a disabled-by-default configuration setting, networkMessageCompressors (aka wire protocol compression), which exposes a vulnerability when enabled that could be exploited by a malicious attacker to deny service or modify memory.
CVE-2017-1554
IBM Infosphere BigInsights 4.2.0 and 4.2.5 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 131398.
CVE-2017-15566
Insecure SPANK environment variable handling exists in SchedMD Slurm before 16.05.11, 17.x before 17.02.9, and 17.11.x before 17.11.0rc2, allowing privilege escalation to root during Prolog or Epilog execution.
CVE-2017-15918
Sera 1.2 stores the user's login password in plain text in their home directory. This makes privilege escalation trivial and also exposes the user and system keychains to local attacks.
CVE-2017-16244
Cross-Site Request Forgery exists in OctoberCMS 1.0.426 (aka Build 426) due to improper validation of CSRF tokens for postback handling, allowing an attacker to successfully take over the victim's account. The attack bypasses a protection mechanism involving X-CSRF headers and CSRF tokens via a certain _handler postback variable.
CVE-2017-16248
The Catalyst-Plugin-Static-Simple module before 0.34 for Perl allows remote attackers to read arbitrary files if there is a '.' character anywhere in the pathname, which differs from the intended policy of allowing access only when the filename itself has a '.' character.
CVE-2017-16352
GraphicsMagick 1.3.26 is vulnerable to a heap-based buffer overflow vulnerability found in the "Display visual image directory" feature of the DescribeImage() function of the magick/describe.c file. One possible way to trigger the vulnerability is to run the identify command on a specially crafted MIFF format file with the verbose flag.
CVE-2017-16353
GraphicsMagick 1.3.26 is vulnerable to a memory information disclosure vulnerability found in the DescribeImage function of the magick/describe.c file, because of a heap-based buffer over-read. The portion of the code containing the vulnerability is responsible for printing the IPTC Profile information contained in the image. This vulnerability can be triggered with a specially crafted MIFF file. There is an out-of-bounds buffer dereference because certain increments are never checked.
CVE-2017-16357
In radare 2.0.1, a memory corruption vulnerability exists in store_versioninfo_gnu_verdef() and store_versioninfo_gnu_verneed() in libr/bin/format/elf/elf.c, as demonstrated by an invalid free. This error is due to improper sh_size validation when allocating memory.
CVE-2017-16358
In radare 2.0.1, an out-of-bounds read vulnerability exists in string_scan_range() in libr/bin/bin.c when doing a string search.
CVE-2017-16359
In radare 2.0.1, a pointer wraparound vulnerability exists in store_versioninfo_gnu_verdef() in libr/bin/format/elf/elf.c.
