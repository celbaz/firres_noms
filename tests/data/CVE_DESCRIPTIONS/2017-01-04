CVE-2014-9911
Stack-based buffer overflow in the ures_getByKeyWithFallback function in common/uresbund.cpp in International Components for Unicode (ICU) before 54.1 for C/C++ allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted uloc_getDisplayName call.
CVE-2014-9912
The get_icu_disp_value_src_php function in ext/intl/locale/locale_methods.c in PHP before 5.3.29, 5.4.x before 5.4.30, and 5.5.x before 5.5.14 does not properly restrict calls to the ICU uresbund.cpp component, which allows remote attackers to cause a denial of service (buffer overflow) or possibly have unspecified other impact via a locale_get_display_name call with a long first argument.
CVE-2016-10112
Cross-site scripting (XSS) vulnerability in the WooCommerce plugin before 2.6.9 for WordPress allows remote authenticated administrators to inject arbitrary web script or HTML by providing crafted tax-rate table values in CSV format.
CVE-2016-10114
SQL injection vulnerability in the "aWeb Cart Watching System for Virtuemart" extension before 2.6.1 for Joomla! allows remote attackers to execute arbitrary SQL commands via vectors involving categorysearch and smartSearch.
CVE-2016-10115
NETGEAR Arlo base stations with firmware 1.7.5_6178 and earlier, Arlo Q devices with firmware 1.8.0_5551 and earlier, and Arlo Q Plus devices with firmware 1.8.1_6094 and earlier have a default password of 12345678, which makes it easier for remote attackers to obtain access after a factory reset or in a factory configuration.
CVE-2016-10116
NETGEAR Arlo base stations with firmware 1.7.5_6178 and earlier, Arlo Q devices with firmware 1.8.0_5551 and earlier, and Arlo Q Plus devices with firmware 1.8.1_6094 and earlier use a pattern of adjective, noun, and three-digit number for the customized password, which makes it easier for remote attackers to obtain access via a dictionary attack.
CVE-2016-6595
** DISPUTED ** The SwarmKit toolkit 1.12.0 for Docker allows remote authenticated users to cause a denial of service (prevention of cluster joins) via a long sequence of join and quit actions.  NOTE: the vendor disputes this issue, stating that this sequence is not "removing the state that is left by old nodes. At some point the manager obviously stops being able to accept new nodes, since it runs out of memory. Given that both for Docker swarm and for Docker Swarmkit nodes are *required* to provide a secret token (it's actually the only mode of operation), this means that no adversary can simply join nodes and exhaust manager resources. We can't do anything about a manager running out of memory and not being able to add new legitimate nodes to the system. This is merely a resource provisioning issue, and definitely not a CVE worthy vulnerability."
CVE-2016-6894
Arista EOS 4.15 before 4.15.8M, 4.16 before 4.16.7M, and 4.17 before 4.17.0F on DCS-7050 series devices allow remote attackers to cause a denial of service (device reboot) by sending crafted packets to the control plane.
CVE-2016-7399
scripts/license.pl in Veritas NetBackup Appliance 2.6.0.x through 2.6.0.4, 2.6.1.x through 2.6.1.2, 2.7.x through 2.7.3, and 3.0.x allow remote attackers to execute arbitrary commands via shell metacharacters in the hostName parameter to appliancews/getLicense.
CVE-2016-7902
Unrestricted file upload vulnerability in the fileUnzip->unzip method in Dotclear before 2.10.3 allows remote authenticated users with permissions to manage media items to execute arbitrary code by uploading a ZIP file containing a file with a crafted extension, as demonstrated by .php.txt or .php%20.
CVE-2016-7903
Dotclear before 2.10.3, when the Host header is not part of the web server routing process, allows remote attackers to modify the password reset address link via the HTTP Host header.
CVE-2016-8670
Integer signedness error in the dynamicGetbuf function in gd_io_dp.c in the GD Graphics Library (aka libgd) through 2.2.3, as used in PHP before 5.6.28 and 7.x before 7.0.13, allows remote attackers to cause a denial of service (stack-based buffer overflow) or possibly have unspecified other impact via a crafted imagecreatefromstring call.
CVE-2016-8860
Tor before 0.2.8.9 and 0.2.9.x before 0.2.9.4-alpha had internal functions that were entitled to expect that buf_t data had NUL termination, but the implementation of or/buffers.c did not ensure that NUL termination was present, which allows remote attackers to cause a denial of service (client, hidden service, relay, or authority crash) via crafted data.
CVE-2016-9137
Use-after-free vulnerability in the CURLFile implementation in ext/curl/curl_file.c in PHP before 5.6.27 and 7.x before 7.0.12 allows remote attackers to cause a denial of service or possibly have unspecified other impact via crafted serialized data that is mishandled during __wakeup processing.
CVE-2016-9138
PHP through 5.6.27 and 7.x through 7.0.12 mishandles property modification during __wakeup processing, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via crafted serialized data, as demonstrated by Exception::__toString with DateInterval::__wakeup.
CVE-2016-9933
Stack consumption vulnerability in the gdImageFillToBorder function in gd.c in the GD Graphics Library (aka libgd) before 2.2.2, as used in PHP before 5.6.28 and 7.x before 7.0.13, allows remote attackers to cause a denial of service (segmentation violation) via a crafted imagefilltoborder call that triggers use of a negative color value.
CVE-2016-9934
ext/wddx/wddx.c in PHP before 5.6.28 and 7.x before 7.0.13 allows remote attackers to cause a denial of service (NULL pointer dereference) via crafted serialized data in a wddxPacket XML document, as demonstrated by a PDORow string.
CVE-2016-9935
The php_wddx_push_element function in ext/wddx/wddx.c in PHP before 5.6.29 and 7.x before 7.0.14 allows remote attackers to cause a denial of service (out-of-bounds read and memory corruption) or possibly have unspecified other impact via an empty boolean element in a wddxPacket XML document.
CVE-2016-9936
The unserialize implementation in ext/standard/var.c in PHP 7.x before 7.0.14 allows remote attackers to cause a denial of service (use-after-free) or possibly have unspecified other impact via crafted serialized data.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-6834.
