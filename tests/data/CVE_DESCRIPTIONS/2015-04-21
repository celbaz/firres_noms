CVE-2014-3586
The default configuration for the Command Line Interface in Red Hat Enterprise Application Platform before 6.4.0 and WildFly (formerly JBoss Application Server) uses weak permissions for .jboss-cli-history, which allows local users to obtain sensitive information via unspecified vectors.
CVE-2014-5361
Multiple cross-site request forgery (CSRF) vulnerabilities in Landesk Management Suite 9.6 and earlier allow remote attackers to hijack the authentication of administrators for requests that (1) start, (2) stop, or (3) restart services via a request to remote/serverServices.aspx.
CVE-2014-5370
Directory traversal vulnerability in the CFChart servlet (com.naryx.tagfusion.cfm.cfchartServlet) in New Atlanta BlueDragon before 7.1.1.18527 allows remote attackers to read or possibly delete arbitrary files via a .. (dot dot) in the QUERY_STRING to cfchart.cfchart.
CVE-2014-8111
Apache Tomcat Connectors (mod_jk) before 1.2.41 ignores JkUnmount rules for subtrees of previous JkMount rules, which allows remote attackers to access otherwise restricted artifacts via unspecified vectors.
CVE-2014-8125
XML external entity (XXE) vulnerability in Drools and jBPM before 6.2.0 allows remote attackers to read arbitrary files or possibly have other unspecified impact via a crafted BPMN2 file.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-9718
The (1) BMDMA and (2) AHCI HBA interfaces in the IDE functionality in QEMU 1.0 through 2.1.3 have multiple interpretations of a function's return value, which allows guest OS users to cause a host OS denial of service (memory consumption or infinite loop, and system crash) via a PRDT with zero complete sectors, related to the bmdma_prepare_buf and ahci_dma_prepare_buf functions.
CVE-2015-0135
IBM Domino 8.5 before 8.5.3 FP6 IF4 and 9.0 before 9.0.1 FP3 IF2 allows remote attackers to execute arbitrary code or cause a denial of service (integer truncation and application crash) via a crafted GIF image, aka SPR KLYH9T7NT9.
CVE-2015-0702
Unrestricted file upload vulnerability in the Custom Prompts upload implementation in Cisco Unified MeetingPlace 8.6(1.9) allows remote authenticated users to execute arbitrary code by using the languageShortName parameter to upload a file that provides shell access, aka Bug ID CSCus95712.
CVE-2015-0703
Cross-site scripting (XSS) vulnerability in the administrative web interface in Cisco Unified MeetingPlace 8.6(1.9) allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka Bug ID CSCus95857.
CVE-2015-1701
Win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Vista SP2, and Server 2008 SP2 allows local users to gain privileges via a crafted application, as exploited in the wild in April 2015, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2015-2041
net/llc/sysctl_net_llc.c in the Linux kernel before 3.19 uses an incorrect data type in a sysctl table, which allows local users to obtain potentially sensitive information from kernel memory or possibly have unspecified other impact by accessing a sysctl entry.
CVE-2015-2042
net/rds/sysctl.c in the Linux kernel before 3.19 uses an incorrect data type in a sysctl table, which allows local users to obtain potentially sensitive information from kernel memory or possibly have unspecified other impact by accessing a sysctl entry.
CVE-2015-2825
Unrestricted file upload vulnerability in sam-ajax-admin.php in the Simple Ads Manager plugin before 2.5.96 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in the directory specified by the path parameter.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-3342
Open redirect vulnerability in the Ubercart Currency Conversion module before 6.x-1.2 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the destination query parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3343
Cross-site request forgery (CSRF) vulnerability in the OPAC module before 7.x-2.3 for Drupal allows remote attackers to hijack the authentication of unspecified victims for requests that remove a mapping via unknown vectors.
CVE-2015-3344
Cross-site scripting (XSS) vulnerability in the Course module 6.x-1.x before 6.x-1.2 and 7.x-1.x before 7.x-1.4 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3345
SQL injection vulnerability in the PHPlist Integration Module before 6.x-1.7 for Drupal allows remote administrators to execute arbitrary SQL commands via unspecified vectors, related to the "phpList database."
CVE-2015-3346
SQL injection vulnerability in the WikiWiki module before 6.x-1.2 for Drupal allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-3347
Cross-site request forgery (CSRF) vulnerability in the Cloudwords for Multilingual Drupal module before 7.x-2.3 for Drupal allows remote attackers to hijack the authentication of unspecified victims via an unknown menu callback.
CVE-2015-3348
Cross-site scripting (XSS) vulnerability in the Cloudwords for Multilingual Drupal module before 7.x-2.3 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3349
Multiple cross-site request forgery (CSRF) vulnerabilities in the Htaccess module before 7.x-2.3 for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) deploy or (2) delete an .htaccess file via unspecified vectors.
CVE-2015-3350
Cross-site request forgery (CSRF) vulnerability in the Todo Filter module before 6.x-1.1 and 7.x-1.x before 7.x-1.1 for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that toggle a task via unspecified vectors.
CVE-2015-3351
Multiple cross-site request forgery (CSRF) vulnerabilities in the Log Watcher module before 6.x-1.2 for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) enable, (2) disable, or (3) delete a report via unspecified vectors.
CVE-2015-3352
Multiple cross-site request forgery (CSRF) vulnerabilities in the Jammer module before 6.x-1.8 and 7.x-1.x before 7.x-1.4 for Drupal allow remote attackers to hijack the authentication of administrators for requests that delete a setting for (1) hidden form elements or (2) status messages via unspecified vectors, related to "report administration."
CVE-2015-3353
Cross-site scripting (XSS) vulnerability in the Field Display Label module before 7.x-1.3 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via the alternate field label in content types settings.
CVE-2015-3354
Cross-site request forgery (CSRF) vulnerability in the Wishlist module before 6.x-2.7 and 7.x-2.x before 7.x-2.7 for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that delete wishlist purchase intentions via unspecified vectors.
CVE-2015-3355
Multiple cross-site request forgery (CSRF) vulnerabilities in the Batch Jobs module before 7.x-1.2 for Drupal allow remote attackers to hijack the authentication of certain users for requests that (1) delete a batch job record or (2) execute a task via unspecified vectors.
CVE-2015-3356
Multiple cross-site request forgery (CSRF) vulnerabilities in the Tadaa! module before 7.x-1.4 for Drupal allow remote attackers to hijack the authentication of arbitrary users for requests that (1) enable or (2) disable modules or (3) change variables via unspecified vectors.
CVE-2015-3357
Cross-site scripting (XSS) vulnerability in the Wishlist module before 6.x-2.7 and 7.x-2.x before 7.x-2.7 for Drupal allows remote authenticated users with the "access wishlists" permission to inject arbitrary web script or HTML via unspecified vectors, which are not properly handled in a log message.
CVE-2015-3358
Multiple open redirect vulnerabilities in the Tadaa! module before 7.x-1.4 for Drupal allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in a destination parameter, related to callbacks that (1) enable and disable modules or (2) change variables.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3359
Multiple cross-site scripting (XSS) vulnerabilities in the Room Reservations module before 7.x-1.1 for Drupal allow remote authenticated users with the "Administer the room reservations system" permission to inject arbitrary web script or HTML via the (1) node title of a "Room Reservations Category" or (2) body of a "Room Reservations Room" node.
CVE-2015-3360
Cross-site scripting (XSS) vulnerability in the Term Merge module before 7.x-1.2 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-3361
Cross-site scripting (XSS) vulnerability in the Linkit module before 7.x-2.7 and 7.x-3.x before 7.x-3.3 for Drupal, when the node search plugin is enabled, allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3362
Cross-site scripting (XSS) vulnerability in the Video module before 7.x-2.11 for Drupal, when using the video WYSIWYG plugin, allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3363
Cross-site request forgery (CSRF) vulnerability in the Contact Form Fields module before 6.x-2.3 for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete fields via unspecified vectors.
CVE-2015-3364
Cross-site scripting (XSS) vulnerability in the Content Analysis module before 6.x-1.7 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, which are not properly handled in a log message.
CVE-2015-3365
Cross-site scripting (XSS) vulnerability in the nodeauthor module for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a Profile2 field in a provided block.
CVE-2015-3366
Cross-site request forgery (CSRF) vulnerability in the Alfresco module before 6.x-1.3 for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that delete an alfresco node via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2411523">advisory</a>:  "A malicious user could cause a user to delete alfresco nodes by getting the user's browser to make a request to a specially-crafted URL while the user was logged in."  Only integrity and availability are affected.
CVE-2015-3367
Multiple cross-site request forgery (CSRF) vulnerabilities in the Patterns module before 7.x-2.2 for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) restore, (2) publish, or (3) unpublish a pattern via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2411539">advisory</a>:  "A malicious user could cause an administrator to restore, publish and unpublish patterns by getting the administrator's browser to make a request to a specially-crafted URL."  Only integrity and availability are affected.
CVE-2015-3368
Cross-site scripting (XSS) vulnerability in the administration user interface in the Classified Ads module before 6.x-3.1 and 7.x-3.x before 7.x-3.1 for Drupal allows remote authenticated users with the "administer taxonomy" permission to inject arbitrary web script or HTML via a category name.
CVE-2015-3369
Cross-site scripting (XSS) vulnerability in the Taxonews module before 6.x-1.2 and 7.x-1.x before 7.x-1.1 for Drupal allows remote authenticated users with the "administer taxonomy" permission to inject arbitrary web script or HTML via a term name in a block.
CVE-2015-3370
Cross-site request forgery (CSRF) vulnerability in the Node Invite module before 6.x-2.5 for Drupal allows remote attackers to hijack the authentication of users with the "node_invite_can_manage_invite" permission for requests that re-enable node invitations via unspecified vectors.
CVE-2015-3371
Open redirect vulnerability in the Node Invite module before 6.x-2.5 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via the destination parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3372
Cross-site scripting (XSS) vulnerability in the Node Invite module before 6.x-2.5 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3373
The Amazon AWS module before 7.x-1.3 for Drupal uses the base URL and AWS access key to generate the access token, which makes it easier for remote attackers to guess the token value and create backups via a crafted URL.
CVE-2015-3374
Multiple cross-site request forgery (CSRF) vulnerabilities in the Corner module for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) enable or (2) disable corners via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2411741">advisory</a>:  "A malicious user can cause an administrator to enable and disable corners by getting the administrator's browser to make a request to a specially-crafted URL while the administrator is logged in."  Only integrity and availability are affected.
CVE-2015-3375
Cross-site request forgery (CSRF) vulnerability in the Shibboleth Authentication module before 6.x-4.1 and 7.x-4.x before 7.x-4.1 for Drupal allows remote attackers to hijack the authentication of administrators for requests that delete user role matching rules via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2411737">advisory</a>:  "A malicious attacker can delete matching rules from the list by getting the administrator's browser to make a request to a specially-crafted URL while the administrator is logged in."  Only integrity and availability are affected
CVE-2015-3376
Cross-site scripting (XSS) vulnerability in the Quizzler module before 7-x.1.16 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3378
Open redirect vulnerability in the Views module before 6.x-2.18, 6.x-3.x before 6.x-3.2, and 7.x-3.x before 7.x-3.10 for Drupal, when the Views UI submodule is enabled, allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via vectors related to the break lock page for edited views.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3379
The Views module before 6.x-2.18, 6.x-3.x before 6.x-3.2, and 7.x-3.x before 7.x-3.10 for Drupal does not properly restrict access to the default views configurations, which allows remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2015-3380
Multiple cross-site request forgery (CSRF) vulnerabilities in the Feature Set module for Drupal allow remote attackers to hijack the authentication of administrators for requests that (1) enable or (2) disable a module via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2424409">advisory</a>:  "A malicious user can cause an administrator to enable and disable modules by getting the administrator's browser to make a request to a specially-crafted URL."  Only integrity and availability are affected.
CVE-2015-3381
Cross-site scripting (XSS) vulnerability in the Node basket module for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-3382
Multiple cross-site request forgery (CSRF) vulnerabilities in the Node basket module for Drupal allow remote attackers to hijack the authentication of arbitrary users for requests that (1) add or (2) remove nodes from a basket via unspecified vectors.
Per the <a href="https://www.drupal.org/node/2424419">advisory</a>:  "A malicious user can cause another user to add/remove nodes of the basket by getting his browser to make a request to a specially-crafted URL."  Only integrity and availability are affected.
CVE-2015-3383
Open redirect vulnerability in the Node basket module for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3384
Cross-site scripting (XSS) vulnerability in the Bank Account Listing Page in the Commerce Balanced Payments module for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-3385
Cross-site scripting (XSS) vulnerability in the Taxonomy Path module before 7.x-1.2 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via the "Link to path" field formatter.
CVE-2015-3386
Cross-site scripting (XSS) vulnerability in the Node Access Product module for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3387
Multiple cross-site scripting (XSS) vulnerabilities in the Taxonomy Tools module before 7.x-1.4 for Drupal allow remote authenticated users to inject arbitrary web script or HTML via a (1) node or (2) taxonomy term title.
CVE-2015-3388
Cross-site request forgery (CSRF) vulnerability in the Commerce Balanced Payments module for Drupal allows remote attackers to hijack the authentication of arbitrary users for requests that delete the user's configured bank accounts via unspecified vectors.
CVE-2015-3389
Cross-site scripting (XSS) vulnerability in the Download counts report page in the Public Download Count module (pubdlcnt) 7.x-1.x-dev and earlier for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-3390
Cross-site scripting (XSS) vulnerability in the Facebook Album Fetcher module for Drupal allows remote authenticated users with the "access administration pages" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-3391
The Path Breadcrumbs module before 7.x-3.2 for Drupal allows remote attackers to bypass intended access restrictions and obtain sensitive node titles by reading a 403 Not Found page.
CVE-2015-3392
Cross-site scripting (XSS) vulnerability in the Ajax Timeline module before 7.x-1.1 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a node title.
CVE-2015-3393
Open redirect vulnerability in the Commerce WeDeal module before 7.x-1.3 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via an unspecified parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
