CVE-2015-8156
Unquoted Windows search path vulnerability in EEDService in Symantec Endpoint Encryption (SEE) 11.x before 11.1.1 allows local users to gain privileges via a Trojan horse executable file in the %SYSTEMDRIVE% directory, as demonstrated by program.exe.
<a href="http://cwe.mitre.org/data/definitions/426.html">CWE-426: Untrusted Search Path</a>
CVE-2015-8530
Stack-based buffer overflow in the Initialize function in an ActiveX control in IBM SPSS Statistics 19 and 20 before 20.0.0.2-IF0008, 21 before 21.0.0.2-IF0010, 22 before 22.0.0.2-IF0011, 23 before 23.0.0.3-IF0001, and 24 before 24.0.0.0-IF0003 allows remote authenticated users to execute arbitrary code via a long argument.
CVE-2016-1206
The WPS implementation on I-O DATA DEVICE WN-GDN/R3, WN-GDN/R3-C, WN-GDN/R3-S, and WN-GDN/R3-U devices does not limit PIN guesses, which allows remote attackers to obtain network access via a brute-force attack.
CVE-2016-1207
Cross-site scripting (XSS) vulnerability on I-O DATA DEVICE WN-G300R devices with firmware 1.12 and earlier, WN-G300R2 devices with firmware 1.12 and earlier, and WN-G300R3 devices with firmware 1.01 and earlier allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1208
The server in Apple FileMaker before 14.0.4 on OS X allows remote attackers to read PHP source code via unspecified vectors.
CVE-2016-1209
The Ninja Forms plugin before 2.9.42.1 for WordPress allows remote attackers to conduct PHP object injection attacks via crafted serialized values in a POST request.
CVE-2016-1399
The packet-processing microcode in Cisco IOS 15.2(2)EA, 15.2(2)EA1, 15.2(2)EA2, and 15.2(4)EA on Industrial Ethernet 4000 devices and 15.2(2)EB and 15.2(2)EB1 on Industrial Ethernet 5000 devices allows remote attackers to cause a denial of service (packet data corruption) via crafted IPv4 ICMP packets, aka Bug ID CSCuy13431.
CVE-2016-1660
Blink, as used in Google Chrome before 50.0.2661.94, mishandles assertions in the WTF::BitArray and WTF::double_conversion::Vector classes, which allows remote attackers to cause a denial of service (out-of-bounds write) or possibly have unspecified other impact via a crafted web site.
CVE-2016-1661
Blink, as used in Google Chrome before 50.0.2661.94, does not ensure that frames satisfy a check for the same renderer process in addition to a Same Origin Policy check, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via a crafted web site, related to BindingSecurity.cpp and DOMWindow.cpp.
CVE-2016-1662
extensions/renderer/gc_callback.cc in Google Chrome before 50.0.2661.94 does not prevent fallback execution once the Garbage Collection callback has started, which allows remote attackers to cause a denial of service (use-after-free) or possibly have unspecified other impact via unknown vectors.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-1663
The SerializedScriptValue::transferArrayBuffers function in WebKit/Source/bindings/core/v8/SerializedScriptValue.cpp in the V8 bindings in Blink, as used in Google Chrome before 50.0.2661.94, mishandles certain array-buffer data structures, which allows remote attackers to cause a denial of service (use-after-free) or possibly have unspecified other impact via a crafted web site.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-1664
The HistoryController::UpdateForCommit function in content/renderer/history_controller.cc in Google Chrome before 50.0.2661.94 mishandles the interaction between subframe forward navigations and other forward navigations, which allows remote attackers to spoof the address bar via a crafted web site.
CVE-2016-1665
The JSGenericLowering class in compiler/js-generic-lowering.cc in Google V8, as used in Google Chrome before 50.0.2661.94, mishandles comparison operators, which allows remote attackers to obtain sensitive information via crafted JavaScript code.
CVE-2016-1666
Multiple unspecified vulnerabilities in Google Chrome before 50.0.2661.94 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2016-1667
The TreeScope::adoptIfNeeded function in WebKit/Source/core/dom/TreeScope.cpp in the DOM implementation in Blink, as used in Google Chrome before 50.0.2661.102, does not prevent script execution during node-adoption operations, which allows remote attackers to bypass the Same Origin Policy via a crafted web site.
CVE-2016-1668
The forEachForBinding function in WebKit/Source/bindings/core/v8/Iterable.h in the V8 bindings in Blink, as used in Google Chrome before 50.0.2661.102, uses an improper creation context, which allows remote attackers to bypass the Same Origin Policy via a crafted web site.
CVE-2016-1669
The Zone::New function in zone.cc in Google V8 before 5.0.71.47, as used in Google Chrome before 50.0.2661.102, does not properly determine when to expand certain memory allocations, which allows remote attackers to cause a denial of service (buffer overflow) or possibly have unspecified other impact via crafted JavaScript code.
CVE-2016-1670
Race condition in the ResourceDispatcherHostImpl::BeginRequest function in content/browser/loader/resource_dispatcher_host_impl.cc in Google Chrome before 50.0.2661.102 allows remote attackers to make arbitrary HTTP requests by leveraging access to a renderer process and reusing a request ID.
CVE-2016-1671
Google Chrome before 50.0.2661.102 on Android mishandles / (slash) and \ (backslash) characters, which allows attackers to conduct directory traversal attacks via a file: URL, related to net/base/escape.cc and net/base/filename_util.cc.
CVE-2016-2015
HPE System Management Homepage before 7.5.5 allows local users to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-2016
Base-VxFS-50 B.05.00.01 through B.05.00.02, Base-VxFS-501 B.05.01.0 through B.05.01.03, and Base-VxFS-51 B.05.10.00 through B.05.10.02 on HPE HP-UX 11iv3 with VxFS 5.0, VxFS 5.0.1, and VxFS 5.1SP1 mishandles ACL inheritance for default:class: entries, default:other: entries, and default:user: entries, which allows local users to bypass intended access restrictions by leveraging the configuration of a parent directory.
CVE-2016-2296
Meteocontrol WEB'log Basic 100, Light, Pro, and Pro Unlimited does not require authentication for "post-admin" login pages, which allows remote attackers to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-2297
Meteocontrol WEB'log Basic 100, Light, Pro, and Pro Unlimited allows remote attackers to execute arbitrary commands via an "access command shell-like feature."
CVE-2016-2298
Meteocontrol WEB'log Basic 100, Light, Pro, and Pro Unlimited allows remote attackers to obtain sensitive cleartext information via unspecified vectors.
CVE-2016-4325
Lantronix xPrintServer devices with firmware before 5.0.1-65 have hardcoded credentials, which allows remote attackers to obtain root access via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/798.html">CWE-798: Use of Hard-coded Credentials</a>
