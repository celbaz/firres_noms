CVE-2009-5031
ModSecurity before 2.5.11 treats request parameter values containing single quotes as files, which allows remote attackers to bypass filtering rules and perform other attacks such as cross-site scripting (XSS) attacks via a single quote in a request parameter in the Content-Disposition field of a request with a multipart/form-data Content-Type header.
CVE-2011-2199
Buffer overflow in tftp-hpa before 5.1 allows remote attackers to cause a denial of service and possibly execute arbitrary code via the utimeout option.
CVE-2011-3148
Stack-based buffer overflow in the _assemble_line function in modules/pam_env/pam_env.c in Linux-PAM (aka pam) before 1.1.5 allows local users to cause a denial of service (crash) and possibly execute arbitrary code via a long string of white spaces at the beginning of the ~/.pam_environment file.
CVE-2011-3149
The _expand_arg function in the pam_env module (modules/pam_env/pam_env.c) in Linux-PAM (aka pam) before 1.1.5 does not properly handle when environment variable expansion can overflow, which allows local users to cause a denial of service (CPU consumption).
CVE-2011-3464
Off-by-one error in the png_formatted_warning function in pngerror.c in libpng 1.5.4 through 1.5.7 might allow remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via unspecified vectors, which trigger a stack-based buffer overflow.
CVE-2012-2088
Integer signedness error in the TIFFReadDirectory function in tif_dirread.c in libtiff 3.9.4 and earlier allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a negative tile depth in a tiff image, which triggers an improper conversion between signed and unsigned types, leading to a heap-based buffer overflow.
CVE-2012-2113
Multiple integer overflows in tiff2pdf in libtiff before 4.0.2 allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted tiff image, which triggers a heap-based buffer overflow.
CVE-2012-2737
The user_change_icon_file_authorized_cb function in /usr/libexec/accounts-daemon in AccountsService before 0.6.22 does not properly check the UID when copying an icon file to the system cache directory, which allows local users to read arbitrary files via a race condition.
CVE-2012-2738
The VteTerminal in gnome-terminal (vte) before 0.32.2 allows remote authenticated users to cause a denial of service (long loop and CPU consumption) via an escape sequence with a large repeat count value.
CVE-2012-2751
ModSecurity before 2.6.6, when used with PHP, does not properly handle single quotes not at the beginning of a request parameter value in the Content-Disposition field of a request with a multipart/form-data Content-Type header, which allows remote attackers to bypass filtering rules and perform other attacks such as cross-site scripting (XSS) attacks.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2009-5031.
CVE-2012-3356
The remote SVN views functionality (lib/vclib/svn/svn_ra.py) in ViewVC before 1.1.15 does not properly perform authorization, which allows remote attackers to bypass intended access restrictions via unspecified vectors.
CVE-2012-3357
The SVN revision view (lib/vclib/svn/svn_repos.py) in ViewVC before 1.1.15 does not properly handle log messages when a readable path is copied from an unreadable path, which allows remote attackers to obtain sensitive information, related to a "log msg leak."
CVE-2012-3360
Directory traversal vulnerability in virt/disk/api.py in OpenStack Compute (Nova) Folsom (2012.2) and Essex (2012.1), when used over libvirt-based hypervisors, allows remote authenticated users to write arbitrary files to the disk image via a .. (dot dot) in the path attribute of a file element.
CVE-2012-3361
virt/disk/api.py in OpenStack Compute (Nova) Folsom (2012.2), Essex (2012.1), and Diablo (2011.3) allows remote authenticated users to overwrite arbitrary files via a symlink attack on a file in an image.
CVE-2012-3383
The map_meta_cap function in wp-includes/capabilities.php in WordPress 3.4.x before 3.4.2, when the multisite feature is enabled, does not properly assign the unfiltered_html capability, which allows remote authenticated users to bypass intended access restrictions and conduct cross-site scripting (XSS) attacks by leveraging the Administrator or Editor role and composing crafted text.
CVE-2012-3384
Cross-site request forgery (CSRF) vulnerability in the customizer in WordPress before 3.4.1 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2012-3385
WordPress before 3.4.1 does not properly restrict access to post contents such as private or draft posts, which allows remote authors or contributors to obtain sensitive information via unknown vectors.
CVE-2012-4045
Multiple heap-based buffer overflows in bmp.w5s in Winamp before 5.63 build 3235 allow remote attackers to execute arbitrary code via the (1) strf chunk in BI_RGB or (2) UYVY video data in an AVI file, or (3) decompressed TechSmith Screen Capture Codec (TSCC) data in an AVI file.
