CVE-2015-4029
Cross-site scripting (XSS) vulnerability in the WebGUI in pfSense before 2.2.3 allows remote attackers to inject arbitrary web script or HTML via the zone parameter in a del action to services_captiveportal_zones.php.
CVE-2015-4425
Directory traversal vulnerability in pimcore before build 3473 allows remote authenticated users with the "assets" permission to create or write to arbitrary files via a .. (dot dot) in the dir parameter to admin/asset/add-asset-compatibility.
CVE-2015-4426
SQL injection vulnerability in pimcore before build 3473 allows remote attackers to execute arbitrary SQL commands via the filter parameter to admin/asset/grid-proxy.
CVE-2015-4670
Directory traversal vulnerability in the AjaxFileUpload control in DevExpress AJAX Control Toolkit (aka AjaxControlToolkit) before 15.1 allows remote attackers to write to arbitrary files via a .. (dot dot) in the fileId parameter to AjaxFileUploadHandler.axd.
CVE-2015-5481
Cross-site scripting (XSS) vulnerability in forms/panels.php in the GD bbPress Attachments plugin before 2.3 for WordPress allows remote attackers to inject arbitrary web script or HTML via the tab parameter in the gdbbpress_attachments page to wp-admin/edit.php.
CVE-2015-5482
Directory traversal vulnerability in the GD bbPress Attachments plugin before 2.3 for WordPress allows remote administrators to include and execute arbitrary local files via a .. (dot dot) in the tab parameter in the gdbbpress_attachments page to wp-admin/edit.php.
CVE-2015-5485
Cross-site scripting (XSS) vulnerability in the Event Import page (import-eventbrite-events.php) in the Modern Tribe Eventbrite Tickets plugin before 3.10.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the "error" parameter to wp-admin/edit.php.
CVE-2015-5487
Cross-site scripting (XSS) vulnerability in the Camtasia Relay module 6.x-2.x before 6.x-3.2 and 7.x-2.x before 7.x-1.3 for Drupal allows remote authenticated users with the "view meta information" permission to inject arbitrary web script or HTML via unspecified vectors related to the meta access tab.
CVE-2015-5488
Cross-site scripting (XSS) vulnerability in the MailChimp Signup submodule in the MailChimp module 7.x-3.x before 7.x-3.3 for Drupal allows remote authenticated users with the "administer mailchimp" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5489
Cross-site scripting (XSS) vulnerability in the Smart Trim module 7.x-1.x before 7.x-1.5 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via vectors involving the field settings form.
CVE-2015-5490
The _views_fetch_data method in includes/cache.inc in the Views module 7.x-3.5 through 7.x-3.10 for Drupal does not rebuild the full cache if the static cache is not empty, which allows remote attackers to bypass intended filters and obtain access to hidden content via unspecified vectors.
CVE-2015-5491
The Dynamic display block module 7.x-1.x before 7.x-1.1 for Drupal allows remote authenticated users to bypass intended access restrictions and read sensitive titles by leveraging the "administer ddblock" permission.
CVE-2015-5492
Cross-site scripting (XSS) vulnerability in the Video Consultation module for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5493
The Entityform Block module 7.x-1.x before 7.x-1.3 for Drupal does not properly check permissions when a form is locked to a role, which allows remote attackers to obtain access to certain entityforms via unspecified vectors.
CVE-2015-5494
Cross-site scripting (XSS) vulnerability in the Webform Matrix Component module 7.x-4.x before 7.x-4.13 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5495
Cross-site scripting (XSS) vulnerability in the Mobile sliding menu module 7.x-2.x before 7.x-2.1 for Drupal allows remote authenticated users with the "administer menu" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5496
The pass2pdf module for Drupal does not restrict access to generated PDF files, which allows remote attackers to obtain user passwords via unspecified vectors.
CVE-2015-5497
Cross-site scripting (XSS) vulnerability in the Web Links module 6.x-2.x before 6.x-2.6 and 7.x-1.x before 7.x-1.0 for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5498
The Shipwire API module 7.x-1.x before 7.x-1.03 for Drupal does not check the view permission for the shipments overview (admin/shipwire/shipments), which allows remote attackers to obtain sensitive information via a request to the page.
CVE-2015-5499
The Navigate module for Drupal does not properly check permissions, which allows remote authenticated users to modify custom widgets and create widget database records by leveraging the "navigate view" permission.
CVE-2015-5500
Cross-site scripting (XSS) vulnerability in the Navigate module for Drupal allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5501
The Hostmaster (Aegir) module 6.x-2.x before 6.x-2.4 and 7.x-3.x before 7.x-3.0-beta2 for Drupal allows remote attackers to execute arbitrary PHP code via a crafted file in the directory used to write Apache vhost files for hosted sites in a multi-site environment.
CVE-2015-5502
The Storage API module 7.x-1.x before 7.x-1.8 for Drupal does not properly restrict access to Storage API fields attached to entities that are not nodes, which allows remote attackers to have unspecified impact via unknown vectors.
CVE-2015-5503
Open redirect vulnerability in the Chamilo integration module 7.x-1.x before 7.x-1.2 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified parameters.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-5504
SQL injection vulnerability in the Novalnet Payment Module Ubercart module for Drupal allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-5505
The HTTP Strict Transport Security (HSTS) module 6.x-1.x before 6.x-1.1 and 7.x-1.x before 7.x-1.2 for Drupal does not properly implement the "include subdomains" directive, which causes the HSTS policy to not be applied to subdomains and allows man-in-the-middle attackers to have unspecified impact via unknown vectors.
CVE-2015-5506
The Apache Solr Real-Time module 7.x-1.x before 7.x-1.2 for Drupal does not check the status of an entity when indexing, which allows remote attackers to obtain information about unpublished content via a search.
CVE-2015-5507
Cross-site scripting (XSS) vulnerability in the Inline Entity Form module 7.x-1.x before 7.x-1.6 for Drupal allows remote authenticated users with permission to create or edit fields to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5508
Cross-site request forgery (CSRF) vulnerability in the XC NCIP Provider module in the eXtensible Catalog (XC) Drupal Toolkit allows remote attackers to hijack the authentication of users with the "administer ncip providers" permission for requests that alter NCIP providers via a crafted request.
CVE-2015-5509
The Administration Views module 7.x-1.x before 7.x-1.4 for Drupal, when used with other unspecified modules, does not properly grant access to administration pages, which allows remote administrators to bypass intended restrictions via unspecified vectors.
CVE-2015-5510
Open redirect vulnerability in the Content Construction Kit (CCK) 6.x-2.x before 6.x-2.10 for Drupal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via the destinations parameter, related to administration pages.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-5511
The HybridAuth Social Login module 7.x-2.x before 7.x-2.13 for Drupal allows remote attackers to bypass the user registration by administrator only configuration and create an account via a social login.
CVE-2015-5512
The me aliases module 6.x-2.x before 6.x-2.10 and 7.x-1.x before 7.x-1.2 for Drupal allows remote attackers to access Views using the "me" user argument handler by substituting "me" for a user id in a URL.
CVE-2015-5513
Cross-site scripting (XSS) vulnerability in the Shibboleth authentication module 6.x-4.x before 6.x-4.2 and 7.x-4.x before 7.x-4.2 for Drupal allows remote authenticated users with the "Administer blocks" permission to inject arbitrary web script or HTML via unspecified vectors related to a login link.
CVE-2015-5514
Cross-site scripting (XSS) vulnerability in the Migrate module 7.x-2.x before 7.x-2.8 for Drupal, when the migrate_ui submodule is enabled, allows user-assisted remote attackers to inject arbitrary web script or HTML via a destination field label.
CVE-2015-5515
The Views Bulk Operations (VBO) module 6.x-1.x and 7.x-3.x before 7.x-3.3 for Drupal, when the bulk operation for changing Roles is enabled, allows remote authenticated users to edit user accounts and add arbitrary roles to the accounts by leveraging access to a user account listing view with VBO enabled.
CVE-2015-5599
Multiple SQL injection vulnerabilities in upload.php in the Powerplay Gallery plugin 3.3 for WordPress allow remote attackers to execute arbitrary SQL commands via the (1) albumid or (2) name parameter.
CVE-2015-5681
Unrestricted file upload vulnerability in upload.php in the Powerplay Gallery plugin 3.3 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in *_uploadfolder/big/.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-6508
Cross-site scripting (XSS) vulnerability in pfSense before 2.2.3 allows remote attackers to inject arbitrary web script or HTML via the descr parameter in a "new" action to system_authservers.php.
CVE-2015-6509
Multiple cross-site scripting (XSS) vulnerabilities in pfSense before 2.2.3 allow remote attackers to inject arbitrary web script or HTML via the (1) proxypass parameter to system_advanced_misc.php; (2) adaptiveend, (3) adaptivestart, (4) maximumstates, (5) maximumtableentries, or (6) aliasesresolveinterval parameter to system_advanced_firewall.php; (7) proxyurl, (8) proxyuser, or (9) proxyport parameter to system_advanced_misc.php; or (10) name, (11) notification_name, (12) ipaddress, (13) password, (14) smtpipaddress, (15) smtpport, (16) smtpfromaddress, (17) smtpnotifyemailaddress, (18) smtpusername, or (19) smtppassword parameter to system_advanced_notifications.php.
CVE-2015-6510
Multiple cross-site scripting (XSS) vulnerabilities in pfSense before 2.2.3 allow remote attackers to inject arbitrary web script or HTML via the (1) srctrack, (2) use_mfs_tmp_size, or (3) use_mfs_var_size parameter to system_advanced_misc.php; the (4) port, (5) snaplen, or (6) count parameter to diag_packet_capture.php; the (7) pppoe_resethour, (8) pppoe_resetminute, (9) wpa_group_rekey, or (10) wpa_gmk_rekey parameter to interfaces.php; the (11) pppoe_resethour or (12) pppoe_resetminute parameter to interfaces_ppps_edit.php; the (13) member[] parameter to interfaces_qinq_edit.php; the (14) port or (15) retry parameter to load_balancer_pool_edit.php; the (16) pkgrepourl parameter to pkg_mgr_settings.php; the (17) zone parameter to services_captiveportal.php; the port parameter to (18) services_dnsmasq.php or (19) services_unbound.php; the (20) cache_max_ttl or (21) cache_min_ttl parameter to services_unbound_advanced.php; the (22) sshport parameter to system_advanced_admin.php; the (23) id, (24) tunable, (25) descr, or (26) value parameter to system_advanced_sysctl.php; the (27) firmwareurl, (28) repositoryurl, or (29) branch parameter to system_firmware_settings.php; the (30) pfsyncpeerip, (31) synchronizetoip, (32) username, or (33) passwordfld parameter to system_hasync.php; the (34) maxmss parameter to vpn_ipsec_settings.php; the (35) ntp_server1, (36) ntp_server2, (37) wins_server1, or (38) wins_server2 parameter to vpn_openvpn_csc.php; or unspecified parameters to (39) load_balancer_relay_action.php, (40) load_balancer_relay_action_edit.php, (41) load_balancer_relay_protocol.php, or (42) load_balancer_relay_protocol_edit.php.
CVE-2015-6511
Cross-site scripting (XSS) vulnerability in pfSense before 2.2.3 allows remote attackers to inject arbitrary web script or HTML via the server[] parameter to services_ntpd.php.
CVE-2015-6512
SQL injection vulnerability in the get_messages function in server/plugins/chatroom/chatroom.php in FreiChat 9.6 allows remote attackers to execute arbitrary SQL commands via the time parameter to server/freichat.php.
CVE-2015-6513
Multiple SQL injection vulnerabilities in the J2Store (com_j2store) extension before 3.1.7 for Joomla! allow remote attackers to execute arbitrary SQL commands via the (1) sortby or (2) manufacturer_ids[] parameter to index.php.
CVE-2015-6514
Cross-site scripting (XSS) vulnerability in the Dashboard in Splunk Enterprise 6.2.x before 6.2.4 and Splunk Light 6.2.x before 6.2.4 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-6515
Cross-site scripting (XSS) vulnerability in Splunk Web in Splunk Enterprise 6.2.x before 6.2.4, 6.1.x before 6.1.8, 6.0.x before 6.0.9, and 5.0.x before 5.0.13 and Splunk Light 6.2.x before 6.2.4 allows remote attackers to inject arbitrary web script or HTML via a header.
CVE-2015-6516
SQL injection vulnerability in cygnux.org sysPass 1.0.9 and earlier allows remote authenticated users to execute arbitrary SQL commands via the search parameter to ajax/ajax_search.php.
CVE-2015-6517
Cross-site request forgery (CSRF) vulnerability in phpLiteAdmin 1.1 allows remote attackers to hijack the authentication of users for requests that drop database tables via the droptable parameter to phpliteadmin.php.
CVE-2015-6518
Multiple cross-site scripting (XSS) vulnerabilities in phpLiteAdmin 1.1 allow remote attackers to inject arbitrary web script or HTML via the (1) PATH_INFO, (2) droptable parameter, or (3) table parameter to phpliteadmin.php.
CVE-2015-6519
SQL injection vulnerability in Arab Portal 3 allows remote attackers to execute arbitrary SQL commands via the showemail parameter in a signup action to members.php.
