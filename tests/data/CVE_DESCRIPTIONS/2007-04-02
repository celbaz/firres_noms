CVE-2007-1793
SPBBCDrv.sys in Symantec Norton Personal Firewall 2006 9.1.0.33 and 9.1.1.7 does not validate certain arguments before being passed to hooked SSDT function handlers, which allows local users to cause a denial of service (crash) or possibly execute arbitrary code via crafted arguments to the (1) NtCreateMutant and (2) NtOpenEvent functions.  NOTE: it was later reported that Norton Internet Security 2008 15.0.0.60, and possibly other versions back to 2006, are also affected.
CVE-2007-1794
The Javascript engine in Mozilla 1.7 and earlier on Sun Solaris 8, 9, and 10 might allow remote attackers to execute arbitrary code via vectors involving garbage collection that causes deletion of a temporary object that is still being used.  NOTE: this issue might be related to CVE-2006-3805.
CVE-2007-1795
JCcorp URLshrink 1.3.1 allows remote attackers to execute arbitrary PHP code via the email address field in an HTML link.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1796
Multiple unspecified vulnerabilities in JCcorp URLshrink before 1.3.2 have unspecified attack vectors and impact.
CVE-2007-1797
Multiple integer overflows in ImageMagick before 6.3.3-5 allow remote attackers to execute arbitrary code via (1) a crafted DCM image, which results in a heap-based overflow in the ReadDCMImage function, or (2) the (a) colors or (b) comments field in a crafted XWD image, which results in a heap-based overflow in the ReadXWDImage function, different issues than CVE-2007-1667.
CVE-2007-1798
Buffer overflow in the drmgr command in IBM AIX 5.2 and 5.3 allows local users to cause a denial of service (crash) and possibly execute arbitrary code via a long path name.
CVE-2007-1799
Directory traversal vulnerability in torrent.cpp in KTorrent before 2.1.3 only checks for the ".." string, which allows remote attackers to overwrite arbitrary files via modified ".." sequences in a torrent filename, as demonstrated by "../" sequences, due to an incomplete fix for CVE-2007-1384.
CVE-2007-1800
Cisco Secure ACS does not require authentication when Cisco Trust Agent (CTA) transmits posture information, which might allow remote attackers to gain network access via a spoofed Network Endpoint Assessment posture, aka "NACATTACK." NOTE: this attack might be limited to authenticated users and devices.
CVE-2007-1801
Directory traversal vulnerability in inc/lang.php in sBLOG 0.7.3 Beta allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the conf_lang_default parameter, as demonstrated by injecting PHP sequences into an Apache HTTP Server log file, which is then included by inc/lang.php.
CVE-2007-1802
Cross-site scripting (XSS) vulnerability in MailDwarf 3.01 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-1803
Unspecified vulnerability in MailDwarf 3.01 and earlier allows remote attackers to send e-mail to addresses different from the configured addresses.
CVE-2007-1804
PulseAudio 0.9.5 allows remote attackers to cause a denial of service (daemon crash) via (1) a PA_PSTREAM_DESCRIPTOR_LENGTH value of FRAME_SIZE_MAX_ALLOW sent on TCP port 9875, which triggers a p->export assertion failure in do_read; (2) a PA_PSTREAM_DESCRIPTOR_LENGTH value of 0 sent on TCP port 9875, which triggers a length assertion failure in pa_memblock_new; or (3) an empty packet on UDP port 9875, which triggers a t assertion failure in pa_sdp_parse; and allows remote authenticated users to cause a denial of service (daemon crash) via a crafted packet on TCP port 9875 that (4) triggers a maxlength assertion failure in pa_memblockq_new, (5) triggers a size assertion failure in pa_xmalloc, or (6) plays a certain sound file.
CVE-2007-1805
SQL injection vulnerability in genre.php in the debaser 0.92 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the genreid parameter.
CVE-2007-1806
SQL injection vulnerability in categos.php in the RM+Soft Gallery (rmgallery) 1.0 module for Xoops allows remote attackers to execute arbitrary SQL commands via the idcat parameter.
CVE-2007-1807
SQL injection vulnerability in modules/myalbum/viewcat.php in the myAlbum-P 2.0 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2007-1808
SQL injection vulnerability in show.php in the Camportail 1.1 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the camid parameter in a showcam action.
CVE-2007-1809
Multiple PHP remote file inclusion vulnerabilities in GraFX Company WebSite Builder (CWB) PRO 1.5 allow remote attackers to execute arbitrary PHP code via a URL in the INCLUDE_PATH parameter to (1) cls_headline_prod.php, (2) cls_listorders.php, or (3) cls_viewpastorders.php in include/, different vectors than CVE-2007-1513.
CVE-2007-1810
SQL injection vulnerability in product_details.php in the Kshop 1.17 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-1811
SQL injection vulnerability in index.php in the Tiny Event (tinyevent) 1.01 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the id parameter in a show action.
CVE-2007-1812
PHP remote file inclusion vulnerability in utilitaires/gestion_sondage.php in BT-Sondage 112 allows remote attackers to execute arbitrary PHP code via a URL in the repertoire_visiteur parameter.
CVE-2007-1813
SQL injection vulnerability in display.php in the eCal 2.24 and earlier module for Xoops allows remote attackers to execute arbitrary SQL commands via the katid parameter.
CVE-2007-1814
SQL injection vulnerability in viewcat.php in the Core module for Xoops allows remote attackers to execute arbitrary SQL commands via the cid parameter, a different vector than CVE-2007-0377.
CVE-2007-1815
SQL injection vulnerability in viewcat.php in the Library module for Xoops allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2007-1816
SQL injection vulnerability in viewcat.php in the Tutoriais module for Xoops allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2007-1817
SQL injection vulnerability in index.php in the Lykos Reviews (lykos_reviews) 1.00 module for Xoops allows remote attackers to execute arbitrary SQL commands via the uid parameter in a u action.
CVE-2007-1818
PHP remote file inclusion vulnerability in MOD_forum_fields_parse.php in the Forum picture and META tags 1.7 module for phpBB allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-1819
Stack-based buffer overflow in the SPIDERLib.Loader ActiveX control (Spider90.ocx) 9.1.0.4353 in TestDirector (TD) for Mercury Quality Center 9.0 before Patch 12.1, and 8.2 SP1 before Patch 32, allows remote attackers to execute arbitrary code via a long ProgColor property.
CVE-2007-1820
Nortel Networks CallPilot and Meridian Mail voicemail systems, when a mailbox has auto logon enabled, allow remote attackers to retrieve or remove messages, or reconfigure the mailbox, by spoofing Calling Number Identification (CNID, aka Caller ID).
Access complexity set to Medium because Nortel Networks voicemail systems do not hard code or default to this behavior.
CVE-2007-1821
Sprint Nextel Sprint voice mail systems allow remote attackers to retrieve or remove messages, or reconfigure mailboxes, by spoofing Calling Number Identification (CNID, aka Caller ID).
CVE-2007-1822
Alcatel-Lucent Lucent Technologies voice mail systems allow remote attackers to retrieve or remove messages, or reconfigure mailboxes, by spoofing Calling Number Identification (CNID, aka Caller ID).
CVE-2007-1823
T-Mobile voice mail systems allow remote attackers to retrieve or remove messages, or reconfigure mailboxes, by spoofing Calling Number Identification (CNID, aka Caller ID).
CVE-2007-1824
Buffer overflow in the php_stream_filter_create function in PHP 5 before 5.2.1 allows remote attackers to cause a denial of service (application crash) via a php://filter/ URL that has a name ending in the '.' character.
CVE-2007-1825
Buffer overflow in the imap_mail_compose function in PHP 5 before 5.2.1, and PHP 4 before 4.4.5, allows remote attackers to execute arbitrary code via a long boundary string in a type.parameters field. NOTE: as of 20070411, it appears that this issue might be subsumed by CVE-2007-0906.3.
CVE-2007-1826
Unspecified vulnerability in the IPSec Manager Service for Cisco Unified CallManager (CUCM) 5.0 before 5.0(4a)SU1 and Cisco Unified Presence Server (CUPS) 1.0 before 1.0(3) allows remote attackers to cause a denial of service (loss of cluster services) via a "specific UDP packet" to UDP port 8500, aka bug ID CSCsg60949.
