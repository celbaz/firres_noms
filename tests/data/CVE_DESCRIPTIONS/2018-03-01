CVE-2017-12627
In Apache Xerces-C XML Parser library before 3.2.1, processing of external DTD paths can result in a null pointer dereference under certain conditions.
CVE-2017-14798
A race condition in the postgresql init script could be used by attackers able to access the postgresql account to escalate their privileges to root.
CVE-2017-14799
A cross site scripting attack in handling the ESP login parameter handling in NetIQ Access Manager before 4.3.3 could be used to inject javascript code into the login page.
CVE-2017-14800
A reflected cross site scripting attack in the NetIQ Access Manager before 4.3.3 using the "typecontainerid" parameter of the policy editor could allowed code injection into pages of authenticated users.
CVE-2017-14804
The build package before 20171128 did not check directory names during extraction of build results that allowed untrusted builds to write outside of the target system,allowing escape out of buildroots.
CVE-2017-15134
A stack buffer overflow flaw was found in the way 389-ds-base 1.3.6.x before 1.3.6.13, 1.3.7.x before 1.3.7.9, 1.4.x before 1.4.0.5 handled certain LDAP search filters. A remote, unauthenticated attacker could potentially use this flaw to make ns-slapd crash via a specially crafted LDAP request, thus resulting in denial of service.
CVE-2017-18207
** DISPUTED ** The Wave_read._read_fmt_chunk function in Lib/wave.py in Python through 3.6.4 does not ensure a nonzero channel value, which allows attackers to cause a denial of service (divide-by-zero and exception) via a crafted wav format audio file. NOTE: the vendor disputes this issue because Python applications "need to be prepared to handle a wide variety of exceptions."
CVE-2017-18208
The madvise_willneed function in mm/madvise.c in the Linux kernel before 4.14.4 allows local users to cause a denial of service (infinite loop) by triggering use of MADVISE_WILLNEED for a DAX mapping.
CVE-2017-18209
In the GetOpenCLCachedFilesDirectory function in magick/opencl.c in ImageMagick 7.0.7, a NULL pointer dereference vulnerability occurs because a memory allocation result is not checked, related to GetOpenCLCacheDirectory.
CVE-2017-18210
In ImageMagick 7.0.7, a NULL pointer dereference vulnerability was found in the function BenchmarkOpenCLDevices in MagickCore/opencl.c because a memory allocation result is not checked.
CVE-2017-18211
In ImageMagick 7.0.7, a NULL pointer dereference vulnerability was found in the function saveBinaryCLProgram in magick/opencl.c because a program-lookup result is not checked, related to CacheOpenCLKernel.
CVE-2017-18212
An issue was discovered in JerryScript 1.0. There is a heap-based buffer over-read in the lit_read_code_unit_from_hex function in lit/lit-char-helpers.c via a RegExp("[\x0"); payload.
CVE-2017-5188
The bs_worker code in open build service before 20170320 followed relative symlinks, allowing reading of files outside of the package source directory during build, allowing leakage of private information.
CVE-2017-6150
Under certain conditions for F5 BIG-IP systems 13.0.0 or 12.1.0 - 12.1.3.1, using FastL4 profiles, when the Reassemble IP Fragments option is disabled (default), some specific large fragmented packets may restart the Traffic Management Microkernel (TMM).
CVE-2017-6154
On F5 BIG-IP systems running 13.0.0, 12.1.0 - 12.1.3.1, or 11.6.1 - 11.6.2, the BIG-IP ASM bd daemon may core dump memory under some circumstances when processing undisclosed types of data on systems with 48 or more CPU cores.
CVE-2017-6926
In Drupal versions 8.4.x versions before 8.4.5 users with permission to post comments are able to view content and comments they do not have access to, and are also able to add comments to this content. This vulnerability is mitigated by the fact that the comment system must be enabled and the attacker must have permission to post comments.
CVE-2017-6927
Drupal 8.4.x versions before 8.4.5 and Drupal 7.x versions before 7.57 has a Drupal.checkPlain() JavaScript function which is used to escape potentially dangerous text before outputting it to HTML (as JavaScript output does not typically go through Twig autoescaping). This function does not correctly handle all methods of injecting malicious HTML, leading to a cross-site scripting vulnerability under certain circumstances. The PHP functions which Drupal provides for HTML escaping are not affected.
CVE-2017-6928
Drupal core 7.x versions before 7.57 when using Drupal's private file system, Drupal will check to make sure a user has access to a file before allowing the user to view or download it. This check fails under certain conditions in which one module is trying to grant access to the file and another is trying to deny it, leading to an access bypass vulnerability. This vulnerability is mitigated by the fact that it only occurs for unusual site configurations.
CVE-2017-6929
A jQuery cross site scripting vulnerability is present when making Ajax requests to untrusted domains. This vulnerability is mitigated by the fact that it requires contributed or custom modules in order to exploit. For Drupal 8, this vulnerability was already fixed in Drupal 8.4.0 in the Drupal core upgrade to jQuery 3. For Drupal 7, it is fixed in the current release (Drupal 7.57) for jQuery 1.4.4 (the version that ships with Drupal 7 core) as well as for other newer versions of jQuery that might be used on the site, for example using the jQuery Update module.
CVE-2017-6930
In Drupal versions 8.4.x versions before 8.4.5 when using node access controls with a multilingual site, Drupal marks the untranslated version of a node as the default fallback for access queries. This fallback is used for languages that do not yet have a translated version of the created node. This can result in an access bypass vulnerability. This issue is mitigated by the fact that it only applies to sites that a) use the Content Translation module; and b) use a node access module such as Domain Access which implement hook_node_access_records().
CVE-2017-6931
In Drupal versions 8.4.x versions before 8.4.5 the Settings Tray module has a vulnerability that allows users to update certain data that they do not have the permissions for. If you have implemented a Settings Tray form in contrib or a custom module, the correct access checks should be added. This release fixes the only two implementations in core, but does not harden against other such bypasses. This vulnerability can be mitigated by disabling the Settings Tray module.
CVE-2017-6932
Drupal core 7.x versions before 7.57 has an external link injection vulnerability when the language switcher block is used. A similar vulnerability exists in various custom and contributed modules. This vulnerability could allow an attacker to trick users into unwillingly navigating to an external site.
CVE-2017-7426
The NetIQ Identity Manager Plugins before 4.6.1 contained various XML External XML Entity (XXE) handling flaws that could be used by attackers to leak information or cause denial of service attacks.
CVE-2017-7435
In libzypp before 20170803 it was possible to add unsigned YUM repositories without warning to the user that could lead to man in the middle or malicious servers to inject malicious RPM packages into a users system.
CVE-2017-7436
In libzypp before 20170803 it was possible to retrieve unsigned packages without a warning to the user which could lead to man in the middle or malicious servers to inject malicious RPM packages into a users system.
CVE-2017-9268
In the open build service before 201707022 the wipetrigger and rebuild actions checked the wrong project for permissions, allowing authenticated users to cause operations on projects where they did not have permissions leading to denial of service (resource consumption).
CVE-2017-9269
In libzypp before August 2018 GPG keys attached to YUM repositories were not correctly pinned, allowing malicious repository mirrors to silently downgrade to unsigned repositories with potential malicious content.
CVE-2017-9270
In cryptctl before version 2.0 a malicious server could send RPC requests that could overwrite files outside of the cryptctl key database.
CVE-2017-9271
The commandline package update tool zypper writes HTTP proxy credentials into its logfile, allowing local attackers to gain access to proxies used.
CVE-2017-9274
A shell command injection in the obs-service-source_validator before 0.7 could be used to execute code as the packager when checking RPM SPEC files with specific macro constructs.
CVE-2017-9286
The packaging of NextCloud in openSUSE used /srv/www/htdocs in an unsafe manner, which could have allowed scripts running as wwwrun user to escalate privileges to root during nextcloud package upgrade.
CVE-2018-2365
SAP NetWeaver Portal, WebDynpro Java, 7.30, 7.31, 7.40, 7.50, does not sufficiently encode user controlled inputs, resulting in Cross-Site Scripting (XSS) vulnerability.
CVE-2018-2367
ABAP File Interface in, SAP BASIS, from 7.00 to 7.02, from 7.10 to 7.11, 7.30, 7.31, 7.40, from 7.50 to 7.52, allows an attacker to exploit insufficient validation of path information provided by users, thus characters representing "traverse to parent directory" are passed through to the file APIs.
CVE-2018-2368
SAP NetWeaver System Landscape Directory, LM-CORE 7.10, 7.20, 7.30, 7.31, 7.40, does not perform any authentication checks for functionalities that require user identity.
CVE-2018-2380
SAP CRM, 7.01, 7.02,7.30, 7.31, 7.33, 7.54, allows an attacker to exploit insufficient validation of path information provided by users, thus characters representing "traverse to parent directory" are passed through to the file APIs.
CVE-2018-5314
Command injection vulnerability in Citrix NetScaler ADC and NetScaler Gateway 11.0 before build 70.16, 11.1 before build 55.13, and 12.0 before build 53.13; and the NetScaler Load Balancing instance distributed with NetScaler SD-WAN/CloudBridge 4000, 4100, 5000 and 5100 WAN Optimization Edition 9.3.0 allows remote attackers to execute a system command or read arbitrary files via an SSH login prompt.
CVE-2018-5500
On F5 BIG-IP systems running 13.0.0, 12.1.0 - 12.1.3.1, or 11.6.1 - 11.6.2, every Multipath TCP (MCTCP) connection established leaks a small amount of memory. Virtual server using TCP profile with Multipath TCP (MCTCP) feature enabled will be affected by this issue.
CVE-2018-5501
In some circumstances, on F5 BIG-IP systems running 13.0.0, 12.1.0 - 12.1.3.1, any 11.6.x or 11.5.x release, or 11.2.1, TCP DNS profile allows excessive buffering due to lack of flow control.
CVE-2018-6653
comforte SWAP 1049 through 1069 and 20.0.0 through 21.5.3 (as used in SSLOBJ on HPE NonStop SSL T0910, and in the comforte SecurCS, SecurFTP, SecurLib/SSL-AT, and SecurTN products), after executing the RELOAD CERTIFICATES command, does not ensure that clients use a strong TLS cipher suite, which makes it easier for remote attackers to defeat intended cryptographic protection mechanisms by sniffing the network. This is fixed in 21.6.0.
CVE-2018-7047
An issue was discovered in the MBeans Server in Wowza Streaming Engine before 4.7.1. The file system may be read and written to via JMX using the default JMX credentials (remote code execution may be possible as well).
CVE-2018-7048
An issue was discovered in Wowza Streaming Engine before 4.7.1. There is a denial of service (memory consumption) via a crafted HTTP request.
CVE-2018-7049
An issue was discovered in Wowza Streaming Engine before 4.7.1. There is an XSS vulnerability in the HTTP providers (com.wowza.wms.http.HTTPProviderMediaList and com.wowza.wms.http.streammanager.HTTPStreamManager) causing script injection and/or reflection via a crafted HTTP request.
CVE-2018-7550
The load_multiboot function in hw/i386/multiboot.c in Quick Emulator (aka QEMU) allows local guest OS users to execute arbitrary code on the QEMU host via a mh_load_end_addr value greater than mh_bss_end_addr, which triggers an out-of-bounds read or write memory access.
CVE-2018-7561
Stack-based Buffer Overflow in httpd on Tenda AC9 devices V15.03.05.14_EN allows remote attackers to cause a denial of service or possibly have unspecified other impact.
CVE-2018-7573
An issue was discovered in FTPShell Client 6.7. A remote FTP server can send 400 characters of 'F' in conjunction with the FTP 220 response code to crash the application; after this overflow, one can run arbitrary code on the victim machine. This is similar to CVE-2009-3364 and CVE-2017-6465.
CVE-2018-7579
\application\admin\controller\update_urls.class.php in YzmCMS 3.6 has SQL Injection via the catids array parameter to admin/update_urls/update_category_url.html.
CVE-2018-7584
In PHP through 5.6.33, 7.0.x before 7.0.28, 7.1.x through 7.1.14, and 7.2.x through 7.2.2, there is a stack-based buffer under-read while parsing an HTTP response in the php_stream_url_wrap_http_ex function in ext/standard/http_fopen_wrapper.c. This subsequently results in copying a large string.
CVE-2018-7586
In the nextgen-gallery plugin before 2.2.50 for WordPress, gallery paths are not secured.
CVE-2018-7587
An issue was discovered in CImg v.220. DoS occurs when loading a crafted bmp image that triggers an allocation failure in load_bmp in CImg.h.
CVE-2018-7588
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image.
CVE-2018-7589
An issue was discovered in CImg v.220. A double free in load_bmp in CImg.h occurs when loading a crafted bmp image.
CVE-2018-7590
CSRF exists in Hoosk 1.7.0 via /admin/users/new/add, resulting in account creation.
CVE-2018-7634
An issue was discovered in Enalean Tuleap 9.17. Lack of CSRF attack mitigation while changing an e-mail address makes it possible to abuse the functionality by attackers. By making a CSRF attack, an attacker could make a victim change his registered e-mail address on the application, leading to account takeover.
