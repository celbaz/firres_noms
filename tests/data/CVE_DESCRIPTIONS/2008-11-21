CVE-2008-5182
The inotify functionality in Linux kernel 2.6 before 2.6.28-rc5 might allow local users to gain privileges via unknown vectors related to race conditions in inotify watch removal and umount.
CVE-2008-5183
cupsd in CUPS 1.3.9 and earlier allows local users, and possibly remote attackers, to cause a denial of service (daemon crash) by adding a large number of RSS Subscriptions, which triggers a NULL pointer dereference.  NOTE: this issue can be triggered remotely by leveraging CVE-2008-5184.
CVE-2008-5184
The web interface (cgi-bin/admin.c) in CUPS before 1.3.8 uses the guest username when a user is not logged on to the web server, which makes it easier for remote attackers to bypass intended policy and conduct CSRF attacks via the (1) add and (2) cancel RSS subscription functions.
CVE-2008-5185
The highlighting functionality in geshi.php in GeSHi before 1.0.8 allows remote attackers to cause a denial of service (infinite loop) via an XML sequence containing an opening delimiter without a closing delimiter, as demonstrated using "<".
CVE-2008-5186
** DISPUTED **  The set_language_path function in geshi.php in Generic Syntax Highlighter (GeSHi) before 1.0.8.1 might allow remote attackers to conduct file inclusion attacks via crafted inputs that influence the default language path ($path variable).  NOTE: this issue has been disputed by a vendor, stating that only a static value is used, so this is not a vulnerability in GeSHi. Separate CVE identifiers would be created for web applications that integrate GeSHi in a way that allows control of the default language path.
CVE-2008-5187
The load function in the XPM loader for imlib2 1.4.2, and possibly other versions, allows attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted XPM file that triggers a "pointer arithmetic error" and a heap-based buffer overflow, a different vulnerability than CVE-2008-2426.
CVE-2008-5188
The (1) ecryptfs-setup-private, (2) ecryptfs-setup-confidential, and (3) ecryptfs-setup-pam-wrapped.sh scripts in ecryptfs-utils 45 through 61 in eCryptfs place cleartext passwords on command lines, which allows local users to obtain sensitive information by listing the process.
CVE-2008-5189
CRLF injection vulnerability in Ruby on Rails before 2.0.5 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a crafted URL to the redirect_to function.
CVE-2008-5190
SQL injection vulnerability in index.php in eSHOP100 allows remote attackers to execute arbitrary SQL commands via the SUB parameter.
CVE-2008-5191
Multiple SQL injection vulnerabilities in SePortal 2.4 allow remote attackers to execute arbitrary SQL commands via the (1) poll_id parameter to poll.php and the (2) sp_id parameter to staticpages.php.
CVE-2008-5192
SQL injection vulnerability in forum.asp in W1L3D4 Philboard 1.14 and 1.2 allows remote attackers to execute arbitrary SQL commands via the forumid parameter.  NOTE: this might overlap CVE-2008-2334, CVE-2008-1939, CVE-2007-2641, or CVE-2007-0920.
CVE-2008-5193
Cross-site scripting (XSS) vulnerability in search.asp in W1L3D4 Philboard 1.14 and 1.2 allows remote attackers to inject arbitrary web script or HTML via the searchterms parameter.  NOTE: this might overlap CVE-2007-4024.
CVE-2008-5194
SQL injection vulnerability in checkavail.php in SoftVisions Software Online Booking Manager (obm) 2.2 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5195
Multiple SQL injection vulnerabilities in SebracCMS (sbcms) 0.4 allow remote attackers to execute arbitrary SQL commands via (1) the recid parameter to cms/form/read.php, (2) the uname parameter to cms/index.php, and other unspecified vectors.
CVE-2008-5196
SQL injection vulnerability in kroax.php in the Kroax (the_kroax) 4.42 and earlier module for PHP-Fusion allows remote attackers to execute arbitrary SQL commands via the category parameter.
CVE-2008-5197
SQL injection vulnerability in classifieds.php in PHP-Fusion allows remote attackers to execute arbitrary SQL commands via the lid parameter in a detail_adverts action.
CVE-2008-5198
SQL injection vulnerability in memberlist.php in Acmlmboard 1.A2 allows remote attackers to execute arbitrary SQL commands via the pow parameter.
CVE-2008-5199
PHP remote file inclusion vulnerability in include.php in PHPOutsourcing IdeaBox (aka IdeBox) 1.1 allows remote attackers to execute arbitrary PHP code via a URL in the gorumDir parameter.
CVE-2008-5200
SQL injection vulnerability in the Xe webtv (com_xewebtv) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a detail action to index.php.
CVE-2008-5201
Directory traversal vulnerability in index.php in OTManager CMS 24a allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the conteudo parameter.  NOTE: in some environments, this can be leveraged for remote file inclusion by using a UNC share pathname or an ftp, ftps, or ssh2.sftp URL.
CVE-2008-5202
Cross-site scripting (XSS) vulnerability in index.php in OTManager CMS 24a allows remote attackers to inject arbitrary web script or HTML via the conteudo parameter.
CVE-2008-5203
Cross-site scripting (XSS) vulnerability in external_vote.php in PowerAward 1.1.0 RC1 allows remote attackers to inject arbitrary web script or HTML via the l_vote_done parameter.
CVE-2008-5204
Multiple directory traversal vulnerabilities in PowerAward 1.1.0 RC1, when register_globals is enabled, allow remote attackers to include and execute arbitrary local files via directory traversal sequences in the lang parameter to (1) agb.php, (2) angemeldet.php, (3) anmelden.php, (4) charts.php, (5) external_vote.php, (6) guestbook.php, (7) impressum.php, (8) index.php, (9) rss-reader.php, (10) statistic.php, (11) teilnehmer.php, (12) topsites.php, (13) votecode.php, (14) voting.php, and (15) winner.php.
CVE-2008-5205
Cross-site scripting (XSS) vulnerability in edit.php in wellyblog allows remote attackers to inject arbitrary web script or HTML via the articleid parameter in an add action.
CVE-2008-5206
PHP remote file inclusion vulnerability in modules/mod_mainmenu.php in MosXML 1 Alpha allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5207
Multiple directory traversal vulnerabilities in Jonascms 1.2 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the taal parameter to (1) backup.php and (2) gb_voegtoe.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
