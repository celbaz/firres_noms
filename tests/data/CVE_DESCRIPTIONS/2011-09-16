CVE-2011-3211
The server in Bcfg2 1.1.2 and earlier, and 1.2 prerelease, allows remote attackers to execute arbitrary commands via shell metacharacters in data received from a client.
CVE-2011-3321
Heap-based buffer overflow in the Siemens WinCC Runtime Advanced Loader, as used in SIMATIC WinCC flexible Runtime and SIMATIC WinCC (TIA Portal) Runtime Advanced, allows remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via a crafted packet to TCP port 2308.
CVE-2011-3486
Beckhoff TwinCAT 2.11.0.2004 and earlier allows remote attackers to cause a denial of service via a crafted request to UDP port 48899, which triggers an out-of-bounds read.
CVE-2011-3487
Directory traversal vulnerability in CarelDataServer.exe in Carel PlantVisor 2.4.4 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in an HTTP GET request.
CVE-2011-3488
Use-after-free vulnerability in Equis MetaStock 11 and earlier allows remote attackers to execute arbitrary code via a malformed (1) mwc chart, (2) mws chart, (3) mwt template, or (4) mwl layout.
CVE-2011-3489
RnaUtility.dll in RsvcHost.exe 2.30.0.23 in Rockwell RSLogix 19 and earlier allows remote attackers to cause a denial of service (crash) via a crafted rna packet with a long string to TCP port 4446 that triggers (1) "a memset zero overflow" or (2) an out-of-bounds read, related to improper handling of a 32-bit size field.
CVE-2011-3490
Multiple stack-based buffer overflows in service.exe in Measuresoft ScadaPro 4.0.0 and earlier allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long command to port 11234, as demonstrated with the TF command.
CVE-2011-3491
Heap-based buffer overflow in Progea Movicon / PowerHMI 11.2.1085 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a negative Content-Length field.
CVE-2011-3492
Stack-based buffer overflow in Azeotech DAQFactory 5.85 build 1853 and earlier allows remote attackers to cause a denial of service (crash) and execute arbitrary code via a crafted NETB packet to UDP port 20034.
CVE-2011-3493
Multiple stack-based buffer overflows in the DH_OneSecondTick function in Cogent DataHub 7.1.1.63 and earlier allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via long (1) domain, (2) report_domain, (3) register_datahub, or (4) slave commands.
CVE-2011-3494
WinSig.exe in eSignal 10.6.2425 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via (1) a long StyleTemplate element in a QUO, SUM or POR file, which triggers a stack-based buffer overflow, or (2) a long Font->FaceName field (aka FaceName element), which triggers a heap-based buffer overflow.  NOTE: some of these details are obtained from third party information.
CVE-2011-3495
Multiple directory traversal vulnerabilities in service.exe in Measuresoft ScadaPro 4.0.0 and earlier allow remote attackers to read, modify, or delete arbitrary files via the (1) RF, (2) wF, (3) UF, or (4) NF command.
CVE-2011-3496
service.exe in Measuresoft ScadaPro 4.0.0 and earlier allows remote attackers to execute arbitrary commands via shell metacharacters in the (1) BF, (2) OF, or (3) EF command.
CVE-2011-3497
service.exe in Measuresoft ScadaPro 4.0.0 and earlier allows remote attackers to execute arbitrary DLL functions via the XF function, possibly related to an insecure exposed method.
CVE-2011-3498
Heap-based buffer overflow in Progea Movicon / PowerHMI 11.2.1085 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long request.
CVE-2011-3499
Progea Movicon / PowerHMI 11.2.1085 and earlier allows remote attackers to cause a denial of service (memory corruption and crash) and possibly execute arbitrary code via an EIDP packet with a large size field, which writes a zero byte to an arbitrary memory location.
CVE-2011-3500
Directory traversal vulnerability in the web server in Cogent DataHub 7.1.1.63 and earlier allows remote attackers to read arbitrary files via a ..\ (dot dot backslash) in an HTTP request.
CVE-2011-3501
Integer overflow in Cogent DataHub 7.1.1.63 and earlier allows remote attackers to cause a denial of service (crash) via a negative or large Content-Length value.
CVE-2011-3502
The web server in Cogent DataHub 7.1.1.63 and earlier allows remote attackers to obtain the source code of executable files via a request with a trailing (1) space or (2) %2e (encoded dot).
CVE-2011-3503
Untrusted search path vulnerability in eSignal 10.6.2425.1208, and possibly other versions, allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse JRS_UT.dll that is located in the same folder as a .quo (QUOTE) file.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
Per: http://cwe.mitre.org/data/definitions/426.html
'CWE-426: Untrusted Search Path'
