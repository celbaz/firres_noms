CVE-2010-4250
Memory leak in the inotify_init1 function in fs/notify/inotify/inotify_user.c in the Linux kernel before 2.6.37 allows local users to cause a denial of service (memory consumption) via vectors involving failed attempts to create files.
CVE-2010-4648
The orinoco_ioctl_set_auth function in drivers/net/wireless/orinoco/wext.c in the Linux kernel before 2.6.37 does not properly implement a TKIP protection mechanism, which makes it easier for remote attackers to obtain access to a Wi-Fi network by reading Wi-Fi frames.
CVE-2010-4650
Buffer overflow in the fuse_do_ioctl function in fs/fuse/file.c in the Linux kernel before 2.6.37 allows local users to cause a denial of service or possibly have unspecified other impact by leveraging the ability to operate a CUSE server.
CVE-2011-0006
The ima_lsm_rule_init function in security/integrity/ima/ima_policy.c in the Linux kernel before 2.6.37, when the Linux Security Modules (LSM) framework is disabled, allows local users to bypass Integrity Measurement Architecture (IMA) rules in opportunistic circumstances by leveraging an administrator's addition of an IMA rule for LSM.
CVE-2011-0011
qemu-kvm before 0.11.0 disables VNC authentication when the password is cleared, which allows remote attackers to bypass authentication and establish VNC sessions.
CVE-2011-0716
The br_multicast_add_group function in net/bridge/br_multicast.c in the Linux kernel before 2.6.38, when a certain Ethernet bridge configuration is used, allows local users to cause a denial of service (memory corruption and system crash) by sending IGMP packets to a local interface.
CVE-2011-1021
drivers/acpi/debugfs.c in the Linux kernel before 3.0 allows local users to modify arbitrary kernel memory locations by leveraging root privileges to write to the /sys/kernel/debug/acpi/custom_method file. NOTE: this vulnerability exists because of an incomplete fix for CVE-2010-4347.
CVE-2011-1023
The Reliable Datagram Sockets (RDS) subsystem in the Linux kernel before 2.6.38 does not properly handle congestion map updates, which allows local users to cause a denial of service (BUG_ON and system crash) via vectors involving (1) a loopback (aka loop) transmit operation or (2) an InfiniBand (aka ib) transmit operation.
CVE-2011-1078
The sco_sock_getsockopt_old function in net/bluetooth/sco.c in the Linux kernel before 2.6.39 does not initialize a certain structure, which allows local users to obtain potentially sensitive information from kernel stack memory via the SCO_CONNINFO option.
CVE-2011-1079
The bnep_sock_ioctl function in net/bluetooth/bnep/sock.c in the Linux kernel before 2.6.39 does not ensure that a certain device field ends with a '\0' character, which allows local users to obtain potentially sensitive information from kernel stack memory, or cause a denial of service (BUG and system crash), via a BNEPCONNADD command.
CVE-2011-1080
The do_replace function in net/bridge/netfilter/ebtables.c in the Linux kernel before 2.6.39 does not ensure that a certain name field ends with a '\0' character, which allows local users to obtain potentially sensitive information from kernel stack memory by leveraging the CAP_NET_ADMIN capability to replace a table, and then reading a modprobe command line.
CVE-2011-1160
The tpm_open function in drivers/char/tpm/tpm.c in the Linux kernel before 2.6.39 does not initialize a certain buffer, which allows local users to obtain potentially sensitive information from kernel memory via unspecified vectors.
CVE-2011-1476
Integer underflow in the Open Sound System (OSS) subsystem in the Linux kernel before 2.6.39 on unspecified non-x86 platforms allows local users to cause a denial of service (memory corruption) by leveraging write access to /dev/sequencer.
CVE-2011-1477
Multiple array index errors in sound/oss/opl3.c in the Linux kernel before 2.6.39 allow local users to cause a denial of service (heap memory corruption) or possibly gain privileges by leveraging write access to /dev/sequencer.
CVE-2011-1479
Double free vulnerability in the inotify subsystem in the Linux kernel before 2.6.39 allows local users to cause a denial of service (system crash) via vectors involving failed attempts to create files.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2010-4250.
CVE-2011-1493
Array index error in the rose_parse_national function in net/rose/rose_subr.c in the Linux kernel before 2.6.39 allows remote attackers to cause a denial of service (heap memory corruption) or possibly have unspecified other impact by composing FAC_NATIONAL_DIGIS data that specifies a large number of digipeaters, and then sending this data to a ROSE socket.
CVE-2011-1750
Multiple heap-based buffer overflows in the virtio-blk driver (hw/virtio-blk.c) in qemu-kvm 0.14.0 allow local guest users to cause a denial of service (guest crash) and possibly gain privileges via a (1) write request to the virtio_blk_handle_write function or (2) read request to the virtio_blk_handle_read function that is not properly aligned.
CVE-2011-1751
The pciej_write function in hw/acpi_piix4.c in the PIIX4 Power Management emulation in qemu-kvm does not check if a device is hotpluggable before unplugging the PCI-ISA bridge, which allows privileged guest users to cause a denial of service (guest crash) and possibly execute arbitrary code by sending a crafted value to the 0xae08 (PCI_EJ_BASE) I/O port, which leads to a use-after-free related to "active qemu timers."
CVE-2011-2212
Buffer overflow in the virtio subsystem in qemu-kvm 0.14.0 and earlier allows privileged guest users to cause a denial of service (guest crash) or gain privileges via a crafted indirect descriptor related to "virtqueue in and out requests."
CVE-2011-2512
The virtio_queue_notify in qemu-kvm 0.14.0 and earlier does not properly validate the virtqueue number, which allows guest users to cause a denial of service (guest crash) and possibly execute arbitrary code via a negative number in the Queue Notify field of the Virtio Header, which bypasses a signed comparison.
CVE-2011-2527
The change_process_uid function in os-posix.c in Qemu 0.14.0 and earlier does not properly drop group privileges when the -runas option is used, which allows local guest users to access restricted files on the host.
CVE-2011-2709
libgssapi and libgssglue before 0.4 do not properly check privileges, which allows local users to load untrusted configuration files and execute arbitrary code via the GSSAPI_MECH_CONF environment variable, as demonstrated using mount.nfs.
CVE-2011-4324
The encode_share_access function in fs/nfs/nfs4xdr.c in the Linux kernel before 2.6.29 allows local users to cause a denial of service (BUG and system crash) by using the mknod system call with a pathname on an NFSv4 filesystem.
CVE-2011-4599
Stack-based buffer overflow in the _canonicalize function in common/uloc.c in International Components for Unicode (ICU) before 49.1 allows remote attackers to execute arbitrary code via a crafted locale ID that is not properly handled during variant canonicalization.
CVE-2011-4913
The rose_parse_ccitt function in net/rose/rose_subr.c in the Linux kernel before 2.6.39 does not validate the FAC_CCITT_DEST_NSAP and FAC_CCITT_SRC_NSAP fields, which allows remote attackers to (1) cause a denial of service (integer underflow, heap memory corruption, and panic) via a small length value in data sent to a ROSE socket, or (2) conduct stack-based buffer overflow attacks via a large length value in data sent to a ROSE socket.
CVE-2011-4914
The ROSE protocol implementation in the Linux kernel before 2.6.39 does not verify that certain data-length values are consistent with the amount of data sent, which might allow remote attackers to obtain sensitive information from kernel memory or cause a denial of service (out-of-bounds read) via crafted data to a ROSE socket.
CVE-2012-0028
The robust futex implementation in the Linux kernel before 2.6.28 does not properly handle processes that make exec system calls, which allows local users to cause a denial of service or possibly gain privileges by writing to a memory location in a child process.
CVE-2012-0219
Heap-based buffer overflow in the xioscan_readline function in xio-readline.c in socat 1.4.0.0 through 1.7.2.0 and 2.0.0-b1 through 2.0.0-b4 allows local users to execute arbitrary code via the READLINE address.
CVE-2012-1149
Integer overflow in the vclmi.dll module in OpenOffice.org (OOo) 3.3, 3.4 Beta, and possibly earlier, and LibreOffice before 3.5.3, allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted embedded image object, as demonstrated by a JPEG image in a .DOC file, which triggers a heap-based buffer overflow.
CVE-2012-1616
Use-after-free vulnerability in icclib before 2.13, as used by Argyll CMS before 1.4 and possibly other programs, allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a crafted ICC profile file.
CVE-2012-2127
fs/proc/root.c in the procfs implementation in the Linux kernel before 3.2 does not properly interact with CLONE_NEWPID clone system calls, which allows remote attackers to cause a denial of service (reference leak and memory consumption) by making many connections to a daemon that uses PID namespaces to isolate clients, as demonstrated by vsftpd.
CVE-2012-2149
The WPXContentListener::_closeTableRow function in WPXContentListener.cpp in libwpd 0.8.8, as used by OpenOffice.org (OOo) before 3.4, allows remote attackers to execute arbitrary code via a crafted Wordperfect .WPD document that causes a negative array index to be used.  NOTE: some sources report this issue as an integer overflow.
CVE-2012-2389
hostapd 0.7.3, and possibly other versions before 1.0, uses 0644 permissions for /etc/hostapd/hostapd.conf, which might allow local users to obtain sensitive information such as credentials.
CVE-2012-2654
The (1) EC2 and (2) OS APIs in OpenStack Compute (Nova) Folsom (2012.2), Essex (2012.1), and Diablo (2011.3) do not properly check the protocol when security groups are created and the network protocol is not specified entirely in lowercase, which allows remote attackers to bypass intended access restrictions.
CVE-2012-2716
Cross-site request forgery (CSRF) vulnerability in the Comment Moderation module 6.x-1.x before 6.x-1.1 for Drupal allows remote attackers to hijack the authentication of administrators for requests that publish comments.
CVE-2012-2718
SQL injection vulnerability in the Counter module for Drupal allows remote attackers to execute arbitrary SQL commands via unspecified vectors related to "recording visits."
CVE-2012-3791
Multiple SQL injection vulnerabilities in Simple Web Content Management System 1.1 allow remote attackers to execute arbitrary SQL commands via the id parameter to (1) item_delete.php, (2) item_status.php, (3) item_detail.php, (4) item_modify.php, or (5) item_position.php in admin/; or (6) status parameter to admin/item_status.php.
