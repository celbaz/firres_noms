CVE-2018-10143
The Palo Alto Networks Expedition Migration tool 1.0.107 and earlier may allow an unauthenticated attacker with remote access to run system level commands on the device hosting this service/application.
CVE-2018-11457
A vulnerability has been identified in SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). The integrated web server on port 4842/tcp of the affected products could allow a remote attacker to execute code with privileged permissions on the system by sending specially crafted network requests to port 4842/tcp. Please note that this vulnerability is only exploitable if port 4842/tcp is manually opened in the firewall configuration of network port X130. The security vulnerability could be exploited by an attacker with network access to the affected devices on port 4842/tcp. Successful exploitation requires no privileges and no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the web server. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11458
A vulnerability has been identified in SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). The integrated VNC server on port 5900/tcp of the affected products could allow a remote attacker to execute code with privileged permissions on the system by sending specially crafted network requests to port 5900/tcp. Please note that this vulnerability is only exploitable if port 5900/tcp is manually opened in the firewall configuration of network port X130. The security vulnerability could be exploited by an attacker with network access to the affected devices and port. Successful exploitation requires no privileges and no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the VNC server. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11459
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). A local attacker could modify a user-writeable configuration file so that after reboot or manual initiation the system reloads the modified configuration file and attacker-controlled code is executed with elevated privileges. The security vulnerability could be exploited by an attacker with local access to the affected system. Successful exploitation requires user privileges but no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11460
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). A local attacker with elevated user privileges (manufact) could modify a CRAMFS archive so that after reboot the system loads the modified CRAMFS file and attacker-controlled code is executed with root privileges. The security vulnerability could be exploited by an attacker with local access to the affected systems. Successful exploitation requires elevated user privileges (manufact) but no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11461
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). A local attacker with user privileges could use the service command application for privilege escalation to an elevated user but not root. The security vulnerability could be exploited by an attacker with local access to the affected systems. Successful exploitation requires user privileges but no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11462
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). By sending a specially crafted authentication request to the affected systems a remote attacker could escalate his privileges to an elevated user account but not to root. The security vulnerability could be exploited by an attacker with network access to the affected systems. Successful exploitation requires no privileges and no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11463
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). A buffer overflow in the service command application could allow a local attacker to execute code with elevated privileges. The security vulnerability could be exploited by an attacker with local access to the affected systems. Successful exploitation requires user privileges but no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11464
A vulnerability has been identified in SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). The integrated VNC server on port 5900/tcp of the affected products could allow a remote attacker to cause a Denial-of-Service condition of the VNC server. Please note that this vulnerability is only exploitable if port 5900/tcp is manually opened in the firewall configuration of network port X130. The security vulnerability could be exploited by an attacker with network access to the affected devices and port. Successful exploitation requires no privileges and no user interaction. The vulnerability could allow an attacker to compromise availability of the VNC server. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11465
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). A local attacker could use ioctl calls to do out of bounds reads, arbitrary writes, or execute code in kernel mode. The security vulnerability could be exploited by an attacker with local access to the affected systems. Successful exploitation requires user privileges but no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11466
A vulnerability has been identified in SINUMERIK 808D V4.7 (All versions), SINUMERIK 808D V4.8 (All versions), SINUMERIK 828D V4.7 (All versions < V4.7 SP6 HF1), SINUMERIK 840D sl V4.7 (All versions < V4.7 SP6 HF5), SINUMERIK 840D sl V4.8 (All versions < V4.8 SP3). Specially crafted network packets sent to port 102/tcp (ISO-TSAP) could allow a remote attacker to either cause a Denial-of-Service condition of the integrated software firewall or allow to execute code in the context of the software firewall. The security vulnerability could be exploited by an attacker with network access to the affected systems on port 102/tcp. Successful exploitation requires no user privileges and no user interaction. The vulnerability could allow an attacker to compromise confidentiality, integrity and availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known
CVE-2018-13816
A vulnerability has been identified in TIM 1531 IRC (All version < V2.0). The devices was missing proper authentication on port 102/tcp, although configured. Successful exploitation requires an attacker to be able to send packets to port 102/tcp of the affected device. No user interaction and no user privileges are required to exploit the vulnerability. At the time of advisory publication no public exploitation of this vulnerability was known.
CVE-2018-1474
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 is vulnerable to HTTP response splitting attacks, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability to inject arbitrary HTTP headers and cause the server to return a split response, once the URL is clicked. This would allow the attacker to perform further attacks, such as Web cache poisoning or cross-site scripting, and possibly obtain sensitive information. IBM X-force ID: 140692.
CVE-2018-1476
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 140757.
CVE-2018-1478
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 140760.
CVE-2018-1480
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 does not set the 'HttpOnly' attribute on authorization tokens or session cookies. If a Cross-Site Scripting vulnerability also existed attackers may be able to get the cookie values via malicious JavaScript and then hijack the user session. IBM X-Force ID: 140762.
CVE-2018-1481
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 140763.
CVE-2018-1484
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 does not set the secure attribute on authorization tokens or session cookies. Attackers may be able to get the cookie values by sending a http:// link to a user or by planting this link in a site the user goes to. The cookie will be sent to the insecure link and the attacker can then obtain the cookie value by snooping the traffic. IBM X-Force ID: 140969.
CVE-2018-1485
IBM BigFix Platform 9.2.0 through 9.2.14 and 9.5 through 9.5.9 does not renew a session variable after a successful authentication which could lead to session fixation/hijacking vulnerability. This could force a user to utilize a cookie that may be known to an attacker. IBM X-Force ID: 140970.
CVE-2018-15328
On BIG-IP 14.0.x, 13.x, 12.x, and 11.x, Enterprise Manager 3.1.1, BIG-IQ 6.x, 5.x, and 4.x, and iWorkflow 2.x, the passphrases for SNMPv3 users and trap destinations that are used for authentication and privacy are not handled by the BIG-IP system Secure Vault feature; they are written in the clear to the various configuration files.
CVE-2018-15717
Open Dental before version 18.4 stores user passwords as base64 encoded MD5 hashes.
CVE-2018-15718
Open Dental before version 18.4 transmits the entire user database over the network when a remote unauthenticated user accesses the command prompt. This allows the attacker to gain access to usernames, password hashes, privilege levels, and more.
CVE-2018-15719
Open Dental before version 18.4 installs a mysql database and uses the default credentials of "root" with a blank password. This allows anyone on the network with access to the server to access all database information.
CVE-2018-16867
A flaw was found in qemu Media Transfer Protocol (MTP) before version 3.1.0. A path traversal in the in usb_mtp_write_data function in hw/usb/dev-mtp.c due to an improper filename sanitization. When the guest device is mounted in read-write mode, this allows to read/write arbitrary files which may lead do DoS scenario OR possibly lead to code execution on the host.
CVE-2018-17949
Cross site scripting vulnerability in iManager prior to 3.1 SP2.
CVE-2018-17950
Incorrect enforcement of authorization checks in eDirectory prior to 9.1 SP2
CVE-2018-17952
Cross site scripting vulnerability in eDirectory prior to 9.1 SP2
CVE-2018-18397
The userfaultfd implementation in the Linux kernel before 4.19.7 mishandles access control for certain UFFDIO_ ioctl calls, as demonstrated by allowing local users to write data into holes in a tmpfs file (if the user has read-only access to that file, and that file contains holes), related to fs/userfaultfd.c and mm/userfaultfd.c.
CVE-2018-1901
IBM WebSphere Application Server 8.5 and 9.0 could allow a remote attacker to temporarily gain elevated privileges on the system, caused by incorrect cached value being used. IBM X-Force ID: 152530.
CVE-2018-1926
IBM WebSphere Application Server 7.0, 8.0, 8.5, and 9.0 Admin Console is vulnerable to cross-site request forgery, caused by improper validation of user-supplied input. By persuading a user to visit a malicious URL, a remote attacker could send a specially-crafted request. An attacker could exploit this vulnerability to perform CSRF attack and update available applications. IBM X-Force ID: 152992.
CVE-2018-20094
An issue was discovered in XXL-CONF 1.6.0. There is a path traversal vulnerability via ../ in the keys parameter that can download any configuration file, related to ConfController.java and PropUtil.java.
CVE-2018-20095
An issue was discovered in EnsureCapacity in Core/Ap4Array.h in Bento4 1.5.1-627. Crafted MP4 input triggers an attempt at excessive memory allocation, as demonstrated by mp42hls.
CVE-2018-20096
There is a heap-based buffer over-read in the Exiv2::tEXtToDataBuf function of pngimage.cpp in Exiv2 0.27-RC3. A crafted input will lead to a remote denial of service attack.
CVE-2018-20097
There is a SEGV in Exiv2::Internal::TiffParserWorker::findPrimaryGroups of tiffimage_int.cpp in Exiv2 0.27-RC3. A crafted input will lead to a remote denial of service attack.
CVE-2018-20098
There is a heap-based buffer over-read in Exiv2::Jp2Image::encodeJp2Header of jp2image.cpp in Exiv2 0.27-RC3. A crafted input will lead to a remote denial of service attack.
CVE-2018-20099
There is an infinite loop in Exiv2::Jp2Image::encodeJp2Header of jp2image.cpp in Exiv2 0.27-RC3. A crafted input will lead to a remote denial of service attack.
CVE-2018-20101
The codection "Import users from CSV with meta" plugin before 1.12.1 for WordPress allows XSS via the value of a cell.
CVE-2018-20102
An out-of-bounds read in dns_validate_dns_response in dns.c was discovered in HAProxy through 1.8.14. Due to a missing check when validating DNS responses, remote attackers might be able read the 16 bytes corresponding to an AAAA record from the non-initialized part of the buffer, possibly accessing anything that was left on the stack, or even past the end of the 8193-byte buffer, depending on the value of accepted_payload_size.
CVE-2018-20103
An issue was discovered in dns.c in HAProxy through 1.8.14. In the case of a compressed pointer, a crafted packet can trigger infinite recursion by making the pointer point to itself, or create a long chain of valid pointers resulting in stack exhaustion.
CVE-2018-6704
Privilege escalation vulnerability in McAfee Agent (MA) for Linux 5.0.0 through 5.0.6, 5.5.0, and 5.5.1 allows local users to perform arbitrary command execution via specific conditions.
CVE-2018-6705
Privilege escalation vulnerability in McAfee Agent (MA) for Linux 5.0.0 through 5.0.6, 5.5.0, and 5.5.1 allows local users to perform arbitrary command execution via specific conditions.
CVE-2018-6706
Insecure handling of temporary files in non-Windows McAfee Agent 5.0.0 through 5.0.6, 5.5.0, and 5.5.1 allows an Unprivileged User to introduce custom paths during agent installation in Linux via unspecified vectors.
CVE-2018-8477
An information disclosure vulnerability exists when the Windows kernel improperly handles objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8621, CVE-2018-8622.
CVE-2018-8514
An information disclosure vulnerability exists when Remote Procedure Call runtime improperly initializes objects in memory, aka "Remote Procedure Call runtime Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8517
A denial of service vulnerability exists when .NET Framework improperly handles special web requests, aka ".NET Framework Denial Of Service Vulnerability." This affects Microsoft .NET Framework 4.6, Microsoft .NET Framework 3.5, Microsoft .NET Framework 4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.6/4.6.1/4.6.2/4.7/4.7.1/4.7.1/4.7.2, Microsoft .NET Framework 3.5.1, Microsoft .NET Framework 4.6.2/4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.5.2, Microsoft .NET Framework 4.7.1/4.7.2, Microsoft .NET Framework 4.7.2.
CVE-2018-8540
A remote code execution vulnerability exists when the Microsoft .NET Framework fails to validate input properly, aka ".NET Framework Remote Code Injection Vulnerability." This affects Microsoft .NET Framework 4.6, Microsoft .NET Framework 3.5, Microsoft .NET Framework 4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.6/4.6.1/4.6.2/4.7/4.7.1/4.7.1/4.7.2, Microsoft .NET Framework 3.5.1, Microsoft .NET Framework 4.6.2/4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.5.2, Microsoft .NET Framework 4.7.1/4.7.2, Microsoft .NET Framework 4.7.2, Microsoft .NET Framework 4.6.2.
CVE-2018-8580
An information disclosure vulnerability exists where certain modes of the search function in Microsoft SharePoint Server are vulnerable to cross-site search attacks (a variant of cross-site request forgery, CSRF), aka "Microsoft SharePoint Information Disclosure Vulnerability." This affects Microsoft SharePoint.
CVE-2018-8583
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8617, CVE-2018-8618, CVE-2018-8624, CVE-2018-8629.
CVE-2018-8587
A remote code execution vulnerability exists in Microsoft Outlook software when it fails to properly handle objects in memory, aka "Microsoft Outlook Remote Code Execution Vulnerability." This affects Office 365 ProPlus, Microsoft Office, Microsoft Outlook.
CVE-2018-8595
An information disclosure vulnerability exists when the Windows GDI component improperly discloses the contents of its memory, aka "Windows GDI Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8596.
CVE-2018-8596
An information disclosure vulnerability exists when the Windows GDI component improperly discloses the contents of its memory, aka "Windows GDI Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8595.
CVE-2018-8597
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Office 365 ProPlus, Microsoft Office, Microsoft Excel. This CVE ID is unique from CVE-2018-8636.
CVE-2018-8598
An information disclosure vulnerability exists when Microsoft Excel improperly discloses the contents of its memory, aka "Microsoft Excel Information Disclosure Vulnerability." This affects Office 365 ProPlus, Microsoft Office, Microsoft Excel. This CVE ID is unique from CVE-2018-8627.
CVE-2018-8599
An elevation of privilege vulnerability exists when the Diagnostics Hub Standard Collector Service improperly impersonates certain file operations, aka "Diagnostics Hub Standard Collector Service Elevation of Privilege Vulnerability." This affects Microsoft Visual Studio, Windows Server 2019, Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8604
A tampering vulnerability exists when Microsoft Exchange Server fails to properly handle profile data, aka "Microsoft Exchange Server Tampering Vulnerability." This affects Microsoft Exchange Server.
CVE-2018-8611
An elevation of privilege vulnerability exists when the Windows kernel fails to properly handle objects in memory, aka "Windows Kernel Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8612
A Denial Of Service vulnerability exists when Connected User Experiences and Telemetry Service fails to validate certain function values, aka "Connected User Experiences and Telemetry Service Denial of Service Vulnerability." This affects Windows Server 2016, Windows 10, Windows Server 2019, Windows 10 Servers.
CVE-2018-8617
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8583, CVE-2018-8618, CVE-2018-8624, CVE-2018-8629.
CVE-2018-8618
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8583, CVE-2018-8617, CVE-2018-8624, CVE-2018-8629.
CVE-2018-8619
A remote code execution vulnerability exists when the Internet Explorer VBScript execution policy does not properly restrict VBScript under specific conditions, aka "Internet Explorer Remote Code Execution Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10.
CVE-2018-8621
An information disclosure vulnerability exists when the Windows kernel improperly handles objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows Server 2012, Windows 7, Windows Server 2008 R2. This CVE ID is unique from CVE-2018-8477, CVE-2018-8622.
CVE-2018-8622
An information disclosure vulnerability exists when the Windows kernel improperly handles objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2008 R2. This CVE ID is unique from CVE-2018-8477, CVE-2018-8621.
CVE-2018-8624
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8583, CVE-2018-8617, CVE-2018-8618, CVE-2018-8629.
CVE-2018-8625
A remote code execution vulnerability exists in the way that the VBScript engine handles objects in memory, aka "Windows VBScript Engine Remote Code Execution Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10.
CVE-2018-8626
A remote code execution vulnerability exists in Windows Domain Name System (DNS) servers when they fail to properly handle requests, aka "Windows DNS Server Heap Overflow Vulnerability." This affects Windows Server 2012 R2, Windows Server 2019, Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8627
An information disclosure vulnerability exists when Microsoft Excel software reads out of bound memory due to an uninitialized variable, which could disclose the contents of memory, aka "Microsoft Excel Information Disclosure Vulnerability." This affects Microsoft Office, Office 365 ProPlus, Microsoft Excel, Microsoft Excel Viewer, Excel. This CVE ID is unique from CVE-2018-8598.
CVE-2018-8628
A remote code execution vulnerability exists in Microsoft PowerPoint software when the software fails to properly handle objects in memory, aka "Microsoft PowerPoint Remote Code Execution Vulnerability." This affects Microsoft Office, Office 365 ProPlus, Microsoft PowerPoint, Microsoft SharePoint, Microsoft PowerPoint Viewer, Office Online Server, Microsoft SharePoint Server.
CVE-2018-8629
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8583, CVE-2018-8617, CVE-2018-8618, CVE-2018-8624.
CVE-2018-8631
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10.
CVE-2018-8634
A remote code execution vulnerability exists in Windows where Microsoft text-to-speech fails to properly handle objects in the memory, aka "Microsoft Text-To-Speech Remote Code Execution Vulnerability." This affects Windows Server 2016, Windows 10, Windows Server 2019, Windows 10 Servers.
CVE-2018-8635
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted authentication request to an affected SharePoint server, aka "Microsoft SharePoint Server Elevation of Privilege Vulnerability." This affects Microsoft SharePoint Server, Microsoft SharePoint.
CVE-2018-8636
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Office 365 ProPlus, Microsoft Office, Microsoft Excel. This CVE ID is unique from CVE-2018-8597.
CVE-2018-8637
An information disclosure vulnerability exists in Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (KASLR) bypass, aka "Win32k Information Disclosure Vulnerability." This affects Windows 10 Servers, Windows 10, Windows Server 2019.
CVE-2018-8638
An information disclosure vulnerability exists when DirectX improperly handles objects in memory, aka "DirectX Information Disclosure Vulnerability." This affects Windows 10, Windows Server 2019.
CVE-2018-8639
An elevation of privilege vulnerability exists in Windows when the Win32k component fails to properly handle objects in memory, aka "Win32k Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8641.
CVE-2018-8641
An elevation of privilege vulnerability exists in Windows when the Windows kernel-mode driver fails to properly handle objects in memory, aka "Win32k Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2019, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8639.
CVE-2018-8643
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10.
CVE-2018-8649
A denial of service vulnerability exists when Windows improperly handles objects in memory, aka "Windows Denial of Service Vulnerability." This affects Windows 10, Windows Server 2019.
CVE-2018-8650
A cross-site-scripting (XSS) vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft Office SharePoint XSS Vulnerability." This affects Microsoft SharePoint.
CVE-2018-8651
A cross site scripting vulnerability exists when Microsoft Dynamics NAV does not properly sanitize a specially crafted web request to an affected Dynamics NAV server, aka "Microsoft Dynamics NAV Cross Site Scripting Vulnerability." This affects Microsoft Dynamics NAV.
CVE-2018-8652
A Cross-site Scripting (XSS) vulnerability exists when Windows Azure Pack does not properly sanitize user-provided input, aka "Windows Azure Pack Cross Site Scripting Vulnerability." This affects Windows Azure Pack Rollup 13.1.
