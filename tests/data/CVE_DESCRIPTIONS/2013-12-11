CVE-2013-3878
Stack-based buffer overflow in the LRPC client in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 allows local users to gain privileges by operating an LRPC server that sends a crafted LPC port message, aka "LRPC Client Buffer Overrun Vulnerability."
CVE-2013-3899
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 does not properly validate addresses, which allows local users to gain privileges via a crafted application, aka "Win32k Memory Corruption Vulnerability."
CVE-2013-3900
The WinVerifyTrust function in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly validate PE file digests during Authenticode signature verification, which allows remote attackers to execute arbitrary code via a crafted PE file, aka "WinVerifyTrust Signature Validation Vulnerability."
CVE-2013-3902
Use-after-free vulnerability in win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2008 R2 SP1 and Windows 7 SP1 on 64-bit platforms allows local users to gain privileges via a crafted application, aka "Win32k Use After Free Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms13-101

"Affected Software
Windows 7 for 32-bit Systems Service Pack 1 (2893984)"

CVE-2013-3903
Array index error in win32k.sys in the kernel-mode drivers in Microsoft Windows 8, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to cause a denial of service (reboot) via a crafted TrueType font (TTF) file, aka "TrueType Font Parsing Vulnerability."
CVE-2013-3907
portcls.sys in the kernel-mode drivers in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges via a crafted application, aka "Port-Class Driver Double Fetch Vulnerability."
CVE-2013-5042
Cross-site scripting (XSS) vulnerability in Microsoft ASP.NET SignalR 1.1.x before 1.1.4 and 2.0.x before 2.0.1, and Visual Studio Team Foundation Server 2013, allows remote attackers to inject arbitrary web script or HTML via crafted Forever Frame transport protocol data, aka "SignalR XSS Vulnerability."
CVE-2013-5045
Microsoft Internet Explorer 10 and 11 allows local users to bypass the Protected Mode protection mechanism, and consequently gain privileges, by leveraging the ability to execute sandboxed code, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2013-5046
Microsoft Internet Explorer 7 through 11 allows local users to bypass the Protected Mode protection mechanism, and consequently gain privileges, by leveraging the ability to execute sandboxed code, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2013-5047
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-5048.
CVE-2013-5048
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-5047.
CVE-2013-5049
Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-5051
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-5052
Microsoft Internet Explorer 7 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-5054
Microsoft Office 2013 and 2013 RT allows remote attackers to discover authentication tokens via a crafted response to a file-open request for an Office file on a web site, as exploited in the wild in 2013, aka "Token Hijacking Vulnerability."
CVE-2013-5056
Use-after-free vulnerability in the Scripting Runtime Object Library in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site that is visited with Internet Explorer, aka "Use-After-Free Vulnerability in Microsoft Scripting Runtime Object Library."
CVE-2013-5057
hxds.dll in Microsoft Office 2007 SP3 and 2010 SP1 and SP2 does not implement the ASLR protection mechanism, which makes it easier for remote attackers to execute arbitrary code via a crafted COM component on a web site that is visited with Internet Explorer, as exploited in the wild in December 2013, aka "HXDS ASLR Vulnerability."
CVE-2013-5058
Integer overflow in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, and Windows Server 2012 Gold and R2 allows local users to gain privileges via a crafted application, aka "Win32k Integer Overflow Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms13-101

"Win32k Integer Overflow Vulnerability - CVE-2013-5058

A denial of service vulnerability exists in the way that the Win32k.sys kernel-mode driver handles objects in memory. An attacker who successfully exploited this vulnerability could cause the target system to stop responding."
CVE-2013-5059
Microsoft SharePoint Server 2010 SP1 and SP2 and 2013, and Office Web Apps 2013, allows remote attackers to execute arbitrary code via crafted page content, aka "SharePoint Page Content Vulnerabilities."
CVE-2013-5072
Cross-site scripting (XSS) vulnerability in Outlook Web Access in Microsoft Exchange Server 2010 SP2 and SP3 and 2013 Cumulative Update 2 and 3 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka "OWA XSS Vulnerability."
CVE-2013-5331
Adobe Flash Player before 11.7.700.257 and 11.8.x and 11.9.x before 11.9.900.170 on Windows and Mac OS X and before 11.2.202.332 on Linux, Adobe AIR before 3.9.0.1380, Adobe AIR SDK before 3.9.0.1380, and Adobe AIR SDK & Compiler before 3.9.0.1380 allow remote attackers to execute arbitrary code via crafted .swf content that leverages an unspecified "type confusion," as exploited in the wild in December 2013.
Per: http://helpx.adobe.com/security/products/flash-player/apsb13-28.html

"Adobe is aware of reports that an exploit designed to trick the user into opening a Microsoft Word document with malicious Flash (.swf) content exists for CVE-2013-5331."
CVE-2013-5332
Adobe Flash Player before 11.7.700.257 and 11.8.x and 11.9.x before 11.9.900.170 on Windows and Mac OS X and before 11.2.202.332 on Linux, Adobe AIR before 3.9.0.1380, Adobe AIR SDK before 3.9.0.1380, and Adobe AIR SDK & Compiler before 3.9.0.1380 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
Per: http://helpx.adobe.com/security/products/flash-player/apsb13-28.html

"Adobe is aware of reports that an exploit designed to trick the user into opening a Microsoft Word document with malicious Flash (.swf) content exists for CVE-2013-5331."
CVE-2013-5333
Adobe Shockwave Player before 12.0.7.148 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2013-5334.
CVE-2013-5334
Adobe Shockwave Player before 12.0.7.148 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2013-5333.
CVE-2013-5609
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2013-5610
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 26.0 and SeaMonkey before 2.23 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2013-5611
Mozilla Firefox before 26.0 does not properly remove the Application Installation doorhanger, which makes it easier for remote attackers to spoof a Web App installation site by controlling the timing of page navigation.
CVE-2013-5612
Cross-site scripting (XSS) vulnerability in Mozilla Firefox before 26.0 and SeaMonkey before 2.23 makes it easier for remote attackers to inject arbitrary web script or HTML by leveraging a Same Origin Policy violation triggered by lack of a charset parameter in a Content-Type HTTP header.
CVE-2013-5613
Use-after-free vulnerability in the PresShell::DispatchSynthMouseMove function in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via vectors involving synthetic mouse movement, related to the RestyleManager::GetHoverGeneration function.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2013-5614
Mozilla Firefox before 26.0 and SeaMonkey before 2.23 do not properly consider the sandbox attribute of an IFRAME element during processing of a contained OBJECT element, which allows remote attackers to bypass intended sandbox restrictions via a crafted web site.
CVE-2013-5615
The JavaScript implementation in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 does not properly enforce certain typeset restrictions on the generation of GetElementIC typed array stubs, which has unspecified impact and remote attack vectors.
CVE-2013-5616
Use-after-free vulnerability in the nsEventListenerManager::HandleEventSubType function in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via vectors related to mListeners event listeners.
CVE-2013-5618
Use-after-free vulnerability in the nsNodeUtils::LastRelease function in the table-editing user interface in the editor component in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 allows remote attackers to execute arbitrary code by triggering improper garbage collection.
CVE-2013-5619
Multiple integer overflows in the binary-search implementation in SpiderMonkey in Mozilla Firefox before 26.0 and SeaMonkey before 2.23 might allow remote attackers to cause a denial of service (out-of-bounds array access) or possibly have unspecified other impact via crafted JavaScript code.
CVE-2013-6671
The nsGfxScrollFrameInner::IsLTR function in Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 allows remote attackers to execute arbitrary code via crafted use of JavaScript code for ordered list elements.
CVE-2013-6672
Mozilla Firefox before 26.0 and SeaMonkey before 2.23 on Linux allow user-assisted remote attackers to read clipboard data by leveraging certain middle-click paste operations.
Per: http://www.mozilla.org/security/announce/2013/mfsa2013-112.html

"Windows and OS X systems are not affected by this issue. "
CVE-2013-6673
Mozilla Firefox before 26.0, Firefox ESR 24.x before 24.2, Thunderbird before 24.2, and SeaMonkey before 2.23 do not recognize a user's removal of trust from an EV X.509 certificate, which makes it easier for man-in-the-middle attackers to spoof SSL servers in opportunistic circumstances via a valid certificate that is unacceptable to the user.
