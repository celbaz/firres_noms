CVE-2008-3684
Heap-based buffer overflow in aws_tmxn.exe in the Admin Agent service in the server in EMC Documentum ApplicationXtender Workflow, possibly 5.40 SP1 and earlier, allows remote attackers to execute arbitrary code via crafted packet data to TCP port 2606.
CVE-2008-3685
Directory traversal vulnerability in aws_tmxn.exe in the Admin Agent service in the server in EMC Documentum ApplicationXtender Workflow, possibly 5.40 SP1 and earlier, allows remote attackers to upload arbitrary files, and execute arbitrary code, via directory traversal sequences in requests to TCP port 2606.
CVE-2009-1007
Unspecified vulnerability in the Data Mining component in Oracle Database 10.2.0.4 allows remote authenticated users to affect confidentiality, integrity, and availability, related to SYS.DMP_SYS.
CVE-2009-1018
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 allows remote authenticated users to affect confidentiality and integrity, related to SYS.LTRIC (WMSYS.LTRIC).
CVE-2009-1479
Directory traversal vulnerability in client/desktop/default.htm in Boxalino before 09.05.25-0421 allows remote attackers to read arbitrary files via a .. (dot dot) in the url parameter.
CVE-2009-1964
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
CVE-2009-1965
Unspecified vulnerability in the Net Foundation Layer component in Oracle Database 9.2.0.8 and 10.1.0.5 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

This vulnerability only affects the Windows platform.
CVE-2009-1971
Unspecified vulnerability in the Data Pump component in Oracle Database 10.1.0.5, 10.2.0.3, and 11.1.0.7 allows remote authenticated users to affect integrity via unknown vectors.
CVE-2009-1972
Unspecified vulnerability in the Auditing component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, 10.2.0.4, and 11.1.0.7 allows remote authenticated users to affect integrity, related to DBMS_SYS_SQL and DBMS_SQL.
CVE-2009-1979
Unspecified vulnerability in the Network Authentication component in Oracle Database 10.1.0.5 and 10.2.0.4 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.  NOTE: the previous information was obtained from the October 2009 CPU.  Oracle has not commented on claims from an independent researcher that this is related to improper validation of the AUTH_SESSKEY parameter length that leads to arbitrary code execution.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

# The CVSS Base Score is 10.0 only for Windows. For Linux, Unix and other platforms, the CVSS Base Score is 7.5, and the impacts for Confidentiality, Integrity and Availability are Partial+.
CVE-2009-1985
Unspecified vulnerability in the Network Authentication component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.4 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

"The CVSS Base Score is 10.0 only for Windows. For Linux, Unix and other platforms, the CVSS Base Score is 7.5, and the impacts for Confidentiality, Integrity and Availability are Partial+."
CVE-2009-1990
Unspecified vulnerability in the Business Intelligence Enterprise Edition component in Oracle Application Server 10.1.3.4.1 allows local users to affect confidentiality via unknown vectors.
CVE-2009-1991
Unspecified vulnerability in the Oracle Text component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.4 allows remote authenticated users to affect confidentiality and integrity, related to CTXSYS.DRVXTABC.  NOTE: the previous information was obtained from the October 2009 CPU.  Oracle has not commented on claims from an established researcher that this is for multiple SQL injection vulnerabilities via the (1) idx_owner or (2) idx_name parameters to the create_tables procedure.
CVE-2009-1992
Unspecified vulnerability in the Core RDBMS component in Oracle Database 9.2.0.8, 10.1.0.5, and 10.2.0.4 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

This vulnerability only affects the Windows platform.

The following Oracle Database Server vulnerability included in this Critical Patch Update affects client-only installations: CVE-2009-1992.
CVE-2009-1993
Unspecified vulnerability in the Application Express component in Oracle Database 3.0.1 allows remote authenticated users to affect confidentiality and integrity, related to FLOWS_030000.WWV_EXECUTE_IMMEDIATE.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

"Overview of Oracle Application Express

Oracle Application Express is a rapid web application development tool for the Oracle Database. In Oracle Database releases up to and including 10g Release 2, Oracle Application Express was separately installed from a Companion CD supplied with the Oracle Database CD set or from a package downloaded from an Oracle web site. If you have not installed Oracle Application Express from the companion CD or from a packaged download from an Oracle web site, no further action is required. From Oracle Database 11g onwards, Oracle Application Express is included in the default installation of the Oracle Database.

If you have Oracle Application Express installed in an Oracle Database home, then refer to Critical Patch Update October 2009 Patch Availability Document for Oracle Products, My Oracle Support Note 881382.1 for the version to be installed. "
CVE-2009-1994
Unspecified vulnerability in the Oracle Spatial component in Oracle Database 10.1.0.5 allows remote authenticated users to affect confidentiality, integrity, and availability, related to MDSYS.PRVT_CMT_CBK.
CVE-2009-1995
Unspecified vulnerability in the Advanced Queuing component in Oracle Database 10.2.0.4 and 11.1.0.7 allows remote authenticated users to affect confidentiality and integrity, related to SYS.DBMS_AQ_INV.
CVE-2009-1997
Unspecified vulnerability in the Authentication component in Oracle Database 10.2.0.3 and 11.1.0.7 allows remote attackers to affect confidentiality via unknown vectors.
CVE-2009-1998
Unspecified vulnerability in the Oracle Communications Order and Service Management component in Oracle Industry Applications 2.8.0, 6.2.0, 6.3.0, and 6.3.1 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
CVE-2009-1999
Unspecified vulnerability in the Business Intelligence Enterprise Edition component in unspecified Oracle Application Server versions allows remote attackers to affect integrity via unknown vectors.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

"Fixed in all supported versions. No patch provided in this Critical Patch Update."
CVE-2009-2000
Unspecified vulnerability in the Authentication component in Oracle Database 11.1.0.7 allows remote attackers to affect confidentiality via unknown vectors.
CVE-2009-2001
Unspecified vulnerability in the PL/SQL component in Oracle Database 10.2.0.4 and 11.1.0.7 allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-2002
Unspecified vulnerability in the WebLogic Portal component in BEA Product Suite 8.1.6, 9.2.3, 10.0.1, 10.2.1, and 10.3.1.0.0 allows remote attackers to affect integrity via unknown vectors.
CVE-2009-2911
SystemTap 1.0, when the --unprivileged option is used, does not properly restrict certain data sizes, which allows local users to (1) cause a denial of service or gain privileges via a print operation with a large number of arguments that trigger a kernel stack overflow, (2) cause a denial of service via crafted DWARF expressions that trigger a kernel stack frame overflow, or (3) cause a denial of service (infinite loop) via vectors that trigger creation of large unwind tables, related to Common Information Entry (CIE) and Call Frame Instruction (CFI) records.
CVE-2009-2940
The pygresql module 3.8.1 and 4.0 for Python does not properly support the PQescapeStringConn function, which might allow remote attackers to leverage escaping issues involving multibyte character encodings.
CVE-2009-2942
The mysql-ocaml bindings 1.0.4 for MySQL do not properly support the mysql_real_escape_string function, which might allow remote attackers to leverage escaping issues involving multibyte character encodings.
CVE-2009-2943
The postgresql-ocaml bindings 1.5.4, 1.7.0, and 1.12.1 for PostgreSQL libpq do not properly support the PQescapeStringConn function, which might allow remote attackers to leverage escaping issues involving multibyte character encodings.
CVE-2009-3392
Unspecified vulnerability in the Agile Engineering Data Management (EDM) component in Oracle E-Business Suite 6.1.0.0 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-3393
Unspecified vulnerability in the Oracle Application Object Library component in Oracle E-Business Suite 11.5.10.2 allows remote attackers to affect integrity via unknown vectors.
CVE-2009-3395
Unspecified vulnerability in the AutoVue component in Oracle E-Business Suite 19.3.2 allows remote attackers to affect availability via unknown vectors.
CVE-2009-3396
Unspecified vulnerability in the WebLogic Server component in BEA Product Suite 9.0, 9.1, 9.2.3, 10.0.1, and 10.3 allows remote attackers to affect integrity, related to WLS Console.
CVE-2009-3397
Unspecified vulnerability in the Oracle Application Object Library component in Oracle E-Business Suite 12.0.6 and 12.1.1 allows remote attackers to affect confidentiality via unknown vectors.
CVE-2009-3399
Unspecified vulnerability in the WebLogic Server component in BEA Product Suite 7.0.6 and 8.1.5 allows remote attackers to affect integrity, related to WLS Console.
CVE-2009-3400
Unspecified vulnerability in the Oracle Advanced Benefits component in Oracle E-Business Suite 11.5.10.2, 12.0.6, and 12.1.1 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
CVE-2009-3401
Unspecified vulnerability in the Oracle Applications Technology Stack component in Oracle E-Business Suite 11.5.10.2, 12.0.6, and 12.1.1 allows local users to affect confidentiality via unknown vectors.
CVE-2009-3402
Unspecified vulnerability in the Oracle Applications Framework component in Oracle E-Business Suite 11.5.10.2, 12.0.6, and 12.1.1 allows remote authenticated users to affect confidentiality via unknown vectors.
CVE-2009-3403
Unspecified vulnerability in the JRockit component in BEA Product Suite R27.6.4: JRE/JDK, 1.4.2, 5, and, and 6 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.  NOTE: this issue subsumes CVE-2009-2670, CVE-2009-2671, CVE-2009-2672, CVE-2009-2673, CVE-2009-2674, CVE-2009-2675, and CVE-2009-2676.
Per: http://www.oracle.com/technology/deploy/security/critical-patch-updates/cpuoct2009.html

"Sun MicroSystems released a Security Alert in August 2009 to address multiple vulnerabilities affecting the Sun Java Runtime Environment. Oracle CVE-2009-3403 refers to the advisories that were applicable to JRockit from the Sun Alert. The CVSS score of this vulnerability CVE# reflects the highest among those fixed in JRockit. The score is calculated by National Vulnerability Database (NVD), not Oracle. The complete list of all advisories addressed in JRockit under CVE-2009-3403 is as follows: CVE-2009-2670, CVE-2009-2671, CVE-2009-2672, CVE-2009-2673, CVE-2009-2674, CVE-2009-2675, CVE-2009-2676."
CVE-2009-3404
Unspecified vulnerability in the PeopleSoft PeopleTools & Enterprise Portal component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.49.23 allows remote authenticated users to affect integrity via unknown vectors.
CVE-2009-3405
Unspecified vulnerability in the JD Edwards Tools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.98.1.4 allows remote authenticated users to affect integrity and availability via unknown vectors.
CVE-2009-3406
Unspecified vulnerability in the JD Edwards Tools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.98.2.1 allows remote authenticated users to affect confidentiality via unknown vectors.
CVE-2009-3407
Unspecified vulnerability in the Portal component in Oracle Application Server 10.1.2.3 and 10.1.4.2 allows remote attackers to affect integrity via unknown vectors, a different vulnerability than CVE-2009-0974 and CVE-2009-0983.
CVE-2009-3408
Unspecified vulnerability in the Oracle Application Object Library component in Oracle E-Business Suite 11.5.10 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-3409
Unspecified vulnerability in the PeopleSoft Enterprise HCM (TAM) component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 9.0 Bundle 10 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
CVE-2009-3620
The ATI Rage 128 (aka r128) driver in the Linux kernel before 2.6.31-git11 does not properly verify Concurrent Command Engine (CCE) state initialization, which allows local users to cause a denial of service (NULL pointer dereference and system crash) or possibly gain privileges via unspecified ioctl calls.
CVE-2009-3621
net/unix/af_unix.c in the Linux kernel 2.6.31.4 and earlier allows local users to cause a denial of service (system hang) by creating an abstract-namespace AF_UNIX listening socket, performing a shutdown operation on this socket, and then performing a series of connect operations to this socket.
CVE-2009-3744
rep_serv.exe 6.3.1.3 in the server in EMC RepliStor allows remote attackers to cause a denial of service via a crafted packet to TCP port 7144.
CVE-2009-3745
Cross-site scripting (XSS) vulnerability in the help pages in IBM Rational AppScan Enterprise Edition 5.5.0.2 allows remote attackers to inject arbitrary web script or HTML via the query string.
CVE-2009-3746
XScreenSaver in Sun Solaris 10, when the accessibility feature is enabled, allows physically proximate attackers to obtain sensitive information by reading popup windows, which are displayed even when the screen is locked, a different vulnerability than CVE-2009-1276 and CVE-2009-2711.
CVE-2009-3747
Cross-site scripting (XSS) vulnerability in index.php in TBmnetCMS 1.0 allows remote attackers to inject arbitrary web script or HTML via the content parameter.  NOTE: this was originally reported for tbmnet.php, but that program does not exist in the TBmnetCMS 1.0 distribution.
CVE-2009-3748
Multiple cross-site scripting (XSS) vulnerabilities in the Web Administrator in Websense Personal Email Manager 7.1 before Hotfix 4 and Email Security 7.1 before Hotfix 4 allow remote attackers to inject arbitrary web script or HTML via the (1) FileName, (2) IsolatedMessageID, (3) ServerName, (4) Dictionary, (5) Scoring, and (6) MessagePart parameters to web/msgList/viewmsg/actions/msgAnalyse.asp; the (7) Queue, (8) FileName, (9) IsolatedMessageID, and (10) ServerName parameters to actions/msgForwardToRiskFilter.asp and viewHeaders.asp in web/msgList/viewmsg/; and (11) the subject in an e-mail message that is held in a Queue.
CVE-2009-3749
The Web Administrator service (STEMWADM.EXE) in Websense Personal Email Manager 7.1 before Hotfix 4 and Email Security 7.1 before Hotfix 4 allows remote attackers to cause a denial of service (crash) by sending a HTTP GET request to TCP port 8181 and closing the socket before the service can send a response.
CVE-2009-3750
SQL injection vulnerability in read.php in ToyLog 0.1 allows remote attackers to execute arbitrary SQL commands via the idm parameter.
CVE-2009-3751
Cross-site scripting (XSS) vulnerability in home.php in Opial 1.0 allows remote attackers to inject arbitrary web script or HTML via the genres_parent parameter.
CVE-2009-3752
SQL injection vulnerability in home.php in Opial 1.0 allows remote attackers to execute arbitrary SQL commands via the genres_parent parameter.
CVE-2009-3753
Unrestricted file upload vulnerability in Opial 1.0 allows remote attackers to execute arbitrary code by uploading a file with an executable extension as a User Image, then accessing it via a request to the file in userimages, related to register.php.
CVE-2009-3754
Multiple SQL injection vulnerabilities in phpBMS 0.96 allow remote attackers to execute arbitrary SQL commands via the (1) id parameter to modules/bms/invoices_discount_ajax.php, (2) f parameter to dbgraphic.php, and (3) tid parameter in a show action to advancedsearch.php.
CVE-2009-3755
Multiple cross-site scripting (XSS) vulnerabilities in phpBMS 0.96 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) index.php and (2) modules\base\myaccount.php; and the PATH_INFO to (3) modules_view.php, (4) tabledefs_options.php, and (5) adminsettings.php in phpbms\modules\base\.
CVE-2009-3756
phpBMS 0.96 allows remote attackers to obtain sensitive information via a direct request to (1) footer.php, (2) header.php, (3) the show action in advancedsearch.php, and (4) choicelist.php, which reveals the installation path in an error message.
CVE-2009-3757
Multiple cross-site scripting (XSS) vulnerabilities in sample code in the XenServer Resource Kit in Citrix XenCenterWeb allow remote attackers to inject arbitrary web script or HTML via the (1) username parameter to config/edituser.php; (2) location, (3) sessionid, and (4) vmname parameters to console.php; (5) vmrefid and (6) vmname parameters to forcerestart.php; and (7) vmname and (8) vmrefid parameters to forcesd.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-3758
SQL injection vulnerability in login.php in sample code in the XenServer Resource Kit in Citrix XenCenterWeb allows remote attackers to execute arbitrary SQL commands via the username parameter.  NOTE: some of these details are obtained from third party information.
CVE-2009-3759
Multiple cross-site request forgery (CSRF) vulnerabilities in sample code in the XenServer Resource Kit in Citrix XenCenterWeb allow remote attackers to hijack the authentication of administrators for (1) requests that change the password via the username parameter to config/changepw.php or (2) stop a virtual machine via the stop_vmname parameter to hardstopvm.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-3760
Static code injection vulnerability in config/writeconfig.php in the sample code in the XenServer Resource Kit in Citrix XenCenterWeb allows remote attackers to inject arbitrary PHP code into include/config.ini.php via the pool1 parameter.  NOTE: some of these details are obtained from third party information.
