CVE-2014-5414
Beckhoff Embedded PC images before 2014-10-22 and Automation Device Specification (ADS) TwinCAT components do not restrict the number of authentication attempts, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2014-5415
Beckhoff Embedded PC images before 2014-10-22 and Automation Device Specification (ADS) TwinCAT components might allow remote attackers to obtain access via the (1) Windows CE Remote Configuration Tool, (2) CE Remote Display service, or (3) TELNET service.
CVE-2016-0913
The client in EMC Replication Manager (RM) before 5.5.3.0_01-PatchHotfix, EMC Network Module for Microsoft 3.x, and EMC Networker Module for Microsoft 8.2.x before 8.2.3.6 allows remote RM servers to execute arbitrary commands by placing a crafted script in an SMB share.
CVE-2016-1246
Buffer overflow in the DBD::mysql module before 4.037 for Perl allows context-dependent attackers to cause a denial of service (crash) via vectors related to an error message.
CVE-2016-1455
Cisco NX-OS before 7.0(3)I2(2e) and 7.0(3)I4 before 7.0(3)I4(1) has an incorrect iptables local-interface configuration, which allows remote attackers to obtain sensitive information via TCP or UDP traffic, aka Bug ID CSCuz05365.
CVE-2016-2307
American Auto-Matrix Aspect-Nexus Building Automation Front-End Solutions application before 3.0.0 and Aspect-Matrix Building Automation Front-End Solutions application allow remote attackers to read arbitrary files via unspecified vectors, as demonstrated by the configuration file.
CVE-2016-2308
American Auto-Matrix Aspect-Nexus Building Automation Front-End Solutions application before 3.0.0 and Aspect-Matrix Building Automation Front-End Solutions application store passwords in cleartext, which allows remote attackers to obtain sensitive information by reading a file.
<a href="http://cwe.mitre.org/data/definitions/312.html">CWE-312: Cleartext Storage of Sensitive Information</a>
CVE-2016-4387
The Filter SDK in HPE KeyView 10.18 through 10.24 allows remote attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4388, CVE-2016-4389, and CVE-2016-4390.
CVE-2016-4388
The Filter SDK in HPE KeyView 10.18 through 10.24 allows remote attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4387, CVE-2016-4389, and CVE-2016-4390.
CVE-2016-4389
The Filter SDK in HPE KeyView 10.18 through 10.24 allows remote attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4387, CVE-2016-4388, and CVE-2016-4390.
CVE-2016-4390
The Filter SDK in HPE KeyView 10.18 through 10.24 allows remote attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4387, CVE-2016-4388, and CVE-2016-4389.
CVE-2016-4551
The (1) SAP_BASIS and (2) SAP_ABA components 7.00 SP Level 0031 in SAP NetWeaver 2004s might allow remote attackers to spoof IP addresses written to the Security Audit Log via vectors related to the network landscape, aka SAP Security Note 2190621.
CVE-2016-5084
Johnson & Johnson Animas OneTouch Ping devices do not use encryption for certain data, which might allow remote attackers to obtain sensitive information by sniffing the network.
CVE-2016-5085
Johnson & Johnson Animas OneTouch Ping devices do not properly generate random numbers, which makes it easier for remote attackers to spoof meters by sniffing the network and then engaging in an authentication handshake.
CVE-2016-5086
Johnson & Johnson Animas OneTouch Ping devices allow remote attackers to bypass authentication via replay attacks.
CVE-2016-5686
Johnson & Johnson Animas OneTouch Ping devices mishandle acknowledgements, which makes it easier for remote attackers to bypass authentication via a custom communication protocol.
CVE-2016-5745
F5 BIG-IP LTM systems 11.x before 11.2.1 HF16, 11.3.x, 11.4.x before 11.4.1 HF11, 11.5.0, 11.5.1 before HF11, 11.5.2, 11.5.3, 11.5.4 before HF2, 11.6.0 before HF8, 11.6.1 before HF1, 12.0.0 before HF4, and 12.1.0 before HF2 allow remote attackers to modify or extract system configuration files via vectors involving NAT64.
CVE-2016-5892
Cross-site scripting (XSS) vulnerability in IBM 10x, as used in Multi-Enterprise Integration Gateway 1.x through 1.0.0.1 and B2B Advanced Communications before 1.0.0.5_2, allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5901
Cross-site scripting (XSS) vulnerability in a test page in IBM Business Process Manager Advanced 8.5.6.0 through 8.5.7.0 before cumulative fix 2016.09 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5983
IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.43, 8.0 before 8.0.0.13, 8.5 before 8.5.5.11, 9.0 before 9.0.0.2, and Liberty before 16.0.0.4 allows remote authenticated users to execute arbitrary Java code via a crafted serialized object.
CVE-2016-6378
Cisco IOS XE 3.1 through 3.17 and 16.1 through 16.2 allows remote attackers to cause a denial of service (device reload) via crafted ICMP packets that require NAT, aka Bug ID CSCuw85853.
CVE-2016-6379
Cisco IOS 12.2 and IOS XE 3.14 through 3.16 and 16.1 allow remote attackers to cause a denial of service (device reload) via crafted IP Detail Record (IPDR) packets, aka Bug ID CSCuu35089.
CVE-2016-6380
The DNS forwarder in Cisco IOS 12.0 through 12.4 and 15.0 through 15.6 and IOS XE 3.1 through 3.15 allows remote attackers to obtain sensitive information from process memory or cause a denial of service (data corruption or device reload) via a crafted DNS response, aka Bug ID CSCup90532.
CVE-2016-6381
Cisco IOS 12.4 and 15.0 through 15.6 and IOS XE 3.1 through 3.18 and 16.1 allow remote attackers to cause a denial of service (memory consumption or device reload) via fragmented IKEv1 packets, aka Bug ID CSCuy47382.
CVE-2016-6382
Cisco IOS 15.2 through 15.6 and IOS XE 3.6 through 3.17 and 16.1 allow remote attackers to cause a denial of service (device restart) via a malformed IPv6 Protocol Independent Multicast (PIM) register packet, aka Bug ID CSCuy16399.
CVE-2016-6384
Cisco IOS 12.2 through 12.4 and 15.0 through 15.6 and IOS XE 3.1 through 3.17 and 16.2 allow remote attackers to cause a denial of service (device reload) via crafted fields in an H.323 message, aka Bug ID CSCux04257.
CVE-2016-6385
Memory leak in the Smart Install client implementation in Cisco IOS 12.2 and 15.0 through 15.2 and IOS XE 3.2 through 3.8 allows remote attackers to cause a denial of service (memory consumption) via crafted image-list parameters, aka Bug ID CSCuy82367.
CVE-2016-6386
Cisco IOS XE 3.1 through 3.17 and 16.1 on 64-bit platforms allows remote attackers to cause a denial of service (data-structure corruption and device reload) via fragmented IPv4 packets, aka Bug ID CSCux66005.
CVE-2016-6391
Cisco IOS 12.2 and 15.0 through 15.3 allows remote attackers to cause a denial of service (traffic-processing outage) via a crafted series of Common Industrial Protocol (CIP) requests, aka Bug ID CSCur69036.
CVE-2016-6392
Cisco IOS 12.2 and 15.0 through 15.3 and IOS XE 3.1 through 3.9 allow remote attackers to cause a denial of service (device restart) via a crafted IPv4 Multicast Source Discovery Protocol (MSDP) Source-Active (SA) message, aka Bug ID CSCud36767.
CVE-2016-6393
The AAA service in Cisco IOS 12.0 through 12.4 and 15.0 through 15.6 and IOS XE 2.1 through 3.18 and 16.2 allows remote attackers to cause a denial of service (device reload) via a failed SSH connection attempt that is mishandled during generation of an error-log message, aka Bug ID CSCuy87667.
CVE-2016-6416
The FTP service in Cisco AsyncOS on Email Security Appliance (ESA) devices 9.6.0-000 through 9.9.6-026, Web Security Appliance (WSA) devices 9.0.0-162 through 9.5.0-444, and Content Security Management Appliance (SMA) devices allows remote attackers to cause a denial of service via a flood of FTP traffic, aka Bug IDs CSCuz82907, CSCuz84330, and CSCuz86065.
CVE-2016-6417
Cross-site request forgery (CSRF) vulnerability in Cisco FireSIGHT System Software 4.10.2 through 6.1.0 and Firepower Management Center allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCva21636.
CVE-2016-6418
Cross-site scripting (XSS) vulnerability in Cisco Videoscape Distribution Suite Service Manager (VDS-SM) 3.0 through 3.4.0 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCva14552.
CVE-2016-6419
SQL injection vulnerability in Cisco Firepower Management Center 4.10.3 through 5.4.0 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCur25485.
CVE-2016-6420
Cisco FireSIGHT System Software 4.10.3 through 5.4.0 in Firepower Management Center allows remote authenticated users to bypass authorization checks and gain privileges via a crafted HTTP request, aka Bug ID CSCur25467.
CVE-2016-6421
Cisco IOS XR 5.2.2 allows remote attackers to cause a denial of service (process restart) via a crafted OSPF Link State Advertisement (LSA) update, aka Bug ID CSCvb05643.
CVE-2016-6423
The IKEv2 client and initiator implementations in Cisco IOS 15.5(3)M and IOS XE allow remote IKEv2 servers to cause a denial of service (device reload) via crafted IKEv2 packets, aka Bug ID CSCux97540.
CVE-2016-6426
The j_spring_security_switch_user function in Cisco Unified Intelligence Center (CUIC) 8.5.4 through 9.1(1), as used in Unified Contact Center Express 10.0(1) through 11.0(1), allows remote attackers to create user accounts by visiting an unspecified web page, aka Bug IDs CSCuy75027 and CSCuy81653.
CVE-2016-6550
The U by BB&T app 1.5.4 and earlier for iOS does not properly verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2016-6645
The vApp Managers web application in EMC Unisphere for VMAX Virtual Appliance 8.x before 8.3.0 and Solutions Enabler Virtual Appliance 8.x before 8.3.0 allows remote authenticated users to execute arbitrary code via crafted input to the (1) GeneralCmdRequest, (2) PersistantDataRequest, or (3) GetCommandExecRequest class.
CVE-2016-6646
The vApp Managers web application in EMC Unisphere for VMAX Virtual Appliance 8.x before 8.3.0 and Solutions Enabler Virtual Appliance 8.x before 8.3.0 allows remote attackers to execute arbitrary code via crafted input to the (1) GetSymmCmdRequest or (2) RemoteServiceHandler class.
CVE-2016-6652
SQL injection vulnerability in Pivotal Spring Data JPA before 1.9.6 (Gosling SR6) and 1.10.x before 1.10.4 (Hopper SR4), when used with a repository that defines a String query using the @Query annotation, allows attackers to execute arbitrary JPQL commands via a sort instance with a function call.
CVE-2016-7020
Use-after-free vulnerability in Adobe Flash Player before 18.0.0.366 and 19.x through 22.x before 22.0.0.209 on Windows and OS X and before 11.2.202.632 on Linux allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4173, CVE-2016-4174, CVE-2016-4222, CVE-2016-4226, CVE-2016-4227, CVE-2016-4228, CVE-2016-4229, CVE-2016-4230, CVE-2016-4231, and CVE-2016-4248.
CVE-2016-7161
Heap-based buffer overflow in the .receive callback of xlnx.xps-ethernetlite in QEMU (aka Quick Emulator) allows attackers to execute arbitrary code on the QEMU host via a large ethlite packet.
CVE-2016-7435
The (1) SCTC_REFRESH_EXPORT_TAB_COMP, (2) SCTC_REFRESH_CHECK_ENV, and (3) SCTC_TMS_MAINTAIN_ALOG functions in the SCTC subpackage in SAP Netweaver 7.40 SP 12 allow remote authenticated users with certain permissions to execute arbitrary commands via vectors involving a CALL 'SYSTEM' statement, aka SAP Security Note 2260344.
CVE-2016-7560
The rsyncd server in Fortinet FortiWLC 6.1-2-29 and earlier, 7.0-9-1, 7.0-10-0, 8.0-5-0, 8.1-2-0, and 8.2-4-0 has a hardcoded rsync account, which allows remote attackers to read or write to arbitrary files via unspecified vectors.
CVE-2016-7561
Fortinet FortiWLC 6.1-2-29 and earlier, 7.0-9-1, 7.0-10-0, 8.0-5-0, 8.1-2-0, and 8.2-4-0 allow administrators to obtain sensitive user credentials by reading the pam.log file.
CVE-2016-7907
The imx_fec_do_tx function in hw/net/imx_fec.c in QEMU (aka Quick Emulator) does not properly limit the buffer descriptor count when transmitting packets, which allows local guest OS administrators to cause a denial of service (infinite loop and QEMU process crash) via vectors involving a buffer descriptor with a length of 0 and crafted values in bd.flags.
CVE-2016-7908
The mcf_fec_do_tx function in hw/net/mcf_fec.c in QEMU (aka Quick Emulator) does not properly limit the buffer descriptor count when transmitting packets, which allows local guest OS administrators to cause a denial of service (infinite loop and QEMU process crash) via vectors involving a buffer descriptor with a length of 0 and crafted values in bd.flags.
CVE-2016-7909
The pcnet_rdra_addr function in hw/net/pcnet.c in QEMU (aka Quick Emulator) allows local guest OS administrators to cause a denial of service (infinite loop and QEMU process crash) by setting the (1) receive or (2) transmit descriptor ring length to 0.
CVE-2016-8343
Directory traversal vulnerability in INDAS Web SCADA before 3 allows remote attackers to read arbitrary files via unspecified vectors.
