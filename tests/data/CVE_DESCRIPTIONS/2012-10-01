CVE-2011-4551
Cross-site scripting (XSS) vulnerability in tiki-cookie-jar.php in TikiWiki CMS/Groupware before 8.2 and LTS before 6.5 allows remote attackers to inject arbitrary web script or HTML via arbitrary parameters.
CVE-2011-4945
PolicyKit 0.103 sets the AdminIdentities to "wheel" by default, which allows local users in the wheel group to gain root privileges without authentication.
CVE-2011-5202
BazisVirtualCDBus.sys in WinCDEmu 3.6 allows local users to cause a denial of service (system crash) via the unmount command to batchmnt.exe.
CVE-2012-0748
Multiple cross-site request forgery (CSRF) vulnerabilities in unspecified services in IBM Rational Team Concert (RTC) 4.x before 4.0.0.1 allow remote attackers to hijack the authentication of arbitrary users for requests that modify work items.
CVE-2012-0989
Cross-site scripting (XSS) vulnerability in OneOrZero AIMS 2.8.0 Trial Edition build231211 and possibly earlier allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to index.php.
CVE-2012-1470
Multiple cross-site scripting (XSS) vulnerabilities in code_editor.php in ocPortal before 7.1.6 allow remote attackers to inject arbitrary web script or HTML via the (1) path or (2) line parameters.
CVE-2012-1471
Directory traversal vulnerability in catalogue_file.php in ocPortal before 7.1.6 allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter.
CVE-2012-1576
The myuser_delete function in libathemecore/account.c in Atheme 5.x before 5.2.7, 6.x before 6.0.10, and 7.x before 7.0.0-beta2 does not properly clean up CertFP entries when a user is deleted, which allows remote attackers to access a different user account or cause a denial of service (daemon crash) via a login as a deleted user.
CVE-2012-1588
Algorithmic complexity vulnerability in the _filter_url function in the text filtering system (modules/filter/filter.module) in Drupal 7.x before 7.14 allows remote authenticated users with certain roles to cause a denial of service (CPU consumption) via a long email address.
CVE-2012-1590
The forum list in Drupal 7.x before 7.14 does not properly check user permissions for unpublished forum posts, which allows remote authenticated users to obtain sensitive information such as the post title via the forum overview page.
CVE-2012-1591
The image module in Drupal 7.x before 7.14 does not properly check permissions when caching derivative image styles of private images, which allows remote attackers to read private image styles.
CVE-2012-1602
user.php in NextBBS 0.6 allows remote attackers to bypass authentication and gain administrator access by setting the userkey cookie to 1.
CVE-2012-1603
Multiple SQL injection vulnerabilities in ajaxserver.php in NextBBS 0.6 allow remote attackers to execute arbitrary SQL commands via the (1) curstr parameter in the findUsers function, (2) id parameter in the isIdAvailable function, or (3) username parameter in the getGreetings function.
CVE-2012-1604
Cross-site scripting (XSS) vulnerability in NextBBS 0.6 allows remote attackers to inject arbitrary web script or HTML via the do parameter to index.php.
CVE-2012-1636
Cross-site request forgery (CSRF) vulnerability in the stickynote module before 7.x-1.1 for Drupal allows remote attackers to hijack the authentication of users for requests that delete stickynotes via unspecified vectors.
CVE-2012-1639
Multiple cross-site scripting (XSS) vulnerabilities in product/commerce_product.module in the Drupal Commerce module for Drupal before 7.x-1.2 allow remote authenticated users to inject arbitrary web script or HTML via the (1) sku or (2) title parameters.
CVE-2012-1897
Multiple cross-site request forgery (CSRF) vulnerabilities in Wolf CMS 0.75 and earlier allow remote attackers to hijack the authentication of administrators for requests that (1) delete users via the user id number to admin/user/delete; (2) delete pages via the page id number to admin/page/delete; delete the (3) images or (4) themes directory via the directory name to admin/plugin/file_manager/delete, and possibly other directories; or (5) logout the user via a request to admin/login/logout.
CVE-2012-1898
Multiple cross-site scripting (XSS) vulnerabilities in wolfcms/admin/user/add in Wolf CMS 0.75 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) user[name], (2) user[email], or (3) user[username] parameters.
CVE-2012-2153
Drupal 7.x before 7.14 does not properly restrict access to nodes in a list when using a "contributed node access module," which allows remote authenticated users with the "Access the content overview page" permission to read all published nodes by accessing the admin/content page.
CVE-2012-2240
scripts/dscverify.pl in devscripts before 2.12.3 allows remote attackers to execute arbitrary commands via unspecified vectors related to "arguments to external commands."
CVE-2012-2241
scripts/dget.pl in devscripts before 2.12.3 allows remote attackers to delete arbitrary files via a crafted (1) .dsc or (2) .changes file, probably related to a NULL byte in a filename.
CVE-2012-2242
scripts/dget.pl in devscripts before 2.10.73 allows remote attackers to execute arbitrary commands via a crafted (1) .dsc or (2) .changes file, related to "arguments to external commands" that are not properly escaped, a different vulnerability than CVE-2012-2240.
CVE-2012-3035
Buffer overflow in Emerson DeltaV 9.3.1 and 10.3 through 11.3.1 allows remote attackers to cause a denial of service (daemon crash) via a long string to an unspecified port.
CVE-2012-3319
IBM Rational Business Developer 8.x before 8.0.1.4 allows remote attackers to obtain potentially sensitive information via a connection to a web service created with the Rational Business Developer product.
CVE-2012-3500
scripts/annotate-output.sh in devscripts before 2.12.2, as used in rpmdevtools before 8.3, allows local users to modify arbitrary files via a symlink attack on the temporary (1) standard output or (2) standard error output file.
CVE-2012-4063
The Apache Santuario configuration in Eucalyptus before 3.1.1 does not properly restrict applying XML Signature transforms to documents, which allows remote attackers to cause a denial of service via unspecified vectors.
CVE-2012-4064
Eucalyptus before 3.1.1 does not properly restrict the binding of external SOAP web-services messages, which allows remote authenticated users to gain privileges by sending a message to (1) Cloud Controller or (2) Walrus with the internal message format and a modified user id.
CVE-2012-4065
Eucalyptus before 3.1.1 does not properly restrict the binding of external SOAP web-services messages, which allows remote authenticated users to bypass unspecified authorization checks and obtain direct access to a (1) Cloud Controller or (2) Walrus service via a crafted message, as demonstrated by changes to a volume, snapshot, or cloud configuration setting.
CVE-2012-4242
Cross-site scripting (XSS) vulnerability in the MF Gig Calendar plugin 0.9.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the query string to the calendar page.
CVE-2012-4415
Stack-based buffer overflow in the guac_client_plugin_open function in libguac in Guacamole before 0.6.3 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a long protocol name.
CVE-2012-4427
The gnome-shell plugin 3.4.1 in GNOME allows remote attackers to force the download and installation of arbitrary extensions from extensions.gnome.org via a crafted web page.
CVE-2012-4429
Vino 2.28, 2.32, 3.4.2, and earlier allows remote attackers to read clipboard activity by listening on TCP port 5900.
CVE-2012-4432
Use-after-free vulnerability in opngreduc.c in OptiPNG Hg and 0.7.x before 0.7.3 might allow remote attackers to execute arbitrary code via unspecified vectors related to "palette reduction."
CVE-2012-4437
Cross-site scripting (XSS) vulnerability in the SmartyException class in Smarty (aka smarty-php) before 3.1.12 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors that trigger a Smarty exception.
CVE-2012-4450
389 Directory Server 1.2.10 does not properly update the ACL when a DN entry is moved by a modrdn operation, which allows remote authenticated users with certain permissions to bypass ACL restrictions and access the DN entry.
CVE-2012-4830
Unspecified vulnerability in IBM WebSphere Commerce 6.0 through 6.0.0.11 and 7.0 through 7.0.0.6 allows remote attackers to obtain users' personal data via unknown vectors.
CVE-2012-4833
fuser in IBM AIX 6.1 and 7.1, and VIOS 2.2.1.4-FP-25 SP-02, does not properly restrict the -k option, which allows local users to kill arbitrary processes via a crafted command line.
CVE-2012-5223
The proc_deutf function in includes/functions_vbseocp_abstract.php in vBSEO 3.5.0, 3.5.1, 3.5.2, 3.6.0, and earlier allows remote attackers to insert and execute arbitrary PHP code via "complex curly syntax" in the char_repl parameter, which is inserted into a regular expression that is processed by the preg_replace function with the eval switch.
CVE-2012-5224
PHP remote file inclusion vulnerability in vb/includes/vba_cmps_include_bottom.php in vBadvanced CMPS 3.2.2 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the pages[template] parameter.
CVE-2012-5225
Cross-site scripting (XSS) vulnerability in webscr.php in xClick Cart 1.0.1 and 1.0.2 allows remote attackers to inject arbitrary web script or HTML via the shopping_url parameter.
CVE-2012-5226
Multiple cross-site scripting (XSS) vulnerabilities in Peel SHOPPING 2.8 and 2.9 allow remote attackers to inject arbitrary web script or HTML via the (1) motclef parameter to achat/recherche.php or (2) PATH_INFO to index.php.
CVE-2012-5227
SQL injection vulnerability in administrer/tva.php in Peel SHOPPING 2.8 and 2.9 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2012-5228
Cross-site scripting (XSS) vulnerability in admin/index.php in phplist 2.10.9, 2.10.17, and possibly other versions before 2.10.19 allows remote attackers to inject arbitrary web script or HTML via the testtarget parameter.  NOTE: some of these details are obtained from third party information.
CVE-2012-5229
Cross-site scripting (XSS) vulnerability in css/gallery-css.php in the Slideshow Gallery2 plugin for WordPress allows remote attackers to inject arbitrary web script or HTML via the border parameter.
CVE-2012-5230
Unspecified vulnerability in the JE Story Submit (com_jesubmit) component before 1.9 for Joomla! has unknown impact and attack vectors.
CVE-2012-5231
miniCMS 1.0 and 2.0 allows remote attackers to execute arbitrary PHP code via a crafted (1) pagename or (2) area variable containing an executable extension, which is not properly handled by (a) update.php when writing files to content/, or (b) updatenews.php when writing files to content/news/.
CVE-2012-5232
Cross-site scripting (XSS) vulnerability in the Quickl Form component for Joomla! allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-5233
Cross-site scripting (XSS) vulnerability in the stickynote module before 7.x-1.1 for Drupal allows remote authenticated users with edit stickynotes privileges to inject arbitrary web script or HTML via unspecified vecotrs.
CVE-2012-5234
Open redirect vulnerability in index.php in ocPortal before 7.1.6 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the redirect parameter.
