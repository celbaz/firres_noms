CVE-2015-1989
SQL injection vulnerability in IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-1993
IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 does not set the secure flag for unspecified cookies in an https session, which makes it easier for remote attackers to capture these cookies by intercepting their transmission within an http session.
<a href="https://cwe.mitre.org/data/definitions/614.html">CWE-614: Sensitive Cookie in HTTPS Session Without 'Secure' Attribute</a>
CVE-2015-1994
IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 does not include the HTTPOnly flag in a Set-Cookie header for the session cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.
CVE-2015-1995
Multiple cross-site scripting (XSS) vulnerabilities in IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 allow remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-1996
IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 does not prevent caching of HTTPS responses, which allows physically proximate attackers to obtain sensitive local-cache information by leveraging an unattended workstation.
CVE-2015-1997
Cross-site request forgery (CSRF) vulnerability in IBM Security QRadar Vulnerability Manager 7.2.x before 7.2.5 Patch 5 allows remote attackers to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2015-1999
IBM Security QRadar Incident Forensics 7.2.x before 7.2.5 Patch 5 places session IDs in https URLs, which allows remote attackers to obtain sensitive information by reading (1) web-server access logs, (2) web-server Referer logs, or (3) the browser history.
CVE-2015-2017
CRLF injection vulnerability in IBM WebSphere Application Server (WAS) 6.1 through 6.1.0.47, 7.0 before 7.0.0.39, 8.0 before 8.0.0.12, and 8.5 before 8.5.5.8 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a crafted URL.
<a href="http://cwe.mitre.org/data/definitions/113.html">CWE-113: Improper Neutralization of CRLF Sequences in HTTP Headers ('HTTP Response Splitting')</a>
CVE-2015-4928
Apache Ambari before 2.1, as used in IBM Infosphere BigInsights 4.x before 4.1, includes cleartext passwords on a Configs screen, which allows physically proximate attackers to obtain sensitive information by reading password fields.
Per <a href="http://www-01.ibm.com/support/docview.wss?uid=swg21969202"></a> CVSS Vector: AV:N/AC:M/Au:S/C:P/I:N/A:N states that this is a network attack and not local
CVE-2015-4940
Apache Ambari before 2.1, as used in IBM Infosphere BigInsights 4.x before 4.1, stores a cleartext BigSheets password in a configuration file, which allows local users to obtain sensitive information by reading this file.
CVE-2015-4963
IBM Security Access Manager for Web 7.x before 7.0.0.16 and 8.x before 8.0.1.3 mishandles WebSEAL HTTPTransformation requests, which allows remote attackers to read or write to arbitrary files via unspecified vectors.
CVE-2015-4966
IBM Maximo Asset Management 7.1 through 7.1.1.13, 7.5.0 before 7.5.0.9 FP009, and 7.6.0 before 7.6.0.2 IFIX001; Maximo Asset Management 7.5.0 before 7.5.0.9 FP009, 7.5.1, and 7.6.0 before 7.6.0.2 IFIX001 for SmartCloud Control Desk; and Maximo Asset Management 7.1 through 7.1.1.13 and 7.2 for Tivoli IT Asset Management for IT and certain other products have a default administrator account, which makes it easier for remote authenticated users to obtain access via unspecified vectors.
CVE-2015-5005
CSPOC in IBM PowerHA SystemMirror on AIX 6.1 and 7.1 allows remote authenticated users to perform an "su root" action by leveraging presence on the cluster-wide password-change list.
CVE-2015-5015
IBM WebSphere Commerce Enterprise 7.0.0.9 and 8.x before Feature Pack 8 allows remote attackers to obtain sensitive information via a crafted REST URL.
CVE-2015-5019
IBM Sterling Integrator 5.1 before 5010004_8 and Sterling B2B Integrator 5.2 before 5020500_9 allow remote authenticated users to read or upload files by leveraging a password-change requirement.
CVE-2015-5043
diag in IBM Security Guardium 8.2 before p6015, 9.0 before p6015, 9.1, 9.5, and 10.0 before p6015 allows local users to obtain root access via unspecified key sequences.
CVE-2015-5044
The Flow Collector in IBM Security QRadar QFLOW 7.1.x before 7.1 MR2 Patch 11 IF3 and 7.2.x before 7.2.5 Patch 4 IF3 allows remote attackers to cause a denial of service via unspecified packets.
CVE-2015-7395
IBM Maximo Asset Management 7.1 through 7.1.1.13, 7.5.0 before 7.5.0.8 IFIX005, and 7.6.0 before 7.6.0.2 FP002; Maximo Asset Management 7.5.0 before 7.5.0.8 IFIX005, 7.5.1, and 7.6.0 before 7.6.0.2 FP002 for SmartCloud Control Desk; and Maximo Asset Management 7.1 through 7.1.1.13 and 7.2 for Tivoli IT Asset Management for IT and certain other products allow remote authenticated users to bypass intended work-order change restrictions via unspecified vectors.
CVE-2015-7412
The GatewayScript modules on IBM DataPower Gateways with software 7.2.0.x before 7.2.0.1, when the GatewayScript decryption API or a JWE decrypt action is enabled, do not require signed ciphertext data, which makes it easier for remote attackers to obtain plaintext data via a padding-oracle attack.
