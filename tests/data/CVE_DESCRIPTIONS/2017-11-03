CVE-2017-1000131
Mahara 15.04 before 15.04.8 and 15.10 before 15.10.4 and 16.04 before 16.04.2 are vulnerable to users staying logged in to their Mahara account even when they have been logged out of Moodle (when using MNet) as Mahara did not properly implement one of the MNet SSO API functions.
CVE-2017-1000132
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable to a maliciously created .swf files that can have its code executed when a user tries to download the file.
CVE-2017-1000133
Mahara 15.04 before 15.04.8 and 15.10 before 15.10.4 and 16.04 before 16.04.2 are vulnerable to a user - in some circumstances causing another user's artefacts to be included in a Leap2a export of their own pages.
CVE-2017-1000134
Mahara 1.8 before 1.8.6 and 1.9 before 1.9.4 and 1.10 before 1.10.1 and 15.04 before 15.04.0 are vulnerable because group members can lose access to the group files they uploaded if another group member changes the access permissions on them.
CVE-2017-1000135
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable as logged-in users can stay logged in after the institution they belong to is suspended.
CVE-2017-1000136
Mahara 1.8 before 1.8.6 and 1.9 before 1.9.4 and 1.10 before 1.10.1 and 15.04 before 15.04.0 are vulnerable to old sessions not being invalidated after a password change.
CVE-2017-1000137
Mahara 1.10 before 1.10.0 and 15.04 before 15.04.0 are vulnerable to possible cross site scripting when adding a text block to a page via the keyboard (rather than drag and drop).
CVE-2017-1000138
Mahara 1.10 before 1.10.0 and 15.04 before 15.04.0 are vulnerable to possible cross site scripting when dragging/dropping files into a collection if the file has Javascript code in its title.
CVE-2017-1000139
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable to server-side request forgery attacks as not all processes of curl redirects are checked against a white or black list. Employing SafeCurl will prevent issues.
CVE-2017-1000140
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable to a maliciously created .xml file that can have its code executed when user tries to download the file.
CVE-2017-1000142
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable to users being able to delete their submitted page through URL manipulation.
CVE-2017-1000143
Mahara 1.8 before 1.8.7 and 1.9 before 1.9.5 and 1.10 before 1.10.3 and 15.04 before 15.04.0 are vulnerable to users receiving watchlist notifications about pages they do not have access to anymore.
CVE-2017-1000144
Mahara 1.9 before 1.9.6 and 1.10 before 1.10.4 and 15.04 before 15.04.1 are vulnerable to a site admin or institution admin being able to place HTML and Javascript into an institution display name, which will be displayed to other users unescaped on some Mahara system pages.
CVE-2017-1000145
Mahara 1.9 before 1.9.7 and 1.10 before 1.10.5 and 15.04 before 15.04.2 are vulnerable to anonymous comments being able to be placed on artefact detail pages even when the site administrator had disallowed anonymous comments.
CVE-2017-1000146
Mahara 1.9 before 1.9.7 and 1.10 before 1.10.5 and 15.04 before 15.04.2 are vulnerable to the arbitrary execution of Javascript in the browser of a logged-in user because the title of the portfolio page was not being properly escaped in the AJAX script that updates the Add/remove watchlist link on artefact detail pages.
CVE-2017-1000147
Mahara 1.9 before 1.9.8 and 1.10 before 1.10.6 and 15.04 before 15.04.3 are vulnerable to perform a cross-site request forgery (CSRF) attack on the uploader contained in Mahara's filebrowser widget. This could allow an attacker to trick a Mahara user into unknowingly uploading malicious files into their Mahara account.
CVE-2017-1000148
Mahara 15.04 before 15.04.8 and 15.10 before 15.10.4 and 16.04 before 16.04.2 are vulnerable to PHP code execution as Mahara would pass portions of the XML through the PHP "unserialize()" function when importing a skin from an XML file.
CVE-2017-1000149
Mahara 1.10 before 1.10.9 and 15.04 before 15.04.6 and 15.10 before 15.10.2 are vulnerable to XSS due to window.opener (target="_blank" and window.open())
CVE-2017-1000150
Mahara 15.04 before 15.04.7 and 15.10 before 15.10.3 are vulnerable to prevent session IDs from being regenerated on login or logout. This makes users of the site more vulnerable to session fixation attacks.
CVE-2017-1000151
Mahara 15.04 before 15.04.9 and 15.10 before 15.10.5 and 16.04 before 16.04.3 are vulnerable to passwords or other sensitive information being passed by unusual parameters to end up in an error log.
CVE-2017-1000152
Mahara 15.04 before 15.04.7 and 15.10 before 15.10.3 running PHP 5.3 are vulnerable to one user being logged in as another user on a separate computer as the same session ID is served. This situation can occur when a user takes an action that forces another user to be logged out of Mahara, such as an admin changing another user's account settings.
CVE-2017-1000153
Mahara 15.04 before 15.04.10 and 15.10 before 15.10.6 and 16.04 before 16.04.4 are vulnerable to incorrect access control after the password reset link is sent via email and then user changes default email, Mahara fails to invalidate old link.Consequently the link in email can be used to gain access to the user's account.
CVE-2017-1000154
Mahara 15.04 before 15.04.8 and 15.10 before 15.10.4 and 16.04 before 16.04.2 are vulnerable to some authentication methods, which do not use Mahara's built-in login form, still allowing users to log in even if their institution was expired or suspended.
CVE-2017-1000155
Mahara 15.04 before 15.04.8 and 15.10 before 15.10.4 and 16.04 before 16.04.2 are vulnerable to profile pictures being accessed without any access control checks consequently allowing any of a user's uploaded profile pictures to be viewable by anyone, whether or not they were currently selected as the "default" or used in any pages.
CVE-2017-1000156
Mahara 15.04 before 15.04.9 and 15.10 before 15.10.5 and 16.04 before 16.04.3 are vulnerable to a group's configuration page being editable by any group member even when they didn't have the admin role.
CVE-2017-1000157
Mahara 15.04 before 15.04.13 and 16.04 before 16.04.7 and 16.10 before 16.10.4 and 17.04 before 17.04.2 are vulnerable to recording plain text passwords in the event_log table during the user creation process if full event logging was turned on.
CVE-2017-1000171
Mahara Mobile before 1.2.1 is vulnerable to passwords being sent to the Mahara access log in plain text.
CVE-2017-14359
A potential security vulnerability has been identified in HPE Performance Center versions 12.20. The vulnerability could be remotely exploited to allow cross-site scripting.
CVE-2017-16237
In Vir.IT eXplorer Anti-Virus before 8.5.42, the driver file (VIAGLT64.SYS) contains an Arbitrary Write vulnerability because of not validating input values from IOCtl 0x8273007C.
CVE-2017-16513
Ipswitch WS_FTP Professional before 12.6.0.3 has buffer overflows in the local search field and the backup locations field, aka WSCLT-1729.
CVE-2017-16516
In the yajl-ruby gem 1.3.0 for Ruby, when a crafted JSON file is supplied to Yajl::Parser.new.parse, the whole ruby process crashes with a SIGABRT in the yajl_string_decode function in yajl_encode.c. This results in the whole ruby process terminating and potentially a denial of service.
CVE-2017-16522
MitraStar GPT-2541GNAC (HGU) 1.00(VNJ0)b1 and DSL-100HN-T1 ES_113WJY0b16 devices allow remote authenticated users to obtain root access by specifying /bin/sh as the command to execute.
CVE-2017-16523
MitraStar GPT-2541GNAC (HGU) 1.00(VNJ0)b1 and DSL-100HN-T1 ES_113WJY0b16 devices have a zyad1234 password for the zyad1234 account, which is equivalent to root and undocumented.
