CVE-2007-4575
HSQLDB before 1.8.0.9, as used in OpenOffice.org (OOo) 2 before 2.3.1, allows user-assisted remote attackers to execute arbitrary Java code via crafted database documents, related to "exposing static java methods."
CVE-2007-5769
Double free vulnerability in the getreply function in ftp.c in netkit ftp (netkit-ftp) 0.17 20040614 and later allows remote FTP servers to cause a denial of service (application crash) and possibly have unspecified other impact via some types of FTP protocol behavior. NOTE: the netkit-ftpd issue is covered by CVE-2007-6263.
CVE-2007-5894
** DISPUTED **  The reply function in ftpd.c in the gssftp ftpd in MIT Kerberos 5 (krb5) does not initialize the length variable when auth_type has a certain value, which has unknown impact and remote authenticated attack vectors.  NOTE: the original disclosure misidentifies the conditions under which the uninitialized variable is used.  NOTE: the vendor disputes this issue, stating " The 'length' variable is only uninitialized if 'auth_type' is neither the 'KERBEROS_V4' nor 'GSSAPI'; this condition cannot occur in the unmodified source code."
CVE-2007-5901
Use-after-free vulnerability in the gss_indicate_mechs function in lib/gssapi/mechglue/g_initialize.c in MIT Kerberos 5 (krb5) has unknown impact and attack vectors.  NOTE: this might be the result of a typo in the source code.
Information from Apple: http://docs.info.apple.com/article.html?artnum=307562
CVE-2007-5902
Integer overflow in the svcauth_gss_get_principal function in lib/rpc/svc_auth_gss.c in MIT Kerberos 5 (krb5) allows remote attackers to have an unknown impact via a large length value for a GSS client name in an RPC request.
CVE-2007-5938
The iwl_set_rate function in compatible/iwl3945-base.c in iwlwifi 1.1.21 and earlier dereferences an iwl_get_hw_mode return value without checking for NULL, which might allow remote attackers to cause a denial of service (kernel panic) via unspecified vectors during module initialization.
CVE-2007-5939
The gss_userok function in appl/ftp/ftpd/gss_userok.c in Heimdal 0.7.2 does not allocate memory for the ticketfile pointer before calling free, which allows remote attackers to have an unknown impact via an invalid username.  NOTE: the vulnerability was originally reported for ftpd.c, but this is incorrect.
CVE-2007-5971
Double free vulnerability in the gss_krb5int_make_seal_token_v3 function in lib/gssapi/krb5/k5sealv3.c in MIT Kerberos 5 (krb5) has unknown impact and attack vectors.
Information from Apple: http://docs.info.apple.com/article.html?artnum=307562
CVE-2007-5972
Double free vulnerability in the krb5_def_store_mkey function in lib/kdb/kdb_default.c in MIT Kerberos 5 (krb5) 1.5 has unknown impact and remote authenticated attack vectors.  NOTE: the free operations occur in code that stores the krb5kdc master key, and so the attacker must have privileges to store this key.
CVE-2007-6194
Unspecified vulnerability in HP Select Identity 4.01 before 4.01.012 and 4.1x before 4.13.003 allows remote attackers to obtain unspecified access via unknown vectors.
CVE-2007-6260
The installation process for Oracle 10g and llg uses accounts with default passwords, which allows remote attackers to obtain login access by connecting to the Listener.  NOTE: at the end of the installation, if performed using the Database Configuration Assistant (DBCA), most accounts are disabled or their passwords are changed.
CVE-2007-6261
Integer overflow in the load_threadstack function in the Mach-O loader (mach_loader.c) in the xnu kernel in Apple Mac OS X 10.4 through 10.5.1 allows local users to cause a denial of service (infinite loop) via a crafted Mach-O binary.
CVE-2007-6262
A certain ActiveX control in axvlc.dll in VideoLAN VLC 0.8.6 before 0.8.6d allows remote attackers to execute arbitrary code via crafted arguments to the (1) addTarget, (2) getVariable, or (3) setVariable function, resulting from a "bad initialized pointer," aka a "recursive plugin release vulnerability."
CVE-2007-6263
The dataconn function in ftpd.c in netkit ftpd (netkit-ftpd) 0.17, when certain modifications to support SSL have been introduced, calls fclose on an uninitialized file stream, which allows remote attackers to cause a denial of service (daemon crash) and possibly have unspecified other impact via some types of FTP over SSL protocol behavior, as demonstrated by breaking a passive FTP DATA connection in a way that triggers an error in the server's SSL_accept function. NOTE: the netkit ftp issue is covered by CVE-2007-5769.
