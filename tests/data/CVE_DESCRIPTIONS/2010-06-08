CVE-2010-0484
The Windows kernel-mode drivers in win32k.sys in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista SP1 and SP2, and Server 2008 Gold and SP2 "do not properly validate changes in certain kernel objects," which allows local users to execute arbitrary code via vectors related to Device Contexts (DC) and the GetDCEx function, aka "Win32k Improper Data Validation Vulnerability."
CVE-2010-0485
The Windows kernel-mode drivers in win32k.sys in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista SP1 and SP2, Server 2008 Gold and SP2, Windows 7, and Server 2008 R2 "do not properly validate all callback parameters when creating a new window," which allows local users to execute arbitrary code, aka "Win32k Window Creation Vulnerability."
CVE-2010-0811
Multiple unspecified vulnerabilities in the Microsoft Internet Explorer 8 Developer Tools ActiveX control in Microsoft Windows 2000 SP4, Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allow remote attackers to execute arbitrary code via unknown vectors that "corrupt the system state," aka "Microsoft Internet Explorer 8 Developer Tools Vulnerability."
CVE-2010-0819
Unspecified vulnerability in the Windows OpenType Compact Font Format (CFF) driver in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista SP1 and SP2, Server 2008 SP2 and R2, and Windows 7 allows local users to execute arbitrary code via unknown vectors related to improper validation when copying data from user mode to kernel mode, aka "OpenType CFF Font Driver Memory Corruption Vulnerability."
CVE-2010-0821
Unspecified vulnerability in Microsoft Office Excel 2002 SP3, 2003 SP3, 2007 SP1 and SP2; Office 2004 for mac; Office 2008 for Mac; Open XML File Format Converter for Mac; Office Excel Viewer SP1 and SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP1 and SP2; allows remote attackers to execute arbitrary code via an Excel file with a crafted SxView record, related to improper validation of unspecified structures, aka "Excel Record Parsing Memory Corruption Vulnerability," a different vulnerability than CVE-2010-0824 and CVE-2010-1245.
CVE-2010-0822
Stack-based buffer overflow in Microsoft Office Excel 2002 SP3, Office 2004 for Mac, Office 2008 for Mac, and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via an Excel file with a crafted OBJ (0x5D) record, aka "Excel Object Stack Overflow Vulnerability."
CVE-2010-0823
Unspecified vulnerability in Microsoft Office Excel 2002 SP3, 2003 SP3, 2007 SP1 and SP2; Office 2004 for mac; Office 2008 for Mac; Open XML File Format Converter for Mac; Office Excel Viewer SP1 and SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP1 and SP2; allows remote attackers to execute arbitrary code via a crafted Excel file, aka "Excel Memory Corruption Vulnerability," a different vulnerability than CVE-2010-1247 and CVE-2010-1249.
CVE-2010-0824
Unspecified vulnerability in Microsoft Office Excel 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via an Excel file with a malformed WOPT (0x80B) record, aka "Excel Record Memory Corruption Vulnerability," a different vulnerability than CVE-2010-0821 and CVE-2010-1245.
CVE-2010-1245
Unspecified vulnerability in Microsoft Office Excel 2002 SP3, Office 2004 for Mac, Office 2008 for Mac, and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via an Excel file with a malformed SxView (0xB0) record, aka "Excel Record Memory Corruption Vulnerability," a different vulnerability than CVE-2010-0824 and CVE-2010-0821.
CVE-2010-1246
Stack-based buffer overflow in Microsoft Office Excel 2002 SP3 allows remote attackers to execute arbitrary code via an Excel file with a malformed RTD (0x813) record, aka "Excel RTD Memory Corruption Vulnerability."
CVE-2010-1247
Unspecified vulnerability in Microsoft Office Excel 2002 SP3 allows remote attackers to execute arbitrary code via an Excel file with a malformed RTD (0x813) record that triggers heap corruption, aka "Excel Memory Corruption Vulnerability," a different vulnerability than CVE-2010-0823 and CVE-2010-1249.
CVE-2010-1248
Buffer overflow in Microsoft Office Excel 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via an Excel file with a malformed HFPicture (0x866) record, aka "Excel HFPicture Memory Corruption Vulnerability."
CVE-2010-1249
Buffer overflow in Microsoft Office Excel 2002 SP3, Office 2004 for Mac, Office 2008 for Mac, and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via an Excel file with a malformed ExternName (0x23) record, aka "Excel Memory Corruption Vulnerability," a different vulnerability than CVE-2010-0823 and CVE-2010-1247.
CVE-2010-1250
Heap-based buffer overflow in Microsoft Office Excel 2002 SP3, Office 2004 for Mac, Office 2008 for Mac, and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via an Excel file with malformed (1) EDG (0x88) and (2) Publisher (0x89) records, aka "Excel EDG Memory Corruption Vulnerability."
CVE-2010-1251
Unspecified vulnerability in Microsoft Office Excel 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via a crafted Excel file, aka "Excel Record Stack Corruption Vulnerability."
CVE-2010-1252
Unspecified vulnerability in Microsoft Office Excel 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via a crafted Excel file, aka "Excel String Variable Vulnerability."
CVE-2010-1253
Microsoft Office Excel 2002 SP3, 2007 SP1, and SP2; Office 2004 for mac; Office 2008 for Mac; Open XML File Format Converter for Mac; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP1 and SP2; allows remote attackers to execute arbitrary code via an Excel file with crafted DBQueryExt records that allow a function call to a "user-controlled pointer," aka "Excel ADO Object Vulnerability."
CVE-2010-1254
The installation for Microsoft Open XML File Format Converter for Mac sets insecure ACLs for the /Applications folder, which allows local users to execute arbitrary code by replacing the executable with a Trojan Horse, aka "Mac Office Open XML Permissions Vulnerability."
CVE-2010-1255
The Windows kernel-mode drivers in win32k.sys in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista SP1 and SP2, Server 2008 Gold and SP2, Windows 7, and Server 2008 R2 allows local users to execute arbitrary code via vectors related to "glyph outline information" and TrueType fonts, aka "Win32k TrueType Font Parsing Vulnerability."
CVE-2010-1256
Unspecified vulnerability in Microsoft IIS 6.0, 7.0, and 7.5, when Extended Protection for Authentication is enabled, allows remote authenticated users to execute arbitrary code via unknown vectors related to "token checking" that trigger memory corruption, aka "IIS Authentication Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/bulletin/ms10-040.mspx

'Mitigating Factors for IIS Authentication Memory Corruption Vulnerability - CVE-2010-1256

Without the installation of KB973917 on Windows Server 2003, Windows Vista, and Windows Server 2008, systems will not have the Extended Protection for Authentication feature and will not be vulnerable.

Extended Protection for Authentication is not enabled by default on any affected platform, even when a system has installed KB973917. Systems are only affected when this feature is enabled.'
CVE-2010-1257
Cross-site scripting (XSS) vulnerability in the toStaticHTML API, as used in Microsoft Office InfoPath 2003 SP3, 2007 SP1, and 2007 SP2; Office SharePoint Server 2007 SP1 and SP2; SharePoint Services 3.0 SP1 and SP2; and Internet Explorer 8 allows remote attackers to inject arbitrary web script or HTML via vectors related to sanitization.
CVE-2010-1259
Microsoft Internet Explorer 6 SP1 and SP2, 7, and 8 allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2010-1260
The IE8 Developer Toolbar in Microsoft Internet Explorer 8 SP1, SP2, and SP3 allows user-assisted remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "HTML Element Memory Corruption Vulnerability."
CVE-2010-1261
The IE8 Developer Toolbar in Microsoft Internet Explorer 8 SP1, SP2, and SP3 allows user-assisted remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2010-1262
Microsoft Internet Explorer 6 SP1 and SP2, 7, and 8 allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, related to the CStyleSheet object and a free of the root container, aka "Memory Corruption Vulnerability."
CVE-2010-1263
Windows Shell and WordPad in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7; Microsoft Office XP SP3; Office 2003 SP3; and Office System 2007 SP1 and SP2 do not properly validate COM objects during instantiation, which allows remote attackers to execute arbitrary code via a crafted file, aka "COM Validation Vulnerability."
CVE-2010-1264
Unspecified vulnerability in Microsoft Windows SharePoint Services 3.0 SP1 and SP2 allows remote attackers to cause a denial of service (hang) via crafted requests to the Help page that cause repeated restarts of the application pool, aka "Sharepoint Help Page Denial of Service Vulnerability."
CVE-2010-1297
Adobe Flash Player before 9.0.277.0 and 10.x before 10.1.53.64; Adobe AIR before 2.0.2.12610; and Adobe Reader and Acrobat 9.x before 9.3.3, and 8.x before 8.2.3 on Windows and Mac OS X, allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted SWF content, related to authplay.dll and the ActionScript Virtual Machine 2 (AVM2) newfunction instruction, as exploited in the wild in June 2010.
CVE-2010-1636
The btrfs_ioctl_clone function in fs/btrfs/ioctl.c in the btrfs functionality in the Linux kernel 2.6.29 through 2.6.32, and possibly other versions, does not ensure that a cloned file descriptor has been opened for reading, which allows local users to read sensitive information from a write-only file descriptor.
CVE-2010-1647
Cross-site scripting (XSS) vulnerability in MediaWiki 1.15 before 1.15.4 and 1.16 before 1.16 beta 3 allows remote attackers to inject arbitrary web script or HTML via crafted Cascading Style Sheets (CSS) strings that are processed as script by Internet Explorer.
CVE-2010-1648
Cross-site request forgery (CSRF) vulnerability in the login interface in MediaWiki 1.15 before 1.15.4 and 1.16 before 1.16 beta 3 allows remote attackers to hijack the authentication of users for requests that (1) create accounts or (2) reset passwords, related to the Special:Userlogin form.
CVE-2010-1649
Multiple cross-site scripting (XSS) vulnerabilities in the back end in Joomla! 1.5 through 1.5.17 allow remote attackers to inject arbitrary web script or HTML via unknown vectors related to "various administrator screens," possibly the search parameter in administrator/index.php.
CVE-2010-1848
Directory traversal vulnerability in MySQL 5.0 through 5.0.91 and 5.1 before 5.1.47 allows remote authenticated users to bypass intended table grants to read field definitions of arbitrary tables, and on 5.1 to read or delete content of arbitrary tables, via a .. (dot dot) in a table name.
CVE-2010-1849
The my_net_skip_rest function in sql/net_serv.cc in MySQL 5.0 through 5.0.91 and 5.1 before 5.1.47 allows remote attackers to cause a denial of service (CPU and bandwidth consumption) by sending a large number of packets that exceed the maximum length.
Per: http://cwe.mitre.org/data/definitions/371.html

'CWE-371: State Issues'
CVE-2010-1850
Buffer overflow in MySQL 5.0 through 5.0.91 and 5.1 before 5.1.47 allows remote authenticated users to execute arbitrary code via a COM_FIELD_LIST command with a long table name.
CVE-2010-1879
Unspecified vulnerability in Quartz.dll for DirectShow; Windows Media Format Runtime 9, 9.5, and 11; Media Encoder 9; and the Asycfilt.dll COM component allows remote attackers to execute arbitrary code via a media file with crafted compression data, aka "Media Decompression Vulnerability."
CVE-2010-1880
Unspecified vulnerability in Quartz.dll for DirectShow on Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista SP1, and Server 2008 allows remote attackers to execute arbitrary code via a media file with crafted compression data, aka "MJPEG Media Decompression Vulnerability."
CVE-2010-2059
lib/fsm.c in RPM 4.8.0 and unspecified 4.7.x and 4.6.x versions, and RPM before 4.4.3, does not properly reset the metadata of an executable file during replacement of the file in an RPM package upgrade, which might allow local users to gain privileges by creating a hard link to a vulnerable (1) setuid or (2) setgid file.
CVE-2010-2060
The put command functionality in beanstalkd 1.4.5 and earlier allows remote attackers to execute arbitrary Beanstalk commands via the body in a job that is too big, which is not properly handled by the dispatch_cmd function in prot.c.
Per: http://cwe.mitre.org/data/definitions/77.html

'CWE-77: Improper Sanitization of Special Elements used in a Command ('Command Injection')'
CVE-2010-2159
Dameng DM Database Server allows remote authenticated users to cause a denial of service (crash) and possibly execute arbitrary code via unspecified vectors related to the SP_DEL_BAK_EXPIRED procedure in wdm_dll.dll, which triggers memory corruption.
CVE-2010-2190
The (1) trim, (2) ltrim, (3) rtrim, and (4) substr_replace functions in PHP 5.2 through 5.2.13 and 5.3 through 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) by causing a userspace interruption of an internal function, related to the call time pass by reference feature.
CVE-2010-2191
The (1) parse_str, (2) preg_match, (3) unpack, and (4) pack functions; the (5) ZEND_FETCH_RW, (6) ZEND_CONCAT, and (7) ZEND_ASSIGN_CONCAT opcodes; and the (8) ArrayObject::uasort method in PHP 5.2 through 5.2.13 and 5.3 through 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) or trigger memory corruption by causing a userspace interruption of an internal function or handler.  NOTE: vectors 2 through 4 are related to the call time pass by reference feature.
CVE-2010-2197
rpmbuild in RPM 4.8.0 and earlier does not properly parse the syntax of spec files, which allows user-assisted remote attackers to remove home directories via vectors involving a ;~ (semicolon tilde) sequence in a Name tag.
CVE-2010-2198
lib/fsm.c in RPM 4.8.0 and earlier does not properly reset the metadata of an executable file during replacement of the file in an RPM package upgrade or deletion of the file in an RPM package removal, which might allow local users to gain privileges or bypass intended access restrictions by creating a hard link to a vulnerable file that has (1) POSIX file capabilities or (2) SELinux context information, a related issue to CVE-2010-2059.
CVE-2010-2199
lib/fsm.c in RPM 4.8.0 and earlier does not properly reset the metadata of an executable file during replacement of the file in an RPM package upgrade or deletion of the file in an RPM package removal, which might allow local users to bypass intended access restrictions by creating a hard link to a vulnerable file that has a POSIX ACL, a related issue to CVE-2010-2059.
