CVE-2015-1779
The VNC websocket frame decoder in QEMU allows remote attackers to cause a denial of service (memory and CPU consumption) via a large (1) websocket payload or (2) HTTP headers section.
CVE-2015-4671
Cross-site scripting (XSS) vulnerability in OpenCart before 2.1.0.2 allows remote attackers to inject arbitrary web script or HTML via the zone_id parameter to index.php.
CVE-2015-4703
Absolute path traversal vulnerability in mysqldump_download.php in the WordPress Rename plugin 1.0 for WordPress allows remote attackers to read arbitrary files via a full pathname in the dumpfname parameter.
CVE-2015-5471
Absolute path traversal vulnerability in include/user/download.php in the Swim Team plugin 1.44.10777 for WordPress allows remote attackers to read arbitrary files via a full pathname in the file parameter.
CVE-2015-7242
Cross-site scripting (XSS) vulnerability in the Push-Service-Mails feature in AVM FRITZ!OS before 6.30 allows remote attackers to inject arbitrary web script or HTML via the display name in the FROM field of an SIP INVITE message.
CVE-2015-7393
dcoep in BIG-IP LTM, Analytics, APM, ASM, and Link Controller 11.2.0 through 11.6.0 and 12.0.0 before 12.0.0 HF1, BIG-IP AAM 11.4.0 through 11.6.0 and 12.0.0 before 12.0.0 HF1, BIG-IP AFM and PEM 11.3.0 through 11.6.0 and 12.0.0 before 12.0.0 HF1, BIG-IP DNS 12.0.0 before 12.0.0 HF1, BIG-IP Edge Gateway, WebAccelerator, and WOM 11.2.0 through 11.3.0, BIG-IP GTM 11.2.0 through 11.6.0, BIG-IP PSM 11.2.0 through 11.4.1, Enterprise Manager 3.0.0 through 3.1.1, BIG-IQ Cloud 4.0.0 through 4.5.0, BIG-IQ Device 4.2.0 through 4.5.0, BIG-IQ Security 4.0.0 through 4.5.0, BIG-IQ ADC 4.5.0, BIG-IQ Centralized Management 4.6.0, and BIG-IQ Cloud and Orchestration 1.0.0 allows local users with advanced shell (bash) access to gain privileges via unspecified vectors.
CVE-2015-7548
OpenStack Compute (Nova) before 2015.1.3 (kilo) and 12.0.x before 12.0.1 (liberty), when using libvirt to spawn instances and use_cow_images is set to false, allow remote authenticated users to read arbitrary files by overwriting an instance disk with a crafted image and requesting a snapshot.
CVE-2015-7759
BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, Link Controller, and PEM 12.0.0 before HF1, when the TCP profile for a virtual server is configured with Congestion Metrics Cache enabled, allow remote attackers to cause a denial of service (Traffic Management Microkernel (TMM) restart) via crafted ICMP packets, related to Path MTU (PMTU) discovery.
CVE-2015-8088
Heap-based buffer overflow in the HIFI driver in Huawei Mate 7 phones with software MT7-UL00 before MT7-UL00C17B354, MT7-TL10 before MT7-TL10C00B354, MT7-TL00 before MT7-TL00C01B354, and MT7-CL00 before MT7-CL00C92B354 and P8 phones with software GRA-TL00 before GRA-TL00C01B220SP01, GRA-CL00 before GRA-CL00C92B220, GRA-CL10 before GRA-CL10C92B220, GRA-UL00 before GRA-UL00C00B220, and GRA-UL10 before GRA-UL10C00B220 allows attackers to cause a denial of service (reboot) or execute arbitrary code via a crafted application.
CVE-2015-8098
F5 BIG-IP APM 11.4.1 before 11.4.1 HF9, 11.5.x before 11.5.3, and 11.6.0 before 11.6.0 HF4 allow remote attackers to cause a denial of service or execute arbitrary code via unspecified vectors related to processing a Citrix Remote Desktop connection through a virtual server configured with a remote desktop profile, aka an "Out-of-bounds memory vulnerability."
CVE-2015-8306
Buffer overflow in the HIFI driver in Huawei P8 phones with software GRA-TL00 before GRA-TL00C01B230, GRA-CL00 before GRA-CL00C92B230, GRA-CL10 before GRA-CL10C92B230, GRA-UL00 before GRA-UL00C00B230, and GRA-UL10 before GRA-UL10C00B230 allows attackers to cause a denial of service (system crash) or execute arbitrary code via an unspecified parameter.
CVE-2015-8337
The HIFI driver in Huawei P8 phones with software GRA-TL00 before GRA-TL00C01B220SP01, GRA-CL00 before GRA-CL00C92B220, GRA-CL10 before GRA-CL10C92B220, GRA-UL00 before GRA-UL00C00B220, GRA-UL10 before GRA-UL10C00B220 and Mate7 phones with software MT7-UL00 before MT7-UL00C17B354, MT7-TL10 before MT7-TL10C00B354, MT7-TL00 before MT7-TL00C01B354, and MT7-CL00 before MT7-CL00C92B354 allows remote attackers to cause a denial of service (invalid memory access and reboot) via unspecified vectors related to "input null pointer as parameter."
<a href="https://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-8396
Integer overflow in the ImageRegionReader::ReadIntoBuffer function in MediaStorageAndFileFormat/gdcmImageRegionReader.cxx in Grassroots DICOM (aka GDCM) before 2.6.2 allows attackers to execute arbitrary code via crafted header dimensions in a DICOM image file, which triggers a buffer overflow.
CVE-2015-8397
The JPEGLSCodec::DecodeExtent function in MediaStorageAndFileFormat/gdcmJPEGLSCodec.cxx in Grassroots DICOM (aka GDCM) before 2.6.2 allows remote attackers to obtain sensitive information from process memory or cause a denial of service (application crash) via an embedded JPEG-LS image with dimensions larger than the selected region in a (1) two-dimensional or (2) three-dimensional DICOM image file, which triggers an out-of-bounds read.
CVE-2015-8400
The HTTPS fallback implementation in Shell In A Box (aka shellinabox) before 2.19 makes it easier for remote attackers to conduct DNS rebinding attacks via the "/plain" URL.
CVE-2015-8603
Cross-site scripting (XSS) vulnerability in Serendipity before 2.0.3 allows remote attackers to inject arbitrary web script or HTML via the serendipity[entry_id] parameter in an "edit" admin action to serendipity_admin.php.
Per http://blog.s9y.org/archives/266-Serendipity-2.0.3-released.html:
"The issue only affects logged-in authors, where HTML can be inserted into the comment editing form when they click specially crafted links.Due to the required authentication we consider the issue of medium impact, but suggest everyone to perform the update."
CVE-2015-8611
BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, Link Controller, and PEM 12.0.0 before HF1 on the 2000, 4000, 5000, 7000, and 10000 platforms do not properly sync passwords with the Always-On Management (AOM) subsystem, which might allow remote attackers to obtain login access to AOM via an (1) expired or (2) default password.
CVE-2015-8659
The idle stream handling in nghttp2 before 1.6.0 allows attackers to have unspecified impact via unknown vectors, aka a heap-use-after-free bug.
CVE-2015-8672
The presentation transmission permission management mechanism in Huawei TE30, TE40, TE50, and TE60 multimedia video conferencing endpoints with software before V100R001C10SPC100 allows remote attackers to cause a denial of service (wired presentation outage) via unspecified vectors involving a wireless presentation.
CVE-2015-8673
Huawei TE30, TE40, TE50, and TE60 multimedia video conferencing endpoints with software before V100R001C10SPC100 do not require entry of the old password when changing the password for the Debug account, which allows physically proximate attackers to change the password by leveraging an unattended workstation.
CVE-2015-8769
SQL injection vulnerability in Joomla! 3.x before 3.4.7 allows attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2016-1231
Directory traversal vulnerability in the HTTP file-serving module (mod_http_files) in Prosody 0.9.x before 0.9.9 allows remote attackers to read arbitrary files via a .. (dot dot) in an unspecified path.
CVE-2016-1232
The mod_dialback module in Prosody before 0.9.9 does not properly generate random values for the secret token for server-to-server dialback authentication, which makes it easier for attackers to spoof servers via a brute force attack.
<a href="https://cwe.mitre.org/data/definitions/338.html">CWE-338: Use of Cryptographically Weak Pseudo-Random Number Generator (PRNG)</a>
CVE-2016-1715
The swin.sys kernel driver in McAfee Application Control (MAC) 6.1.0 before build 706, 6.1.1 before build 404, 6.1.2 before build 449, 6.1.3 before build 441, and 6.2.0 before build 505 on 32-bit Windows platforms allows local users to cause a denial of service (memory corruption and system crash) or gain privileges via a 768 syscall, which triggers a zero to be written to an arbitrary kernel memory location.
