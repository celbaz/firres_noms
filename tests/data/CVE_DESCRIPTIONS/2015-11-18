CVE-2015-4852
The WLS Security component in Oracle WebLogic Server 10.3.6.0, 12.1.2.0, 12.1.3.0, and 12.2.1.0 allows remote attackers to execute arbitrary commands via a crafted serialized Java object in T3 protocol traffic to TCP port 7001, related to oracle_common/modules/com.bea.core.apache.commons.collections.jar. NOTE: the scope of this CVE is limited to the WebLogic Server product.
CVE-2015-5253
The SAML Web SSO module in Apache CXF before 2.7.18, 3.0.x before 3.0.7, and 3.1.x before 3.1.3 allows remote authenticated users to bypass authentication via a crafted SAML response with a valid signed assertion, related to a "wrapping attack."
CVE-2015-5255
Adobe BlazeDS, as used in ColdFusion 10 before Update 18 and 11 before Update 7 and LiveCycle Data Services 3.0.x before 3.0.0.354175, 3.1.x before 3.1.0.354180, 4.5.x before 4.5.1.354177, 4.6.2.x before 4.6.2.354178, and 4.7.x before 4.7.0.354178, allows remote attackers to send HTTP traffic to intranet servers via a crafted XML document, related to a Server-Side Request Forgery (SSRF) issue.
CVE-2015-5999
Multiple cross-site request forgery (CSRF) vulnerabilities in the D-Link DIR-816L Wireless Router with firmware before 2.06.B09_BETA allow remote attackers to hijack the authentication of administrators for requests that (1) change the admin password, (2) change the network policy, or (3) possibly have other unspecified impact via crafted requests to hedwig.cgi and pigwidgeon.cgi.
CVE-2015-6330
Cross-site request forgery (CSRF) vulnerability in Cisco Prime Collaboration Assurance 10.5(1) and 10.6 allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCus62712.
CVE-2015-6357
The rule-update feature in Cisco FireSIGHT Management Center (MC) 5.2 through 5.4.0.1 does not verify the X.509 certificate of the support.sourcefire.com SSL server, which allows man-in-the-middle attackers to spoof this server and provide an invalid package, and consequently execute arbitrary code, via a crafted certificate, aka Bug ID CSCuw06444.
CVE-2015-6372
Cross-site scripting (XSS) vulnerability in the web-based management interface in Cisco Firepower Extensible Operating System 1.1(1.160) on Firepower 9000 devices allows remote attackers to inject arbitrary web script or HTML via a crafted value, aka Bug ID CSCux10614.
CVE-2015-6373
Cross-site request forgery (CSRF) vulnerability in Cisco Firepower Extensible Operating System 1.1(1.160) on Firepower 9000 devices allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCux10611.
CVE-2015-6847
The default configuration of EMC VPLEX GeoSynchrony 5.4 SP1 before P3 stores cleartext NAVISPHERE GUI passwords in a log file, which allows local users to obtain sensitive information by reading this file.
CVE-2015-7941
libxml2 2.9.2 does not properly stop parsing invalid input, which allows context-dependent attackers to cause a denial of service (out-of-bounds read and libxml2 crash) via crafted XML data to the (1) xmlParseEntityDecl or (2) xmlParseConditionalSections function in parser.c, as demonstrated by non-terminated entities.
"context dependent" seems to point to MiTM attack due to: If a user or automated system were tricked into opening a specially
crafted document, an attacker could possibly cause libxml2 to crash,
resulting in a denial of service. This issue only affected
Ubuntu 12.04 LTS, Ubuntu 14.04 LTS and Ubuntu 15.04.
CVE-2015-7942
The xmlParseConditionalSections function in parser.c in libxml2 does not properly skip intermediary entities when it stops parsing invalid input, which allows context-dependent attackers to cause a denial of service (out-of-bounds read and crash) via crafted XML data, a different vulnerability than CVE-2015-7941.
CVE-2015-8023
The server implementation of the EAP-MSCHAPv2 protocol in the eap-mschapv2 plugin in strongSwan 4.2.12 through 5.x before 5.3.4 does not properly validate local state, which allows remote attackers to bypass authentication via an empty Success message in response to an initial Challenge message.
CVE-2015-8035
The xz_decomp function in xzlib.c in libxml2 2.9.1 does not properly detect compression errors, which allows context-dependent attackers to cause a denial of service (process hang) via crafted XML data.
CVE-2015-8051
The Adobe Premiere Clip app before 1.2.1 for iOS mishandles unspecified input, which has unknown impact and attack vectors.
CVE-2015-8052
Cross-site scripting (XSS) vulnerability in Adobe ColdFusion 10 before Update 18 and 11 before Update 7 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2015-8053.
CVE-2015-8053
Cross-site scripting (XSS) vulnerability in Adobe ColdFusion 10 before Update 18 and 11 before Update 7 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2015-8052.
CVE-2015-8090
The Web Server component in TIBCO LogLogic Unity before 1.1.1 allows remote authenticated users to gain privileges, and consequently obtain sensitive information, via an HTTP request.
