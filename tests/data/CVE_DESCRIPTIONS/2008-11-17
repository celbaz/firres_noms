CVE-2007-0072
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to a read operation over RPC.
CVE-2007-0073
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to a file read operation over RPC.
CVE-2007-0074
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to a folder read operation over RPC.
CVE-2008-0012
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to the product's configuration, a different vulnerability than CVE-2008-0013 and CVE-2008-0014.
CVE-2008-0013
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to the product's configuration, a different vulnerability than CVE-2008-0012 and CVE-2008-0014.
CVE-2008-0014
Heap-based buffer overflow in an unspecified procedure in Trend Micro ServerProtect 5.7 and 5.58 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to the product's configuration, a different vulnerability than CVE-2008-0012 and CVE-2008-0013.
CVE-2008-3623
Heap-based buffer overflow in CoreGraphics in Apple Safari before 3.2 on Windows, in iPhone OS 1.0 through 2.2.1, and in iPhone OS for iPod touch 1.1 through 2.2.1 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted image, related to improper handling of color spaces.
CVE-2008-3644
Apple Safari before 3.2 does not properly prevent caching of form data for form fields that have autocomplete disabled, which allows local users to obtain sensitive information by reading the browser's page cache.
CVE-2008-4216
The plug-in interface in WebKit in Apple Safari before 3.2 does not prevent plug-ins from accessing local URLs, which allows remote attackers to obtain sensitive information via vectors that "launch local files."
CVE-2008-4415
Unspecified vulnerability in HP Service Manager (HPSM) before 7.01.71 allows remote authenticated users to execute arbitrary code via unknown vectors.
CVE-2008-4824
Multiple unspecified vulnerabilities in Adobe Flash Player 10.x before 10.0.12.36 and 9.x before 9.0.151.0 allow remote attackers to execute arbitrary code via unknown vectors related to "input validation errors."
CVE-2008-4832
rc.sysinit in initscripts 8.12-8.21 and 8.56.15-0.1 on rPath allows local users to delete arbitrary files via a symlink attack on a directory under (1) /var/lock or (2) /var/run.  NOTE: this issue exists because of a race condition in an incorrect fix for CVE-2008-3524. NOTE: exploitation may require an unusual scenario in which rc.sysinit is executed other than at boot time.
CVE-2008-5025
Stack-based buffer overflow in the hfs_cat_find_brec function in fs/hfs/catalog.c in the Linux kernel before 2.6.28-rc1 allows attackers to cause a denial of service (memory corruption or system crash) via an hfs filesystem image with an invalid catalog namelength field, a related issue to CVE-2008-4933.
CVE-2008-5098
Cross-site scripting (XSS) vulnerability in Sun Java System Messaging Server 6.2 and 6.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2007-2904.
http://sunsolve.sun.com/search/document.do?assetkey=1-26-242186-1

5. Resolution

This issue is addressed in the following releases:

SPARC Platform

    * Sun Java System Messaging Server 6.2 and 6.3 (for Solaris 9 and Solaris 10) with patch 120228-29 or later
    * Sun Java System Messaging Server 6.3 (64-bit Solaris) with patch 126479-10 or later

x86 Platform

    * Sun Java System Messaging Server 6.2 and 6.3 (for Solaris 9 and Solaris 10) with patch 120229-29 or later
    * Sun Java System Messaging Server 6.3 (64-bit) with patch 126480-10 or later

Linux Platform

    * Sun Java System Messaging Server 6.2 and 6.3 (for RHEL 3 and RHEL 4) with patch 120230-29 or later
CVE-2008-5099
Sun Logical Domain Manager (aka LDoms Manager or ldm) 1.0 through 1.0.3 displays the value of the OpenBoot PROM (OBP) security-password variable in cleartext, which allows local users to bypass the SPARC firmware's password protection, and gain privileges or obtain data access, via the "ldm ls -l" command, a different vulnerability than CVE-2008-4992.
http://sunsolve.sun.com/search/document.do?assetkey=1-26-243606-1

Note: The x86 Platform is not affected by this issue, as Logical Domains (LDoms) software is supported only on the Sun SPARC Platform.
CVE-2008-5100
The strong name (SN) implementation in Microsoft .NET Framework 2.0.50727 relies on the digital signature Public Key Token embedded in the pathname of a DLL file instead of the digital signature of this file itself, which makes it easier for attackers to bypass Global Assembly Cache (GAC) and Code Access Security (CAS) protection mechanisms, aka MSRC ticket MSRC8566gs.
CVE-2008-5101
Buffer overflow in the BMP reader in OptiPNG 0.6 and 0.6.1 allows user-assisted attackers to execute arbitrary code via a crafted BMP image, related to an "array overflow."
CVE-2008-5102
PythonScripts in Zope 2 2.11.2 and earlier, as used in Conga and other products, allows remote authenticated users to cause a denial of service (resource consumption or application halt) via certain (1) raise or (2) import statements.
http://www.zope.org/Products/Zope/Hotfix-2008-08-12/README.txt

Affected Versions
* Zope 2.7.0 to Zope 2.11.2

---

http://openwall.com/lists/oss-security/2008/11/12/2

Affected Conga versions: - checked conga-0.9.1-8 (contains Zope2.7.5 RC2), conga-0.12.0-7.el5 (contains Zope-2.8.4),
                               - but older,newer Conga versions can be also vulnerable to this issue (based on Zope 2 version).
CVE-2008-5103
The (1) python-vm-builder and (2) ubuntu-vm-builder implementations in VMBuilder 0.9 in Ubuntu 8.10 omit the -e option when invoking chpasswd with a root:! argument, which configures the root account with a cleartext password of ! (exclamation point) and allows attackers to bypass intended login restrictions.
CVE-2008-5104
Ubuntu 6.06 LTS, 7.10, 8.04 LTS, and 8.10, when installed as a virtual machine by (1) python-vm-builder or (2) ubuntu-vm-builder in VMBuilder 0.9 in Ubuntu 8.10, have ! (exclamation point) as the default root password, which allows attackers to bypass intended login restrictions.
CVE-2008-5105
KarjaSoft Sami FTP Server 2.0.x allows remote attackers to cause a denial of service (daemon crash or hang) via certain (1) APPE, (2) CWD, (3) DELE, (4) MKD, (5) RMD, (6) RETR, (7) RNFR, (8) RNTO, (9) SIZE, and (10) STOR commands.
CVE-2008-5106
Buffer overflow in KarjaSoft Sami FTP Server 2.0.x allows remote attackers to cause a denial of service (daemon crash) and possibly execute arbitrary code via a long argument to an arbitrary command, which triggers the overflow when the SamyFtp.binlog log file is viewed in the management console.  NOTE: this may overlap CVE-2006-0441 and CVE-2006-2212.
CVE-2008-5107
The installation process for Citrix Presentation Server 4.5 and Desktop Server 1.0, when MSI logging is enabled, stores database credentials in MSI log files, which allows local users to obtain these credentials by reading the log files.
CVE-2008-5108
Unspecified vulnerability in Adobe AIR 1.1 and earlier allows context-dependent attackers to execute untrusted JavaScript in an AIR application via unknown attack vectors.
CVE-2008-5110
syslog-ng does not call chdir when it calls chroot, which might allow attackers to escape the intended jail.  NOTE: this is only a vulnerability when a separate vulnerability is present.
CVE-2008-5111
Unspecified vulnerability in the socket function in Sun Solaris 10 and OpenSolaris snv_57 through snv_91, when InfiniBand hardware is not installed, allows local users to cause a denial of service (panic) via unknown vectors, related to the socksdpv_close function.
CVE-2008-5112
The LDAP server in Active Directory in Microsoft Windows 2000 SP4 and Server 2003 SP1 and SP2 responds differently to a failed bind attempt depending on whether the user account exists and is permitted to login, which allows remote attackers to enumerate valid usernames via a series of LDAP bind requests, as demonstrated by ldapuserenum.
CVE-2008-5113
WordPress 2.6.3 relies on the REQUEST superglobal array in certain dangerous situations, which makes it easier for remote attackers to conduct delayed and persistent cross-site request forgery (CSRF) attacks via crafted cookies, as demonstrated by attacks that (1) delete user accounts or (2) cause a denial of service (loss of application access).  NOTE: this issue relies on the presence of an independent vulnerability that allows cookie injection.
