CVE-2015-5224
The mkostemp function in login-utils in util-linux when used incorrectly allows remote attackers to cause file name collision and possibly other attacks.
CVE-2017-11159
Multiple untrusted search path vulnerabilities in installer in Synology Photo Station Uploader before 1.4.2-084 on Windows allows local attackers to execute arbitrary code and conduct DLL hijacking attack via a Trojan horse (1) shfolder.dll, (2) ntmarta.dll, (3) secur32.dll or (4) dwmapi.dll file in the current working directory.
CVE-2017-11317
Telerik.Web.UI in Progress Telerik UI for ASP.NET AJAX before R1 2017 and R2 before R2 2017 SP2 uses weak RadAsyncUpload encryption, which allows remote attackers to perform arbitrary file uploads or execute arbitrary code.
CVE-2017-11357
Progress Telerik UI for ASP.NET AJAX before R2 2017 SP2 does not properly restrict user input to RadAsyncUpload, which allows remote attackers to perform arbitrary file uploads or execute arbitrary code.
CVE-2017-11610
The XML-RPC server in supervisor before 3.0.1, 3.1.x before 3.1.4, 3.2.x before 3.2.4, and 3.3.x before 3.3.3 allows remote authenticated users to execute arbitrary commands via a crafted XML-RPC request, related to nested supervisord namespace lookups.
CVE-2017-12791
Directory traversal vulnerability in minion id validation in SaltStack Salt before 2016.11.7 and 2017.7.x before 2017.7.1 allows remote minions with incorrect credentials to authenticate to a master via a crafted minion ID.
CVE-2017-12809
QEMU (aka Quick Emulator), when built with the IDE disk and CD/DVD-ROM Emulator support, allows local guest OS privileged users to cause a denial of service (NULL pointer dereference and QEMU process crash) by flushing an empty CDROM device drive.
CVE-2017-12844
Cross-site scripting (XSS) vulnerability in the admin panel in IceWarp Mail Server 10.4.4 allows remote authenticated domain administrators to inject arbitrary web script or HTML via a crafted user name.
CVE-2017-12847
Nagios Core before 4.3.3 creates a nagios.lock PID file after dropping privileges to a non-root account, which might allow local users to kill arbitrary processes by leveraging access to this non-root account for nagios.lock modification before a root script executes a "kill `cat /pathname/nagios.lock`" command.
CVE-2017-12858
Double free vulnerability in the _zip_dirent_read function in zip_dirent.c in libzip allows attackers to have unspecified impact via unknown vectors.
CVE-2017-12904
Improper Neutralization of Special Elements used in an OS Command in bookmarking function of Newsbeuter versions 0.7 through 2.9 allows remote attackers to perform user-assisted code execution by crafting an RSS item that includes shell code in its title and/or URL.
CVE-2017-12965
Session fixation vulnerability in Apache2Triad 1.5.4 allows remote attackers to hijack web sessions via the PHPSESSID parameter.
CVE-2017-12970
Cross-site request forgery (CSRF) vulnerability in Apache2Triad 1.5.4 allows remote attackers to hijack the authentication of authenticated users for requests that (1) add or (2) delete user accounts via a request to phpsftpd/users.php.
CVE-2017-12971
Cross-site scripting (XSS) vulnerability in Apache2Triad 1.5.4 allows remote attackers to inject arbitrary web script or HTML via the account parameter to phpsftpd/users.php.
CVE-2017-13130
mcmnm in BMC Patrol allows local users to gain privileges via a crafted libmcmclnx.so file in the current working directory, because it is setuid root and the RPATH variable begins with the .: substring.
CVE-2017-13131
In ImageMagick 7.0.6-8, a memory leak vulnerability was found in the function ReadMIFFImage in coders/miff.c, which allows attackers to cause a denial of service (memory consumption in NewLinkedList in MagickCore/linked-list.c) via a crafted file.
CVE-2017-13132
In ImageMagick 7.0.6-8, the WritePDFImage function in coders/pdf.c operates on an incorrect data structure in the "dump uncompressed PseudoColor packets" step, which allows attackers to cause a denial of service (assertion failure in WriteBlobStream in MagickCore/blob.c) via a crafted file.
CVE-2017-13133
In ImageMagick 7.0.6-8, the load_level function in coders/xcf.c lacks offset validation, which allows attackers to cause a denial of service (load_tile memory exhaustion) via a crafted file.
CVE-2017-13134
In ImageMagick 7.0.6-6 and GraphicsMagick 1.3.26, a heap-based buffer over-read was found in the function SFWScan in coders/sfw.c, which allows attackers to cause a denial of service via a crafted file.
CVE-2017-13137
The FormCraft Basic plugin 1.0.5 for WordPress has SQL injection in the id parameter to form.php.
CVE-2017-13138
DOM based Cross-site scripting (XSS) vulnerability in the Bridge theme before 11.2 for WordPress allows remote attackers to inject arbitrary JavaScript.
CVE-2017-13139
In ImageMagick before 6.9.9-0 and 7.x before 7.0.6-1, the ReadOneMNGImage function in coders/png.c has an out-of-bounds read with the MNG CLIP chunk.
CVE-2017-13140
In ImageMagick before 6.9.9-1 and 7.x before 7.0.6-2, the ReadOnePNGImage function in coders/png.c allows remote attackers to cause a denial of service (application hang in LockSemaphoreInfo) via a PNG file with a width equal to MAGICK_WIDTH_LIMIT.
CVE-2017-13141
In ImageMagick before 6.9.9-4 and 7.x before 7.0.6-4, a crafted file could trigger a memory leak in ReadOnePNGImage in coders/png.c.
CVE-2017-13142
In ImageMagick before 6.9.9-0 and 7.x before 7.0.6-1, a crafted PNG file could trigger a crash because there was an insufficient check for short files.
CVE-2017-13143
In ImageMagick before 6.9.7-6 and 7.x before 7.0.4-6, the ReadMATImage function in coders/mat.c uses uninitialized data, which might allow remote attackers to obtain sensitive information from process memory.
CVE-2017-13144
In ImageMagick before 6.9.7-10, there is a crash (rather than a "width or height exceeds limit" error report) if the image dimensions are too large, as demonstrated by use of the mpc coder.
CVE-2017-13145
In ImageMagick before 6.9.8-8 and 7.x before 7.0.5-9, the ReadJP2Image function in coders/jp2.c does not properly validate the channel geometry, leading to a crash.
CVE-2017-13146
In ImageMagick before 6.9.8-5 and 7.x before 7.0.5-6, there is a memory leak in the ReadMATImage function in coders/mat.c.
CVE-2017-13147
In GraphicsMagick 1.3.26, an allocation failure vulnerability was found in the function ReadMNGImage in coders/png.c when a small MNG file has a MEND chunk with a large length value.
CVE-2017-13648
In GraphicsMagick 1.3.26, a memory leak vulnerability was found in the function ReadMATImage in coders/mat.c.
CVE-2017-13649
UnrealIRCd 4.0.13 and earlier creates a PID file after dropping privileges to a non-root account, which might allow local users to kill arbitrary processes by leveraging access to this non-root account for PID file modification before a root script executes a "kill `cat /pathname`" command. NOTE: the vendor indicates that there is no common or recommended scenario in which a root script would execute this kill command.
CVE-2017-9506
The IconUriServlet of the Atlassian OAuth Plugin from version 1.3.0 before version 1.9.12 and from version 2.0.0 before version 2.0.4 allows remote attackers to access the content of internal network resources and/or perform an XSS attack via Server Side Request Forgery (SSRF).
