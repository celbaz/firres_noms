CVE-2009-0958
Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 stores an exception for a hostname when the user accepts an untrusted Exchange server certificate, which causes it to be accepted without prompting in future usage and allows remote Exchange servers to obtain sensitive information such as credentials.
CVE-2009-0959
The MPEG-4 video codec in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 allows remote attackers to cause a denial of service (device reset) via a crafted MPEG-4 video file that triggers an "input validation issue."
CVE-2009-0960
The Mail component in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 does not provide an option to disable remote image loading in HTML email, which allows remote attackers to determine the device address and when an e-mail is read via an HTML email containing an image URL.
CVE-2009-0961
The Mail component in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 dismisses the call approval dialog when another alert appears, which might allow remote attackers to force the iPhone to place a call without user approval by causing an application to trigger an alert.
CVE-2009-1679
The Profiles component in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1, when installing a configuration profile, can replace the password policy from Exchange ActiveSync with a weaker password policy, which allows physically proximate attackers to bypass the intended policy.
CVE-2009-1680
Safari in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 does not properly clear the search history when it is cleared from the Settings application, which allows physically proximate attackers to obtain the search history.
CVE-2009-1683
The Telephony component in Apple iPhone OS 1.0 through 2.2.1 and iPhone OS for iPod touch 1.1 through 2.2.1 allows remote attackers to cause a denial of service (device reset) via a crafted ICMP echo request, which triggers an assertion error related to a "logic issue."
CVE-2009-1692
WebKit before r41741, as used in Apple iPhone OS 1.0 through 2.2.1, iPhone OS for iPod touch 1.1 through 2.2.1, Safari, and other software, allows remote attackers to cause a denial of service (memory consumption or device reset) via a web page containing an HTMLSelectElement object with a large length attribute, related to the length property of a Select object.
CVE-2009-2122
SQL injection vulnerability in viewimg.php in the Paolo Palmonari Photoracer plugin 1.0 for WordPress allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-2123
Multiple SQL injection vulnerabilities in Elvin 1.2.0 allow remote attackers to execute arbitrary SQL commands via the (1) inUser (aka Username) and (2) inPass (aka Password) parameters to (a) inc/login.ei, reachable through login.php; and the (3) id parameter to (b) show_bug.php and (c) show_activity.php.  NOTE: it was later reported that vector 3c also affects 1.2.2.
CVE-2009-2124
Directory traversal vulnerability in page.php in Elvin 1.2.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the id parameter.
CVE-2009-2125
delete_bug.php in Elvin before 1.2.1 does not require administrative privileges, which allows remote authenticated users to bypass intended access restrictions and delete arbitrary bugs.
CVE-2009-2126
Cross-site scripting (XSS) vulnerability in close_bug.php in Elvin before 1.2.1 allows remote attackers to inject arbitrary web script or HTML via the title (aka subject) field.
CVE-2009-2127
Cross-site scripting (XSS) vulnerability in show_activity.php in Elvin 1.2.0 allows remote attackers to inject arbitrary web script or HTML via the id parameter.
CVE-2009-2128
SQL injection vulnerability in close_bug.php in Elvin before 1.2.1 allows remote attackers to execute arbitrary SQL commands via the title (aka subject) field.
CVE-2009-2129
Cross-site request forgery (CSRF) vulnerability in login.php in Elvin 1.2.0 allows remote attackers to hijack the authentication of arbitrary users via a logout action.
CVE-2009-2130
Elvin 1.2.0 allows remote attackers to read the PHP source code of (1) login.ei, (2) jump_bug.ei, or (3) create_account.ei in inc/ via a direct request.
CVE-2009-2131
Cross-site scripting (XSS) vulnerability in 4images 1.7.7 and earlier allows remote authenticated users to inject arbitrary web script or HTML by providing a crafted user_homepage parameter to member.php, and then posting a comment associated with a picture.
CVE-2009-2132
Directory traversal vulnerability in global.php in 4images before 1.7.7, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the l parameter.
CVE-2009-2133
Multiple cross-site scripting (XSS) vulnerabilities in Pivot 1.40.4 and 1.40.7 allow remote attackers to inject arbitrary web script or HTML via the (1) menu or (2) sort parameter to pivot/index.php, (3) the value of a check array parameter in a delete action to pivot/index.php, (4) the element name in a check array parameter in a delete action to pivot/index.php, (5) the edituser parameter in an edituser action to pivot/index.php, (6) the edit parameter in a templates action to pivot/index.php, (7) the blog parameter in a blog_edit1 action to pivot/index.php, (8) the cat parameter in a cat_edit action to pivot/index.php, (9) a certain form field in a doaction=1 request to pivot/index.php, (10) the url field in a my_weblog edit_prefs action to pivot/user.php, or (11) the username (aka name) field in a my_weblog reg_user action to pivot/user.php.
CVE-2009-2134
pivot/tb.php in Pivot 1.40.4 and 1.40.7 allows remote attackers to obtain sensitive information via an invalid url parameter, which reveals the installation path in an error message.
CVE-2009-2135
Multiple race conditions in the Solaris Event Port API in Sun Solaris 10 and OpenSolaris before snv_107 allow local users to cause a denial of service (panic) via unspecified vectors related to a race between the port_dissociate and close functions.
CVE-2009-2136
Unspecified vulnerability in the TCP/IP networking stack in Sun Solaris 10, and OpenSolaris snv_01 through snv_82 and snv_111 through snv_117, when a Cassini GigaSwift Ethernet Adapter (aka CE) interface is used, allows remote attackers to cause a denial of service (panic) via vectors involving jumbo frames.
Per http://sunsolve.sun.com/search/document.do?assetkey=1-66-257008-1

"Note 2: A system is only vulnerable to this issue if it is using a GigaSwift Ethernet Adapter (CE) interface (ce(7D)) which has been configured to accept jumbo frames, and hardware checksumming is enabled."
CVE-2009-2137
Memory leak in the Ultra-SPARC T2 crypto provider device driver (aka n2cp) in Sun Solaris 10, and OpenSolaris snv_54 through snv_112, allows context-dependent attackers to cause a denial of service (memory consumption) via unspecified vectors related to a large keylen value.
CVE-2009-2138
Multiple open redirect vulnerabilities in TBDev.NET 01-01-08 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via (1) the returnto parameter to login.php or (2) the returnto parameter in a delete action to news.php.  NOTE: this can be leveraged for cross-site scripting (XSS) by redirecting to a data: URI.
