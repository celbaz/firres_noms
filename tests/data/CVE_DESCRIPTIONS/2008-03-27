CVE-2008-0537
Unspecified vulnerability in the Supervisor Engine 32 (Sup32), Supervisor Engine 720 (Sup720), and Route Switch Processor 720 (RSP720) for multiple Cisco products, when using Multi Protocol Label Switching (MPLS) VPN and OSPF sham-link, allows remote attackers to cause a denial of service (blocked queue, device restart, or memory leak) via unknown vectors.
CVE-2008-1150
The virtual private dial-up network (VPDN) component in Cisco IOS before 12.3 allows remote attackers to cause a denial of service (resource exhaustion) via a series of PPTP sessions, related to the persistence of interface descriptor block (IDB) data structures after process termination, aka bug ID CSCdv59309.
CVE-2008-1151
Memory leak in the virtual private dial-up network (VPDN) component in Cisco IOS before 12.3 allows remote attackers to cause a denial of service (memory consumption) via a series of PPTP sessions, related to "dead memory" that remains allocated after process termination, aka bug ID CSCsj58566.
CVE-2008-1152
The data-link switching (DLSw) component in Cisco IOS 12.0 through 12.4 allows remote attackers to cause a denial of service (device restart or memory consumption) via crafted (1) UDP port 2067 or (2) IP protocol 91 packets.
CVE-2008-1153
Cisco IOS 12.1, 12.2, 12.3, and 12.4, with IPv4 UDP services and the IPv6 protocol enabled, allows remote attackers to cause a denial of service (device crash and possible blocked interface) via a crafted IPv6 packet to the device.
CVE-2008-1156
Unspecified vulnerability in the Multicast Virtual Private Network (MVPN) implementation in Cisco IOS 12.0, 12.2, 12.3, and 12.4 allows remote attackers to create "extra multicast states on the core routers" via a crafted Multicast Distribution Tree (MDT) Data Join message.
CVE-2008-1233
Unspecified vulnerability in Mozilla Firefox before 2.0.0.13, Thunderbird before 2.0.0.13, and SeaMonkey before 1.1.9 allows remote attackers to execute arbitrary code via "XPCNativeWrapper pollution."
CVE-2008-1234
Cross-site scripting (XSS) vulnerability in Mozilla Firefox before 2.0.0.13, Thunderbird before 2.0.0.13, and SeaMonkey before 1.1.9 allows remote attackers to inject arbitrary web script or HTML via event handlers, aka "Universal XSS using event handlers."
CVE-2008-1235
Unspecified vulnerability in Mozilla Firefox before 2.0.0.13, Thunderbird before 2.0.0.13, and SeaMonkey before 1.1.9 allows remote attackers to execute arbitrary code via unknown vectors that cause JavaScript to execute with the wrong principal, aka "Privilege escalation via incorrect principals."
CVE-2008-1236
Multiple unspecified vulnerabilities in Mozilla Firefox before 2.0.0.13, Thunderbird before 2.0.0.13, and SeaMonkey before 1.1.9 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unknown vectors related to the layout engine.
CVE-2008-1237
Multiple unspecified vulnerabilities in Mozilla Firefox before 2.0.0.13, Thunderbird before 2.0.0.13, and SeaMonkey before 1.1.9 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unknown vectors related to the JavaScript engine.
CVE-2008-1238
Mozilla Firefox before 2.0.0.13 and SeaMonkey before 1.1.9, when generating the HTTP Referer header, does not list the entire URL when it contains Basic Authentication credentials without a username, which makes it easier for remote attackers to bypass application protection mechanisms that rely on Referer headers, such as with some Cross-Site Request Forgery (CSRF) mechanisms.
CVE-2008-1241
GUI overlay vulnerability in Mozilla Firefox before 2.0.0.13 and SeaMonkey before 1.1.9 allows remote attackers to spoof form elements and redirect user inputs via a borderless XUL pop-up window from a background tab.
CVE-2008-1384
Integer overflow in PHP 5.2.5 and earlier allows context-dependent attackers to cause a denial of service and possibly have unspecified other impact via a printf format parameter with a large width specifier, related to the php_sprintf_appendstring function in formatted_print.c and probably other functions for formatted strings (aka *printf functions).
CVE-2008-1391
Multiple integer overflows in libc in NetBSD 4.x, FreeBSD 6.x and 7.x, and probably other BSD and Apple Mac OS platforms allow context-dependent attackers to execute arbitrary code via large values of certain integer fields in the format argument to (1) the strfmon function in lib/libc/stdlib/strfmon.c, related to the GET_NUMBER macro; and (2) the printf function, related to left_prec and right_prec.
CVE-2008-1530
GnuPG (gpg) 1.4.8 and 2.0.8 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via crafted duplicate keys that are imported from key servers, which triggers "memory corruption around deduplication of user IDs."
CVE-2008-1531
The connection_state_machine function (connections.c) in lighttpd 1.4.19 and earlier, and 1.5.x before 1.5.0, allows remote attackers to cause a denial of service (active SSL connection loss) by triggering an SSL error, such as disconnecting before a download has finished, which causes all active SSL connections to be lost.
