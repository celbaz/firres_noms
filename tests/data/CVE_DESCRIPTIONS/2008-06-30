CVE-2008-0598
Unspecified vulnerability in the 32-bit and 64-bit emulation in the Linux kernel 2.6.9, 2.6.18, and probably other versions allows local users to read uninitialized memory via unknown vectors involving a crafted binary.
CVE-2008-2365
Race condition in the ptrace and utrace support in the Linux kernel 2.6.9 through 2.6.25, as used in Red Hat Enterprise Linux (RHEL) 4, allows local users to cause a denial of service (oops) via a long series of PTRACE_ATTACH ptrace calls to another user's process that trigger a conflict between utrace_detach and report_quiescent, related to "late ptrace_may_attach() check" and "race around &dead_engine_ops setting," a different vulnerability than CVE-2007-0771 and CVE-2008-1514.  NOTE: this issue might only affect kernel versions before 2.6.16.x.
CVE-2008-2462
Cross-site scripting (XSS) vulnerability in the viewfile documentation command in Caucho Resin before 3.0.25, and 3.1.x before 3.1.4, allows remote attackers to inject arbitrary web script or HTML via the file parameter.
CVE-2008-2729
arch/x86_64/lib/copy_user.S in the Linux kernel before 2.6.19 on some AMD64 systems does not erase destination memory locations after an exception during kernel memory copy, which allows local users to obtain sensitive information.
CVE-2008-2901
Multiple SQL injection vulnerabilities in Haudenschilt Family Connections CMS (FCMS) 1.4 allow remote authenticated users to execute arbitrary SQL commands via the (1) address parameter to addressbook.php, the (2) getnews parameter to familynews.php, and the (3) poll_id parameter to home.php in a results action.
CVE-2008-2902
SQL injection vulnerability in profile.php in AlstraSoft AskMe Pro 2.1 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.  NOTE: The que_id parameter to forum_answer.php is already covered by CVE-2007-4085.
CVE-2008-2903
SQL injection vulnerability in news.php in Advanced Webhost Billing System (AWBS) 2.3.3 through 2.7.1, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the viewnews parameter.
CVE-2008-2904
SQL injection vulnerability in shop.php in Conkurent PHPMyCart allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2008-2905
PHP remote file inclusion vulnerability in includes/Cache/Lite/Output.php in the Cache_Lite package in Mambo 4.6.4 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.
CVE-2008-2906
SQL injection vulnerability in lista_anexos.php in WebChamado 1.1 allows remote attackers to execute arbitrary SQL commands via the tsk_id parameter.
CVE-2008-2907
SQL injection vulnerability in admin/index.php in WebChamado 1.1, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the eml parameter.
CVE-2008-2908
Multiple stack-based buffer overflows in a certain ActiveX control in ienipp.ocx in Novell iPrint Client for Windows before 4.36 allow remote attackers to execute arbitrary code via a long value of the (1) operation, (2) printer-url, or (3) target-frame parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-2909
SQL injection vulnerability in results.php in Clever Copy 3.0 allows remote attackers to execute arbitrary SQL commands via the searchtype parameter.
CVE-2008-2910
Buffer overflow in the DXTTextOutEffect ActiveX control (aka the Text-Effect DXT Filter), as distributed in TextOut.dll 6.0.18.1 and mvtextout.dll, in muvee autoProducer 6.0 and 6.1 allows remote attackers to execute arbitrary code via a long FontSetting property value.
CVE-2008-2911
Multiple cross-site scripting (XSS) vulnerabilities in index.php in Contenido 4.8.4 allow remote attackers to inject arbitrary web script or HTML via the (1) contenido, (2) Belang, and (3) username parameters.
CVE-2008-2912
Multiple PHP remote file inclusion vulnerabilities in Contenido CMS 4.8.4 allow remote attackers to execute arbitrary PHP code via a URL in the (1) contenido_path parameter to (a) contenido/backend_search.php; the (2) cfg[path][contenido] parameter to (b) move_articles.php, (c) move_old_stats.php, (d) optimize_database.php, (e) run_newsletter_job.php, (f) send_reminder.php, (g) session_cleanup.php, and (h) setfrontenduserstate.php in contenido/cronjobs/, and (i) includes/include.newsletter_jobs_subnav.php and (j) plugins/content_allocation/includes/include.right_top.php in contenido/; the (3) cfg[path][templates] parameter to (k) includes/include.newsletter_jobs_subnav.php and (l) plugins/content_allocation/includes/include.right_top.php in contenido/; and the (4) cfg[templates][right_top_blank] parameter to (m) plugins/content_allocation/includes/include.right_top.php and (n) contenido/includes/include.newsletter_jobs_subnav.php in contenido/, different vectors than CVE-2006-5380.
CVE-2008-2913
Directory traversal vulnerability in func.php in Devalcms 1.4a, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the currentpath parameter, in conjunction with certain ... (triple dot) and ..... sequences in the currentfile parameter, to index.php.
CVE-2008-2914
SQL injection vulnerability in jobseekers/JobSearch3.php (aka the search module) in PHP JOBWEBSITE PRO allows remote attackers to execute arbitrary SQL commands via the (1) kw or (2) position parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-2915
Multiple SQL injection vulnerabilities in jobseekers/JobSearch.php (aka the search module) in Pre Job Board allow remote attackers to execute arbitrary SQL commands via the (1) position or (2) kw parameter.
CVE-2008-2916
Multiple SQL injection vulnerabilities in Pre ADS Portal 2.0 and earlier, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) cid parameter to showcategory.php and the (2) id parameter to software-description.php.
CVE-2008-2917
SQL injection vulnerability in productsofcat.asp in E-SMART CART allows remote attackers to execute arbitrary SQL commands via the category_id parameter.
CVE-2008-2918
SQL injection vulnerability in details.php in Application Dynamics Cartweaver 3.0 allows remote attackers to execute arbitrary SQL commands via the prodId parameter, possibly a related issue to CVE-2006-2046.3.
CVE-2008-2919
SQL injection vulnerability in listing.php in Gryphon gllcTS2 4.2.4 allows remote attackers to execute arbitrary SQL commands via the sort parameter.
CVE-2008-2920
admin/filemanager/ (aka the File Manager) in EZTechhelp EZCMS 1.2 and earlier does not require authentication, which allows remote attackers to create, modify, read, and delete files.
CVE-2008-2921
SQL injection vulnerability in index.php in EZTechhelp EZCMS 1.2 and earlier allows remote attackers to execute arbitrary SQL commands via the page parameter.
CVE-2008-2922
Stack-based buffer overflow in artegic Dana IRC client 1.3 and earlier allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long IRC message.
CVE-2008-2923
Cross-site scripting (XSS) vulnerability in read/search/results in Lyris ListManager 8.8, 8.95, and 9.3d allows remote attackers to inject arbitrary web script or HTML via the words parameter.
CVE-2008-2924
Cross-site scripting (XSS) vulnerability in Webmatic before 2.8 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-2925
SQL injection vulnerability in Webmatic before 2.8 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2008-2942
Directory traversal vulnerability in patch.py in Mercurial 1.0.1 allows user-assisted attackers to modify arbitrary files via ".." (dot dot) sequences in a patch file.
CVE-2008-2943
Double free vulnerability in IBM Tivoli Directory Server (TDS) 6.1.0.0 through 6.1.0.15 allows remote authenticated administrators to cause a denial of service (ABEND) and possibly execute arbitrary code by using ldapadd to attempt to create a duplicate ibm-globalAdminGroup LDAP database entry.  NOTE: the vendor states "There is no real risk of a vulnerability," although there are likely scenarios in which a user is allowed to make administrative LDAP requests but does not have the privileges to stop the server.
CVE-2008-2944
Double free vulnerability in the utrace support in the Linux kernel, probably 2.6.18, in Red Hat Enterprise Linux (RHEL) 5 and Fedora Core 6 (FC6) allows local users to cause a denial of service (oops), as demonstrated by a crash when running the GNU GDB testsuite, a different vulnerability than CVE-2008-2365.
CVE-2008-2945
Sun Java System Access Manager 6.3 through 7.1 and Sun Java System Identity Server 6.1 and 6.2 do not properly process XSLT stylesheets in XSLT transforms in XML signatures, which allows context-dependent attackers to execute arbitrary code via a crafted stylesheet, a related issue to CVE-2007-3715, CVE-2007-3716, and CVE-2007-4289.
CVE-2008-2946
The SNMP-DMI mapper subagent daemon (aka snmpXdmid) in Solstice Enterprise Agents in Sun Solaris 8 through 10 allows remote attackers to cause a denial of service (daemon crash) via malformed packets.
CVE-2008-2947
Cross-domain vulnerability in Microsoft Internet Explorer 5.01 SP4, 6, and 7 allows remote attackers to access restricted information from other domains via JavaScript that uses the Object data type for the value of a (1) location or (2) location.href property, related to incorrect determination of the origin of web script, aka "Window Location Property Cross-Domain Vulnerability." NOTE: according to Microsoft, CVE-2008-2948 and CVE-2008-2949 are duplicates of this issue, probably different attack vectors.
CVE-2008-2948
Cross-domain vulnerability in Microsoft Internet Explorer 7 and 8 allows remote attackers to change the location property of a frame via the Object data type, and use a frame from a different domain to observe domain-independent events, as demonstrated by observing onkeydown events with caballero-listener.  NOTE: according to Microsoft, this is a duplicate of CVE-2008-2947, possibly a different attack vector.
CVE-2008-2949
Cross-domain vulnerability in Microsoft Internet Explorer 6 and 7 allows remote attackers to change the location property of a frame via the String data type, and use a frame from a different domain to observe domain-independent events, as demonstrated by observing onkeydown events with caballero-listener.  NOTE: according to Microsoft, this is a duplicate of CVE-2008-2947, possibly a different attack vector.
