CVE-2016-0316
Cross-site scripting (XSS) vulnerability in Lifecycle Query Engine (LQE) in IBM Jazz Reporting Service 6.0 and 6.0.1 before 6.0.1 iFix006 and 6.0.2 before iFix003 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0317
Lifecycle Query Engine (LQE) in IBM Jazz Reporting Service 6.0 and 6.0.1 before 6.0.1 iFix006 allows remote attackers to conduct clickjacking attacks via unspecified vectors.
CVE-2016-0318
Lifecycle Query Engine (LQE) in IBM Jazz Reporting Service 6.0 and 6.0.1 before 6.0.1 iFix006 does not destroy a Session ID upon a logout action, which allows remote attackers to obtain access by leveraging an unattended workstation.
CVE-2016-0319
The XML parser in Lifecycle Query Engine (LQE) in IBM Jazz Reporting Service 6.0 and 6.0.1 before 6.0.1 iFix006 allows remote authenticated administrators to read arbitrary files or cause a denial of service via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2016-2926
Cross-site scripting (XSS) vulnerability in IBM Rational Collaborative Lifecycle Management 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; Rational Quality Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; Rational Team Concert 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; Rational DOORS Next Generation 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; Rational Engineering Lifecycle Manager 4.x before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; Rational Rhapsody Design Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3; and Rational Software Architect Design Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix19, and 6.0 before 6.0.2 iFix3 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-2927
IBM BigFix Remote Control before 9.1.3 does not properly restrict the set of available encryption algorithms, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by sniffing the network and performing calculations on encrypted data.
CVE-2016-2928
IBM BigFix Remote Control before 9.1.3 allows remote authenticated users to obtain sensitive information by reading error logs.
CVE-2016-2929
IBM BigFix Remote Control before 9.1.3 does not properly restrict password choices, which makes it easier for remote attackers to obtain access via a brute-force approach.
CVE-2016-2947
IBM Rational Collaborative Lifecycle Management 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; Rational Quality Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; Rational Team Concert 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; Rational DOORS Next Generation 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; Rational Engineering Lifecycle Manager 4.x before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; Rational Rhapsody Design Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5; and Rational Software Architect Design Manager 4.0 before 4.0.7 iFix11, 5.0 before 5.0.2 iFix18, and 6.0 before 6.0.2 iFix5 allow remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2016-2984
IBM Spectrum Scale 4.1.1.x before 4.1.1.8 and 4.2.x before 4.2.0.4 and General Parallel File System (GPFS) 3.5.x before 3.5.0.32 and 4.1.x before 4.1.1.8 allow local users to gain privileges via crafted command-line parameters to a /usr/lpp/mmfs/bin/ setuid program.
CVE-2016-2985
IBM Spectrum Scale 4.1.1.x before 4.1.1.8 and 4.2.x before 4.2.0.4 and General Parallel File System (GPFS) 3.5.x before 3.5.0.32 and 4.1.x before 4.1.1.8 allow local users to gain privileges via crafted environment variables to a /usr/lpp/mmfs/bin/ setuid program.
CVE-2016-2986
Cross-site scripting (XSS) vulnerability in IBM Rational Collaborative Lifecycle Management 6.x before 6.0.1 iFix6, Rational Quality Manager 6.x before 6.0.1 iFix6, Rational Team Concert 6.x before 6.0.1 iFix6, Rational DOORS Next Generation 6.x before 6.0.1 iFix6, Rational Engineering Lifecycle Manager 6.x before 6.0.1 iFix6, and Rational Rhapsody Design Manager 6.x before 6.0.1 iFix6 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-2988
IBM Tivoli Storage Manger for Virtual Environments: Data Protection for VMware (aka Spectrum Protect for Virtual Environments) 6.4.x before 6.4.3.4 and 7.1.x before 7.1.6 allows remote authenticated users to bypass a TSM credential requirement and obtain administrative access by leveraging multiple simultaneous logins.
CVE-2016-3025
IBM Security Access Manager for Mobile 8.x before 8.0.1.4 IF3 and Security Access Manager 9.x before 9.0.1.0 IF5 do not properly restrict failed login attempts, which makes it easier for remote attackers to obtain access via a brute-force approach.
CVE-2016-3028
IBM Security Access Manager for Web 7.0 before IF2 and 8.0 before 8.0.1.4 IF3 and Security Access Manager 9.0 before 9.0.1.0 IF5 allow remote authenticated users to execute arbitrary commands by leveraging LMI admin access.
CVE-2016-3904
An elevation of privilege vulnerability in the Qualcomm bus driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30311977. References: Qualcomm QC-CR#1050455.
CVE-2016-3906
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30445973. References: Qualcomm QC-CR#1054344.
CVE-2016-3907
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30593266. References: Qualcomm QC-CR#1054352.
CVE-2016-3919
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-5195.  Reason: This candidate is a reservation duplicate of CVE-2016-5195.  Notes: All CVE users should reference CVE-2016-5195 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-5788
General Electric (GE) Bently Nevada 3500/22M USB with firmware before 5.0 and Bently Nevada 3500/22M Serial have open ports, which makes it easier for remote attackers to obtain privileged access via unspecified vectors.
CVE-2016-5955
Cross-site scripting (XSS) vulnerability in IBM Rational DOORS Next Generation 6.0.2 before iFix004 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5967
The installation component in IBM Rational Asset Analyzer (RAA) 6.1.0 before FP10 allows local users to discover the WAS Admin password by reading IM native logs.
CVE-2016-5968
The Replay Server in IBM Tealeaf Customer Experience 8.x before 8.7.1.8847 FP10, 8.8.x before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108 FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224 FP3 allows remote attackers to conduct SSRF attacks via unspecified vectors.
CVE-2016-5981
Cross-site scripting (XSS) vulnerability in IBM FileNet Workplace XT through 1.1.5.2-WPXT-LA011 and FileNet Workplace (Application Engine) through 4.0.2.14-P8AE-IF001, when RegExpSecurityFilter and ScriptSecurityFilter are misconfigured, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5991
IBM Sterling Connect:Direct 4.5.00, 4.5.01, 4.6.0 before 4.6.0.6 iFix008, and 4.7.0 before 4.7.0.4 on Windows allows local users to gain privileges via unspecified vectors.
CVE-2016-5992
IBM Sterling Connect:Direct 4.5.00, 4.5.01, 4.6.0 before 4.6.0.6 iFix008, and 4.7.0 before 4.7.0.4 on Windows allows local users to cause a denial of service via unspecified vectors.
CVE-2016-6698
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30741851. References: Qualcomm QC-CR#1058826.
CVE-2016-6700
An elevation of privilege vulnerability in libzipfile in Android 4.x before 4.4.4, 5.0.x before 5.0.2, and 5.1.x before 5.1.1 could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30916186.
CVE-2016-6701
A remote code execution vulnerability in libskia in Android 7.0 before 2016-11-01 could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as High due to the possibility of remote code execution within the context of the gallery process. Android ID: A-30190637.
CVE-2016-6702
A remote code execution vulnerability in libjpeg in Android 4.x before 4.4.4, 5.0.x before 5.0.2, and 5.1.x before 5.1.1 could enable an attacker using a specially crafted file to execute arbitrary code in the context of an unprivileged process. This issue is rated as High due to the possibility of remote code execution in an application that uses libjpeg. Android ID: A-30259087.
CVE-2016-6703
A remote code execution vulnerability in an Android runtime library in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, and 6.x before 2016-11-01 could enable an attacker using a specially crafted payload to execute arbitrary code in the context of an unprivileged process. This issue is rated as High due to the possibility of remote code execution in an application that uses the Android runtime. Android ID: A-30765246.
CVE-2016-6704
An elevation of privilege vulnerability in Mediaserver in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as High because it could be used to gain local access to elevated capabilities, which are not normally accessible to a third-party application. Android ID: A-30229821.
CVE-2016-6705
An elevation of privilege vulnerability in Mediaserver in Android 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as High because it could be used to gain local access to elevated capabilities, which are not normally accessible to a third-party application. Android ID: A-30907212.
CVE-2016-6707
An elevation of privilege vulnerability in System Server in Android 6.x before 2016-11-01 and 7.0 before 2016-11-01 could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as High because it could be used to gain local access to elevated capabilities, which are not normally accessible to a third-party application. Android ID: A-31350622.
CVE-2016-6708
An elevation of privilege in the System UI in Android 7.0 before 2016-11-01 could enable a local malicious user to bypass the security prompt of your work profile in Multi-Window mode. This issue is rated as High because it is a local bypass of user interaction requirements for any developer or security setting modifications. Android ID: A-30693465.
CVE-2016-6709
An information disclosure vulnerability in Conscrypt and BoringSSL in Android 6.x before 2016-11-01 and 7.0 before 2016-11-01 could enable a man-in-the-middle attacker to gain access to sensitive information if a non-standard cipher suite is used by an application. This issue is rated as High because it could be used to access data without permission. Android ID: A-31081987.
CVE-2016-6710
An information disclosure vulnerability in the download manager in Android 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to bypass operating system protections that isolate application data from other applications. This issue is rated as High because it could be used to gain access to data that the application does not have access to. Android ID: A-30537115.
CVE-2016-6713
A remote denial of service vulnerability in Mediaserver in Android 6.x before 2016-11-01 and 7.0 before 2016-11-01 could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High due to the possibility of remote denial of service. Android ID: A-30822755.
CVE-2016-6714
A remote denial of service vulnerability in Mediaserver in Android 6.x before 2016-11-01 and 7.0 before 2016-11-01 could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High due to the possibility of remote denial of service. Android ID: A-31092462.
CVE-2016-6715
An elevation of privilege vulnerability in the Framework APIs in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could allow a local malicious application to record audio without the user's permission. This issue is rated as Moderate because it is a local bypass of user interaction requirements (access to functionality that would normally require either user initiation or user permission.) Android ID: A-29833954.
CVE-2016-6716
An elevation of privilege vulnerability in the AOSP Launcher in Android 7.0 before 2016-11-01 could allow a local malicious application to create shortcuts that have elevated privileges without the user's consent. This issue is rated as Moderate because it is a local bypass of user interaction requirements (access to functionality that would normally require either user initiation or user permission). Android ID: A-30778130.
CVE-2016-6717
An elevation of privilege vulnerability in Mediaserver in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as Moderate because it first requires exploitation of a separate vulnerability. Android ID: A-31350239.
CVE-2016-6718
An elevation of privilege vulnerability in the Account Manager Service in Android 7.0 before 2016-11-01 could enable a local malicious application to retrieve sensitive information without user interaction. This issue is rated as Moderate because it is a local bypass of user interaction requirements (access to functionality that would normally require either user initiation or user permission.) Android ID: A-30455516.
CVE-2016-6719
An elevation of privilege vulnerability in the Bluetooth component in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to pair with any Bluetooth device without user consent. This issue is rated as Moderate because it is a local bypass of user interaction requirements (access to functionality that would normally require either user initiation or user permission.) Android ID: A-29043989.
CVE-2016-6721
An information disclosure vulnerability in Mediaserver in Android 6.x before 2016-11-01 and 7.0 before 2016-11-01 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access sensitive data without permission. Android ID: A-30875060.
CVE-2016-6723
A denial of service vulnerability in Proxy Auto Config in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a remote attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as Moderate because it requires an uncommon device configuration. Android ID: A-30100884.
CVE-2016-6724
A denial of service vulnerability in the Input Manager Service in Android 4.x before 4.4.4, 5.0.x before 5.0.2, 5.1.x before 5.1.1, 6.x before 2016-11-01, and 7.0 before 2016-11-01 could enable a local malicious application to cause the device to continually reboot. This issue is rated as Moderate because it is a temporary denial of service that requires a factory reset to fix. Android ID: A-30568284.
CVE-2016-6725
A remote code execution vulnerability in the Qualcomm crypto driver in Android before 2016-11-05 could enable a remote attacker to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of remote code execution in the context of the kernel. Android ID: A-30515053. References: Qualcomm QC-CR#1050970.
CVE-2016-6728
An elevation of privilege vulnerability in the kernel ION subsystem in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30400942.
CVE-2016-6729
An elevation of privilege vulnerability in the Qualcomm bootloader in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30977990. References: Qualcomm QC-CR#977684.
CVE-2016-6730
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30904789. References: NVIDIA N-CVE-2016-6730.
CVE-2016-6731
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30906023. References: NVIDIA N-CVE-2016-6731.
CVE-2016-6732
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30906599. References: NVIDIA N-CVE-2016-6732.
CVE-2016-6733
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30906694. References: NVIDIA N-CVE-2016-6733.
CVE-2016-6734
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30907120. References: NVIDIA N-CVE-2016-6734.
CVE-2016-6735
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30907701. References: NVIDIA N-CVE-2016-6735.
CVE-2016-6736
An elevation of privilege vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30953284. References: NVIDIA N-CVE-2016-6736.
CVE-2016-6737
An elevation of privilege vulnerability in the kernel ION subsystem in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Android ID: A-30928456.
CVE-2016-6738
An elevation of privilege vulnerability in the Qualcomm crypto engine driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30034511. References: Qualcomm QC-CR#1050538.
CVE-2016-6739
An elevation of privilege vulnerability in the Qualcomm camera driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30074605. References: Qualcomm QC-CR#1049826.
CVE-2016-6740
An elevation of privilege vulnerability in the Qualcomm camera driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30143904. References: Qualcomm QC-CR#1056307.
CVE-2016-6741
An elevation of privilege vulnerability in the Qualcomm camera driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30559423. References: Qualcomm QC-CR#1060554.
CVE-2016-6742
An elevation of privilege vulnerability in the Synaptics touchscreen driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30799828.
CVE-2016-6743
An elevation of privilege vulnerability in the Synaptics touchscreen driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30937462.
CVE-2016-6744
An elevation of privilege vulnerability in the Synaptics touchscreen driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-30970485.
CVE-2016-6745
An elevation of privilege vulnerability in the Synaptics touchscreen driver in Android before 2016-11-05 could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Android ID: A-31252388.
CVE-2016-6746
An information disclosure vulnerability in the NVIDIA GPU driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as High because it could be used to access sensitive data without explicit user permission. Android ID: A-30955105. References: NVIDIA N-CVE-2016-6746.
CVE-2016-6747
A denial of service vulnerability in Mediaserver in Android before 2016-11-05 could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High due to the possibility of remote denial of service. Android ID: A-31244612. References: NVIDIA N-CVE-2016-6747.
CVE-2016-6748
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30076504. References: Qualcomm QC-CR#987018.
CVE-2016-6749
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30228438. References: Qualcomm QC-CR#1052818.
CVE-2016-6750
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30312054. References: Qualcomm QC-CR#1052825.
CVE-2016-6751
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30902162. References: Qualcomm QC-CR#1062271.
CVE-2016-6752
An information disclosure vulnerability in Qualcomm components including the GPU driver, power driver, SMSM Point-to-Point driver, and sound driver in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-31498159. References: Qualcomm QC-CR#987051.
CVE-2016-6753
An information disclosure vulnerability in kernel components, including the process-grouping subsystem and the networking subsystem, in Android before 2016-11-05 could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Android ID: A-30149174.
CVE-2016-6754
A remote code execution vulnerability in Webview in Android 5.0.x before 5.0.2, 5.1.x before 5.1.1, and 6.x before 2016-11-05 could enable a remote attacker to execute arbitrary code when the user is navigating to a website. This issue is rated as High due to the possibility of remote code execution in an unprivileged process. Android ID: A-31217937.
CVE-2016-9449
The taxonomy module in Drupal 7.x before 7.52 and 8.x before 8.2.3 might allow remote authenticated users to obtain sensitive information about taxonomy terms by leveraging inconsistent naming of access query tags.
CVE-2016-9450
The user password reset form in Drupal 8.x before 8.2.3 allows remote attackers to conduct cache poisoning attacks by leveraging failure to specify a correct cache context.
CVE-2016-9451
Confirmation forms in Drupal 7.x before 7.52 make it easier for remote authenticated users to conduct open redirect attacks via unspecified vectors.
CVE-2016-9452
The transliterate mechanism in Drupal 8.x before 8.2.3 allows remote attackers to cause a denial of service via a crafted URL.
CVE-2016-9621
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-9429.  Reason: This candidate is a reservation duplicate of CVE-2016-9429.  Notes: All CVE users should reference CVE-2016-9429 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
