CVE-2013-0159
The fedora-business-cards package before 1-0.1.beta1.fc17 on Fedora 17 and before 1-0.1.beta1.fc18 on Fedora 18 allows local users to cause a denial of service or write to arbitrary files via a symlink attack on /tmp/fedora-business-cards-buffer.svg.
CVE-2013-0185
Cross-site request forgery (CSRF) vulnerability in ManageIQ Enterprise Virtualization Manager (EVM) allows remote attackers to hijack the authentication of users for requests that have unspecified impact via unknown vectors.
CVE-2013-2049
Red Hat CloudForms 2 Management Engine (CFME) allows remote attackers to conduct session tampering attacks by leveraging use of a static secret_token.rb secret.
CVE-2013-4035
IBM Sterling Connect:Direct for OpenVMS 3.4.00, 3.4.01, 3.5.00, 3.6.0, and 3.6.0.1 allow remote attackers to have unspecified impact by leveraging failure to reject client requests for an unencrypted session when used as the server in a TCP/IP session and configured for SSL encryption with the client. IBM X-Force ID: 86138.
CVE-2013-4040
IBM Tivoli Application Dependency Discovery Manager (TADDM) 7.1.2.x before 7.2.1.5 and 7.2.x before 7.2.2.0 on Unix use weak permissions (755) for unspecified configuration and log files, which allows local users to obtain sensitive information by reading the files. IBM X-Force ID: 86176.
CVE-2013-4201
Katello allows remote authenticated users to call the "system remove_deletion" CLI command via vectors related to "remove system" permissions.
CVE-2013-4209
Automatic Bug Reporting Tool (ABRT) before 2.1.6 allows local users to obtain sensitive information about arbitrary files via vectors related to sha1sums.
CVE-2016-10036
Unrestricted file upload vulnerability in ui/artifact/upload in JFrog Artifactory before 4.16 allows remote attackers to (1) deploy an arbitrary servlet application and execute arbitrary code by uploading a war file or (2) possibly write to arbitrary files and cause a denial of service by uploading an HTML file.
CVE-2017-14012
Boston Scientific ZOOM LATITUDE PRM Model 3120 does not encrypt PHI at rest. CVSS v3 base score: 4.6; CVSS vector string: AV:P/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N.
CVE-2017-14014
Boston Scientific ZOOM LATITUDE PRM Model 3120 uses a hard-coded cryptographic key to encrypt PHI prior to having it transferred to removable media. CVSS v3 base score: 4.6; CVSS vector string: AV:P/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N.
CVE-2017-17020
On D-Link DCS-5009 devices with firmware 1.08.11 and earlier, DCS-5010 devices with firmware 1.14.09 and earlier, and DCS-5020L devices with firmware before 1.15.01, command injection in alphapd (binary responsible for running the camera's web server) allows remote authenticated attackers to execute code through sanitized /setSystemAdmin user input in the AdminID field being passed directly to a call to system.
CVE-2017-18264
An issue was discovered in libraries/common.inc.php in phpMyAdmin 4.0 before 4.0.10.20, 4.4.x, 4.6.x, and 4.7.0 prereleases. The restrictions caused by $cfg['Servers'][$i]['AllowNoPassword'] = false are bypassed under certain PHP versions (e.g., version 5). This can allow the login of users who have no password set even if the administrator has set $cfg['Servers'][$i]['AllowNoPassword'] to false (which is also the default). This occurs because some implementations of the PHP substr function return false when given '' as the first argument.
CVE-2017-5535
The GridServer Broker, GridServer Driver, and GridServer Engine components of TIBCO Software Inc. TIBCO DataSynapse GridServer Manager contain vulnerabilities related to both the improper use of encryption mechanisms and the use of weak ciphers. A malicious actor could theoretically compromise the traffic between any of the components. Affected releases include TIBCO Software Inc.'s TIBCO DataSynapse GridServer Manager: versions up to and including 5.1.3; 6.0.0; 6.0.1; 6.0.2; 6.1.0; 6.1.1; and 6.2.0.
CVE-2017-5536
The GridServer Broker, and GridServer Director components of TIBCO Software Inc. TIBCO DataSynapse GridServer Manager contain vulnerabilities which may allow an authenticated user to perform cross-site scripting (XSS). In addition, an authenticated user could be a victim of a cross-site request forgery (CSRF) attack. Affected releases include TIBCO Software Inc.'s TIBCO DataSynapse GridServer Manager: versions up to and including 5.1.3; 6.0.0; 6.0.1; 6.0.2; 6.1.0; 6.1.1; and 6.2.0.
CVE-2018-1000166
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-3848 and CVE-2018-3849. Reason: This candidate is a reservation duplicate of CVE-2018-3848 and CVE-2018-3849. Notes: All CVE users should reference CVE-2018-3848 and CVE-2018-3849 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-10255
A CSV Injection vulnerability was discovered in clustercoding Blog Master Pro v1.0 that allows a user with low level privileges to inject a command that will be included in the exported CSV file, leading to possible code execution.
CVE-2018-10256
A SQL Injection vulnerability was discovered in HRSALE The Ultimate HRM v1.0.2 that allows a user with low level privileges to directly modify the SQL query.
CVE-2018-10257
A CSV Injection vulnerability was discovered in HRSALE The Ultimate HRM v1.0.2 that allows a user with low level privileges to inject a command that will be included in the exported CSV file, leading to possible code execution.
CVE-2018-10258
A CSV Injection vulnerability was discovered in Shopy Point of Sale v1.0 that allows a user with low level privileges to inject a command that will be included in the exported CSV file, leading to possible code execution.
CVE-2018-10259
An Authenticated Stored XSS vulnerability was found in HRSALE The Ultimate HRM v1.0.2, exploitable by a low privileged user.
CVE-2018-10260
A Local File Inclusion vulnerability was found in HRSALE The Ultimate HRM v1.0.2, exploitable by a low privileged user.
CVE-2018-10365
An XSS issue was discovered in the Threads to Link plugin 1.3 for MyBB. When editing a thread, the user is given the option to convert the thread to a link. The thread link input box is not properly sanitized.
CVE-2018-10371
An issue was discovered in the wunderfarm WF Cookie Consent plugin 1.1.3 for WordPress. A persistent cross-site scripting vulnerability has been identified in the web interface of the plugin that allows the execution of arbitrary HTML/script code to be executed in a victim's web browser via a page title.
CVE-2018-10581
In Octopus Deploy 3.4.x before 2018.4.7, an authenticated user is able to view/update/save variable values within the Tenant Variables area for Environments that do not exist within their associated Team scoping. This occurs in situations where this authenticated user also belongs to multiple teams, where one of the Teams has the VariableEdit permission or VariableView permissions for the Environment.
CVE-2018-10583
An information disclosure vulnerability occurs when LibreOffice 6.0.3 and Apache OpenOffice Writer 4.1.5 automatically process and initiate an SMB connection embedded in a malicious file, as demonstrated by xlink:href=file://192.168.0.2/test.jpg within an office:document-content element in a .odt XML document.
CVE-2018-1502
IBM Content Manager Enterprise Edition Resource Manager 8.4.3 and 9.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141338.
CVE-2018-6242
Some NVIDIA Tegra mobile processors released prior to 2016 contain a buffer overflow vulnerability in BootROM Recovery Mode (RCM). An attacker with physical access to the device's USB and the ability to force the device to reboot into RCM could exploit the vulnerability to execute unverified code.
CVE-2018-6589
CA Spectrum 10.1 prior to 10.01.02.PTF_10.1.239 and 10.2.x prior to 10.2.3 allows remote attackers to cause a denial of service via unspecified vectors.
CVE-2018-8938
A Code Injection issue was discovered in DlgSelectMibFile.asp in Ipswitch WhatsUp Gold before 2018 (18.0). Malicious actors can inject a specially crafted SNMP MIB file that could allow them to execute arbitrary commands and code on the WhatsUp Gold server.
CVE-2018-8939
An SSRF issue was discovered in NmAPI.exe in Ipswitch WhatsUp Gold before 2018 (18.0). Malicious actors can submit specially crafted requests via the NmAPI executable to (1) gain unauthorized access to the WhatsUp Gold system, (2) obtain information about the WhatsUp Gold system, or (3) execute remote commands.
CVE-2018-9232
Due to the lack of firmware authentication in the upgrade process of T&W WIFI Repeater BE126 devices, an attacker can craft a malicious firmware and use it as an update.
CVE-2018-9336
openvpnserv.exe (aka the interactive service helper) in OpenVPN 2.4.x before 2.4.6 allows a local attacker to cause a double-free of memory by sending a malformed request to the interactive service. This could cause a denial-of-service through memory corruption or possibly have unspecified other impact including privilege escalation.
