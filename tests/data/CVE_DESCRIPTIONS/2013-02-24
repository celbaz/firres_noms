CVE-2012-0439
An ActiveX control in gwcls1.dll in the client in Novell GroupWise 8.0 before 8.0.3 HP2 and 2012 before SP1 HP1 allows remote attackers to execute arbitrary code via (1) a pointer argument to the SetEngine method or (2) an XPItem pointer argument to an unspecified method.
CVE-2012-2697
Unspecified vulnerability in autofs, as used in Red Hat Enterprise Linux (RHEL) 5, allows local users to cause a denial of service (autofs crash and delayed mounts) or prevent "mount expiration" via unspecified vectors related to "using an LDAP-based automount map."
CVE-2012-4704
Array index error in 3S CODESYS Gateway-Server before 2.3.9.27 allows remote attackers to execute arbitrary code via a crafted packet.
CVE-2012-4705
Directory traversal vulnerability in 3S CODESYS Gateway-Server before 2.3.9.27 allows remote attackers to execute arbitrary code via vectors involving a crafted pathname.
CVE-2012-4706
Integer signedness error in 3S CODESYS Gateway-Server before 2.3.9.27 allows remote attackers to cause a denial of service via a crafted packet that triggers a heap-based buffer overflow.
CVE-2012-4707
3S CODESYS Gateway-Server before 2.3.9.27 allows remote attackers to execute arbitrary code via vectors that trigger an out-of-bounds memory access.
CVE-2012-4708
Stack-based buffer overflow in 3S CODESYS Gateway-Server before 2.3.9.27 allows remote attackers to execute arbitrary code via a crafted packet.
CVE-2012-5337
Multiple cross-site scripting (XSS) vulnerabilities in jforum.page in JForum 2.1.9 allow remote attackers to inject arbitrary web script or HTML via the (1) action, (2) match_type, (3) sort_by, or (4) start parameters.
CVE-2012-5624
The XMLHttpRequest object in Qt before 4.8.4 enables http redirection to the file scheme, which allows man-in-the-middle attackers to force the read of arbitrary local files and possibly obtain sensitive information via a file: URL to a QML application.
Per http://www.ubuntu.com/usn/USN-1723-1/

A security issue affects these releases of Ubuntu and its derivatives:
    Ubuntu 12.10
    Ubuntu 12.04 LTS
    Ubuntu 11.10
    Ubuntu 10.04 LTS

CVE-2012-5646
node-util/www/html/restorer.php in the Red Hat OpenShift Origin before 1.0.5-3 allows remote attackers to execute arbitrary commands via a crafted uuid in the PATH_INFO.
CVE-2012-5647
Open redirect vulnerability in node-util/www/html/restorer.php in Red Hat OpenShift Origin before 1.0.5-3 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the PATH_INFO.
CVE-2012-5658
rhc-chk.rb in Red Hat OpenShift Origin before 1.1, when -d (debug mode) is used, outputs the password and other sensitive information in cleartext, which allows context-dependent attackers to obtain sensitive information, as demonstrated by including log files or Bugzilla reports in support channels.
CVE-2012-6072
CRLF injection vulnerability in Jenkins before 1.491, Jenkins LTS before 1.480.1, and Jenkins Enterprise 1.424.x before 1.424.6.13, 1.447.x before 1.447.4.1, and 1.466.x before 1.466.10.1 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors.
CVE-2012-6073
Open redirect vulnerability in Jenkins before 1.491, Jenkins LTS before 1.480.1, and Jenkins Enterprise 1.424.x before 1.424.6.13, 1.447.x before 1.447.4.1, and 1.466.x before 1.466.10.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2012-6074
Cross-site scripting (XSS) vulnerability in Jenkins before 1.491, Jenkins LTS before 1.480.1, and Jenkins Enterprise 1.424.x before 1.424.6.13, 1.447.x before 1.447.4.1, and 1.466.x before 1.466.10.1 allows remote authenticated users with write access to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-6093
The QSslSocket::sslErrors function in Qt before 4.6.5, 4.7.x before 4.7.6, 4.8.x before 4.8.5, when using certain versions of openSSL, uses an "incompatible structure layout" that can read memory from the wrong location, which causes Qt to report an incorrect error when certificate validation fails and might cause users to make unsafe security decisions to accept a certificate.
CVE-2012-6121
Cross-site scripting (XSS) vulnerability in Roundcube Webmail before 0.8.5 allows remote attackers to inject arbitrary web script or HTML via a (1) data:text or (2) vbscript link.
CVE-2012-6128
Multiple stack-based buffer overflows in http.c in OpenConnect before 4.08 allow remote VPN gateways to cause a denial of service (application crash) via a long (1) hostname, (2) path, or (3) cookie list in a response.
CVE-2012-6273
SQL injection vulnerability in BigAntSoft BigAnt IM Message Server allows remote attackers to execute arbitrary SQL commands via an SHU (aka search user) request.
CVE-2012-6274
BigAntSoft BigAnt IM Message Server does not require authentication for file uploading, which allows remote attackers to create arbitrary files under AntServer\DocData\Public via unspecified vectors.
CVE-2012-6275
Multiple stack-based buffer overflows in AntDS.exe in BigAntSoft BigAnt IM Message Server allow remote attackers to have an unspecified impact via (1) the filename header in an SCH request or (2) the userid component in a DUPF request.
CVE-2013-0108
An ActiveX control in HscRemoteDeploy.dll in Honeywell Enterprise Buildings Integrator (EBI) R310, R400.2, R410.1, and R410.2; SymmetrE R310, R410.1, and R410.2; ComfortPoint Open Manager (aka CPO-M) Station R100; and HMIWeb Browser client packages allows remote attackers to execute arbitrary code via a crafted HTML document.
CVE-2013-0113
Nuance PDF Reader 7.0 and PDF Viewer Plus 7.1 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted PDF document.
CVE-2013-0118
CS-Cart before 3.0.6, when PayPal Standard Payments is configured, allows remote attackers to set the payment recipient via a modified value of the merchant's e-mail address, as demonstrated by setting the recipient to one's self.
CVE-2013-0120
The web interface on Dell PowerConnect 6248P switches allows remote attackers to cause a denial of service (device crash) via a malformed request.
CVE-2013-0158
Unspecified vulnerability in Jenkins before 1.498, Jenkins LTS before 1.480.2, and Jenkins Enterprise 1.447.x before 1.447.6.1 and 1.466.x before 1.466.12.1, when a slave is attached and anonymous read access is enabled, allows remote attackers to obtain the master cryptographic key via unknown vectors.
CVE-2013-0164
The lockwrap function in port-proxy/bin/openshift-port-proxy-cfg in Red Hat OpenShift Origin before 1.1 allows local users to overwrite arbitrary files via a symlink attack on a temporary file with a predictable name in /tmp.
CVE-2013-0212
store/swift.py in OpenStack Glance Essex (2012.1), Folsom (2012.2) before 2012.2.3, and Grizzly, when in Swift single tenant mode, logs the Swift endpoint's user name and password in cleartext when the endpoint is misconfigured or unusable, allows remote authenticated users to obtain sensitive information by reading the error messages.
Per http://www.ubuntu.com/usn/usn-1710-1/
A security issue affects these releases of Ubuntu and its derivatives:
Ubuntu 12.10, Ubuntu 12.04 LTS, Ubuntu 11.10
CVE-2013-0219
System Security Services Daemon (SSSD) before 1.9.4, when (1) creating, (2) copying, or (3) removing a user home directory tree, allows local users to create, modify, or delete arbitrary files via a symlink attack on another user's files.
Per https://access.redhat.com/security/cve/CVE-2013-0219

This issue affects the version of sssd shipped with Red Hat Enterprise Linux 5 and 6.

CVE-2013-0220
The (1) sss_autofs_cmd_getautomntent and (2) sss_autofs_cmd_getautomntbyname function in responder/autofs/autofssrv_cmd.c and the (3) ssh_cmd_parse_request function in responder/ssh/sshsrv_cmd.c in System Security Services Daemon (SSSD) before 1.9.4 allow remote attackers to cause a denial of service (out-of-bounds read, crash, and restart) via a crafted SSSD packet.
CVE-2013-0247
OpenStack Keystone Essex 2012.1.3 and earlier, Folsom 2012.2.3 and earlier, and Grizzly grizzly-2 and earlier allows remote attackers to cause a denial of service (disk consumption) via many invalid token requests that trigger excessive generation of log entries.
Per http://www.ubuntu.com/usn/USN-1715-1/
A security issue affects these releases of Ubuntu and its derivatives:
Ubuntu 12.10 Ubuntu 12.04 LTS
CVE-2013-0785
Cross-site scripting (XSS) vulnerability in show_bug.cgi in Bugzilla before 3.6.13, 3.7.x and 4.0.x before 4.0.10, 4.1.x and 4.2.x before 4.2.5, and 4.3.x and 4.4.x before 4.4rc2 allows remote attackers to inject arbitrary web script or HTML via the id parameter in conjunction with an invalid value of the format parameter.
CVE-2013-0786
The Bugzilla::Search::build_subselect function in Bugzilla 2.x and 3.x before 3.6.13 and 3.7.x and 4.0.x before 4.0.10 generates different error messages for invalid product queries depending on whether a product exists, which allows remote attackers to discover private product names by using debug mode for a query.
CVE-2013-0804
The client in Novell GroupWise 8.0 before 8.0.3 HP2 and 2012 before SP1 HP1 allows remote attackers to execute arbitrary code or cause a denial of service (incorrect pointer dereference) via unspecified vectors.
