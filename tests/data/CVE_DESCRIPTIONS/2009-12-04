CVE-2009-2631
Multiple clientless SSL VPN products that run in web browsers, including Stonesoft StoneGate; Cisco ASA; SonicWALL E-Class SSL VPN and SonicWALL SSL VPN; SafeNet SecureWire Access Gateway; Juniper Networks Secure Access; Nortel CallPilot; Citrix Access Gateway; and other products, when running in configurations that do not restrict access to the same domain as the VPN, retrieve the content of remote URLs from one domain and rewrite them so they originate from the VPN's domain, which violates the same origin policy and allows remote attackers to conduct cross-site scripting attacks, read cookies that originated from other domains, access the Web VPN session to gain access to internal resources, perform key logging, and conduct other attacks.  NOTE: it could be argued that this is a fundamental design problem in any clientless VPN solution, as opposed to a commonly-introduced error that can be fixed in separate implementations. Therefore a single CVE has been assigned for all products that have this design.
CVE-2009-3304
GForge 4.5.14, 4.7 rc2, and 4.8.2 allows local users to overwrite arbitrary files via a symlink attack on authorized_keys files in users' home directories, related to deb-specific/ssh_dump_update.pl and cronjobs/cvs-cron/ssh_create.php.
CVE-2009-3560
The big2_toUtf8 function in lib/xmltok.c in libexpat in Expat 2.0.1, as used in the XML-Twig module for Perl, allows context-dependent attackers to cause a denial of service (application crash) via an XML document with malformed UTF-8 sequences that trigger a buffer over-read, related to the doProlog function in lib/xmlparse.c, a different vulnerability than CVE-2009-2625 and CVE-2009-3720.
CVE-2009-4020
Stack-based buffer overflow in the hfs subsystem in the Linux kernel 2.6.32 allows remote attackers to have an unspecified impact via a crafted Hierarchical File System (HFS) filesystem, related to the hfs_readdir function in fs/hfs/dir.c.
CVE-2009-4148
DAZ Studio 2.3.3.161, 2.3.3.163, and 3.0.1.135 allows remote attackers to execute arbitrary JavaScript code via a (1) .ds, (2) .dsa, (3) .dse, or (4) .dsb file, as demonstrated by code that loads the WScript.Shell ActiveX control, related to a "script injection vulnerability."
CVE-2009-4195
Buffer overflow in Adobe Illustrator CS4 14.0.0, CS3 13.0.3 and earlier, and CS3 13.0.0 allows remote attackers to execute arbitrary code via a long DSC comment in an Encapsulated PostScript (.eps) file. NOTE: some of these details are obtained from third party information.
CVE-2009-4196
Multiple cross-site scripting (XSS) vulnerabilities in multiple scripts in Forms/ in Huawei MT882 V100R002B020 ARG-T running firmware 3.7.9.98 allow remote attackers to inject arbitrary web script or HTML via the (1) BackButton parameter to error_1; (2) wzConnFlag parameter to fresh_pppoe_1; (3) diag_pppindex_argen and (4) DiagStartFlag parameters to rpDiag_argen_1; (5) wzdmz_active and (6) wzdmzHostIP parameters to rpNATdmz_argen_1; (7) wzVIRTUALSVR_endPort, (8) wzVIRTUALSVR_endPortLocal, (9) wzVIRTUALSVR_IndexFlag, (10) wzVIRTUALSVR_localIP, (11) wzVIRTUALSVR_startPort, and (12) wzVIRTUALSVR_startPortLocal parameters to rpNATvirsvr_argen_1; (13) Connect_DialFlag, (14) Connect_DialHidden, and (15) Connect_Flag parameters to rpStatus_argen_1; (16) Telephone_select, and (17) wzFirstFlag parameters to rpwizard_1; and (18) wzConnectFlag parameter to rpwizPppoe_1.
CVE-2009-4197
rpwizPppoe.htm in Huawei MT882 V100R002B020 ARG-T running firmware 3.7.9.98 contains a form that does not disable the autocomplete setting for the password parameter, which makes it easier for local users or physically proximate attackers to obtain the password from web browsers that support autocomplete.
CVE-2009-4198
SQL injection vulnerability in my_orders.php in MyMiniBill allows remote authenticated users to execute arbitrary SQL commands via the orderid parameter in a status action.
CVE-2009-4199
Multiple SQL injection vulnerabilities in the Mambo Resident (aka Mos Res or com_mosres) component 1.0f for Mambo and Joomla!, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) property_uid parameter in a viewproperty action to index.php and the (2) regID parameter in a showregion action to index.php.
CVE-2009-4200
SQL injection vulnerability in the Seminar (com_seminar) component 1.28 for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a View_seminar action to index.php.
CVE-2009-4201
Multiple stack-based buffer overflows in Mp3 Tag Assistant Professional 2.92 build 300 allow remote attackers to execute arbitrary code via an MP3 file with a long string in the (1) ID3v1, (2) ID3v2, or (3) APEv2 metadata field.
CVE-2009-4202
Directory traversal vulnerability in the Omilen Photo Gallery (com_omphotogallery) component Beta 0.5 for Joomla! allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the controller parameter to index.php.
CVE-2009-4203
Multiple SQL injection vulnerabilities in admin/aclass/admin_func.php in Arab Portal 2.2 allow remote attackers to execute arbitrary SQL commands via the (1) X-Forwarded-For or (2) Client-IP HTTP header in a request to the default URI under admin/.
CVE-2009-4204
SQL injection vulnerability in read.php in Flashlight Free Edition allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-4205
Directory traversal vulnerability in admin.php in Flashlight Free Edition allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the action parameter.
CVE-2009-4206
SQL injection vulnerability in admin.link.modify.php in Million Dollar Text Links 1.0 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-4207
Cross-site scripting (XSS) vulnerability in the Webform module 5.x before 5.x-2.7 and 6.x before 6.x-2.7, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via a submission.
CVE-2009-4208
SQL injection vulnerability in the os_news module in Open-school (OS) 1.0 allows remote attackers to execute arbitrary SQL commands via the id parameter in a show action to index.php.
CVE-2009-4209
Multiple cross-site scripting (XSS) vulnerabilities in admin/index.php in moziloCMS 1.11.1 allow remote attackers to inject arbitrary web script or HTML via the (1) cat and (2) file parameters in an editsite action, different vectors than CVE-2008-6127 and CVE-2009-1367.
CVE-2009-4211
The U.S. Defense Information Systems Agency (DISA) Security Readiness Review (SRR) script for the Solaris x86 platform executes files in arbitrary directories as root for filenames equal to (1) java, (2) openssl, (3) php, (4) snort, (5) tshark, (6) vncserver, or (7) wireshark, which allows local users to gain privileges via a Trojan horse program.
