CVE-2007-3996
Multiple integer overflows in libgd in PHP before 5.2.4 allow remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a large (1) srcW or (2) srcH value to the (a) gdImageCopyResized function, or a large (3) sy (height) or (4) sx (width) value to the (b) gdImageCreate or the (c) gdImageCreateTrueColor function.
CVE-2007-3997
The (1) MySQL and (2) MySQLi extensions in PHP 4 before 4.4.8, and PHP 5 before 5.2.4, allow remote attackers to bypass safe_mode and open_basedir restrictions via MySQL LOCAL INFILE operations, as demonstrated by a query with LOAD DATA LOCAL INFILE.
CVE-2007-3998
The wordwrap function in PHP 4 before 4.4.8, and PHP 5 before 5.2.4, does not properly use the breakcharlen variable, which allows remote attackers to cause a denial of service (divide-by-zero error and application crash, or infinite loop) via certain arguments, as demonstrated by a 'chr(0), 0, ""' argument set.
CVE-2007-4650
Multiple unspecified vulnerabilities in Gallery before 2.2.3 allow attackers to (1) rename items, (2) read and modify item properties, or (3) lock and replace items via unknown vectors in (a) the WebDAV module; and (4) edit unspecified data files using "linked items" in WebDAV and (b) Reupload modules.
CVE-2007-4652
The session extension in PHP before 5.2.4 might allow local users to bypass open_basedir restrictions via a session file that is a symlink.
CVE-2007-4653
SQL injection vulnerability in links.php in the Links MOD 1.2.2 and earlier for phpBB 2.0.22 and earlier allows remote attackers to execute arbitrary SQL commands via the start parameter in a search action.
CVE-2007-4654
Unspecified vulnerability in SSHield 1.6.1 with OpenSSH 3.0.2p1 on Cisco WebNS 8.20.0.1 on Cisco Content Services Switch (CSS) series 11000 devices allows remote attackers to cause a denial of service (connection slot exhaustion and device crash) via a series of large packets designed to exploit the SSH CRC32 attack detection overflow (CVE-2001-0144), possibly a related issue to CVE-2002-1024.
CVE-2007-4655
Multiple directory traversal vulnerabilities in CGI RESCUE Shopping Basket Professional 7.51 and earlier allow remote attackers to list arbitrary directories, and possibly read arbitrary files, via directory traversal sequences in unspecified parameters to (1) list.cgi or (2) list2.cgi.
Additional information can be found at: http://www.securityfocus.com/bid/25500/info
CVE-2007-4656
backup-manager-upload in Backup Manager before 0.6.3 provides the FTP server hostname, username, and password as plaintext command line arguments during FTP uploads, which allows local users to obtain sensitive information by listing the process and its arguments, a different vulnerability than CVE-2007-2766.
CVE-2007-4657
Multiple integer overflows in PHP 4 before 4.4.8, and PHP 5 before 5.2.4, allow remote attackers to obtain sensitive information (memory contents) or cause a denial of service (thread crash) via a large len value to the (1) strspn or (2) strcspn function, which triggers an out-of-bounds read.  NOTE: this affects different product versions than CVE-2007-3996.
CVE-2007-4658
The money_format function in PHP 5 before 5.2.4, and PHP 4 before 4.4.8, permits multiple (1) %i and (2) %n tokens, which has unknown impact and attack vectors, possibly related to a format string vulnerability.
CVE-2007-4659
The zend_alter_ini_entry function in PHP before 5.2.4 does not properly handle an interruption to the flow of execution triggered by a memory_limit violation, which has unknown impact and attack vectors.
CVE-2007-4660
Unspecified vulnerability in the chunk_split function in PHP before 5.2.4 has unknown impact and attack vectors, related to an incorrect size calculation.
CVE-2007-4661
The chunk_split function in string.c in PHP 5.2.3 does not properly calculate the needed buffer size due to precision loss when performing integer arithmetic with floating point numbers, which has unknown attack vectors and impact, possibly resulting in a heap-based buffer overflow.  NOTE: this is due to an incomplete fix for CVE-2007-2872.
CVE-2007-4662
Buffer overflow in the php_openssl_make_REQ function in PHP before 5.2.4 has unknown impact and attack vectors.
CVE-2007-4663
Directory traversal vulnerability in PHP before 5.2.4 allows attackers to bypass open_basedir restrictions via unspecified vectors involving the glob function.
CVE-2007-4664
Unspecified vulnerability in the (1) attach database and (2) create database functionality in Firebird before 2.0.2, when a filename exceeds MAX_PATH_LEN, has unknown impact and attack vectors, aka CORE-1405.
CVE-2007-4665
Unspecified vulnerability in the server in Firebird before 2.0.2 allows remote attackers to cause a denial of service (daemon crash) via an XNET session that makes multiple simultaneous requests to register events, aka CORE-1403.
CVE-2007-4666
Unspecified vulnerability in the server in Firebird before 2.0.2, when a Superserver/TCP/IP environment is configured, allows remote attackers to cause a denial of service (CPU and memory consumption) via "large network packets with garbage", aka CORE-1397.
CVE-2007-4667
Unspecified vulnerability in the Services API in Firebird before 2.0.2 allows remote attackers to cause a denial of service, aka CORE-1149.
CVE-2007-4668
Unspecified vulnerability in the server in Firebird before 2.0.2 allows remote attackers to determine the existence of arbitrary files, and possibly obtain other "file access," via unknown vectors, aka CORE-1312.
CVE-2007-4669
The Services API in Firebird before 2.0.2 allows remote authenticated users without SYSDBA privileges to read the server log (firebird.log), aka CORE-1148.
