CVE-2008-3864
The ApiThread function in the firewall service (aka TmPfw.exe) in Trend Micro Network Security Component (NSC) modules, as used in Trend Micro OfficeScan 8.0 SP1 Patch 1 and Internet Security 2007 and 2008 17.0.1224, allows remote attackers to cause a denial of service (service crash) via a packet with a large value in an unspecified size field.
CVE-2008-3865
Multiple heap-based buffer overflows in the ApiThread function in the firewall service (aka TmPfw.exe) in Trend Micro Network Security Component (NSC) modules, as used in Trend Micro OfficeScan 8.0 SP1 Patch 1 and Internet Security 2007 and 2008 17.0.1224, allow remote attackers to execute arbitrary code via a packet with a small value in an unspecified size field.
CVE-2008-3866
The Trend Micro Personal Firewall service (aka TmPfw.exe) in Trend Micro Network Security Component (NSC) modules, as used in Trend Micro OfficeScan 8.0 SP1 Patch 1 and Internet Security 2007 and 2008 17.0.1224, relies on client-side password protection implemented in the configuration GUI, which allows local users to bypass intended access restrictions and change firewall settings by using a modified client to send crafted packets.
CVE-2008-5916
gitweb/gitweb.perl in gitweb in Git 1.6.x before 1.6.0.6, 1.5.6.x before 1.5.6.6, 1.5.5.x before 1.5.5.6, 1.5.4.x before 1.5.4.7, and other versions after 1.4.3 allows local repository owners to execute arbitrary commands by modifying the diff.external configuration variable and executing a crafted gitweb query.
CVE-2008-5917
Cross-site scripting (XSS) vulnerability in the XSS filter (framework/Text_Filter/Filter/xss.php) in Horde Application Framework 3.2.2 and 3.3, when Internet Explorer is being used, allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to style attributes.
CVE-2008-5918
Cross-site scripting (XSS) vulnerability in the getParameterisedSelfUrl function in index.php in WebSVN 2.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2008-5919
Directory traversal vulnerability in rss.php in WebSVN 2.0 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to overwrite arbitrary files via directory traversal sequences in the rev parameter.
CVE-2008-5920
The create_anchors function in utils.inc in WebSVN 1.x allows remote attackers to execute arbitrary PHP code via a crafted username that is processed by the preg_replace function with the eval switch.
Patch information - http://websvn.tigris.org/
CVE-2008-5921
SQL injection vulnerability in albums.php in Umer Inc Songs Portal allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5922
Multiple PHP remote file inclusion vulnerabilities in themes/default/index.php in Cant Find A Gaming CMS (CFAGCMS) 1 allow remote attackers to execute arbitrary PHP code via a URL in the (1) main and (2) right parameters.
CVE-2008-5923
SQL injection vulnerability in default.asp in ASP-DEv XM Events Diary allows remote attackers to execute arbitrary SQL commands the cat parameter.
CVE-2008-5924
SQL injection vulnerability in diary_viewC.asp in ASP-DEv XM Events Diary allows remote attackers to execute arbitrary SQL commands via the cat parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5925
ASP-DEv XM Events Diary stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file via a direct request for diary.mdb.
CVE-2008-5926
Multiple SQL injection vulnerabilities in login.asp in ASP-DEv Internal E-Mail System allow remote attackers to execute arbitrary SQL commands via the (1) login parameter (aka user field) or the (2) password parameter (aka pass field).  NOTE: some of these details are obtained from third party information.
CVE-2008-5927
Multiple SQL injection vulnerabilities in admin/usercheck.php in FlexPHPNews 0.0.6 allow remote attackers to execute arbitrary SQL commands via the (1) checkuser parameter (aka username field) or (2) checkpass parameter (aka password field) to admin/index.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-5928
SQL injection vulnerability in redir.php in Free Links Directory Script (FLDS) 1.2a allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5929
VP-ASP Shopping Cart 6.50 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database containing the password via a direct request for database/shopping650.mdb.  NOTE: some of these details are obtained from third party information.
CVE-2008-5930
SQL injection vulnerability in admin/blog_comments.asp in The Net Guys ASPired2Blog allows remote attackers to execute arbitrary SQL commands via the BlogID parameter.
CVE-2008-5931
The Net Guys ASPired2Blog stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file containing usernames and passwords via a direct request for admin/blog.mdb.  NOTE: some of these details are obtained from third party information.
CVE-2008-5932
CodeAvalanche FreeForum stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file containing the password via a direct request for _private/CAForum.mdb.  NOTE: some of these details are obtained from third party information.
CVE-2008-5933
Multiple cross-site scripting (XSS) vulnerabilities in index.php in CMS ISWEB 3.0 allow remote attackers to inject arbitrary web script or HTML via (1) the strcerca parameter (aka the input field for the cerca action) or (2) the id_oggetto parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5934
SQL injection vulnerability in index.php in CMS ISWEB 3.0 allows remote attackers to execute arbitrary SQL commands via the id_sezione parameter.
CVE-2008-5935
Facto stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file containing the password via a direct request for database/facto.mdb.  NOTE: some of these details are obtained from third party information.
CVE-2009-0001
Heap-based buffer overflow in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a crafted RTSP URL.
CVE-2009-0002
Heap-based buffer overflow in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a QTVR movie file with crafted THKD atoms.
CVE-2009-0003
Heap-based buffer overflow in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and execute arbitrary code via an AVI movie file with an invalid nBlockAlign value in the _WAVEFORMATEX structure.
CVE-2009-0004
Buffer overflow in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a crafted MP3 audio file.
CVE-2009-0005
Unspecified vulnerability in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a crafted H.263 encoded movie file that triggers memory corruption.
CVE-2009-0006
Integer signedness error in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a Cinepak encoded movie file with a crafted MDAT atom that triggers a heap-based buffer overflow.
CVE-2009-0007
Heap-based buffer overflow in Apple QuickTime before 7.6 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a QuickTime movie file containing invalid image width data in JPEG atoms within STSD atoms.
CVE-2009-0026
Multiple cross-site scripting (XSS) vulnerabilities in Apache Jackrabbit before 1.5.2 allow remote attackers to inject arbitrary web script or HTML via the q parameter to (1) search.jsp or (2) swr.jsp.
CVE-2009-0030
A certain Red Hat patch for SquirrelMail 1.4.8 sets the same SQMSESSID cookie value for all sessions, which allows remote authenticated users to access other users' folder lists and configuration data in opportunistic circumstances by using the standard webmail.php interface.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2008-3663.
CVE-2009-0031
Memory leak in the keyctl_join_session_keyring function (security/keys/keyctl.c) in Linux kernel 2.6.29-rc2 and earlier allows local users to cause a denial of service (kernel memory consumption) via unknown vectors related to a "missing kfree."
CVE-2009-0219
The PDF distiller in the Attachment Service in Research in Motion (RIM) BlackBerry Enterprise Server (BES) 4.1.3 through 4.1.6, BlackBerry Professional Software 4.1.4, and BlackBerry Unite! before 1.0.3 bundle 28 performs delete operations on uninitialized pointers, which allows user-assisted remote attackers to execute arbitrary code via a crafted data stream in a .pdf file.
CVE-2009-0240
listing.php in WebSVN 2.0 and possibly 1.7 beta, when using an SVN authz file, allows remote authenticated users to read changelogs or diffs for restricted projects via a modified repname parameter.
CVE-2009-0241
Stack-based buffer overflow in the process_path function in gmetad/server.c in Ganglia 3.1.1 allows remote attackers to cause a denial of service (crash) via a request to the gmetad service with a long pathname.
CVE-2009-0242
** REJECT **  gmetad in Ganglia 3.1.1, when supporting multiple requests per connection on an interactive port, allows remote attackers to cause a denial of service via a request to the gmetad service with a path that does not exist, which causes Ganglia to (1) perform excessive CPU computation and (2) send the entire tree, which consumes network bandwidth.  NOTE: the vendor and original researcher have disputed this issue, since legitimate requests can generate the same amount of resource consumption.  CVE concurs with the dispute, so this identifier should not be used.
CVE-2009-0243
Microsoft Windows does not properly enforce the Autorun and NoDriveTypeAutoRun registry values, which allows physically proximate attackers to execute arbitrary code by (1) inserting CD-ROM media, (2) inserting DVD media, (3) connecting a USB device, and (4) connecting a Firewire device; (5) allows user-assisted remote attackers to execute arbitrary code by mapping a network drive; and allows user-assisted attackers to execute arbitrary code by clicking on (6) an icon under My Computer\Devices with Removable Storage and (7) an option in an AutoPlay dialog, related to the Autorun.inf file.  NOTE: vectors 1 and 3 on Vista are already covered by CVE-2008-0951.
CVE-2009-0244
Directory traversal vulnerability in the OBEX FTP Service in the Microsoft Bluetooth stack in Windows Mobile 6 Professional, and probably Windows Mobile 5.0 for Pocket PC and 5.0 for Pocket PC Phone Edition, allows remote authenticated users to list arbitrary directories, and create or read arbitrary files, via a .. (dot dot) in a pathname.  NOTE: this can be leveraged for code execution by writing to a Startup folder.
per: http://www.seguridadmobile.com/windows-mobile/windows-mobile-security/Microsoft-Bluetooth-Stack-Directory-Traversal.html

"Non vulnerable products: Windows Mobile devices 5.0 and 6 not using Microsoft Bluetooth Stack (for example: ASUS P525, ASUS P535, ... using Widcomm/Broadcom Bluetooth Stack)"
