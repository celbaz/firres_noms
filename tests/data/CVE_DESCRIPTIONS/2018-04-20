CVE-2014-0883
Cross-site scripting (XSS) vulnerability in IBM Power Hardware Management Console (HMC) 7R7.1.0, 7R7.2.0, 7R7.3.0 through 7R7.3.5, 7R7.7.0 through SP3, and 7R7.8.0 before SP1 allows remote attackers to inject arbitrary web script or HTML via the user name on the logon screen. IBM X-Force ID: 91163.
CVE-2014-0900
The Device Administrator code in Android before 4.4.1_r1 might allow attackers to spoof device administrators and consequently bypass MDM restrictions by leveraging failure to update the mAdminMap data structure.
CVE-2014-0912
IBM Sterling B2B Integrator 5.1 and 5.2 and Sterling File Gateway 2.1 and 2.2 allow remote attackers to obtain sensitive product information via vectors related to an error page. IBM X-Force ID: 92072.
CVE-2014-0927
The ActiveMQ admin user interface in IBM Sterling B2B Integrator 5.1 and 5.2 and Sterling File Gateway 2.1 and 2.2 allows remote attackers to bypass authentication by leveraging knowledge of the port number and webapp path. IBM X-Force ID: 92259.
CVE-2014-0931
Multiple XML external entity (XXE) vulnerabilities in the (1) CCRC WAN Server / CM Server, (2) Perl CC/CQ integration trigger scripts, (3) CMAPI Java interface, (4) ClearCase remote client, and (5) CMI and OSLC-based ClearQuest integrations components in IBM Rational ClearCase 7.1.0.x, 7.1.1.x, 7.1.2 through 7.1.2.13, 8.0 through 8.0.0.10, and 8.0.1 through 8.0.1.3 allow remote attackers to cause a denial of service or access other servers via crafted XML data. IBM X-Force ID: 92263.
CVE-2014-0950
Multiple XML external entity (XXE) vulnerabilities in (1) CQWeb / CM Server, (2) ClearQuest Native client, (3) ClearQuest Eclipse client, and (4) ClearQuest Eclipse Designer components in IBM Rational ClearQuest 7.1.1 through 7.1.1.9, 7.1.2 through 7.1.2.13, 8.0.0 through 8.0.0.10, and 8.0.1 through 8.0.1.3 allow remote attackers to cause a denial of service or access other servers via crafted XML data. IBM X-Force ID: 92623.
CVE-2014-10073
The create_response function in server/server.c in Psensor before 1.1.4 allows Directory Traversal because it lacks a check for whether a file is under the webserver directory.
CVE-2014-4782
IBM InfoSphere BigInsights 2.1.2 allows remote authenticated users to discover SMTP server credentials via vectors related to the Alert management service. IBM X-Force ID: 95029.
CVE-2014-6108
IBM Tivoli Identity Manager 5.1.x before 5.1.0.15-ISS-TIM-IF0057 and Security Identity Manager 6.0.x before 6.0.0.4-ISS-SIM-IF0001 and 7.0.x before 7.0.0.0-ISS-SIM-IF0003 might allow man-in-the-middle attackers to obtain sensitive information by leveraging an unencrypted connection for interfaces. IBM X-Force ID: 96172.
CVE-2014-6109
IBM Tivoli Identity Manager 5.1.x before 5.1.0.15-ISS-TIM-IF0057 and Security Identity Manager 6.0.x before 6.0.0.4-ISS-SIM-IF0001 and 7.0.x before 7.0.0.0-ISS-SIM-IF0003 allow remote authenticated users to bypass intended access restrictions and obtain sensitive information via vectors related to server side LDAP queries. IBM X-Force ID: 96173.
CVE-2014-6111
IBM Tivoli Identity Manager 5.1.x before 5.1.0.15-ISS-TIM-IF0057 and Security Identity Manager 6.0.x before 6.0.0.4-ISS-SIM-IF0001 and 7.0.x before 7.0.0.0-ISS-SIM-IF0003 store encrypted user credentials and the keystore password in cleartext in configuration files, which allows local users to decrypt SIM credentials via unspecified vectors. IBM X-Force ID: 96180.
CVE-2014-6112
IBM Tivoli Identity Manager 5.1.x before 5.1.0.15-ISS-TIM-IF0057 and Security Identity Manager 6.0.x before 6.0.0.4-ISS-SIM-IF0001 and 7.0.x before 7.0.0.0-ISS-SIM-IF0003 make it easier for remote attackers to obtain sensitive information by leveraging support for weak SSL ciphers. IBM X-Force ID: 96184.
CVE-2017-2825
In the trapper functionality of Zabbix Server 2.4.x, specifically crafted trapper packets can pass database logic checks, resulting in database writes. An attacker can set up a Man-in-the-Middle server to alter trapper requests made between an active Zabbix proxy and Server to trigger this vulnerability.
CVE-2017-8315
Eclipse XML parser for the Eclipse IDE versions 2017.2.5 and earlier was found vulnerable to an XML External Entity attack. An attacker can exploit the vulnerability by implementing malicious code on Androidmanifest.xml.
CVE-2018-0564
Session fixation vulnerability in EC-CUBE (EC-CUBE 3.0.0, EC-CUBE 3.0.1, EC-CUBE 3.0.2, EC-CUBE 3.0.3, EC-CUBE 3..4, EC-CUBE 3.0.5, EC-CUBE 3.0.6, EC-CUBE 3.0.7, EC-CUBE 3.0.8, EC-CUBE 3.0.9, EC-CUBE 3.0.10, EC-CUBE 3.0.11, EC-CUBE 3.0.12, EC-CUBE 3.0.12-p1, EC-CUBE 3.0.13, EC-CUBE 3.0.14, EC-CUBE 3.0.15) allows remote attackers to perform arbitrary operations via unspecified vectors.
CVE-2018-10077
XML external entity (XXE) vulnerability in Geist WatchDog Console 3.2.2 allows remote authenticated administrators to read arbitrary files via crafted XML data.
CVE-2018-10078
Cross-site scripting (XSS) vulnerability in Geist WatchDog Console 3.2.2 allows remote authenticated administrators to inject arbitrary web script or HTML via a server description.
CVE-2018-10079
Geist WatchDog Console 3.2.2 uses a weak ACL for the C:\ProgramData\WatchDog Console directory, which allows local users to modify configuration data by updating (1) config.xml or (2) servers.xml.
CVE-2018-10173
Digital Guardian Management Console 7.1.2.0015 allows authenticated remote code execution because of Arbitrary File Upload functionality.
CVE-2018-10174
Digital Guardian Management Console 7.1.2.0015 has an SSRF issue that allows remote attackers to read arbitrary files via file:// URLs, send TCP traffic to intranet hosts, or obtain an NTLM hash. This can occur even if the logged-in user has a read-only role.
CVE-2018-10175
Digital Guardian Management Console 7.1.2.0015 has an XXE issue.
CVE-2018-10176
Digital Guardian Management Console 7.1.2.0015 has a Directory Traversal issue.
CVE-2018-10201
An issue was discovered in NcMonitorServer.exe in NC Monitor Server in NComputing vSpace Pro 10 and 11. It is possible to read arbitrary files outside the root directory of the web server. This vulnerability could be exploited remotely by a crafted URL without credentials, with .../ or ...\ or ..../ or ....\ as a directory-traversal pattern to TCP port 8667.
CVE-2018-10238
bvlc.c in skarg BACnet Protocol Stack 0.8.5 has a buffer overflow in BACnet/IP BVLC packet processing because of a lack of packet-size validation.
CVE-2018-10245
A Full Path Disclosure vulnerability in AWStats through 7.6 allows remote attackers to know where the config file is allocated, obtaining the full path of the server, a similar issue to CVE-2006-3682. The attack can, for example, use the awstats.pl framename and update parameters.
CVE-2018-10248
An issue was discovered in WUZHI CMS 4.1.0. There is a CSRF vulnerability that can delete any article via index.php?m=content&f=content&v=recycle_delete.
CVE-2018-10249
baijiacms V3 has CSRF via index.php?mod=site&op=edituser&name=manager&do=user to add an administrator account.
CVE-2018-10250
iCMS V7.0.8 has XSS via the admincp.php keywords parameter in a weixin_category action, aka a WeChat Classified Management keyword search.
CVE-2018-1289
In Apache Fineract versions 1.0.0, 0.6.0-incubating, 0.5.0-incubating, 0.4.0-incubating, the system exposes different REST end points to query domain specific entities with a Query Parameter 'orderBy' and 'sortOrder' which are appended directly with SQL statements. A hacker/user can inject/draft the 'orderBy' and 'sortOrder' query parameter in such a way to read/update the data for which he doesn't have authorization.
CVE-2018-1290
In Apache Fineract versions 1.0.0, 0.6.0-incubating, 0.5.0-incubating, 0.4.0-incubating, Using a single quotation escape with two continuous SQL parameters can cause a SQL injection. This could be done in Methods like retrieveAuditEntries of AuditsApiResource Class and retrieveCommands of MakercheckersApiResource Class.
CVE-2018-1291
Apache Fineract 1.0.0, 0.6.0-incubating, 0.5.0-incubating, 0.4.0-incubating exposes different REST end points to query domain specific entities with a Query Parameter 'orderBy' which are appended directly with SQL statements. A hacker/user can inject/draft the 'orderBy' query parameter by way of the "order" param in such a way to read/update the data for which he doesn't have authorization.
CVE-2018-1292
Within the 'getReportType' method in Apache Fineract 1.0.0, 0.6.0-incubating, 0.5.0-incubating, 0.4.0-incubating, a hacker could inject SQL to read/update data for which he doesn't have authorization for by way of the 'reportName' parameter.
CVE-2018-6960
VMware Horizon DaaS (7.x before 8.0.0) contains a broken authentication vulnerability that may allow an attacker to bypass two-factor authentication. Note: In order to exploit this issue, an attacker must have a legitimate account on Horizon DaaS.
CVE-2018-7747
Multiple cross-site scripting (XSS) vulnerabilities in the Caldera Forms plugin before 1.6.0-rc.1 for WordPress allow remote attackers to inject arbitrary web script or HTML via vectors involving (1) a greeting message, (2) the email transaction log, or (3) an imported form.
CVE-2018-8826
ASUS RT-AC51U, RT-AC58U, RT-AC66U, RT-AC1750, RT-ACRH13, and RT-N12 D1 routers with firmware before 3.0.0.4.380.8228; RT-AC52U B1, RT-AC1200 and RT-N600 routers with firmware before 3.0.0.4.380.10446; RT-AC55U and RT-AC55UHP routers with firmware before 3.0.0.4.382.50276; RT-AC86U and RT-AC2900 routers with firmware before 3.0.0.4.384.20648; and possibly other RT-series routers allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2018-9059
Stack-based buffer overflow in Easy File Sharing (EFS) Web Server 7.2 allows remote attackers to execute arbitrary code via a malicious login request to forum.ghp.  NOTE: this may overlap CVE-2014-3791.
