CVE-2017-18269
An SSE2-optimized memmove implementation for i386 in sysdeps/i386/i686/multiarch/memcpy-sse2-unaligned.S in the GNU C Library (aka glibc or libc6) 2.21 through 2.27 does not correctly perform the overlapping memory check if the source memory range spans the middle of the address space, resulting in corrupt data being produced by the copy operation. This may disclose information to context-dependent attackers, or result in a denial of service, or, possibly, code execution.
CVE-2017-18270
In the Linux kernel before 4.13.5, a local user could create keyrings for other users via keyctl commands, setting unwanted defaults or causing a denial of service.
CVE-2017-18271
In ImageMagick 7.0.7-16 Q16 x86_64 2017-12-22, an infinite loop vulnerability was found in the function ReadMIFFImage in coders/miff.c, which allows attackers to cause a denial of service (CPU exhaustion) via a crafted MIFF image file.
CVE-2017-18272
In ImageMagick 7.0.7-16 Q16 x86_64 2017-12-25, there is a use-after-free in ReadOneMNGImage in coders/png.c, which allows attackers to cause a denial of service via a crafted MNG image file that is mishandled in an MngInfoDiscardObject call.
CVE-2017-18273
In ImageMagick 7.0.7-16 Q16 x86_64 2017-12-22, an infinite loop vulnerability was found in the function ReadTXTImage in coders/txt.c, which allows attackers to cause a denial of service (CPU exhaustion) via a crafted image file that is mishandled in a GetImageIndexInList call.
CVE-2017-9635
Schneider Electric Ampla MES 6.4 provides capability to configure users and their privileges. When Ampla MES users are configured to use Simple Security, a weakness in the password hashing algorithm could be exploited to reverse the user's password. Schneider Electric recommends that users of Ampla MES versions 6.4 and prior should upgrade to Ampla MES version 6.5 as soon as possible.
CVE-2017-9637
Schneider Electric Ampla MES 6.4 provides capability to interact with data from third party databases. When connectivity to those databases is configured to use a SQL user name and password, an attacker may be able to sniff details from the connection string. Schneider Electric recommends that users of Ampla MES versions 6.4 and prior should upgrade to Ampla MES version 6.5 as soon as possible.
CVE-2018-1000400
Kubernetes CRI-O version prior to 1.9 contains a Privilege Context Switching Error (CWE-270) vulnerability in the handling of ambient capabilities that can result in containers running with elevated privileges, allowing users abilities they should not have. This attack appears to be exploitable via container execution. This vulnerability appears to have been fixed in 1.9.
CVE-2018-10306
Services/Form/classes/class.ilDateDurationInputGUI.php and Services/Form/classes/class.ilDateTimeInputGUI.php in ILIAS 5.1.x through 5.3.x before 5.3.4 allow XSS via an invalid date.
CVE-2018-10307
error.php in ILIAS 5.2.x through 5.3.x before 5.3.4 allows XSS via the text of a PDO exception.
CVE-2018-10967
On D-Link DIR-550A and DIR-604M devices through v2.10KR, a malicious user can forge an HTTP request to inject operating system commands that can be executed on the device with higher privileges, aka remote code execution.
CVE-2018-10968
On D-Link DIR-550A and DIR-604M devices through v2.10KR, a malicious user can use a default TELNET account to get unauthorized access to vulnerable devices, aka a backdoor access vulnerability.
CVE-2018-11232
The etm_setup_aux function in drivers/hwtracing/coresight/coresight-etm-perf.c in the Linux kernel before 4.10.2 allows attackers to cause a denial of service (panic) because a parameter is incorrectly used as a local variable.
CVE-2018-11236
stdlib/canonicalize.c in the GNU C Library (aka glibc or libc6) 2.27 and earlier, when processing very long pathname arguments to the realpath function, could encounter an integer overflow on 32-bit architectures, leading to a stack-based buffer overflow and, potentially, arbitrary code execution.
CVE-2018-11237
An AVX-512-optimized implementation of the mempcpy function in the GNU C Library (aka glibc or libc6) 2.27 and earlier may write data beyond the target buffer, leading to a buffer overflow in __mempcpy_avx512_no_vzeroupper.
CVE-2018-11243
PackLinuxElf64::unpack in p_lx_elf.cpp in UPX 3.95 allows remote attackers to cause a denial of service (double free), limit the ability of a malware scanner to operate on the entire original data, or possibly have unspecified other impact via a crafted file.
CVE-2018-11244
The BBE theme before 1.53 for WordPress allows a direct launch of an HTML editor.
CVE-2018-11245
app/webroot/js/misp.js in MISP 2.4.91 has a DOM based XSS with cortex type attributes.
CVE-2018-11248
util/FileDownloadUtils.java in FileDownloader 1.7.3 does not check an attachment's name. If an attacker places "../" in the file name, the file can be stored in an unintended directory because of Directory Traversal.
CVE-2018-11251
In ImageMagick 7.0.7-23 Q16 x86_64 2018-01-24, there is a heap-based buffer over-read in ReadSUNImage in coders/sun.c, which allows attackers to cause a denial of service (application crash in SetGrayscaleImage in MagickCore/quantize.c) via a crafted SUN image file.
CVE-2018-11254
An issue was discovered in PoDoFo 0.9.5. There is an Excessive Recursion in the PdfPagesTree::GetPageNode() function of PdfPagesTree.cpp. Remote attackers could leverage this vulnerability to cause a denial of service through a crafted pdf file, a related issue to CVE-2017-8054.
CVE-2018-11255
An issue was discovered in PoDoFo 0.9.5. The function PdfPage::GetPageNumber() in PdfPage.cpp in PoDoFo 0.9.5 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted PDF document.
CVE-2018-11256
An issue was discovered in PoDoFo 0.9.5. The function PdfDocument::Append() in PdfDocument.cpp in PoDoFo 0.9.5 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted PDF document.
CVE-2018-1147
In Nessus before 7.1.0, a XSS vulnerability exists due to improper input validation. A remote authenticated attacker could create and upload a .nessus file, which may be viewed by an administrator allowing for the execution of arbitrary script code in a user's browser session. In other scenarios, XSS could also occur by altering variables from the Advanced Settings.
CVE-2018-1148
In Nessus before 7.1.0, Session Fixation exists due to insufficient session management within the application. An authenticated attacker could maintain system access due to session fixation after a user password change.
CVE-2018-5256
CoreOS Tectonic 1.7.x before 1.7.9-tectonic.4 and 1.8.x before 1.8.4-tectonic.3 mounts a direct proxy to the kubernetes cluster at /api/kubernetes/ which is accessible without authentication to Tectonic and allows an attacker to directly connect to the kubernetes API server. Unauthenticated users are able to list all Namespaces through the Console, resulting in an information disclosure. Tectonic's exposure of an unauthenticated API endpoint containing information regarding the internal state of the cluster can provide an attacker with information that may assist in other attacks against the cluster. For example, an attacker may not have the permissions required to list all namespaces in the cluster but can instead leverage this vulnerability to enumerate the namespaces and then begin to check each namespace for weak authorization policies that may allow further escalation of privileges.
CVE-2018-6562
totemomail Encryption Gateway before 6.0_b567 allows remote attackers to obtain sensitive information about user sessions and encryption key material via a JSONP hijacking attack.
CVE-2018-8015
In Apache ORC 1.0.0 to 1.4.3 a malformed ORC file can trigger an endlessly recursive function call in the C++ or Java parser. The impact of this bug is most likely denial-of-service against software that uses the ORC file parser. With the C++ parser, the stack overflow might possibly corrupt the stack.
CVE-2018-8849
Medtronic N'Vision Clinician Programmer 8840 N'Vision Clinician Programmer, all versions, and 8870 N'Vision removable Application Card, all versions does not encrypt PII and PHI while at rest.
CVE-2018-8867
In GE PACSystems RX3i CPE305/310 version 9.20 and prior, RX3i CPE330 version 9.21 and prior, RX3i CPE 400 version 9.30 and prior, PACSystems RSTi-EP CPE 100 all versions, and PACSystems CPU320/CRU320 RXi all versions, the device does not properly validate input, which could allow a remote attacker to send specially crafted packets causing the device to become unavailable.
CVE-2018-9250
interface\super\edit_list.php in OpenEMR before v5_0_1_1 allows remote authenticated users to execute arbitrary SQL commands via the newlistname parameter.
