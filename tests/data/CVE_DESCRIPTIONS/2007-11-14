CVE-2007-3694
Cross-site scripting (XSS) vulnerability in login.php in Miro Project Broadcast Machine 0.9.9.9 allows remote attackers to inject arbitrary web script or HTML via the username parameter.
CVE-2007-3880
Format string vulnerability in srsexec in Sun Remote Services (SRS) Net Connect 3.2.3 and 3.2.4, as distributed in the SRS Proxy Core (SUNWsrspx) package, allows local users to gain privileges via format string specifiers in unspecified input that is logged through syslog.
CVE-2007-3898
The DNS server in Microsoft Windows 2000 Server SP4, and Server 2003 SP1 and SP2, uses predictable transaction IDs when querying other DNS servers, which allows remote attackers to spoof DNS replies, poison the DNS cache, and facilitate further attack vectors.
CVE-2007-4136
The ricci daemon in Red Hat Conga 0.10.0 allows remote attackers to cause a denial of service (loss of new connections) by repeatedly sending data or attempting connections.
CVE-2007-5667
NWFILTER.SYS in Novell Client 4.91 SP 1 through SP 4 for Windows 2000, XP, and Server 2003 makes the \.\nwfilter device available for arbitrary user-mode input via METHOD_NEITHER IOCTLs, which allows local users to gain privileges by passing a kernel address as an argument and overwriting kernel memory locations.
CVE-2007-5755
Multiple stack-based buffer overflows in the AOL AmpX ActiveX control in AmpX.dll 2.6.1.11 in AOL Radio allow remote attackers to execute arbitrary code via long arguments to unspecified methods.
CVE-2007-5756
Multiple array index errors in the bpf_filter_init function in NPF.SYS in WinPcap before 4.0.2, when run in monitor mode (aka Table Management Extensions or TME), and as used in Wireshark and possibly other products, allow local users to gain privileges via crafted IOCTL requests.
CVE-2007-5770
The (1) Net::ftptls, (2) Net::telnets, (3) Net::imap, (4) Net::pop, and (5) Net::smtp libraries in Ruby 1.8.5 and 1.8.6 do not verify that the commonName (CN) field in a server certificate matches the domain name in a request sent over SSL, which makes it easier for remote attackers to intercept SSL transmissions via a man-in-the-middle attack or spoofed web site, different components than CVE-2007-5162.
CVE-2007-5941
Stack-based buffer overflow in the SWCtl.SWCtl ActiveX control in Adobe Shockwave allows remote attackers to cause a denial of service and possibly execute arbitrary code via a long argument to the ShockwaveVersion method.
CVE-2007-5942
Bandersnatch 0.4 allows remote attackers to obtain sensitive information via a malformed request for index.php with (1) a certain func parameter value; or (2) certain func, jid, page, and limit parameter values; which reveals the path in various error messages.
CVE-2007-5943
Simple Machines Forum (SMF) 1.1.4 allows remote attackers to read a message in private forums by using the advanced search module with the "show results as messages" option, then searching for possible keywords contained in that message.
CVE-2007-5944
Cross-site scripting (XSS) vulnerability in Servlet Engine / Web Container in IBM WebSphere Application Server (WAS) 5.1.1.4 through 5.1.1.16 allows remote attackers to inject arbitrary web script or HTML via the Expect HTTP header.  NOTE: this might be the same issue as CVE-2006-3918, but there are insufficient details to be sure.
CVE-2007-5945
USVN before 0.6.5 allows remote attackers to obtain a list of repository contents via unspecified vectors.
CVE-2007-5946
Unspecified vulnerability in the Aries PA-RISC emulator on HP-UX B.11.23 and B.11.31 on the IA-64 platform allows local users to obtain unspecified access.
CVE-2007-5947
The jar protocol handler in Mozilla Firefox before 2.0.0.10 and SeaMonkey before 1.1.7 retrieves the inner URL regardless of its MIME type, and considers HTML documents within a jar archive to have the same origin as the inner URL, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a jar: URI.
CVE-2007-5948
Multiple cross-site scripting (XSS) vulnerabilities in main.php in SF-Shoutbox 1.2.1 through 1.4 allow remote attackers to inject arbitrary web script or HTML via the (1) nick (aka Name) and (2) shout (aka Shout) parameters.
CVE-2007-5949
Cross-site scripting (XSS) vulnerability in IBM Tivoli Service Desk 6.2 allows remote authenticated users to inject arbitrary web script or HTML via the Description parameter in a Maximo change action.
CVE-2007-5950
Cross-site scripting (XSS) vulnerability in NetCommons before 1.0.11, and 1.1.x before 1.1.2, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2006-4165.
CVE-2007-5951
SQL injection vulnerability in articles.php in E-Vendejo 0.2 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-5952
Cross-site scripting (XSS) vulnerability in admin/index.php in Helios Calendar 1.2.1 Beta allows remote attackers to inject arbitrary web script or HTML via the username parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5953
Unspecified vulnerability in Really Simple CalDAV Store (RSCDS) before 0.9.0 allows attackers to obtain sensitive information via unspecified vectors.
CVE-2007-5954
Cross-site scripting (XSS) vulnerability in buscador.php in JLMForo System allows remote attackers to inject arbitrary web script or HTML via the clave parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5955
Cross-site scripting (XSS) vulnerability in updir.php in UPDIR.NET before 2.04 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-5956
Directory traversal vulnerability in IBM Informix Dynamic Server (IDS) before 10.00.xC7W1 allows local users to gain privileges by referencing modified NLS message files through directory traversal sequences in the DBLANG environment variable.
CVE-2007-5957
Unspecified vulnerability in IBM Informix Dynamic Server (IDS) 10.00.TC3TL and 11.10.TB4TL on Windows allows attackers to cause a denial of service (application crash) via unspecified SQ_ONASSIST requests.
