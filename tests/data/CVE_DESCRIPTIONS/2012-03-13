CVE-2011-1394
IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5; IBM Tivoli Asset Management for IT 6.2, 7.1, and 7.2; IBM Tivoli Service Request Manager 7.1 and 7.2; IBM Maximo Service Desk 6.2; and IBM Tivoli Change and Configuration Management Database (CCMDB) 6.2, 7.1, and 7.2 allow remote attackers to cause a denial of service (memory consumption) by establishing many UI sessions within one HTTP session.
CVE-2011-1395
Cross-site scripting (XSS) vulnerability in imicon.jsp in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5 allows remote attackers to inject arbitrary web script or HTML via the controlid parameter.
CVE-2011-1396
Cross-site scripting (XSS) vulnerability in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5 allows remote attackers to inject arbitrary web script or HTML via the reportType parameter to an unspecified component.
CVE-2011-1397
Cross-site request forgery (CSRF) vulnerability in the Labor Reporting page in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5; IBM Tivoli Asset Management for IT 6.2, 7.1, and 7.2; IBM Tivoli Service Request Manager 7.1 and 7.2; IBM Maximo Service Desk 6.2; and IBM Tivoli Change and Configuration Management Database (CCMDB) 6.2, 7.1, and 7.2 allows remote attackers to hijack the authentication of arbitrary users.
CVE-2011-4816
SQL injection vulnerability in the KPI component in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5; IBM Tivoli Asset Management for IT 6.2, 7.1, and 7.2; IBM Tivoli Service Request Manager 7.1 and 7.2; IBM Maximo Service Desk 6.2; and IBM Tivoli Change and Configuration Management Database (CCMDB) 6.2, 7.1, and 7.2 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2011-4817
The About option on the Help menu in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5; IBM Tivoli Asset Management for IT 6.2, 7.1, and 7.2; IBM Tivoli Service Request Manager 7.1 and 7.2; IBM Maximo Service Desk 6.2; and IBM Tivoli Change and Configuration Management Database (CCMDB) 6.2, 7.1, and 7.2 shows the username, which might allow remote authenticated users to have an unspecified impact via a targeted attack against the corresponding user account.
CVE-2011-4818
Open redirect vulnerability in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5 allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via the uisessionid parameter to an unspecified component.
CVE-2011-4819
Multiple cross-site scripting (XSS) vulnerabilities in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5 allow remote attackers to inject arbitrary web script or HTML via the uisesionid parameter to (1) maximo.jsp or (2) the default URI under ui/.
CVE-2012-0002
The Remote Desktop Protocol (RDP) implementation in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 does not properly process packets in memory, which allows remote attackers to execute arbitrary code by sending crafted RDP packets triggering access to an object that (1) was not properly initialized or (2) is deleted, aka "Remote Desktop Protocol Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms12-020

"By default, the Remote Desktop Protocol is not enabled on any Windows operating system. Systems that do not have RDP enabled are not at risk. Note that on Windows XP and Windows Server 2003, Remote Assistance can enable RDP."
CVE-2012-0006
The DNS server in Microsoft Windows Server 2003 SP2 and Server 2008 SP2, R2, and R2 SP1 does not properly handle objects in memory during record lookup, which allows remote attackers to cause a denial of service (daemon restart) via a crafted query, aka "DNS Denial of Service Vulnerability."
CVE-2012-0008
Untrusted search path vulnerability in Microsoft Visual Studio 2008 SP1, 2010, and 2010 SP1 allows local users to gain privileges via a Trojan horse add-in in an unspecified directory, aka "Visual Studio Add-In Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms12-021

'An attacker could then place a specially crafted add-in in the path used by Visual Studio. When Visual Studio is started by an administrator, the specially crafted add-in would be loaded with the same privileges as the administrator.'

'The vulnerability could not be exploited remotely or by anonymous users.'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'

CVE-2012-0016
Untrusted search path vulnerability in Microsoft Expression Design; Expression Design SP1; and Expression Design 2, 3, and 4 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a .xpr or .DESIGN file, aka "Expression Design Insecure Library Loading Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms12-022

'This is a remote code execution vulnerability.'
Per:  http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2012-0152
The Remote Desktop Protocol (RDP) service in Microsoft Windows Server 2008 R2 and R2 SP1 and Windows 7 Gold and SP1 allows remote attackers to cause a denial of service (application hang) via a series of crafted packets, aka "Terminal Server Denial of Service Vulnerability."
CVE-2012-0156
DirectWrite in Microsoft Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 does not properly render Unicode characters, which allows remote attackers to cause a denial of service (application hang) via a (1) instant message or (2) web site, aka "DirectWrite Application Denial of Service Vulnerability."
CVE-2012-0157
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 does not properly handle window messaging, which allows local users to gain privileges via a crafted application that calls the PostMessage function, aka "PostMessage Function Vulnerability."
CVE-2012-0195
Cross-site scripting (XSS) vulnerability in the Start Center Layout and Configuration component in IBM Maximo Asset Management and Asset Management Essentials 6.2, 7.1, and 7.5; IBM Tivoli Asset Management for IT 6.2, 7.1, and 7.2; IBM Tivoli Service Request Manager 7.1 and 7.2; IBM Maximo Service Desk 6.2; and IBM Tivoli Change and Configuration Management Database (CCMDB) 6.2, 7.1, and 7.2 allows remote attackers to inject arbitrary web script or HTML via the display name.
CVE-2012-0687
TIBCO ActiveMatrix Runtime Platform in Service Grid and Service Bus 2.x before 2.3.2 and BusinessWorks Service Engine before 5.8.2; TIBCO ActiveMatrix Platform in TIBCO Silver Fabric ActiveMatrix Service Grid Distribution 3.1.3, Service Grid and Service Bus 3.x before 3.1.5, BusinessWorks Service Engine 5.9.x before 5.9.3, and BPM before 1.3.0; TIBCO BusinessEvents Runtime in Enterprise and Inference Editions 3.x before 3.0.3, Standard Edition 4.x before 4.0.2, and Standard Edition and Express 5.0.0; and TIBCO BusinessWorks Engine in TIBCO Silver Fabric ActiveMatrix BusinessWorks Distribution 5.9.2 and ActiveMatrix BusinessWorks before 5.9.3 allow remote attackers to obtain sensitive information via a crafted URL.
CVE-2012-0688
Cross-site scripting (XSS) vulnerability in TIBCO ActiveMatrix Platform in TIBCO Silver Fabric ActiveMatrix Service Grid Distribution 3.1.3, Service Grid and Service Bus 3.x before 3.1.5, BusinessWorks Service Engine 5.9.x before 5.9.3, and BPM before 1.3.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-0689
The server in TIBCO ActiveMatrix Platform in TIBCO Silver Fabric ActiveMatrix Service Grid Distribution 3.1.3, Service Grid and Service Bus 3.x before 3.1.5, BusinessWorks Service Engine 5.9.x before 5.9.3, and BPM before 1.3.0 allows remote attackers to discover credentials via unspecified vectors.
CVE-2012-0690
TIBCO Spotfire Web Application, Web Player Application, Automation Services Application, and Analytics Client Application in Spotfire Analytics Server before 10.1.2; Server before 3.3.3; and Web Player, Automation Services, and Professional before 4.0.2 allow remote attackers to obtain sensitive information via a crafted URL.
CVE-2012-0770
Adobe ColdFusion 8.0, 8.0.1, 9.0, and 9.0.1 computes hash values for form parameters without restricting the ability to trigger hash collisions predictably, which allows remote attackers to cause a denial of service (CPU consumption) by sending many crafted parameters.
Per: http://cwe.mitre.org/data/definitions/407.html

'CWE-407: Algorithmic Complexity'
CVE-2012-0884
The implementation of Cryptographic Message Syntax (CMS) and PKCS #7 in OpenSSL before 0.9.8u and 1.x before 1.0.0h does not properly restrict certain oracle behavior, which makes it easier for context-dependent attackers to decrypt data via a Million Message Attack (MMA) adaptive chosen ciphertext attack.
CVE-2012-1098
Cross-site scripting (XSS) vulnerability in Ruby on Rails 3.0.x before 3.0.12, 3.1.x before 3.1.4, and 3.2.x before 3.2.2 allows remote attackers to inject arbitrary web script or HTML via vectors involving a SafeBuffer object that is manipulated through certain methods.
CVE-2012-1099
Cross-site scripting (XSS) vulnerability in actionpack/lib/action_view/helpers/form_options_helper.rb in the select helper in Ruby on Rails 3.0.x before 3.0.12, 3.1.x before 3.1.4, and 3.2.x before 3.2.2 allows remote attackers to inject arbitrary web script or HTML via vectors involving certain generation of OPTION elements within SELECT elements.
CVE-2012-1472
VMware vCenter Chargeback Manager (aka CBM) before 2.0.1 does not properly handle XML API requests, which allows remote attackers to read arbitrary files or cause a denial of service via unspecified vectors.
CVE-2012-1663
Double free vulnerability in libgnutls in GnuTLS before 3.0.14 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted certificate list.
