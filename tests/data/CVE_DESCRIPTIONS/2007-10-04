CVE-2007-4133
The (1) hugetlb_vmtruncate_list and (2) hugetlb_vmtruncate functions in fs/hugetlbfs/inode.c in the Linux kernel before 2.6.19-rc4 perform certain prio_tree calculations using HPAGE_SIZE instead of PAGE_SIZE units, which allows local users to cause a denial of service (panic) via unspecified vectors.
CVE-2007-4673
Argument injection vulnerability in Apple QuickTime 7.2 for Windows XP SP2 and Vista allows remote attackers to execute arbitrary commands via a URL in the qtnext field in a crafted QTL file.  NOTE: this issue may be related to CVE-2006-4965 or CVE-2007-5045.
CVE-2007-5191
mount and umount in util-linux and loop-aes-utils call the setuid and setgid functions in the wrong order and do not check the return values, which might allow attackers to gain privileges via helpers such as mount.nfs.
CVE-2007-5193
The default configuration for twiki 4.1.2 on Debian GNU/Linux, and possibly other operating systems, specifies the work area directory (cfg{RCS}{WorkAreaDir}) under the web document root, which might allow remote attackers to obtain sensitive information when .htaccess restrictions are not applied.
CVE-2007-5194
The Chroot server in rMake 1.0.11 creates a /dev/zero device file with read/write permissions for the rMake user and the same minor device number as /dev/port, which might allow local users to gain root privileges.
CVE-2007-5198
Buffer overflow in the redir function in check_http.c in Nagios Plugins before 1.4.10, when running with the -f (follow) option, allows remote web servers to execute arbitrary code via Location header responses (redirects) with a large number of leading "L" characters.
CVE-2007-5201
The FTP backend for Duplicity before 0.4.9 sends the password as a command line argument when calling ncftp, which might allow local users to read the password by listing the process and its arguments.
CVE-2007-5207
guilt 0.27 allows local users to overwrite arbitrary files via a symlink attack on a guilt.log.[PID] temporary file.
CVE-2007-5209
Stack-based buffer overflow in DriveLock.exe in CenterTools DriveLock 5.0 allows remote attackers to execute arbitrary code via a long HTTP request to TCP port 6061.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5210
Arbor Networks Peakflow SP before 3.5.1 patch 14, and 3.6.x before 3.6.1 patch 5, allows remote authenticated users to bypass access restrictions and read or write unspecified data via unknown vectors. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5211
Multiple cross-site scripting (XSS) vulnerabilities in Arbor Networks Peakflow SP 3.5.1 before patch 14, and 3.6.1 before patch 5, when scope accounts are enabled, allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving GET or POST requests.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5212
Multiple cross-site scripting (XSS) vulnerabilities in the AXIS 2100 Network Camera 2.02 with firmware before 2.43 allow remote attackers to inject arbitrary web script or HTML via (1) parameters associated with saved settings, as demonstrated by the conf_SMTP_MailServer1 parameter to ServerManager.srv; or (2) the subpage parameter to wizard/first/wizard_main_first.shtml.  NOTE: an attacker can leverage a CSRF vulnerability to modify saved settings.
CVE-2007-5213
Multiple cross-site request forgery (CSRF) vulnerabilities in the AXIS 2100 Network Camera 2.02 with firmware 2.43 and earlier allow remote attackers to perform actions as administrators, as demonstrated by (1) an SMTP server change through the conf_SMTP_MailServer1 parameter to ServerManager.srv and (2) a hostname change through the conf_Network_HostName parameter on the Network page.
CVE-2007-5214
Multiple cross-site scripting (XSS) vulnerabilities in the AXIS 2100 Network Camera 2.02 with firmware 2.43 and earlier allow remote attackers to inject arbitrary web script or HTML via (1) the PATH_INFO to the default URI associated with a directory, as demonstrated by (a) the root directory and (b) the view/ directory; (2) parameters associated with saved settings, as demonstrated by (c) the conf_Network_HostName parameter on the Network page and (d) the conf_Layout_OwnTitle parameter to ServerManager.srv; and (3) the query string to ServerManager.srv, which is displayed on the logs page. NOTE: an attacker can leverage a CSRF vulnerability to modify saved settings.
CVE-2007-5215
Multiple PHP remote file inclusion vulnerabilities in Jacob Hinkle GodSend 0.6 allow remote attackers to execute arbitrary PHP code via a URL in the SCRIPT_DIR parameter to (1) gtk/main.inc.php or (2) cmdline.inc.php.  NOTE: vector 2 is disputed by CVE because it is contained in unaccessible code, requiring that two undefined constants be equal.
CVE-2007-5216
Multiple PHP remote file inclusion vulnerabilities in eArk (e-Ark) 1.0 allow remote attackers to execute arbitrary PHP code via a URL in (1) the cfg_vcard_path parameter to src/vcard_inc.php or (2) the cfg_phpmailer_path parameter to src/email_inc.php.  NOTE: the ark_inc.php vector is already covered by CVE-2006-6086.
