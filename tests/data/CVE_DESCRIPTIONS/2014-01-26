CVE-2013-4304
The CentralAuth extension for MediaWiki 1.19.x before 1.19.8, 1.20.x before 1.20.7, and 1.21.x before 1.21.2 caches a valid CentralAuthUser object in the centralauth_User cookie even when a user has not successfully logged in, which allows remote attackers to bypass authentication without a password.
CVE-2013-5364
Secunia CSI Agent 6.0.0.15017 and earlier, 6.0.1.1007 and earlier, and 7.0.0.21 and earlier, when running on Red Hat Linux, uses world-readable and world-writable permissions for /etc/csia_config.xml, which allows local users to change CSI Agent configuration by modifying this file.
CVE-2013-6429
The SourceHttpMessageConverter in Spring MVC in Spring Framework before 3.2.5 and 4.0.0.M1 through 4.0.0.RC1 does not disable external entity resolution, which allows remote attackers to read arbitrary files, cause a denial of service, and conduct CSRF attacks via crafted XML, aka an XML External Entity (XXE) issue, and a different vulnerability than CVE-2013-4152 and CVE-2013-7315.
CVE-2013-6466
Openswan 2.6.39 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and IKE daemon restart) via IKEv2 packets that lack expected payloads.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"
CVE-2013-6467
Libreswan 3.7 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and IKE daemon restart) via IKEv2 packets that lack expected payloads.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476 NULL Pointer Dereference"
CVE-2013-6853
Cross-site scripting (XSS) vulnerability in clickstream.js in Y! Toolbar plugin for FireFox 3.1.0.20130813024103 for Mac, and 2.5.9.2013418100420 for Windows, allows remote attackers to inject arbitrary web script or HTML via a crafted URL that is stored by the victim.
CVE-2013-6891
lppasswd in CUPS before 1.7.1, when running with setuid privileges, allows local users to read portions of arbitrary files via a modified HOME environment variable and a symlink attack involving .cups/client.conf.
CVE-2013-7137
The "remember me" functionality in login.php in Burden before 1.8.1 allows remote attackers to bypass authentication and gain privileges by setting the burden_user_rememberme cookie to 1.
CVE-2013-7140
XML External Entity (XXE) vulnerability in the CalDAV interface in Open-Xchange (OX) AppSuite 7.4.1 and earlier allows remote authenticated users to read portions of arbitrary files via vectors related to the SAX builder and the WebDAV interface.  NOTE: this issue has been labeled as both absolute path traversal and XXE, but the root cause may be XXE, since XXE can be exploited to conduct absolute path traversal and other attacks.
CWE-611: Improper Restriction of XML External Entity Reference ('XXE')
CVE-2013-7141
Cross-site scripting (XSS) vulnerability in Open-Xchange (OX) AppSuite 7.4.1 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to crafted "<%" tags.
CVE-2013-7142
Cross-site scripting (XSS) vulnerability in Open-Xchange (OX) AppSuite 7.4.1 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified oAuth API functions.
CVE-2013-7143
Cross-site scripting (XSS) vulnerability in Open-Xchange (OX) AppSuite 7.4.1 allows remote attackers to inject arbitrary web script or HTML via the title in a mail filter rule.
CVE-2013-7247
cgi-bin/tsaws.cgi in Franklin Fueling Systems TS-550 evo with firmware 2.0.0.6833 and other versions before 2.4.0 allows remote attackers to discover sensitive information (user names and password hashes) via the cmdWebGetConfiguration action in a TSA_REQUEST.
CVE-2013-7248
Franklin Fueling Systems TS-550 evo with firmware 2.0.0.6833 and other versions before 2.4.0 has a hardcoded password for the roleDiag account, which allows remote attackers to gain root privileges, as demonstrated using a cmdWebCheckRole action in a TSA_REQUEST.
CVE-2013-7296
The JBIG2Stream::readSegments method in JBIG2Stream.cc in Poppler before 0.24.5 does not use the correct specifier within a format string, which allows context-dependent attackers to cause a denial of service (segmentation fault and application crash) via a crafted PDF file.
CVE-2013-7298
query_params.cpp in cxxtools before 2.2.1 allows remote attackers to cause a denial of service (infinite recursion and crash) via an HTTP query that contains %% (double percent) characters.
CVE-2013-7299
framework/common/messageheaderparser.cpp in Tntnet before 2.2.1 allows remote attackers to obtain sensitive information via a header that ends in \n instead of \r\n, which prevents a null terminator from being added and causes Tntnet to include headers from other requests.
CVE-2014-0022
The installUpdates function in yum-cron/yum-cron.py in yum 3.4.3 and earlier does not properly check the return value of the sigCheckPkg function, which allows remote attackers to bypass the RMP package signing restriction via an unsigned package.
CVE-2014-0027
The play_wave_from_socket function in audio/auserver.c in Flite 1.4 allows local users to modify arbitrary files via a symlink attack on /tmp/awb.wav.  NOTE: some of these details are obtained from third party information.
CVE-2014-0794
SQL injection vulnerability in the JV Comment (com_jvcomment) component before 3.0.3 for Joomla! allows remote authenticated users to execute arbitrary SQL commands via the id parameter in a comment.like action to index.php.
CVE-2014-1607
** DISPUTED ** Cross-site scripting (XSS) vulnerability in the EventCalendar module for Drupal 7.14 allows remote attackers to inject arbitrary web script or HTML via the year parameter to eventcalander/. NOTE: this issue has been disputed by the Drupal Security Team; it may be site-specific.  If so, then this CVE will be REJECTed in the future.
CVE-2014-1626
XML External Entity (XXE) vulnerability in MARC::File::XML module before 1.0.2 for Perl, as used in Evergreen, Koha, perl4lib, and possibly other products, allows context-dependent attackers to read arbitrary files via a crafted XML file.
CVE-2014-1642
The IRQ setup in Xen 4.2.x and 4.3.x, when using device passthrough and configured to support a large number of CPUs, frees certain memory that may still be intended for use, which allows local guest administrators to cause a denial of service (memory corruption and hypervisor crash) and possibly execute arbitrary code via vectors related to an out-of-memory error that triggers a (1) use-after-free or (2) double free.
CVE-2014-1664
The Citrix GoToMeeting application 5.0.799.1238 for Android logs HTTP requests containing sensitive information, which allows attackers to obtain user IDs, meeting details, and authentication tokens via an application that reads the system log file.
CVE-2014-1666
The do_physdev_op function in Xen 4.1.5, 4.1.6.1, 4.2.2 through 4.2.3, and 4.3.x does not properly restrict access to the (1) PHYSDEVOP_prepare_msix and (2) PHYSDEVOP_release_msix operations, which allows local PV guests to cause a denial of service (host or guest malfunction) or possibly gain privileges via unspecified vectors.
CVE-2014-1671
Multiple SQL injection vulnerabilities in Dell KACE K1000 5.4.76847 and possibly earlier allow remote attackers or remote authenticated users to execute arbitrary SQL commands via the macAddress element in a (1) getUploadPath or (2) getKBot SOAP request to service/kbot_service.php; the ID parameter to (3) userui/advisory_detail.php or (4) userui/ticket.php; and the (5) ORDER[] parameter to userui/ticket_list.php.
CVE-2014-1672
Check Point R75.47 Security Gateway and Management Server does not properly enforce Anti-Spoofing when the routing table is modified and the "Get - Interfaces with Topology" action is performed, which allows attackers to bypass intended access restrictions.
Per: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98087

"This issue affects only R75.47 Security Gateway and R75.47 Management Server."
CVE-2014-1673
Check Point Session Authentication Agent allows remote attackers to obtain sensitive information (user credentials) via unspecified vectors.
