CVE-2008-1446
Integer overflow in the Internet Printing Protocol (IPP) ISAPI extension in Microsoft Internet Information Services (IIS) 5.0 through 7.0 on Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, and Server 2008 allows remote authenticated users to execute arbitrary code via an HTTP POST request that triggers an outbound IPP connection from a web server to a machine operated by the attacker, aka "Integer Overflow in IPP Service Vulnerability."
CVE-2008-2250
The kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly validate window properties sent from a parent window to a child window during creation of a new window, which allows local users to gain privileges via a crafted application, aka "Windows Kernel Window Creation Vulnerability."
CVE-2008-2251
Double free vulnerability in the kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows local users to gain privileges via a crafted application that makes system calls within multiple threads, aka "Windows Kernel Unhandled Exception Vulnerability." NOTE: according to Microsoft, this is not a duplicate of CVE-2008-4510.
CVE-2008-2252
The kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly validate parameters sent from user mode to the kernel, which allows local users to gain privileges via a crafted application, aka "Windows Kernel Memory Corruption Vulnerability."
CVE-2008-3464
afd.sys in the Ancillary Function Driver (AFD) component in Microsoft Windows XP SP2 and SP3 and Windows Server 2003 SP1 and SP2 does not properly validate input sent from user mode to the kernel, which allows local users to gain privileges via a crafted application, as demonstrated using crafted pointers and lengths that bypass intended ProbeForRead and ProbeForWrite restrictions, aka "AFD Kernel Overwrite Vulnerability."
CVE-2008-3466
Microsoft Host Integration Server (HIS) 2000, 2004, and 2006 does not limit RPC access to administrative functions, which allows remote attackers to bypass authentication and execute arbitrary programs via a crafted SNA RPC message using opcode 1 or 6 to call the CreateProcess function, aka "HIS Command Execution Vulnerability."
CVE-2008-3471
Stack-based buffer overflow in Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2 and SP3, and 2007 Gold and SP1; Office Excel Viewer 2003 SP3; Office Excel Viewer; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats Gold and SP1; Office 2004 and 2008 for Mac; and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via a BIFF file with a malformed record that triggers a user-influenced size calculation, aka "File Format Parsing Vulnerability."
CVE-2008-3472
Microsoft Internet Explorer 6 and 7 does not properly determine the domain or security zone of origin of web script, which allows remote attackers to bypass the intended cross-domain security policy, and execute arbitrary code or obtain sensitive information, via a crafted HTML document, aka "HTML Element Cross-Domain Vulnerability."
CVE-2008-3473
Microsoft Internet Explorer 6 and 7 does not properly determine the domain or security zone of origin of web script, which allows remote attackers to bypass the intended cross-domain security policy, and execute arbitrary code or obtain sensitive information, via a crafted HTML document, aka "Event Handling Cross-Domain Vulnerability."
CVE-2008-3474
Microsoft Internet Explorer 6 and 7 does not properly determine the domain or security zone of origin of web script, which allows remote attackers to bypass the intended cross-domain security policy and obtain sensitive information via a crafted HTML document, aka "Cross-Domain Information Disclosure Vulnerability."
CVE-2008-3475
Microsoft Internet Explorer 6 does not properly handle errors related to using the componentFromPoint method on xml objects that have been (1) incorrectly initialized or (2) deleted, which allows remote attackers to execute arbitrary code via a crafted HTML document, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2008-3476
Microsoft Internet Explorer 5.01 SP4 and 6 does not properly handle errors associated with access to uninitialized memory, which allows remote attackers to execute arbitrary code via a crafted HTML document, aka "HTML Objects Memory Corruption Vulnerability."
CVE-2008-3477
Microsoft Excel 2000 SP3, 2002 SP3, and 2003 SP2 and SP3 does not properly validate data in the VBA Performance Cache when processing an Office document with an embedded object, which allows remote attackers to execute arbitrary code via an Excel file containing a crafted value, leading to heap-based buffer overflows, integer overflows, array index errors, and memory corruption, aka "Calendar Object Validation Vulnerability."
CVE-2008-3479
Heap-based buffer overflow in the Microsoft Message Queuing (MSMQ) service (mqsvc.exe) in Microsoft Windows 2000 SP4 allows remote attackers to read memory contents and execute arbitrary code via a crafted RPC call, related to improper processing of parameters to string APIs, aka "Message Queuing Service Remote Code Execution Vulnerability."
CVE-2008-4019
Integer overflow in the REPT function in Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2 and SP3, and 2007 Gold and SP1; Office Excel Viewer 2003 SP3; Office Excel Viewer; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats Gold and SP1; Office SharePoint Server 2007 Gold and SP1; Office 2004 and 2008 for Mac; and Open XML File Format Converter for Mac allows remote attackers to execute arbitrary code via an Excel file containing a formula within a cell, aka "Formula Parsing Vulnerability."
CVE-2008-4020
Cross-site scripting (XSS) vulnerability in Microsoft Office XP SP3 allows remote attackers to inject arbitrary web script or HTML via a document that contains a "Content-Disposition: attachment" header and is accessed through a cdo: URL, which renders the content instead of raising a File Download dialog box, aka "Vulnerability in Content-Disposition Header Vulnerability."
CVE-2008-4023
Active Directory in Microsoft Windows 2000 SP4 does not properly allocate memory for (1) LDAP and (2) LDAPS requests, which allows remote attackers to execute arbitrary code via a crafted request, aka "Active Directory Overflow Vulnerability."
CVE-2008-4036
Integer overflow in Memory Manager in Microsoft Windows XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows local users to gain privileges via a crafted application that triggers an erroneous decrement of a variable, related to validation of parameters for Virtual Address Descriptors (VADs) and a "memory allocation mapping error," aka "Virtual Address Descriptor Elevation of Privilege Vulnerability."
CVE-2008-4038
Buffer underflow in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows remote attackers to execute arbitrary code via a Server Message Block (SMB) request that contains a filename with a crafted length, aka "SMB Buffer Underflow Vulnerability."
CVE-2008-4553
qemu-make-debian-root in qemu 0.9.1-5 on Debian GNU/Linux allows local users to overwrite arbitrary files via a symlink attack on temporary files and directories.
CVE-2008-4554
The do_splice_from function in fs/splice.c in the Linux kernel before 2.6.27 does not reject file descriptors that have the O_APPEND flag set, which allows local users to bypass append mode and make arbitrary changes to other locations in the file.
CVE-2008-4558
Array index error in VLC media player 0.9.2 allows remote attackers to overwrite arbitrary memory and execute arbitrary code via an XSPF playlist file with a negative identifier tag, which passes a signed comparison.
CVE-2008-4569
SQL injection vulnerability in xlacomments.asp in XIGLA Software Absolute Poll Manager XE 4.1 allows remote attackers to execute arbitrary SQL commands via the p parameter.
CVE-2008-4570
SQL injection vulnerability in index.php in Real Estate Classifieds allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2008-4571
Cross-site scripting (XSS) vulnerability in the LiveSearch module in Plone before 3.0.4 allows remote attackers to inject arbitrary web script or HTML via the Description field for search results, as demonstrated using the onerror Javascript even in an IMG tag.
CVE-2008-4572
GuildFTPd 0.999.14, and possibly other versions, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via long arguments to the CWD and LIST commands, which triggers heap corruption related to an improper free call, and possibly triggering a heap-based buffer overflow.
CVE-2008-4573
SQL injection vulnerability in kategori.asp in MunzurSoft Wep Portal W3 allows remote attackers to execute arbitrary SQL commands via the kat parameter.
CVE-2008-4574
SQL injection vulnerability in default.asp in Ayco Okul Portali allows remote attackers to execute arbitrary SQL commands via the linkid parameter.
CVE-2008-4575
Buffer overflow in the DoCommand function in jhead before 2.84 might allow context-dependent attackers to cause a denial of service (crash) via (1) a long -cmd argument and (2) unspecified vectors related to "a bunch of potential string overflows."
CVE-2008-4576
sctp in Linux kernel before 2.6.25.18 allows remote attackers to cause a denial of service (OOPS) via an INIT-ACK that states the peer does not support AUTH, which causes the sctp_process_init function to clean up active transports and triggers the OOPS when the T1-Init timer expires.
CVE-2008-4577
The ACL plugin in Dovecot before 1.1.4 treats negative access rights as if they are positive access rights, which allows attackers to bypass intended access restrictions.
CVE-2008-4578
The ACL plugin in Dovecot before 1.1.4 allows attackers to bypass intended access restrictions by using the "k" right to create unauthorized "parent/child/child" mailboxes.
CVE-2008-4579
The (1) fence_apc and (2) fence_apc_snmp programs, as used in (a) fence 2.02.00-r1 and possibly (b) cman, when running in verbose mode, allows local users to append to arbitrary files via a symlink attack on the apclog temporary file.
CVE-2008-4580
fence_manual, as used in fence 2.02.00-r1 and possibly cman, allows local users to modify arbitrary files via a symlink attack on the fence_manual.fifo temporary file.
CVE-2008-4581
The Editor in IBM ENOVIA SmarTeam 5 before release 18 SP5, and release 19 before SP01, allows remote authenticated users to bypass intended access restrictions and read Document objects via the Workflow Process (aka Flow Process) view.
CVE-2008-4582
Mozilla Firefox 3.0.1 through 3.0.3, Firefox 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13, when running on Windows, do not properly identify the context of Windows .url shortcut files, which allows user-assisted remote attackers to bypass the Same Origin Policy and obtain sensitive information via an HTML document that is directly accessible through a filesystem, as demonstrated by documents in (1) local folders, (2) Windows share folders, and (3) RAR archives, and as demonstrated by IFRAMEs referencing shortcuts that point to (a) about:cache?device=memory and (b) about:cache?device=disk, a variant of CVE-2008-2810.
CVE-2008-4583
Insecure method vulnerability in the Chilkat FTP 2.0 ActiveX component (ChilkatCert.dll) allows remote attackers to overwrite arbitrary files via a full pathname in the SavePkcs8File method.
CVE-2008-4584
Insecure method vulnerability in Chilkat Mail 7.8 ActiveX control (ChilkatCert.dll) allows remote attackers to overwrite arbitrary files via a full pathname to the SaveLastError method.
CVE-2008-4585
Belong Software Site Builder 0.1 beta allows remote attackers to bypass intended access restrictions and perform administrative actions via a direct request to admin/home.php.
CVE-2008-4586
Insecure method vulnerability in the MVSNCLientWebAgent61.WebAgent.1 ActiveX control (isusweb.dll 6.1.100.61372) in Macrovision FLEXnet Connect 6.1 allows remote attackers to force the download and execution of arbitrary files via the DownloadAndExecute method.
CVE-2008-4587
Insecure method vulnerability in the MSVNClientDownloadManager61Lib.DownloadManager.1 ActiveX control (ISDM.exe 6.1.100.61372) in Macrovision FLEXnet Connect 6.1 allows remote attackers to force the download and execution of arbitrary files via the AddFile and RunScheduledJobs methods.  NOTE: this could be leveraged for code execution by uploading executable files to Startup folders.
CVE-2008-4588
Stack-based buffer overflow in the FTP server in Etype Eserv 3.x, possibly 3.26, allows remote attackers to cause a denial of service (daemon crash) and possibly execute arbitrary code via a long argument to the ABOR command.
CVE-2008-4589
Heap-based buffer overflow in the tvtumin.sys kernel driver in Lenovo Rescue and Recovery 4.20, including 4.20.0511 and 4.20.0512, allows local users to execute arbitrary code via a long file name.
