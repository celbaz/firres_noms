CVE-2011-0185
Format string vulnerability in the debug-logging feature in Application Firewall in Apple Mac OS X before 10.7.2 allows local users to gain privileges via a crafted name of an executable file.
CVE-2011-0224
CoreMedia in Apple Mac OS X through 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted QuickTime movie file.
CVE-2011-0229
Apple Type Services (ATS) in Apple Mac OS X through 10.6.8 does not properly handle embedded Type 1 fonts, which allows remote attackers to execute arbitrary code via a crafted document that triggers an out-of-bounds memory access.
CVE-2011-0230
Buffer overflow in the ATSFontDeactivate API in Apple Type Services (ATS) in Apple Mac OS X before 10.7.2 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.
CVE-2011-0231
CFNetwork in Apple Mac OS X before 10.7.2 does not properly follow an intended cookie-storage policy, which makes it easier for remote web servers to track users via a cookie, related to a "synchronization issue."
CVE-2011-0260
The CoreProcesses component in Apple Mac OS X 10.7 before 10.7.2 does not prevent a system window from receiving keystrokes in the locked-screen state, which might allow physically proximate attackers to bypass intended access restrictions by typing into this window.
CVE-2011-3212
CoreStorage in Apple Mac OS X 10.7 before 10.7.2 does not ensure that all disk data is encrypted during the enabling of FileVault, which makes it easier for physically proximate attackers to obtain sensitive information by reading directly from the disk device.
CVE-2011-3213
The File Systems component in Apple Mac OS X before 10.7.2 does not properly track the specific X.509 certificate that a user manually accepted for an initial https WebDAV connection, which allows man-in-the-middle attackers to hijack WebDAV communication by presenting an arbitrary certificate for a subsequent connection.
CVE-2011-3214
IOGraphics in Apple Mac OS X through 10.6.8 does not properly handle a locked-screen state in display sleep mode for an Apple Cinema Display, which allows physically proximate attackers to bypass the password requirement via unspecified vectors.
CVE-2011-3215
The kernel in Apple Mac OS X before 10.7.2 does not properly prevent FireWire DMA in the absence of a login, which allows physically proximate attackers to bypass intended access restrictions and discover a password by making a DMA request in the (1) loginwindow, (2) boot, or (3) shutdown state.
CVE-2011-3216
The kernel in Apple Mac OS X before 10.7.2 does not properly implement the sticky bit for directories, which might allow local users to bypass intended permissions and delete files via an unlink system call.
CVE-2011-3217
MediaKit in Apple Mac OS X through 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted disk image.
CVE-2011-3218
The "Save for Web" selection in QuickTime Player in Apple Mac OS X through 10.6.8 exports HTML documents that contain an http link to a script file, which allows man-in-the-middle attackers to conduct cross-site scripting (XSS) attacks by spoofing the http server during local viewing of an exported document.
CVE-2011-3220
QuickTime in Apple Mac OS X before 10.7.2 does not properly process URL data handlers in movie files, which allows remote attackers to obtain sensitive information from uninitialized memory locations via a crafted file.
CVE-2011-3221
QuickTime in Apple Mac OS X before 10.7.2 does not properly handle the atom hierarchy in movie files, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted file.
CVE-2011-3222
Buffer overflow in QuickTime in Apple Mac OS X before 10.7.2 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted FlashPix file.
CVE-2011-3223
Buffer overflow in QuickTime in Apple Mac OS X before 10.7.2 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted FLIC movie file.
CVE-2011-3224
The User Documentation component in Apple Mac OS X through 10.6.8 uses http sessions for updates to App Store help information, which allows man-in-the-middle attackers to execute arbitrary code by spoofing the http server.
CVE-2011-3225
The SMB File Server component in Apple Mac OS X 10.7 before 10.7.2 does not prevent all guest users from accessing the share point record of a guest-restricted folder, which allows remote attackers to bypass intended browsing restrictions by leveraging access to the nobody account.
CVE-2011-3226
Open Directory in Apple Mac OS X 10.7 before 10.7.2, when an LDAPv3 server is used with RFC 2307 or custom mappings, allows remote attackers to bypass the password requirement by leveraging lack of an AuthenticationAuthority attribute for a user account.
CVE-2011-3227
libsecurity in Apple Mac OS X before 10.7.2 does not properly handle errors during processing of a nonstandard extension in a Certificate Revocation list (CRL), which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) a crafted (1) web site or (2) e-mail message.
CVE-2011-3228
QuickTime in Apple Mac OS X before 10.7.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted movie file.
CVE-2011-3229
Directory traversal vulnerability in Apple Safari before 5.1.1 allows remote attackers to execute arbitrary JavaScript code, in a Safari Extensions context, via a crafted safari-extension: URL.
CVE-2011-3230
Apple Safari before 5.1.1 on Mac OS X does not enforce an intended policy for file: URLs, which allows remote attackers to execute arbitrary code via a crafted web site.
CVE-2011-3231
The SSL implementation in Apple Safari before 5.1.1 on Mac OS X before 10.7 accesses uninitialized memory during the processing of X.509 certificates, which allows remote web servers to execute arbitrary code via a crafted certificate.
CVE-2011-3242
The Private Browsing feature in Apple Safari before 5.1.1 on Mac OS X does not properly recognize the Always value of the Block Cookies setting, which makes it easier for remote web servers to track users via a cookie.
CVE-2011-3243
Cross-site scripting (XSS) vulnerability in WebKit, as used in Apple iOS before 5 and Safari before 5.1.1, allows remote attackers to inject arbitrary web script or HTML via vectors involving inactive DOM windows.
CVE-2011-3245
The Keyboards component in Apple iOS before 5 displays the final character of an entered password during a subsequent use of a keyboard, which allows physically proximate attackers to obtain sensitive information by reading this character.
CVE-2011-3246
CFNetwork in Apple iOS before 5.0.1 and Mac OS X 10.7 before 10.7.2 does not properly parse URLs, which allows remote attackers to trigger visits to unintended web sites, and transmission of cookies to unintended web sites, via a crafted (1) http or (2) https URL.
CVE-2011-3253
CalDAV in Apple iOS before 5 does not validate X.509 certificates for SSL sessions, which allows man-in-the-middle attackers to spoof calendar servers and obtain sensitive information via an arbitrary certificate.
CVE-2011-3254
Cross-site scripting (XSS) vulnerability in Calendar in Apple iOS before 5 allows remote attackers to inject arbitrary web script or HTML via an invitation note.
CVE-2011-3255
CFNetwork in Apple iOS before 5 stores AppleID credentials in an unspecified file, which makes it easier for remote attackers to obtain sensitive information via a crafted application.
CVE-2011-3256
FreeType 2 before 2.4.7, as used in CoreGraphics in Apple iOS before 5, Mandriva Enterprise Server 5, and possibly other products, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted font, a different vulnerability than CVE-2011-0226.
CVE-2011-3257
The Data Access component in Apple iOS before 5 does not properly handle the existence of multiple user accounts on the same mail server, which allows local users to bypass intended access restrictions in opportunistic circumstances by leveraging a different account's cookie.
CVE-2011-3259
The kernel in Apple iOS before 5 and Apple TV before 4.4 does not properly recover memory allocated for incomplete TCP connections, which allows remote attackers to cause a denial of service (resource consumption) by making many connection attempts.
CVE-2011-3260
Buffer overflow in OfficeImport in Apple iOS before 5 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted Microsoft Word document.
CVE-2011-3261
Double free vulnerability in OfficeImport in Apple iOS before 5 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted Excel spreadsheet.
CVE-2011-3426
Cross-site scripting (XSS) vulnerability in Safari in Apple iOS before 5 allows remote web servers to inject arbitrary web script or HTML via a file accompanied by a "Content-Disposition: attachment" HTTP header.
CVE-2011-3427
The Data Security component in Apple iOS before 5 and Apple TV before 4.4 does not properly restrict use of the MD5 hash algorithm within X.509 certificates, which makes it easier for man-in-the-middle attackers to spoof servers or obtain sensitive information via a crafted certificate.
CVE-2011-3429
The Settings component in Apple iOS before 5 stores a cleartext parental-restrictions passcode in an unspecified file, which might allow physically proximate attackers to obtain sensitive information by reading this file.
CVE-2011-3430
The Settings component in Apple iOS before 5, when a configuration profile is used for a locale other than English, does not properly implement localization, which makes it easier for attackers to have an unspecified impact by leveraging incorrect configuration display.
CVE-2011-3431
The Home screen component in Apple iOS before 5 does not properly support a certain application-switching gesture, which might allow physically proximate attackers to obtain sensitive state information by watching the device's screen.
CVE-2011-3432
The UIKit Alerts component in Apple iOS before 5 allows remote attackers to cause a denial of service (device hang) via a long tel: URL that triggers a large size for the acceptance dialog.
CVE-2011-3434
The WiFi component in Apple iOS before 5 stores WiFi credentials in an unspecified file, which makes it easier for remote attackers to obtain sensitive information via a crafted application.
CVE-2011-3435
Open Directory in Apple Mac OS X 10.7 before 10.7.2 allows local users to read the password data of arbitrary users via unspecified vectors.
CVE-2011-3436
Open Directory in Apple Mac OS X 10.7 before 10.7.2 does not require a user to provide the current password before changing this password, which allows remote attackers to bypass intended password-change restrictions by leveraging an unattended workstation.
CVE-2011-3437
Integer signedness error in Apple Type Services (ATS) in Apple Mac OS X 10.7 before 10.7.2 allows remote attackers to execute arbitrary code via a crafted embedded Type 1 font in a document.
