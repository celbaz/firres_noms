CVE-2009-2801
The Application Firewall in Apple Mac OS X 10.5.8 drops unspecified firewall rules after a reboot, which might allow remote attackers to bypass intended access restrictions via packet data, related to a "timing issue."
CVE-2009-4763
Unspecified vulnerability in the ClickHeat plugin, as used in phpMyVisites before 2.4, has unknown impact and attack vectors.  NOTE: due to lack of details from the vendor, it is not clear whether this is related to CVE-2008-5793.
CVE-2010-0055
xar in Apple Mac OS X 10.5.8 does not properly validate package signatures, which allows attackers to have an unspecified impact via a modified package.
CVE-2010-0056
Buffer overflow in Cocoa spell checking in AppKit in Apple Mac OS X 10.5.8 allows user-assisted remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted document.
CVE-2010-0057
AFP Server in Apple Mac OS X before 10.6.3 does not prevent guest use of AFP shares when guest access is disabled, which allows remote attackers to bypass intended access restrictions via a mount request.
CVE-2010-0058
freshclam in ClamAV in Apple Mac OS X 10.5.8 with Security Update 2009-005 has an incorrect launchd.plist ProgramArguments key and consequently does not run, which might allow remote attackers to introduce viruses into the system.
CVE-2010-0059
CoreAudio in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via crafted audio content with QDM2 encoding, which triggers a buffer overflow due to inconsistent length fields, related to QDCA.
CVE-2010-0060
CoreAudio in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via crafted audio content with QDMC encoding.
CVE-2010-0062
Heap-based buffer overflow in quicktime.qts in CoreMedia and QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a malformed .3g2 movie file with H.263 encoding that triggers an incorrect buffer length calculation.
CVE-2010-0063
Incomplete blacklist vulnerability in CoreTypes in Apple Mac OS X before 10.6.3 makes it easier for user-assisted remote attackers to execute arbitrary JavaScript via a web page that offers a download with a Content-Type value that is not on the list of possibly unsafe content types for Safari, as demonstrated by the values for the (1) .ibplugin and (2) .url extensions.
Per: http://cwe.mitre.org/data/slices/2000.html

'Incomplete Blacklist - CWE-184'
CVE-2010-0064
DesktopServices in Apple Mac OS X 10.6 before 10.6.3 preserves file ownership during an authenticated Finder copy, which might allow local users to bypass intended disk-quota restrictions and have unspecified other impact by copying files owned by other users.
CVE-2010-0065
Disk Images in Apple Mac OS X before 10.6.3 allows user-assisted remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted disk image with bzip2 compression.
CVE-2010-0497
Disk Images in Apple Mac OS X before 10.6.3 does not provide the expected warning for an unsafe file type in an internet enabled disk image, which makes it easier for user-assisted remote attackers to execute arbitrary code via a package file type.
CVE-2010-0498
Directory Services in Apple Mac OS X before 10.6.3 does not properly perform authorization during processing of record names, which allows local users to gain privileges via unspecified vectors.
CVE-2010-0500
Event Monitor in Apple Mac OS X before 10.6.3 does not properly validate hostnames of SSH clients, which allows remote attackers to cause a denial of service (arbitrary client blacklisting) via a crafted DNS PTR record, related to a "plist injection issue."
CVE-2010-0501
Directory traversal vulnerability in FTP Server in Apple Mac OS X Server before 10.6.3 allows remote authenticated users to read arbitrary files via crafted filenames.
Per: http://support.apple.com/kb/HT4077

'This issue only affects Mac OS X Server systems.'
CVE-2010-0502
iChat Server in Apple Mac OS X Server before 10.6.3, when group chat is used, does not perform logging for all types of messages, which might allow remote attackers to avoid message auditing via an unspecified selection of message type.
Per: http://support.apple.com/kb/HT4077

'This issue only affects Mac OS X Server systems.
CVE-2010-0503
Use-after-free vulnerability in iChat Server in Apple Mac OS X Server 10.5.8 allows remote authenticated users to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.
Per: http://support.apple.com/kb/HT4077

'This issue only affects Mac OS X Server systems, and does not affect versions 10.6 or later'
CVE-2010-0504
Multiple stack-based buffer overflows in iChat Server in Apple Mac OS X Server before 10.6.3 allow remote attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.
Per: http://support.apple.com/kb/HT4077

'These issues only affect Mac OS X Server systems.'

CVE-2010-0505
Heap-based buffer overflow in ImageIO in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted JP2 (JPEG2000) image, related to incorrect calculation and the CGImageReadGetBytesAtOffset function.
CVE-2010-0506
Buffer overflow in Image RAW in Apple Mac OS X 10.5.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted NEF image.
Per: http://support.apple.com/kb/HT4077

'This issue does not affect Mac OS X v10.6 systems'
CVE-2010-0507
Buffer overflow in Image RAW in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PEF image.
CVE-2010-0508
Mail in Apple Mac OS X before 10.6.3 does not disable the filter rules associated with a deleted mail account, which has unspecified impact and attack vectors.
CVE-2010-0509
SFLServer in OS Services in Apple Mac OS X before 10.6.3 allows local users to gain privileges via vectors related to use of wheel group membership during access to the home directories of user accounts.
CVE-2010-0510
Password Server in Apple Mac OS X Server before 10.6.3 does not properly perform password replication, which might allow remote authenticated users to obtain login access via an expired password.
Per: http://support.apple.com/kb/HT4077

'This issue only affects Mac OS X Server systems'
CVE-2010-0511
Podcast Producer in Apple Mac OS X 10.6 before 10.6.3 deletes the access restrictions of a Podcast Composer workflow when this workflow is overwritten, which allows attackers to access a workflow via unspecified vectors.
CVE-2010-0512
The Accounts Preferences implementation in Apple Mac OS X 10.6 before 10.6.3, when a network account server is used, does not support Login Window access control that is based solely on group membership, which allows attackers to bypass intended access restrictions by entering login credentials.
Per: http://support.apple.com/kb/HT4077

'This issue only affects systems configured to use a network account server, and does not affect systems prior to Mac OS X v10.6.'
CVE-2010-0513
Stack-based buffer overflow in PS Normalizer in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PostScript document.
Per: http://support.apple.com/kb/HT4077

'On Mac OS X v10.6 systems this issue is mitigated by the -fstack-protector compiler flag.'
CVE-2010-0514
Heap-based buffer overflow in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file with H.261 encoding.
CVE-2010-0515
QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted movie file with H.264 encoding.
CVE-2010-0516
Heap-based buffer overflow in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file with RLE encoding, which triggers memory corruption when the length of decompressed data exceeds that of the allocated heap chunk.
CVE-2010-0517
Heap-based buffer overflow in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file with M-JPEG encoding, which causes QuickTime to calculate a buffer size using height and width fields, but to use a different field to control the length of a copy operation.
CVE-2010-0518
QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted movie file with Sorenson encoding.
CVE-2010-0519
Integer overflow in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a FlashPix image with a malformed SubImage Header Stream containing a NumberOfTiles field with a large value.
CVE-2010-0520
Heap-based buffer overflow in QuickTimeAuthoring.qtx in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted FLC file, related to crafted DELTA_FLI chunks and untrusted length values in a .fli file, which are not properly handled during decompression.
CVE-2010-0521
Server Admin in Apple Mac OS X Server before 10.6.3 does not properly enforce authentication for directory binding, which allows remote attackers to obtain potentially sensitive information from Open Directory via unspecified LDAP requests.
CVE-2010-0522
Server Admin in Apple Mac OS X Server 10.5.8 does not properly determine the privileges of users who had former membership in the admin group, which allows remote authenticated users to leverage this former membership to obtain a server connection via screen sharing.
CVE-2010-0523
Wiki Server in Apple Mac OS X 10.5.8 does not restrict the file types of uploaded files, which allows remote attackers to obtain sensitive information or possibly have unspecified other impact via a crafted file, as demonstrated by a Java applet.
Per: http://support.apple.com/kb/HT4077

'This issue only affects Mac OS X Server systems, and does not affect versions 10.6 or later.'
CVE-2010-0524
The default configuration of the FreeRADIUS server in Apple Mac OS X Server before 10.6.3 permits EAP-TLS authenticated connections on the basis of an arbitrary client certificate, which allows remote attackers to obtain network connectivity via a crafted RADIUS Access Request message.
CVE-2010-0525
Mail in Apple Mac OS X before 10.6.3 does not properly enforce the key usage extension during processing of a keychain that specifies multiple certificates for an e-mail recipient, which might make it easier for remote attackers to obtain sensitive information via a brute-force attack on a weakly encrypted e-mail message.
CVE-2010-0526
Heap-based buffer overflow in QuickTimeMPEG.qtx in QuickTime in Apple Mac OS X before 10.6.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted genl atom in a QuickTime movie file with MPEG encoding, which is not properly handled during decompression.
CVE-2010-0533
Directory traversal vulnerability in AFP Server in Apple Mac OS X before 10.6.3 allows remote attackers to list a share root's parent directory, and read and modify files in that directory, via unspecified vectors.
CVE-2010-0534
Wiki Server in Apple Mac OS X 10.6 before 10.6.3 does not enforce the service access control list (SACL) for weblogs during weblog creation, which allows remote authenticated users to publish content via HTTP requests.
CVE-2010-0535
Dovecot in Apple Mac OS X 10.6 before 10.6.3, when Kerberos is enabled, does not properly enforce the service access control list (SACL) for sending and receiving e-mail, which allows remote authenticated users to bypass intended access restrictions via unspecified vectors.
CVE-2010-0537
DesktopServices in Apple Mac OS X 10.6 before 10.6.3 does not properly resolve pathnames in certain circumstances involving an application's save panel, which allows user-assisted remote attackers to trigger unintended remote file copying via a crafted share name.
CVE-2010-1216
PHP remote file inclusion vulnerability in templates/template.php in notsoPureEdit 1.4.1 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the content parameter.  NOTE: some of these details are obtained from third party information.
CVE-2010-1217
Directory traversal vulnerability in the JE Form Creator (com_jeformcr) component for Joomla!, when magic_quotes_gpc is disabled, allows remote attackers to read arbitrary files via directory traversal sequences in the view parameter to index.php. NOTE: the original researcher states that the affected product is JE Tooltip, not Form Creator; however, the exploit URL suggests that Form Creator is affected.
CVE-2010-1218
Cross-site scripting (XSS) vulnerability in the mm_forum extension 1.8.2 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-1219
Directory traversal vulnerability in the JA News (com_janews) component 1.0 for Joomla! allows remote attackers to read arbitrary local files via a .. (dot dot) in the controller parameter to index.php.  NOTE: some of these details are obtained from third party information.
