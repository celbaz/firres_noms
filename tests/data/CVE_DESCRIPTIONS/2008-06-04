CVE-2007-5604
Buffer overflow in the ExtractCab function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to execute arbitrary code via a long first argument, a different vulnerability than CVE-2007-5605, CVE-2007-5606, and CVE-2007-5607.
CVE-2007-5605
Buffer overflow in the GetFileTime function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to execute arbitrary code via a long argument, a different vulnerability than CVE-2007-5604, CVE-2007-5606, and CVE-2007-5607.
CVE-2007-5606
Buffer overflow in the MoveFile function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to execute arbitrary code via a long argument, a different vulnerability than CVE-2007-5604, CVE-2007-5605, and CVE-2007-5607.
CVE-2007-5607
Buffer overflow in the RegistryString function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to execute arbitrary code via a long first argument, a different vulnerability than CVE-2007-5604, CVE-2007-5605, and CVE-2007-5606.
CVE-2007-5608
The DownloadFile function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to force a download of an arbitrary file onto a client machine via a URL in the first argument and a destination filename in the second argument, a different vulnerability than CVE-2008-0952 and CVE-2008-0953.
CVE-2007-5610
The DeleteSingleFile function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to delete an arbitrary file via a full pathname in the argument.
CVE-2008-0952
The AppendStringToFile function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to create files with arbitrary content via a full pathname in the first argument and the content in the second argument, a different vulnerability than CVE-2007-5608 and CVE-2008-0953.
CVE-2008-0953
The StartApp function in the HPISDataManagerLib.Datamgr ActiveX control in HPISDataManager.dll in HP Instant Support before 1.0.0.24 allows remote attackers to execute arbitrary programs via a .exe filename in the argument, a different vulnerability than CVE-2007-5608 and CVE-2008-0953.
CVE-2008-1108
Buffer overflow in Evolution 2.22.1, when the ITip Formatter plugin is disabled, allows remote attackers to execute arbitrary code via a long timezone string in an iCalendar attachment.
CVE-2008-1109
Heap-based buffer overflow in Evolution 2.22.1 allows user-assisted remote attackers to execute arbitrary code via a long DESCRIPTION property in an iCalendar attachment, which is not properly handled during a reply in the calendar view (aka the Calendars window).
CVE-2008-1661
Stack-based buffer overflow in DoubleTake.exe in HP StorageWorks Storage Mirroring (SWSM) before 4.5 SP2 allows remote attackers to execute arbitrary code via a crafted encoded authentication request.
CVE-2008-1770
CRLF injection vulnerability in Akamai Download Manager ActiveX control before 2.2.3.6 allows remote attackers to force the download and execution of arbitrary files via a URL parameter containing an encoded LF followed by a malicious target line.
CVE-2008-1947
Cross-site scripting (XSS) vulnerability in Apache Tomcat 5.5.9 through 5.5.26 and 6.0.0 through 6.0.16 allows remote attackers to inject arbitrary web script or HTML via the name parameter (aka the hostname attribute) to host-manager/html/add.
CVE-2008-2055
Cisco Adaptive Security Appliance (ASA) and Cisco PIX security appliance 7.1.x before 7.1(2)70, 7.2.x before 7.2(4), and 8.0.x before 8.0(3)10 allows remote attackers to cause a denial of service via a crafted TCP ACK packet to the device interface.
CVE-2008-2056
Cisco Adaptive Security Appliance (ASA) and Cisco PIX security appliance 8.0.x before 8.0(3)9 and 8.1.x before 8.1(1)1 allows remote attackers to cause a denial of service (device reload) via a crafted Transport Layer Security (TLS) packet to the device interface.
CVE-2008-2057
The Instant Messenger (IM) inspection engine in Cisco Adaptive Security Appliance (ASA) and Cisco PIX security appliance 7.2.x before 7.2(4), 8.0.x before 8.0(3)10, and 8.1.x before 8.1(1)2 allows remote attackers to cause a denial of service via a crafted packet.
CVE-2008-2058
Cisco Adaptive Security Appliance (ASA) and Cisco PIX security appliance 7.2.x before 7.2(3)2 and 8.0.x before 8.0(2)17 allows remote attackers to cause a denial of service (device reload) via a port scan against TCP port 443 on the device.
CVE-2008-2059
Cisco Adaptive Security Appliance (ASA) and Cisco PIX security appliance 8.0.x before 8.0(3)9 allows remote attackers to bypass control-plane ACLs for the device via unknown vectors.
CVE-2008-2119
Asterisk Open Source 1.0.x and 1.2.x before 1.2.29 and Business Edition A.x.x and B.x.x before B.2.5.3, when pedantic parsing (aka pedanticsipchecking) is enabled, allows remote attackers to cause a denial of service (daemon crash) via a SIP INVITE message that lacks a From header, related to invocations of the ast_uri_decode function, and improper handling of (1) an empty const string and (2) a NULL pointer.
CVE-2008-2401
The Admin Server in Sun Java Active Server Pages (ASP) Server before 4.0.3 allows remote attackers to append to arbitrary new or existing files via the first argument to a certain file that is included by multiple unspecified ASP applications.
CVE-2008-2402
The Admin Server in Sun Java Active Server Pages (ASP) Server before 4.0.3 stores sensitive information under the web root with insufficient access control, which allows remote attackers to read password hashes and configuration data via direct requests for unspecified documents.
CVE-2008-2403
Multiple directory traversal vulnerabilities in unspecified ASP applications in Sun Java Active Server Pages (ASP) Server before 4.0.3 allow remote attackers to read or delete arbitrary files via a .. (dot dot) in the Path parameter to the MapPath method.
CVE-2008-2404
Stack-based buffer overflow in the request handling implementation in Sun Java Active Server Pages (ASP) Server before 4.0.3 allows remote attackers to execute arbitrary code via an unspecified string field.
CVE-2008-2405
Sun Java Active Server Pages (ASP) Server before 4.0.3 allows remote attackers to execute arbitrary commands via shell metacharacters in HTTP requests to unspecified ASP applications.
CVE-2008-2406
The administration application server in Sun Java Active Server Pages (ASP) Server before 4.0.3 allows remote attackers to bypass authentication via direct requests on TCP port 5102.
CVE-2008-2541
Multiple stack-based buffer overflows in the HTTP Gateway Service (icihttp.exe) in CA eTrust Secure Content Manager 8.0 allow remote attackers to execute arbitrary code or cause a denial of service via long FTP responses, related to (1) the file month field in a LIST command; (2) the PASV command; and (3) directories, files, and links in a LIST command.
CVE-2008-2547
Stack-based buffer overflow in msiexec.exe 3.1.4000.1823 and 4.5.6001.22159 in Microsoft Windows Installer allows context-dependent attackers to execute arbitrary code via a long GUID value for the /x (aka /uninstall) option.  NOTE: this issue might cross privilege boundaries if msiexec.exe is reachable via components such as ActiveX controls, and might additionally require a separate vulnerability in the control.
CVE-2008-2548
Stack-based buffer overflow in the JPEG thumbprint component in the EXIF parser on Motorola cell phones with RAZR firmware allows user-assisted remote attackers to execute arbitrary code via an MMS transmission of a malformed JPEG image, which triggers memory corruption.
CVE-2008-2549
Adobe Acrobat Reader 8.1.2 and earlier, and before 7.1.1, allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a malformed PDF document, as demonstrated by 2008-HI2.pdf.
CVE-2008-2550
Unspecified vulnerability in the Web Services Security component in IBM WebSphere Application Server (WAS) 6.1 before 6.1.0.17 has unknown impact and attack vectors related to an attribute in the SOAP security header.
CVE-2008-2551
The DownloaderActiveX Control (DownloaderActiveX.ocx) in Icona SpA C6 Messenger 1.0.0.1 allows remote attackers to force the download and execution of arbitrary files via a URL in the propDownloadUrl parameter with the propPostDownloadAction parameter set to "run."
