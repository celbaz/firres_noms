CVE-2007-1010
Multiple PHP remote file inclusion vulnerabilities in ZebraFeeds 1.0, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the zf_path parameter to (1) aggregator.php and (2) controller.php in newsfeeds/includes/.
CVE-2007-1011
PHP remote file inclusion vulnerability in functions_inc.php in VS-Gastebuch 1.5.3 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the gb_pfad parameter.
CVE-2007-1012
Cross-site scripting (XSS) vulnerability in faq.php in DeskPRO 1.1.0 allows remote attackers to inject arbitrary web script or HTML via the article parameter.
CVE-2007-1013
PHP remote file inclusion vulnerability in generate.php in VirtualSystem Htaccess Passwort Generator 1.1 allows remote attackers to execute arbitrary PHP code via a URL in the ht_pfad parameter.
CVE-2007-1014
Stack-based buffer overflow in VicFTPS before 5.0 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a long CWD command.
CVE-2007-1015
SQL injection vulnerability in HaberDetay.asp in Aktueldownload Haber script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-1016
SQL injection vulnerability in Aktueldownload Haber script allows remote attackers to execute arbitrary SQL commands via certain vectors related to the HaberDetay.asp and rss.asp components, and the id and kid parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.  NOTE: the combination of the HaberDetay.asp component and the id parameter is already covered by another February 2007 CVE candidate.
CVE-2007-1017
PHP remote file inclusion vulnerability in show_news_inc.php in VirtualSystem VS-News-System 1.2.1 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the newsordner parameter.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-1018
PHP remote file inclusion vulnerability in tpl/header.php in VirtualSystem VS-News-System 1.2.1 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the newsordner parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1019
SQL injection vulnerability in news.php in webSPELL 4.01.02, when register_globals is enabled, allows remote attackers to execute arbitrary SQL commands via the showonly parameter to index.php, a different vector than CVE-2006-5388.
Successful exploitation e.g. allows retrieval of password hashes, but requires that "register_globals" is enabled.
CVE-2007-1020
Cross-site scripting (XSS) vulnerability in index.php in CedStat 1.31 allows remote attackers to inject arbitrary web script or HTML via the hier parameter.
CVE-2007-1021
SQL injection vulnerability in inc_listnews.asp in CodeAvalanche News 1.x allows remote attackers to execute arbitrary SQL commands via the CAT_ID parameter.
CVE-2007-1022
SQL injection vulnerability in h_goster.asp in Turuncu Portal 1.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1023
SQL injection vulnerability in pop_profile.asp in Snitz Forums 2000 3.1 SR4 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-1024
PHP remote file inclusion vulnerability in include.php in Meganoide's news 1.1.1 allows remote attackers to execute arbitrary PHP code via a URL in the _SERVER[DOCUMENT_ROOT] parameter.
CVE-2007-1025
PHP remote file inclusion vulnerability in inc/functions_inc.php in VS-Link-Partner 2.1 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the gb_pfad, or possibly script_pfad, parameter.
CVE-2007-1026
SQL injection vulnerability in view.php in XLAtunes 0.1 and earlier allows remote attackers to execute arbitrary SQL commands via the album parameter in view mode.  NOTE: some of these details are obtained from third party information.
CVE-2007-1027
Certain setuid DB2 binaries in IBM DB2 before 9 Fix Pack 2 for Linux and Unix allow local users to overwrite arbitrary files via a symlink attack on the DB2DIAG.LOG temporary file.
CVE-2007-1028
Cross-site scripting (XSS) vulnerability in the Barry Jaspan Image Pager 4.7.x-1.x-dev and 5.x-1.x-dev before 2007-02-08 module for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to HTML entities and the IMG element.
CVE-2007-1029
Stack-based buffer overflow in the Connect method in the IMAP4 component in Quiksoft EasyMail Objects before 6.5 allows remote attackers to execute arbitrary code via a long host name.
CVE-2007-1030
Niels Provos libevent 1.2 and 1.2a allows remote attackers to cause a denial of service (infinite loop) via a DNS response containing a label pointer that references its own offset.
CVE-2007-1031
Directory traversal vulnerability in include/db_conn.php in SpoonLabs Vivvo Article Management CMS 3.4 allows remote attackers to include and execute arbitrary local files via the root parameter.
CVE-2007-1032
Unspecified vulnerability in phpMyFAQ 1.6.9 and earlier, when register_globals is enabled, allows remote attackers to "gain the privilege for uploading files on the server."
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-1033
Unspecified vulnerability in the Secure site 4.7.x-1.x-dev and 5.x-1.x-dev module for Drupal allows remote attackers to bypass access restrictions via a crafted URL.
CVE-2007-1034
SQL injection vulnerability in the category file in modules.php in the Emporium 2.3.0 and earlier module for PHP-Nuke allows remote attackers to execute arbitrary SQL commands via the category_id parameter.
CVE-2007-1035
Unspecified vulnerability in certain demonstration scripts in getID3 1.7.1, as used in the Mediafield and Audio modules for Drupal, allows remote attackers to read and delete arbitrary files, list arbitrary directories, and write to empty files or .mp3 files via unknown vectors.
This vulnerability affects the following versions of Drupal Mediafield Module:
Drupal, Mediafield Module, 4.7.x-1.x-dev
Drupal, Mediafield Module, 5.x-1.x-dev

This vulnerability affects the following versions of Drupal Audio Module:
Drupal, Audio Module, 4.7.x-1.x-dev
Drupal, Audio Module, 5.x-0.2
Drupal, Audio Module, 5.x-0.x-dev

CVE-2007-1036
The default configuration of JBoss does not restrict access to the (1) console and (2) web management interfaces, which allows remote attackers to bypass authentication and gain administrative access via direct requests.
CVE-2007-1037
Stack-based buffer overflow in News File Grabber 4.1.0.1 and earlier allows remote attackers to execute arbitrary code via a .nzb file with a long subject field.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1038
Shemes.com Grabit 1.5.3, and possibly earlier, allows remote attackers to cause a denial of service (application crash) via a .nzb file with a subject field containing ';' (semicolon) characters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1039
Unspecified vulnerability in Peanut Knowledge Base (PeanutKB) 0.0.3 and earlier has unknown impact and attack vectors.
CVE-2007-1040
Directory traversal vulnerability in archives.php in Xpression News (X-News) 1.0.1 allows remote attackers to include arbitrary files or obtain sensitive information via a .. (dot dot) in the xnews-template parameter.
CVE-2007-1041
Multiple stack-based buffer overflows in S&H Computer Systems News Rover 12.1 Rev 1 allow remote attackers to execute arbitrary code via a .nzb file with a long (1) group or (2) subject string.
CVE-2007-1042
Directory traversal vulnerability in news.php in Xpression News (X-News) 1.0.1, when magic_quotes_gpc is disabled, allows remote attackers to include arbitrary files or obtain sensitive information via a .. (dot dot) in the xnews-template parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1043
Ezboo webstats, possibly 3.0.3, allows remote attackers to bypass authentication and gain access via a direct request to (1) update.php and (2) config.php.
CVE-2007-1044
Pearson Education PowerSchool 4.3.6 allows remote attackers to list the contents of the admin folder via a URI composed of the admin/ directory name and an arbitrary filename ending in ".js."  NOTE: it was later reported that this issue had been addressed by 5.1.2.
CVE-2007-1045
mAlbum 0.3 has default accounts (1) "login"/"pass" for its administrative account and (2) "dqsfg"/"sdfg", which allows remote attackers to gain privileges.
mAlbum should reconfigure their administrative login and password from their default values.
CVE-2007-1046
Dem_trac allows remote attackers to read log file contents via a direct request for /anc_sit.txt.
CVE-2007-1047
Unspecified vulnerability in Distributed Checksum Clearinghouse (DCC) before 1.3.51 allows remote attackers to delete or add hosts in /var/dcc/maps.
CVE-2007-1048
PHP remote file inclusion vulnerability in admin_rebuild_search.php in phpbb_wordsearch allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-1049
Cross-site scripting (XSS) vulnerability in the wp_explain_nonce function in the nonce AYS functionality (wp-includes/functions.php) for WordPress 2.0 before 2.0.9 and 2.1 before 2.1.1 allows remote attackers to inject arbitrary web script or HTML via the file parameter to wp-admin/templates.php, and possibly other vectors involving the action variable.
CVE-2007-1050
Multiple cross-site scripting (XSS) vulnerabilities in index.php in AbleDesign MyCalendar allow remote attackers to inject arbitrary web script or HTML via (1) the go parameter, (2) the keyword parameter in the search menu (go=search), or (3) the username or (4) the password in a go=Login action.
CVE-2007-1051
Comodo Firewall Pro (formerly Comodo Personal Firewall) 2.4.17.183 and earlier uses a weak cryptographic hashing function (CRC32) to identify trusted modules, which allows local users to bypass security protections by substituting modified modules that have the same CRC32 value.
CVE-2007-1052
** DISPUTED **  PHP remote file inclusion vulnerability in index.php in PBLang (PBL) 4.60 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the dbpath parameter, a different vector than CVE-2006-5062.  NOTE: this issue has been disputed by a reliable third party for 4.65, stating that the dbpath variable is initialized in an included file that is created upon installation.
CVE-2007-1053
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in phpXmms 1.0 allow remote attackers to execute arbitrary PHP code via a URL in the tcmdp parameter to (1) phpxmmsb.php or (2) phpxmmst.php.  NOTE: this issue has been disputed by a reliable third party, stating that the tcmdp variable is initialized by config.php.
CVE-2007-1054
Cross-site scripting (XSS) vulnerability in the AJAX features in index.php in MediaWiki 1.6.x through 1.9.2, when $wgUseAjax is enabled, allows remote attackers to inject arbitrary web script or HTML via a UTF-7 encoded value of the rs parameter, which is processed by Internet Explorer.
Successful exploitation requires that "$wgUseAjax" is enabled
CVE-2007-1055
Cross-site scripting (XSS) vulnerability in the AJAX features in index.php in MediaWiki 1.9.x before 1.9.0rc2, and 1.8.2 and earlier allows remote attackers to inject arbitrary web script or HTML via the rs parameter.  NOTE: this issue might be a duplicate of CVE-2007-0177.
CVE-2007-1056
VMware Workstation 5.5.3 build 34685 does not provide per-user restrictions on certain privileged actions, which allows local users to perform restricted operations such as changing system time, accessing hardware components, and stopping the "VMware tools service" service. NOTE: exploitation is simplified via (1) weak file permissions (Users = Read & Execute) for %PROGRAMFILES%\VMware; and weak registry key permissions (access by Users) for (2) vmmouse, (3) vmscsi, (4) VMTools, (5) vmx_svga, and (6) vmxnet in HKLM\SYSTEM\CurrentControlSet\Services\; which allows local users to perform various privileged actions outside of the guest OS by executing certain files under %PROGRAMFILES%\VMware\VMware Tools, as demonstrated by (a) VMControlPanel.cpl and (b) vmwareservice.exe.
CVE-2007-1057
The Net Direct client for Linux before 6.0.5 in Nortel Application Switch 2424, VPN 3050 and 3070, and SSL VPN Module 1000 extracts and executes files with insecure permissions, which allows local users to exploit a race condition to replace a world-writable file in /tmp/NetClient and cause another user to execute arbitrary code when attempting to execute this client, as demonstrated by replacing /tmp/NetClient/client.
CVE-2007-1058
SQL injection vulnerability in user_pages/page.asp in Online Web Building 2.0 allows remote attackers to execute arbitrary SQL commands via the art_id parameter.
CVE-2007-1070
Multiple stack-based buffer overflows in Trend Micro ServerProtect for Windows and EMC 5.58, and for Network Appliance Filer 5.61 and 5.62, allow remote attackers to execute arbitrary code via crafted RPC requests to TmRpcSrv.dll that trigger overflows when calling the (1) CMON_NetTestConnection, (2) CMON_ActiveUpdate, and (3) CMON_ActiveRollback functions in (a) StCommon.dll, and (4) ENG_SetRealTimeScanConfigInfo and (5) ENG_SendEMail functions in (b) eng50.dll.
