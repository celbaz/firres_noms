CVE-2012-5500
The batch id change script (renameObjectsByPaths.py) in Plone before 4.2.3 and 4.3 before beta 1 allows remote attackers to change the titles of content items by leveraging a valid CSRF token in a crafted request.
CVE-2012-5508
The error pages in Plone before 4.2.3 and 4.3 before beta 1 allow remote attackers to obtain random numbers and derive the PRNG state for password resets via unspecified vectors.  NOTE: this identifier was SPLIT per ADT2 due to different vulnerability types. CVE-2012-6661 was assigned for the PRNG reseeding issue in Zope.
CVE-2012-6661
Zope before 2.13.19, as used in Plone before 4.2.3 and 4.3 before beta 1, does not reseed the pseudo-random number generator (PRNG), which makes it easier for remote attackers to guess the value via unspecified vectors.  NOTE: this issue was SPLIT from CVE-2012-5508 due to different vulnerability types (ADT2).
CVE-2013-0336
The ipapwd_chpwop function in daemons/ipa-slapi-plugins/ipa-pwd-extop/ipa_pwd_extop.c in the directory server (dirsrv) in FreeIPA before 3.2.0 allows remote attackers to cause a denial of service (crash) via a connection request without a username/dn, related to the 389 directory server.
CVE-2014-0204
OpenStack Identity (Keystone) before 2014.1.1 does not properly handle when a role is assigned to a group that has the same ID as a user, which allows remote authenticated users to gain privileges that are assigned to a group with the same ID.
CVE-2014-0487
APT before 1.0.9 does not verify downloaded files if they have been modified as indicated using the If-Modified-Since header, which has unspecified impact and attack vectors.
CVE-2014-0488
APT before 1.0.9 does not "invalidate repository data" when moving from an unauthenticated to authenticated state, which allows remote attackers to have unspecified impact via crafted repository data.
CVE-2014-0489
APT before 1.0.9, when the Acquire::GzipIndexes option is enabled, does not validate checksums, which allows remote attackers to execute arbitrary code via a crafted package.
CVE-2014-0490
The apt-get download command in APT before 1.0.9 does not properly validate signatures for packages, which allows remote attackers to execute arbitrary code via a crafted package.
CVE-2014-3654
Multiple cross-site scripting (XSS) vulnerabilities in spacewalk-java 2.0.2 in Spacewalk and Red Hat Network (RHN) Satellite 5.5 and 5.6 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors to (1) kickstart/cobbler/CustomSnippetList.do, (2) channels/software/Entitlements.do, or (3) admin/multiorg/OrgUsers.do.
CVE-2014-3712
Katello allows remote attackers to cause a denial of service (memory consumption) via the (1) mode parameter in the setup_utils function in content_search_controller.rb or (2) action parameter in the respond function in api/api_controller.rb in app/controllers/katello/, which is passed to the to_sym method.
CVE-2014-5271
Heap-based buffer overflow in the encode_slice function in libavcodec/proresenc_kostya.c in FFMpeg before 1.1.14, 1.2.x before 1.2.8, 2.x before 2.2.7, and 2.3.x before 2.3.3 and Libav before 10.5 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via unspecified vectors.
CVE-2014-5272
libavcodec/iff.c in FFMpeg before 1.1.14, 1.2.x before 1.2.8, 2.2.x before 2.2.7, and 2.3.x before 2.3.2 allows remote attackers to have unspecified impact via a crafted iff image, which triggers an out-of-bounds array access, related to the rgb8 and rgbn formats.
CVE-2014-5507
iBackup 10.0.0.32 and earlier uses weak permissions (Everyone: Full Control) for ib_service.exe, which allows local users to gain privileges via a Trojan horse file.
CVE-2014-7228
Akeeba Restore (restore.php), as used in Joomla! 2.5.4 through 2.5.25, 3.x through 3.2.5, and 3.3.0 through 3.3.4; Akeeba Backup for Joomla! Professional 3.0.0 through 4.0.2; Backup Professional for WordPress 1.0.b1 through 1.1.3; Solo 1.0.b1 through 1.1.2; Admin Tools Core and Professional 2.0.0 through 2.4.4; and CMS Update 1.0.a1 through 1.0.1, when performing a backup or update for an archive, does not delete parameters from $_GET and $_POST when it is cleansing $_REQUEST, but later accesses $_GET and $_POST using the getQueryParam function, which allows remote attackers to bypass encryption and execute arbitrary code via a command message that extracts a crafted archive.
CVE-2014-8080
The REXML parser in Ruby 1.9.x before 1.9.3-p550, 2.0.x before 2.0.0-p594, and 2.1.x before 2.1.4 allows remote attackers to cause a denial of service (memory consumption) via a crafted XML document, aka an XML Entity Expansion (XEE) attack.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-8350
Smarty before 3.1.21 allows remote attackers to bypass the secure mode restrictions and execute arbitrary PHP code as demonstrated by "{literal}<{/literal}script language=php>" in a template.
CVE-2014-8494
ESTsoft ALUpdate 8.5.1.0.0 uses weak permissions (Users: Full Control) for the (1) AlUpdate folder and (2) AlUpdate.exe, which allows local users to gain privileges via a Trojan horse file.
