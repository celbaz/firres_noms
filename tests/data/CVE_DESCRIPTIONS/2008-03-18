CVE-2008-0044
Multiple buffer overflows in AFP Client in Apple Mac OS X 10.4.11 and 10.5.2 allow remote attackers to cause a denial of service (application termination) and execute arbitrary code via a crafted afp:// URL.
CVE-2008-0045
Unspecified vulnerability in AFP Server in Apple Mac OS X 10.4.11 allows remote attackers to bypass cross-realm authentication via unknown manipulations of Kerberos principal realm names.
CVE-2008-0046
The Application Firewall in Apple Mac OS X 10.5.2 has an incorrect German translation for the "Set access for specific services and applications" radio button that might cause the user to believe that the button is used to restrict access only to specific services and applications, which might allow attackers to bypass intended access restrictions.
CVE-2008-0047
Heap-based buffer overflow in the cgiCompileSearch function in CUPS 1.3.5, and other versions including the version bundled with Apple Mac OS X 10.5.2, when printer sharing is enabled, allows remote attackers to execute arbitrary code via crafted search expressions.
CVE-2008-0048
Stack-based buffer overflow in AppKit in Apple Mac OS X 10.4.11 allows context-dependent attackers to execute arbitrary code via the a long file name to the NSDocument API.
CVE-2008-0049
AppKit in Apple Mac OS X 10.4.11 inadvertently makes an NSApplication mach port available for inter-process communication instead of inter-thread communication, which allows local users to execute arbitrary code via crafted messages to privileged applications.
CVE-2008-0050
CFNetwork in Apple Mac OS X 10.4.11 allows remote HTTPS proxy servers to spoof secure websites via data in a 502 Bad Gateway error.
CVE-2008-0051
Integer overflow in CoreFoundation in Apple Mac OS X 10.4.11 might allow local users to execute arbitrary code via crafted time zone data.
CVE-2008-0052
CoreServices in Apple Mac OS X 10.4.11 treats .ief as a safe file type, which allows remote attackers to force Safari users into opening an .ief file in AppleWorks, even when the "Open 'Safe' files" preference is set.
CVE-2008-0053
Multiple buffer overflows in the HP-GL/2-to-PostScript filter in CUPS before 1.3.6 might allow remote attackers to execute arbitrary code via a crafted HP-GL/2 file.
CVE-2008-0054
Foundation in Apple Mac OS X 10.4.11 might allow context-dependent attackers to execute arbitrary code via a malformed selector name to the NSSelectorFromString API, which causes an "unexpected selector" to be used.
CVE-2008-0055
Foundation in Apple Mac OS X 10.4.11 creates world-writable directories while NSFileManager copies files recursively and only modifies the permissions afterward, which allows local users to modify copied files to cause a denial of service and possibly gain privileges.
CVE-2008-0056
Stack-based buffer overflow in Foundation in Apple Mac OS X 10.4.11 allows context-dependent attackers to execute arbitrary code via a "long pathname with an unexpected structure" that triggers the overflow in NSFileManager.
CVE-2008-0057
Multiple integer overflows in a "legacy serialization format" parser in AppKit in Apple Mac OS X 10.4.11 allows remote attackers to execute arbitrary code via a crafted serialized property list.
CVE-2008-0058
Race condition in the NSURLConnection cache management functionality in Foundation for Apple Mac OS X 10.4.11 allows remote attackers to execute arbitrary code via unspecified manipulations that cause messages to be sent to a deallocated object.
CVE-2008-0059
Race condition in NSXML in Foundation for Apple Mac OS X 10.4.11 allows context-dependent attackers to execute arbitrary code via a crafted XML file, related to "error handling logic."
CVE-2008-0060
Help Viewer in Apple Mac OS X 10.4.11 and 10.5.2 allows remote attackers to execute arbitrary Applescript via a help:topic_list URL that injects HTML or JavaScript into a topic list page, as demonstrated using a help:runscript link.
CVE-2008-0727
Multiple buffer overflows in oninit.exe in IBM Informix Dynamic Server (IDS) 7.x through 11.x allow (1) remote attackers to execute arbitrary code via a long password and (2) remote authenticated users to execute arbitrary code via a long DBPATH value.
All IBM links require software support sign in to view.
CVE-2008-0949
Unspecified vulnerability in IBM Informix Dynamic Server (IDS) 7.x through 11.x allows remote attackers to gain privileges via a malformed connection request packet.
IBM links require software support sign in to access information.
CVE-2008-0987
Stack-based buffer overflow in Image Raw in Apple Mac OS X 10.5.2, and Digital Camera RAW Compatibility before Update 2.0 for Aperture 2 and iPhoto 7.1.2, allows remote attackers to execute arbitrary code via a crafted Adobe Digital Negative (DNG) image.
CVE-2008-0988
Off-by-one error in the Libsystem strnstr API in libc on Apple Mac OS X 10.4.11 allows context-dependent attackers to cause a denial of service (crash) via crafted arguments that trigger a buffer over-read.
CVE-2008-0989
Format string vulnerability in mDNSResponderHelper in Apple Mac OS X 10.5.2 allows local users to execute arbitrary code via format string specifiers in the local hostname.
CVE-2008-0990
notifyd in Apple Mac OS X 10.4.11 does not verify that Mach port death notifications have originated from the kernel, which allows local users to cause a denial of service via spoofed death notifications that prevent other applications from receiving notifications.
CVE-2008-0992
Array index error in pax in Apple Mac OS X 10.5.2 allows context-dependent attackers to execute arbitrary code via an archive with a crafted length value.
CVE-2008-0993
Podcast Capture in Podcast Producer for Apple Mac OS X 10.5.2 invokes a subtask with passwords in command line arguments, which allows local users to read the passwords via process listings.
CVE-2008-0994
Preview in Apple Mac OS X 10.5.2 uses 40-bit RC4 when saving a PDF file with encryption, which makes it easier for attackers to decrypt the file via brute force methods.
CVE-2008-0995
The Printing component in Apple Mac OS X 10.5.2 uses 40-bit RC4 when printing to an encrypted PDF file, which makes it easier for attackers to decrypt the file via brute force methods.
CVE-2008-0996
The Printing component in Apple Mac OS X 10.5.2 might save authentication credentials to disk when starting a job on an authenticated print queue, which might allow local users to obtain the credentials.
CVE-2008-0997
Stack-based buffer overflow in AppKit in Apple Mac OS X 10.4.11 allows user-assisted remote attackers to cause a denial of service (application termination) and execute arbitrary code via a crafted PostScript Printer Description (PPD) file that is not properly handled when querying a network printer.
CVE-2008-0998
Unspecified vulnerability in NetCfgTool in the System Configuration component in Apple Mac OS X 10.4.11 and 10.5.2 allows local users to bypass authorization and execute arbitrary code via crafted distributed objects.
CVE-2008-0999
Apple Mac OS X 10.5.2 allows user-assisted attackers to cause a denial of service (crash) via a crafted Universal Disc Format (UDF) disk image, which triggers a NULL pointer dereference.
CVE-2008-1000
Directory traversal vulnerability in ContentServer.py in the Wiki Server in Apple Mac OS X 10.5.2 (aka Leopard) allows remote authenticated users to write arbitrary files via ".." sequences in file attachments.
CVE-2008-1330
Unspecified vulnerability in the Windows client API in Novell GroupWise 7 before SP3 and 6.5 before SP6 Update 3 allows remote authenticated users to access the non-shared stored e-mail messages of another user who has shared at least one folder with the attacker.
CVE-2008-1368
CRLF injection vulnerability in Microsoft Internet Explorer 5 and 6 allows remote attackers to execute arbitrary FTP commands via an ftp:// URL that contains a URL-encoded CRLF (%0D%0A) before the FTP command, which causes the commands to be inserted into an authenticated FTP connection established earlier in the same browser session, as demonstrated using a DELE command, a variant or possibly a regression of CVE-2004-1166.  NOTE: a trailing "//" can force Internet Explorer to try to reuse an existing authenticated connection.
CVE-2008-1369
A certain incorrect Sun Solaris 10 image on SPARC Enterprise T5120 and T5220 servers has /etc/default/login and /etc/ssh/sshd_config files that configure root logins in a manner unintended by the vendor, which allows remote attackers to gain privileges via unspecified vectors.
Sun link will not execute.
CVE-2008-1370
PHP remote file inclusion vulnerability in index.php in wildmary Yap Blog 1.1 allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-1371
Absolute path traversal vulnerability in install/index.php in Drake CMS 0.4.11 RC8 allows remote attackers to read and execute arbitrary files via a full pathname in the d_root parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE description identifies vulnerability as remote attacker, but both links describe vulnerability as local-file inclusion.
CVE-2008-1372
bzlib.c in bzip2 before 1.0.5 allows user-assisted remote attackers to cause a denial of service (crash) via a crafted file that triggers a buffer over-read, as demonstrated by the PROTOS GENOME test suite for Archive Formats.
CVE-2008-1383
The docert function in ssl-cert.eclass, when used by src_compile or src_install on Gentoo Linux, stores the SSL key in a binpkg, which allows local users to extract the key from the binpkg, and causes multiple systems that use this binpkg to have the same SSL key and certificate.
