CVE-2015-3943
Advantech WebAccess before 8.1 allows remote attackers to read sensitive cleartext information about e-mail project accounts via unspecified vectors.
CVE-2015-3946
Cross-site request forgery (CSRF) vulnerability in Advantech WebAccess before 8.1 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2015-3947
SQL injection vulnerability in Advantech WebAccess before 8.1 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-3948
Cross-site scripting (XSS) vulnerability in Advantech WebAccess before 8.1 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-5007
Cross-site request forgery (CSRF) vulnerability in IBM WebSphere Commerce 6.0 through 6.0.0.11, 7.0 through 7.0.0.9, and 7.0 Feature Pack 8 allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2015-6314
Cisco Wireless LAN Controller (WLC) devices with software 7.6.x, 8.0 before 8.0.121.0, and 8.1 before 8.1.131.0 allow remote attackers to change configuration settings via unspecified vectors, aka Bug ID CSCuw06153.
CVE-2015-6320
The IP ingress packet handler on Cisco Aironet 1800 devices with software 8.1(112.3) and 8.1(112.4) allows remote attackers to cause a denial of service via a crafted header in an IP packet, aka Bug ID CSCuv63138.
CVE-2015-6323
The Admin portal in Cisco Identity Services Engine (ISE) 1.1.x, 1.2.0 before patch 17, 1.2.1 before patch 8, 1.3 before patch 5, and 1.4 before patch 4 allows remote attackers to obtain administrative access via unspecified vectors, aka Bug ID CSCuw34253.
CVE-2015-6336
Cisco Aironet 1800 devices with software 7.2, 7.3, 7.4, 8.1(112.3), 8.1(112.4), and 8.1(15.14) have a default account, which makes it easier for remote attackers to obtain access via unspecified vectors, aka Bug ID CSCuw58062.
CVE-2015-6423
The DCERPC Inspection implementation in Cisco Adaptive Security Appliance (ASA) Software 9.4.1 through 9.5.1 allows remote authenticated users to bypass an intended DCERPC-only ACL by sending arbitrary network traffic, aka Bug ID CSCuu67782.
CVE-2015-6467
Advantech WebAccess before 8.1 allows remote attackers to execute arbitrary code via vectors involving a browser plugin.
CVE-2015-8279
Web Viewer 1.0.0.193 on Samsung SRN-1670D devices allows remote attackers to read arbitrary files via a request to an unspecified PHP script.
CVE-2015-8280
Web Viewer 1.0.0.193 on Samsung SRN-1670D devices allows remote attackers to discover credentials by reading detailed error messages.
CVE-2015-8281
Web Viewer 1.0.0.193 on Samsung SRN-1670D devices allows attackers to bypass filesystem encryption via XOR calculations.
CVE-2015-8675
Huawei S5300 Campus Series switches with software before V200R005SPH008 do not mask the password when uploading files, which allows physically proximate attackers to obtain sensitive password information by reading the display.
CVE-2015-8685
Multiple cross-site scripting (XSS) vulnerabilities in Dolibarr ERP/CRM 3.8.3 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) external calendar url or (2) the bank name field in the "import external calendar" page.
CVE-2015-8688
Gajim before 0.16.5 allows remote attackers to modify the roster and intercept messages via a crafted roster-push IQ stanza.
CVE-2015-8749
The volume_utils._parse_volume_info function in OpenStack Compute (Nova) before 2015.1.3 (kilo) and 12.0.x before 12.0.1 (liberty) includes the connection_info dictionary in the StorageError message when using the Xen backend, which might allow attackers to obtain sensitive password information by reading log files or other unspecified vectors.
CVE-2016-0851
Advantech WebAccess before 8.1 allows remote attackers to cause a denial of service (out-of-bounds memory access) via unspecified vectors.
CVE-2016-0852
Advantech WebAccess before 8.1 allows remote attackers to bypass an intended administrative requirement and obtain file or folder access via unspecified vectors.
CVE-2016-0853
Advantech WebAccess before 8.1 allows remote attackers to obtain sensitive information via crafted input.
CVE-2016-0854
Unrestricted file upload vulnerability in the uploadImageCommon function in the UploadAjaxAction script in the WebAccess Dashboard Viewer in Advantech WebAccess before 8.1 allows remote attackers to write to files of arbitrary types via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2016-0855
Directory traversal vulnerability in Advantech WebAccess before 8.1 allows remote attackers to list arbitrary virtual-directory files via unspecified vectors.
CVE-2016-0856
Multiple stack-based buffer overflows in Advantech WebAccess before 8.1 allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2016-0857
Multiple heap-based buffer overflows in Advantech WebAccess before 8.1 allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2016-0858
Race condition in Advantech WebAccess before 8.1 allows remote attackers to execute arbitrary code or cause a denial of service (buffer overflow) via a crafted request.
CVE-2016-0859
Integer overflow in the Kernel service in Advantech WebAccess before 8.1 allows remote attackers to execute arbitrary code or cause a denial of service (stack-based buffer overflow) via a crafted RPC request.
CVE-2016-0860
Buffer overflow in the BwpAlarm subsystem in Advantech WebAccess before 8.1 allows remote attackers to cause a denial of service via a crafted RPC request.
CVE-2016-1256
Juniper Junos OS before 12.1X44-D55, 12.1X46 before 12.1X46-D40, 12.1X47 before 12.1X47-D25, 12.3 before 12.3R10, 12.3X48 before 12.3X48-D20, 13.2 before 13.2R8, 13.2X51 before 13.2X51-D40, 13.3 before 13.3R7, 14.1 before 14.1R5, 14.1X53 before 14.1X53-D18 or 14.1X53-D30, 14.1X55 before 14.1X55-D25, 14.2 before 14.2R4, 15.1 before 15.1R2, and 15.1X49 before 15.1X49-D10 allow remote attackers to cause a denial of service via a malformed IGMPv3 packet, aka a "multicast denial of service."
CVE-2016-1257
The Routing Engine in Juniper Junos OS 13.2R5 through 13.2R8, 13.3R1 before 13.3R8, 13.3R7 before 13.3R7-S3, 14.1R1 before 14.1R6, 14.1R3 before 14.1R3-S9, 14.1R4 before 14.1R4-S7, 14.1X51 before 14.1X51-D65, 14.1X53 before 14.1X53-D12, 14.1X53 before 14.1X53-D28, 14.1X53 before 4.1X53-D35, 14.2R1 before 14.2R5, 14.2R3 before 14.2R3-S4, 14.2R4 before 14.2R4-S1, 15.1 before 15.1R3, 15.1F2 before 15.1F2-S2, and 15.1X49 before 15.1X49-D40, when LDP is enabled, allows remote attackers to cause a denial of service (RPD routing process crash) via a crafted LDP packet.
CVE-2016-1258
Embedthis Appweb, as used in J-Web in Juniper Junos OS before 12.1X44-D60, 12.1X46 before 12.1X46-D45, 12.1X47 before 12.1X47-D30, 12.3 before 12.3R10, 12.3X48 before 12.3X48-D20, 13.2X51 before 13.2X51-D20, 13.3 before 13.3R8, 14.1 before 14.1R6, and 14.2 before 14.2R5, allows remote attackers to cause a denial of service (J-Web crash) via unspecified vectors.
CVE-2016-1260
Juniper Junos OS before 13.2X51-D36, 14.1X53 before 14.1X53-D25, and 15.2 before 15.2R1 on EX4300 series switches allow remote attackers to cause a denial of service (network loop and bandwidth consumption) via unspecified vectors related to Spanning Tree Protocol (STP) traffic.
CVE-2016-1262
Juniper Junos OS before 12.1X46-D45, 12.1X47 before 12.1X47-D30, 12.1X48 before 12.3X48-D20, and 15.1X49 before 15.1X49-D30 on SRX series devices, when the Real Time Streaming Protocol Application Layer Gateway (RTSP ALG) is enabled, allow remote attackers to cause a denial of service (flowd crash) via a crafted RTSP packet.
CVE-2016-1897
FFmpeg 2.x allows remote attackers to conduct cross-origin attacks and read arbitrary files by using the concat protocol in an HTTP Live Streaming (HLS) M3U8 file, leading to an external HTTP request in which the URL string contains the first line of a local file.
CVE-2016-1898
FFmpeg 2.x allows remote attackers to conduct cross-origin attacks and read arbitrary files by using the subfile protocol in an HTTP Live Streaming (HLS) M3U8 file, leading to an external HTTP request in which the URL string contains an arbitrary line of a local file.
CVE-2016-1909
Fortinet FortiAnalyzer before 5.0.12 and 5.2.x before 5.2.5; FortiSwitch 3.3.x before 3.3.3; FortiCache 3.0.x before 3.0.8; and FortiOS 4.1.x before 4.1.11, 4.2.x before 4.2.16, 4.3.x before 4.3.17 and 5.0.x before 5.0.8 have a hardcoded passphrase for the Fortimanager_Access account, which allows remote attackers to obtain administrative access via an SSH session.
CVE-2016-1910
The User Management Engine (UME) in SAP NetWeaver 7.4 allows attackers to decrypt unspecified data via unknown vectors, aka SAP Security Note 2191290.
CVE-2016-1911
Multiple cross-site scripting (XSS) vulnerabilities in SAP NetWeaver 7.4 allow remote attackers to inject arbitrary web script or HTML via vectors related to the (1) Runtime Workbench (RWB) or (2) Pmitest servlet in the Process Monitoring Infrastructure (PMI), aka SAP Security Notes 2206793 and 2234918.
CVE-2016-1912
Multiple cross-site scripting (XSS) vulnerabilities in Dolibarr ERP/CRM 3.8.3 allow remote authenticated users to inject arbitrary web script or HTML via the (1) lastname, (2) firstname, (3) email, (4) job, or (5) signature parameter to htdocs/user/card.php.
CVE-2016-1913
Multiple cross-site scripting (XSS) vulnerabilities in the Redhen module 7.x-1.x before 7.x-1.11 for Drupal allow remote authenticated users with certain access to inject arbitrary web script or HTML via unspecified vectors, related to (1) individual contacts, (2) notes, or (3) engagement scores.
