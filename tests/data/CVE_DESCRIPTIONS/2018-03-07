CVE-2014-5044
Multiple integer overflows in libgfortran might allow remote attackers to execute arbitrary code or cause a denial of service (Fortran application crash) via vectors related to array allocation.
CVE-2014-8780
Cross-site scripting (XSS) vulnerability in Jease 2.11 allows remote authenticated users to inject arbitrary web script or HTML via a content section note.
CVE-2016-5179
Chrome OS before 53.0.2785.144 allows remote attackers to execute arbitrary commands at boot.
CVE-2016-7443
Exponent CMS 2.3.0 through 2.3.9 allows remote attackers to have unspecified impact via vectors related to "uploading files to wrong location."
CVE-2017-11649
Cross-site request forgery (CSRF) vulnerability in DrayTek Vigor AP910C devices with firmware 1.2.0_RC3 build r6594 allows remote attackers to hijack the authentication of unspecified users for requests that enable SNMP on the remote device via vectors involving goform/setSnmp.
CVE-2017-11650
Cross-site scripting (XSS) vulnerability in DrayTek Vigor AP910C devices with firmware 1.2.0_RC3 build r6594 allows remote attackers to inject arbitrary web script or HTML via vectors involving home.asp.
CVE-2017-12174
It was found that when Artemis and HornetQ before 2.4.0 are configured with UDP discovery and JGroups discovery a huge byte array is created when receiving an unexpected multicast message. This may result in a heap memory exhaustion, full GC, or OutOfMemoryError.
CVE-2017-15367
Bacula-web before 8.0.0-rc2 is affected by multiple SQL Injection vulnerabilities that could allow an attacker to access the Bacula database and, depending on configuration, escalate privileges on the server.
CVE-2017-18221
The __munlock_pagevec function in mm/mlock.c in the Linux kernel before 4.11.4 allows local users to cause a denial of service (NR_MLOCK accounting corruption) via crafted use of mlockall and munlockall system calls.
CVE-2018-1000116
NET-SNMP version 5.7.2 contains a heap corruption vulnerability in the UDP protocol handler that can result in command execution.
CVE-2018-1000117
Python Software Foundation CPython version From 3.2 until 3.6.4 on Windows contains a Buffer Overflow vulnerability in os.symlink() function on Windows that can result in Arbitrary code execution, likely escalation of privilege. This attack appears to be exploitable via a python script that creates a symlink with an attacker controlled name or location. This vulnerability appears to have been fixed in 3.7.0 and 3.6.5.
CVE-2018-1000118
Github Electron version Electron 1.8.2-beta.4 and earlier contains a Command Injection vulnerability in Protocol Handler that can result in command execute. This attack appear to be exploitable via the victim opening an electron protocol handler in their browser. This vulnerability appears to have been fixed in Electron 1.8.2-beta.5. This issue is due to an incomplete fix for CVE-2018-1000006, specifically the black list used was not case insensitive allowing an attacker to potentially bypass it.
CVE-2018-1000119
Sinatra rack-protection versions 1.5.4 and 2.0.0.rc3 and earlier contains a timing attack vulnerability in the CSRF token checking that can result in signatures can be exposed. This attack appear to be exploitable via network connectivity to the ruby application. This vulnerability appears to have been fixed in 1.5.5 and 2.0.0.
CVE-2018-1054
An out-of-bounds memory read flaw was found in the way 389-ds-base handled certain LDAP search filters, affecting all versions including 1.4.x. A remote, unauthenticated attacker could potentially use this flaw to make ns-slapd crash via a specially crafted LDAP request, thus resulting in denial of service.
CVE-2018-5452
A Stack-based Buffer Overflow issue was discovered in Emerson Process Management ControlWave Micro Process Automation Controller: ControlWave Micro [ProConOS v.4.01.280] firmware: CWM v.05.78.00 and prior. A stack-based buffer overflow vulnerability caused by sending crafted packets on Port 20547 could force the PLC to change its state into halt mode.
CVE-2018-7204
inc/logger.php in the Giribaz File Manager plugin before 5.0.2 for WordPress logged activity related to the plugin in /wp-content/uploads/file-manager/log.txt. If a user edits the wp-config.php file using this plugin, the wp-config.php contents get added to log.txt, which is not protected and contains database credentials, salts, etc. These files have been indexed by Google and a simple dork will find affected sites.
CVE-2018-7473
Open redirect vulnerability in the SO Connect SO WIFI hotspot web interface, prior to version 140, allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL.
CVE-2018-7564
Stored XSS exists on Polycom QDX 6000 devices.
CVE-2018-7565
CSRF exists on Polycom QDX 6000 devices.
CVE-2018-7675
In NetIQ Sentinel before 8.1.x, a Sentinel user is logged into the Sentinel Web Interface. After performing some tasks within Sentinel the user does not log out but does go idle for a period of time. This in turn causes the interface to timeout so that it requires the user to re-authenticate. If another user is passing by and decides to login, their credentials are accepted. While The user does not inherit any of the other users privileges, they are able to view the previous screen. In this case it is possible that the user can see another users events or configuration information for whatever view is currently showing.
CVE-2018-7720
A cross-site request forgery (CSRF) vulnerability exists in Western Bridge Cobub Razor 0.7.2 via /index.php?/user/createNewUser/, resulting in account creation.
CVE-2018-7721
Cross Site Scripting (XSS) exists in MetInfo 6.0.0 via /feedback/index.php because app/system/feedback/web/feedback.class.php mishandles input data.
CVE-2018-7738
In util-linux before 2.32-rc1, bash-completion/umount allows local users to gain privileges by embedding shell commands in a mountpoint name, which is mishandled during a umount command (within Bash) by a different user, as demonstrated by logging in as root and entering umount followed by a tab character for autocompletion.
CVE-2018-7739
antsle antman before 0.9.1a allows remote attackers to bypass authentication via invalid characters in the username and password parameters, as demonstrated by a username=>&password=%0a string to the /login URI. This allows obtaining root permissions within the web management console, because the login process uses Java's ProcessBuilder class and a bash script called antsle-auth with insufficient input validation.
CVE-2018-7740
The resv_map_release function in mm/hugetlb.c in the Linux kernel through 4.15.7 allows local users to cause a denial of service (BUG) via a crafted application that makes mmap system calls and has a large pgoff argument to the remap_file_pages system call.
CVE-2018-7741
Eramba e1.0.6.033 has Reflected XSS in the Date Filter via the created parameter to the /crons URI.
CVE-2018-7745
An issue was discovered in Western Bridge Cobub Razor 0.7.2. Authentication is not required for /index.php?/install/installation/createuserinfo requests, resulting in account creation.
CVE-2018-7746
An issue was discovered in Western Bridge Cobub Razor 0.7.2. Authentication is not required for /index.php?/manage/channel/modifychannel. For example, with a crafted channel name, stored XSS is triggered during a later /index.php?/manage/channel request by an admin.
CVE-2018-7752
GPAC through 0.7.1 has a Buffer Overflow in the gf_media_avc_read_sps function in media_tools/av_parsers.c, a different vulnerability than CVE-2018-1000100.
CVE-2018-7753
An issue was discovered in Bleach 2.1.x before 2.1.3. Attributes that have URI values weren't properly sanitized if the values contained character entities. Using character entities, it was possible to construct a URI value with a scheme that was not allowed that would slide through unsanitized.
