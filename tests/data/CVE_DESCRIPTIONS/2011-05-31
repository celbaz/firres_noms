CVE-2011-0546
Symantec Backup Exec 11.0, 12.0, 12.5, 13.0, and 13.0 R2 does not validate identity information sent between the media server and the remote agent, which allows man-in-the-middle attackers to execute NDMP commands via unspecified vectors.
CVE-2011-0628
Integer overflow in Adobe Flash Player before 10.3.181.14 on Windows, Mac OS X, Linux, and Solaris and before 10.3.185.21 on Android allows remote attackers to execute arbitrary code via ActionScript that improperly handles a long array object.
CVE-2011-0766
The random number generator in the Crypto application before 2.0.2.2, and SSH before 2.0.5, as used in the Erlang/OTP ssh library before R14B03, uses predictable seeds based on the current time, which makes it easier for remote attackers to guess DSA host and SSH session keys.
CVE-2011-0943
Cisco IOS XR 3.8.3, 3.8.4, and 3.9.1 allows remote attackers to cause a denial of service (NetIO process restart or device reload) via a crafted IPv4 packet, aka Bug ID CSCth44147.
CVE-2011-0949
Cisco IOS XR 3.6.x, 3.8.x before 3.8.3, and 3.9.x before 3.9.1 does not properly remove sshd_lock files from /tmp/, which allows remote attackers to cause a denial of service (disk consumption) by making many SSHv1 connections, aka Bug ID CSCtd64417.
CVE-2011-1213
Integer underflow in lzhsr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a crafted header in a .lzh attachment that triggers a stack-based buffer overflow, aka SPR PRAD88MJ2W.
CVE-2011-1214
Stack-based buffer overflow in rtfsr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a crafted link in a .rtf attachment, aka SPR PRAD8823JQ.
CVE-2011-1215
Stack-based buffer overflow in mw8sr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a crafted link in a Microsoft Office document attachment, aka SPR PRAD8823ND.
CVE-2011-1216
Stack-based buffer overflow in assr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via crafted tag data in an Applix spreadsheet attachment, aka SPR PRAD8823A7.
CVE-2011-1217
Buffer overflow in kpprzrdr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a crafted .prz attachment.  NOTE: some of these details are obtained from third party information.
CVE-2011-1218
Buffer overflow in kvarcve.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a crafted .zip attachment, aka SPR PRAD8E3NSP. NOTE: some of these details are obtained from third party information.
CVE-2011-1329
WalRack 1.x before 1.1.9 and 2.x before 2.0.7 does not properly restrict file uploads, which allows remote attackers to execute arbitrary PHP code via vectors involving a double extension, as demonstrated by a .php.zzz file.
CVE-2011-1485
Race condition in the pkexec utility and polkitd daemon in PolicyKit (aka polkit) 0.96 allows local users to gain privileges by executing a setuid program from pkexec, related to the use of the effective user ID instead of the real user ID.
CVE-2011-1486
libvirtd in libvirt before 0.9.0 does not use thread-safe error reporting, which allows remote attackers to cause a denial of service (crash) by causing multiple threads to report errors at the same time.
CVE-2011-1512
Heap-based buffer overflow in xlssr.dll in Autonomy KeyView, as used in IBM Lotus Notes before 8.5.2 FP3, allows remote attackers to execute arbitrary code via a malformed BIFF record in a .xls Excel spreadsheet attachment, aka SPR PRAD8E3HKR.
CVE-2011-1645
The web management interface on the Cisco RVS4000 Gigabit Security Router with software 1.x before 1.3.3.4 and 2.x before 2.0.2.7, and the WRVS4400N Gigabit Security Router with software before 2.0.2.1, allows remote attackers to read the backup configuration file, and consequently execute arbitrary code, via unspecified vectors, aka Bug ID CSCtn23871.
CVE-2011-1646
The web management interface on the Cisco RVS4000 Gigabit Security Router with software 1.x before 1.3.3.4 and 2.x before 2.0.2.7, and the WRVS4400N Gigabit Security Router with software before 2.0.2.1, allows remote authenticated users to execute arbitrary commands via the (1) ping test parameter or (2) traceroute test parameter, aka Bug ID CSCtn23871.
CVE-2011-1647
The web management interface on the Cisco RVS4000 Gigabit Security Router with software 1.x before 1.3.3.4 and 2.x before 2.0.2.7, and the WRVS4400N Gigabit Security Router with software before 2.0.2.1, allows remote attackers to read the private key for the admin SSL certificate via unspecified vectors, aka Bug ID CSCtn23871.
CVE-2011-1649
The Internet Streamer application in Cisco Content Delivery System (CDS) with software 2.5.7, 2.5.8, and 2.5.9 before build 126 allows remote attackers to cause a denial of service (Web Engine crash) via a crafted URL, aka Bug IDs CSCtg67333 and CSCth25341.
CVE-2011-1651
Cisco IOS XR 3.9.x and 4.0.x before 4.0.3 and 4.1.x before 4.1.1, when an SPA interface processor is installed, allows remote attackers to cause a denial of service (device reload) via a crafted IPv4 packet, aka Bug ID CSCto45095.
CVE-2011-1910
Off-by-one error in named in ISC BIND 9.x before 9.7.3-P1, 9.8.x before 9.8.0-P2, 9.4-ESV before 9.4-ESV-R4-P1, and 9.6-ESV before 9.6-ESV-R4-P1 allows remote DNS servers to cause a denial of service (assertion failure and daemon exit) via a negative response containing large RRSIG RRsets.
CVE-2011-1922
daemon/worker.c in Unbound 1.x before 1.4.10, when debugging functionality and the interface-automatic option are enabled, allows remote attackers to cause a denial of service (assertion failure and daemon exit) via a crafted DNS request that triggers improper error handling.
CVE-2011-1925
nbd-server.c in Network Block Device (nbd-server) 2.9.21 allows remote attackers to cause a denial of service (NULL pointer dereference and crash) by causing a negotiation failure, as demonstrated by specifying a name for a non-existent export.
Per: http://cwe.mitre.org/data/definitions/476.html 
'CWE-476: NULL Pointer Dereference'
CVE-2011-1937
Cross-site scripting (XSS) vulnerability in Webmin 1.540 and earlier allows local users to inject arbitrary web script or HTML via a chfn command that changes the real (aka Full Name) field, related to useradmin/index.cgi and useradmin/user-lib.pl.
CVE-2011-1938
Stack-based buffer overflow in the socket_connect function in ext/sockets/sockets.c in PHP 5.3.3 through 5.3.6 might allow context-dependent attackers to execute arbitrary code via a long pathname for a UNIX socket.
CVE-2011-1945
The elliptic curve cryptography (ECC) subsystem in OpenSSL 1.0.0d and earlier, when the Elliptic Curve Digital Signature Algorithm (ECDSA) is used for the ECDHE_ECDSA cipher suite, does not properly implement curves over binary fields, which makes it easier for context-dependent attackers to determine private keys via a timing attack and a lattice calculation.
CVE-2011-2214
Unspecified vulnerability in the Open Database Connectivity (ODBC) component in 7T Interactive Graphical SCADA System (IGSS) before 9.0.0.11143 allows remote attackers to execute arbitrary code via a crafted packet to TCP port 20222, which triggers memory corruption related to an "invalid structure being used."
CVE-2011-2215
Unspecified vulnerability in WalRack 1.x before 1.1.8 and 2.x before 2.0.6 has unknown impact and attack vectors, possibly related to file deletion and an encoded URL, a different vulnerability than CVE-2011-1329.
