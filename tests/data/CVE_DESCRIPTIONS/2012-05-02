CVE-2011-2578
Memory leak in Cisco IOS 15.1 and 15.2 allows remote attackers to cause a denial of service (memory consumption) via malformed SIP packets on a NAT interface, aka Bug ID CSCts12366.
CVE-2011-2583
Cisco Unified Contact Center Express (aka CCX) 8.0 and 8.5 allows remote attackers to cause a denial of service via network traffic, as demonstrated by an SEC-BE-STABLE test case, aka Bug ID CSCth33834.
CVE-2011-2586
The HTTP client in Cisco IOS 12.4 and 15.0 allows user-assisted remote attackers to cause a denial of service (device crash) via a malformed HTTP response to a request for service installation, aka Bug ID CSCts12249.
CVE-2011-3283
Cisco Carrier Routing System 3.9.1 allows remote attackers to cause a denial of service (Metro subsystem crash) via a fragmented GRE packet, aka Bug ID CSCts14887.
CVE-2011-3285
CRLF injection vulnerability in /+CSCOE+/logon.html on Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 8.0 through 8.4 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors, aka Bug ID CSCth63101.
CVE-2011-3289
Cisco IOS 12.4 and 15.0 through 15.2 allows physically proximate attackers to bypass the No Service Password-Recovery feature and read the start-up configuration via unspecified vectors, aka Bug ID CSCtr97640.
CVE-2011-3293
Multiple cross-site request forgery (CSRF) vulnerabilities in the Solution Engine in Cisco Secure Access Control Server (ACS) 5.2 allow remote attackers to hijack the authentication of administrators for requests that insert cross-site scripting (XSS) sequences, aka Bug ID CSCtr78143.
CVE-2011-3295
The NETIO and IPV4_IO processes in Cisco IOS XR 3.8 through 4.1, as used in Cisco Carrier Routing System and other products, allow remote attackers to cause a denial of service (CPU consumption) via crafted network traffic, aka Bug ID CSCti59888.
CVE-2011-3309
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 8.2 through 8.4 process IKE requests despite a vpnclient mode configuration, which allows remote attackers to obtain potentially sensitive information by reading IKE responder traffic, aka Bug ID CSCtt07749.
CVE-2011-3317
Multiple cross-site scripting (XSS) vulnerabilities in the Solution Engine in Cisco Secure Access Control Server (ACS) 5.2 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka Bug ID CSCtr78192.
CVE-2011-4006
The ESMTP inspection feature on Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 8.2 through 8.5 allows remote attackers to cause a denial of service (CPU consumption) via an unspecified closing sequence, aka Bug ID CSCtt32565.
CVE-2011-4007
Cisco IOS 15.0 and 15.1 and IOS XE 3.x do not properly handle the "set mpls experimental imposition" command, which allows remote attackers to cause a denial of service (device crash) via network traffic that triggers (1) fragmentation or (2) reassembly, aka Bug ID CSCtr56576.
CVE-2011-4012
Cisco IOS 12.0, 15.0, and 15.1, when a Policy Feature Card 3C (PFC3C) is used, does not create a fragment entry during processing of an ICMPv6 ACL, which has unspecified impact and remote attack vectors, aka Bug ID CSCtj90091.
CVE-2011-4014
The TAC Case Attachment tool in Cisco Wireless Control System (WCS) 7.0 allows remote authenticated users to read arbitrary files under webnms/Temp/ via unspecified vectors, aka Bug ID CSCtq86807.
CVE-2011-4015
Cisco IOS 15.2S allows remote attackers to cause a denial of service (interface queue wedge) via malformed UDP traffic on port 465, aka Bug ID CSCts48300.
CVE-2011-4016
The PPP implementation in Cisco IOS 12.2 and 15.0 through 15.2, when Point-to-Point Termination and Aggregation (PTA) and L2TP are used, allows remote attackers to cause a denial of service (device crash) via crafted network traffic, aka Bug ID CSCtf71673.
CVE-2012-0333
Cisco Small Business IP phones with SPA 500 series firmware 7.4.9 and earlier do not require authentication for Push XML requests, which allows remote attackers to make telephone calls via an XML document, aka Bug ID CSCts08768.
CVE-2012-0335
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 7.2 through 8.4 do not properly perform proxy authentication during attempts to cut through a firewall, which allows remote attackers to obtain sensitive information via a connection attempt, aka Bug ID CSCtx42746.
CVE-2012-0337
SQL injection vulnerability in the web component in Cisco Unified MeetingPlace 7.1 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCtx08939.
CVE-2012-0338
Cisco IOS 12.2 through 12.4 and 15.0 does not recognize the vrf-also keyword during enforcement of access-class commands, which allows remote attackers to establish SSH connections from arbitrary source IP addresses via a standard SSH client, aka Bug ID CSCsv86113.
CVE-2012-0339
Cisco IOS 12.2 through 12.4 and 15.0 does not recognize the vrf-also keyword during enforcement of access-class commands, which allows remote attackers to establish TELNET connections from arbitrary source IP addresses via a standard TELNET client, aka Bug ID CSCsi77774.
CVE-2012-0361
The sccp-protocol component in Cisco IP Communicator (CIPC) 7.0 through 8.6 does not limit the rate of SCCP messages to Cisco Unified Communications Manager (CUCM), which allows remote attackers to cause a denial of service via vectors that trigger (1) on hook and (2) off hook messages, as demonstrated by a Plantronics headset, aka Bug ID CSCti40315.
CVE-2012-0362
The extended ACL functionality in Cisco IOS 12.2(58)SE2 and 15.0(1)SE discards all lines that end with a log or time keyword, which allows remote attackers to bypass intended access restrictions in opportunistic circumstances by sending network traffic, aka Bug ID CSCts01106.
CVE-2012-1819
Untrusted search path vulnerability in WellinTech KingView 6.53 allows local users to gain privileges via a Trojan horse DLL in the current working directory.
Per: http://www.us-cert.gov/control_systems/pdf/ICSA-12-122-01.pdf

'This vulnerability is remotely exploitable but may require the use of social engineering to exploit.'
Per: http://cwe.mitre.org/data/lists/426.html

'Untrusted Search Path'
CVE-2012-2000
Multiple unspecified vulnerabilities in HP System Health Application and Command Line Utilities before 9.0.0 allow remote attackers to execute arbitrary code via unknown vectors.
CVE-2012-2001
Cross-site scripting (XSS) vulnerability in HP SNMP Agents for Linux before 9.0.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2002
Open redirect vulnerability in HP SNMP Agents for Linux before 9.0.0 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2012-2003
Cross-site request forgery (CSRF) vulnerability in HP Insight Management Agents before 9.0.0.0 on Windows Server 2003 and 2008 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2012-2004
Open redirect vulnerability in HP Insight Management Agents before 9.0.0.0 on Windows Server 2003 and 2008 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2012-2005
Cross-site scripting (XSS) vulnerability in HP Insight Management Agents before 9.0.0.0 on Windows Server 2003 and 2008 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2006
Unspecified vulnerability in HP Insight Management Agents before 9.0.0.0 on Windows Server 2003 and 2008 allows remote attackers to modify data or cause a denial of service via unknown vectors.
