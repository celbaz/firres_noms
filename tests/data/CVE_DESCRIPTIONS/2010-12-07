CVE-2010-4150
Double free vulnerability in the imap_do_open function in the IMAP extension (ext/imap/php_imap.c) in PHP 5.2 before 5.2.15 and 5.3 before 5.3.4 allows attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via unspecified vectors.
CVE-2010-4170
The staprun runtime tool in SystemTap 1.3 does not properly clear the environment before executing modprobe, which allows local users to gain privileges by setting the MODPROBE_OPTIONS environment variable to specify a malicious configuration file.
CVE-2010-4171
The staprun runtime tool in SystemTap 1.3 does not verify that a module to unload was previously loaded by SystemTap, which allows local users to cause a denial of service (unloading of arbitrary kernel modules).
CVE-2010-4176
plymouth-pretrigger.sh in dracut and udev, when running on Fedora 13 and 14, sets weak permissions for the /dev/systty device file, which allows remote authenticated users to read terminal data from tty0 for local users.
CVE-2010-4179
The installation documentation for Red Hat Enterprise Messaging, Realtime and Grid (MRG) 1.3 recommends that Condor should be configured so that the MRG Management Console (cumin) can submit jobs for users, which creates a trusted channel with insufficient access control that allows local users with the ability to publish to a broker to run jobs as arbitrary users via Condor QMF plug-ins.
CVE-2010-4246
Multiple cross-site scripting (XSS) vulnerabilities in graph.php in pfSense 1.2.3 and 2 beta 4 allow remote attackers to inject arbitrary web script or HTML via the (1) ifnum or (2) ifname parameter, a different vulnerability than CVE-2008-1182.
CVE-2010-4257
SQL injection vulnerability in the do_trackbacks function in wp-includes/comment.php in WordPress before 3.0.2 allows remote authenticated users to execute arbitrary SQL commands via the Send Trackbacks field.
CVE-2010-4259
Stack-based buffer overflow in FontForge 20100501 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long CHARSET_REGISTRY header in a BDF font file.
CVE-2010-4260
Multiple unspecified vulnerabilities in pdf.c in libclamav in ClamAV before 0.96.5 allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted PDF document, aka (1) "bb #2358" and (2) "bb #2396."
CVE-2010-4261
Off-by-one error in the icon_cb function in pe_icons.c in libclamav in ClamAV before 0.96.5 allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2010-4330
Directory traversal vulnerability in includes/controller.php in Pulse CMS Basic before 1.2.9 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the p parameter to index.php.
CVE-2010-4412
Multiple cross-site scripting (XSS) vulnerabilities in pfSense 2 beta 4 allow remote attackers to inject arbitrary web script or HTML via (1) the id parameter in an olsrd.xml action to pkg_edit.php, (2) the xml parameter to pkg.php, or the if parameter to (3) status_graph.php or (4) interfaces.php, a different vulnerability than CVE-2008-1182 and CVE-2010-4246.
CVE-2010-4479
Unspecified vulnerability in pdf.c in libclamav in ClamAV before 0.96.5 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted PDF document, aka "bb #2380," a different vulnerability than CVE-2010-4260.
CVE-2010-4482
Unspecified vulnerability in Google Chrome before 8.0.552.215 allows remote attackers to bypass the pop-up blocker via unknown vectors.
CVE-2010-4483
Google Chrome before 8.0.552.215 does not properly restrict read access to videos derived from CANVAS elements, which allows remote attackers to bypass the Same Origin Policy and obtain potentially sensitive video data via a crafted web site.
CVE-2010-4484
Google Chrome before 8.0.552.215 does not properly handle HTML5 databases, which allows attackers to cause a denial of service (application crash) via unspecified vectors.
CVE-2010-4485
Google Chrome before 8.0.552.215 does not properly restrict the generation of file dialogs, which allows remote attackers to cause a denial of service (reduced usability and possible application crash) via a crafted web site.
CVE-2010-4486
Use-after-free vulnerability in Google Chrome before 8.0.552.215 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to history handling.
CVE-2010-4487
Incomplete blacklist vulnerability in Google Chrome before 8.0.552.215 on Linux and Mac OS X allows remote attackers to have an unspecified impact via a "dangerous file."
Per: http://cwe.mitre.org/data/definitions/184.html

'CWE-184: Incomplete Blacklist'
CVE-2010-4488
Google Chrome before 8.0.552.215 does not properly handle HTTP proxy authentication, which allows remote attackers to cause a denial of service (application crash) via unspecified vectors.
CVE-2010-4489
libvpx, as used in Google Chrome before 8.0.552.215 and possibly other products, allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted WebM video.  NOTE: this vulnerability exists because of a regression.
CVE-2010-4490
Google Chrome before 8.0.552.215 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via malformed video content that triggers an indexing error.
CVE-2010-4491
Google Chrome before 8.0.552.215 does not properly restrict privileged extensions, which allows remote attackers to cause a denial of service (memory corruption) via a crafted extension.
CVE-2010-4492
Use-after-free vulnerability in Google Chrome before 8.0.552.215 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors involving SVG animations.
CVE-2010-4493
Use-after-free vulnerability in Google Chrome before 8.0.552.215 allows remote attackers to cause a denial of service via vectors related to the handling of mouse dragging events.
CVE-2010-4494
Double free vulnerability in libxml2 2.7.8 and other versions, as used in Google Chrome before 8.0.552.215 and other products, allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to XPath handling.
