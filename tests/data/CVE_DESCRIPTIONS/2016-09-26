CVE-2016-0248
IBM Security Guardium 9.0 before p700 and 10.0 before p100 allows man-in-the-middle attackers to obtain sensitive query-string information from SSL sessions via unspecified vectors.
CVE-2016-0379
IBM WebSphere MQ 7.5 before 7.5.0.7 and 8.0 before 8.0.0.5 mishandles protocol flows, which allows remote authenticated users to cause a denial of service (channel outage) by leveraging queue-manager rights.
CVE-2016-2999
IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to obtain sensitive information via an unspecified brute-force attack.
CVE-2016-3000
The help service in IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to cause a denial of service (service degradation) via a crafted URL.
CVE-2016-3001
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string, a different vulnerability than CVE-2016-3003 and CVE-2016-3006.
CVE-2016-3003
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string, a different vulnerability than CVE-2016-3001 and CVE-2016-3006.
CVE-2016-3006
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string, a different vulnerability than CVE-2016-3001 and CVE-2016-3003.
CVE-2016-3007
Cross-site request forgery (CSRF) vulnerability in IBM Connections 4.x through 4.5 CR5, 5.0 before CR4, and 5.5 before CR1 allows remote authenticated users to hijack the authentication of arbitrary users.
CVE-2016-3040
IBM WebSphere Application Server (WAS) Liberty, as used in IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8, allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2016-3110
mod_cluster, as used in Red Hat JBoss Web Server 2.1, allows remote attackers to cause a denial of service (Apache http server crash) via an MCMP message containing a series of = (equals) characters after a legitimate element.
CVE-2016-3639
SAP HANA DB 1.00.091.00.1418659308 allows remote attackers to obtain sensitive topology information via an unspecified HTTP request, aka SAP Security Note 2176128.
CVE-2016-4303
The parse_string function in cjson.c in the cJSON library mishandles UTF8/16 strings, which allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a non-hex character in a JSON string, which triggers a heap-based buffer overflow.
CVE-2016-4972
OpenStack Murano before 1.0.3 (liberty) and 2.x before 2.0.1 (mitaka), Murano-dashboard before 1.0.3 (liberty) and 2.x before 2.0.1 (mitaka), and python-muranoclient before 0.7.3 (liberty) and 0.8.x before 0.8.5 (mitaka) improperly use loaders inherited from yaml.Loader when parsing MuranoPL and UI files, which allows remote attackers to create arbitrary Python objects and execute arbitrary code via crafted extended YAML tags in UI definitions in packages.
CVE-2016-4993
CRLF injection vulnerability in the Undertow web server in WildFly 10.0.0, as used in Red Hat JBoss Enterprise Application Platform (EAP) 7.x before 7.0.2, allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors.
CVE-2016-5395
Cross-site scripting (XSS) vulnerability in the create user functionality in the policy admin tool in Apache Ranger before 0.6.1 allows remote authenticated administrators to inject arbitrary web script or HTML via vectors related to policies.
CVE-2016-5406
The domain controller in Red Hat JBoss Enterprise Application Platform (EAP) 7.x before 7.0.2 allows remote authenticated users to gain privileges by leveraging failure to propagate administrative RBAC configuration to all slaves.
CVE-2016-5746
libstorage, libstorage-ng, and yast-storage improperly store passphrases for encrypted storage devices in a temporary file on disk, which might allow local users to obtain sensitive information by reading the file, as demonstrated by /tmp/libstorage-XXXXXX/pwdf.
<a href="http://cwe.mitre.org/data/definitions/313.html">CWE-313: Cleartext Storage in a File or on Disk</a>
CVE-2016-5943
IBM Spectrum Control (formerly Tivoli Storage Productivity Center) 5.2.x before 5.2.11 allows remote authenticated users to bypass intended access restrictions, and read task details or edit properties, via unspecified vectors.
CVE-2016-5944
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Spectrum Control (formerly Tivoli Storage Productivity Center) 5.2.x before 5.2.11 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string.
CVE-2016-5945
IBM Spectrum Control (formerly Tivoli Storage Productivity Center) 5.2.x before 5.2.11 allows remote authenticated users to upload non-executable files via a crafted HTTP request.
CVE-2016-5946
Directory traversal vulnerability in IBM Spectrum Control (formerly Tivoli Storage Productivity Center) 5.2.x before 5.2.11 allows remote authenticated users to read arbitrary files via a .. (dot dot) in a URL.
CVE-2016-5947
IBM Spectrum Control (formerly Tivoli Storage Productivity Center) 5.2.x before 5.2.11 allows remote authenticated users to conduct clickjacking attacks via a crafted web site.
CVE-2016-5957
IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 allows remote attackers to defeat cryptographic protection mechanisms and obtain sensitive information by leveraging a weak algorithm.
CVE-2016-5963
IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 does not properly validate updates, which allows remote authenticated users to execute arbitrary code via unspecified vectors.
CVE-2016-5970
Directory traversal vulnerability in IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 allows remote authenticated users to read arbitrary files via a .. (dot dot) in a URL.
CVE-2016-5971
IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 allows remote authenticated users to read arbitrary files or cause a denial of service (memory consumption) via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2016-5972
IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 uses weak permissions for unspecified resources, which allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-5974
Cross-site scripting (XSS) vulnerability in the Web UI in IBM Security Privileged Identity Manager (ISPIM) Virtual Appliance 2.x before 2.0.2 FP8 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string.
CVE-2016-5975
Cross-site scripting (XSS) vulnerability in the Web UI in the web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string, a different vulnerability than CVE-2016-5978.
CVE-2016-5976
The web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 allows remote authenticated users to discover component passwords via unspecified vectors.
CVE-2016-5977
Open redirect vulnerability in the web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2016-5978
Cross-site scripting (XSS) vulnerability in the Web UI in the web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 allows remote authenticated users to inject arbitrary web script or HTML via an embedded string, a different vulnerability than CVE-2016-5975.
CVE-2016-5996
The web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 does not enforce password-length restrictions, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2016-5997
The web portal in IBM Tealeaf Customer Experience before 8.7.1.8847 FP10, 8.8 before 8.8.0.9049 FP9, 9.0.0 and 9.0.1 before 9.0.1.1117 FP5, 9.0.1A before 9.0.1.5108_9.0.1A FP5, 9.0.2 before 9.0.2.1223 FP3, and 9.0.2A before 9.0.2.5224_9.0.2A FP3 does not apply password-quality rules to password changes, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2016-6038
Directory traversal vulnerability in Eclipse Help in IBM Tivoli Lightweight Infrastructure (aka LWI), as used in AIX 5.3, 6.1, and 7.1, allows remote authenticated users to read arbitrary files via a crafted URL.
CVE-2016-6142
SAP HANA DB 1.00.73.00.389160 (NewDB100_REL) allows remote attackers to inject arbitrary audit trail fields into the SYSLOG via vectors related to the SQL protocol, aka SAP Security Note 2197459.
<a href="http://cwe.mitre.org/data/definitions/117.html">CWE-117: Improper Output Neutralization for Logs</a>
CVE-2016-6153
os_unix.c in SQLite before 3.13.0 improperly implements the temporary directory search algorithm, which might allow local users to obtain sensitive information, cause a denial of service (application crash), or have unspecified other impact by leveraging use of the current working directory for temporary files.
CVE-2016-6172
PowerDNS (aka pdns) Authoritative Server before 4.0.1 allows remote primary DNS servers to cause a denial of service (memory exhaustion and secondary DNS server crash) via a large (1) AXFR or (2) IXFR response.
CVE-2016-6276
Citrix Linux Virtual Delivery Agent (aka VDA, formerly Linux Virtual Desktop) before 1.4.0 allows local users to gain root privileges via unspecified vectors.
CVE-2016-6304
Multiple memory leaks in t1_lib.c in OpenSSL before 1.0.1u, 1.0.2 before 1.0.2i, and 1.1.0 before 1.1.0a allow remote attackers to cause a denial of service (memory consumption) via large OCSP Status Request extensions.
CVE-2016-6305
The ssl3_read_bytes function in record/rec_layer_s3.c in OpenSSL 1.1.0 before 1.1.0a allows remote attackers to cause a denial of service (infinite loop) by triggering a zero-length record in an SSL_peek call.
CVE-2016-6306
The certificate parser in OpenSSL before 1.0.1u and 1.0.2 before 1.0.2i might allow remote attackers to cause a denial of service (out-of-bounds read) via crafted certificate operations, related to s3_clnt.c and s3_srvr.c.
CVE-2016-6307
The state-machine implementation in OpenSSL 1.1.0 before 1.1.0a allocates memory before checking for an excessive length, which might allow remote attackers to cause a denial of service (memory consumption) via crafted TLS messages, related to statem/statem.c and statem/statem_lib.c.
CVE-2016-6308
statem/statem_dtls.c in the DTLS implementation in OpenSSL 1.1.0 before 1.1.0a allocates memory before checking for an excessive length, which might allow remote attackers to cause a denial of service (memory consumption) via crafted DTLS messages.
CVE-2016-6309
statem/statem.c in OpenSSL 1.1.0a does not consider memory-block movement after a realloc call, which allows remote attackers to cause a denial of service (use-after-free) or possibly execute arbitrary code via a crafted TLS session.
CVE-2016-6518
Memory leak in Huawei S9300, S5300, S5700, S6700, S7700, S9700, and S12700 devices allows remote attackers to cause a denial of service (memory consumption and restart) via a large number of malformed packets.
CVE-2016-6826
Huawei AnyMail before 2.6.0301.0060 allows remote attackers to cause a denial of service (application crash) via a crafted compressed email attachment.
CVE-2016-6827
Huawei FusionCompute before V100R005C10CP7002 stores cleartext AES keys in a file, which allows remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2016-6840
Cross-site scripting (XSS) vulnerability in the management interface in Huawei OceanStor ISM before V200R001C04SPC200 allows remote attackers to inject arbitrary web script or HTML via the loginName parameter to cgi-bin/doLogin_CgiEntry and possibly other unspecified vectors.
CVE-2016-6901
Format string vulnerability in Huawei AR100, AR120, AR150, AR200, AR500, AR550, AR1200, AR2200, AR2500, AR3200, and AR3600 routers with software before V200R007C00SPC900 and NetEngine 16EX routers with software before V200R007C00SPC900 allows remote authenticated users to cause a denial of service via format string specifiers in vectors involving partial commands.
CVE-2016-6913
Cross-site scripting (XSS) vulnerability in AlienVault OSSIM before 5.3 and USM before 5.3 allows remote attackers to inject arbitrary web script or HTML via the back parameter to ossim/conf/reload.php.
CVE-2016-6980
Use-after-free vulnerability in Adobe Digital Editions before 4.5.2 allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2016-4263.
CVE-2016-7052
crypto/x509/x509_vfy.c in OpenSSL 1.0.2i allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) by triggering a CRL operation.
CVE-2016-7098
Race condition in wget 1.17 and earlier, when used in recursive or mirroring mode to download a single file, might allow remote servers to bypass intended access list restrictions by keeping an HTTP connection open.
CVE-2016-7142
The m_sasl module in InspIRCd before 2.0.23, when used with a service that supports SASL_EXTERNAL authentication, allows remote attackers to spoof certificate fingerprints and consequently log in as another user via a crafted SASL message.
CVE-2016-7162
The _g_file_remove_directory function in file-utils.c in File Roller 3.5.4 through 3.20.2 allows remote attackers to delete arbitrary files via a symlink attack on a folder in an archive.
CVE-2016-7554
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2016-8279
The video driver in Huawei Mate S smartphones with software CRR-TL00 before CRR-TL00C01B362, CRR-UL20 before CRR-UL20C00B362, CRR-CL00 before CRR-CL00C92B362, and CRR-CL20 before CRR-CL20C92B362; P8 smartphones with software GRA-TL00 before GRA-TL00C01B366, GRA-UL00 before GRA-UL00C00B366, GRA-UL10 before GRA-UL10C00B366, and GRA-CL00 before GRA-CL00C92B366; and Honor 6 and Honor 6 Plus smartphones with software before 6.9.16 allows attackers to cause a denial of service (device reboot) via a crafted application.
