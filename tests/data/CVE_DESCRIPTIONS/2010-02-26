CVE-2009-4652
The (1) Conn_GetCipherInfo and (2) Conn_UsesSSL functions in src/ngircd/conn.c in ngIRCd 13 and 14, when SSL/TLS support is present and standalone mode is disabled, allow remote attackers to cause a denial of service (application crash) by sending the MOTD command from another server in the same IRC network, possibly related to an array index error.
CVE-2009-4653
Stack-based buffer overflow in the dhost module in Novell eDirectory 8.8 SP5 for Windows allows remote authenticated users to cause a denial of service (dhost.exe crash) and possibly execute arbitrary code via a long string to /dhost/modules?I:.
CVE-2009-4654
Stack-based buffer overflow in the dhost module in Novell eDirectory 8.8 SP5 for Windows allows remote authenticated users to execute arbitrary code via long sadminpwd and verifypwd parameters in a submit action to /dhost/httpstk.
CVE-2009-4655
The dhost web service in Novell eDirectory 8.8.5 uses a predictable session cookie, which makes it easier for remote attackers to hijack sessions via a modified cookie.
CVE-2010-0667
MoinMoin 1.9 before 1.9.1 does not perform the expected clearing of the sys.argv array in situations where the GATEWAY_INTERFACE environment variable is set, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2010-0668
Unspecified vulnerability in MoinMoin 1.5.x through 1.7.x, 1.8.x before 1.8.7, and 1.9.x before 1.9.2 has unknown impact and attack vectors, related to configurations that have a non-empty superuser list, the xmlrpc action enabled, the SyncPages action enabled, or OpenID configured.
CVE-2010-0669
MoinMoin before 1.8.7 and 1.9.x before 1.9.2 does not properly sanitize user profiles, which has unspecified impact and attack vectors.
CVE-2010-0689
The ExecuteExe method in the DVBSExeCall Control ActiveX control 1.0.0.1 in DVBSExeCall.ocx in DATEV Base System (aka Grundpaket Basis) allows remote attackers to execute arbitrary commands via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Sanitization of Special Elements used in a Command ('Command Injection')"
CVE-2010-0712
Multiple SQL injection vulnerabilities in zport/dmd/Events/getJSONEventsInfo in Zenoss 2.3.3, and other versions before 2.5, allow remote authenticated users to execute arbitrary SQL commands via the (1) severity, (2) state, (3) filter, (4) offset, and (5) count parameters.
CVE-2010-0713
Multiple cross-site request forgery (CSRF) vulnerabilities in Zenoss 2.3.3, and other versions before 2.5, allow remote attackers to hijack the authentication of an administrator for (1) requests that reset user passwords via zport/dmd/ZenUsers/admin, and (2) requests that change user commands, which allows for remote execution of system commands via zport/dmd/userCommands/.
CVE-2010-0714
Cross-site scripting (XSS) vulnerability in login.jsp in IBM WebSphere Portal, IBM Lotus Web Content Management (WCM), and IBM Lotus Workplace Web Content Management 5.1.0.0 through 5.1.0.5, 6.0.0.0 through 6.0.0.4, 6.0.1.0 through 6.0.1.7, 6.1.0.0 through 6.1.0.3, and 6.1.5.0; and IBM Lotus Quickr services 8.0, 8.0.0.2, 8.1, 8.1.1, and 8.1.1.1 for WebSphere Portal; allows remote attackers to inject arbitrary web script or HTML via the query string.
CVE-2010-0715
Open redirect vulnerability in login.jsp in IBM WebSphere Portal, IBM Lotus Web Content Management (WCM), and IBM Lotus Workplace Web Content Management 5.1.0.0 through 5.1.0.5, 6.0.0.0 through 6.0.0.4, 6.0.1.0 through 6.0.1.7, 6.1.0.0 through 6.1.0.3, and 6.1.5.0; and IBM Lotus Quickr services 8.0, 8.0.0.2, 8.1, 8.1.1, and 8.1.1.1 for WebSphere Portal; allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via the query string.
CVE-2010-0716
_layouts/Upload.aspx in the Documents module in Microsoft SharePoint before 2010 uses URLs with the same hostname and port number for a web site's primary files and individual users' uploaded files (aka attachments), which allows remote authenticated users to leverage same-origin relationships and conduct cross-site scripting (XSS) attacks by uploading TXT files, a related issue to CVE-2008-5026. NOTE: the vendor disputes the significance of this issue, because cross-domain isolation can be implemented when needed.
CVE-2010-0717
The default configuration of cfg.packagepages_actions_excluded in MoinMoin before 1.8.7 does not prevent unsafe package actions, which has unspecified impact and attack vectors.
CVE-2010-0718
Buffer overflow in Microsoft Windows Media Player 9 and 11.0.5721.5145 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted .mpg file.
CVE-2010-0719
An unspecified API in Microsoft Windows 2000, Windows XP, Windows Server 2003, Windows Vista, Windows Server 2008, and Windows 7 does not validate arguments, which allows local users to cause a denial of service (system crash) via a crafted application.
CVE-2010-0720
SQL injection vulnerability in news.php in Erotik Auktionshaus allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0721
SQL injection vulnerability in news.php in Auktionshaus Gelb 3.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0722
SQL injection vulnerability in news.php in Php Auktion Pro allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0723
SQL injection vulnerability in news.php in Ero Auktion 2.0 and 2010 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0724
SQL injection vulnerability in showimg.php in Arab Cart 1.0.2.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-0725
Cross-site scripting (XSS) vulnerability in showimg.php in Arab Cart 1.0.2.0 allows remote attackers to inject arbitrary web script or HTML via the id parameter.
