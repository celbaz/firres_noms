CVE-2017-1265
IBM Security Guardium 10.0, 10.0.1, 10.1, 10.1.2, 10.1.3, 10.1.4, and 10.5 does not validate, or incorrectly validates, a certificate. This weakness might allow an attacker to spoof a trusted entity by using a man-in-the-middle (MITM) techniques. IBM X-Force ID: 124740.
CVE-2017-1272
IBM Security Guardium 10.0 and 10.5 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 124747. IBM X-Force ID: 124747.
CVE-2017-1597
IBM Security Guardium 10.0, 10.0.1, 10.1, 10.1.2, 10.1.3, 10.1.4, and 10.5 Database Activity Monitor does not require that users should have strong passwords by default, which makes it easier for attackers to compromise user accounts. IBM X-Force ID: 132610.
CVE-2017-18352
Error reporting within Rendertron 1.0.0 allows reflected Cross Site Scripting (XSS) from invalid URLs.
CVE-2017-18353
Rendertron 1.0.0 includes an _ah/stop route to shutdown the Chrome instance responsible for serving render requests to all users. Visiting this route with a GET request allows any unauthorized remote attacker to disable the core service of the application.
CVE-2017-18354
Rendertron 1.0.0 allows for alternative protocols such as 'file://' introducing a Local File Inclusion (LFI) bug where arbitrary files can be read by a remote attacker.
CVE-2017-18355
Installed packages are exposed by node_modules in Rendertron 1.0.0, allowing remote attackers to read absolute paths on the server by examining the "_where" attribute of package.json files.
CVE-2018-14852
Out-of-bounds array access in dhd_rx_frame in drivers/net/wireless/bcmdhd4358/dhd_linux.c in the bcmdhd4358 Wi-Fi driver on the Samsung Galaxy S6 SM-G920F G920FXXU5EQH7 allows an attacker (who has obtained code execution on the Wi-Fi chip) to cause invalid accesses to operating system memory due to improper validation of the network interface index provided by the Wi-Fi chip's firmware.
CVE-2018-14853
A NULL pointer dereference in dhd_prot_txdata_write_flush in drivers/net/wireless/bcmdhd4358/dhd_msgbuf.c in the bcmdhd4358 Wi-Fi driver on the Samsung Galaxy S6 SM-G920F G920FXXU5EQH7 allows an attacker (who has obtained code execution on the Wi-Fi chip) to cause the device to reboot. The Samsung ID is SVE-2018-11783.
CVE-2018-14854
Buffer overflow in dhd_bus_flow_ring_delete_response in drivers/net/wireless/bcmdhd4358/dhd_pcie.c in the bcmdhd4358 Wi-Fi driver on the Samsung Galaxy S6 SM-G920F G920FXXU5EQH7 allow an attacker (who has obtained code execution on the Wi-Fi chip) to cause the device driver to perform invalid memory accesses. The Samsung ID is SVE-2018-11785.
CVE-2018-14855
Buffer overflow in dhd_bus_flow_ring_flush_response in drivers/net/wireless/bcmdhd4358/dhd_pcie.c in the bcmdhd4358 Wi-Fi driver on the Samsung Galaxy S6 allow an attacker (who has obtained code execution on the Wi-Fi chip) to cause the device driver to perform invalid memory accesses. The Samsung ID is SVE-2018-11785.
CVE-2018-14856
Buffer overflow in dhd_bus_flow_ring_create_response in drivers/net/wireless/bcmdhd4358/dhd_pcie.c in the bcmdhd4358 Wi-Fi driver on the Samsung Galaxy S6 SM-G920F G920FXXU5EQH7 allow an attacker (who has obtained code execution on the Wi-Fi) chip to cause the device driver to perform invalid memory accesses. The Samsung ID is SVE-2018-11785.
CVE-2018-16596
A stack-based buffer overflow in the LAN UPnP service running on UDP port 1900 of Swisscom Internet-Box (2, Standard, and Plus) prior to v09.04.00 and Internet-Box light prior to v08.05.02 allows remote code execution. No authentication is required to exploit this vulnerability. Sending a simple UDP packet to port 1900 allows an attacker to execute code on a remote device. However, this is only possible if the attacker is inside the LAN. Because of ASLR, the success rate is not 100% and leads instead to a DoS of the UPnP service. The remaining functionality of the Internet Box is not affected. A reboot of the Internet Box is necessary to attempt the exploit again.
CVE-2018-18245
Nagios Core 4.4.2 has XSS via the alert summary reports of plugin results, as demonstrated by a SCRIPT element delivered by a modified check_load plugin to NRPE.
CVE-2018-18246
Icinga Web 2 before 2.6.2 has CSRF via /icingaweb2/config/moduledisable?name=monitoring to disable the monitoring module, or via /icingaweb2/config/moduleenable?name=setup to enable the setup module.
CVE-2018-18247
Icinga Web 2 before 2.6.2 has XSS via the /icingaweb2/navigation/add icon parameter.
CVE-2018-18248
Icinga Web 2 has XSS via the /icingaweb2/monitoring/list/services dir parameter, the /icingaweb2/user/list query string, the /icingaweb2/monitoring/timeline query string, or the /icingaweb2/setup query string.
CVE-2018-18249
Icinga Web 2 before 2.6.2 allows injection of PHP ini-file directives via vectors involving environment variables as the channel to send information to the attacker, such as a name=${PATH}_${APACHE_RUN_DIR}_${APACHE_RUN_USER} parameter to /icingaweb2/navigation/add or /icingaweb2/dashboard/new-dashlet.
CVE-2018-18250
Icinga Web 2 before 2.6.2 allows parameters that break navigation dashlets, as demonstrated by a single '$' character as the Name of a Navigation item.
CVE-2018-18555
A sandbox escape issue was discovered in VyOS 1.1.8. It provides a restricted management shell for operator users to administer the device. By issuing various shell special characters with certain commands, an authenticated operator user can break out of the management shell and gain access to the underlying Linux shell. The user can then run arbitrary operating system commands with the privileges afforded by their account.
CVE-2018-18556
A privilege escalation issue was discovered in VyOS 1.1.8. The default configuration also allows operator users to execute the pppd binary with elevated (sudo) permissions. Certain input parameters are not properly validated. A malicious operator user can run the binary with elevated permissions and leverage its improper input validation condition to spawn an attacker-controlled shell with root privileges.
CVE-2018-1889
IBM Security Guardium 10.0 and 10.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 152080.
CVE-2018-1891
IBM Security Guardium 10 and 10.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 152082.
CVE-2018-19036
An issue was discovered in several Bosch IP cameras for firmware versions 6.32 and higher. A malicious client could potentially succeed in the unauthorized execution of code on the device via the network interface.
CVE-2018-19295
Sylabs Singularity 2.4 to 2.6 allows local users to conduct Improper Input Validation attacks.
CVE-2018-19649
XSS exists in InfoVista VistaPortal SE Version 5.1 (build 51029). VPortal/mgtconsole/RolePermissions.jsp has reflected XSS via the ConnPoolName parameter.
CVE-2018-19765
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "EditCurrentPresentSpace.jsp" has reflected XSS via the ConnPoolName, GroupId, and ParentId parameters.
CVE-2018-19766
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "GroupRessourceAdmin.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19767
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "PresentSpace.jsp" has reflected XSS via the ConnPoolName and GroupId parameters.
CVE-2018-19768
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "SubPagePackages.jsp" has reflected XSS via the ConnPoolName and GroupId parameters.
CVE-2018-19769
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "UserProperties.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19770
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "Users.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19771
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "EditCurrentPool.jsp" has reflected XSS via the PropName parameter.
CVE-2018-19772
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "EditCurrentPresentSpace.jsp" has reflected XSS via the ConnPoolName, GroupId, and ParentId parameters.
CVE-2018-19773
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "EditCurrentUser.jsp" has reflected XSS via the GroupId and ConnPoolName parameters.
CVE-2018-19774
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "PresentSpace.jsp" has reflected XSS via the GroupId and ConnPoolName parameters.
CVE-2018-19775
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "Variables.jsp" has reflected XSS via the ConnPoolName and GroupId parameters.
CVE-2018-19809
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/GroupCopy.jsp" has reflected XSS via the ConnPoolName, GroupId, or type parameter.
CVE-2018-19810
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/GroupMove.jsp" has reflected XSS via the ConnPoolName, GroupId, or type parameter.
CVE-2018-19811
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Import.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19812
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/SubFolderPackages.jsp" has reflected XSS via the GroupId parameter.
CVE-2018-19813
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Subscribers.jsp" has reflected XSS via the ConnPoolName or GroupId parameter.
CVE-2018-19814
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Subscriptions.jsp" has reflected XSS via the ConnPoolName or GroupId parameter.
CVE-2018-19815
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/UserPopupAddNewProp.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19816
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/categorytree/ChooseCategory.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19817
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/AdminAuthorisationFrame.jsp" has reflected XSS via the ConnPoolName or GroupId parameter.
CVE-2018-19818
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Contacts.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19819
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Rights.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19820
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/Roles.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19821
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/SecurityPolicies.jsp" has reflected XSS via the ConnPoolName parameter.
CVE-2018-19822
Cross Site Scripting exists in InfoVista VistaPortal SE Version 5.1 (build 51029). The page "/VPortal/mgtconsole/SharedCriteria.jsp" has reflected XSS via the ConnPoolName or GroupId parameter.
CVE-2018-19828
Artica Integria IMS 5.0.83 has XSS via the search_string parameter.
CVE-2018-19933
Bolt CMS <3.6.2 allows XSS via text input click preview button as demonstrated by the Title field of a Configured and New Entry.
CVE-2018-19936
PrinterOn Enterprise 4.1.4 allows Arbitrary File Deletion.
CVE-2018-19974
In YARA 3.8.1, bytecode in a specially crafted compiled rule can read uninitialized data from VM scratch memory in libyara/exec.c. This can allow attackers to discover addresses in the real stack (not the YARA virtual stack).
CVE-2018-19975
In YARA 3.8.1, bytecode in a specially crafted compiled rule can read data from any arbitrary address in memory, in libyara/exec.c. Specifically, OP_COUNT can read a DWORD.
CVE-2018-19976
In YARA 3.8.1, bytecode in a specially crafted compiled rule is exposed to information about its environment, in libyara/exec.c. This is a consequence of the design of the YARA virtual machine.
CVE-2018-20027
The yaml_parse.load method in Pylearn2 allows code injection.
CVE-2018-20092
PTC ThingWorx Platform through 8.3.0 is vulnerable to a directory traversal attack on ZIP files via a POST request.
CVE-2018-20123
pvrdma_realize in hw/rdma/vmw/pvrdma_main.c in QEMU has a Memory leak after an initialisation error.
CVE-2018-20133
ymlref allows code injection.
CVE-2018-20167
Terminology before 1.3.1 allows Remote Code Execution because popmedia is mishandled, as demonstrated by an unsafe "cat README.md" command when \e}pn is used. A popmedia control sequence can allow the malicious execution of executable file formats registered in the X desktop share MIME types (/usr/share/applications). The control sequence defers unknown file types to the handle_unknown_media() function, which executes xdg-open against the filename specified in the sequence. The use of xdg-open for all unknown file types allows executable file formats with a registered shared MIME type to be executed. An attacker can achieve remote code execution by introducing an executable file and a plain text file containing the control sequence through a fake software project (e.g., in Git or a tarball). When the control sequence is rendered (such as with cat), the executable file will be run.
CVE-2018-20168
Google gVisor before 2018-08-22 reuses a pagetable in a different level with the paging-structure cache intact, which allows attackers to cause a denial of service ("physical address not valid" panic) via a crafted application.
CVE-2018-20169
An issue was discovered in the Linux kernel before 4.19.9. The USB subsystem mishandles size checks during the reading of an extra descriptor, related to __usb_get_extra_descriptor in drivers/usb/core/usb.c.
CVE-2018-20170
** DISPUTED ** OpenStack Keystone through 14.0.1 has a user enumeration vulnerability because invalid usernames have much faster responses than valid ones for a POST /v3/auth/tokens request. NOTE: the vendor's position is that this is a hardening opportunity, and not necessarily an issue that should have an OpenStack Security Advisory.
CVE-2018-20171
An issue was discovered in Nagios XI before 5.5.8. The url parameter of rss_dashlet/magpierss/scripts/magpie_simple.php is not filtered, resulting in an XSS vulnerability.
CVE-2018-20172
An issue was discovered in Nagios XI before 5.5.8. The rss_url parameter of rss_dashlet/magpierss/scripts/magpie_slashbox.php is not filtered, resulting in an XSS vulnerability.
CVE-2018-20173
Zoho ManageEngine OpManager 12.3 before 123238 allows SQL injection via the getGraphData API.
CVE-2018-20184
In GraphicsMagick 1.4 snapshot-20181209 Q8, there is a heap-based buffer overflow in the WriteTGAImage function of tga.c, which allows attackers to cause a denial of service via a crafted image file, because the number of rows or columns can exceed the pixel-dimension restrictions of the TGA specification.
CVE-2018-20185
In GraphicsMagick 1.4 snapshot-20181209 Q8 on 32-bit platforms, there is a heap-based buffer over-read in the ReadBMPImage function of bmp.c, which allows attackers to cause a denial of service via a crafted bmp image file. This only affects GraphicsMagick installations with customized BMP limits.
CVE-2018-20186
An issue was discovered in Bento4 1.5.1-627. AP4_Sample::ReadData in Core/Ap4Sample.cpp allows attackers to trigger an attempted excessive memory allocation, related to AP4_DataBuffer::SetDataSize and AP4_DataBuffer::ReallocateBuffer in Core/Ap4DataBuffer.cpp.
CVE-2018-20188
FUEL CMS 1.4.3 has CSRF via users/create/ to add an administrator account.
CVE-2018-20189
In GraphicsMagick 1.3.31, the ReadDIBImage function of coders/dib.c has a vulnerability allowing a crash and denial of service via a dib file that is crafted to appear with direct pixel values and also colormapping (which is not available beyond 8-bits/sample), and therefore lacks indexes initialization.
CVE-2018-20190
In LibSass 3.5.5, a NULL Pointer Dereference in the function Sass::Eval::operator()(Sass::Supports_Operator*) in eval.cpp may cause a Denial of Service (application crash) via a crafted sass input file.
CVE-2018-7797
A URL redirection vulnerability exists in Power Monitoring Expert, Energy Expert (formerly Power Manager) - EcoStruxure Power Monitoring Expert (PME) v8.2 (all editions), EcoStruxure Energy Expert 1.3 (formerly Power Manager), EcoStruxure Power SCADA Operation (PSO) 8.2 Advanced Reports and Dashboards Module, EcoStruxure Power Monitoring Expert (PME) v9.0, EcoStruxure Energy Expert v2.0, and EcoStruxure Power SCADA Operation (PSO) 9.0 Advanced Reports and Dashboards Module which could cause a phishing attack when redirected to a malicious site.
CVE-2018-7804
A URL Redirection to Untrusted Site vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 where a user clicking on a specially crafted link can be redirected to a URL of the attacker's choosing.
CVE-2018-7812
An Information Exposure through Discrepancy vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 where the web server sends different responses in a way that exposes security-relevant information about the state of the product, such as whether a particular operation was successful or not.
CVE-2018-7833
An Improper Check for Unusual or Exceptional Conditions vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 where an unauthenticated user can send a specially crafted XML data via a POST request to cause the web server to become unavailable
