CVE-2012-6687
FastCGI (aka fcgi and libfcgi) 2.4.0 allows remote attackers to cause a denial of service (segmentation fault and crash) via a large number of connections.
CVE-2014-1831
Phusion Passenger before 4.0.37 allows local users to write to certain files and directories via a symlink attack on (1) control_process.pid or a (2) generation-* file.
<a href="http://cwe.mitre.org/data/definitions/61.html">CWE-61: UNIX Symbolic Link (Symlink) Following</a>
CVE-2014-1832
Phusion Passenger 4.0.37 allows local users to write to certain files and directories via a symlink attack on (1) control_process.pid or a (2) generation-* file.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-1831.
<a href="http://cwe.mitre.org/data/definitions/61.html">CWE-61: UNIX Symbolic Link (Symlink) Following</a>
CVE-2014-3578
Directory traversal vulnerability in Pivotal Spring Framework 3.x before 3.2.9 and 4.0 before 4.0.5 allows remote attackers to read arbitrary files via a crafted URL.
CVE-2014-5286
The ActiveMatrix Policy Manager Authentication module in TIBCO ActiveMatrix Policy Agent 3.x before 3.1.2, ActiveMatrix Policy Manager 3.x before 3.1.2, ActiveMatrix Management Agent 1.x before 1.2.1 for WCF, and ActiveMatrix Management Agent 1.x before 1.2.1 for WebSphere allows remote attackers to gain privileges and obtain sensitive information via unspecified vectors.
CVE-2014-5352
The krb5_gss_process_context_token function in lib/gssapi/krb5/process_context_token.c in the libgssapi_krb5 library in MIT Kerberos 5 (aka krb5) through 1.11.5, 1.12.x through 1.12.2, and 1.13.x before 1.13.1 does not properly maintain security-context handles, which allows remote authenticated users to cause a denial of service (use-after-free and double free, and daemon crash) or possibly execute arbitrary code via crafted GSSAPI traffic, as demonstrated by traffic to kadmind.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2014-6147
IBM Flex System Manager (FSM) 1.1.x.x, 1.2.0.x, 1.2.1.x, 1.3.0.0, 1.3.1.0, and 1.3.2.0 allows local users to obtain sensitive information, and consequently gain privileges or conduct impersonation attacks, via unspecified vectors.
CVE-2014-6301
Multiple cross-site scripting (XSS) vulnerabilities in the tables-management module in PNMsoft Sequence Kinetics before 7.7 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6302
The Monitoring Administration pages in PNMsoft Sequence Kinetics before 7.7 allow remote attackers to read arbitrary files via an XML external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-6303
The Monitoring Administration pages in PNMsoft Sequence Kinetics before 7.7 do not properly detect recursion during entity expansion, which allows remote attackers to cause a denial of service (resource consumption) via a crafted XML document containing a large number of nested entity references, a similar issue to CVE-2003-1564.
CVE-2014-6304
The Form Controls CSS file in PNMsoft Sequence Kinetics before 7.7 allows remote attackers to obtain sensitive source-code information via unspecified vectors.
CVE-2014-8165
scripts/amsvis/powerpcAMS/amsnet.py in powerpc-utils-python uses the pickle Python module unsafely, which allows remote attackers to execute arbitrary code via a crafted serialized object.
CVE-2014-8690
Multiple cross-site scripting (XSS) vulnerabilities in Exponent CMS before 2.1.4 patch 6, 2.2.x before 2.2.3 patch 9, and 2.3.x before 2.3.1 patch 4 allow remote attackers to inject arbitrary web script or HTML via the (1) PATH_INFO, the (2) src parameter in a none action to index.php, or the (3) "First Name" or (4) "Last Name" field to users/edituser.
CVE-2014-9421
The auth_gssapi_unwrap_data function in lib/rpc/auth_gssapi_misc.c in MIT Kerberos 5 (aka krb5) through 1.11.5, 1.12.x through 1.12.2, and 1.13.x before 1.13.1 does not properly handle partial XDR deserialization, which allows remote authenticated users to cause a denial of service (use-after-free and double free, and daemon crash) or possibly execute arbitrary code via malformed XDR data, as demonstrated by data sent to kadmind.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2014-9422
The check_rpcsec_auth function in kadmin/server/kadm_rpc_svc.c in kadmind in MIT Kerberos 5 (aka krb5) through 1.11.5, 1.12.x through 1.12.2, and 1.13.x before 1.13.1 allows remote authenticated users to bypass a kadmin/* authorization check and obtain administrative access by leveraging access to a two-component principal with an initial "kadmind" substring, as demonstrated by a "ka/x" principal.
CVE-2014-9423
The svcauth_gss_accept_sec_context function in lib/rpc/svc_auth_gss.c in MIT Kerberos 5 (aka krb5) 1.11.x through 1.11.5, 1.12.x through 1.12.2, and 1.13.x before 1.13.1 transmits uninitialized interposer data to clients, which allows remote attackers to obtain sensitive information from process heap memory by sniffing the network for data in a handle field.
CVE-2014-9465
senddocument.php in Zarafa WebApp before 2.0 beta 3 and WebAccess in Zarafa Collaboration Platform (ZCP) 7.x before 7.1.12 beta 1 and 7.2.x before 7.2.0 beta 1 allows remote attackers to cause a denial of service (/tmp disk consumption) by uploading a large number of files.
CVE-2014-9468
Multiple cross-site scripting (XSS) vulnerabilities in InstantASP InstantForum.NET 4.1.3, 4.1.2, 4.1.1, 4.0.0, 4.1.0, and 3.4.0 allow remote attackers to inject arbitrary web script or HTML via the SessionID parameter to (1) Join.aspx or (2) Logon.aspx.
CVE-2014-9679
Integer underflow in the cupsRasterReadPixels function in filter/raster.c in CUPS before 2.0.2 allows remote attackers to have unspecified impact via a malformed compressed raster file, which triggers a buffer overflow.
CVE-2015-0622
The Wireless Intrusion Detection (aka WIDS) functionality on Cisco Wireless LAN Controller (WLC) devices allows remote attackers to cause a denial of service (device outage) via crafted packets that are improperly handled during rendering of the Signature Events Summary page, aka Bug ID CSCus46861.
CVE-2015-0623
Cross-site scripting (XSS) vulnerability in the Administrator report page on Cisco Web Security Appliance (WSA) devices allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka Bug ID CSCus40627.
CVE-2015-0626
The SOAP interface in Cisco Hosted Collaboration Solution (HCS) allows remote attackers to obtain access to system-management tools via crafted Challenge SOAP calls, aka Bug ID CSCuc38114.
CVE-2015-1197
cpio 2.11, when using the --no-absolute-filenames option, allows local users to write to arbitrary files via a symlink attack on a file in an archive.
<a href="http://cwe.mitre.org/data/definitions/61.html">CWE-61: UNIX Symbolic Link (Symlink) Following</a>
CVE-2015-1349
named in ISC BIND 9.7.0 through 9.9.6 before 9.9.6-P2 and 9.10.x before 9.10.1-P2, when DNSSEC validation and the managed-keys feature are enabled, allows remote attackers to cause a denial of service (assertion failure and daemon exit, or daemon crash) by triggering an incorrect trust-anchor management scenario in which no key is ready for use.
CVE-2015-1515
The dwall.sys driver in SoftSphere DefenseWall Personal Firewall 3.24 allows local users to write data to arbitrary memory locations, and consequently gain privileges, via a crafted 0x00222000, 0x00222004, 0x00222008, 0x0022200c, or 0x00222010 IOCTL call.
CVE-2015-1585
Fat Free CRM before 0.13.6 allows remote attackers to conduct cross-site request forgery (CSRF) attacks via a request without the authenticity_token, as demonstrated by a crafted HTML page that creates a new administrator account.
CVE-2015-1587
Unrestricted file upload vulnerability in file_to_index.php in Maarch LetterBox 2.8 and earlier and GEC/GED 1.4 and earlier allows remote attackers to execute arbitrary PHP code by uploading a file with a PHP extension, then accessing it via a request to a predictable filename in tmp/.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-1592
Movable Type Pro, Open Source, and Advanced before 5.2.12 and Pro and Advanced 6.0.x before 6.0.7 does not properly use the Perl Storable::thaw function, which allows remote attackers to include and execute arbitrary local Perl files and possibly execute arbitrary code via unspecified vectors.
CVE-2015-1603
Multiple cross-site scripting (XSS) vulnerabilities in Adminsystems CMS before 4.0.2 allow remote attackers to inject arbitrary web script or HTML via the (1) page parameter to index.php or (2) id parameter in a users_users action to asys/site/system.php.
CVE-2015-1604
Unrestricted file upload vulnerability in asys/site/files.php in Adminsystems CMS before 4.0.2 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in upload/files/.
CVE-2015-1614
Multiple cross-site request forgery (CSRF) vulnerabilities in the Image Metadata Cruncher plugin for WordPress allow remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via the (1) image_metadata_cruncher[alt] or (2) image_metadata_cruncher[caption] parameter in an update action in the image_metadata_cruncher_title page to wp-admin/options.php or (3) custom image meta tag to the image metadata cruncher page.
CVE-2015-1879
Cross-site scripting (XSS) vulnerability in the Google Doc Embedder plugin before 2.5.19 for WordPress allows remote attackers to inject arbitrary web script or HTML via the profile parameter in an edit action in the gde-settings page to wp-admin/options-general.php.
