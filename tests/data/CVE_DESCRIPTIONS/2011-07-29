CVE-2011-2400
Cross-site scripting (XSS) vulnerability in HP SiteScope 9.x, 10.x, and 11.x allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-2401
Session fixation vulnerability in HP SiteScope 9.x, 10.x, and 11.x allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html
'CWE-384: Session Fixation'
CVE-2011-2522
Multiple cross-site request forgery (CSRF) vulnerabilities in the Samba Web Administration Tool (SWAT) in Samba 3.x before 3.5.10 allow remote attackers to hijack the authentication of administrators for requests that (1) shut down daemons, (2) start daemons, (3) add shares, (4) remove shares, (5) add printers, (6) remove printers, (7) add user accounts, or (8) remove user accounts, as demonstrated by certain start, stop, and restart parameters to the status program.
CVE-2011-2694
Cross-site scripting (XSS) vulnerability in the chg_passwd function in web/swat.c in the Samba Web Administration Tool (SWAT) in Samba 3.x before 3.5.10 allows remote authenticated administrators to inject arbitrary web script or HTML via the username parameter to the passwd program (aka the user field to the Change Password page).
Per: http://www.samba.org/samba/security/CVE-2011-2694

'Note that SWAT must be enabled in order for this vulnerability to be exploitable. By default, SWAT is *not* enabled on a Samba install.'

CVE-2011-2697
foomatic-rip-hplip in HP Linux Imaging and Printing (HPLIP) 3.11.5 allows remote attackers to execute arbitrary code via a crafted *FoomaticRIPCommandLine field in a .ppd file.
CVE-2011-2959
Stack-based buffer overflow in the Open Database Connectivity (ODBC) service (Odbcixv9se.exe) in 7-Technologies Interactive Graphical SCADA System (IGSS) 9 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted packet to TCP port 22202.
CVE-2011-2960
Heap-based buffer overflow in httpsvr.exe 6.0.5.3 in Sunway ForceControl 6.1 SP1, SP2, and SP3 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted URL.
CVE-2011-2961
Heap-based buffer overflow in AngelServer.exe 6.0.11.3 in Sunway pNetPower allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted UDP packet.
CVE-2011-2962
Multiple stack-based buffer overflows in Invensys Wonderware Information Server 3.1, 4.0, and 4.0 SP1 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via two unspecified ActiveX controls.
CVE-2011-2963
TCPUploadServer.exe in Progea Movicon 11.2 before Build 1084 does not require authentication for critical functions, which allows remote attackers to obtain sensitive information, delete files, execute arbitrary programs, or cause a denial of service (crash) via a crafted packet to TCP port 10651.
CVE-2011-2964
foomaticrip.c in foomatic-rip in foomatic-filters in Foomatic 4.0.6 allows remote attackers to execute arbitrary code via a crafted *FoomaticRIPCommandLine field in a .ppd file, a different vulnerability than CVE-2011-2697.
