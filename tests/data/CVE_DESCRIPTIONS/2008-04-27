CVE-2008-1966
Multiple buffer overflows in the JAR file administration routines in the BSU JAVA subcomponent in IBM DB2 8 before FP16, 9.1 before FP4a, and 9.5 before FP1 allow remote authenticated users to cause a denial of service (instance crash) via a call to the (1) RECOVERJAR or (2) REMOVE_JAR procedure with a crafted parameter, related to (a) sqlj.install_jar and (b) sqlj.replace_jar.
http://www-1.ibm.com/support/docview.wss?uid=swg21255572http://www-1.ibm.com/support/docview.wss?uid=swg21287889
http://www-1.ibm.com/support/docview.wss?uid=swg21256235
CVE-2008-1967
Cross-site scripting (XSS) vulnerability in CFLogon/CFLogon.asp in Cezanne 6.5.1 and 7 allows remote attackers to inject arbitrary web script or HTML via the SleUserName parameter.
CVE-2008-1968
Multiple SQL injection vulnerabilities in Cezanne 7 allow remote authenticated users to execute arbitrary SQL commands via the FUNID parameter to (1) CFLookup.asp and (2) CznCommon/CznCustomContainer.asp.
CVE-2008-1969
Multiple cross-site scripting (XSS) vulnerabilities in Cezanne 6.5.1 and 7 allow remote attackers to inject arbitrary web script or HTML via the (1) LookUPId and (2) CbFun parameters to (a) CFLookUP.asp; (3) TitleParms, (4) WidgetsHeights, (5) WidgetsLinks, and (6) WidgetsTitles parameters to (b) CznCommon/CznCustomContainer.asp, (7) CFTARGET parameter to (c) home.asp, (8) PersonOid parameter to (d) PeopleWeb/Cards/CVCard.asp, (9) DESTLINKOID and PersonOID parameters to (e) PeopleWeb/Cards/PayrollCard.asp, and the (10) FolderTemplateId and (11) FolderTemplateName parameters to (f) PeopleWeb/CznDocFolder/CznDFStartProcess.asp.
CVE-2008-1970
muCommander before 0.8.2 stores credentials.xml with insecure permissions, which allows local users to obtain credentials.
CVE-2008-1971
phShoutBox Final 1.5 and earlier only checks passwords when specified in $_POST, which allows remote attackers to gain privileges by setting the (1) phadmin cookie to admin.php, or (2) in 1.4 and earlier, the ssbadmin cookie to shoutadmin.php.
CVE-2008-1972
Multiple cross-site scripting (XSS) vulnerabilities in the user account creation feature in Exponent CMS 0.96.6-GA20071003 and earlier, when the Allow Registration? configuration option is enabled, allow remote attackers to inject arbitrary web script or HTML via the (1) username, (2) firstname, (3) lastname, and (4) e-mail address fields.  NOTE: some of these details are obtained from third party information.
Patch Link: http://www.exponentcms.org/index.php?action=view&id=64&module=newsmodule&src=%40random44fe03276195
CVE-2008-1973
Heap-based buffer overflow in SubEdit Player build 4056 and 4066 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a long subtitle file.
CVE-2008-1974
Cross-site scripting (XSS) vulnerability in addevent.php in Horde Kronolith 2.1.7, Groupware Webmail Edition 1.0.6, and Groupware 1.0.5 allows remote attackers to inject arbitrary web script or HTML via the url parameter.
CVE-2008-1975
SQL injection vulnerability in index.php in E-RESERV 2.1 allows remote attackers to execute arbitrary SQL commands via the ID_loc parameter.
CVE-2008-1976
Multiple cross-site scripting (XSS) vulnerabilities in the Drupal modules (1) Internationalization (i18n) 5.x before 5.x-2.3 and 5.x-1.1 and 6.x before 6.x-1.0 beta 1; and (2) Localizer 5.x before 5.x-3.4, 5.x-2.1, and 5.x-1.11; allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-1977
Cross-site request forgery (CSRF) vulnerability in the Internationalization (i18n) Drupal module 5.x before 5.x-2.3 and 5.x-1.1, and 6.x before 6.x-1.0 beta 1, allows remote attackers to change node translation relationships via unspecified vectors.
CVE-2008-1978
Cross-site scripting (XSS) vulnerability in the Ubercart 5.x before 5.x-1.0 rc3 module for Drupal allows remote authenticated users to inject arbitrary web script or HTML via node titles related to unspecified product features, a different vector than CVE-2008-1428.
CVE-2008-1979
The Discovery Service (casdscvc) in CA ARCserve Backup 12.0.5454.0 and earlier allows remote attackers to cause a denial of service (crash) via a packet with a large integer value used in an increment to TCP port 41523, which triggers a buffer over-read.
CVE-2008-1980
Cross-site scripting (XSS) vulnerability in E-Publish 5.x before 5.x-1.1 and 6.x before 6.x-1.0 beta1, a Drupal module, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-1981
Cross-site request forgery (CSRF) vulnerability in E-Publish 5.x before 5.x-1.1 and 6.x before 6.x-1.0 beta1, a Drupal module, allows remote attackers to perform unauthorized actions as other users via unspecified vectors.
CVE-2008-1982
SQL injection vulnerability in ss_load.php in the Spreadsheet (wpSS) 0.6 and earlier plugin for WordPress allows remote attackers to execute arbitrary SQL commands via the ss_id parameter.
CVE-2008-1983
Cross-site scripting (XSS) vulnerability in Advanced Electron Forum (AEF) 1.0.6 allows remote attackers to inject arbitrary web script or HTML via the beg parameter in a members action to index.php.
CVE-2008-1984
The eTrust Common Services (Transport) Daemon (eCSqdmn) in CA Secure Content Manager 8.0.28000.511 and earlier allows remote attackers to cause a denial of service (crash or CPU consumption) via a malformed packet to TCP port 1882.
CVE-2008-1985
Cross-site scripting (XSS) vulnerability in base.php in DigitalHive 2.0 RC2 allows remote attackers to inject arbitrary web script or HTML via the mt parameter, possibly related to membres.php.
CVE-2008-1986
Cross-site scripting (XSS) vulnerability in liste_article.php in Blog Pixel Motion (aka PixelMotion) allows remote attackers to inject arbitrary web script or HTML via the jours parameter.
CVE-2008-1987
Cross-site scripting (XSS) vulnerability in search.php in EncapsGallery 2.0.2 allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2008-1988
Unrestricted file upload vulnerability in the file_upload function in core/misc.class.php in EncapsGallery 2.0.2 allows remote authenticated administrators to upload and execute arbitrary PHP files by uploading a file with an executable extension, then accessing it via a direct request to the file in the rwx_gallery directory.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-1989
PHP remote file inclusion vulnerability in 123flashchat.php in the 123 Flash Chat 6.8.0 module for e107, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the e107path parameter.
CVE-2008-1990
Multiple SQL injection vulnerabilities in Acidcat CMS 3.4.1 allow remote attackers to execute arbitrary SQL commands via the (1) cID parameter to default.asp and the (2) username parameter to main_login2.asp.
CVE-2008-1991
Cross-site scripting (XSS) vulnerability in admin_colors_swatch.asp in Acidcat CMS 3.4.1 allows remote attackers to inject arbitrary web script or HTML via the field parameter.
CVE-2008-1992
Acidcat CMS 3.4.1 does not properly restrict access to (1) default_mail_aspemail.asp, (2) default_mail_cdosys.asp or (3) default_mail_jmail.asp, which allows remote attackers to bypass restrictions and relay email messages with modified From, FromName, and To fields.
CVE-2008-1993
Acidcat CMS 3.4.1 does not restrict access to the FCKEditor component, which allows remote attackers to upload arbitrary files.
CVE-2008-1994
Multiple stack-based buffer overflows in (a) acon.c, (b) menu.c, and (c) child.c in Acon 1.0.5-5 through 1.0.5-7 allow local users to execute arbitrary code via (1) a long HOME environment variable or (2) a large number of terminal columns.
