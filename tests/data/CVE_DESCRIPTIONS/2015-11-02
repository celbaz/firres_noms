CVE-2015-1775
Server-side request forgery (SSRF) vulnerability in the proxy endpoint (api/v1/proxy) in Apache Ambari before 2.1.0 allows remote authenticated users to conduct port scans and access unsecured services via a crafted REST call.
<a href="https://cwe.mitre.org/data/definitions/918.html">CWE-918: Server-Side Request Forgery (SSRF)</a>
CVE-2015-3186
Cross-site scripting (XSS) vulnerability in Apache Ambari before 2.1.0 allows remote authenticated cluster operator users to inject arbitrary web script or HTML via the note field in a configuration change.
CVE-2015-3270
Apache Ambari before 2.0.2 or 2.1.x before 2.1.1 allows remote authenticated users to gain administrative privileges via unspecified vectors, possibly related to changing passwords.
CVE-2015-5210
Open redirect vulnerability in Apache Ambari before 2.1.2 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the targetURI parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-5291
Heap-based buffer overflow in PolarSSL 1.x before 1.2.17 and ARM mbed TLS (formerly PolarSSL) 1.3.x before 1.3.14 and 2.x before 2.1.2 allows remote SSL servers to cause a denial of service (client crash) and possibly execute arbitrary code via a long hostname to the server name indication (SNI) extension, which is not properly handled when creating a ClientHello message.  NOTE: this identifier has been SPLIT per ADT3 due to different affected version ranges. See CVE-2015-8036 for the session ticket issue that was introduced in 1.3.0.
CVE-2015-5308
Multiple SQL injection vulnerabilities in cs_admin_users.php in the wp-championship plugin 5.8 for WordPress allow remote attackers to execute arbitrary SQL commands via the (1) user, (2) isadmin, (3) mail service, (4) mailresceipt, (5) stellv, (6) champtipp, (7) tippgroup, or (8) userid parameter.
CVE-2015-5470
The label decompression functionality in PowerDNS Recursor before 3.6.4 and 3.7.x before 3.7.3 and Authoritative (Auth) Server before 3.3.3 and 3.4.x before 3.4.5 allows remote attackers to cause a denial of service (CPU consumption or crash) via a request with a long name that refers to itself.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-1868.
CVE-2015-5534
Multiple cross-site request forgery (CSRF) vulnerabilities in Oxwall before 1.8 allow remote attackers to hijack the authentication of administrators for requests that (1) put the website under maintenance via the maintenance_enable parameter or (2) conduct cross-site scripting (XSS) attacks via the maintenance_text parameter to admin/pages/maintenance.
CVE-2015-6031
Buffer overflow in the IGDstartelt function in igd_desc_parse.c in the MiniUPnP client (aka MiniUPnPc) before 1.9.20150917 allows remote UPNP servers to cause a denial of service (application crash) and possibly execute arbitrary code via an "oversized" XML element name.
CVE-2015-8036
Heap-based buffer overflow in ARM mbed TLS (formerly PolarSSL) 1.3.x before 1.3.14 and 2.x before 2.1.2 allows remote SSL servers to cause a denial of service (client crash) and possibly execute arbitrary code via a long session ticket name to the session ticket extension, which is not properly handled when creating a ClientHello message to resume a session.  NOTE: this identifier was SPLIT from CVE-2015-5291 per ADT3 due to different affected version ranges.
CVE-2015-8037
Multiple cross-site scripting (XSS) vulnerabilities in the Graphical User Interface (GUI) in Fortinet FortiManager before 5.2.4 allow remote attackers to inject arbitrary web script or HTML via the (1) SOMVpnSSLPortalDialog or (2) FGDMngUpdHistory.
CVE-2015-8038
Multiple cross-site scripting (XSS) vulnerabilities in the Graphical User Interface (GUI) in Fortinet FortiManager before 5.2.4 allow remote attackers to inject arbitrary web script or HTML via the (1) sharedjobmanager or (2) SOMServiceObjDialog.
CVE-2015-8039
Samsung SmartViewer allows remote attackers to execute arbitrary code via unspecified vectors to the (1) DVRSetupSave method in the STWAxConfig control or (2) SendCustomPacket method in the STWAxConfigNVR control, which trigger an untrusted pointer dereference.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-8040
The rtsp_getdlsendtime method in the CNC_Ctrl control in Samsung SmartViewer allows remote attackers to execute arbitrary code via an index value.
