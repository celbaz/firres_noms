CVE-2016-6538
The TrackR Bravo mobile app stores the account password used to authenticate to the cloud API in cleartext in the cache.db file. Updated apps, version 5.1.6 for iOS and 2.2.5 for Android, have been released by the vendor to address the vulnerabilities in CVE-2016-6538, CVE-2016-6539, CVE-2016-6540 and CVE-2016-6541.
CVE-2016-6539
The Trackr device ID is constructed of a manufacturer identifier of four zeroes followed by the BLE MAC address in reverse. The MAC address can be obtained by being in close proximity to the Bluetooth device, effectively exposing the device ID. The ID can be used to track devices. Updated apps, version 5.1.6 for iOS and 2.2.5 for Android, have been released by the vendor to address the vulnerabilities in CVE-2016-6538, CVE-2016-6539, CVE-2016-6540 and CVE-2016-6541.
CVE-2016-6540
Unauthenticated access to the cloud-based service maintained by TrackR Bravo is allowed for querying or sending GPS data for any Trackr device by using the tracker ID number which can be discovered as described in CVE-2016-6539. Updated apps, version 5.1.6 for iOS and 2.2.5 for Android, have been released by the vendor to address the vulnerabilities in CVE-2016-6538, CVE-2016-6539, CVE-2016-6540 and CVE-2016-6541.
CVE-2016-6541
TrackR Bravo device allows unauthenticated pairing, which enables unauthenticated connected applications to write to various device attributes. Updated apps, version 5.1.6 for iOS and 2.2.5 for Android, have been released by the vendor to address the vulnerabilities in CVE-2016-6538, CVE-2016-6539, CVE-2016-6540 and CVE-2016-6541.
CVE-2017-11088
Improper Input Validation in Linux io-prefetch in Snapdragon Mobile and Snapdragon Wear, A SQL injection vulnerability exists in versions MSM8909W, MSM8996AU, SD 210/SD 212/SD 205, SD 430, SD 450, SD 617, SD 625, SD 650/52, SD 820, SD 835, SD 845.
CVE-2017-1237
IBM Jazz based applications are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 124355.
CVE-2017-1238
IBM Quality Manager (RQM) 5.0.x and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 124356.
CVE-2017-1239
IBM Quality Manager (RQM) 5.0.x and 6.0 through 6.0.5 could reveal sensitive information in HTTP 500 Internal Server Error responses. IBM X-Force ID: 124357.
CVE-2017-1242
IBM Quality Manager (RQM) 5.0.x and 6.0 through 6.0.5 are vulnerable to HTML injection. A remote attacker could inject malicious HTML code, which when viewed, would be executed in the victim's Web browser within the security context of the hosting site. IBM X-Force ID: 124524.
CVE-2017-1248
IBM Quality Manager (RQM) 5.0.x and 6.0 through 6.0.5 are vulnerable to HTML injection. A remote attacker could inject malicious HTML code, which when viewed, would be executed in the victim's Web browser within the security context of the hosting site. IBM X-Force ID: 124628.
CVE-2017-1329
IBM Quality Manager (RQM) 5.0.x and 6.0 through 6.0.5 are vulnerable to HTML injection. A remote attacker could inject malicious HTML code, which when viewed, would be executed in the victim's Web browser within the security context of the hosting site. IBM X-Force ID: 126231.
CVE-2017-14872
While flashing a meta image, a buffer over-read can potentially occur when the number of images are out of the maximum range of 32 in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2017-1488
An undisclosed vulnerability in Jazz common products exists with potential for information disclosure. IBM X-Force ID: 128627.
CVE-2017-14893
While flashing meta image, a buffer over-read may potentially occur when the image size is smaller than the image header size or is smaller than the image header size + total image header entry in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2017-1509
IBM Jazz Foundation products could allow an authenticated user to obtain sensitive information from a stack trace that could be used to aid future attacks. IBM X-Force ID: 129719.
CVE-2017-1559
Multiple IBM Rational products could disclose sensitive information by an attacker that intercepts vulnerable requests. IBM X-Force ID: 131758.
CVE-2017-15824
In Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, the function UpdateDeviceStatus() writes a local stack buffer without initialization to flash memory using WriteToPartition() which may potentially leak memory.
CVE-2017-15851
Lack of copy_from_user and information leak in function "msm_ois_subdev_do_ioctl, file msm_ois.c can lead to a camera crash in all Android releases(Android for MSM, Firefox OS for MSM, QRD Android) from CAF using the Linux kernel
CVE-2017-15856
Due to a race condition while processing the power stats debug file to read status, a double free condition can occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2017-1795
IBM WebSphere MQ 7.5, 8.0, and 9.0 through 9.0.4 could allow a local user to obtain highly sensitive information via trace logs in IBM WebSphere MQ Managed File Transfer. IBM X-Force ID: 137042.
CVE-2017-18158
Possible buffer overflows and array out of bounds accesses in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05 while flashing images.
CVE-2017-18159
In Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, while processing a StrHwPlatform with length smaller than EFICHIPINFO_MAX_ID_LENGTH, an array out of bounds access may occur.
CVE-2017-2665
The skyring-setup command creates random password for mongodb skyring database but it writes password in plain text to /etc/skyring/skyring.conf file which is owned by root but read by local user. Any local user who has access to system running skyring service will be able to get password in plain text.
CVE-2018-10892
The default OCI linux spec in oci/defaults{_linux}.go in Docker/Moby from 1.11 to current does not block /proc/acpi pathnames. The flaw allows an attacker to modify host's hardware like enabling/disabling bluetooth or turning up/down keyboard brightness.
CVE-2018-11124
Cross-site scripting (XSS) vulnerability in Attributes functionality in Open-AudIT Community edition before 2.2.2 allows remote attackers to inject arbitrary web script or HTML via a crafted attribute name of an Attribute.
CVE-2018-11257
Permissions, Privileges, and Access Controls in TA in Snapdragon Mobile has an options that allows RPMB erase for secure devices in versions SD 210/SD 212/SD 205, SD 845, SD 850.
CVE-2018-11258
In ADSP RPC in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear, a Use After Free condition can occur in versions MDM9206, MDM9607, MDM9650, MSM8909W, MSM8996AU, SD 210/SD 212/SD 205, SD 425, SD 450, SD 615/16/SD 415, SD 625, SD 650/52, SD 820, SD 820A, SD 835, SD 845, SDX20.
CVE-2018-11259
Due to Improper Access Control of NAND-based EFS in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear, From fastboot on a NAND-based device, the EFS partition can be erased. Apps processor then has non-secure world full read/write access to the partition until the modem boots and configures the EFS partition addresses in its MPU partition.
CVE-2018-11304
Possible buffer overflow in msm_adsp_stream_callback_put due to lack of input validation of user-provided data that leads to integer overflow in all Android releases(Android for MSM, Firefox OS for MSM, QRD Android) from CAF using the Linux kernel.
CVE-2018-13108
All ADB broadband gateways / routers based on the Epicentro platform are affected by a local root jailbreak vulnerability where attackers are able to gain root access on the device, and extract further information such as sensitive configuration data of the ISP (e.g., VoIP credentials) or attack the internal network of the ISP.
CVE-2018-13109
All ADB broadband gateways / routers based on the Epicentro platform are affected by an authorization bypass vulnerability where attackers are able to access and manipulate settings within the web interface that are forbidden to end users (e.g., by the ISP). An attacker would be able to enable the TELNET server or other settings as well.
CVE-2018-13110
All ADB broadband gateways / routers based on the Epicentro platform are affected by a privilege escalation vulnerability where attackers can gain access to the command line interface (CLI) if previously disabled by the ISP, escalate their privileges, and perform further attacks.
CVE-2018-13346
The mpatch_apply function in mpatch.c in Mercurial before 4.6.1 incorrectly proceeds in cases where the fragment start is past the end of the original data, aka OVE-20180430-0004.
CVE-2018-13347
mpatch.c in Mercurial before 4.6.1 mishandles integer addition and subtraction, aka OVE-20180430-0002.
CVE-2018-13348
The mpatch_decode function in mpatch.c in Mercurial before 4.6.1 mishandles certain situations where there should be at least 12 bytes remaining after the current position in the patch data, but actually are not, aka OVE-20180430-0001.
CVE-2018-13405
The inode_init_owner function in fs/inode.c in the Linux kernel through 4.17.4 allows local users to create files with an unintended group ownership, in a scenario where a directory is SGID to a certain group and is writable by a user who is not a member of that group. Here, the non-member can trigger creation of a plain file whose group ownership is that group. The intended behavior was that the non-member can trigger creation of a directory (but not a plain file) whose group ownership is that group. The non-member can escalate privileges by making the plain file executable and SGID.
CVE-2018-13406
An integer overflow in the uvesafb_setcmap function in drivers/video/fbdev/uvesafb.c in the Linux kernel before 4.17.4 could result in local attackers being able to crash the kernel or potentially elevate privileges because kmalloc_array is not used.
CVE-2018-13407
A CSRF issue was discovered in Jirafeau before 3.4.1. The "delete file" feature on the admin panel is not protected against automated requests and could be abused.
CVE-2018-13408
An issue was discovered in Jirafeau before 3.4.1. The "search file by link" form is affected by reflected XSS that could allow, by targeting an administrator, stealing a session and gaining administrative privileges.
CVE-2018-13409
An issue was discovered in Jirafeau before 3.4.1. The "search file by hash" form is affected by reflected XSS that could allow, by targeting an administrator, stealing a session and gaining administrative privileges.
CVE-2018-13410
** DISPUTED ** Info-ZIP Zip 3.0, when the -T and -TT command-line options are used, allows attackers to cause a denial of service (invalid free and application crash) or possibly have unspecified other impact because of an off-by-one error. NOTE: it is unclear whether there are realistic scenarios in which an untrusted party controls the -TT value, given that the entire purpose of -TT is execution of arbitrary commands.
CVE-2018-1494
IBM DOORS Next Generation (DNG/RRC) 5.0 through 5.0.2 and 6.0 through 6.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141097.
CVE-2018-1542
IBM FileNet Content Manager, IBM Content Foundation, and IBM Case Foundation Administration Console for Content Platform Engine (ACCE) 5.2.1 and 5.5.0 are vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 142597.
CVE-2018-1546
IBM API Connect 5.0.0.0 through 5.0.8.3 could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques. IBM X-Force ID: 142650.
CVE-2018-1555
IBM FileNet Content Manager 5.2.1 and 5.5.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 142892.
CVE-2018-1556
IBM FileNet Content Manager 5.2.1 and 5.5.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 142893.
CVE-2018-1621
IBM WebSphere Application Server 7.0, 8.0, 8.5, and 9.0 could allow a local attacker to obtain clear text password in a trace file caused by improper handling of some datasource custom properties. IBM X-Force ID: 144346.
CVE-2018-1676
IBM Planning Analytics 2.0.0 through 2.0.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 145118.
CVE-2018-3564
In the FastRPC driver in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, a Use After Free condition can occur when mapping on the remote processor fails.
CVE-2018-3569
A buffer over-read can occur during a fast initial link setup (FILS) connection in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-3570
In the cpuidle driver in all Android releases(Android for MSM, Firefox OS for MSM, QRD Android) from CAF using the Linux kernel, the list_for_each macro was not used correctly which could lead to an untrusted pointer dereference.
CVE-2018-3577
While processing fragments, when the fragment count becomes very large, an integer overflow leading to a buffer overflow can occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-3586
An integer overflow to buffer overflow vulnerability exists in the ADSPRPC heap manager in all Android releases(Android for MSM, Firefox OS for MSM, QRD Android) from CAF using the Linux kernel.
CVE-2018-3587
In a firmware memory dump feature in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android), a Use After Free condition can occur.
CVE-2018-3597
In the ADSP RPC driver in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, an arbitrary kernel write can occur.
CVE-2018-3608
A vulnerability in Trend Micro Maximum Security's (Consumer) 2018 (versions 12.0.1191 and below) User-Mode Hooking (UMH) driver could allow an attacker to create a specially crafted packet that could alter a vulnerable system in such a way that malicious code could be injected into other processes.
CVE-2018-5829
In wlan_hdd_cfg80211_set_privacy_ibss() in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, a buffer over-read can potentially occur.
CVE-2018-5830
While processing the HTT_T2H_MSG_TYPE_MGMT_TX_COMPL_IND message, a buffer overflow can potentially occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5831
In the KGSL driver in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, a reference counting error can lead to a Use After Free condition.
CVE-2018-5832
Due to a race condition in a camera driver ioctl handler in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, a Use After Free condition can occur.
CVE-2018-5834
In __wlan_hdd_cfg80211_vendor_scan(), a buffer overwrite can potentially occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5835
If the seq_len is greater then CSR_MAX_RSC_LEN, a buffer overflow in __wlan_hdd_cfg80211_add_key() may occur when copying keyRSC in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5836
In wma_nan_rsp_event_handler() in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, the data_len value is received from firmware and not properly validated which could potentially lead to an out-of-bounds access.
CVE-2018-5838
Improper Validation of Array Index In the adreno OpenGL driver in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear, an out-of-bounds access can occur in SurfaceFlinger.
CVE-2018-5853
A race condition exists in a driver in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-05-05 potentially leading to a use-after-free condition.
CVE-2018-5855
While padding or shrinking a nested wmi packet in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, a buffer over-read can potentially occur.
CVE-2018-5858
In the audio debugfs in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, out of bounds access can occur.
CVE-2018-5859
Due to a race condition in the MDSS MDP driver in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, a Use After Free condition can occur.
CVE-2018-5862
In __wlan_hdd_cfg80211_vendor_scan() in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, when SCAN_SSIDS and QCA_WLAN_VENDOR_ATTR_SCAN_FREQUENCIES are parsed, a buffer overwrite can potentially occur.
CVE-2018-5864
While processing a WMI_APFIND event in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, a buffer over-read and information leak can potentially occur.
CVE-2018-5865
While processing a debug log event from firmware in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, an integer underflow and/or buffer over-read can occur.
CVE-2018-5872
While parsing over-the-air information elements in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05, the use of an out-of-range pointer offset can occur.
CVE-2018-5873
An issue was discovered in the __ns_get_path function in fs/nsfs.c in the Linux kernel before 4.11. Due to a race condition when accessing files, a Use After Free condition can occur. This also affects all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-07-05.
CVE-2018-5874
While parsing an mp4 file, a stack-based buffer overflow can occur in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5875
While parsing an mp4 file, an integer overflow leading to a buffer overflow can occur in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5876
While parsing an mp4 file, a buffer overflow can occur in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5878
While sending the response to a RIL_REQUEST_GET_SMSC_ADDRESS message, a buffer overflow can occur in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5882
While parsing a Flac file with a corrupted comment block, a buffer over-read can occur in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5884
Improper Access Control in Multimedia in Snapdragon Mobile and Snapdragon Wear, Non-standard applications without permission may acquire permission of Qualcomm-specific proprietary intents.
CVE-2018-5885
While loading dynamic fonts, a buffer overflow may occur if the number of segments in the font file is out of range in Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5886
A pointer in an ADSPRPC command is not properly validated in all Android releases from CAF using the Linux kernel (Android for MSM, Firefox OS for MSM, QRD Android), which can lead to kernel memory being accessed.
CVE-2018-5887
While processing the USB StrSerialDescriptor array, an array index out of bounds can occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5888
While processing the system path, an out of bounds access can occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5889
While processing a compressed kernel image, a buffer overflow can occur in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5890
If the fdt_totalsize is reported as 0 for the current device tree, it bypasses an error check for a valid device tree in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5891
While processing modem SSR after IMS is registered, the IMS data daemon is restarted but the ipc_dataHandle is no longer available. Consequently, the DPL thread frees the internal memory for dataDHandle but the local variable pointer is not updated which can lead to a Use After Free condition in Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5892
The Touch Pal application can collect user behavior data without awareness by the user in Snapdragon Mobile and Snapdragon Wear.
CVE-2018-5893
While processing a message from firmware in htt_t2h_msg_handler_fast() in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, a buffer overwrite can occur.
CVE-2018-5894
Improper Validation of Array Index in Multimedia While parsing an mp4 file in Snapdragon Automobile, Snapdragon Mobile and Snapdragon Wear, an out-of-bounds access can occur.
CVE-2018-5895
Buffer over-read may happen in wma_process_utf_event() due to improper buffer length validation before writing into param_buf->num_wow_packet_buffer in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5896
In Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, kernel panic may happen due to out-of-bound read, caused by not checking source buffer length against length of packet stream to be copied.
CVE-2018-5897
While reading the data from buffer in dci_process_ctrl_status() there can be buffer over-read problem if the len is not checked correctly in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5898
Integer overflow can occur in msm_pcm_adsp_stream_cmd_put() function if the user supplied data "param_length" goes beyond certain limit in Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05.
CVE-2018-5899
In Android releases from CAF using the linux kernel (Android for MSM, Firefox OS for MSM, QRD Android) before security patch level 2018-06-05, whenever TDLS connection is setup, we are freeing the netbuf in ol_tx_completion_handler and after that, we are accessing it in NBUF_UPDATE_TX_PKT_COUNT causing a use after free.
CVE-2018-5907
Possible buffer overflow in msm_adsp_stream_callback_put due to lack of input validation of user-provided data that leads to integer overflow in all Android releases(Android for MSM, Firefox OS for MSM, QRD Android) from CAF using the Linux kernel.
CVE-2018-8929
Improper restriction of communication channel to intended endpoints vulnerability in HTTP daemon in Synology SSL VPN Client before 1.2.4-0224 allows remote attackers to conduct man-in-the-middle attacks via a crafted payload.
