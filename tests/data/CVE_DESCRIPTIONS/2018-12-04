CVE-2018-0468
A vulnerability in the configuration of a local database installed as part of the Cisco Energy Management Suite (CEMS) could allow an authenticated, local attacker to access and alter confidential data. The vulnerability is due to the installation of the PostgreSQL database with unchanged default access credentials. An attacker could exploit this vulnerability by logging in to the machine where CEMS is installed and establishing a local connection to the database. The fix for this vulnerability randomizes the database access password in new installations; however, the fix will not change the password for existing installations. Users are required to manually change the password, as documented in the Workarounds section of this advisory. There are workarounds that address this vulnerability.
CVE-2018-11347
The YunoHost 2.7.2 through 2.7.14 web application is affected by one HTTP Response Header Injection. This flaw allows an attacker to inject, into the response from the server, one or several HTTP Header. It requires an interaction with the user to send him the malicious link. It could be used to perform other attacks such as user redirection to a malicious website, HTTP response splitting, or HTTP cache poisoning.
CVE-2018-11348
Two XSS vulnerabilities are located in the profile edition page of the user panel of the YunoHost 2.7.2 through 2.7.14 web application. By injecting a JavaScript payload, these flaws could be used to manipulate a user's session.
CVE-2018-12305
Cross-site scripting in File Explorer in ASUSTOR ADM version 3.1.1 allows attackers to execute JavaScript by uploading SVG images with embedded JavaScript.
CVE-2018-12306
Directory Traversal in File Explorer in ASUSTOR ADM version 3.1.1 allows attackers to view arbitrary files by modifying the "file1" URL parameter, a similar issue to CVE-2018-11344.
CVE-2018-12307
OS command injection in user.cgi in ASUSTOR ADM version 3.1.1 allows attackers to execute system commands as root via the "name" POST parameter.
CVE-2018-12308
Encryption key disclosure in share.cgi in ASUSTOR ADM version 3.1.1 allows attackers to obtain the encryption key via the "encrypt_key" URL parameter.
CVE-2018-12309
Directory Traversal in upload.cgi in ASUSTOR ADM version 3.1.1 allows attackers to upload files to arbitrary locations by modifying the "path" URL parameter.  NOTE: the "filename" POST parameter is covered by CVE-2018-11345.
CVE-2018-12310
Cross-site scripting in the Login page in ASUSTOR ADM version 3.1.1 allows attackers to execute JavaScript via the System Announcement feature.
CVE-2018-12311
Cross-site scripting vulnerability in File Explorer in ASUSTOR ADM version 3.1.1 allows attackers to execute arbitrary JavaScript when a file is moved via a malicious filename.
CVE-2018-12312
OS command injection in user.cgi in ASUSTOR ADM version 3.1.1 allows attackers to execute system commands as root via the "secret_key" URL parameter.
CVE-2018-12313
OS command injection in snmp.cgi in ASUSTOR ADM version 3.1.1 allows attackers to execute system commands without authentication via the "rocommunity" URL parameter.
CVE-2018-12314
Directory Traversal in downloadwallpaper.cgi in ASUSTOR ADM version 3.1.1 allows attackers to download arbitrary files by manipulating the "file" and "folder" URL parameters.
CVE-2018-12315
Missing verification of a password in ASUSTOR ADM version 3.1.1 allows attackers to change account passwords without entering the current password.
CVE-2018-12316
OS Command Injection in upload.cgi in ASUSTOR ADM version 3.1.1 allows attackers to execute system commands by modifying the filename POST parameter.
CVE-2018-12317
OS command injection in group.cgi in ASUSTOR ADM version 3.1.1 allows attackers to execute system commands as root by modifying the "name" POST parameter.
CVE-2018-12318
Information disclosure in the SNMP settings page in ASUSTOR ADM version 3.1.1 allows attackers to obtain the SNMP password in cleartext.
CVE-2018-12319
Denial-of-service in the login page of ASUSTOR ADM 3.1.1 allows attackers to prevent users from signing in by placing malformed text in the title.
CVE-2018-16478
A Path Traversal in simplehttpserver versions <=0.2.1 allows to list any file in another folder of web root.
CVE-2018-16628
panel/login in Kirby v2.5.12 allows XSS via a blog name.
CVE-2018-16629
panel/uploads/#elf_l1_XA in Subrion CMS v4.2.1 allows XSS via an SVG file with JavaScript in a SCRIPT element.
CVE-2018-16631
Subrion CMS v4.2.1 allows XSS via the panel/configuration/general/ SITE TITLE parameter.
CVE-2018-16633
Pluck v4.7.7 allows XSS via the admin.php?action=editpage&page= page title.
CVE-2018-16634
Pluck v4.7.7 allows CSRF via admin.php?action=settings.
CVE-2018-17157
In FreeBSD before 11.2-STABLE(r340854) and 11.2-RELEASE-p5, an integer overflow error when handling opcodes can cause memory corruption by sending a specially crafted NFSv4 request. Unprivileged remote users with access to the NFS server may be able to execute arbitrary code.
CVE-2018-17158
In FreeBSD before 11.2-STABLE(r340854) and 11.2-RELEASE-p5, an integer overflow error can occur when handling the client address length field in an NFSv4 request. Unprivileged remote users with access to the NFS server can crash the system by sending a specially crafted NFSv4 request.
CVE-2018-17159
In FreeBSD before 11.2-STABLE(r340854) and 11.2-RELEASE-p5, the NFS server lacks a bounds check in the READDIRPLUS NFS request. Unprivileged remote users with access to the NFS server can cause a resource exhaustion by forcing the server to allocate an arbitrarily large memory allocation.
CVE-2018-17160
In FreeBSD before 11.2-STABLE(r341486) and 11.2-RELEASE-p6, insufficient bounds checking in one of the device models provided by bhyve can permit a guest operating system to overwrite memory in the bhyve host possibly permitting arbitrary code execution. A guest OS using a firmware image can cause the bhyve process to crash, or possibly execute arbitrary code on the host as root.
CVE-2018-17939
An issue was discovered in GitLab Community and Enterprise Edition 11.1.x before 11.1.8, 11.2.x before 11.2.5, and 11.3.x before 11.3.2. There is Information Exposure via the merge request JSON endpoint.
CVE-2018-17975
An issue was discovered in GitLab Community Edition 11.x before 11.1.8, 11.2.x before 11.2.5, and 11.3.x before 11.3.2. There is Information Exposure via the GFM markdown API.
CVE-2018-17976
An issue was discovered in GitLab Community Edition 11.x before 11.1.8, 11.2.x before 11.2.5, and 11.3.x before 11.3.2. There is Information Exposure via Epic change descriptions.
CVE-2018-18640
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It has Information Exposure Through Browser Caching.
CVE-2018-18641
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It has Cleartext Storage of Sensitive Information.
CVE-2018-18642
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It has XSS.
CVE-2018-18644
An issue was discovered in GitLab Community and Enterprise Edition 11.x before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It allows Information Exposure via a Gitlab Prometheus integration.
CVE-2018-18645
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It allows for Information Exposure via unsubscribe links in email replies.
CVE-2018-18646
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It allows SSRF.
CVE-2018-18647
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It has Missing Authorization.
CVE-2018-18648
An issue was discovered in GitLab Community and Enterprise Edition before 11.2.7, 11.3.x before 11.3.8, and 11.4.x before 11.4.3. It has Information Exposure Through an Error Message.
CVE-2018-18843
The Kubernetes integration in GitLab Enterprise Edition 11.x before 11.2.8, 11.3.x before 11.3.9, and 11.4.x before 11.4.4 has SSRF.
CVE-2018-18989
In CX-One Versions 4.42 and prior (CX-Programmer Versions 9.66 and prior and CX-Server Versions 5.0.23 and prior), when processing project files, the application fails to check if it is referencing freed memory. An attacker could use a specially crafted project file to exploit and execute code under the privileges of the application.
CVE-2018-18991
Reflected cross-site scripting (non-persistent) in SCADA WebServer (Versions prior to 2.03.0001) could allow an attacker to send a crafted URL that contains JavaScript, which can be reflected off the web application to the victim's browser.
CVE-2018-18993
Two stack-based buffer overflow vulnerabilities have been discovered in CX-One Versions 4.42 and prior (CX-Programmer Versions 9.66 and prior and CX-Server Versions 5.0.23 and prior). When processing project files, the application allows input data to exceed the buffer. An attacker could use a specially crafted project file to overflow the buffer and execute code under the privileges of the application.
CVE-2018-19591
In the GNU C Library (aka glibc or libc6) through 2.28, attempting to resolve a crafted hostname via getaddrinfo() leads to the allocation of a socket descriptor that is not closed. This is related to the if_nametoindex() function.
CVE-2018-19837
In LibSass prior to 3.5.5, Sass::Eval::operator()(Sass::Binary_Expression*) inside eval.cpp allows attackers to cause a denial-of-service resulting from stack consumption via a crafted sass file, because of certain incorrect parsing of '%' as a modulo operator in parser.cpp.
CVE-2018-19838
In LibSass prior to 3.5.5, functions inside ast.cpp for IMPLEMENT_AST_OPERATORS expansion allow attackers to cause a denial-of-service resulting from stack consumption via a crafted sass file, as demonstrated by recursive calls involving clone(), cloneChildren(), and copy().
CVE-2018-19839
In LibSass prior to 3.5.5, the function handle_error in sass_context.cpp allows attackers to cause a denial-of-service resulting from a heap-based buffer over-read via a crafted sass file.
CVE-2018-19840
The function WavpackPackInit in pack_utils.c in libwavpack.a in WavPack through 5.1.0 allows attackers to cause a denial-of-service (resource exhaustion caused by an infinite loop) via a crafted wav audio file because WavpackSetConfiguration64 mishandles a sample rate of zero.
CVE-2018-19841
The function WavpackVerifySingleBlock in open_utils.c in libwavpack.a in WavPack through 5.1.0 allows attackers to cause a denial-of-service (out-of-bounds read and application crash) via a crafted WavPack Lossless Audio file, as demonstrated by wvunpack.
CVE-2018-19842
getToken in libr/asm/p/asm_x86_nz.c in radare2 before 3.1.0 allows attackers to cause a denial of service (stack-based buffer over-read) via crafted x86 assembly data, as demonstrated by rasm2.
CVE-2018-19843
opmov in libr/asm/p/asm_x86_nz.c in radare2 before 3.1.0 allows attackers to cause a denial of service (buffer over-read) via crafted x86 assembly data, as demonstrated by rasm2.
CVE-2018-19849
An issue was discovered in YzmCMS 5.2. XSS exists via the admin/content/search.html searinfo parameter.
CVE-2018-19853
An issue was discovered in hitshop through 2014-07-15. There is an elevation-of-privilege vulnerability (that allows control over the whole web site) via the admin.php/user/add URI because a storekeeper account (which is supposed to have only privileges for commodity management) can add an administrator account.
CVE-2018-19854
An issue was discovered in the Linux kernel before 4.19.3. crypto_report_one() and related functions in crypto/crypto_user.c (the crypto user configuration API) do not fully initialize structures that are copied to userspace, potentially leaking sensitive memory to user programs. NOTE: this is a CVE-2013-2547 regression but with easier exploitability because the attacker does not need a capability (however, the system must have the CONFIG_CRYPTO_USER kconfig option).
CVE-2018-5496
Data ONTAP operating in 7-Mode versions prior to 8.2.5P2 are susceptible to a vulnerability which discloses sensitive information to an unauthorized user.
CVE-2018-6085
Re-entry of a destructor in Networking Disk Cache in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code via a crafted HTML page.
CVE-2018-6086
A double-eviction in the Incognito mode cache that lead to a user-after-free in Networking Disk Cache in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code via a crafted HTML page.
CVE-2018-6087
A use-after-free in WebAssembly in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2018-6088
An iterator-invalidation bug in PDFium in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted PDF file.
CVE-2018-6089
A lack of CORS checks, after a Service Worker redirected to a cross-origin PDF, in Service Worker in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to leak limited cross-origin data via a crafted HTML page.
CVE-2018-6090
An integer overflow that lead to a heap buffer-overflow in Skia in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2018-6092
An integer overflow on 32-bit systems in WebAssembly in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2018-6094
Inline metadata in GarbageCollection in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2018-6095
Inappropriate dismissal of file picker on keyboard events in Blink in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to read local files via a crafted HTML page.
CVE-2018-6098
Incorrect handling of confusable characters in URL Formatter in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to perform domain spoofing via IDN homographs via a crafted domain name.
CVE-2018-6099
A lack of CORS checks in Blink in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to leak limited cross-origin data via a crafted HTML page.
CVE-2018-6101
A lack of host validation in DevTools in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to execute arbitrary code via a crafted HTML page, if the user is running a remote DevTools debugging server.
CVE-2018-6102
Missing confusable characters in Internationalization in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted domain name.
CVE-2018-6103
A stagnant permission prompt in Prompts in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to bypass permission policy via a crafted HTML page.
CVE-2018-6104
Incorrect handling of confusable characters in URL Formatter in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to perform domain spoofing via IDN homographs via a crafted domain name.
CVE-2018-6105
Incorrect handling of confusable characters in Omnibox in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to perform domain spoofing via IDN homographs via a crafted domain name.
CVE-2018-6107
Incorrect handling of confusable characters in URL Formatter in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to perform domain spoofing via IDN homographs via a crafted domain name.
CVE-2018-6108
Incorrect handling of confusable characters in URL Formatter in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to perform domain spoofing via IDN homographs via a crafted HTML page.
CVE-2018-6115
Inappropriate setting of the SEE_MASK_FLAG_NO_UI flag in file downloads in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to potentially bypass OS malware checks via a crafted HTML page.
CVE-2018-6116
A nullptr dereference in WebAssembly in Google Chrome prior to 66.0.3359.117 allowed a remote attacker to potentially perform out of bounds memory access via a crafted HTML page.
CVE-2018-6152
The implementation of the Page.downloadBehavior backend unconditionally marked downloaded files as safe, regardless of file type in Google Chrome prior to 66.0.3359.117 allowed an attacker who convinced a user to install a malicious extension to potentially perform a sandbox escape via a crafted HTML page and user interaction.
CVE-2018-6981
VMware ESXi 6.7 without ESXi670-201811401-BG and VMware ESXi 6.5 without ESXi650-201811301-BG, VMware ESXi 6.0 without ESXi600-201811401-BG, VMware Workstation 15, VMware Workstation 14.1.3 or below, VMware Fusion 11, VMware Fusion 10.1.3 or below contain uninitialized stack memory usage in the vmxnet3 virtual network adapter which may allow a guest to execute code on the host.
CVE-2018-6982
VMware ESXi 6.7 without ESXi670-201811401-BG and VMware ESXi 6.5 without ESXi650-201811301-BG contain uninitialized stack memory usage in the vmxnet3 virtual network adapter which may lead to an information leak from host to guest.
CVE-2018-7956
Huawei VIP App is a mobile app for Malaysia customers that purchased P20 Series, Nova 3/3i and Mate 20. There is a vulnerability in versions before 4.0.5 that attackers can conduct bruteforce to the VIP App Web Services to get user information.
CVE-2018-7987
There is an out-of-bounds write vulnerability on Huawei P20 smartphones with versions before 8.1.0.171(C00). The software does not handle the response message properly when the user doing certain inquiry operation, an attacker could send crafted message to the device, successful exploit could cause a denial of service condition.
