CVE-2009-5030
The tcd_free_encode function in tcd.c in OpenJPEG 1.3 through 1.5 allows remote attackers to cause a denial of service (memory corruption) and possibly execute arbitrary code via crafted tile information in a Gray16 TIFF image, which causes insufficient memory to be allocated and leads to an "invalid free."
CVE-2012-0866
CREATE TRIGGER in PostgreSQL 8.3.x before 8.3.18, 8.4.x before 8.4.11, 9.0.x before 9.0.7, and 9.1.x before 9.1.3 does not properly check the execute permission for trigger functions marked SECURITY DEFINER, which allows remote authenticated users to execute otherwise restricted triggers on arbitrary data by installing the trigger on an attacker-owned table.
CVE-2012-0867
PostgreSQL 8.4.x before 8.4.11, 9.0.x before 9.0.7, and 9.1.x before 9.1.3 truncates the common name to only 32 characters when verifying SSL certificates, which allows remote attackers to spoof connections when the host name is exactly 32 characters.
CVE-2012-0868
CRLF injection vulnerability in pg_dump in PostgreSQL 8.3.x before 8.3.18, 8.4.x before 8.4.11, 9.0.x before 9.0.7, and 9.1.x before 9.1.3 allows user-assisted remote attackers to execute arbitrary SQL commands via a crafted file containing object names with newlines, which are inserted into an SQL script that is used when the database is restored.
CVE-2012-1948
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2012-1949
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox 4.x through 13.0, Thunderbird 5.0 through 13.0, and SeaMonkey before 2.11 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2012-1950
The drag-and-drop implementation in Mozilla Firefox 4.x through 13.0 and Firefox ESR 10.x before 10.0.6 allows remote attackers to spoof the address bar by canceling a page load.
CVE-2012-1951
Use-after-free vulnerability in the nsSMILTimeValueSpec::IsEventBased function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allows remote attackers to cause a denial of service (heap memory corruption) or possibly execute arbitrary code by interacting with objects used for SMIL Timing.
CVE-2012-1952
The nsTableFrame::InsertFrames function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 does not properly perform a cast of a frame variable during processing of mixed row-group and column-group frames, which might allow remote attackers to execute arbitrary code via a crafted web site.
CVE-2012-1953
The ElementAnimations::EnsureStyleRuleFor function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allows remote attackers to cause a denial of service (buffer over-read, incorrect pointer dereference, and heap-based buffer overflow) or possibly execute arbitrary code via a crafted web site.
CVE-2012-1954
Use-after-free vulnerability in the nsDocument::AdoptNode function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allows remote attackers to cause a denial of service (heap memory corruption) or possibly execute arbitrary code via vectors involving multiple adoptions and empty documents.
CVE-2012-1955
Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allow remote attackers to spoof the address bar via vectors involving history.forward and history.back calls.
CVE-2012-1957
An unspecified parser-utility class in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 does not properly handle EMBED elements within description elements in RSS feeds, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a feed.
CVE-2012-1958
Use-after-free vulnerability in the nsGlobalWindow::PageHidden function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 might allow remote attackers to execute arbitrary code via vectors related to focused content.
CVE-2012-1959
Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 do not consider the presence of same-compartment security wrappers (SCSW) during the cross-compartment wrapping of objects, which allows remote attackers to bypass intended XBL access restrictions via crafted content.
CVE-2012-1960
The qcms_transform_data_rgb_out_lut_sse2 function in the QCMS implementation in Mozilla Firefox 4.x through 13.0, Thunderbird 5.0 through 13.0, and SeaMonkey before 2.11 might allow remote attackers to obtain sensitive information from process memory via a crafted color profile that triggers an out-of-bounds read operation.
CVE-2012-1961
Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 do not properly handle duplicate values in X-Frame-Options headers, which makes it easier for remote attackers to conduct clickjacking attacks via a FRAME element referencing a web site that produces these duplicate values.
CVE-2012-1962
Use-after-free vulnerability in the JSDependentString::undepend function in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 allows remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via vectors involving strings with multiple dependencies.
CVE-2012-1963
The Content Security Policy (CSP) functionality in Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 does not properly restrict the strings placed into the blocked-uri parameter of a violation report, which allows remote web servers to capture OpenID credentials and OAuth 2.0 access tokens by triggering a violation.
CVE-2012-1964
The certificate-warning functionality in browser/components/certerror/content/aboutCertError.xhtml in Mozilla Firefox 4.x through 12.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 12.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.10 does not properly handle attempted clickjacking of the about:certerror page, which allows man-in-the-middle attackers to trick users into adding an unintended exception via an IFRAME element.
CVE-2012-1965
Mozilla Firefox 4.x through 13.0 and Firefox ESR 10.x before 10.0.6 do not properly establish the security context of a feed: URL, which allows remote attackers to bypass unspecified cross-site scripting (XSS) protection mechanisms via a feed:javascript: URL.
CVE-2012-1966
Mozilla Firefox 4.x through 13.0 and Firefox ESR 10.x before 10.0.6 do not have the same context-menu restrictions for data: URLs as for javascript: URLs, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted URL.
CVE-2012-1967
Mozilla Firefox 4.x through 13.0, Firefox ESR 10.x before 10.0.6, Thunderbird 5.0 through 13.0, Thunderbird ESR 10.x before 10.0.6, and SeaMonkey before 2.11 do not properly implement the JavaScript sandbox utility, which allows remote attackers to execute arbitrary JavaScript code with improper privileges via a javascript: URL.
CVE-2012-2139
Directory traversal vulnerability in lib/mail/network/delivery_methods/file_delivery.rb in the Mail gem before 2.4.4 for Ruby allows remote attackers to read arbitrary files via a .. (dot dot) in the to parameter.
CVE-2012-2140
The Mail gem before 2.4.3 for Ruby allows remote attackers to execute arbitrary commands via shell metacharacters in a (1) sendmail or (2) exim delivery.
CVE-2012-2303
The Spaces module 6.x-3.x before 6.x-3.4 for Drupal does not enforce permissions on non-object pages, which allows remote attackers to obtain sensitive information and possibly have other impacts via unspecified vectors to the (1) Spaces or (2) Spaces OG module.
CVE-2012-2655
PostgreSQL 8.3.x before 8.3.19, 8.4.x before 8.4.12, 9.0.x before 9.0.8, and 9.1.x before 9.1.4 allows remote authenticated users to cause a denial of service (server crash) by adding the (1) SECURITY DEFINER or (2) SET attributes to a procedural language's call handler.
CVE-2012-3358
Multiple heap-based buffer overflows in the j2k_read_sot function in j2k.c in OpenJPEG 1.5 allow remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted (1) tile number or (2) tile length in a JPEG 2000 image file.
CVE-2012-4033
Multiple unspecified vulnerabilities in the Zingiri Web Shop plugin before 2.4.0 for WordPress have unknown impact and attack vectors.
