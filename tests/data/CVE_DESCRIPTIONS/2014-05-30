CVE-2012-5560
The default configuration in mate-settings-daemon 1.5.3 allows local users to change the timezone for the system via a crafted D-Bus call.
CVE-2012-5572
CRLF injection vulnerability in the cookie method (lib/Dancer/Cookie.pm) in Dancer before 1.3114 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a cookie name, a different vulnerability than CVE-2012-5526.
CVE-2012-5876
Multiple off-by-one errors in NMMediaServerService.dll in Nero MediaHome 4.5.8.0 and earlier allow remote attackers to cause a denial of service (crash) via a long string in the (1) request line or (2) HTTP Referer header to TCP port 54444, which triggers a heap-based buffer overflow.
CVE-2012-5877
Nero MediaHome 4.5.8.0 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via an HTTP header without a name.
Per http://cwe.mitre.org/data/definitions/476.html
"CWE-476: NULL Pointer Dereference"
CVE-2013-4143
The (1) checkPasswd and (2) checkGroupXlockPasswds functions in xlockmore before 5.43 do not properly handle when a NULL value is returned upon an error by the crypt or dispcrypt function as implemented in glibc 2.17 and later, which allows attackers to bypass the screen lock via vectors related to invalid salts.
per http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"
CVE-2013-5919
Suricata before 1.4.6 allows remote attackers to cause a denial of service (crash) via a malformed SSL record.
CVE-2013-6744
The Stored Procedure infrastructure in IBM DB2 9.5, 9.7 before FP9a, 10.1 before FP3a, and 10.5 before FP3a on Windows allows remote authenticated users to gain privileges by leveraging the CONNECT privilege and the CREATE_EXTERNAL_ROUTINE authority.
CVE-2013-6788
The Bitrix e-Store module before 14.0.1 for Bitrix Site Manager uses sequential values for the BITRIX_SM_SALE_UID cookie, which makes it easier for remote attackers to guess the cookie value and bypass authentication via a brute force attack.
CVE-2014-0202
The setup script in ovirt-engine-dwh, as used in the Red Hat Enterprise Virtualization Manager data warehouse (rhevm-dwh) package before 3.3.3, stores the history database password in cleartext, which allows local users to obtain sensitive information by reading an unspecified file.
CVE-2014-0907
Multiple untrusted search path vulnerabilities in unspecified (1) setuid and (2) setgid programs in IBM DB2 9.5, 9.7 before FP9a, 9.8, 10.1 before FP3a, and 10.5 before FP3a on Linux and UNIX allow local users to gain root privileges via a Trojan horse library.
Per http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2014-0925
Open redirect vulnerability in IBM Sterling Control Center 5.4.0 before 5.4.0.1 iFix 3 and 5.4.1 before 5.4.1.0 iFix 2 allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via a crafted URL.
Per: http://cwe.mitre.org/data/definitions/601.html

CWE-601: URL Redirection to Untrusted Site
CVE-2014-2342
Triangle MicroWorks SCADA Data Gateway before 3.00.0635 allows remote attackers to cause a denial of service (excessive data processing) via a crafted DNP3 packet.
CVE-2014-2343
Triangle MicroWorks SCADA Data Gateway before 3.00.0635 allows physically proximate attackers to cause a denial of service (excessive data processing) via a crafted DNP request over a serial line.
CVE-2014-2352
Directory traversal vulnerability in Cogent DataHub before 7.3.5 allows remote attackers to read arbitrary files of unspecified types, or cause a web-server denial of service, via a crafted pathname.
CVE-2014-2353
Cross-site scripting (XSS) vulnerability in Cogent DataHub before 7.3.5 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-2354
Cogent DataHub before 7.3.5 does not use a salt during password hashing, which makes it easier for context-dependent attackers to obtain cleartext passwords via a brute-force attack.
CVE-2014-3010
Cross-site scripting (XSS) vulnerability in the Web UI in IBM WebSphere Service Registry and Repository (WSRR) 6.2, 6.3 before 6.3.0.6, 7.0 before 7.0.0.6, 7.5 before 7.5.0.5, and 8.0 before 8.0.0.3 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-3227
dpkg 1.15.9, 1.16.x before 1.16.14, and 1.17.x before 1.17.9 expect the patch program to be compliant with a need for the "C-style encoded filenames" feature, but is supported in environments with noncompliant patch programs, which triggers an interaction error that allows remote attackers to conduct directory traversal attacks and modify files outside of the intended directories via a crafted source package. NOTE: this vulnerability exists because of reliance on unrealistic constraints on the behavior of an external program.
CVE-2014-3463
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a unique security issue.  Notes: none.
CVE-2014-3780
Unspecified vulnerability in Citrix VDI-In-A-Box 5.3.x before 5.3.8 and 5.4.x before 5.4.4 allows remote attackers to bypass authentication via unspecified vectors, related to a Java servlet.
CVE-2014-3864
Directory traversal vulnerability in dpkg-source in dpkg-dev 1.3.0 allows remote attackers to modify files outside of the intended directories via a crafted source package that lacks a --- header line.
CVE-2014-3865
Multiple directory traversal vulnerabilities in dpkg-source in dpkg-dev 1.3.0 allow remote attackers to modify files outside of the intended directories via a source package with a crafted Index: pseudo-header in conjunction with (1) missing --- and +++ header lines or (2) a +++ header line with a blank pathname.
CVE-2014-3921
Cross-site scripting (XSS) vulnerability in popup.php in the Simple Popup Images plugin for WordPress allows remote attackers to inject arbitrary web script or HTML via the z parameter.
CVE-2014-3922
Cross-site scripting (XSS) vulnerability in Trend Micro InterScan Messaging Security Virtual Appliance 8.5.1.1516 allows remote authenticated users to inject arbitrary web script or HTML via the addWhiteListDomainStr parameter to addWhiteListDomain.imss.
CVE-2014-3923
Multiple cross-site scripting (XSS) vulnerabilities in the Digital Zoom Studio (DZS) Video Gallery plugin for WordPress allow remote attackers to inject arbitrary web script or HTML via the logoLink parameter to (1) preview.swf, (2) preview_skin_rouge.swf, (3) preview_allchars.swf, or (4) preview_skin_overlay.swf in deploy/.
CVE-2014-3924
Multiple cross-site scripting (XSS) vulnerabilities in Webmin before 1.690 and Usermin before 1.600 allow remote attackers to inject arbitrary web script or HTML via vectors related to popup windows.
