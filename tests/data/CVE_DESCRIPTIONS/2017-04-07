CVE-2007-6759
Dataprobe iBootBar (with 2007-09-20 and possibly later released firmware) allows remote attackers to bypass authentication, and conduct power-cycle attacks on connected devices, via a DCRABBIT cookie.
CVE-2007-6760
Dataprobe iBootBar (with 2007-09-20 and possibly later beta firmware) allows remote attackers to bypass authentication, and conduct power-cycle attacks on connected devices, via a DCCOOKIE cookie.
CVE-2016-6805
Apache Ignite before 1.9 allows man-in-the-middle attackers to read arbitrary files via XXE in modified update-notifier documents.
CVE-2016-7786
Sophos Cyberoam UTM CR25iNG 10.6.3 MR-5 allows remote authenticated users to bypass intended access restrictions via direct object reference, as demonstrated by a request for Licenseinformation.jsp. This is fixed in 10.6.5.
CVE-2016-9195
A vulnerability in RADIUS Change of Authorization (CoA) request processing in the Cisco Wireless LAN Controller (WLC) could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition by disconnecting a single connection. This vulnerability affects Cisco Wireless LAN Controller running software release 8.3.102.0. More Information: CSCvb01835. Known Fixed Releases: 8.4(1.49) 8.3(111.0) 8.3(108.0) 8.3(104.24) 8.3(102.3).
CVE-2016-9196
A vulnerability in login authentication management in Cisco Aironet 1800, 2800, and 3800 Series Access Point platforms could allow an authenticated, local attacker to gain unrestricted root access to the underlying Linux operating system. The root Linux shell is provided for advanced troubleshooting and should not be available to individual users, even those with root privileges. The attacker must have the root password to exploit this vulnerability. More Information: CSCvb13893. Known Affected Releases: 8.2(121.0) 8.3(102.0). Known Fixed Releases: 8.4(1.53) 8.4(1.52) 8.3(111.0) 8.3(104.23) 8.2(130.0) 8.2(124.1).
CVE-2016-9197
A vulnerability in the CLI command parser of the Cisco Mobility Express 2800 and 3800 Series Wireless LAN Controllers could allow an authenticated, local attacker to obtain access to the underlying operating system shell with root-level privileges. More Information: CSCvb70351. Known Affected Releases: 8.3(102.0).
CVE-2017-0454
An elevation of privilege vulnerability in the Qualcomm audio driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-33353700. References: QC-CR#1104067.
CVE-2017-0462
An elevation of privilege vulnerability in the Qualcomm Seemp driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.18. Android ID: A-33353601. References: QC-CR#1102288.
CVE-2017-0538
A remote code execution vulnerability in libavc in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33641588.
CVE-2017-0539
A remote code execution vulnerability in libhevc in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33864300.
CVE-2017-0540
A remote code execution vulnerability in libhevc in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33966031.
CVE-2017-0541
A remote code execution vulnerability in sonivox in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34031018.
CVE-2017-0542
A remote code execution vulnerability in libavc in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33934721.
CVE-2017-0543
A remote code execution vulnerability in libavc in Mediaserver could enable an attacker using a specially crafted file to cause memory corruption during media file and data processing. This issue is rated as Critical due to the possibility of remote code execution within the context of the Mediaserver process. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34097866.
CVE-2017-0544
An elevation of privilege vulnerability in CameraBase could enable a local malicious application to execute arbitrary code. This issue is rated as High because it is a local arbitrary code execution in a privileged process. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-31992879.
CVE-2017-0545
An elevation of privilege vulnerability in Audioserver could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as High because it could be used to gain local access to elevated capabilities, which are not normally accessible to a third-party application. Product: Android. Versions: 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-32591350.
CVE-2017-0546
An elevation of privilege vulnerability in SurfaceFlinger could enable a local malicious application to execute arbitrary code within the context of a privileged process. This issue is rated as High because it could be used to gain local access to elevated capabilities, which are not normally accessible to a third-party application. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-32628763.
CVE-2017-0547
An information disclosure vulnerability in libmedia in Mediaserver could enable a local malicious application to access data outside of its permission levels. This issue is rated as High because it is a general bypass for operating system protections that isolate application data from other applications. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33861560.
CVE-2017-0548
A remote denial of service vulnerability in libskia could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High severity due to the possibility of remote denial of service. Product: Android. Versions: 7.0, 7.1.1. Android ID: A-33251605.
CVE-2017-0549
A remote denial of service vulnerability in libavc in Mediaserver could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High severity due to the possibility of remote denial of service. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33818508.
CVE-2017-0550
A remote denial of service vulnerability in libavc in Mediaserver could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High severity due to the possibility of remote denial of service. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33933140.
CVE-2017-0551
A remote denial of service vulnerability in libavc in Mediaserver could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High severity due to the possibility of remote denial of service. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34097231.
CVE-2017-0552
A remote denial of service vulnerability in libavc in Mediaserver could enable an attacker to use a specially crafted file to cause a device hang or reboot. This issue is rated as High severity due to the possibility of remote denial of service. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34097915.
CVE-2017-0553
An elevation of privilege vulnerability in libnl could enable a local malicious application to execute arbitrary code within the context of the Wi-Fi service. This issue is rated as Moderate because it first requires compromising a privileged process and is mitigated by current platform configurations. Product: Android. Versions: 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-32342065. NOTE: this issue also exists in the upstream libnl before 3.3.0 library.
CVE-2017-0554
An elevation of privilege vulnerability in the Telephony component could enable a local malicious application to access capabilities outside of its permission levels. This issue is rated as Moderate because it could be used to gain access to elevated capabilities, which are not normally accessible to a third-party application. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33815946.
CVE-2017-0555
An information disclosure vulnerability in libavc in Mediaserver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access data without permission. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33551775.
CVE-2017-0556
An information disclosure vulnerability in libmpeg2 in Mediaserver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access data without permission. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34093952.
CVE-2017-0557
An information disclosure vulnerability in libmpeg2 in Mediaserver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access data without permission. Product: Android. Versions: 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34093073.
CVE-2017-0558
An information disclosure vulnerability in Mediaserver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access data without permission. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-34056274.
CVE-2017-0559
An information disclosure vulnerability in libskia could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it could be used to access data without permission. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-33897722.
CVE-2017-0560
An information disclosure vulnerability in the factory reset process could enable a local malicious attacker to access data from the previous owner. This issue is rated as Moderate due to the possibility of bypassing device protection. Product: Android. Versions: 4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1. Android ID: A-30681079.
CVE-2017-0561
A remote code execution vulnerability in the Broadcom Wi-Fi firmware could enable a remote attacker to execute arbitrary code within the context of the Wi-Fi SoC. This issue is rated as Critical due to the possibility of remote code execution in the context of the Wi-Fi SoC. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34199105. References: B-RB#110814.
CVE-2017-0562
An elevation of privilege vulnerability in the MediaTek touchscreen driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Product: Android. Versions: N/A. Android ID: A-30202425. References: M-ALPS02898189.
CVE-2017-0563
An elevation of privilege vulnerability in the HTC touchscreen driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Product: Android. Versions: Kernel-3.10. Android ID: A-32089409.
CVE-2017-0564
An elevation of privilege vulnerability in the kernel ION subsystem could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34276203.
CVE-2017-0565
An elevation of privilege vulnerability in the MediaTek thermal driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-28175904. References: M-ALPS02696516.
CVE-2017-0566
An elevation of privilege vulnerability in the MediaTek camera driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-28470975. References: M-ALPS02696367.
CVE-2017-0567
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-32125310. References: B-RB#112575.
CVE-2017-0568
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34197514. References: B-RB#112600.
CVE-2017-0569
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34198729. References: B-RB#110666.
CVE-2017-0570
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34199963. References: B-RB#110688.
CVE-2017-0571
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34203305. References: B-RB#111541.
CVE-2017-0572
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10. Android ID: A-34198931. References: B-RB#112597.
CVE-2017-0573
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34469904. References: B-RB#91539.
CVE-2017-0574
An elevation of privilege vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34624457. References: B-RB#113189.
CVE-2017-0575
An elevation of privilege vulnerability in the Qualcomm Wi-Fi driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-32658595. References: QC-CR#1103099.
CVE-2017-0576
An elevation of privilege vulnerability in the Qualcomm crypto engine driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-33544431. References: QC-CR#1103089.
CVE-2017-0577
An elevation of privilege vulnerability in the HTC touchscreen driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.18. Android ID: A-33842951.
CVE-2017-0578
An elevation of privilege vulnerability in the DTS sound driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: N/A. Android ID: A-33964406.
CVE-2017-0579
An elevation of privilege vulnerability in the Qualcomm video driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-34125463. References: QC-CR#1115406.
CVE-2017-0580
An elevation of privilege vulnerability in the Synaptics Touchscreen driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.18. Android ID: A-34325986.
CVE-2017-0581
An elevation of privilege vulnerability in the Synaptics Touchscreen driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.18. Android ID: A-34614485.
CVE-2017-0582
An elevation of privilege vulnerability in the HTC OEM fastboot command could enable a local malicious application to execute arbitrary code within the context of the sensor hub. This issue is rated as Moderate because it first requires exploitation of separate vulnerabilities. Product: Android. Versions: Kernel-3.10. Android ID: A-33178836.
CVE-2017-0583
An elevation of privilege vulnerability in the Qualcomm CP access driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Moderate because it first requires compromising a privileged process and because of vulnerability specific details which limit the impact of the issue. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-32068683. References: QC-CR#1103788.
CVE-2017-0584
An information disclosure vulnerability in the Qualcomm Wi-Fi driver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-32074353. References: QC-CR#1104731.
CVE-2017-0585
An information disclosure vulnerability in the Broadcom Wi-Fi driver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-32475556. References: B-RB#112953.
CVE-2017-0586
An information disclosure vulnerability in the Qualcomm sound driver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: Kernel-3.10, Kernel-3.18. Android ID: A-33649808. References: QC-CR#1097569.
CVE-2017-2387
The Apple Music (aka com.apple.android.music) application before 2.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2017-3817
A vulnerability in the role-based resource checking functionality of Cisco Unified Computing System (UCS) Director could allow an authenticated, remote attacker to view unauthorized information for any virtual machine in a UCS domain. More Information: CSCvc32434. Known Affected Releases: 5.5(0.1) 6.0(0.0).
CVE-2017-3848
A vulnerability in the HTTP web-based management interface of Cisco Prime Infrastructure could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface of the affected system. More Information: CSCuw63001 CSCuw63003. Known Affected Releases: 2.2(2). Known Fixed Releases: 3.1(0.0).
CVE-2017-3884
A vulnerability in the web interface of Cisco Prime Infrastructure and Cisco Evolved Programmable Network (EPN) Manager could allow an authenticated, remote attacker to access sensitive data. The attacker does not need administrator credentials and could use this information to conduct additional reconnaissance attacks. More Information: CSCvc60031 (Fixed) CSCvc60041 (Fixed) CSCvc60095 (Open) CSCvc60102 (Open). Known Affected Releases: 2.2 2.2(3) 3.0 3.1(0.0) 3.1(0.128) 3.1(4.0) 3.1(5.0) 3.2(0.0) 2.0(4.0.45D).
CVE-2017-3885
A vulnerability in the detection engine reassembly of Secure Sockets Layer (SSL) packets for Cisco Firepower System Software could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition because the Snort process consumes a high level of CPU resources. Affected Products: This vulnerability affects Cisco Firepower System Software running software releases 6.0.0, 6.1.0, 6.2.0, or 6.2.1 when the device is configured with an SSL policy that has at least one rule specifying traffic decryption. More Information: CSCvc58563. Known Affected Releases: 6.0.0 6.1.0 6.2.0 6.2.1.
CVE-2017-3886
A vulnerability in the Cisco Unified Communications Manager web interface could allow an authenticated, remote attacker to impact the confidentiality of the system by executing arbitrary SQL queries, aka SQL Injection. The attacker must be authenticated as an administrative user to execute SQL database queries. More Information: CSCvc74291. Known Affected Releases: 1.0(1.10000.10) 11.5(1.10000.6). Known Fixed Releases: 12.0(0.98000.619) 12.0(0.98000.485) 12.0(0.98000.212) 11.5(1.13035.1) 11.0(1.23900.5) 11.0(1.23900.2) 11.0(1.23067.1) 10.5(2.15900.2).
CVE-2017-3887
A vulnerability in the detection engine that handles Secure Sockets Layer (SSL) packets for Cisco Firepower System Software could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition because the Snort process unexpectedly restarts. This vulnerability affects Cisco Firepower System Software prior to the first fixed release when it is configured with an SSL Decrypt-Resign policy. More Information: CSCvb62292. Known Affected Releases: 6.0.1 6.1.0 6.2.0. Known Fixed Releases: 6.2.0 6.1.0.2.
CVE-2017-3888
A vulnerability in the web-based management interface of Cisco Unified Communications Manager could allow an authenticated, remote attacker to conduct a reflected cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. This vulnerability affects Cisco Unified Communications Manager with a default configuration running an affected software release with the attacker authenticated as the administrative user. More Information: CSCvc83712. Known Affected Releases: 12.0(0.98000.452). Known Fixed Releases: 12.0(0.98000.750) 12.0(0.98000.708) 12.0(0.98000.707) 12.0(0.98000.704) 12.0(0.98000.554) 12.0(0.98000.546) 12.0(0.98000.543) 12.0(0.98000.248) 12.0(0.98000.244) 12.0(0.98000.242).
CVE-2017-3889
A vulnerability in the web interface of the Cisco Registered Envelope Service could allow an unauthenticated, remote attacker to redirect a user to a undesired web page, aka an Open Redirect. This vulnerability affects the Cisco Registered Envelope cloud-based service. More Information: CSCvc60123. Known Affected Releases: 5.1.0-015.
CVE-2017-6019
An issue was discovered in Schneider Electric Conext ComBox, model 865-1058, all firmware versions prior to V3.03 BN 830. A series of rapid requests to the device may cause it to reboot.
CVE-2017-6033
A DLL Hijacking issue was discovered in Schneider Electric Interactive Graphical SCADA System (IGSS) Software, Version 12 and previous versions. The software will execute a malicious file if it is named the same as a legitimate file and placed in a location that is earlier in the search path.
CVE-2017-6597
A vulnerability in the local-mgmt CLI command of the Cisco Unified Computing System (UCS) Manager, Cisco Firepower 4100 Series Next-Generation Firewall (NGFW), and Cisco Firepower 9300 Security Appliance could allow an authenticated, local attacker to perform a command injection attack. More Information: CSCvb61394 CSCvb86816. Known Affected Releases: 2.0(1.68) 3.1(1k)A. Known Fixed Releases: 92.2(1.101) 92.1(1.1658) 2.0(1.115).
CVE-2017-6598
A vulnerability in the debug plug-in functionality of the Cisco Unified Computing System (UCS) Manager, Cisco Firepower 4100 Series Next-Generation Firewall (NGFW), and Cisco Firepower 9300 Security Appliance could allow an authenticated, local attacker to execute arbitrary commands, aka Privilege Escalation. More Information: CSCvb86725 CSCvb86797. Known Affected Releases: 2.0(1.68) 3.1(1k)A. Known Fixed Releases: 92.2(1.105) 92.1(1.1733) 2.1(1.69).
CVE-2017-6599
A vulnerability in Google-defined remote procedure call (gRPC) handling in Cisco IOS XR Software could allow an unauthenticated, remote attacker to cause the Event Management Service daemon (emsd) to crash due to a system memory leak, resulting in a denial of service (DoS) condition. This vulnerability affects Cisco IOS XR Software with gRPC enabled. More Information: CSCvb14433. Known Affected Releases: 6.1.1.BASE 6.2.1.BASE. Known Fixed Releases: 6.2.1.22i.MGBL 6.1.22.9i.MGBL 6.1.21.12i.MGBL 6.1.2.13i.MGBL.
CVE-2017-6600
A vulnerability in the CLI of the Cisco Unified Computing System (UCS) Manager, Cisco Firepower 4100 Series Next-Generation Firewall (NGFW), and Cisco Firepower 9300 Security Appliance could allow an authenticated, local attacker to perform a command injection attack. More Information: CSCvb61351 CSCvb61637. Known Affected Releases: 2.0(1.68) 3.1(1k)A. Known Fixed Releases: 92.2(1.101) 92.1(1.1645) 2.0(1.82) 1.1(4.136.
CVE-2017-6601
A vulnerability in the CLI of the Cisco Unified Computing System (UCS) Manager, Cisco Firepower 4100 Series Next-Generation Firewall (NGFW), and Cisco Firepower 9300 Security Appliance could allow an authenticated, local attacker to perform a command injection attack. More Information: CSCvb61384 CSCvb86764. Known Affected Releases: 2.0(1.68) 3.1(1k)A. Known Fixed Releases: 92.2(1.101) 92.1(1.1647).
CVE-2017-6602
A vulnerability in the CLI of Cisco Unified Computing System (UCS) Manager, Cisco Firepower 4100 Series Next-Generation Firewall (NGFW), and Cisco Firepower 9300 Security Appliance could allow an authenticated, local attacker to perform a command injection attack. More Information: CSCvb66189 CSCvb86775. Known Affected Releases: 2.0(1.68) 3.1(1k)A. Known Fixed Releases: 92.2(1.101) 92.1(1.1742) 92.1(1.1658) 2.1(1.38) 2.0(1.107) 2.0(1.87) 1.1(4.148) 1.1(4.138).
CVE-2017-6603
A vulnerability in Cisco ASR 903 or ASR 920 Series Devices running with an RSP2 card could allow an unauthenticated, adjacent attacker to cause a denial of service (DoS) condition on a targeted system because of incorrect IPv6 Packet Processing. More Information: CSCuy94366. Known Affected Releases: 15.4(3)S3.15. Known Fixed Releases: 15.6(2)SP 15.6(1.31)SP.
CVE-2017-6604
A vulnerability in the web interface of Cisco Integrated Management Controller (IMC) Software could allow an unauthenticated, remote attacker to redirect a user to a malicious web page. This vulnerability affects the following Cisco products running Cisco IMC Software: Unified Computing System (UCS) B-Series M3 and M4 Blade Servers, Unified Computing System (UCS) C-Series M3 and M4 Rack Servers. More Information: CSCvc37931. Known Affected Releases: 3.1(2c)B.
CVE-2017-6606
A vulnerability in a startup script of Cisco IOS XE Software could allow an unauthenticated attacker with physical access to the targeted system to execute arbitrary commands on the underlying operating system with the privileges of the root user. More Information: CSCuz06639 CSCuz42122. Known Affected Releases: 15.6(1.1)S 16.1.2 16.2.0 15.2(1)E. Known Fixed Releases: Denali-16.1.3 16.2(1.8) 16.1(2.61) 15.6(2)SP 15.6(2)S1 15.6(1)S2 15.5(3)S3a 15.5(3)S3 15.5(2)S4 15.5(1)S4 15.4(3)S6a 15.4(3)S6 15.3(3)S8a 15.3(3)S8 15.2(5)E 15.2(4)E3 15.2(3)E5 15.0(2)SQD3 15.0(1.9.2)SQD3 3.9(0)E.
CVE-2017-7570
PivotX 2.3.11 allows remote authenticated Advanced users to execute arbitrary PHP code by performing an upload with a safe file extension (such as .jpg) and then invoking the duplicate function to change to the .php extension.
CVE-2017-7577
XiongMai uc-httpd has directory traversal allowing the reading of arbitrary files via a "GET ../" HTTP request.
CVE-2017-7578
Multiple heap-based buffer overflows in parser.c in libming 0.4.7 allow remote attackers to cause a denial of service (listswf application crash) or possibly have unspecified other impact via a crafted SWF file. NOTE: this issue exists because of an incomplete fix for CVE-2016-9831.
CVE-2017-7579
inc/PMF/Faq.php in phpMyFAQ before 2.9.7 has XSS in the question field.
CVE-2017-7581
SQL injection vulnerability in NewsController.php in the News module 5.3.2 and earlier for TYPO3 allows unauthenticated users to execute arbitrary SQL commands via vectors involving overwriteDemand for order and OrderByAllowed.
CVE-2017-7583
ILIAS before 5.2.3 has XSS via SVG documents.
CVE-2017-7584
Memory Corruption Vulnerability in Foxit PDF Toolkit before 2.1 allows an attacker to cause Denial of Service & Remote Code Execution when a victim opens a specially crafted PDF file.
CVE-2017-7585
In libsndfile before 1.0.28, an error in the "flac_buffer_copy()" function (flac.c) can be exploited to cause a stack-based buffer overflow via a specially crafted FLAC file.
CVE-2017-7586
In libsndfile before 1.0.28, an error in the "header_read()" function (common.c) when handling ID3 tags can be exploited to cause a stack-based buffer overflow via a specially crafted FLAC file.
