CVE-2009-5018
Stack-based buffer overflow in gif2png.c in gif2png 2.5.3 and earlier might allow context-dependent attackers to execute arbitrary code via a long command-line argument, as demonstrated by a CGI program that launches gif2png.
CVE-2010-0115
SQL injection vulnerability in login.php in the GUI management console in Symantec Web Gateway 4.5 before 4.5.0.376 allows remote attackers to execute arbitrary SQL commands via the USERNAME parameter.
CVE-2010-3086
include/asm-x86/futex.h in the Linux kernel before 2.6.25 does not properly implement exception fixup, which allows local users to cause a denial of service (panic) via an invalid application that triggers a page fault.
CVE-2010-3833
MySQL 5.0 before 5.0.92, 5.1 before 5.1.51, and 5.5 before 5.5.6 does not properly propagate type errors, which allows remote attackers to cause a denial of service (server crash) via crafted arguments to extreme-value functions such as (1) LEAST and (2) GREATEST, related to KILL_BAD_DATA and a "CREATE TABLE ... SELECT."
CVE-2010-3834
Unspecified vulnerability in MySQL 5.0 before 5.0.92, 5.1 before 5.1.51, and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (server crash) via vectors related to "materializing a derived table that required a temporary table for grouping" and "user variable assignments."
CVE-2010-3835
MySQL 5.1 before 5.1.51 and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (mysqld server crash) by performing a user-variable assignment in a logical expression that is calculated and stored in a temporary table for GROUP BY, then causing the expression value to be used after the table is created, which causes the expression to be re-evaluated instead of accessing its value from the table.
CVE-2010-3836
MySQL 5.0 before 5.0.92, 5.1 before 5.1.51, and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (assertion failure and server crash) via vectors related to view preparation, pre-evaluation of LIKE predicates, and IN Optimizers.
CVE-2010-3837
MySQL 5.0 before 5.0.92, 5.1 before 5.1.51, and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (server crash) via a prepared statement that uses GROUP_CONCAT with the WITH ROLLUP modifier, probably triggering a use-after-free error when a copied object is modified in a way that also affects the original object.
CVE-2010-3838
MySQL 5.0 before 5.0.92, 5.1 before 5.1.51, and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (server crash) via a query that uses the (1) GREATEST or (2) LEAST function with a mixed list of numeric and LONGBLOB arguments, which is not properly handled when the function's result is "processed using an intermediate temporary table."
CVE-2010-3839
MySQL 5.1 before 5.1.51 and 5.5 before 5.5.6 allows remote authenticated users to cause a denial of service (infinite loop) via multiple invocations of a (1) prepared statement or (2) stored procedure that creates a query with nested JOIN statements.
CVE-2010-3840
The Gis_line_string::init_from_wkb function in sql/spatial.cc in MySQL 5.1 before 5.1.51 allows remote authenticated users to cause a denial of service (server crash) by calling the PolyFromWKB function with Well-Known Binary (WKB) data containing a crafted number of (1) line strings or (2) line points.
CVE-2010-4334
The IO::Socket::SSL module 1.35 for Perl, when verify_mode is not VERIFY_NONE, fails open to VERIFY_NONE instead of throwing an error when a ca_file/ca_path cannot be verified, which allows remote attackers to bypass intended certificate restrictions.
CVE-2010-4335
The _validatePost function in libs/controller/components/security.php in CakePHP 1.3.x through 1.3.5 and 1.2.8 allows remote attackers to modify the internal Cake cache and execute arbitrary code via a crafted data[_Token][fields] value that is processed by the unserialize function, as demonstrated by modifying the file_map cache to execute arbitrary local files.
CVE-2010-4337
The configure script in gnash 0.8.8 allows local users to overwrite arbitrary files via a symlink attack on the (1) /tmp/gnash-configure-errors.$$, (2) /tmp/gnash-configure-warnings.$$, or (3) /tmp/gnash-configure-recommended.$$ files.
CVE-2010-4339
Cross-site scripting (XSS) vulnerability in Hypermail 2.2.0 allows remote attackers to inject arbitrary web script or HTML via a crafted From address, which is not properly handled when indexing messages.
CVE-2010-4566
The web authentication form in the NT4 authentication component in Citrix Access Gateway Enterprise Edition 9.2-49.8 and earlier, and the NTLM authentication component in Access Gateway Standard and Advanced Editions before Access Gateway 5.0, allows attackers to execute arbitrary commands via shell metacharacters in the password field.
CVE-2010-4694
Buffer overflow in gif2png.c in gif2png 2.5.3 and earlier might allow context-dependent attackers to cause a denial of service (application crash) or have unspecified other impact via a GIF file that contains many images, leading to long extensions such as .p100 for PNG output files, as demonstrated by a CGI program that launches gif2png, a different vulnerability than CVE-2009-5018.
CVE-2010-4695
A certain Fedora patch for gif2png.c in gif2png 2.5.1 and 2.5.2, as distributed in gif2png-2.5.1-1200.fc12 on Fedora 12 and gif2png_2.5.2-1 on Debian GNU/Linux, truncates a GIF pathname specified on the command line, which might allow remote attackers to create PNG files in unintended directories via a crafted command-line argument, as demonstrated by a CGI program that launches gif2png, a different vulnerability than CVE-2009-5018.
CVE-2011-0470
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle extensions notification, which allows remote attackers to cause a denial of service (application crash) via unspecified vectors.
CVE-2011-0471
The node-iteration implementation in Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 does not properly handle pointers, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-0472
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle the printing of PDF documents, which allows user-assisted remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a multi-page document.
CVE-2011-0473
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle Cascading Style Sheets (CSS) token sequences in conjunction with CANVAS elements, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale pointer."
CVE-2011-0474
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle Cascading Style Sheets (CSS) token sequences in conjunction with cursors, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale pointer."
CVE-2011-0475
Use-after-free vulnerability in Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 allows remote attackers to cause a denial of service or possibly have unspecified other impact via a PDF document.
CVE-2011-0476
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 allow remote attackers to cause a denial of service (stack memory corruption) or possibly have unspecified other impact via a PDF document that triggers an out-of-memory error.
CVE-2011-0477
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle a mismatch in video frame sizes, which allows remote attackers to cause a denial of service (incorrect memory access) or possibly have unspecified other impact via unknown vectors.
CVE-2011-0478
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle SVG use elements, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale pointer."
CVE-2011-0479
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly interact with extensions, which allows remote attackers to cause a denial of service via a crafted extension that triggers an uninitialized pointer.
CVE-2011-0480
Multiple buffer overflows in vorbis_dec.c in the Vorbis decoder in FFmpeg, as used in Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344, allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via a crafted WebM file, related to buffers for (1) the channel floor and (2) the channel residue.
CVE-2011-0481
Buffer overflow in Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to PDF shading.
CVE-2011-0482
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly perform a cast of an unspecified variable during handling of anchors, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted HTML document.
CVE-2011-0483
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly perform a cast of an unspecified variable during handling of video, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-0484
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly perform DOM node removal, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale rendering node."
CVE-2011-0485
Google Chrome before 8.0.552.237 and Chrome OS before 8.0.552.344 do not properly handle speech data, which allows remote attackers to execute arbitrary code via unspecified vectors that lead to a "stale pointer."
