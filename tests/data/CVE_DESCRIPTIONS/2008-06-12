CVE-2008-0011
Microsoft DirectX 8.1 through 9.0c, and DirectX on Microsoft XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008, does not properly perform MJPEG error checking, which allows remote attackers to execute arbitrary code via a crafted MJPEG stream in a (1) AVI or (2) ASF file, aka the "MJPEG Decoder Vulnerability."
CVE-2008-0956
Multiple stack-based buffer overflows in the BackWeb Lite Install Runner ActiveX control in the BackWeb Web Package ActiveX object in LiteInstActivator.dll in BackWeb before 8.1.1.87, as used in Logitech Desktop Manager (LDM) before 2.56, allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2008-1440
Microsoft Windows XP SP2 and SP3, and Server 2003 SP1 and SP2, does not properly validate the option length field in Pragmatic General Multicast (PGM) packets, which allows remote attackers to cause a denial of service (infinite loop and system hang) via a crafted PGM packet, aka the "PGM Invalid Length Vulnerability."
CVE-2008-1441
Microsoft Windows XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows remote attackers to cause a denial of service (system hang) via a series of Pragmatic General Multicast (PGM) packets with invalid fragment options, aka the "PGM Malformed Fragment Vulnerability."
CVE-2008-1442
Heap-based buffer overflow in the substringData method in Microsoft Internet Explorer 6 and 7 allows remote attackers to execute arbitrary code, related to an unspecified manipulation of a DOM object before a call to this method, aka the "HTML Objects Memory Corruption Vulnerability."
CVE-2008-1444
Stack-based buffer overflow in Microsoft DirectX 7.0 and 8.1 on Windows 2000 SP4 allows remote attackers to execute arbitrary code via a Synchronized Accessible Media Interchange (SAMI) file with crafted parameters for a Class Name variable, aka the "SAMI Format Parsing Vulnerability."
CVE-2008-1445
Active Directory on Microsoft Windows 2000 Server SP4, XP Professional SP2 and SP3, Server 2003 SP1 and SP2, and Server 2008 allows remote authenticated users to cause a denial of service (system hang or reboot) via a crafted LDAP request.
CVE-2008-1451
The WINS service on Microsoft Windows 2000 SP4, and Server 2003 SP1 and SP2, does not properly validate data structures in WINS network packets, which allows local users to gain privileges via a crafted packet, aka "Memory Overwrite Vulnerability."
CVE-2008-1453
The Bluetooth stack in Microsoft Windows XP SP2 and SP3, and Vista Gold and SP1, allows physically proximate attackers to execute arbitrary code via a large series of Service Discovery Protocol (SDP) packets.
CVE-2008-2668
Multiple cross-site scripting (XSS) vulnerabilities in yBlog 0.2.2.2 allow remote attackers to inject arbitrary web script or HTML via (1) the q parameter to search.php, or the n parameter to (2) user.php or (3) uss.php.
CVE-2008-2669
Multiple SQL injection vulnerabilities in yBlog 0.2.2.2 allow remote attackers to execute arbitrary SQL commands via (1) the q parameter to search.php, or the n parameter to (2) user.php or (3) uss.php.
CVE-2008-2670
Multiple SQL injection vulnerabilities in index.php in Insanely Simple Blog 0.5 allow remote attackers to execute arbitrary SQL commands via (1) the id parameter, or (2) the term parameter in a search action. NOTE: the current_subsection parameter is already covered by CVE-2007-3889.
CVE-2008-2671
SQL injection vulnerability in comments.php in DCFM Blog 0.9.4 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-2672
Multiple directory traversal vulnerabilities in ErfurtWiki R1.02b and earlier, when register_globals is enabled, allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the (1) ewiki_id and (2) ewiki_action parameters to fragments/css.php, and possibly the (3) id parameter to the default URI.  NOTE: the default URI is site-specific but often performs an include_once of ewiki.php.
CVE-2008-2673
SQL injection vulnerability in index.php in Powie pNews 2.08 and 2.10, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the shownews parameter.
CVE-2008-2674
Unspecified vulnerability in the Interstage Management Console, as used in Fujitsu Interstage Application Server 6.0 through 9.0.0A, Apworks Modelers-J 6.0 through 7.0, and Studio 8.0.1 and 9.0.0, allows remote attackers to read or delete arbitrary files via unspecified vectors.
CVE-2008-2675
Cross-site scripting (XSS) vulnerability in index.php in PHP Image Gallery allows remote attackers to inject arbitrary web script or HTML via the action parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-2676
SQL injection vulnerability in the iJoomla News Portal (com_news_portal) component 1.0 and earlier for Joomla! allows remote attackers to execute arbitrary SQL commands via the Itemid parameter to index.php.
CVE-2008-2677
Cross-site scripting (XSS) vulnerability in edit1.php in Telephone Directory 2008 allows remote attackers to inject arbitrary web script or HTML via the action parameter.
CVE-2008-2678
Multiple SQL injection vulnerabilities in Telephone Directory 2008, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) code parameter in a confirm_data action to edit1.php and the (2) id parameter to view_more.php.
CVE-2008-2679
SQL injection vulnerability in the KeyWordsList function in _includes/inc_routines.asp in Realm CMS 2.3 and earlier allows remote attackers to execute arbitrary SQL commands via the kwrd parameter in a kwl action to the default URI.
CVE-2008-2680
Multiple cross-site scripting (XSS) vulnerabilities in _db/compact.asp in Realm CMS 2.3 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) CmpctedDB and (2) Boyut parameters.
CVE-2008-2681
Realm CMS 2.3 and earlier allows remote attackers to obtain sensitive information via a direct request to _db/compact.asp, which reveals the database path in an error message.
CVE-2008-2682
_RealmAdmin/login.asp in Realm CMS 2.3 and earlier allows remote attackers to bypass authentication and access admin pages via certain modified cookies, probably including (1) cUserRole, (2) cUserName, and (3) cUserID.
CVE-2008-2683
The BIDIB.BIDIBCtrl.1 ActiveX control in BIDIB.ocx 10.9.3.0 in Black Ice Barcode SDK 5.01 allows remote attackers to force the download and storage of arbitrary files by specifying the origin URL in the first argument to the DownloadImageFileURL method, and the local filename in the second argument.  NOTE: some of these details are obtained from third party information.
CVE-2008-2684
The BIDIB.BIDIBCtrl.1 ActiveX control in BIDIB.ocx 10.9.3.0 in Black Ice Barcode SDK 5.01 allows remote attackers to execute arbitrary code via long strings in the two arguments to the DownloadImageFileURL method, which trigger memory corruption.  NOTE: some of these details are obtained from third party information.
CVE-2008-2685
SQL injection vulnerability in article.asp in Battle Blog 1.25 Build 4 and earlier allows remote attackers to execute arbitrary SQL commands via the entry parameter, a different vector than CVE-2008-2626.
