CVE-2014-0158
Heap-based buffer overflow in the JPEG2000 image tile decoder in OpenJPEG before 1.5.2 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted file because of incorrect j2k_decode, j2k_read_eoc, and tcd_decode_tile interaction, a related issue to CVE-2013-6045. NOTE: this is not a duplicate of CVE-2013-1447, because the scope of CVE-2013-1447 was specifically defined in http://openwall.com/lists/oss-security/2013/12/04/6 as only "null pointer dereferences, division by zero, and anything that would just fit as DoS."
CVE-2014-1398
The entity wrapper access API in the Entity API module 7.x-1.x before 7.x-1.3 for Drupal might allow remote authenticated users to bypass intended access restrictions on comment, user and node statistics properties via unspecified vectors.
CVE-2014-1399
The entity wrapper access API in the Entity API module 7.x-1.x before 7.x-1.3 for Drupal might allow remote authenticated users to bypass intended access restrictions on referenced entities via unspecified vectors.
CVE-2014-1400
The entity_access API in the Entity API module 7.x-1.x before 7.x-1.3 for Drupal might allow remote authenticated users to bypass intended access restrictions and read unpublished comments via unspecified vectors.
CVE-2014-1889
The Group creation process in the Buddypress plugin before 1.9.2 for WordPress allows remote authenticated users to gain control of arbitrary groups by leveraging a missing permissions check.
CVE-2014-1946
OpenDocMan 1.2.7 and earlier does not properly validate allowed actions, which allows remote authenticated users to bypass an intended access restrictions and assign administrative privileges to themselves via a crafted request to signup.php.
CVE-2014-2073
Stack-based buffer overflow in Dassault Systemes CATIA V5-6R2013 allows remote attackers to execute arbitrary code via a crafted packet, related to "CATV5_Backbone_Bus."
CVE-2014-2078
The backend in Open-Xchange (OX) AppSuite 7.4.2 before 7.4.2-rev9 allows remote attackers to obtain sensitive information about user email addresses in opportunistic circumstances by leveraging a failure in e-mail auto configuration for external accounts.
CVE-2014-3114
The EZPZ One Click Backup (ezpz-one-click-backup) plugin 12.03.10 and earlier for WordPress allows remote attackers to execute arbitrary commands via the cmd parameter to functions/ezpz-archive-cmd.php.
CVE-2014-3999
The Horde_Ldap library before 2.0.6 for Horde allows remote attackers to bypass authentication by leveraging knowledge of the LDAP bind user DN.
CVE-2015-0172
IBM Security SiteProtector System 3.0, 3.1.0 and 3.1.1 allows remote attackers to bypass intended security restrictions and consequently execute unspecified commands and obtain sensitive information via unknown vectors. IBM X-Force ID: 100927.
CVE-2015-1957
IBM WebSphere MQ 7.5.x before 7.5.0.6 and 8.0.x before 8.0.0.3 allows remote authenticated users to obtain sensitive information via a man-in-the-middle attack, related to duplication of message data in cleartext outside the protected payload. IBM X-Force ID: 103482.
CVE-2016-9645
The fix for ikiwiki for CVE-2016-10026 was incomplete resulting in editing restriction bypass for git revert when using git versions older than 2.8.0. This has been fixed in 3.20161229.
CVE-2017-1081
In FreeBSD before 11.0-STABLE, 11.0-RELEASE-p10, 10.3-STABLE, and 10.3-RELEASE-p19, ipfilter using "keep state" or "keep frags" options can cause a kernel panic when fed specially crafted packet fragments due to incorrect memory handling.
CVE-2017-14323
SSRF (Server Side Request Forgery) in getRemoteImage.php in Ueditor in Onethink V1.0 and V1.1 allows remote attackers to obtain sensitive information, attack intranet hosts, or possibly trigger remote command execution via the upfile parameter.
CVE-2017-14611
SSRF (Server Side Request Forgery) in Cockpit 0.13.0 allows remote attackers to read arbitrary files or send TCP traffic to intranet hosts via the url parameter, related to use of the discontinued aheinze/fetch_url_contents component.
CVE-2017-18100
The agile wallboard gadget in Atlassian Jira before version 7.8.1 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the name of quick filters.
CVE-2017-18101
Various administrative external system import resources in Atlassian JIRA Server (including JIRA Core) before version 7.6.5, from version 7.7.0 before version 7.7.3, from version 7.8.0 before version 7.8.3 and before version 7.9.0 allow remote attackers to run import operations and to determine if an internal service exists through missing permission checks.
CVE-2018-2403
Under certain conditions, SAP Disclosure Management 10.1 allows an attacker to access information which would otherwise be restricted. It is possible for an authorized user to get SAP Disclosure Management to point a specific chapter type to a chapter the user has not been given access to.
CVE-2018-2404
SAP Disclosure Management 10.1 allows an attacker to upload any file without proper file format validation.
CVE-2018-2405
SAP Solution Manager, 7.10, 7.20, Incident Management Work Center allows an attacker to upload a malicious script as an attachment and this could lead to possible Cross-Site Scripting.
CVE-2018-2406
Unquoted windows search path (directory/path traversal) vulnerability in Crystal Reports Server, OEM Edition (CRSE), 4.0, 4.10, 4.20, 4.30, startup path.
CVE-2018-2408
Improper Session Management in SAP Business Objects, 4.0, from 4.10, from 4.20, 4.30, CMC/BI Launchpad/Fiorified BI Launchpad. In case of password change for a user, all other active sessions created using older password continues to be active.
CVE-2018-2409
Improper session management when using SAP Cloud Platform 2.0 (Connectivity Service and Cloud Connector). Under certain conditions, data of some other user may be shown or modified when using an application built on top of SAP Cloud Platform.
CVE-2018-2410
SAP Business One, 9.2, 9.3, browser access does not sufficiently encode user controlled inputs, which results in a Cross-Site Scripting (XSS) vulnerability.
CVE-2018-2412
SAP Disclosure Management 10.1 does not perform necessary authorization checks for an authenticated user, resulting in escalation of privileges.
CVE-2018-2413
SAP Disclosure Management 10.1 does not perform necessary authorization checks for an authenticated user, resulting in escalation of privileges.
CVE-2018-3837
An exploitable information disclosure vulnerability exists in the PCX image rendering functionality of Simple DirectMedia Layer SDL2_image-2.0.2. A specially crafted PCX image can cause an out-of-bounds read on the heap, resulting in information disclosure . An attacker can display a specially crafted image to trigger this vulnerability.
CVE-2018-3838
An exploitable information vulnerability exists in the XCF image rendering functionality of Simple DirectMedia Layer SDL2_image-2.0.2. A specially crafted XCF image can cause an out-of-bounds read on the heap, resulting in information disclosure. An attacker can display a specially crafted image to trigger this vulnerability.
CVE-2018-3839
An exploitable code execution vulnerability exists in the XCF image rendering functionality of Simple DirectMedia Layer SDL2_image-2.0.2. A specially crafted XCF image can cause an out-of-bounds write on the heap, resulting in code execution. An attacker can display a specially crafted image to trigger this vulnerability.
CVE-2018-5227
Various administrative application link resources in Atlassian Application Links before version 5.4.4 allow remote attackers with administration rights to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the display url of a configured application link.
CVE-2018-8772
Coship RT3052 4.0.0.48 devices allow XSS via a crafted SSID field on the "Wireless Setting - Basic" screen.
CVE-2018-9037
Monstra CMS 3.0.4 allows remote code execution via an upload_file request for a .zip file, which is automatically extracted and may contain .php files.
CVE-2018-9038
Monstra CMS 3.0.4 allows remote attackers to delete files via an admin/index.php?id=filesmanager&delete_dir=./&path=uploads/ request.
CVE-2018-9840
The Open Whisper Signal app before 2.23.2 for iOS allows physically proximate attackers to bypass the screen locker feature via certain rapid sequences of actions that include app opening, clicking on cancel, and using the home button.
CVE-2018-9918
libqpdf.a in QPDF through 8.0.2 mishandles certain "expected dictionary key but found non-name object" cases, allowing remote attackers to cause a denial of service (stack exhaustion), related to the QPDFObjectHandle and QPDF_Dictionary classes, because nesting in direct objects is not restricted.
CVE-2018-9922
An issue was discovered in idreamsoft iCMS through 7.0.7. Physical path leakage exists via an invalid nickname field that reveals a core/library/weixin.class.php pathname.
CVE-2018-9923
An issue was discovered in idreamsoft iCMS through 7.0.7. CSRF exists in admincp.php, as demonstrated by adding an article via an app=article&do=save&frame=iPHP request.
CVE-2018-9924
An issue was discovered in idreamsoft iCMS through 7.0.7. SQL injection exists via the pid array parameter in an admincp.php?app=tag&do=save&frame=iPHP request.
CVE-2018-9925
An issue was discovered in idreamsoft iCMS through 7.0.7. XSS exists via the nickname field in an admincp.php?app=user&do=save&frame=iPHP request.
CVE-2018-9926
An issue was discovered in WUZHI CMS 4.1.0. There is a CSRF vulnerability that can add an admin account via index.php?m=core&f=power&v=add.
CVE-2018-9927
An issue was discovered in WUZHI CMS 4.1.0. There is a CSRF vulnerability that can add a user account via index.php?m=member&f=index&v=add.
CVE-2018-9928
Cross-site scripting (XSS) vulnerability in save.php in MetInfo 6.0 allows remote attackers to inject arbitrary web script or HTML via the webname or weburl parameter.
CVE-2018-9934
The reset-password feature in MetInfo 6.0 allows remote attackers to change arbitrary passwords via vectors involving a Host HTTP header that is modified to specify a web server under the attacker's control.
CVE-2018-9985
The front page of MetInfo 6.0 allows XSS by sending a feedback message to an administrator.
CVE-2018-9988
ARM mbed TLS before 2.1.11, before 2.7.2, and before 2.8.0 has a buffer over-read in ssl_parse_server_key_exchange() that could cause a crash on invalid input.
CVE-2018-9989
ARM mbed TLS before 2.1.11, before 2.7.2, and before 2.8.0 has a buffer over-read in ssl_parse_server_psk_hint() that could cause a crash on invalid input.
CVE-2018-9993
YUNUCMS 1.0.7 has XSS via the content title on an admin/content/addcontent/cid/## page (aka a news center page).
CVE-2018-9995
TBK DVR4104 and DVR4216 devices, as well as Novo, CeNova, QSee, Pulnix, XVR 5 in 1, Securus, Night OWL, DVR Login, HVR Login, and MDVR Login, which run re-branded versions of the original TBK DVR4104 and DVR4216 series, allow remote attackers to bypass authentication via a "Cookie: uid=admin" header, as demonstrated by a device.rsp?opt=user&cmd=list request that provides credentials within JSON data in a response.
CVE-2018-9996
An issue was discovered in cplus-dem.c in GNU libiberty, as distributed in GNU Binutils 2.30. Stack Exhaustion occurs in the C++ demangling functions provided by libiberty, and there are recursive stack frames: demangle_template_value_parm, demangle_integral_value, and demangle_expression.
