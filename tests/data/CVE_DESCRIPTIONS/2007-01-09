CVE-2007-0024
Integer overflow in the Vector Markup Language (VML) implementation (vgx.dll) in Microsoft Internet Explorer 5.01, 6, and 7 on Windows 2000 SP4, XP SP2, Server 2003, and Server 2003 SP1 allows remote attackers to execute arbitrary code via a crafted web page that contains unspecified integer properties that cause insufficient memory allocation and trigger a buffer overflow, aka the "VML Buffer Overrun Vulnerability."
CVE-2007-0027
Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2, 2004 for Mac, and v.X for Mac allows remote attackers to execute arbitrary code via malformed IMDATA records that trigger memory corruption.
CVE-2007-0028
Microsoft Excel 2000, 2002, 2003, Viewer 2003, Office 2004 for Mac, and Office v.X for Mac does not properly handle certain opcodes, which allows user-assisted remote attackers to execute arbitrary code via a crafted XLS file, which results in an "Improper Memory Access Vulnerability."  NOTE: an early disclosure of this issue used CVE-2006-3432, but only CVE-2007-0028 should be used.
CVE-2007-0029
Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2, 2004 for Mac, and v.X for Mac allows user-assisted remote attackers to execute arbitrary code via a malformed string, aka "Excel Malformed String Vulnerability."
CVE-2007-0030
Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2, 2004 for Mac, and v.X for Mac allows user-assisted remote attackers to execute arbitrary code via an Excel file with an out-of-range Column field in certain BIFF8 record types, which references arbitrary memory.
CVE-2007-0031
Heap-based buffer overflow in Microsoft Excel 2000 SP3, 2002 SP3, 2003 SP2, 2004 for Mac, and v.X for Mac allows user-assisted remote attackers to execute arbitrary code via a BIFF8 spreadsheet with a PALETTE record that contains a large number of entries.
CVE-2007-0033
Microsoft Outlook 2002 and 2003 allows user-assisted remote attackers to execute arbitrary code via a malformed VEVENT record in an .iCal meeting request or ICS file.
CVE-2007-0034
Buffer overflow in the Advanced Search (Finder.exe) feature of Microsoft Outlook 2000, 2002, and 2003 allows user-assisted remote attackers to execute arbitrary code via a crafted Outlook Saved Searches (OSS) file that triggers memory corruption, aka "Microsoft Outlook Advanced Find Vulnerability."
CVE-2007-0102
The Adobe PDF specification 1.3, as implemented by Apple Mac OS X Preview, allows remote attackers to have an unknown impact, possibly including denial of service (infinite loop), arbitrary code execution, or memory corruption, via a PDF file with a (1) crafted catalog dictionary or (2) a crafted Pages attribute that references an invalid page tree node.
CVE-2007-0103
The Adobe PDF specification 1.3, as implemented by Adobe Acrobat before 8.0.0, allows remote attackers to have an unknown impact, possibly including denial of service (infinite loop), arbitrary code execution, or memory corruption, via a PDF file with a (1) crafted catalog dictionary or (2) a crafted Pages attribute that references an invalid page tree node.
CVE-2007-0104
The Adobe PDF specification 1.3, as implemented by (a) xpdf 3.0.1 patch 2, (b) kpdf in KDE before 3.5.5, (c) poppler before 0.5.4, and other products, allows remote attackers to have an unknown impact, possibly including denial of service (infinite loop), arbitrary code execution, or memory corruption, via a PDF file with a (1) crafted catalog dictionary or (2) a crafted Pages attribute that references an invalid page tree node.
CVE-2007-0105
Stack-based buffer overflow in the CSAdmin service in Cisco Secure Access Control Server (ACS) for Windows before 4.1 and ACS Solution Engine before 4.1 allows remote attackers to execute arbitrary code via a crafted HTTP GET request.
CVE-2007-0106
Cross-site scripting (XSS) vulnerability in the CSRF protection scheme in WordPress before 2.0.6 allows remote attackers to inject arbitrary web script or HTML via a CSRF attack with an invalid token and quote characters or HTML tags in URL variable names, which are not properly handled when WordPress generates a new link to verify the request.
CVE-2007-0107
WordPress before 2.0.6, when mbstring is enabled for PHP, decodes alternate character sets after escaping the SQL query, which allows remote attackers to bypass SQL injection protection schemes and execute arbitrary SQL commands via multibyte charsets, as demonstrated using UTF-7.
Successful exploitation requires that the "mbstring" extension be enabled.
This vulnerability is addressed in the following product release:
WordPress, WordPress, 2.0.6
CVE-2007-0108
nwgina.dll in Novell Client 4.91 SP3 for Windows 2000/XP/2003 does not delete user profiles during a Terminal Service or Citrix session, which allows remote authenticated users to invoke alternate user profiles.
CVE-2007-0109
wp-login.php in WordPress 2.0.5 and earlier displays different error messages if a user exists or not, which allows remote attackers to obtain sensitive information and facilitates brute force attacks.
CVE-2007-0110
Cross-site scripting (XSS) vulnerability in nidp/idff/sso in Novell Access Manager Identity Server before 3.0.0-1013 allows remote attackers to inject arbitrary web script or HTML via the IssueInstant parameter, which is not properly handled in the resulting error message.
CVE-2007-0111
Buffer overflow in Resco Photo Viewer for PocketPC 4.11 and 6.01, as used in mobile devices running Windows Mobile 5.0, 2003, and 2003SE, allows remote attackers to execute arbitrary code via a crafted PNG image.
CVE-2007-0112
SQL injection vulnerability in cats.asp in createauction allows remote attackers to execute arbitrary SQL commands via the catid parameter.
CVE-2007-0113
Buffer overflow in Packeteer PacketShaper PacketWise 8.x allows remote authenticated users to cause a denial of service (reset or reboot) via (1) a long traffic class argument to the "class show" command or (2) a long POLICY parameter value in clastree.htm.
CVE-2007-0114
Sun Java System Content Delivery Server 5.0 and 5.0 PU1 allows remote attackers to obtain sensitive information regarding "content details" via unspecified vectors.
CVE-2007-0115
Static code injection vulnerability in Coppermine Photo Gallery 1.4.10 and earlier allows remote authenticated administrators to execute arbitrary PHP code via the Username to login.php, which is injected into an error message in security.log.php, which can then be accessed using viewlog.php.
CVE-2007-0116
Digger Solutions Intranet Open Source (IOS) stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for data/intranet.mdb.
CVE-2007-0117
DiskManagementTool in the DiskManagement.framework 92.29 on Mac OS X 10.4.8 does not properly validate Bill of Materials (BOM) files, which allows attackers to gain privileges via a BOM file under /Library/Receipts/, which triggers arbitrary file permission changes upon execution of a diskutil permission repair operation.
CVE-2007-0118
Multiple absolute path traversal vulnerabilities in EditTag 1.2 allow remote attackers to read arbitrary files via an absolute pathname in the file parameter to (1) edittag.cgi, (2) edittag.pl, (3) edittag_mp.cgi, or (4) edittag_mp.pl.
CVE-2007-0119
Multiple cross-site scripting (XSS) vulnerabilities in EditTag 1.2 allow remote attackers to inject arbitrary web script or HTML via the plain parameter to (1) mkpw_mp.cgi, (2) mkpw.pl, or (3) mkpw.cgi.
CVE-2007-0120
Acunetix Web Vulnerability Scanner (WVS) 4.0 Build 20060717 and earlier allows remote attackers to cause a denial of service (application crash) via multiple HTTP requests containing invalid Content-Length values.
CVE-2007-0121
Cross-site scripting (XSS) vulnerability in search.asp in RI Blog 1.3 allows remote attackers to inject arbitrary web script or HTML via the q parameter.
CVE-2007-0122
Multiple SQL injection vulnerabilities in Coppermine Photo Gallery 1.4.10 and earlier allow remote authenticated administrators to execute arbitrary SQL commands via (1) the cat parameter to albmgr.php, and possibly (2) the gid parameter to usermgr.php; (3) the start parameter to db_ecard.php; and the albumid parameter to unspecified files, related to the (4) filename_to_title and (5) del_titles functions.
CVE-2007-0123
Unrestricted file upload vulnerability in Uber Uploader 4.2 allows remote attackers to upload and execute arbitrary PHP scripts by naming them with a .phtml extension, which bypasses the .php extension check but is still executable on some server configurations.
CVE-2007-0124
Unspecified vulnerability in Drupal before 4.6.11, and 4.7 before 4.7.5, when MySQL is used, allows remote authenticated users to cause a denial of service by poisoning the page cache via unspecified vectors, which triggers erroneous 404 HTTP errors for pages that exist.
CVE-2007-0125
Kaspersky Labs Antivirus Engine 6.0 for Windows and 5.5-10 for Linux before 20070102 enter an infinite loop upon encountering an invalid NumberOfRvaAndSizes value in the Optional Windows Header of a portable executable (PE) file, which allows remote attackers to cause a denial of service (CPU consumption) by scanning a crafted PE file.
CVE-2007-0126
Heap-based buffer overflow in Opera 9.02 allows remote attackers to execute arbitrary code via a JPEG file with an invalid number of index bytes in the Define Huffman Table (DHT) marker.
CVE-2007-0127
The Javascript SVG support in Opera before 9.10 does not properly validate object types in a createSVGTransformFromMatrix request, which allows remote attackers to execute arbitrary code via JavaScript code that uses an invalid object in this request that causes a controlled pointer to be referenced during the virtual function call.
CVE-2007-0128
SQL injection vulnerability in info_book.asp in Digirez 3.4 and earlier allows remote attackers to execute arbitrary SQL commands via the book_id parameter.
CVE-2007-0129
SQL injection vulnerability in main.asp in LocazoList 2.01a beta5 and earlier allows remote attackers to execute arbitrary SQL commands via the subcatID parameter.
CVE-2007-0130
SQL injection vulnerability in user.php in iGeneric iG Calendar 1.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0131
JAMWiki before 0.5.0 does not properly check permissions during moves of "read-only or admin-only topics," which allows remote attackers to make unauthorized changes to the wiki.
CVE-2007-0132
SQL injection vulnerability in compare_product.php in iGeneric iG Shop 1.4 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0133
Multiple SQL injection vulnerabilities in display_review.php in iGeneric iG Shop 1.4 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) id or (2) user_login_cookie parameter.
CVE-2007-0134
Multiple eval injection vulnerabilities in iGeneric iG Shop 1.0 allow remote attackers to execute arbitrary code via the action parameter, which is supplied to an eval function call in (1) cart.php and (2) page.php.  NOTE: a later report and CVE analysis indicate that the vulnerability is present in 1.4.
CVE-2007-0135
PHP remote file inclusion vulnerability in inc/init.inc.php in Aratix 0.2.2 beta 11 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the current_path parameter.
CVE-2007-0136
Multiple cross-site scripting (XSS) vulnerabilities in Drupal before 4.6.11, and 4.7 before 4.7.5, allow remote attackers to inject arbitrary web script or HTML via unspecified parameters in the (1) filter and (2) system modules.  NOTE: some of these details are obtained from third party information.
CVE-2007-0137
Cross-site scripting (XSS) vulnerability in SimpleBoxes/SerendipityNZ Serene Bach 2.05R and earlier, and 2.08D and earlier in the 2.08 series; and (2) sb 1.13D and earlier, and 1.18R and earlier in the 1.18 series; allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-0138
formbankcgi.exe in Fersch Formbankserver 1.9, when the PATH_INFO begins with (1) AbfrageForm or (2) EingabeForm, allows remote attackers to cause a denial of service (daemon crash) via multiple requests containing many /../ sequences in the Name parameter.  NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0139
Unspecified vulnerability in the DECnet-Plus 7.3-2 feature in DECnet/OSI 7.3-2 for OpenVMS ALPHA, and the DECnet-Plus 7.3 feature in DECnet/OSI 7.3 for OpenVMS VAX, allows attackers to obtain "unintended privileged access to data and system resources" via unspecified vectors, related to (1) [SYSEXE]CTF$UI.EXE, (2) [SYSMSG]CTF$MESSAGES.EXE, (3) [SYSHLP]CTF$HELP.HLB, and (4) [SYSMGR]CTF$STARTUP.COM.
CVE-2007-0140
SQL injection vulnerability in down.asp in Kolayindir Download (Yenionline) allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0141
Cross-site scripting (XSS) vulnerability in yald.php in Yet Another Link Directory 1.0 allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2007-0142
SQL injection vulnerability in orange.asp in ShopStoreNow E-commerce Shopping Cart allows remote attackers to execute arbitrary SQL commands via the CatID parameter.
CVE-2007-0143
Multiple PHP remote file inclusion vulnerabilities in NUNE News Script 2.0pre2 allow remote attackers to execute arbitrary PHP code via a URL in the custom_admin_path parameter to (1) index.php or (2) archives.php.
CVE-2007-0144
Cross-site scripting (XSS) vulnerability in search.asp in Digitizing Quote And Ordering System 1.0 allows remote authenticated attackers to inject arbitrary web script or HTML via the ordernum parameter.
CVE-2007-0145
PHP remote file inclusion vulnerability in bn_smrep1.php in BinGoPHP News (BP News) 3.01 allows remote attackers to execute arbitrary PHP code via a URL in the bnrep parameter, a different vector than CVE-2006-4648 and CVE-2006-4649.
CVE-2007-0146
Multiple cross-site scripting (XSS) vulnerabilities in Fix and Chips CMS 1.0 allow remote attackers to inject arbitrary web script or HTML via the (1) id parameter in (a) delete-announce.php; the (2) Announcement form field in (b) staff.php; the (3) Client Name, (4) Business Name, (5) Street, (6) Address 2, (7) Town/City, (8) Postcode, (9) Phone Number, (10) Email Address and (11) Website Address form fields in (c) new_customer.php; and unspecified fields in (d) search.php and (e) client-results.php.
CVE-2007-0147
Cuyahoga before 1.0.1 installs the FCKEditor component with an incorrect deny statement in a Web.config file, which allows remote attackers to upload files when these privileges were intended only for the Administrator and Editor roles.
CVE-2007-0148
Format string vulnerability in OmniGroup OmniWeb 5.5.1 allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via format string specifiers in the Javascript alert function.
CVE-2007-0149
EMembersPro 1.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for users.mdb.
CVE-2007-0150
Multiple PHP remote file inclusion vulnerabilities in index.php in Dayfox Blog allow remote attackers to execute arbitrary PHP code via a URL in the (1) page, (2) subject, and (3) q parameters.
CVE-2007-0151
MitiSoft stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for access_MS/MitiSoft.mdb.
CVE-2007-0152
OhhASP stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for db/OhhASP.mdb.
CVE-2007-0153
AJLogin 3.5 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for ajlogin.mdb.
CVE-2007-0154
Webulas stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for db/db.mdb.
CVE-2007-0155
HarikaOnline 2.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for harikaonline.mdb.
CVE-2007-0156
M-Core stores the database under the web document root, which allows remote attackers to obtain sensitive information via a direct request to db/uyelik.mdb.
CVE-2007-0157
Array index error in the uri_lookup function in the URI parser for neon 0.26.0 to 0.26.2, possibly only on 64-bit platforms, allows remote malicious servers to cause a denial of service (crash) via a URI with non-ASCII characters, which triggers a buffer under-read due to a type conversion error that generates a negative index.
