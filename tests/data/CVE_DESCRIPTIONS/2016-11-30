CVE-2016-2869
Multiple cross-site scripting (XSS) vulnerabilities in the UI in IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 allow remote authenticated users to inject arbitrary web script or HTML via crafted fields in a URL.
CVE-2016-2871
IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 uses cleartext storage for unspecified passwords, which allows local users to obtain sensitive information by reading a configuration file.
CVE-2016-2873
SQL injection vulnerability in IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2016-2874
IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 mishandles authorization, which allows remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2016-2876
IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 executes unspecified processes at an incorrect privilege level, which makes it easier for remote authenticated users to obtain root access by leveraging a command-injection issue.
CVE-2016-2877
IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 uses weak permissions for unspecified directories under the web root, which allows local users to modify data by writing to a file.
CVE-2016-2878
Multiple cross-site request forgery (CSRF) vulnerabilities in IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 allow remote attackers to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2016-2881
IBM QRadar SIEM 7.1 before MR2 Patch 13 and 7.2 before 7.2.7 and QRadar Incident Forensics 7.2 before 7.2.7 allow remote attackers to bypass intended access restrictions via modified request parameters.
CVE-2016-2884
Cross-site request forgery (CSRF) vulnerability in IBM Forms Experience Builder 8.5.x and 8.6.x before 8.6.3.1, in an unspecified non-default configuration, allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2016-2887
IBM IMS Enterprise Suite Data Provider before 3.2.0.1 for Microsoft .NET allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-2917
The notifications component in IBM TRIRIGA Applications 10.4 and 10.5 before 10.5.1 allows remote authenticated users to obtain sensitive password information, and consequently gain privileges, via unspecified vectors.
CVE-2016-2931
IBM BigFix Remote Control before 9.1.3 allows remote attackers to obtain sensitive cleartext information by sniffing the network.
CVE-2016-2932
IBM BigFix Remote Control before 9.1.3 allows remote attackers to conduct XML injection attacks via unspecified vectors.
CVE-2016-2933
Directory traversal vulnerability in IBM BigFix Remote Control before 9.1.3 allows remote authenticated administrators to read arbitrary files via a crafted request.
CVE-2016-2934
Cross-site scripting (XSS) vulnerability in IBM BigFix Remote Control before 9.1.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-2935
The broker application in IBM BigFix Remote Control before 9.1.3 allows remote attackers to cause a denial of service via an invalid HTTP request.
CVE-2016-2936
IBM BigFix Remote Control before 9.1.3 uses cleartext storage for unspecified passwords, which allows local users to obtain sensitive information via unknown vectors.
CVE-2016-2937
IBM BigFix Remote Control before 9.1.3 allows remote attackers to obtain sensitive information or spoof e-mail transmission via a crafted POST request, related to an "untrusted information vulnerability."
CVE-2016-2940
Multiple unspecified vulnerabilities in IBM BigFix Remote Control before 9.1.3 allow remote attackers to obtain sensitive information via unknown vectors.
CVE-2016-2943
IBM BigFix Remote Control before 9.1.3 allows local users to obtain sensitive information by leveraging unspecified privileges to read a log file.
CVE-2016-2944
IBM BigFix Remote Control before 9.1.3 does not properly restrict failed login attempts, which makes it easier for remote attackers to obtain access via a brute-force approach.
CVE-2016-2948
IBM BigFix Remote Control before 9.1.3 allows local users to discover hardcoded credentials via unspecified vectors.
CVE-2016-2949
IBM BigFix Remote Control before 9.1.3 allows local users to obtain sensitive information by reading cached web pages from a different user's session.
CVE-2016-2950
SQL injection vulnerability in IBM BigFix Remote Control before 9.1.3 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2016-2951
IBM BigFix Remote Control before 9.1.3 does not properly set the default encryption strength, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by sniffing the network and performing calculations on encrypted data.
CVE-2016-2952
IBM BigFix Remote Control before 9.1.3 does not enable the HSTS protection mechanism, which makes it easier for remote attackers to obtain sensitive information by leveraging use of HTTP.
CVE-2016-2953
IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 does not require SSL, which allows remote attackers to obtain sensitive cleartext information by sniffing the network.
CVE-2016-2957
IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 allows remote authenticated users to obtain sensitive information by reading a stack trace in a response.
CVE-2016-2958
IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 allows remote authenticated users to obtain sensitive information by reading an "archaic" e-mail address in a response.
CVE-2016-2963
Cross-site request forgery (CSRF) vulnerability in IBM BigFix Remote Control before 9.1.3 allows remote attackers to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2016-3002
IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 allows physically proximate attackers to obtain sensitive information by reading cached data on a client device.
CVE-2016-3004
Cross-site request forgery (CSRF) vulnerability in IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 allows remote authenticated users to hijack the authentication of arbitrary users for requests that modify the set of available applications.
CVE-2016-3009
Cross-site request forgery (CSRF) vulnerability in IBM Connections 4.0 through CR4, 4.5 through CR5, and 5.0 before CR4 allows remote authenticated users to hijack the authentication of arbitrary users for requests that modify the Connections generic page.
CVE-2016-3014
Cross-site scripting (XSS) vulnerability in IBM Rational Collaborative Lifecycle Management 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, Rational Quality Manager 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, Rational Team Concert 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, Rational DOORS Next Generation 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, Rational Engineering Lifecycle Manager 4.x before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, Rational Rhapsody Design Manager 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17, and Rational Software Architect Design Manager 4.0 before 4.0.7 iFix11 and 5.0 before 5.0.2 iFix17 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-3057
Cross-site scripting (XSS) vulnerability in IBM Sterling B2B Integrator 5.2 before 5020500_14 and 5.2 06 before 5020602_1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5890
IBM Sterling B2B Integrator 5.2 before 5020500_14 and 5.2 06 before 5020602_1 allows remote authenticated users to change arbitrary passwords via unspecified vectors.
CVE-2016-5905
Cross-site scripting (XSS) vulnerability in IBM Maximo Asset Management 7.5 before 7.5.0.10 IF3 and 7.6 before 7.6.0.5 IF2 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-5987
IBM Maximo Asset Management 7.1 through 7.1.1.13, 7.5 before 7.5.0.10 IF4, and 7.6 before 7.6.0.5 IF3 allows remote attackers to obtain sensitive information via a crafted HTTP request that triggers construction of a runtime error message.
CVE-2016-8222
A vulnerability has been identified in a signed kernel driver for the BIOS of some ThinkPad systems that can allow an attacker with Windows administrator-level privileges to call System Management Mode (SMM) services. This could lead to a denial of service attack or allow certain BIOS variables or settings to be altered (such as boot sequence). The setting or changing of BIOS passwords is not affected by this vulnerability.
CVE-2016-9564
Buffer overflow in send_redirect() in Boa Webserver 0.92r allows remote attackers to DoS via an HTTP GET request requesting a long URI with only '/' and '.' characters.
