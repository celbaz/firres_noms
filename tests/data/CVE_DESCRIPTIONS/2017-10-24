CVE-2013-3734
** DISPUTED ** The Embedded Jopr component in JBoss Application Server includes the cleartext datasource password in unspecified HTML responses, which might allow (1) man-in-the-middle attackers to obtain sensitive information by leveraging failure to use SSL or (2) attackers to obtain sensitive information by reading the HTML source code.  NOTE: the vendor says that this does not cross a trust boundary and that it is recommended best-practice that SSL is configured for the administrative console.
CVE-2014-0691
Cisco WebEx Meetings Server before 1.1 uses meeting IDs with insufficient entropy, which makes it easier for remote attackers to bypass authentication and join arbitrary meetings without a password, aka Bug ID CSCuc79643.
CVE-2014-1203
The get_login_ip_config_file function in Eyou Mail System before 3.6 allows remote attackers to execute arbitrary commands via shell metacharacters in the domain parameter to admin/domain/ip_login_set/d_ip_login_get.php.
CVE-2015-5170
Cloud Foundry Runtime cf-release before 216, UAA before 2.5.2, and Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.7.0 allow remote attackers to conduct cross-site request forgery (CSRF) attacks on PWS and log a user into an arbitrary account by leveraging lack of CSRF checks.
CVE-2015-5171
The password change functionality in Cloud Foundry Runtime cf-release before 216, UAA before 2.5.2, and Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.7.0 allow attackers to have unspecified impact by leveraging failure to expire existing sessions.
CVE-2015-5172
Cloud Foundry Runtime cf-release before 216, UAA before 2.5.2, and Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.7.0 allow attackers to have unspecified impact by leveraging failure to expire password reset links.
CVE-2015-5173
Cloud Foundry Runtime cf-release before 216, UAA before 2.5.2, and Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.7.0 allow attackers to have unspecified impact via vectors involving emails with password recovery links, aka "Cross Domain Referer Leakage."
CVE-2016-10517
networking.c in Redis before 3.2.7 allows "Cross Protocol Scripting" because it lacks a check for POST and Host: strings, which are not valid in the Redis protocol (but commonly occur when an attack triggers an HTTP request to the Redis TCP port).
CVE-2016-3049
IBM OpenPages GRC Platform 7.1, 7.2, and 7.3 is vulnerable to HTML injection. A remote attacker could inject malicious HTML code, which when viewed, would be executed in the victim's Web browser within the security context of the hosting site. IBM X-Force ID: 114712.
CVE-2017-1209
IBM Daeja ViewONE Professional, Standard & Virtual 4.1.5.1 and 5.0.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 123849.
CVE-2017-1210
IBM Daeja ViewONE Professional, Standard & Virtual 4.1.5.1 and 5.0.2 could allow an unauthenticated attacker to inject data into log files made to look legitimate. IBM X-Force ID: 123850.
CVE-2017-1211
IBM Daeja ViewONE Professional, Standard & Virtual 4.1.5.1 and 5.0.2 could disclose sensitive information to a local user when logging is enabled. IBM X-Force ID: 123851.
CVE-2017-1212
IBM Daeja ViewONE Professional, Standard & Virtual 4.1.5.1 and 5.0.2 is vulnerable to a denial of service when viewing or opening a large file. IBM X-Force ID: 123852.
CVE-2017-12613
When apr_time_exp*() or apr_os_exp_time*() functions are invoked with an invalid month field value in Apache Portable Runtime APR 1.6.2 and prior, out of bounds memory may be accessed in converting this value to an apr_time_exp_t value, potentially revealing the contents of a different static heap value or resulting in program termination, and may represent an information disclosure or denial of service vulnerability to applications which call these APR functions with unvalidated external input.
CVE-2017-12618
Apache Portable Runtime Utility (APR-util) 1.6.0 and prior fail to validate the integrity of SDBM database files used by apr_sdbm*() functions, resulting in a possible out of bound read access. A local user with write access to the database can make a program or process using these functions crash, and cause a denial of service.
CVE-2017-1375
IBM System Storage Storwize V7000 Unified (V7000U) 1.5 and 1.6 uses weaker than expected cryptographic algorithms that could allow an attacker to decrypt highly sensitive information. IBM X-Force ID: 126868.
CVE-2017-14695
Directory traversal vulnerability in minion id validation in SaltStack Salt before 2016.3.8, 2016.11.x before 2016.11.8, and 2017.7.x before 2017.7.2 allows remote minions with incorrect credentials to authenticate to a master via a crafted minion ID.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2017-12791.
CVE-2017-14696
SaltStack Salt before 2016.3.8, 2016.11.x before 2016.11.8, and 2017.7.x before 2017.7.2 allows remote attackers to cause a denial of service via a crafted authentication request.
CVE-2017-15081
In PHPSUGAR PHP Melody CMS 2.6.1, SQL Injection exists via the playlist parameter to playlists.php.
CVE-2017-15186
Double free vulnerability in FFmpeg 3.3.4 and earlier allows remote attackers to cause a denial of service via a crafted AVI file.
CVE-2017-15222
Buffer Overflow vulnerability in Ayukov NFTPD 2.0 and earlier allows remote attackers to execute arbitrary code.
CVE-2017-15223
Denial-of-service vulnerability in ArGoSoft Mini Mail Server 1.0.0.2 and earlier allows remote attackers to waste CPU resources (memory consumption) via unspecified vectors, possibly triggering an infinite loop.
CVE-2017-1523
IBM InfoSphere Master Data Management - Collaborative Edition 11.5 could allow an unauthorized user to download reports without authentication. IBM X-Force ID: 129892.
CVE-2017-1583
IBM WebSphere Application Server (IBM Liberty for Java for Bluemix 3.13)could allow a remote attacker to obtain sensitive information caused by improper error handling by MyFaces in JSF.
CVE-2017-15863
Cross Site Scripting (XSS) exists in the wp-noexternallinks plugin before 3.5.19 for WordPress via the date1 or date2 parameter to wp-admin/options-general.php.
CVE-2017-15867
Multiple cross-site scripting (XSS) vulnerabilities in the user-login-history plugin through 1.5.2 for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) date_from, (2) date_to, (3) user_id, (4) username, (5) country_name, (6) browser, (7) operating_system, or (8) ip_address parameter to admin/partials/listing/listing.php.
CVE-2017-15871
** DISPUTED ** The deserialize function in serialize-to-js through 1.1.1 allows attackers to cause a denial of service via vectors involving an Immediately Invoked Function Expression "function()" substring, as demonstrated by a "function(){console.log(" call or a simple infinite loop. NOTE: the vendor agrees that denial of service can occur but notes that deserialize is explicitly listed as "harmful" within the README.md file.
CVE-2017-15872
phpwcms 1.8.9 has XSS in include/inc_tmpl/admin.edituser.tmpl.php and include/inc_tmpl/admin.newuser.tmpl.php via the username (aka new_login) field.
CVE-2017-15873
The get_next_block function in archival/libarchive/decompress_bunzip2.c in BusyBox 1.27.2 has an Integer Overflow that may lead to a write access violation.
CVE-2017-15874
archival/libarchive/decompress_unlzma.c in BusyBox 1.27.2 has an Integer Underflow that leads to a read access violation.
CVE-2017-15878
A cross-site scripting (XSS) vulnerability exists in fields/types/markdown/MarkdownType.js in KeystoneJS before 4.0.0-beta.7 via the Contact Us feature.
CVE-2017-15879
CSV Injection (aka Excel Macro Injection or Formula Injection) exists in admin/server/api/download.js and lib/list/getCSVData.js in KeystoneJS before 4.0.0-beta.7 via a value that is mishandled in a CSV export.
CVE-2017-15880
SQL injection vulnerability vulnerability in the EyesOfNetwork web interface (aka eonweb) 5.1-0 allows remote authenticated administrators to execute arbitrary SQL commands via the group_name parameter to module/admin_group/add_modify_group.php (for insert_group and update_group).
CVE-2017-15881
Cross-Site Scripting vulnerability in KeystoneJS before 4.0.0-beta.7 allows remote authenticated administrators to inject arbitrary web script or HTML via the "content brief" or "content extended" field, a different vulnerability than CVE-2017-15878.
