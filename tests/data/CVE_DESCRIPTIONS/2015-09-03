CVE-2015-1291
The ContainerNode::parserRemoveChild function in core/dom/ContainerNode.cpp in Blink, as used in Google Chrome before 45.0.2454.85, does not check whether a node is expected, which allows remote attackers to bypass the Same Origin Policy or cause a denial of service (DOM tree corruption) via a web site with crafted JavaScript code and IFRAME elements.
CVE-2015-1292
The NavigatorServiceWorker::serviceWorker function in modules/serviceworkers/NavigatorServiceWorker.cpp in Blink, as used in Google Chrome before 45.0.2454.85, allows remote attackers to bypass the Same Origin Policy by accessing a Service Worker.
CVE-2015-1293
The DOM implementation in Blink, as used in Google Chrome before 45.0.2454.85, allows remote attackers to bypass the Same Origin Policy via unspecified vectors.
CVE-2015-1294
Use-after-free vulnerability in the SkMatrix::invertNonIdentity function in core/SkMatrix.cpp in Skia, as used in Google Chrome before 45.0.2454.85, allows remote attackers to cause a denial of service or possibly have unspecified other impact by triggering the use of matrix elements that lead to an infinite result during an inversion calculation.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1295
Multiple use-after-free vulnerabilities in the PrintWebViewHelper class in components/printing/renderer/print_web_view_helper.cc in Google Chrome before 45.0.2454.85 allow user-assisted remote attackers to cause a denial of service or possibly have unspecified other impact by triggering nested IPC messages during preparation for printing, as demonstrated by messages associated with PDF documents in conjunction with messages about printer capabilities.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1296
The UnescapeURLWithAdjustmentsImpl implementation in net/base/escape.cc in Google Chrome before 45.0.2454.85 does not prevent display of Unicode LOCK characters in the omnibox, which makes it easier for remote attackers to spoof the SSL lock icon by placing one of these characters at the end of a URL, as demonstrated by the omnibox in localizations for right-to-left languages.
CVE-2015-1297
The WebRequest API implementation in extensions/browser/api/web_request/web_request_api.cc in Google Chrome before 45.0.2454.85 does not properly consider a request's source before accepting the request, which allows remote attackers to bypass intended access restrictions via a crafted (1) app or (2) extension.
CVE-2015-1298
The RuntimeEventRouter::OnExtensionUninstalled function in extensions/browser/api/runtime/runtime_api.cc in Google Chrome before 45.0.2454.85 does not ensure that the setUninstallURL preference corresponds to the URL of a web site, which allows user-assisted remote attackers to trigger access to an arbitrary URL via a crafted extension that is uninstalled.
CVE-2015-1299
Use-after-free vulnerability in the shared-timer implementation in Blink, as used in Google Chrome before 45.0.2454.85, allows remote attackers to cause a denial of service or possibly have unspecified other impact by leveraging erroneous timer firing, related to ThreadTimers.cpp and Timer.cpp.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1300
The FrameFetchContext::updateTimingInfoForIFrameNavigation function in core/loader/FrameFetchContext.cpp in Blink, as used in Google Chrome before 45.0.2454.85, does not properly restrict the availability of IFRAME Resource Timing API times, which allows remote attackers to obtain sensitive information via crafted JavaScript code that leverages a history.back call.
CVE-2015-1301
Multiple unspecified vulnerabilities in Google Chrome before 45.0.2454.85 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2015-1516
Cross-site scripting (XSS) vulnerability in Polycom RealPresence CloudAXIS Suite before 1.7.0 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-4077
The (1) mdare64_48.sys, (2) mdare32_48.sys, (3) mdare32_52.sys, and (4) mdare64_52.sys drivers in Fortinet FortiClient before 5.2.4 allow local users to read arbitrary kernel memory via a 0x22608C ioctl call.
CVE-2015-4552
Cross-site scripting (XSS) vulnerability in the quick edit function in xmlhttp.php in MyBB (aka MyBulletinBoard) before 1.8.5 allows remote attackers to inject arbitrary web script or HTML via the content of a post.
CVE-2015-5189
Race condition in pcsd in PCS 0.9.139 and earlier uses a global variable to validate usernames, which allows remote authenticated users to gain privileges by sending a command that is checked for security after another user is authenticated.
CVE-2015-5190
The pcsd web UI in PCS 0.9.139 and earlier allows remote authenticated users to execute arbitrary commands via "escape characters" in a URL.
CVE-2015-5735
The (1) mdare64_48.sys, (2) mdare32_48.sys, (3) mdare32_52.sys, and (4) mdare64_52.sys drivers in Fortinet FortiClient before 5.2.4 allow local users to write to arbitrary memory locations via a 0x226108 ioctl call.
CVE-2015-5736
The Fortishield.sys driver in Fortinet FortiClient before 5.2.4 allows local users to execute arbitrary code with kernel privileges by setting the callback function in a (1) 0x220024 or (2) 0x220028 ioctl call.
CVE-2015-5737
The (1) mdare64_48.sys, (2) mdare32_48.sys, (3) mdare32_52.sys, (4) mdare64_52.sys, and (5) Fortishield.sys drivers in Fortinet FortiClient before 5.2.4 do not properly restrict access to the API for management of processes and the Windows registry, which allows local users to obtain a privileged handle to a PID and possibly have unspecified other impact, as demonstrated by a 0x2220c8 ioctl call.
CVE-2015-6506
Cross-site scripting (XSS) vulnerability in the cryptography interface in Request Tracker (RT) before 4.2.12 allows remote attackers to inject arbitrary web script or HTML via a crafted public key.
CVE-2015-6545
Cross-site request forgery (CSRF) vulnerability in ajax.php in Cerb before 7.0.4 allows remote attackers to hijack the authentication of administrators for requests that add an administrator account via a saveWorkerPeek action.
CVE-2015-6580
Multiple unspecified vulnerabilities in Google V8 before 4.5.103.29, as used in Google Chrome before 45.0.2454.85, allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2015-6581
Double free vulnerability in the opj_j2k_copy_default_tcp_and_create_tcd function in j2k.c in OpenJPEG before r3002, as used in PDFium in Google Chrome before 45.0.2454.85, allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) by triggering a memory-allocation failure.
<a href="http://cwe.mitre.org/data/definitions/415.html">CWE-415: Double Free</a>
CVE-2015-6582
The decompose function in platform/transforms/TransformationMatrix.cpp in Blink, as used in Google Chrome before 45.0.2454.85, does not verify that a matrix inversion succeeded, which allows remote attackers to cause a denial of service (uninitialized memory access and application crash) or possibly have unspecified other impact via a crafted web site.
CVE-2015-6583
Google Chrome before 45.0.2454.85 does not display a location bar for a hosted app's window after navigation away from the installation site, which might make it easier for remote attackers to spoof content via a crafted app, related to browser.cc and hosted_app_browser_controller.cc.
CVE-2015-6654
The xenmem_add_to_physmap_one function in arch/arm/mm.c in Xen 4.5.x, 4.4.x, and earlier does not limit the number of printk console messages when reporting a failure to retrieve a reference on a foreign page, which allows remote domains to cause a denial of service by leveraging permissions to map the memory of a foreign guest.
