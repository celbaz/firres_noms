CVE-2012-0417
Integer overflow in GroupWise Internet Agent (GWIA) in Novell GroupWise 8.0 before Support Pack 3 and 2012 before Support Pack 1 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2012-0418
Unspecified vulnerability in the client in Novell GroupWise 8.0 before Support Pack 3 and 2012 before Support Pack 1 on Windows allows user-assisted remote attackers to execute arbitrary code via a crafted file.
CVE-2012-0419
Directory traversal vulnerability in the agent HTTP interfaces in Novell GroupWise 8.0 before Support Pack 3 and 2012 before Support Pack 1 allows remote attackers to read arbitrary files via directory traversal sequences in a request.
CVE-2012-0956
ubiquity-slideshow-ubuntu before 58.2, during installation, allows remote man-in-the-middle attackers to execute arbitrary web script or HTML and read arbitrary files via a crafted attribute in the <a> tag of a Twitter feed.
CVE-2012-1833
VMware SpringSource Grails before 1.3.8, and 2.x before 2.0.2, does not properly restrict data binding, which might allow remote attackers to bypass intended access restrictions and modify arbitrary object properties via a crafted request parameter to an application.
CVE-2012-2145
Apache Qpid 0.17 and earlier does not properly restrict incoming client connections, which allows remote attackers to cause a denial of service (file descriptor consumption) via a large number of incomplete connections.
CVE-2012-2680
Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, does not properly restrict access to resources, which allows remote attackers to obtain sensitive information via unspecified vectors related to (1) "web pages," (2) "export functionality," and (3) "image viewing."
CVE-2012-2681
Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, uses predictable random numbers to generate session keys, which makes it easier for remote attackers to guess the session key.
CVE-2012-2683
Multiple cross-site scripting (XSS) vulnerabilities in Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to (1) "error message displays" or (2) "in source HTML on certain pages."
CVE-2012-2684
Multiple SQL injection vulnerabilities in the get_sample_filters_by_signature function in Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allow remote attackers to execute arbitrary SQL commands via the (1) agent or (2) object id.
CVE-2012-2685
Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allows remote authenticated users to cause a denial of service (memory consumption) via a large size in an image request.
CVE-2012-2734
Multiple cross-site request forgery (CSRF) vulnerabilities in Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allow remote attackers to hijack the authentication of arbitrary users for requests that execute commands via unspecified vectors.
CVE-2012-2735
Session fixation vulnerability in Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allows remote attackers to hijack web sessions via a crafted session cookie.
Per: http://rhn.redhat.com/errata/RHSA-2012-1278.html

" An authenticated user able to
pre-set the Cumin session cookie in a victim's browser could possibly use
this flaw to steal the victim's session after they log into Cumin."
Per: http://cwe.mitre.org/data/definitions/384.html 'CWE-384: Session Fixation'
CVE-2012-2998
SQL injection vulnerability in the ad hoc query module in Trend Micro Control Manager (TMCM) before 5.5.0.1823 and 6.0 before 6.0.0.1449 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-3459
Cumin before 0.1.5444, as used in Red Hat Enterprise Messaging, Realtime, and Grid (MRG) 2.0, allows remote authenticated users to modify Condor attributes and possibly gain privileges via crafted additional parameters in an HTTP POST request, which triggers a job attribute change request to Condor.
CVE-2012-3491
src/condor_schedd.V6/schedd.cpp in Condor 7.6.x before 7.6.10 and 7.8.x before 7.8.4 does not properly check the permissions of jobs, which allows remote authenticated users to remove arbitrary idle jobs via unspecified vectors.
CVE-2012-3492
The filesystem authentication (condor_io/condor_auth_fs.cpp) in Condor 7.6.x before 7.6.10 and 7.8.x before 7.8.4 uses authentication directories even when they have weak permissions, which allows remote attackers to impersonate users by renaming a user's authentication directory.
CVE-2012-3493
The command_give_request_ad function in condor_startd.V6/command.cpp Condor 7.6.x before 7.6.10 and 7.8.x before 7.8.4 allows remote attackers to obtain sensitive information, and possibly control or start arbitrary jobs, via a ClassAd request to the condor_startd port, which leaks the ClaimId.
CVE-2012-4016
The ATOK application before 1.0.4 for Android allows remote attackers to read the learning information file, and obtain sensitive input-string information, via a crafted application.
CVE-2012-4017
The jigbrowser+ application before 1.5.0 for Android does not properly implement the WebView class, which allows remote attackers to obtain sensitive information via a crafted application.
CVE-2012-4051
Multiple cross-site request forgery (CSRF) vulnerabilities in editAccount.html in the JAMF Software Server (JSS) interface in JAMF Casper Suite before 8.61 allow remote attackers to hijack the authentication of administrators for requests that (1) create user accounts or (2) change passwords via a Save action.
CVE-2012-4448
Cross-site request forgery (CSRF) vulnerability in wp-admin/index.php in WordPress 3.4.2 allows remote attackers to hijack the authentication of administrators for requests that modify an RSS URL via a dashboard_incoming_links edit action.
CVE-2012-4912
Cross-site scripting (XSS) vulnerability in the WebAccess component in Novell GroupWise 8.0 before Support Pack 3 and 2012 before Support Pack 1 allows remote attackers to inject arbitrary web script or HTML via a crafted signature in an HTML e-mail message.
CVE-2012-5048
APIFTP Server in Optimalog Optima PLC 1.5.2 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via a crafted packet.
CVE-2012-5049
APIFTP Server in Optimalog Optima PLC 1.5.2 and earlier allows remote attackers to cause a denial of service (infinite loop) via a malformed packet.
CVE-2012-5196
Multiple buffer overflows in Condor 7.6.x before 7.6.10 and 7.8.x before 7.8.4 have unknown impact and attack vectors.
CVE-2012-5197
Multiple unspecified vulnerabilities in Condor 7.6.x before 7.6.10 and 7.8.x before 7.8.4 have unknown impact and attack vectors related to "error checking of system calls."
