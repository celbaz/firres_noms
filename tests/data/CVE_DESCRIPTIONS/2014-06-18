CVE-2011-2592
Heap-based buffer overflow in the StartEpa method in the nsepacom ActiveX control (nsepa.exe) in Citrix Access Gateway Enterprise Edition Plug-in for Windows 9.x before 9.3-57.5 and 10.0 before 10.0-69.4 allows remote attackers to execute arbitrary code via a long CSEC HTTP response header.
CVE-2012-2592
Cross-site scripting (XSS) vulnerability in Axigen Mail Server 8.0.1 allows remote attackers to inject arbitrary web script or HTML via the body of an email.
CVE-2013-5017
SNMPConfig.php in the management console in Symantec Web Gateway (SWG) before 5.2.1 allows remote attackers to execute arbitrary commands via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2013-6221
Directory traversal vulnerability in CommunicationServlet in HP Service Virtualization 3.x before 3.50.1, when the AutoPass license server is enabled, allows remote attackers to create arbitrary files and consequently execute arbitrary code via unspecified vectors, aka ZDI-CAN-2031.
CVE-2014-0598
Directory traversal vulnerability in iPrint in Novell Open Enterprise Server (OES) 11 SP1 before Maintenance Update 9151 on Linux has unspecified impact and remote attack vectors.
CVE-2014-0599
Cross-site scripting (XSS) vulnerability in iPrint in Novell Open Enterprise Server (OES) 11 SP1 before Maintenance Update 9151 on Linux allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-0910
Cross-site scripting (XSS) vulnerability in IBM WebSphere Portal 6.1.0.0 through 6.1.0.6 CF27, 6.1.5.0 through 6.1.5.3 CF27, and 7.0.0 through 7.0.0.2 CF28 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-1650
SQL injection vulnerability in user.php in the management console in Symantec Web Gateway (SWG) before 5.2.1 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-1651
SQL injection vulnerability in clientreport.php in the management console in Symantec Web Gateway (SWG) before 5.2 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-1652
Multiple cross-site scripting (XSS) vulnerabilities in the management console in Symantec Web Gateway (SWG) before 5.2 allow remote authenticated users to inject arbitrary web script or HTML via unspecified report parameters.
CVE-2014-2000
The NTT 050 plus application before 4.2.1 for Android allows attackers to obtain sensitive information by leveraging the ability to read system log files.
CVE-2014-2151
The WebVPN portal in Cisco Adaptive Security Appliance (ASA) Software 8.4(.7.15) and earlier allows remote authenticated users to obtain sensitive information via a crafted JavaScript file, aka Bug ID CSCui04520.
CVE-2014-2779
mpengine.dll in Microsoft Malware Protection Engine before 1.1.10701.0 allows remote attackers to cause a denial of service (system hang) via a crafted file.
CVE-2014-2949
SQL injection vulnerability in the web service in F5 ARX Data Manager 3.0.0 through 3.1.0 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-3012
Multiple CRLF injection vulnerabilities in IBM Curam Social Program Management 5.2 SP1 through 6.0.5.4 allow remote authenticated users to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified parameters to custom JSPs.
Per: http://cwe.mitre.org/data/definitions/93.html

"CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')"
CVE-2014-3013
Multiple cross-site scripting (XSS) vulnerabilities in IBM Curam Social Program Management 4.5 SP10 through 6.0.5.4 allow remote authenticated users to inject arbitrary web script or HTML via crafted input to a (1) custom JSP or (2) custom renderer.
CVE-2014-3876
Multiple cross-site scripting (XSS) vulnerabilities in Frams' Fast File EXchange (F*EX, aka fex) before fex-20140530 allow remote attackers to inject arbitrary web script or HTML via the (1) akey parameter to rup or (2) disclaimer or (3) gm parameter to fuc.
CVE-2014-3877
Incomplete blacklist vulnerability in Frams' Fast File EXchange (F*EX, aka fex) before fex-20140530 allows remote attackers to conduct cross-site scripting (XSS) attacks via the addto parameter to fup.
Per: http://cwe.mitre.org/data/definitions/184.html

"CWE-184: Incomplete Blacklist"
CVE-2014-4020
The dissect_frame function in epan/dissectors/packet-frame.c in the frame metadissector in Wireshark 1.10.x before 1.10.8 interprets a negative integer as a length value even though it was intended to represent an error condition, which allows remote attackers to cause a denial of service (application crash) via a crafted packet.
CVE-2014-4021
Xen 3.2.x through 4.4.x does not properly clean memory pages recovered from guests, which allows local guest OS users to obtain sensitive information via unspecified vectors.
CVE-2014-4049
Heap-based buffer overflow in the php_parserr function in ext/standard/dns.c in PHP 5.6.0beta4 and earlier allows remote servers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted DNS TXT record, related to the dns_get_record function.
CVE-2014-4151
The av-centerd SOAP service in AlienVault OSSIM before 4.8.0 allows remote attackers to create arbitrary files and execute arbitrary code via a crafted set_file request.
CVE-2014-4152
The av-centerd SOAP service in AlienVault OSSIM before 4.8.0 allows remote attackers to execute arbitrary code via a crafted remote_task request, related to injecting an ssh public key.
CVE-2014-4153
The av-centerd SOAP service in AlienVault OSSIM before 4.8.0 allows remote attackers to read arbitrary files via a crafted get_file request.
CVE-2014-4174
wiretap/libpcap.c in the libpcap file parser in Wireshark 1.10.x before 1.10.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted packet-trace file that includes a large packet.
CVE-2014-4286
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2013-4286.  Reason: This candidate is a duplicate of CVE-2013-4286.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2013-4286 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2014-4301
Multiple cross-site scripting (XSS) vulnerabilities in the respond_error function in routing.py in Eugene Pankov Ajenti before 1.2.21.7 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) resources.js or (2) resources.css in ajenti:static/, related to the traceback page.
CVE-2014-4302
Cross-site scripting (XSS) vulnerability in rating/rating.php in HAM3D Shop Engine allows remote attackers to inject arbitrary web script or HTML via the ID parameter.
CVE-2014-4303
Multiple cross-site scripting (XSS) vulnerabilities in the Touch theme 7.x-1.x before 7.x-1.9 for Drupal allow remote authenticated users with the Administer themes permission to inject arbitrary web script or HTML via vectors related to the (1) Twitter and (2) Facebook username settings.
CVE-2014-4304
Cross-site scripting (XSS) vulnerability in browse.php in SQL Buddy 1.3.3 and earlier allows remote attackers to inject arbitrary web script or HTML via the table parameter.
CVE-2014-4305
Multiple SQL injection vulnerabilities in NICE Recording eXpress (aka Cybertech eXpress) 6.5.7 and earlier allow remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-4306
Directory traversal vulnerability in logs-x.php in WebTitan before 4.04 allows remote attackers to read arbitrary files via a .. (dot dot) in the logfile parameter in a download action.
CVE-2014-4307
SQL injection vulnerability in categories-x.php in WebTitan before 4.04 allows remote attackers to execute arbitrary SQL commands via the sortkey parameter.
CVE-2014-4308
Multiple cross-site scripting (XSS) vulnerabilities in NICE Recording eXpress (aka Cybertech eXpress) before 6.5.5 allow remote attackers to inject arbitrary web script or HTML via the (1) USRLNM parameter to myaccount/mysettings.edit.validate.asp or the frame parameter to (2) iframe.picker.statchannels.asp, (3) iframe.picker.channelgroups.asp, (4) iframe.picker.extensions.asp, (5) iframe.picker.licenseusergroups.asp, (6) iframe.picker.licenseusers.asp, (7) iframe.picker.lookup.asp, or (8) iframe.picker.marks.asp in _ifr/.
CVE-2014-4309
Multiple cross-site scripting (XSS) vulnerabilities in Openfiler 2.99 allow remote attackers to inject arbitrary web script or HTML via the (1) TinkerAjax parameter to uptime.html, or remote authenticated users to inject arbitrary web script or HTML via the (2) MaxInstances, (3) PassivePorts, (4) Port, (5) ServerName, (6) TimeoutLogin, (7) TimeoutNoTransfer, or (8) TimeoutStalled parameter to admin/services_ftp.html; the (9) dns1 or (10) dns2 parameter to admin/system.html; the (11) newTgtName parameter to admin/volumes_iscsi_targets.html; the User-Agent HTTP header to (12) language.html, (13) login.html, or (14) password.html in account/; or the User-Agent HTTP header to (15) account_groups.html, (16) account_users.html, (17) services.html, (18) services_ftp.html, (19) services_iscsi_target.html, (20) services_rsync.html, (21) system_clock.html, (22) system_info.html, (23) system_ups.html, (24) volumes_editpartitions.html, or (25) volumes_iscsi_targets.html in admin/.
