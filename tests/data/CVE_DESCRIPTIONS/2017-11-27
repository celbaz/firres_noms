CVE-2015-7267
Samsung 850 Pro and PM851 solid-state drives and Seagate ST500LT015 and ST500LT025 hard disk drives, when in sleep mode and operating in Opal or eDrive mode on Lenovo ThinkPad T440s laptops with BIOS 2.32; ThinkPad W541 laptops with BIOS 2.21; Dell Latitude E6410 laptops with BIOS A16; or Latitude E6430 laptops with BIOS A16, allow physically proximate attackers to bypass self-encrypting drive (SED) protection by leveraging failure to detect when SATA drives are unplugged in Sleep Mode, aka a "Hot Plug attack."
CVE-2015-7268
Samsung 850 Pro and PM851 solid-state drives and Seagate ST500LT015 and ST500LT025 hard disk drives, when used on Windows and operating in Opal mode on Lenovo ThinkPad T440s laptops with BIOS 2.32 or ThinkPad W541 laptops with BIOS 2.21, or in Opal or eDrive mode on Dell Latitude E6410 laptops with BIOS A16 or Latitude E6430 laptops with BIOS A16, allow physically proximate attackers to bypass self-encrypting drive (SED) protection by triggering a soft reset and booting from an alternative OS, aka a "Forced Restart Attack."
CVE-2015-7269
Seagate ST500LT015 hard disk drives, when operating in eDrive mode on Lenovo ThinkPad W541 laptops with BIOS 2.21, allow physically proximate attackers to bypass self-encrypting drive (SED) protection by attaching a second SATA connector to exposed pins, maintaining an alternate power source, and attaching the data cable to another machine, aka a "Hot Unplug Attack."
CVE-2016-6024
IBM Jazz technology based products might divulge information that might be useful in helping attackers through error messages. IBM X-Force ID: 116868.
CVE-2017-0910
In Zulip Server before 1.7.1, on a server with multiple realms, a vulnerability in the invitation system lets an authorized user of one realm on the server create a user account on any other realm.
CVE-2017-1000159
Command injection in evince via filename when printing to PDF. This affects versions earlier than 3.25.91.
CVE-2017-1000207
A vulnerability in Swagger-Parser's version <= 1.0.30 and Swagger codegen version <= 2.2.2 yaml parsing functionality results in arbitrary code being executed when a maliciously crafted yaml Open-API specification is parsed. This in particular, affects the 'generate' and 'validate' command in swagger-codegen (<= 2.2.2) and can lead to arbitrary code being executed when these commands are used on a well-crafted yaml specification.
CVE-2017-1000214
GitPHP by xiphux is vulnerable to OS Command Injections
CVE-2017-1001002
math.js before 3.17.0 had an arbitrary code execution in the JavaScript engine. Creating a typed function with JavaScript code in the name could result arbitrary execution.
CVE-2017-1001003
math.js before 3.17.0 had an issue where private properties such as a constructor could be replaced by using unicode characters when creating an object.
CVE-2017-1001004
typed-function before 0.10.6 had an arbitrary code execution in the JavaScript engine. Creating a typed function with JavaScript code in the name could result arbitrary execution.
CVE-2017-1240
IBM Rhapsody DM products could reveal sensitive information in HTTP 500 Internal Server Error responses. IBM X-Force ID: 124359.
CVE-2017-1251
An undisclosed vulnerability in CLM applications may result in some administrative deployment parameters being shown to an attacker. IBM X-Force ID: 124631.
CVE-2017-1283
IBM WebSphere MQ 8.0 and 9.0 could allow an authenticated user to cause a shared memory leak by MQ applications using dynamic queues, which can lead to lack of resources for other MQ applications. IBM X-Force ID: 125144.
CVE-2017-14176
Bazaar through 2.7.0, when Subprocess SSH is used, allows remote attackers to execute arbitrary commands via a bzr+ssh URL with an initial dash character in the hostname, a related issue to CVE-2017-9800, CVE-2017-12836, CVE-2017-12976, CVE-2017-16228, CVE-2017-1000116, and CVE-2017-1000117.
CVE-2017-14390
In Cloud Foundry Foundation cf-deployment v0.35.0, a misconfiguration with Loggregator and syslog-drain causes logs to be drained to unintended locations.
CVE-2017-14585
A Server Side Request Forgery (SSRF) vulnerability could lead to remote code execution for authenticated administrators. This issue was introduced in version 2.2.0 of Hipchat Server and version 3.0.0 of Hipchat Data Center. Versions of Hipchat Server starting with 2.2.0 and before 2.2.6 are affected by this vulnerability. Versions of Hipchat Data Center starting with 3.0.0 and before 3.1.0 are affected.
CVE-2017-14586
The Hipchat for Mac desktop client is vulnerable to client-side remote code execution via video call link parsing. Hipchat for Mac desktop clients at or above version 4.0 and before version 4.30 are affected by this vulnerability.
CVE-2017-1461
IBM DOORS Next Generation (DNG/RRC) 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128460.
CVE-2017-14746
Use-after-free vulnerability in Samba 4.x before 4.7.3 allows remote attackers to execute arbitrary code via a crafted SMB1 request.
CVE-2017-1484
IBM WebSphere Commerce Enterprise, Professional, Express, and Developer 7.0 and 8.0 could allow an authenticated attacker to obtain information such as user personal data. IBM X-Force ID: 128622.
CVE-2017-15051
Multiple stored cross-site scripting (XSS) vulnerabilities in TeamPass before 2.1.27.9 allow authenticated remote attackers to inject arbitrary web script or HTML via the (1) URL value of an item or (2) user log history. To exploit the vulnerability, the attacker must be first authenticated to the application. For the first one, the attacker has to simply inject XSS code within the URL field of a shared item. For the second one however, the attacker must prepare a payload within its profile, and then ask an administrator to modify its profile. From there, whenever the administrator accesses the log, it can be XSS'ed.
CVE-2017-15052
TeamPass before 2.1.27.9 does not properly enforce manager access control when requesting users.queries.php. It is then possible for a manager user to delete an arbitrary user (including admin), or modify attributes of any arbitrary user except administrator. To exploit the vulnerability, an authenticated attacker must have the manager rights on the application, then tamper with the requests sent directly, for example by changing the "id" parameter when invoking "delete_user" on users.queries.php.
CVE-2017-15053
TeamPass before 2.1.27.9 does not properly enforce manager access control when requesting roles.queries.php. It is then possible for a manager user to modify any arbitrary roles within the application, or delete any arbitrary role. To exploit the vulnerability, an authenticated attacker must have the manager rights on the application, then tamper with the requests sent directly, for example by changing the "id" parameter when invoking "delete_role" on roles.queries.php.
CVE-2017-15054
An arbitrary file upload vulnerability, present in TeamPass before 2.1.27.9, allows remote authenticated users to upload arbitrary files leading to Remote Command Execution. To exploit this vulnerability, an authenticated attacker has to tamper with parameters of a request to upload.files.php, in order to select the correct branch and be able to upload any arbitrary file. From there, it can simply access the file to execute code on the server.
CVE-2017-15055
TeamPass before 2.1.27.9 does not properly enforce item access control when requesting items.queries.php. It is then possible to copy any arbitrary item into a directory controlled by the attacker, edit any item within a read-only directory, delete an arbitrary item, delete the file attachments of an arbitrary item, copy the password of an arbitrary item to the copy/paste buffer, access the history of an arbitrary item, and edit attributes of an arbitrary directory. To exploit the vulnerability, an authenticated attacker must tamper with the requests sent directly, for example by changing the "item_id" parameter when invoking "copy_item" on items.queries.php.
CVE-2017-15100
An attacker submitting facts to the Foreman server containing HTML can cause a stored XSS on certain pages: (1) Facts page, when clicking on the "chart" button and hovering over the chart; (2) Trends page, when checking the graph for a trend based on a such fact; (3) Statistics page, for facts that are aggregated on this page.
CVE-2017-15114
When libvirtd is configured by OSP director (tripleo-heat-templates) to use the TLS transport it defaults to the same certificate authority as all non-libvirtd services. As no additional authentication is configured this allows these services to connect to libvirtd (which is equivalent to root access). If a vulnerability exists in another service it could, combined with this flaw, be exploited to escalate privileges to gain control over compute nodes.
CVE-2017-15117
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2017-15275
Samba before 4.7.3 might allow remote attackers to obtain sensitive information by leveraging failure of the server to clear allocated heap memory.
CVE-2017-1560
IBM DOORS Next Generation (DNG/RRC) 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 131759.
CVE-2017-1570
IBM Jazz Foundation products could allow an authenticated user to obtain sensitive information from stack traces. IBM X-Force ID: 131852.
CVE-2017-1593
IBM DOORS Next Generation (DNG/RRC) 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 132494.
CVE-2017-1607
IBM DOORS Next Generation (DNG/RRC) 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 132927.
CVE-2017-1628
IBM Business Process Manager 8.6.0.0 allows authenticated users to stop and resume the Event Manager by calling a REST API with incorrect authorization checks.
CVE-2017-1650
IBM DOORS Next Generation (DNG/RRC) 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 133260.
CVE-2017-1678
IBM DOORS Next Generation (DNG/RRC) 4.0, 5.0, and 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 134000.
CVE-2017-1688
IBM DOORS Next Generation (DNG/RRC) 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 134063.
CVE-2017-1689
IBM DOORS Next Generation (DNG/RRC) 6.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 134064.
CVE-2017-16955
SQL injection vulnerability in the InLinks plugin through 1.1 for WordPress allows authenticated users to execute arbitrary SQL commands via the "keyword" parameter to /wp-admin/options-general.php?page=inlinks/inlinks.php.
CVE-2017-16956
b3log Symphony (aka Sym) 2.2.0 allows an XSS attack by sending a private letter with a certain /article URI, and a second private letter with a modified title.
CVE-2017-16957
TP-Link TL-WVR, TL-WAR, TL-ER, and TL-R devices allow remote authenticated users to execute arbitrary commands via shell metacharacters in the iface field of an admin/diagnostic command to cgi-bin/luci, related to the zone_get_effect_devices function in /usr/lib/lua/luci/controller/admin/diagnostic.lua in uhttpd.
CVE-2017-16958
TP-Link TL-WVR, TL-WAR, TL-ER, and TL-R devices allow remote authenticated users to execute arbitrary commands via shell metacharacters in the t_bindif field of an admin/bridge command to cgi-bin/luci, related to the get_device_byif function in /usr/lib/lua/luci/controller/admin/bridge.lua in uhttpd.
CVE-2017-16959
The locale feature in cgi-bin/luci on TP-Link TL-WVR, TL-WAR, TL-ER, and TL-R devices allows remote authenticated users to test for the existence of arbitrary files by making an operation=write;locale=%0d request, and then making an operation=read request with a crafted Accept-Language HTTP header, related to the set_sysinfo and get_sysinfo functions in /usr/lib/lua/luci/controller/locale.lua in uhttpd.
CVE-2017-16960
TP-Link TL-WVR, TL-WAR, TL-ER, and TL-R devices allow remote authenticated users to execute arbitrary commands via shell metacharacters in the t_bindif field of an admin/interface command to cgi-bin/luci, related to the get_device_byif function in /usr/lib/lua/luci/controller/admin/interface.lua in uhttpd.
CVE-2017-16961
A SQL injection vulnerability in core/inc/auto-modules.php in BigTree CMS through 4.2.19 allows remote authenticated attackers to obtain information in the context of the user used by the application to retrieve data from the database. The attack uses an admin/trees/add/process request with a crafted _tags[] parameter that is mishandled in a later admin/ajax/dashboard/approve-change request.
CVE-2017-16962
The WebMail components (Crystal, pronto, and pronto4) in CommuniGate Pro before 6.2.1 have stored XSS vulnerabilities via (1) the location or details field of a Google Calendar invitation, (2) a crafted Outlook.com calendar (aka Hotmail Calendar) invitation, (3) e-mail granting access to a directory that has JavaScript in its name, (4) JavaScript in a note name, (5) JavaScript in a task name, or (6) HTML e-mail that is mishandled in the Inbox component.
CVE-2017-16994
The walk_hugetlb_range function in mm/pagewalk.c in the Linux kernel before 4.14.2 mishandles holes in hugetlb ranges, which allows local users to obtain sensitive information from uninitialized kernel memory via crafted use of the mincore() system call.
CVE-2017-4995
An issue was discovered in Pivotal Spring Security 4.2.0.RELEASE through 4.2.2.RELEASE, and Spring Security 5.0.0.M1. When configured to enable default typing, Jackson contained a deserialization vulnerability that could lead to arbitrary code execution. Jackson fixed this vulnerability by blacklisting known "deserialization gadgets." Spring Security configures Jackson with global default typing enabled, which means that (through the previous exploit) arbitrary code could be executed if all of the following is true: (1) Spring Security's Jackson support is being leveraged by invoking SecurityJackson2Modules.getModules(ClassLoader) or SecurityJackson2Modules.enableDefaultTyping(ObjectMapper); (2) Jackson is used to deserialize data that is not trusted (Spring Security does not perform deserialization using Jackson, so this is an explicit choice of the user); and (3) there is an unknown (Jackson is not blacklisting it already) "deserialization gadget" that allows code execution present on the classpath. Jackson provides a blacklisting approach to protecting against this type of attack, but Spring Security should be proactive against blocking unknown "deserialization gadgets" when Spring Security enables default typing.
CVE-2017-8028
In Pivotal Spring-LDAP versions 1.3.0 - 2.3.1, when connected to some LDAP servers, when no additional attributes are bound, and when using LDAP BindAuthenticator with org.springframework.ldap.core.support.DefaultTlsDirContextAuthenticationStrategy as the authentication strategy, and setting userSearch, authentication is allowed with an arbitrary password when the username is correct. This occurs because some LDAP vendors require an explicit operation for the LDAP bind to take effect.
CVE-2017-8031
An issue was discovered in Cloud Foundry Foundation cf-release (all versions prior to v279) and UAA (30.x versions prior to 30.6, 45.x versions prior to 45.4, 52.x versions prior to 52.1). In some cases, the UAA allows an authenticated user for a particular client to revoke client tokens for other users on the same client. This occurs only if the client is using opaque tokens or JWT tokens validated using the check_token endpoint. A malicious actor could cause denial of service.
CVE-2017-8038
In Cloud Foundry Foundation Credhub-release version 1.1.0, access control lists (ACLs) enforce whether an authenticated user can perform an operation on a credential. For installations using ACLs, the ACL was bypassed for the CredHub interpolate endpoint, allowing authenticated applications to view any credential within the CredHub installation.
CVE-2017-8039
An issue was discovered in Pivotal Spring Web Flow through 2.4.5. Applications that do not change the value of the MvcViewFactoryCreator useSpringBinding property which is disabled by default (i.e., set to 'false') can be vulnerable to malicious EL expressions in view states that process form submissions but do not have a sub-element to declare explicit data binding property mappings. NOTE: this issue exists because of an incomplete fix for CVE-2017-4971.
CVE-2017-8044
In Pivotal Single Sign-On for PCF (1.3.x versions prior to 1.3.4 and 1.4.x versions prior to 1.4.3), certain pages allow code to be injected into the DOM environment through query parameters, leading to XSS attacks.
CVE-2017-8045
In Pivotal Spring AMQP versions prior to 1.7.4, 1.6.11, and 1.5.7, an org.springframework.amqp.core.Message may be unsafely deserialized when being converted into a string. A malicious payload could be crafted to exploit this and enable a remote code execution attack.
CVE-2017-9316
Firmware upgrade authentication bypass vulnerability was found in Dahua IPC-HDW4300S and some IP products. The vulnerability was caused by internal Debug function. This particular function was used for problem analysis and performance tuning during product development phase. It allowed the device to receive only specific data (one direction, no transmit) and therefore it was not involved in any instance of collecting user privacy data or allowing remote code execution.
