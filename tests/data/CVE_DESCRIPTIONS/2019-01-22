CVE-2017-6922
In Drupal core 8.x prior to 8.3.4 and Drupal core 7.x prior to 7.56; Private files that have been uploaded by an anonymous user but not permanently attached to content on the site should only be visible to the anonymous user that uploaded them, rather than all anonymous users. Drupal core did not previously provide this protection, allowing an access bypass vulnerability to occur. This issue is mitigated by the fact that in order to be affected, the site must allow anonymous users to upload files into a private file system.
CVE-2017-6923
In Drupal 8.x prior to 8.3.7 When creating a view, you can optionally use Ajax to update the displayed data via filter parameters. The views subsystem/module did not restrict access to the Ajax endpoint to only views configured to use Ajax. This is mitigated if you have access restrictions on the view. It is best practice to always include some form of access restrictions on all views, even if you are using another module to display them.
CVE-2018-13374
A Improper Access Control in Fortinet FortiOS allows attacker to obtain the LDAP server login credentials configured in FortiGate via pointing a LDAP server connectivity test request to a rogue LDAP server instead of the configured one.
CVE-2018-14666
An improper authorization flaw was found in the Smart Class feature of Foreman. An attacker can use it to change configuration of any host registered in Red Hat Satellite, independent of the organization the host belongs to. This flaw affects all Red Hat Satellite 6 versions.
CVE-2018-19011
CX-Supervisor (Versions 3.42 and prior) can execute code that has been injected into a project file. An attacker could exploit this to execute code under the privileges of the application.
CVE-2018-19013
An attacker could inject commands to delete files and/or delete the contents of a file on CX-Supervisor (Versions 3.42 and prior) through a specially crafted project file.
CVE-2018-19017
Several use after free vulnerabilities have been identified in CX-Supervisor (Versions 3.42 and prior). When processing project files, the application fails to check if it is referencing freed memory. An attacker could use a specially crafted project file to exploit and execute code under the privileges of the application.
CVE-2018-19019
A type confusion vulnerability exists when processing project files in CX-Supervisor (Versions 3.42 and prior). An attacker could use a specially crafted project file to exploit and execute code under the privileges of the application.
CVE-2018-19634
CA Service Desk Manager 14.1 and 17 contain a vulnerability that can allow a malicious actor to access survey information.
CVE-2018-19635
CA Service Desk Manager 14.1 and 17 contain a vulnerability that can allow a malicious actor to escalate privileges in the user interface.
CVE-2018-6443
A vulnerability in Brocade Network Advisor Versions before 14.3.1 could allow an unauthenticated, remote attacker to log in to the JBoss Administration interface of an affected system using an undocumented user credentials and install additional JEE applications. A remote unauthenticated user who has access to Network Advisor client libraries and able to decrypt the Jboss credentials could gain access to the Jboss web console.
CVE-2018-6444
A Vulnerability in Brocade Network Advisor versions before 14.1.0 could allow a remote unauthenticated attacker to execute arbitray code. The vulnerability could also be exploited to execute arbitrary OS Commands.
CVE-2018-6445
A Vulnerability in Brocade Network Advisor versions before 14.0.3 could allow a remote unauthenticated attacker to export the current user database which includes the encrypted (not hashed) password of the systems. The attacker could gain access to the Brocade Network Advisor System after extracting/decrypting the passwords.
