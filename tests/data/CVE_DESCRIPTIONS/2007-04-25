CVE-2007-2139
Multiple stack-based buffer overflows in the SUN RPC service in CA (formerly Computer Associates) BrightStor ARCserve Media Server, as used in BrightStor ARCserve Backup 9.01 through 11.5 SP2, BrightStor Enterprise Backup 10.5, Server Protection Suite 2, and Business Protection Suite 2, allow remote attackers to execute arbitrary code via malformed RPC strings, a different vulnerability than CVE-2006-5171, CVE-2006-5172, and CVE-2007-1785.
CVE-2007-2230
SQL injection vulnerability in CA Clever Path Portal allows remote authenticated users to execute limited SQL commands and retrieve arbitrary database contents via (1) the ofinterest parameter in a light search query, (2) description parameter in the advanced search query, and possibly other vectors.
CVE-2007-2231
Directory traversal vulnerability in index/mbox/mbox-storage.c in Dovecot before 1.0.rc29, when using the zlib plugin, allows remote attackers to read arbitrary gzipped (.gz) mailboxes (mbox files) via a .. (dot dot) sequence in the mailbox name.
CVE-2007-2232
The CHECK command in Cosign 2.0.1 and earlier allows remote attackers to bypass authentication requirements via CR (\r) sequences in the cosign cookie parameter.
CVE-2007-2233
cosign-bin/cosign.cgi in Cosign 2.0.2 and earlier allows remote authenticated users to perform unauthorized actions as an arbitrary user by using CR (\r) sequences in the service parameter to inject LOGIN and REGISTER commands with the desired username.
CVE-2007-2234
include/common.php in PunBB 1.2.14 and earlier does not properly handle a disabled ini_get function when checking the register_globals setting, which allows remote attackers to register global parameters, as demonstrated by an SQL injection attack on the search_id parameter to search.php.
CVE-2007-2235
Multiple cross-site scripting (XSS) vulnerabilities in PunBB 1.2.14 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) Referer HTTP header to misc.php or the (2) category name when deleting a category in admin_categories.php.
CVE-2007-2236
footer.php in PunBB 1.2.14 and earlier allows remote attackers to include local files in include/user/ via a cross-site scripting (XSS) attack, or via the pun_include tag, as demonstrated by use of admin_options.php to execute PHP code from an uploaded avatar file.
CVE-2007-2242
The IPv6 protocol allows remote attackers to cause a denial of service via crafted IPv6 type 0 route headers (IPV6_RTHDR_TYPE_0) that create network amplification between two routers.
CVE-2007-2243
OpenSSH 4.6 and earlier, when ChallengeResponseAuthentication is enabled, allows remote attackers to determine the existence of user accounts by attempting to authenticate via S/KEY, which displays a different response if the user account exists, a similar issue to CVE-2001-1483.
CVE-2007-2244
Multiple buffer overflows in Adobe Photoshop CS2 and CS3, Illustrator CS3, and GoLive 9 allow user-assisted remote attackers to execute arbitrary code via a crafted (1) BMP, (2) DIB, or (3) RLE file.
CVE-2007-2245
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin before 2.10.1.0 allow remote attackers to inject arbitrary web script or HTML via (1) the fieldkey parameter to browse_foreigners.php or (2) certain input to the PMA_sanitize function.
CVE-2007-2246
Unspecified vulnerability in HP-UX B.11.00 and B.11.11, when running sendmail 8.9.3 or 8.11.1; and HP-UX B.11.23 when running sendmail 8.11.1; allows remote attackers to cause a denial of service via unknown attack vectors.  NOTE: due to the lack of details from HP, it is not known whether this issue is a duplicate of another CVE such as CVE-2006-1173 or CVE-2006-4434.
CVE-2007-2247
SQL injection vulnerability in modules/news/article.php in phpMySpace Gold 8.10 allows remote attackers to execute arbitrary SQL commands via the item_id parameter.
CVE-2007-2248
Multiple cross-site scripting (XSS) vulnerabilities in admin.php in Phorum before 5.1.22 allow remote attackers to inject arbitrary web script or HTML via the (1) group_id parameter in the groups module or (2) the smiley_id parameter in the smileys modsettings module.
This vulnerability is addressed in the following product release:
Phorum, Phorum, 5.1.22
CVE-2007-2249
include/controlcenter/users.php in Phorum before 5.1.22 allows remote authenticated moderators to gain privileges via a modified (1) user_ids POST parameter or (2) userdata array.
CVE-2007-2250
admin.php in Phorum before 5.1.22 allows remote attackers to obtain the full path via the module[] parameter.
CVE-2007-2251
Unspecified vulnerability in the Roles module in Xaraya 1.1.2 and earlier allows attackers to gain privileges via unspecified vectors, probably related to incorrect permission checking in xartemplates/user-view.xd.
CVE-2007-2252
Directory traversal vulnerability in iconspopup.php in Exponent CMS 0.96.6 Alpha and earlier allows remote attackers to obtain sensitive information via a .. (dot dot) in the icodir parameter.
CVE-2007-2253
Exponent CMS 0.96.6 Alpha and earlier allows remote attackers to obtain path information via a direct request for (1) sdk/blanks/formcontrol.php and (2) sdk/blanks/file_modules.php.
CVE-2007-2254
PHP remote file inclusion vulnerability in admin/setup/level2.php in PHP Classifieds 6.04, and probably earlier versions, allows remote attackers to execute arbitrary PHP code via a URL in the dir parameter.  NOTE: this product was referred to as "Allfaclassfieds" in the original disclosure.
CVE-2007-2255
Multiple PHP remote file inclusion vulnerabilities in Download-Engine 1.4.3 allow remote attackers to execute arbitrary PHP code via a URL in the (1) eng_dir parameter to addmember.php, (2) lang_path parameter to admin/enginelib/class.phpmailer.php, and the (3) spaw_root parameter to admin/includes/spaw/dialogs/colorpicker.php, different vectors than CVE-2006-5291 and CVE-2006-5459.  NOTE: vector 3 might be an issue in SPAW.
CVE-2007-2256
Cross-site scripting (XSS) vulnerability in you.php in TJSChat 0.95 allows remote attackers to inject arbitrary web script or HTML via the user parameter.
CVE-2007-2257
PHP remote file inclusion vulnerability in subscp.php in Fully Modded phpBB2 allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-2258
PHP remote file inclusion vulnerability in includes/init.inc.php in PHPMyBibli allows remote attackers to execute arbitrary PHP code via a URL in the base_path parameter.
CVE-2007-2259
SQL injection vulnerability in forum.php in EsForum 3.0 allows remote attackers to execute arbitrary SQL commands via the idsalon parameter.
CVE-2007-2260
Multiple PHP remote file inclusion vulnerabilities in bibtex mase beta 2.0 allow remote attackers to execute arbitrary PHP code via a URL in the bibtexrootrel parameter to (1) unavailable.php, (2) source.php, (3) log.php, (4) latex.php, (5) indexinfo.php, (6) index.php, (7) importinfo.php, (8) import.php, (9) examplefile.php, (10) clearinfo.php, (11) clear.php, (12) aboutinfo.php, (13) about.php, and other unspecified files.
CVE-2007-2261
PHP remote file inclusion vulnerability in espaces/communiques/annotations.php in C-Arbre 0.6PR7 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the root_path parameter, a different vector than CVE-2007-1721.
CVE-2007-2262
Multiple PHP remote file inclusion vulnerabilities in html/php/detail.php in Sinato jmuffin allow remote attackers to execute arbitrary PHP code via a URL in the (1) relPath and (2) folder parameters.  NOTE: this product was originally reported as "File117".
CVE-2007-2265
Cross-site scripting (XSS) vulnerability in YA Book 0.98-alpha allows remote attackers to inject arbitrary web script or HTML via the City field in a sign action in index.php.
CVE-2007-2266
Progress Webspeed Messenger allows remote attackers to read, create, modify, and execute arbitrary files by invoking webutil/_cpyfile.p in the WService parameter to (1) cgiip.exe or (2) wsisa.dll in scripts/, as demonstrated by using the save,editor options to create a new file using the fileName parameter.
CVE-2007-2267
Unspecified vulnerability in Sun Cluster 3.1 and Solaris Cluster 3.2 before 20070424 allows remote authenticated users, operating from a different cluster node, to cause a denial of service (data corruption or send_mondo panic) via unspecified vectors, as demonstrated by EMC Symcli backup software 6.2.1.
CVE-2007-2268
Multiple directory traversal vulnerabilities in SWsoft Plesk for Windows 7.6.1, 8.1.0, and 8.1.1 allow remote attackers to read arbitrary files via a .. (dot dot) in the locale_id parameter to (1) login.php3 or (2) login_up.php3.
CVE-2007-2269
Directory traversal vulnerability in top.php3 in SWsoft Plesk for Windows 8.1 and 8.1.1 allows remote attackers to read arbitrary files via a .. (dot dot) in the locale_id parameter.
CVE-2007-2270
The Linksys SPA941 VoIP Phone allows remote attackers to cause a denial of service (device reboot) via a 0377 (0xff) character in the From header, and possibly certain other locations, in a SIP INVITE request.
CVE-2007-2271
Directory traversal vulnerability in Rajneel Lal TotaRam USP FOSS Distribution 1.01 allows remote attackers to read arbitrary files via a .. (dot dot) in the dnld parameter.
CVE-2007-2272
PHP remote file inclusion vulnerability in docs/front-end-demo/cart2.php in Advanced Webhost Billing System (AWBS) 2.4.0 allows remote attackers to execute arbitrary PHP code via a URL in the workdir parameter.
CVE-2007-2273
PHP remote file inclusion vulnerability in include/loading.php in Alessandro Lulli wavewoo 0.1.1 allows remote attackers to execute arbitrary PHP code via a URL in the path_include parameter.
CVE-2007-2274
The BitTorrent implementation in Opera 9.2 allows remote attackers to cause a denial of service (CPU consumption and application crash) via a malformed torrent file.  NOTE: the original disclosure refers to this as a memory leak, but it is not certain.
CVE-2007-2275
Unspecified vulnerability in HP StorageWorks Command View Advanced Edition for XP before 5.6.0-01, XP Replication Monitor before 5.6.0-01, and XP Tiered Storage Manager before 5.5.0-02 allows local users to access other accounts via unspecified vectors during registration or addition of new users.
CVE-2007-2276
** DISPUTED **  3Com TippingPoint IPS allows remote attackers to cause a denial of service (device hang) via a flood of packets on TCP port 80 with sequentially increasing source ports, related to a "badly written loop."  NOTE: the vendor disputes this issue, stating that the product has "performed as expected with no DoS emerging."
CVE-2007-2277
Session fixation vulnerability in Plogger allows remote attackers to hijack web sessions by setting the PHPSESSID parameter.
CVE-2007-2278
Multiple PHP remote file inclusion vulnerabilities in DCP-Portal 6.1.1 allow remote attackers to execute arbitrary PHP code via a URL in (1) the path parameter to library/adodb/adodb.inc.php, (2) the abs_path_editor parameter to library/editor/editor.php, or (3) the cfgfile_to_load parameter to admin/phpMyAdmin/libraries/common.lib.php.
