CVE-2016-10717
A vulnerability in the encryption and permission implementation of Malwarebytes Anti-Malware consumer version 2.2.1 and prior (fixed in 3.0.4) allows an attacker to take control of the whitelisting feature (exclusions.dat under %SYSTEMDRIVE%\ProgramData) to permit execution of unauthorized applications including malware and malicious websites. Files blacklisted by Malwarebytes Malware Protect can be executed, and domains blacklisted by Malwarebytes Web Protect can be reached through HTTP.
CVE-2017-0914
Gitlab Community and Enterprise Editions version 10.1, 10.2, and 10.2.4 are vulnerable to a SQL injection in the MilestoneFinder component resulting in disclosure of all data in a GitLab instance's database.
CVE-2017-0915
Gitlab Community Edition version 10.2.4 is vulnerable to a lack of input validation in the GitlabProjectsImportService resulting in remote code execution.
CVE-2017-0916
Gitlab Community Edition version 10.3 is vulnerable to a lack of input validation in the system_hook_push queue through web hook component resulting in remote code execution.
CVE-2017-0917
Gitlab Community Edition version 10.2.4 is vulnerable to lack of input validation in the CI job component resulting in persistent cross site scripting.
CVE-2017-0918
Gitlab Community Edition version 10.3 is vulnerable to a path traversal issue in the GitLab CI runner component resulting in remote code execution.
CVE-2017-0922
Gitlab Enterprise Edition version 10.3 is vulnerable to an authorization bypass issue in the GitLab Projects::BoardsController component resulting in an information disclosure on any board object.
CVE-2017-0923
Gitlab Community Edition version 9.1 is vulnerable to lack of input validation in the IPython notebooks component resulting in persistent cross site scripting.
CVE-2017-0924
Gitlab Community Edition version 10.2.4 is vulnerable to lack of input validation in the labels component resulting in persistent cross site scripting.
CVE-2017-0925
Gitlab Enterprise Edition version 10.1.0 is vulnerable to an insufficiently protected credential issue in the project service integration API endpoint resulting in an information disclosure of plaintext password.
CVE-2017-0926
Gitlab Community Edition version 10.3 is vulnerable to an improper authorization issue in the Oauth sign-in component resulting in unauthorized user login.
CVE-2017-0927
Gitlab Community Edition version 10.3 is vulnerable to an improper authorization issue in the deployment keys component resulting in unauthorized use of deployment keys by guest users.
CVE-2017-18241
fs/f2fs/segment.c in the Linux kernel before 4.13 allows local users to cause a denial of service (NULL pointer dereference and panic) by using a noflush_merge option that triggers a NULL value for a flush_cmd_control data structure.
CVE-2018-1229
Pivotal Spring Batch Admin, all versions, contains a stored XSS vulnerability in the file upload feature. An unauthenticated malicious user with network access to Spring Batch Admin could store an arbitrary web script that would be executed by other users. This issue has not been patched because Spring Batch Admin has reached end of life.
CVE-2018-1230
Pivotal Spring Batch Admin, all versions, does not contain cross site request forgery protection. A remote unauthenticated user could craft a malicious site that executes requests to Spring Batch Admin. This issue has not been patched because Spring Batch Admin has reached end of life.
CVE-2018-1344
Addresses potential communication downgrade attack in NetIQ iManager versions prior to 3.1
CVE-2018-1345
NetIQ iManager, versions prior to 3.1, under some circumstances could be susceptible to an elevation of privilege attack.
CVE-2018-1346
Addresses denial of service attack to eDirectory versions prior to 9.1.
CVE-2018-1347
The administrative web interface in NetIQ iManager, versions prior to 3.1, are vulnerable to reflected cross site scripting.
CVE-2018-3710
Gitlab Community and Enterprise Editions version 10.3.3 is vulnerable to an Insecure Temporary File in the project import component resulting remote code execution.
CVE-2018-7269
The findByCondition function in framework/db/ActiveRecord.php in Yii 2.x before 2.0.15 allows remote attackers to conduct SQL injection attacks via a findOne() or findAll() call, unless a developer recognizes an undocumented need to sanitize array input.
CVE-2018-7513
In Omron CX-Supervisor Versions 3.30 and prior, parsing malformed project files may cause a stack-based buffer overflow.
CVE-2018-7515
In Omron CX-Supervisor Versions 3.30 and prior, access of uninitialized pointer vulnerabilities can be exploited when CX Supervisor indirectly calls an initialized pointer when parsing malformed packets.
CVE-2018-7517
In Omron CX-Supervisor Versions 3.30 and prior, parsing malformed project files may cause an out of bounds vulnerability.
CVE-2018-7519
In Omron CX-Supervisor Versions 3.30 and prior, parsing malformed project files may cause a heap-based buffer overflow.
CVE-2018-7521
In Omron CX-Supervisor Versions 3.30 and prior, use after free vulnerabilities can be exploited when CX Supervisor parses a specially crafted project file.
CVE-2018-7523
In Omron CX-Supervisor Versions 3.30 and prior, parsing malformed project files may cause a double free vulnerability.
CVE-2018-7525
In Omron CX-Supervisor Versions 3.30 and prior, processing a malformed packet by a certain executable may cause an untrusted pointer dereference vulnerability.
CVE-2018-8073
Yii 2.x before 2.0.15 allows remote attackers to execute arbitrary LUA code via a variant of the CVE-2018-7269 attack in conjunction with the Redis extension.
CVE-2018-8074
Yii 2.x before 2.0.15 allows remote attackers to inject unintended search conditions via a variant of the CVE-2018-7269 attack in conjunction with the Elasticsearch extension.
