CVE-2014-3573
The oVirt Engine backend module, as used in Red Hat Enterprise Virtualization Manager before 3.4.2, uses an "insecure DocumentBuilderFactory," which allows remote attackers to read arbitrary files or possibly have other unspecified impact via a crafted XML/RSDL document, related to an XML External Entity (XXE) issue.
CVE-2014-4351
Buffer overflow in QuickTime in Apple OS X before 10.10 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via crafted audio samples in an m4a file.
CVE-2014-4391
The Code Signing feature in Apple OS X before 10.10 does not properly handle incomplete resource envelopes in signed bundles, which allows remote attackers to bypass intended app-author restrictions by omitting an execution-related resource.
CVE-2014-4417
Safari in Apple OS X before 10.10 allows remote attackers to cause a denial of service (universal Push Notification outage) via a web site that triggers an uncaught SafariNotificationAgent exception by providing a crafted Push Notification.
CVE-2014-4425
CFPreferences in Apple OS X before 10.10 does not properly enforce the "require password after sleep or screen saver begins" setting, which makes it easier for physically proximate attackers to obtain access by leveraging an unattended workstation.
CVE-2014-4426
AFP File Server in Apple OS X before 10.10 allows remote attackers to discover the network addresses of all interfaces via an unspecified command to one interface.
CVE-2014-4427
App Sandbox in Apple OS X before 10.10 allows attackers to bypass a sandbox protection mechanism via the accessibility API.
CVE-2014-4428
Bluetooth in Apple OS X before 10.10 does not require encryption for HID Low Energy devices, which allows remote attackers to spoof a device by leveraging previous pairing.
CVE-2014-4430
CoreStorage in Apple OS X before 10.10 retains a volume's encryption keys upon an eject action in the unlocked state, which makes it easier for physically proximate attackers to obtain cleartext data via a remount.
CVE-2014-4431
Dock in Apple OS X before 10.10 does not properly manage the screen-lock state, which allows physically proximate attackers to view windows by leveraging an unattended workstation.
CVE-2014-4432
fdesetup in Apple OS X before 10.10 does not properly display the encryption status in between a setting-update action and a reboot action, which might make it easier for physically proximate attackers to obtain cleartext data by leveraging ignorance of the reboot requirement.
CVE-2014-4433
Heap-based buffer overflow in the kernel in Apple OS X before 10.10 allows physically proximate attackers to execute arbitrary code via crafted resource forks in an HFS filesystem.
CVE-2014-4434
The kernel in Apple OS X before 10.10 allows physically proximate attackers to cause a denial of service (NULL pointer dereference and system crash) via a crafted filename on an HFS filesystem.
CVE-2014-4435
The "iCloud Find My Mac" feature in Apple OS X before 10.10 does not properly enforce rate limiting of lost-mode PIN entry, which makes it easier for physically proximate attackers to obtain access via a brute-force attack involving a series of reboots.
CVE-2014-4436
IOHIDFamily in Apple OS X before 10.10 allows attackers to cause denial of service (out-of-bounds read operation) via a crafted application.
CVE-2014-4437
LaunchServices in Apple OS X before 10.10 allows attackers to bypass intended sandbox restrictions via an application that specifies a crafted handler for the Content-Type field of an object.
CVE-2014-4438
Race condition in LoginWindow in Apple OS X before 10.10 allows physically proximate attackers to obtain access by leveraging an unattended workstation on which screen locking had been attempted.
CVE-2014-4439
Mail in Apple OS X before 10.10 does not properly recognize the removal of a recipient address from a message, which makes it easier for remote attackers to obtain sensitive information in opportunistic circumstances by reading a message intended exclusively for other recipients.
CVE-2014-4440
The MCX Desktop Config Profiles implementation in Apple OS X before 10.10 retains web-proxy settings from uninstalled mobile-configuration profiles, which allows remote attackers to obtain sensitive information in opportunistic circumstances by leveraging access to an unintended proxy server.
CVE-2014-4441
NetFS Client Framework in Apple OS X before 10.10 does not ensure that the disabling of File Sharing is always possible, which allows remote attackers to read or write to files by leveraging a state in which File Sharing is permanently enabled.
CVE-2014-4442
The kernel in Apple OS X before 10.10 allows local users to cause a denial of service (panic) via a message to a system control socket.
CVE-2014-4443
Apple OS X before 10.10 allows remote attackers to cause a denial of service (NULL pointer dereference) via crafted ASN.1 data.
CVE-2014-4444
SecurityAgent in Apple OS X before 10.10 does not ensure that a Kerberos ticket is in the cache for the correct user, which allows local users to gain privileges in opportunistic circumstances by leveraging a Fast User Switching login.
CVE-2014-4446
Mail Service in Apple OS X Server before 4.0 does not enforce SACL changes until after a service restart, which allows remote authenticated users to bypass intended access restrictions in opportunistic circumstances by leveraging a change made by an administrator.
CVE-2014-4447
Profile Manager in Apple OS X Server before 4.0 allows local users to discover cleartext passwords by reading a file after a (1) profile setup or (2) profile edit occurs.
