CVE-2012-5855
The SHAddToRecentDocs function in VideoLAN VLC media player 2.0.4 and earlier might allow user-assisted attackers to cause a denial of service (crash) via a crafted file name that triggers an incorrect string-length calculation when the file is added to VLC.  NOTE: it is not clear whether this issue crosses privilege boundaries or whether it can be exploited without user interaction.
CVE-2013-1132
Multiple cross-site scripting (XSS) vulnerabilities in Cisco Unified Communications Domain Manager allow remote attackers to inject arbitrary web script or HTML via vectors involving the (1) IptAccountMgmt, (2) IptFeatureConfigTemplateMgmt, (3) IptFeatureDisplayPolicyMgmt, or (4) IptProviderMgmt page, aka Bug IDs CSCud69972, CSCud70193, and CSCud70261.
CVE-2013-1300
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Win32k Memory Allocation Vulnerability."
CVE-2013-1340
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Win32k Dereference Vulnerability."
CVE-2013-1345
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Win32k Vulnerability."
CVE-2013-1868
Multiple buffer overflows in VideoLAN VLC media player 2.0.4 and earlier allow remote attackers to cause a denial of service (crash) and execute arbitrary code via vectors related to the (1) freetype renderer and (2) HTML subtitle parser.
CVE-2013-1896
mod_dav.c in the Apache HTTP Server before 2.2.25 does not properly determine whether DAV is enabled for a URI, which allows remote attackers to cause a denial of service (segmentation fault) via a MERGE request in which the URI is configured for handling by the mod_dav_svn module, but a certain href attribute in XML data refers to a non-DAV URI.
CVE-2013-1954
The ASF Demuxer (modules/demux/asf/asf.c) in VideoLAN VLC media player 2.0.5 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted ASF movie that triggers an out-of-bounds read.
CVE-2013-1965
Apache Struts Showcase App 2.0.0 through 2.3.13, as used in Struts 2 before 2.3.14.1, allows remote attackers to execute arbitrary OGNL code via a crafted parameter name that is not properly handled when invoking a redirect.
CVE-2013-1966
Apache Struts 2 before 2.3.14.1 allows remote attackers to execute arbitrary OGNL code via a crafted request that is not properly handled when using the includeParams attribute in the (1) URL or (2) A tag.
CVE-2013-2115
Apache Struts 2 before 2.3.14.2 allows remote attackers to execute arbitrary OGNL code via a crafted request that is not properly handled when using the includeParams attribute in the (1) URL or (2) A tag. NOTE: this issue is due to an incomplete fix for CVE-2013-1966.
CVE-2013-2352
LeftHand OS (aka SAN iQ) 10.5 and earlier on HP StoreVirtual Storage devices does not provide a mechanism for disabling the HP Support challenge-response root-login feature, which makes it easier for remote attackers to obtain administrative access by leveraging knowledge of an unused one-time password.
CVE-2013-2784
Triangle Research International (aka Tri) Nano-10 PLC devices with firmware before r81 use an incorrect algorithm for bounds checking of data in Modbus/TCP packets, which allows remote attackers to cause a denial of service (networking outage) via a crafted packet to TCP port 502.
CVE-2013-2786
Alstom Grid MiCOM S1 Agile before 1.0.3 and Alstom Grid MiCOM S1 Studio use weak permissions for the MiCOM S1 %PROGRAMFILES% directory, which allows local users to gain privileges via a Trojan horse executable file.
CVE-2013-2853
The HTTPS implementation in Google Chrome before 28.0.1500.71 does not ensure that headers are terminated by \r\n\r\n (carriage return, newline, carriage return, newline), which allows man-in-the-middle attackers to have an unspecified impact via vectors that trigger header truncation.
CVE-2013-2867
Google Chrome before 28.0.1500.71 does not properly prevent pop-under windows, which allows remote attackers to have an unspecified impact via a crafted web site.
CVE-2013-2868
common/extensions/sync_helper.cc in Google Chrome before 28.0.1500.71 proceeds with sync operations for NPAPI extensions without checking for a certain plugin permission setting, which might allow remote attackers to trigger unwanted extension changes via unspecified vectors.
CVE-2013-2869
Google Chrome before 28.0.1500.71 allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted JPEG2000 image.
CVE-2013-2870
Use-after-free vulnerability in Google Chrome before 28.0.1500.71 allows remote servers to execute arbitrary code via crafted response traffic after a URL request.
CVE-2013-2871
Use-after-free vulnerability in Google Chrome before 28.0.1500.71 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the handling of input.
CVE-2013-2872
Google Chrome before 28.0.1500.71 on Mac OS X does not ensure a sufficient source of entropy for renderer processes, which might make it easier for remote attackers to defeat cryptographic protection mechanisms in third-party components via unspecified vectors.
CVE-2013-2873
Use-after-free vulnerability in Google Chrome before 28.0.1500.71 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors involving a 404 HTTP status code during the loading of resources.
CVE-2013-2874
Google Chrome before 28.0.1500.71 on Windows, when an Nvidia GPU is used, allows remote attackers to bypass intended restrictions on access to screen data via vectors involving IPC transmission of GL textures.
CVE-2013-2875
core/rendering/svg/SVGInlineTextBox.cpp in the SVG implementation in Blink, as used in Google Chrome before 28.0.1500.71, allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2013-2876
browser/extensions/api/tabs/tabs_api.cc in Google Chrome before 28.0.1500.71 does not properly enforce restrictions on the capture of screenshots by extensions, which allows remote attackers to obtain sensitive information about the content of a previous page via vectors involving an interstitial page.
CVE-2013-2877
parser.c in libxml2 before 2.9.0, as used in Google Chrome before 28.0.1500.71 and other products, allows remote attackers to cause a denial of service (out-of-bounds read) via a document that ends abruptly, related to the lack of certain checks for the XML_PARSER_EOF state.
CVE-2013-2878
Google Chrome before 28.0.1500.71 allows remote attackers to cause a denial of service (out-of-bounds read) via vectors related to the handling of text.
CVE-2013-2879
Google Chrome before 28.0.1500.71 does not properly determine the circumstances in which a renderer process can be considered a trusted process for sign-in and subsequent sync operations, which makes it easier for remote attackers to conduct phishing attacks via a crafted web site.
CVE-2013-2880
Multiple unspecified vulnerabilities in Google Chrome before 28.0.1500.71 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2013-3115
Microsoft Internet Explorer 7 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3162.
CVE-2013-3127
The Microsoft WMV video codec in wmv9vcm.dll, wmvdmod.dll in Windows Media Format Runtime 9 and 9.5, and wmvdecod.dll in Windows Media Format Runtime 11 and Windows Media Player 11 and 12 allows remote attackers to execute arbitrary code via a crafted media file, aka "WMV Video Decoder Remote Code Execution Vulnerability."
CVE-2013-3129
Microsoft .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, and 4.5; Silverlight 5 before 5.1.20513.0; win32k.sys in the kernel-mode drivers, and GDI+, DirectWrite, and Journal, in Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT; GDI+ in Office 2003 SP3, 2007 SP3, and 2010 SP1; GDI+ in Visual Studio .NET 2003 SP1; and GDI+ in Lync 2010, 2010 Attendee, 2013, and Basic 2013 allow remote attackers to execute arbitrary code via a crafted TrueType Font (TTF) file, aka "TrueType Font Parsing Vulnerability."
CVE-2013-3131
Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, and 4.5, and Silverlight 5 before 5.1.20513.0, does not properly prevent changes to data in multidimensional arrays of structures, which allows remote attackers to execute arbitrary code via (1) a crafted .NET Framework application or (2) a crafted Silverlight application, aka "Array Access Violation Vulnerability."
CVE-2013-3132
Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly check the permissions of objects that use reflection, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application, aka "Delegate Reflection Bypass Vulnerability."
CVE-2013-3133
Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly check the permissions of objects that use reflection, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application, aka "Anonymous Method Injection Vulnerability."
CVE-2013-3134
The Common Language Runtime (CLR) in Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 on 64-bit platforms does not properly allocate arrays of structures, which allows remote attackers to execute arbitrary code via a crafted .NET Framework application that changes array data, aka "Array Allocation Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms13-052#section6

'Systems running 32-bit versions of Windows are not affected by this vulnerability.'
CVE-2013-3143
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3161.
CVE-2013-3144
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3151 and CVE-2013-3163.
CVE-2013-3145
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3150.
CVE-2013-3146
Microsoft Internet Explorer 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3152.
CVE-2013-3147
Microsoft Internet Explorer 6 through 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3148
Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3153.
CVE-2013-3149
Microsoft Internet Explorer 7 and 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3150
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3145.
CVE-2013-3151
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3144 and CVE-2013-3163.
CVE-2013-3152
Microsoft Internet Explorer 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3146.
CVE-2013-3153
Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3148.
CVE-2013-3154
The signature-update functionality in Windows Defender on Microsoft Windows 7 and Windows Server 2008 R2 relies on an incorrect pathname, which allows local users to gain privileges via a Trojan horse application in the %SYSTEMDRIVE% top-level directory, aka "Microsoft Windows 7 Defender Improper Pathname Vulnerability."
CVE-2013-3161
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3143.
CVE-2013-3162
Microsoft Internet Explorer 7 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3115.
CVE-2013-3163
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2013-3144 and CVE-2013-3151.
CVE-2013-3164
Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2013-3166
Cross-site scripting (XSS) vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to inject arbitrary web script or HTML via vectors involving incorrect auto-selection of the Shift JIS encoding, leading to cross-domain scrolling events, aka "Shift JIS Character Encoding Vulnerability," a different vulnerability than CVE-2013-0015.
CVE-2013-3167
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, and Windows 7 SP1 does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Win32k Information Disclosure Vulnerability."
CVE-2013-3171
The serialization functionality in Microsoft .NET Framework 2.0 SP2, 3.5, 3.5 SP1, 3.5.1, 4, and 4.5 does not properly check the permissions of delegate objects, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (XBAP) or (2) a crafted .NET Framework application that leverages a partial-trust relationship, aka "Delegate Serialization Vulnerability."
CVE-2013-3172
Buffer overflow in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, and Windows 7 SP1 allows local users to cause a denial of service (system hang) via a crafted application that leverages improper handling of objects in memory, aka "Win32k Buffer Overflow Vulnerability."
CVE-2013-3173
Buffer overflow in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT allows local users to gain privileges via a crafted application that leverages improper handling of objects in memory, aka "Win32k Buffer Overwrite Vulnerability."
CVE-2013-3174
DirectShow in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, and Windows Server 2012 allows remote attackers to execute arbitrary code via a crafted GIF file, aka "DirectShow Arbitrary Memory Overwrite Vulnerability."
CVE-2013-3178
Microsoft Silverlight 5 before 5.1.20513.0 does not properly initialize arrays, which allows remote attackers to execute arbitrary code or cause a denial of service (NULL pointer dereference) via a crafted Silverlight application, aka "Null Pointer Vulnerability."
CVE-2013-3245
** DISPUTED ** plugins/demux/libmkv_plugin.dll in VideoLAN VLC Media Player 2.0.7, and possibly other versions, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted MKV file, possibly involving an integer overflow and out-of-bounds read or heap-based buffer overflow, or an uncaught exception.  NOTE: the vendor disputes the severity and claimed vulnerability type of this issue, stating "This PoC crashes VLC, indeed, but does nothing more... this is not an integer overflow error, but an uncaught exception and I doubt that it is exploitable. This uncaught exception makes VLC abort, not execute random code, on my Linux 64bits machine." A PoC posted by the original researcher shows signs of an attacker-controlled out-of-bounds read, but the affected instruction does not involve a register that directly influences control flow.
CVE-2013-3344
Heap-based buffer overflow in Adobe Flash Player before 11.7.700.232 and 11.8.x before 11.8.800.94 on Windows and Mac OS X, before 11.2.202.297 on Linux, before 11.1.111.64 on Android 2.x and 3.x, and before 11.1.115.69 on Android 4.x allows attackers to execute arbitrary code via unspecified vectors.
CVE-2013-3345
Adobe Flash Player before 11.7.700.232 and 11.8.x before 11.8.800.94 on Windows and Mac OS X, before 11.2.202.297 on Linux, before 11.1.111.64 on Android 2.x and 3.x, and before 11.1.115.69 on Android 4.x allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2013-3347
Integer overflow in Adobe Flash Player before 11.7.700.232 and 11.8.x before 11.8.800.94 on Windows and Mac OS X, before 11.2.202.297 on Linux, before 11.1.111.64 on Android 2.x and 3.x, and before 11.1.115.69 on Android 4.x allows attackers to execute arbitrary code via PCM data that is not properly handled during resampling.
CVE-2013-3348
Adobe Shockwave Player before 12.0.3.133 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2013-3349
Unspecified vulnerability in Adobe ColdFusion 9.0 through 9.0.2, when the JRun application server is used, allows remote attackers to cause a denial of service via unknown vectors.
CVE-2013-3350
Adobe ColdFusion 10 before Update 11 allows remote attackers to call ColdFusion Components (CFC) public methods via WebSockets.
CVE-2013-3400
The license-installation module in Cisco NX-OS on Nexus 1000V devices allows local users to execute arbitrary commands via crafted "install license" arguments, aka Bug ID CSCuh30824.
CVE-2013-3405
The web portal in TC software on Cisco TelePresence endpoints does not require an exact password match during a login attempt by a user who has not configured a password, which allows remote attackers to bypass authentication by sending an arbitrary password, aka Bug ID CSCud96071.
CVE-2013-3408
The firmware on Cisco Virtualization Experience Client 6000 devices sets incorrect operating-system permissions, which allows local users to gain privileges via an unspecified sequence of commands, aka Bug ID CSCuc31764.
CVE-2013-3416
Cross-site scripting (XSS) vulnerability in the web framework in the unified-communications management implementation in Cisco Unified Operations Manager and Unified Service Monitor allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter, aka Bug IDs CSCuh47574 and CSCuh95997.
CVE-2013-3579
The Lookout Mobile Security application before 8.17-8a39d3f for Android allows attackers to cause a denial of service (application crash) via a crafted application that sends an intent to com.lookout.security.ScanTell with zero arguments.
