CVE-2008-4563
Heap-based buffer overflow in adsmdll.dll 5.3.7.7296, as used by the daemon (dsmsvc.exe) in the backup server in IBM Tivoli Storage Manager (TSM) Express 5.3.7.3 and earlier and TSM 5.2, 5.3 before 5.3.6.0, and 5.4.0.0 through 5.4.4.0, allows remote attackers to execute arbitrary code via a crafted length value.
CVE-2009-0093
Windows DNS Server in Microsoft Windows 2000 SP4, Server 2003 SP1 and SP2, and Server 2008, when dynamic updates are enabled, does not restrict registration of the "wpad" hostname, which allows remote authenticated users to hijack the Web Proxy Auto-Discovery (WPAD) feature, and conduct man-in-the-middle attacks by spoofing a proxy server, via a Dynamic Update request for this hostname, aka "DNS Server Vulnerability in WPAD Registration Vulnerability," a related issue to CVE-2007-1692.
CVE-2009-0094
The WINS server in Microsoft Windows 2000 SP4 and Server 2003 SP1 and SP2 does not restrict registration of the (1) "wpad" and (2) "isatap" NetBIOS names, which allows remote authenticated users to hijack the Web Proxy Auto-Discovery (WPAD) and Intra-Site Automatic Tunnel Addressing Protocol (ISATAP) features, and conduct man-in-the-middle attacks by spoofing a proxy server or ISATAP route, by registering one of these names in the WINS database, aka "WPAD WINS Server Registration Vulnerability," a related issue to CVE-2007-1692.
Per: http://www.microsoft.com/technet/security/Bulletin/MS09-008.mspx

Mitigating Factors for WPAD WINS Server Registration Vulnerability - CVE-2009-0094

Mitigation refers to a setting, common configuration, or general best-practice, existing in a default state, that could reduce the severity of exploitation of a vulnerability. The following mitigating factors may be helpful in your situation.	

If WINS server already has WPAD and ISATAP registered than an attacker will not be able to register these as well.

CVE-2009-0233
The DNS Resolver Cache Service (aka DNSCache) in Windows DNS Server in Microsoft Windows 2000 SP4, Server 2003 SP1 and SP2, and Server 2008, when dynamic updates are enabled, does not reuse cached DNS responses in all applicable situations, which makes it easier for remote attackers to predict transaction IDs and poison caches by simultaneously sending crafted DNS queries and responses, aka "DNS Server Query Validation Vulnerability."
CVE-2009-0234
The DNS Resolver Cache Service (aka DNSCache) in Windows DNS Server in Microsoft Windows 2000 SP4, Server 2003 SP1 and SP2, and Server 2008 does not properly cache crafted DNS responses, which makes it easier for remote attackers to predict transaction IDs and poison caches by sending many crafted DNS queries that trigger "unnecessary lookups," aka "DNS Server Response Validation Vulnerability."
CVE-2009-0660
Multiple cross-site scripting (XSS) vulnerabilities in Mahara 1.0 before 1.0.10 and 1.1 before 1.1.2 allow remote attackers to inject arbitrary web script or HTML via a (1) profile and (2) blog, a different vulnerability than CVE-2009-0487.
CVE-2009-0712
Unspecified vulnerability in WMI Mapper for HP Systems Insight Manager before 2.5.2.0 allows local users to gain privileges via unknown vectors.
CVE-2009-0713
Unspecified vulnerability in WMI Mapper for HP Systems Insight Manager before 2.5.2.0 allows remote attackers to obtain sensitive information via unknown vectors.
CVE-2009-0848
Untrusted search path vulnerability in GTK2 in OpenSUSE 11.0 and 11.1 allows local users to execute arbitrary code via a Trojan horse GTK module in an unspecified "relative search path."
CVE-2009-0854
Untrusted search path vulnerability in dash 0.5.4, when used as a login shell, allows local users to execute arbitrary code via a Trojan horse .profile file in the current working directory.
CVE-2009-0871
The SIP channel driver in Asterisk Open Source 1.4.22, 1.4.23, and 1.4.23.1; 1.6.0 before 1.6.0.6; 1.6.1 before 1.6.1.0-rc2; and Asterisk Business Edition C.2.3, with the pedantic option enabled, allows remote authenticated users to cause a denial of service (crash) via a SIP INVITE request without any headers, which triggers a NULL pointer dereference in the (1) sip_uri_headers_cmp and (2) sip_uri_params_cmp functions.
CVE-2009-0872
The NFS server in Sun Solaris 10, and OpenSolaris before snv_111, does not properly implement the AUTH_NONE (aka sec=none) security mode in combination with other security modes, which allows remote attackers to bypass intended access restrictions and read or modify files, as demonstrated by a combination of the AUTH_NONE and AUTH_SYS security modes.
CVE-2009-0873
The NFS daemon (aka nfsd) in Sun Solaris 10 and OpenSolaris before snv_106, when NFSv3 is used, does not properly implement combinations of security modes, which allows remote attackers to bypass intended access restrictions and read or modify files, as demonstrated by a combination of the sec=sys and sec=krb5 security modes, related to modes that "override each other."
