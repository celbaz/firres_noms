CVE-2011-5267
Multiple cross-site scripting (XSS) vulnerabilities in spell-check-savedicts.php in the SpellChecker module in Xinha, as used in WikiWig 5.01 and possibly other products, allow remote attackers to inject arbitrary web script or HTML via the (1) to_p_dict or (2) to_r_list parameter.  NOTE: this issue might be related to the htmlarea plugin and CVE-2013-5670.
CVE-2012-4502
Multiple integer overflows in pktlength.c in Chrony before 1.29 allow remote attackers to cause a denial of service (crash) via a crafted (1) REQ_SUBNETS_ACCESSED or (2) REQ_CLIENT_ACCESSES command request to the PKL_CommandLength function or crafted (3) RPY_SUBNETS_ACCESSED, (4) RPY_CLIENT_ACCESSES, (5) RPY_CLIENT_ACCESSES_BY_INDEX, or (6) RPY_MANUAL_LIST command reply to the PKL_ReplyLength function, which triggers an out-of-bounds read or buffer overflow.  NOTE: versions 1.27 and 1.28 do not require authentication to exploit.
CVE-2012-4503
cmdmon.c in Chrony before 1.29 allows remote attackers to obtain potentially sensitive information from stack memory via vectors related to (1) an invalid subnet in a RPY_SUBNETS_ACCESSED command to the handle_subnets_accessed function or (2) a RPY_CLIENT_ACCESSES command to the handle_client_accesses function when client logging is disabled, which causes uninitialized data to be included in a reply.
CVE-2013-3263
Multiple cross-site scripting (XSS) vulnerabilities in the WP Ultimate Email Marketer plugin 1.1.0 and possibly earlier for Wordpress allow remote attackers to inject arbitrary web script or HTML via the (1) siteurl parameter to campaign/campaignone.php; the (2) action, (3) campaignname, (4) campaignformat, or (5) emailtemplate parameter to campaign/campaigntwo.php; the (6) listid parameter to list/edit.php; the (7) campaignid or (8) siteurl parameter to campaign/editcampaign.php; the (9) campaignid parameter to campaign/selectlistb4send.php; the (10) campaignid, (11) campaignname, (12) campaignsubject, or (13) selectedcampaigns parameter to campaign/sendCampaign.php; or the (14) campaignid, (15) campaignname, (16) campaignformat, or (17) action parameter to campaign/updatecampaign.php.
CVE-2013-3264
The WP Ultimate Email Marketer plugin 1.1.0 and possibly earlier for Wordpress does not properly restrict access to (1) list/edit.php and (2) campaign/editCampaign.php, which allows remote attackers to modify list or campaign data.
CVE-2013-4134
OpenAFS before 1.4.15, 1.6.x before 1.6.5, and 1.7.x before 1.7.26 uses weak encryption (DES) for Kerberos keys, which makes it easier for remote attackers to obtain the service key.
CVE-2013-4135
The vos command in OpenAFS 1.6.x before 1.6.5, when using the -encrypt option, only enables integrity protection and sends data in cleartext, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2013-4419
The guestfish command in libguestfs 1.20.12, 1.22.7, and earlier, when using the --remote or --listen option, does not properly check the ownership of /tmp/.guestfish-$UID/ when creating a temporary socket file in this directory, which allows local users to write to the socket and execute arbitrary commands by creating /tmp/.guestfish-$UID/ in advance.
CVE-2013-4435
Salt (aka SaltStack) 0.15.0 through 0.17.0 allows remote authenticated users who are using external authentication or client ACL to execute restricted routines by embedding the routine in another routine.
CVE-2013-4436
The default configuration for salt-ssh in Salt (aka SaltStack) 0.17.0 does not validate the SSH host key of requests, which allows remote attackers to have unspecified impact via a man-in-the-middle (MITM) attack.
CVE-2013-4437
Unspecified vulnerability in salt-ssh in Salt (aka SaltStack) 0.17.0 has unspecified impact and vectors related to "insecure Usage of /tmp."
CVE-2013-4438
Salt (aka SaltStack) before 0.17.1 allows remote attackers to execute arbitrary YAML code via unspecified vectors.  NOTE: the vendor states that this might not be a vulnerability because the YAML to be loaded has already been determined to be safe.
CVE-2013-4439
Salt (aka SaltStack) before 0.15.0 through 0.17.0 allows remote authenticated minions to impersonate arbitrary minions via a crafted minion with a valid key.
CVE-2013-4453
Cross-site scripting (XSS) vulnerability in templates/login.php in LDAP Account Manager (LAM) 4.3 and 4.2.1 allows remote attackers to inject arbitrary web script or HTML via the language parameter.
CVE-2013-4497
The XenAPI backend in OpenStack Compute (Nova) Folsom, Grizzly, and Havana before 2013.2 does not properly apply security groups (1) when resizing an image or (2) during live migration, which allows remote attackers to bypass intended restrictions.
CVE-2013-5670
Cross-site scripting (XSS) vulnerability in spell-check-savedicts.php in the htmlarea SpellChecker module, as used in Serendipity before 1.7.3 and possibly other products, allows remote attackers to inject arbitrary web script or HTML via the to_r_list parameter.
CVE-2013-5688
Multiple directory traversal vulnerabilities in index.php in AjaXplorer 5.0.2 and earlier allow remote authenticated users to read arbitrary files via a ../%00 (dot dot backslash encoded null byte) in the file parameter in a (1) download or (2) get_content action, or (3) upload arbitrary files via a ../%00 (dot dot backslash encoded null byte) in the dir parameter in an upload action.
CVE-2013-5689
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2013-5688. Reason: This issue has been MERGED with CVE-2013-5688 in accordance with CVE content decisions, because it is the same type of vulnerability affecting the same versions. Notes: All CVE users should reference CVE-2013-5688 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2013-5694
SQL injection vulnerability in status/service/acknowledge in Opsview before 4.4.1 allows remote attackers to execute arbitrary SQL commands via the service_selection parameter.
CVE-2013-5695
Multiple cross-site scripting (XSS) vulnerabilities in Opsview before 4.4.1 allow remote attackers to inject arbitrary web script or HTML via the (1) id parameter to admin/auditlog/, (2) PATH_INFO to info/host/ or (3) viewport/, (4) back parameter to login, or (5) "from" parameter to status/service/recheck.
CVE-2013-6077
Citrix XenDesktop 7.0, when upgraded from XenDesktop 5.x, does not properly enforce policy rule permissions, which allows remote attackers to bypass intended restrictions.
CVE-2013-6172
steps/utils/save_pref.inc in Roundcube webmail before 0.8.7 and 0.9.x before 0.9.5 allows remote attackers to modify configuration settings via the _session parameter, which can be leveraged to read arbitrary files, conduct SQL injection attacks, and execute arbitrary code.
CVE-2013-6617
The salt master in Salt (aka SaltStack) 0.11.0 through 0.17.0 does not properly drop group privileges, which makes it easier for remote attackers to gain privileges.
CVE-2013-6618
jsdm/ajax/port.php in J-Web in Juniper Junos before 10.4R13, 11.4 before 11.4R7, 12.1 before 12.1R5, 12.2 before 12.2R3, and 12.3 before 12.3R1 allows remote authenticated users to execute arbitrary commands via the rsargs parameter in an exec action.
