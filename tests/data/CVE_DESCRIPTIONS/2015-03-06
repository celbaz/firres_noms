CVE-2014-2130
Cisco Secure Access Control Server (ACS) provides an unintentional administration web interface based on Apache Tomcat, which allows remote authenticated users to modify application files and configuration files, and consequently execute arbitrary code, by leveraging administrative privileges, aka Bug ID CSCuj83189.
CVE-2014-8891
Unspecified vulnerability in the Java Virtual Machine (JVM) in IBM SDK, Java Technology Edition 5.0 before SR16-FP9, 6 before SR16-FP3, 6R1 before SR8-FP3, 7 before SR8-FP10, and 7R1 before SR2-FP10 allows remote attackers to escape the Java sandbox and execute arbitrary code via unspecified vectors related to the security manager.
CVE-2014-8892
Unspecified vulnerability in the Java Virtual Machine (JVM) in IBM SDK, Java Technology Edition 5.0 before SR16-FP9, 6 before SR16-FP3, 6R1 before SR8-FP3, 7 before SR8-FP10, and 7R1 before SR2-FP10 allows remote attackers to bypass intended access permissions and obtain sensitive information via unspecified vectors related to the security manager.
CVE-2015-0598
The RADIUS implementation in Cisco IOS and IOS XE allows remote attackers to cause a denial of service (device reload) via crafted IPv6 Attributes in Access-Accept packets, aka Bug IDs CSCur84322 and CSCur27693.
Per <a href="http://tools.cisco.com/security/center/content/CiscoSecurityNotice/CVE-2015-0598">this link</a>, authentication is required.
CVE-2015-0607
The Authentication Proxy feature in Cisco IOS does not properly handle invalid AAA return codes from RADIUS and TACACS+ servers, which allows remote attackers to bypass authentication in opportunistic circumstances via a connection attempt that triggers an invalid code, as demonstrated by a connection attempt with a blank password, aka Bug IDs CSCuo09400 and CSCun16016.
CVE-2015-0657
Cisco IOS XR allows remote attackers to cause a denial of service (RSVP process reload) via a malformed RSVP packet, aka Bug ID CSCur69192.
CVE-2015-0659
The Autonomic Networking Infrastructure (ANI) implementation in Cisco IOS allows remote attackers to trigger self-referential adjacencies via a crafted Autonomic Networking (AN) message, aka Bug ID CSCup62157.
CVE-2015-0661
The SNMPv2 implementation in Cisco IOS XR allows remote authenticated users to cause a denial of service (snmpd daemon reload) via a malformed SNMP packet, aka Bug ID CSCur25858.
CVE-2015-1170
The NVIDIA Display Driver R304 before 309.08, R340 before 341.44, R343 before 345.20, and R346 before 347.52 does not properly validate local client impersonation levels when performing a "kernel administrator check," which allows local users to gain administrator privileges via unspecified API calls.
CVE-2015-1483
Symantec NetBackup OpsCenter 7.6.0.2 through 7.6.1 on Linux and UNIX allows remote attackers to execute arbitrary JavaScript code via unspecified vectors.
CVE-2015-1637
Schannel (aka Secure Channel) in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly restrict TLS state transitions, which makes it easier for remote attackers to conduct cipher-downgrade attacks to EXPORT_RSA ciphers via crafted TLS traffic, related to the "FREAK" issue, a different vulnerability than CVE-2015-0204 and CVE-2015-1067.
