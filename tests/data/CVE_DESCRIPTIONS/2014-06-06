CVE-2012-5390
The standard universe shadow (condor_shadow.std) component in Condor 7.7.3 through 7.7.6, 7.8.0 before 7.8.5, and 7.9.0 does no properly check privileges, which allows remote attackers to gain privileges via a crafted standard universe job.
CVE-2012-5583
phpCAS before 1.3.2 does not verify that the server hostname matches a domain name in the subject's Common Name (CN) or subjectAltName field of the X.509 certificate, which allows man-in-the-middle attackers to spoof SSL servers via an arbitrary valid certificate.
CVE-2013-0250
The init_nss_hash function in exec/totemcrypto.c in Corosync 2.0 before 2.3 does not properly initialize the HMAC key, which allows remote attackers to cause a denial of service (crash) via a crafted packet.
Per: http://cwe.mitre.org/data/definitions/665.html

"CWE-665: Improper Initialization"
CVE-2013-2602
Multiple array index errors in the MyHeritage SEQueryObject ActiveX control (SearchEngineQuery.dll) 1.0.2.0 allow remote attackers to execute arbitrary code via the (1) seTokensArray, or (2) seTokensValuesArray parameter to the AddTokens method; (3) seLastNameTokensArray parameter to the AddLastNameTokens method; (4) seFrameIdArray, (5) seSourceIdArray, (6) seHasBreakdownArray, (7) seIsIndexedArray, (8) seAllConcatArray, (9) seRefererURLArray, or (10) seMandatoryFieldsArray parameter to the AddMultipleSearches method; (11) seSourceIdArray, (12) seIsIndexedArray, (13) seAllConcatArray, (14) seRefererURLArray, (15) seQATestsArray, (16) seAllSourceIDsArray, (17) seAllSourceTitlesArray, (18) seMandatoryFieldsArray, or (19) seAllSourceRootURLArray parameter to the TestYourself method.
Per: http://cwe.mitre.org/data/definitions/129.html

"CWE-129: Improper Validation of Array Index"
CVE-2013-4724
DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions, does not include the HTTPOnly flag in a Set-Cookie header for an unspecified cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.
CVE-2013-4725
DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions, does not set the secure flag for an unspecified cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2013-4727
DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions, allows remote attackers to obtain sensitive information via a request to Admin/top.aspx.
CVE-2013-4728
DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions, allows remote attackers to obtain sensitive information via a .. (dot dot) in the "l" parameter, which reveals the installation path in an error message.
CVE-2014-2503
The thumbnail proxy server in EMC Documentum Digital Asset Manager (DAM) 6.5 SP3, 6.5 SP4, 6.5 SP5, and 6.5 SP6 before P13 allows remote attackers to conduct Documentum Query Language (DQL) injection attacks and bypass intended restrictions on querying objects via a crafted parameter in a query string.
CVE-2014-2575
Directory traversal vulnerability in the File Manager component in DevExpress ASPxFileManager Control for ASP.NET WebForms and MVC before 13.1.10 and 13.2.x before 13.2.9 allows remote authenticated users to read or write arbitrary files via a .. (dot dot) in the __EVENTARGUMENT parameter.
Per: http://security.devexpress.com/de7c4756/?id=ff8c1703126f4717993ac3608a65a2e2

"Affected Products

ASPxFileManager Control for WebForms and MVC (v10.2 and higher)"
CVE-2014-3966
Cross-site scripting (XSS) vulnerability in Special:PasswordReset in MediaWiki before 1.19.16, 1.21.x before 1.21.10, and 1.22.x before 1.22.7, when wgRawHtml is enabled, allows remote attackers to inject arbitrary web script or HTML via an invalid username.
CVE-2014-3984
Multiple unspecified vulnerabilities in Libav before 0.8.12 allow remote attackers to have unknown impact and vectors.
