CVE-2008-3818
Cisco ONS 15310-CL, 15310-MA, 15327, 15454, 15454 SDH, and 15600 with software 7.0.2 through 7.0.6, 7.2.2, 8.0.x, 8.5.1, and 8.5.2 allows remote attackers to cause a denial of service (control-card reset) via a crafted TCP session.
CVE-2008-3821
Multiple cross-site scripting (XSS) vulnerabilities in the HTTP server in Cisco IOS 11.0 through 12.4 allow remote attackers to inject arbitrary web script or HTML via (1) the query string to the ping program or (2) unspecified other aspects of the URI.
CVE-2008-4444
Cisco Unified IP Phone (aka SIP phone) 7960G and 7940G with firmware P0S3-08-9-00 and possibly other versions before 8.10 allows remote attackers to cause a denial of service (device reboot) or possibly execute arbitrary code via a Realtime Transport Protocol (RTP) packet with malformed headers.
CVE-2008-4770
The CMsgReader::readRect function in the VNC Viewer component in RealVNC VNC Free Edition 4.0 through 4.1.2, Enterprise Edition E4.0 through E4.4.2, and Personal Edition P4.0 through P4.4.2 allows remote VNC servers to execute arbitrary code via crafted RFB protocol data, related to "encoding type."
CVE-2008-5908
Unspecified vulnerability in the root/boot archive tool in Sun OpenSolaris has unknown impact and local attack vectors, related to a "Temporary file vulnerability," aka Bug ID 6653455.
CVE-2008-5909
Unspecified vulnerability in conv_lpd in Sun OpenSolaris has unknown impact and local attack vectors, related to improper handling of temporary files, aka Bug ID 6655641.
CVE-2008-5910
Unspecified vulnerability in txzonemgr in Sun OpenSolaris has unknown impact and local attack vectors, related to a "Temporary file vulnerability," aka Bug ID 6653462.
CVE-2009-0053
PXE Encryption in Cisco IronPort Encryption Appliance 6.2.4 before 6.2.4.1.1, 6.2.5, 6.2.6, 6.2.7 before 6.2.7.7, 6.3 before 6.3.0.4, and 6.5 before 6.5.0.2; and Cisco IronPort PostX 6.2.1 before 6.2.1.1 and 6.2.2 before 6.2.2.3; allows remote attackers to obtain the decryption key via unspecified vectors, related to a "logic error."
CVE-2009-0054
PXE Encryption in Cisco IronPort Encryption Appliance 6.2.4 before 6.2.4.1.1, 6.2.5, 6.2.6, 6.2.7 before 6.2.7.7, 6.3 before 6.3.0.4, and 6.5 before 6.5.0.2; and Cisco IronPort PostX 6.2.1 before 6.2.1.1 and 6.2.2 before 6.2.2.3; allows remote attackers to capture credentials by tricking a user into reading a modified or crafted e-mail message.
CVE-2009-0055
Cross-site request forgery (CSRF) vulnerability in the administration interface in Cisco IronPort Encryption Appliance 6.2.4 before 6.2.4.1.1, 6.2.5, 6.2.6, 6.2.7 before 6.2.7.7, 6.3 before 6.3.0.4, and 6.5 before 6.5.0.2; and Cisco IronPort PostX 6.2.1 before 6.2.1.1 and 6.2.2 before 6.2.2.3; allows remote attackers to modify appliance preferences as arbitrary users via unspecified vectors.
CVE-2009-0056
Cross-site request forgery (CSRF) vulnerability in the administration interface in Cisco IronPort Encryption Appliance 6.2.4 before 6.2.4.1.1, 6.2.5, 6.2.6, 6.2.7 before 6.2.7.7, 6.3 before 6.3.0.4, and 6.5 before 6.5.0.2; and Cisco IronPort PostX 6.2.1 before 6.2.1.1 and 6.2.2 before 6.2.2.3; allows remote attackers to execute commands and modify appliance preferences as arbitrary users via a logout action.
CVE-2009-0134
Insecure method vulnerability in the EasyGrid.SGCtrl.32 ActiveX control in EasyGrid.ocx 1.0.0.1 in AAA EasyGrid ActiveX 3.51 allows remote attackers to create and overwrite arbitrary files via the (1) DoSaveFile or (2) DoSaveHtmlFile method.  NOTE: vector 1 could be leveraged for code execution by creating executable files in Startup folders or by accessing files using hcp:// URLs.  NOTE: some of these details are obtained from third party information.
CVE-2009-0135
Multiple integer overflows in the Audible::Tag::readTag function in metadata/audible/audibletag.cpp in Amarok 1.4.10 through 2.0.1 allow remote attackers to execute arbitrary code via an Audible Audio (.aa) file with a large (1) nlen or (2) vlen Tag value, each of which triggers a heap-based buffer overflow.
CVE-2009-0136
Multiple array index errors in the Audible::Tag::readTag function in metadata/audible/audibletag.cpp in Amarok 1.4.10 through 2.0.1 allow remote attackers to cause a denial of service (application crash) or execute arbitrary code via an Audible Audio (.aa) file with a crafted (1) nlen or (2) vlen Tag value, each of which can lead to an invalid pointer dereference, or the writing of a 0x00 byte to an arbitrary memory location, after an allocation failure.
CVE-2009-0167
Unspecified vulnerability in lpadmin in Sun Solaris 10 and OpenSolaris snv_61 through snv_106 allows local users to cause a denial of service via unspecified vectors, related to enumeration of "wrong printers," aka a "Temporary file vulnerability."
CVE-2009-0168
Unspecified vulnerability in ppdmgr in Sun Solaris 10 and OpenSolaris snv_61 through snv_106 allows local users to cause a denial of service via unspecified vectors, related to a failure to "include all cache files," and improper handling of temporary files.
CVE-2009-0169
Sun Java System Access Manager 7.1 allows remote authenticated sub-realm administrators to gain privileges, as demonstrated by creating the amadmin account in the sub-realm, and then logging in as amadmin in the root realm.
CVE-2009-0170
Sun Java System Access Manager 6.3 2005Q1, 7 2005Q4, and 7.1 allows remote authenticated users with console privileges to discover passwords, and obtain unspecified other "access to resources," by visiting the Configuration Items component in the console.
CVE-2009-0171
The Sun SPARC Enterprise M4000 and M5000 Server, within a certain range of serial numbers, allows remote attackers to use the manufacturing root password, perform a root login to the eXtended System Control Facility Unit (aka XSCFU or Service Processor), and have unspecified other impact.
CVE-2009-0172
Unspecified vulnerability in IBM DB2 8 before FP17a, 9.1 before FP6a, and 9.5 before FP3a allows remote attackers to cause a denial of service (infinite loop) via a crafted CONNECT data stream.
CVE-2009-0173
Unspecified vulnerability in the server in IBM DB2 8 before FP17a, 9.1 before FP6a, and 9.5 before FP3a allows remote authenticated users to cause a denial of service (trap) via a crafted data stream.
