CVE-2010-5323
Directory traversal vulnerability in UploadServlet in the Remote Management component in Novell ZENworks Configuration Management (ZCM) 10 before 10.3 allows remote attackers to execute arbitrary code via a crafted WAR pathname in the filename parameter in conjunction with WAR content in the POST data, a different vulnerability than CVE-2010-5324.
CVE-2010-5324
Directory traversal vulnerability in UploadServlet in the Remote Management component in Novell ZENworks Configuration Management (ZCM) 10 before 10.3 allows remote attackers to execute arbitrary code via a zenworks-fileupload request with a crafted directory name in the type parameter, in conjunction with a WAR filename in the filename parameter and WAR content in the POST data, a different vulnerability than CVE-2010-5323.
CVE-2014-0230
Apache Tomcat 6.x before 6.0.44, 7.x before 7.0.55, and 8.x before 8.0.9 does not properly handle cases where an HTTP response occurs before finishing the reading of an entire request body, which allows remote attackers to cause a denial of service (thread consumption) via a series of aborted upload attempts.
CVE-2014-6175
Cross-site scripting (XSS) vulnerability in IBM Marketing Operations 7.x and 8.x before 8.5.0.7.2, 8.6.x before 8.6.0.8, 9.0.x before 9.0.0.4.1, 9.1.0.x before 9.1.0.5, and 9.1.1.x before 9.1.1.2 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6222
Directory traversal vulnerability in IBM Marketing Operations 7.x and 8.x before 8.5.0.7.2, 8.6.x before 8.6.0.8, 9.0.x before 9.0.0.4.1, 9.1.0.x before 9.1.0.5, and 9.1.1.x before 9.1.1.2 allows remote authenticated users to read arbitrary files via a .. (dot dot) in a URL.
CVE-2014-7810
The Expression Language (EL) implementation in Apache Tomcat 6.x before 6.0.44, 7.x before 7.0.58, and 8.x before 8.0.16 does not properly consider the possibility of an accessible interface implemented by an inaccessible class, which allows attackers to bypass a SecurityManager protection mechanism via a web application that leverages use of incorrect privileges during EL evaluation.
CVE-2014-8887
IBM Marketing Operations 7.x and 8.x before 8.5.0.7.2, 8.6.x before 8.6.0.8, 9.0.x before 9.0.0.4.1, 9.1.0.x before 9.1.0.5, and 9.1.1.x before 9.1.1.2 allows remote authenticated users to upload arbitrary GIFAR files, and consequently modify data, via unspecified vectors.
CVE-2015-0112
Jazz Team Server in Jazz Foundation in IBM Rational Collaborative Lifecycle Management (CLM) 3.0.1, 4.x before 4.0.7 IF5, and 5.x before 5.0.2 IF4; Rational Quality Manager (RQM) 2.0 through 2.0.1, 3.0 through 3.0.1.6, 4.0 through 4.0.7, and 5.0 through 5.0.2; Rational Team Concert (RTC) 2.0 through 2.0.0.2, 3.x before 3.0.1.6 IF6, 4.x before 4.0.7 IF5, and 5.x before 5.0.2 IF4; Rational Requirements Composer (RRC) 2.0 through 2.0.0.4, 3.x before 3.0.1.6 IF6, and 4.0 through 4.0.7; Rational DOORS Next Generation (RDNG) 4.x before 4.0.7 IF5 and 5.x before 5.0.2 IF4; Rational Engineering Lifecycle Manager (RELM) 1.0 through 1.0.0.1, 4.0.3 through 4.0.7, and 5.0 through 5.0.2; Rational Rhapsody Design Manager (DM) 3.0 through 3.0.1, 4.0 through 4.0.7, and 5.0 through 5.0.2; and Rational Software Architect Design Manager (RSA DM) 3.0 through 3.0.1, 4.0 through 4.0.7, and 5.0 through 5.0.2 allows remote authenticated users to read arbitrary files via an XML external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-0767
Cisco Edge 300 software 1.0 and 1.1 on Edge 340 devices allows local users to obtain root privileges via unspecified commands, aka Bug ID CSCur18132.
CVE-2015-0770
CRLF injection vulnerability in Cisco TelePresence TC 6.x before 6.3.4 and 7.x before 7.3.3 on Integrator C SX20 devices allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a crafted URL, aka Bug ID CSCut79341.
CVE-2015-0779
Directory traversal vulnerability in UploadServlet in Novell ZENworks Configuration Management (ZCM) 10 and 11 before 11.3.2 allows remote attackers to execute arbitrary code via a crafted directory name in the uid parameter, in conjunction with a WAR filename in the filename parameter and WAR content in the POST data, a different vulnerability than CVE-2010-5323 and CVE-2010-5324.
CVE-2015-2125
Unspecified vulnerability in HP WebInspect 7.x through 10.4 before 10.4 update 1 allows remote authenticated users to bypass intended access restrictions via unknown vectors.
CVE-2015-4001
Integer signedness error in the oz_hcd_get_desc_cnf function in drivers/staging/ozwpan/ozhcd.c in the OZWPAN driver in the Linux kernel through 4.0.5 allows remote attackers to cause a denial of service (system crash) or possibly execute arbitrary code via a crafted packet.
CVE-2015-4002
drivers/staging/ozwpan/ozusbsvc1.c in the OZWPAN driver in the Linux kernel through 4.0.5 does not ensure that certain length values are sufficiently large, which allows remote attackers to cause a denial of service (system crash or large loop) or possibly execute arbitrary code via a crafted packet, related to the (1) oz_usb_rx and (2) oz_usb_handle_ep_data functions.
CVE-2015-4003
The oz_usb_handle_ep_data function in drivers/staging/ozwpan/ozusbsvc1.c in the OZWPAN driver in the Linux kernel through 4.0.5 allows remote attackers to cause a denial of service (divide-by-zero error and system crash) via a crafted packet.
CVE-2015-4004
The OZWPAN driver in the Linux kernel through 4.0.5 relies on an untrusted length field during packet parsing, which allows remote attackers to obtain sensitive information from kernel memory or cause a denial of service (out-of-bounds read and system crash) via a crafted packet.
