CVE-2014-0029
Multiple cross-site scripting (XSS) vulnerabilities in the SAM web application in Red Hat katello-headpin allow remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2014-0208
Cross-site scripting (XSS) vulnerability in the search auto-completion functionality in Foreman before 1.4.4 allows remote authenticated users to inject arbitrary web script or HTML via a crafted key name.
CVE-2014-3702
Directory traversal vulnerability in eNovance eDeploy allows remote attackers to create arbitrary directories and files and consequently cause a denial of service (resource consumption) via a .. (dot dot) the session parameter.
CVE-2014-7851
oVirt 3.2.2 through 3.5.0 does not invalidate the restapi session after logout from the webadmin, which allows remote authenticated users with knowledge of another user's session data to gain that user's privileges by replacing their session token with that of another user.
CVE-2014-8087
Cross-site scripting (XSS) vulnerability in the post highlights plugin before 2.6.1 for WordPress allows remote attackers to inject arbitrary web script or HTML via the txt parameter in a headline action to ajax/ph_save.php.
CVE-2014-8621
SQL injection vulnerability in the Store Locator plugin 2.3 through 3.11 for WordPress allows remote attackers to execute arbitrary SQL commands via the sl_custom_field parameter to sl-xml.php.
CVE-2014-9147
Fiyo CMS 2.0.1.8 allows remote attackers to obtain sensitive information via a direct request to the database backup file in .backup/.
CVE-2014-9148
Fiyo CMS 2.0.1.8 allows remote attackers to bypass intended access restrictions and execute the (1) "Install and Update" or (2) Backup super administrator function via the view parameter in a direct request to fiyo/dapur.
CVE-2015-2780
Unrestricted file upload vulnerability in Berta CMS allows remote attackers to execute arbitrary code by uploading a crafted image file with an executable extension, then accessing it via a direct request to the file in an unspecified directory.
CVE-2015-3229
fedora-cloud-atomic.ks in spin-kickstarts allows remote attackers to conduct man-in-the-middle attacks by leveraging use of HTTP to download Fedora Atomic updates.
CVE-2015-4650
Aruba Networks ClearPass Policy Manager before 6.4.7 and 6.5.x before 6.5.2 allows remote attackers to gain shell access and execute arbitrary code with root privileges via unspecified vectors.
CVE-2015-7504
Heap-based buffer overflow in the pcnet_receive function in hw/net/pcnet.c in QEMU allows guest OS administrators to cause a denial of service (instance crash) or possibly execute arbitrary code via a series of packets in loopback mode.
CVE-2015-7687
Use-after-free vulnerability in OpenSMTPD before 5.7.2 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via vectors involving req_ca_vrfy_smtp and req_ca_vrfy_mta.
CVE-2016-4461
Apache Struts 2.x before 2.3.29 allows remote attackers to execute arbitrary code via a "%{}" sequence in a tag attribute, aka forced double OGNL evaluation.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2016-0785.
CVE-2016-8734
Apache Subversion's mod_dontdothat module and HTTP clients 1.4.0 through 1.8.16, and 1.9.0 through 1.9.4 are vulnerable to a denial-of-service attack caused by exponential XML entity expansion. The attack can cause the targeted process to consume an excessive amount of CPU resources or memory.
CVE-2017-0316
In GeForce Experience (GFE) 3.x before 3.10.0.55, NVIDIA Installer Framework contains a vulnerability in NVISystemService64 where a value passed from a user to the driver is used without validation, which may lead to denial of service or possible escalation of privileges.
CVE-2017-14952
Double free in i18n/zonemeta.cpp in International Components for Unicode (ICU) for C/C++ through 59.1 allows remote attackers to execute arbitrary code via a crafted string, aka a "redundant UVector entry clean up function call" issue.
CVE-2017-15221
ASX to MP3 converter 3.1.3.7.2010.11.05 has a buffer overflow via a crafted M3U file, a related issue to CVE-2009-1324.
CVE-2017-15265
Race condition in the ALSA subsystem in the Linux kernel before 4.13.8 allows local users to cause a denial of service (use-after-free) or possibly have unspecified other impact via crafted /dev/snd/seq ioctl calls, related to sound/core/seq/seq_clientmgr.c and sound/core/seq/seq_ports.c.
CVE-2017-15289
The mode4and5 write functions in hw/display/cirrus_vga.c in Qemu allow local OS guest privileged users to cause a denial of service (out-of-bounds write access and Qemu process crash) via vectors related to dst calculation.
CVE-2017-15293
Xpress Server in SAP POS does not require authentication for file read and erase operations, daemon shutdown, terminal read operations, or certain attacks on credentials. This is SAP Security Note 2520064.
CVE-2017-15294
The Java administration console in SAP CRM has XSS. This is SAP Security Note 2478964.
CVE-2017-15295
Xpress Server in SAP POS does not require authentication for read/write/delete file access. This is SAP Security Note 2520064.
CVE-2017-15296
The Java component in SAP CRM has CSRF. This is SAP Security Note 2478964.
CVE-2017-15297
SAP Hostcontrol does not require authentication for the SOAP SAPControl endpoint. This is SAP Security Note 2442993.
CVE-2017-15302
In CPUID CPU-Z through 1.81, there are improper access rights to a kernel-mode driver (e.g., cpuz143_x64.sys for version 1.43) that can result in information disclosure or elevation of privileges, because of an arbitrary read of any physical address via ioctl 0x9C402604. Any application running on the system (Windows), including sandboxed users, can issue an ioctl to this driver without any validation. Furthermore, the driver can map any physical page on the system and returns the allocated map page address to the user: that results in an information leak and EoP. NOTE: the vendor indicates that the arbitrary read itself is intentional behavior (for ACPI scan functionality); the security issue is the lack of an ACL.
CVE-2017-15303
In CPUID CPU-Z before 1.43, there is an arbitrary memory write that results directly in elevation of privileges, because any program running on the local machine (while CPU-Z is running) can issue an ioctl 0x9C402430 call to the kernel-mode driver (e.g., cpuz141_x64.sys for version 1.41).
CVE-2017-15361
The Infineon RSA library 1.02.013 in Infineon Trusted Platform Module (TPM) firmware, such as versions before 0000000000000422 - 4.34, before 000000000000062b - 6.43, and before 0000000000008521 - 133.33, mishandles RSA key generation, which makes it easier for attackers to defeat various cryptographic protection mechanisms via targeted attacks, aka ROCA. Examples of affected technologies include BitLocker with TPM 1.2, YubiKey 4 (before 4.3.5) PGP key generation, and the Cached User Data encryption feature in Chrome OS.
CVE-2017-15362
osTicket 1.10.1 allows arbitrary client-side JavaScript code execution on victims who click a crafted support/scp/tickets.php?status= link, aka XSS. Session ID and data theft may follow as well as the possibility of bypassing CSRF protections, injection of iframes to establish communication channels, etc. The vulnerability is present after login into the application. This affects a different tickets.php file than CVE-2015-1176.
CVE-2017-15368
The wasm_dis function in libr/asm/arch/wasm/wasm.c in radare2 2.0.0 allows remote attackers to cause a denial of service (stack-based buffer over-read and application crash) or possibly have unspecified other impact via a crafted WASM file that triggers an incorrect r_hex_bin2str call.
CVE-2017-15369
The build_filter_chain function in pdf/pdf-stream.c in Artifex MuPDF before 2017-09-25 mishandles a certain case where a variable may reside in a register, which allows remote attackers to cause a denial of service (Fitz fz_drop_imp use-after-free and application crash) or possibly have unspecified other impact via a crafted PDF document.
CVE-2017-15370
There is a heap-based buffer overflow in the ImaExpandS function of ima_rw.c in Sound eXchange (SoX) 14.4.2. A Crafted input will lead to a denial of service attack during conversion of an audio file.
CVE-2017-15371
There is a reachable assertion abort in the function sox_append_comment() in formats.c in Sound eXchange (SoX) 14.4.2. A Crafted input will lead to a denial of service attack during conversion of an audio file.
CVE-2017-15372
There is a stack-based buffer overflow in the lsx_ms_adpcm_block_expand_i function of adpcm.c in Sound eXchange (SoX) 14.4.2. A Crafted input will lead to a denial of service attack during conversion of an audio file.
CVE-2017-15373
E-Sic 1.0 allows SQL injection via the q parameter to esiclivre/restrito/inc/lkpcep.php (aka the search private area).
CVE-2017-15374
Shopware v5.2.5 - v5.3 is vulnerable to cross site scripting in the customer and order section of the content management system backend modules. Remote attackers are able to inject malicious script code into the firstname, lastname, or order input fields to provoke persistent execution in the customer and orders section of the backend. The execution occurs in the administrator backend listing when processing a preview of the customers (kunden) or orders (bestellungen). The injection can be performed interactively via user registration or by manipulation of the order information inputs. The issue can be exploited by low privileged user accounts against higher privileged (admin or moderator) accounts.
CVE-2017-15375
Multiple client-side cross site scripting vulnerabilities have been discovered in the WpJobBoard v4.5.1 web-application for WordPress. The vulnerabilities are located in the `query` and `id` parameters of the `wpjb-email`, `wpjb-job`, `wpjb-application`, and `wpjb-membership` modules. Remote attackers are able to inject malicious script code to hijack admin session credentials via the backend, or to manipulate the backend on client-side performed requests. The attack vector is non-persistent and the request method to inject is GET. The attacker does not need a privileged user account to perform a successful exploitation.
CVE-2017-15376
The TELNET service in Mobatek MobaXterm 10.4 does not require authentication, which allows remote attackers to execute arbitrary commands via TCP port 23.
CVE-2017-15383
Nero 7.10.1.0 has an unquoted BINARY_PATH_NAME for NBService, exploitable via a Trojan horse Nero.exe file in the %PROGRAMFILES(x86)%\Nero directory.
CVE-2017-15384
rate-me.php in Rate Me 1.0 has XSS via the id field in a rate action.
CVE-2017-15385
The store_versioninfo_gnu_verdef function in libr/bin/format/elf/elf.c in radare2 2.0.0 allows remote attackers to cause a denial of service (r_read_le16 invalid write and application crash) or possibly have unspecified other impact via a crafted ELF file.
CVE-2017-9367
A directory traversal vulnerability in the BlackBerry Workspaces Server could potentially allow an attacker to execute or upload arbitrary files, or reveal the content of arbitrary files anywhere on the web server by crafting a URL with a manipulated POST request.
CVE-2017-9368
An information disclosure vulnerability in the BlackBerry Workspaces Server could result in an attacker gaining access to source code for server-side applications by crafting a request for specific files.
