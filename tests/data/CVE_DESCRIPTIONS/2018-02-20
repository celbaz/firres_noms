CVE-2015-2081
Datto ALTO and SIRIS devices allow Remote Code Execution via unauthenticated requests to PHP scripts.
CVE-2015-6544
Cross-site scripting (XSS) vulnerability in application/dashboard.class.inc.php in Combodo iTop before 2.2.0-2459 allows remote attackers to inject arbitrary web script or HTML via a dashboard title.
CVE-2015-9254
Datto ALTO and SIRIS devices have a default VNC password.
CVE-2015-9255
Datto ALTO and SIRIS devices allow remote attackers to obtain sensitive information about data, software versions, configuration, and virtual machines via a request to a Web Virtual Directory.
CVE-2015-9256
Datto ALTO and SIRIS devices allow remote attackers to obtain sensitive information via access to device/VM restore mount points, because they do not have ACLs by default.
CVE-2016-6272
XPath injection vulnerability in Epic MyChart allows remote attackers to access contents of an XML document containing static display strings, such as field labels, via the topic parameter to help.asp. NOTE: this was originally reported as a SQL injection vulnerability, but this may be inaccurate.
CVE-2017-10963
In Knox SDS IAM (Identity Access Management) and EMM (Enterprise Mobility Management) 16.11 on Samsung mobile devices, a man-in-the-middle attacker can install any application into the Knox container (without the user's knowledge) by inspecting network traffic from a Samsung server and injecting content at a certain point in the update sequence. This installed application can further leak information stored inside the Knox container to the outside world.
CVE-2017-12415
OXID eShop Community Edition before 6.0.0 RC2 (development), 4.10.x before 4.10.5 (maintenance), and 4.9.x before 4.9.10 (legacy), Enterprise Edition before 6.0.0 RC2 (development), 5.2.x before 5.2.10 (legacy), and 5.3.x before 5.3.5 (maintenance), and Professional Edition before 6.0.0 RC2 (development), 4.9.x before 4.9.10 (legacy) and 4.10.x before 4.10.5 (maintenance) allow remote attackers to hijack the cart session of a client via Cross-Site Request Forgery (CSRF) if the following pre-conditions are met: (1) the attacker knows which shop is presently used by the client, (2) the attacker knows the exact time when the customer will add product items to the cart, (3) the attacker knows which product items are already in the cart (has to know their article IDs), and (4) the attacker would be able to trick user into clicking a button (submit form) of an e-mail or remote site within the period of visiting the shop and placing an order.
CVE-2017-14993
OXID eShop Community Edition before 6.0.0 RC3 (development), 4.10.x before 4.10.6 (maintenance), and 4.9.x before 4.9.11 (legacy), Enterprise Edition before 6.0.0 RC3 (development), 5.2.x before 5.2.11 (legacy), and 5.3.x before 5.3.6 (maintenance), and Professional Edition before 6.0.0 RC3 (development), 4.9.x before 4.9.11 (legacy) and 4.10.x before 4.10.6 (maintenance) allow remote attackers to crawl specially crafted URLs (aka "forced browsing") in order to overflow the database of the shop and consequently make it stop working. Prerequisite: the shop allows rendering empty categories to the storefront via an admin option.
CVE-2017-16356
Reflected XSS in Kubik-Rubik SIGE (aka Simple Image Gallery Extended) before 3.3.0 allows attackers to execute JavaScript in a victim's browser by having them visit a plugins/content/sige/plugin_sige/print.php link with a crafted img, name, or caption parameter.
CVE-2017-16835
The "Photo,Video Locker-Calculator" application 12.0 for Android has android:allowBackup="true" in AndroidManifest.xml, which allows attackers to obtain sensitive cleartext information via an "adb backup '-f smart.calculator.gallerylock'" command.
CVE-2017-17454
Mahara 16.10 before 16.10.7 and 17.04 before 17.04.5 and 17.10 before 17.10.2 have a Cross Site Scripting (XSS) vulnerability when a user enters invalid UTF-8 characters. These are now going to be discarded in Mahara along with NULL characters and invalid Unicode characters. Mahara will also avoid direct $_GET and $_POST usage where possible, and instead use param_exists() and the correct param_*() function to fetch the expected value.
CVE-2017-17455
Mahara 16.10 before 16.10.7, 17.04 before 17.04.5, and 17.10 before 17.10.2 are vulnerable to being forced, via a man-in-the-middle attack, to interact with Mahara on the HTTP protocol rather than HTTPS even when an SSL certificate is present.
CVE-2017-18192
smart/calculator/gallerylock/CalculatorActivity.java in the "Photo,Video Locker-Calculator" application through 18 for Android allows attackers to access files via the backdoor 17621762 PIN.
CVE-2017-6192
Buffer overflow in APNGDis 2.8 and earlier allows a remote attackers to cause denial of service and possibly execute arbitrary code via a crafted image containing a malformed chunk size descriptor.
CVE-2017-6193
Buffer overflow in APNGDis 2.8 and earlier allows remote attackers to cause a denial of service and possibly execute arbitrary code via a crafted image containing a malformed image size descriptor in the IHDR chunk.
CVE-2018-5477
An Information Exposure issue was discovered in ABB netCADOPS Web Application Version 3.4 and prior, netCADOPS Web Application Version 7.1 and prior, netCADOPS Web Application Version 7.2x and prior, netCADOPS Web Application Version 8.0 and prior, and netCADOPS Web Application Version 8.1 and prior. A vulnerability exists in the password entry section of netCADOPS Web Application that may expose critical database information.
CVE-2018-6356
Jenkins before 2.107 and Jenkins LTS before 2.89.4 did not properly prevent specifying relative paths that escape a base directory for URLs accessing plugin resource files. This allowed users with Overall/Read permission to download files from the Jenkins master they should not have access to. On Windows, any file accessible to the Jenkins master process could be downloaded. On other operating systems, any file within the Jenkins home directory accessible to the Jenkins master process could be downloaded.
CVE-2018-6459
The rsa_pss_params_parse function in libstrongswan/credentials/keys/signature_params.c in strongSwan 5.6.1 allows remote attackers to cause a denial of service via a crafted RSASSA-PSS signature that lacks a mask generation function parameter.
CVE-2018-6487
Remote Disclosure of Information in Micro Focus Universal CMDB Foundation Software, version numbers 10.10, 10.11, 10.20, 10.21, 10.22, 10.30, 10.31, 4.10, 4.11. This vulnerability could be remotely exploited to allow disclosure of information.
CVE-2018-6940
A /shell?cmd= XSS issue exists in the HTTPD component of NAT32 v2.2 Build 22284 devices that can be exploited for Remote Code Execution in conjunction with CSRF.
CVE-2018-6941
A /shell?cmd= CSRF issue exists in the HTTPD component of NAT32 v2.2 Build 22284 devices that can be exploited for Remote Code Execution in conjunction with XSS.
CVE-2018-7046
** DISPUTED ** Arbitrary code execution vulnerability in Kentico 9 through 11 allows remote authenticated users to execute arbitrary operating system commands in a dynamic .NET code evaluation context via C# code in a "Pages -> Edit -> Template -> Edit template properties -> Layout" box.  NOTE: the vendor has responded that there is intended functionality for authorized users to edit and update ascx code layout.
CVE-2018-7205
** DISPUTED ** Reflected Cross-Site Scripting vulnerability in "Design" on "Edit device layout" in Kentico 9 through 11 allows remote attackers to execute malicious JavaScript via a malicious devicename parameter in a link that is entered via the "Pages -> Edit template properties -> Device Layouts -> Create device layout (and edit created device layout) -> Design" screens.  NOTE: the vendor has responded that there is intended functionality for authorized users to edit and update ascx code layout.
CVE-2018-7259
The FSX / P3Dv4 installer 2.0.1.231 for Flight Sim Labs A320-X sends a user's Google account credentials to http://installLog.flightsimlabs.com/LogHandler3.ashx if a pirated serial number has been entered, which allows remote attackers to obtain sensitive information, e.g., by sniffing the network for cleartext HTTP traffic. This behavior was removed in 2.0.1.232.
CVE-2018-7263
The mad_decoder_run() function in decoder.c in Underbit libmad through 0.15.1b allows remote attackers to cause a denial of service (SIGABRT because of double free or corruption) or possibly have unspecified other impact via a crafted file. NOTE: this may overlap CVE-2017-11552.
CVE-2018-7265
Shimmie 2 2.6.0 allows an attacker to upload a crafted SVG file that enables stored XSS.
