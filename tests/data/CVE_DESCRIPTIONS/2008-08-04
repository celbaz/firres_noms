CVE-2008-1232
Cross-site scripting (XSS) vulnerability in Apache Tomcat 4.1.0 through 4.1.37, 5.5.0 through 5.5.26, and 6.0.0 through 6.0.16 allows remote attackers to inject arbitrary web script or HTML via a crafted string that is used in the message argument to the HttpServletResponse.sendError method.
CVE-2008-2320
Stack-based buffer overflow in CarbonCore in Apple Mac OS X 10.4.11 and 10.5.4, iPhone OS 1.0 through 2.2.1, and iPhone OS for iPod touch 1.1 through 2.2.1 allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via a long filename to the file management API.
CVE-2008-2321
Unspecified vulnerability in CoreGraphics in Apple Mac OS X 10.4.11 and 10.5.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via unknown vectors involving "processing of arguments."
CVE-2008-2322
Integer overflow in CoreGraphics in Apple Mac OS X 10.4.11, 10.5.2, and 10.5.4 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a PDF file with a long Type 1 font, which triggers a heap-based buffer overflow.
CVE-2008-2323
Unspecified vulnerability in Data Detectors Engine in Apple Mac OS X 10.5.4 allows attackers to cause a denial of service (resource consumption) via crafted textual content in messages.
CVE-2008-2324
The Repair Permissions tool in Disk Utility in Apple Mac OS X 10.4.11 adds the setuid bit to the emacs executable file, which allows local users to gain privileges by executing commands within emacs.
CVE-2008-2325
QuickLook in Apple Mac OS X 10.4.11 and 10.5.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted Microsoft Office file, related to insufficient "bounds checking."
CVE-2008-2370
Apache Tomcat 4.1.0 through 4.1.37, 5.5.0 through 5.5.26, and 6.0.0 through 6.0.16, when a RequestDispatcher is used, performs path normalization before removing the query string from the URI, which allows remote attackers to conduct directory traversal attacks and read arbitrary files via a .. (dot dot) in a request parameter.
CVE-2008-3423
IBM WebSphere Portal 5.1 through 6.1.0.0 allows remote attackers to bypass authentication and obtain administrative access via unspecified vectors.
CVE-2008-3444
The content layout component in Mozilla Firefox 3.0 and 3.0.1 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted but well-formed web page that contains "a simple set of legitimate HTML tags."
CVE-2008-3445
SQL injection vulnerability in index.php in phpMyRealty (PMR) 2.0.0 allows remote attackers to execute arbitrary SQL commands via the location parameter.
CVE-2008-3446
Directory traversal vulnerability in inc/wysiwyg.php in LetterIt 2 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the language parameter.
CVE-2008-3447
The scanning engine in F-Prot Antivirus 6.2.1 4252 allows remote attackers to cause a denial of service (infinite loop) via a malformed ZIP archive, probably related to invalid offsets.
CVE-2008-3448
Cross-site scripting (XSS) vulnerability in index.php in common solutions csphonebook 1.02 allows remote attackers to inject arbitrary web script or HTML via the letter parameter.
CVE-2008-3449
MailEnable Professional 3.5.2 and Enterprise 3.52 allow remote attackers to cause a denial of service (crash) via multiple IMAP connection requests to the same folder.
CVE-2008-3450
Unspecified vulnerability in the namefs kernel module in Sun Solaris 8 through 10 allows local users to gain privileges or cause a denial of service (panic) via unspecified vectors.
CVE-2008-3451
PhpWebGallery 1.7.0 and 1.7.1 allows remote authenticated users with advisor privileges to obtain the real e-mail addresses of other users by editing the user's profile.
CVE-2008-3452
SQL injection vulnerability in the Calendar module in eNdonesia 8.4 allows remote attackers to execute arbitrary SQL commands via the loc_id parameter in a list_events action to mod.php.
CVE-2008-3453
Multiple unspecified vulnerabilities in ImpressCMS 1.0 have unknown impact and attack vectors, related to modules/admin.php and "a few files."
CVE-2008-3454
JnSHosts PHP Hosting Directory 2.0 allows remote attackers to bypass authentication and gain administrative access by setting the "adm" cookie value to 1.
CVE-2008-3455
PHP remote file inclusion vulnerability in include/admin.php in JnSHosts PHP Hosting Directory 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the rd parameter.
CVE-2008-3456
phpMyAdmin before 2.11.8 does not sufficiently prevent its pages from using frames that point to pages in other domains, which makes it easier for remote attackers to conduct spoofing or phishing activities via a cross-site framing attack.
CVE-2008-3457
Cross-site scripting (XSS) vulnerability in setup.php in phpMyAdmin before 2.11.8 allows user-assisted remote attackers to inject arbitrary web script or HTML via crafted setup arguments.  NOTE: this issue can only be exploited in limited scenarios in which the attacker must be able to modify config/config.inc.php.
CVE-2008-3458
Vtiger CRM before 5.0.4 stores sensitive information under the web root with insufficient access control, which allows remote attackers to read mail merge templates via a direct request to the wordtemplatedownload directory.
CVE-2008-3459
Unspecified vulnerability in OpenVPN 2.1-beta14 through 2.1-rc8, when running on non-Windows systems, allows remote servers to execute arbitrary commands via crafted (1) lladdr and (2) iproute configuration directives, probably related to shell metacharacters.
The following events must take place for successful exploitation:
1) the client has agreed to allow the server to push configuration directives to it by including "pull" or the macro "client" in its configuration file
2) the client succesfully authenticates the server 
3) the server is malicious or has been compromised and is under the control of the attacker
4) the client is running a non-Windows OS.
