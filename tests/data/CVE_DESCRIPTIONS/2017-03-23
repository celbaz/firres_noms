CVE-2013-6446
The JobHistory Server in Cloudera CDH 4.x before 4.6.0 and 5.x before 5.0.0 Beta 2, when using MRv2/YARN with HTTP authentication, allows remote authenticated users to obtain sensitive job information by leveraging failure to enforce job ACLs.
CVE-2014-0229
Apache Hadoop 0.23.x before 0.23.11 and 2.x before 2.4.1, as used in Cloudera CDH 5.0.x before 5.0.2, do not check authorization for the (1) refreshNamenodes, (2) deleteBlockPool, and (3) shutdownDatanode HDFS admin commands, which allows remote authenticated users to cause a denial of service (DataNodes shutdown) or perform unnecessary operations by issuing a command.
CVE-2014-7279
The Konke Smart Plug K does not require authentication for TELNET sessions, which allows remote attackers to obtain "equipment  management authority" via TCP traffic to port 23.
CVE-2014-8731
PHPMemcachedAdmin 1.2.2 and earlier allows remote attackers to execute arbitrary PHP code via vectors related "serialized data and the last part of the concatenated filename," which creates a file in webroot.
CVE-2014-9915
Off-by-one error in ImageMagick before 6.6.0-4 allows remote attackers to cause a denial of service (application crash) via a crafted 8BIM profile.
CVE-2015-0855
The _mediaLibraryPlayCb function in mainwindow.py in pitivi before 0.95 allows attackers to execute arbitrary code via shell metacharacters in a file path.
CVE-2015-2263
Cloudera Manager 4.x, 5.0.x before 5.0.6, 5.1.x before 5.1.5, 5.2.x before 5.2.5, and 5.3.x before 5.3.3 uses global read permissions for files in its configuration directory when starting YARN NodeManager, which allows local users to obtain sensitive information by reading the files, as demonstrated by yarn.keytab or ssl-server.xml in /var/run/cloudera-scm-agent/process.
CVE-2015-4078
Cloudera Navigator 2.2.x before 2.2.4 and 2.3.x before 2.3.3 include support for SSLv3 when configured to use SSL/TLS, which makes it easier for man-in-the-middle attackers to obtain cleartext data via a padding-oracle attack, a variant of CVE-2014-3566 (aka POODLE).
CVE-2015-4166
Cloudera Key Trustee Server before 5.4.3 does not store keys synchronously, which might allow attackers to have unspecified impact via vectors related to loss of an encryption key.
CVE-2015-5729
The Soft Access Point (AP) feature in Samsung Smart TVs X10P, X12, X14H, X14J, and NT14U and Xpress M288OFW printers generate weak WPA2 PSK keys, which makes it easier for remote attackers to obtain sensitive information or bypass authentication via a brute-force attack.
CVE-2015-8622
Cross-site scripting (XSS) vulnerability in MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1, when is configured with a relative URL, allows remote authenticated users to inject arbitrary web script or HTML via wikitext, as demonstrated by a wikilink to a page named "javascript:alert('XSS!')."
CVE-2015-8623
The User::matchEditToken function in includes/User.php in MediaWiki before 1.23.12 and 1.24.x before 1.24.5 does not perform token comparison in constant time before returning, which allows remote attackers to guess the edit token and bypass CSRF protection via a timing attack, a different vulnerability than CVE-2015-8624.
CVE-2015-8624
The User::matchEditToken function in includes/User.php in MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1 does not perform token comparison in constant time before determining if a debugging message should be logged, which allows remote attackers to guess the edit token and bypass CSRF protection via a timing attack, a different vulnerability than CVE-2015-8623.
CVE-2015-8625
MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1 do not properly sanitize parameters when calling the cURL library, which allows remote attackers to read arbitrary files via an @ (at sign) character in unspecified POST array parameters.
CVE-2015-8626
The User::randomPassword function in MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1 generates passwords smaller than $wgMinimalPasswordLength, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2015-8627
MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1 do not properly normalize IP addresses containing zero-padded octets, which might allow remote attackers to bypass intended access restrictions by using an IP address that was not supposed to have been allowed.
CVE-2015-8628
The (1) Special:MyPage, (2) Special:MyTalk, (3) Special:MyContributions, (4) Special:MyUploads, and (5) Special:AllMyUploads pages in MediaWiki before 1.23.12, 1.24.x before 1.24.5, 1.25.x before 1.25.4, and 1.26.x before 1.26.1 allow remote attackers to obtain sensitive user login information via crafted links combined with page view statistics.
CVE-2015-8687
Multiple cross-site scripting (XSS) vulnerabilities in the Management Console in Alcatel-Lucent Motive Home Device Manager (HDM) before 4.2 allow remote attackers to inject arbitrary web script or HTML via the (1) deviceTypeID parameter to DeviceType/getDeviceType.do; the (2) policyActionClass or (3) policyActionName parameter to PolicyAction/findPolicyActions.do; the deviceID parameter to (4) SingleDeviceMgmt/getDevice.do or (5) device/editDevice.do; the operation parameter to (6) ajax.do or (7) xmlHttp.do; or the (8) policyAction, (9) policyClass, or (10) policyName parameter to policy/findPolicies.do.
CVE-2016-10046
Heap-based buffer overflow in the DrawImage function in magick/draw.c in ImageMagick before 6.9.5-5 allows remote attackers to cause a denial of service (application crash) via a crafted image file.
CVE-2016-10047
Memory leak in the NewXMLTree function in magick/xml-tree.c in ImageMagick before 6.9.4-7 allows remote attackers to cause a denial of service (memory consumption) via a crafted XML file.
CVE-2016-10048
Directory traversal vulnerability in magick/module.c in ImageMagick 6.9.4-7 allows remote attackers to load arbitrary modules via unspecified vectors.
CVE-2016-10049
Buffer overflow in the ReadRLEImage function in coders/rle.c in ImageMagick before 6.9.4-4 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted RLE file.
CVE-2016-10050
Heap-based buffer overflow in the ReadRLEImage function in coders/rle.c in ImageMagick 6.9.4-8 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted RLE file.
CVE-2016-10051
Use-after-free vulnerability in the ReadPWPImage function in coders/pwp.c in ImageMagick 6.9.5-5 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10052
Buffer overflow in the WriteProfile function in coders/jpeg.c in ImageMagick before 6.9.5-6 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10053
The WriteTIFFImage function in coders/tiff.c in ImageMagick before 6.9.5-8 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted file.
CVE-2016-10054
Buffer overflow in the WriteMAPImage function in coders/map.c in ImageMagick before 6.9.5-8 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10055
Buffer overflow in the WritePDBImage function in coders/pdb.c in ImageMagick before 6.9.5-8 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10056
Buffer overflow in the sixel_decode function in coders/sixel.c in ImageMagick before 6.9.5-8 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10057
Buffer overflow in the WriteGROUP4Image function in coders/tiff.c in ImageMagick before 6.9.5-8 allows remote attackers to cause a denial of service (application crash) or have other unspecified impact via a crafted file.
CVE-2016-10058
Memory leak in the ReadPSDLayers function in coders/psd.c in ImageMagick before 6.9.6-3 allows remote attackers to cause a denial of service (memory consumption) via a crafted image file.
CVE-2016-10059
Buffer overflow in coders/tiff.c in ImageMagick before 6.9.4-1 allows remote attackers to cause a denial of service (application crash) or have unspecified other impact via a crafted TIFF file.
CVE-2016-10254
The allocate_elf function in common.h in elfutils before 0.168 allows remote attackers to cause a denial of service (crash) via a crafted ELF file, which triggers a memory allocation failure.
CVE-2016-10255
The __libelf_set_rawdata_wrlock function in elf_getdata.c in elfutils before 0.168 allows remote attackers to cause a denial of service (crash) via a crafted (1) sh_off or (2) sh_size ELF header value, which triggers a memory allocation failure.
CVE-2016-1597
A logged-in user in NetIQ Access Governance Suite 6.0 through 6.4 could escalate privileges to administrator.
CVE-2016-1602
A code injection in the supportconfig data collection tool in supportutils in SUSE Linux Enterprise Server 12 and 12-SP1 and SUSE Linux Enterprise Desktop 12 and 12-SP1 could be used by local attackers to execute code as the user running supportconfig (usually root).
CVE-2016-1603
An information leak in the NetIQ IDM ServiceNow Driver before 1.0.0.1 could expose cryptographic attributes to logged-in users.
CVE-2016-5747
A security vulnerability in cookie handling in the http stack implementation in NDSD in Novell eDirectory before 9.0.1 allows remote attackers to bypass intended access restrictions by leveraging predictable cookies.
CVE-2016-5748
External Entity Processing (XXE) vulnerability in the "risk score" application of NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 could be used to disclose the content of local files to logged-in users.
CVE-2016-5749
NetIQ Access Manager 4.1 before 4.1.2 HF 1 and 4.2 before 4.2.2 was parsing incoming SAML requests with external entity resolution enabled, which could lead to local file disclosure via an XML External Entity (XXE) attack.
CVE-2016-5750
The certificate upload feature in iManager in NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 could be used to upload JSP pages that would be executed as the iManager user, allowing code execution by logged-in remote users.
CVE-2016-5751
An unfiltered finalizer target URL in the SAML processing feature in Identity Server in NetIQ Access Manager 4.1 before 4.1.2 HF1 and 4.2 before 4.2.2 could be used to trigger XSS and leak authentication credentials.
CVE-2016-5752
The SAML2 implementation in Identity Server in NetIQ Access Manager 4.1 before 4.1.2 HF1 and 4.2 before 4.2.2 was handling unsigned SAML requests incorrectly, leaking results to a potentially malicious "Assertion Consumer Service URL" instead of the original requester.
CVE-2016-5754
Presence of a .htaccess file could leak information in NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before SP2.
CVE-2016-5755
NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 was vulnerable to clickjacking attacks due to a missing SAMEORIGIN filter in the "high encryption" setting.
CVE-2016-5756
Multiple components of the web tools in NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 were vulnerable to Reflected Cross Site Scripting attacks which could be used to hijack user sessions: nps/servlet/frameservice, nps/servlet/webacc, roma/admin/cntl, roma/jsp/admin/appliance/devicedetail_edit.jsp, roma/jsp/admin/managementip/mgmt_ip_details_frameset.jsp, roma/jsp/admin/managementip/mgmt_ip_details_middleframe.jsp, roma/jsp/volsc/monitoring/appliance.jsp, and roma/jsp/volsc/monitoring/graph.jsp.
CVE-2016-5757
iManager Admin Console in NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 was vulnerable to iFrame manipulation attacks, which could allow remote users to gain access to authentication credentials.
CVE-2016-5758
A cross site request forgery protection mechanism in NetIQ Access Manager 4.1 before 4.1.2 Hot Fix 1 and 4.2 before 4.2.2 could be circumvented by repeated uploads causing a high load.
CVE-2016-6225
xbcrypt in Percona XtraBackup before 2.3.6 and 2.4.x before 2.4.5 does not properly set the initialization vector (IV) for encryption, which makes it easier for context-dependent attackers to obtain sensitive information from encrypted backup files via a Chosen-Plaintext attack. NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-6394.
CVE-2016-7468
An unauthenticated remote attacker may be able to disrupt services on F5 BIG-IP 11.4.1 - 11.5.4 devices with maliciously crafted network traffic. This vulnerability affects virtual servers associated with TCP profiles when the BIG-IP system's tm.tcpprogressive db variable value is set to non-default setting "enabled". The default value for the tm.tcpprogressive db variable is "negotiate". An attacker may be able to disrupt traffic or cause the BIG-IP system to fail over to another device in the device group.
CVE-2016-8885
The bmp_getdata function in libjasper/bmp/bmp_dec.c in JasPer before 1.900.9 allows remote attackers to cause a denial of service (NULL pointer dereference) by calling the imginfo command with a crafted BMP image.
CVE-2016-8886
The jas_malloc function in libjasper/base/jas_malloc.c in JasPer before 1.900.11 allows remote attackers to have unspecified impact via a crafted file, which triggers a memory allocation failure.
CVE-2016-8887
The jp2_colr_destroy function in libjasper/jp2/jp2_cod.c in JasPer before 1.900.10 allows remote attackers to cause a denial of service (NULL pointer dereference).
CVE-2016-9011
The wmf_malloc function in api.c in libwmf 0.2.8.4 allows remote attackers to cause a denial of service (application crash) via a crafted wmf file, which triggers a memory allocation failure.
CVE-2016-9167
NDSD in Novell eDirectory before 9.0.2 did not calculate ACLs on LDAP objects across partition boundaries correctly, which could lead to a privilege escalation by modifying user attributes that would otherwise be filtered by an ACL.
CVE-2016-9168
A missing X-Frame-Options header in the NDS Utility Monitor in NDSD in Novell eDirectory before 9.0.2 could be used by remote attackers for clickjacking.
CVE-2016-9169
A reflected XSS vulnerability exists in the web console of the Document Viewer Agent in Novell GroupWise before 2014 R2 Support Pack 1 Hot Patch 2 that may enable a remote attacker to execute JavaScript in the context of a valid user's browser session by getting the user to click on a specially crafted link. This could lead to session compromise or other browser-based attacks.
CVE-2016-9262
Multiple integer overflows in the (1) jas_realloc function in base/jas_malloc.c and (2) mem_resize function in base/jas_stream.c in JasPer before 1.900.22 allow remote attackers to cause a denial of service via a crafted image, which triggers use after free vulnerabilities.
CVE-2016-9264
Buffer overflow in the printMP3Headers function in listmp3.c in Libming 0.4.7 allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted mp3 file.
CVE-2016-9265
The printMP3Headers function in listmp3.c in Libming 0.4.7 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted mp3 file.
CVE-2016-9266
listmp3.c in libming 0.4.7 allows remote attackers to unspecified impact via a crafted mp3 file, which triggers an invalid left shift.
CVE-2016-9275
Heap-based buffer overflow in the _dwarf_skim_forms function in libdwarf/dwarf_macro5.c in Libdwarf before 20161124 allows remote attackers to cause a denial of service (out-of-bounds read).
CVE-2016-9276
The dwarf_get_aranges_list function in dwarf_arrange.c in Libdwarf before 20161124 allows remote attackers to cause a denial of service (out-of-bounds read).
CVE-2016-9387
Integer overflow in the jpc_dec_process_siz function in libjasper/jpc/jpc_dec.c in JasPer before 1.900.13 allows remote attackers to have unspecified impact via a crafted file, which triggers an assertion failure.
CVE-2016-9388
The ras_getcmap function in ras_dec.c in JasPer before 1.900.14 allows remote attackers to cause a denial of service (assertion failure) via a crafted image file.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9389
The jpc_irct and jpc_iict functions in jpc_mct.c in JasPer before 1.900.14 allow remote attackers to cause a denial of service (assertion failure).
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9390
The jas_seq2d_create function in jas_seq.c in JasPer before 1.900.14 allows remote attackers to cause a denial of service (assertion failure) via a crafted image file.
CVE-2016-9391
The jpc_bitstream_getbits function in jpc_bs.c in JasPer before 2.0.10 allows remote attackers to cause a denial of service (assertion failure) via a very large integer.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9392
The calcstepsizes function in jpc_dec.c in JasPer before 1.900.17 allows remote attackers to cause a denial of service (assertion failure) via a crafted file.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9393
The jpc_pi_nextrpcl function in jpc_t2cod.c in JasPer before 1.900.17 allows remote attackers to cause a denial of service (assertion failure) via a crafted file.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9394
The jas_seq2d_create function in jas_seq.c in JasPer before 1.900.17 allows remote attackers to cause a denial of service (assertion failure) via a crafted file.
CVE-2016-9395
The jas_seq2d_create function in jas_seq.c in JasPer before 1.900.25 allows remote attackers to cause a denial of service (assertion failure) via a crafted file.
CVE-2016-9396
The JPC_NOMINALGAIN function in jpc/jpc_t1cod.c in JasPer through 2.0.12 allows remote attackers to cause a denial of service (JPC_COX_RFT assertion failure) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9397
The jpc_dequantize function in jpc_dec.c in JasPer 1.900.13 allows remote attackers to cause a denial of service (assertion failure) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9398
The jpc_floorlog2 function in jpc_math.c in JasPer before 1.900.17 allows remote attackers to cause a denial of service (assertion failure) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9399
The calcstepsizes function in jpc_dec.c in JasPer 1.900.22 allows remote attackers to cause a denial of service (assertion failure) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/617.html">CWE-617: Reachable Assertion</a>
CVE-2016-9556
The IsPixelGray function in MagickCore/pixel-accessor.h in ImageMagick 7.0.3-8 allows remote attackers to cause a denial of service (out-of-bounds heap read) via a crafted image file.
CVE-2016-9557
Integer overflow in jas_image.c in JasPer before 1.900.25 allows remote attackers to cause a denial of service (application crash) via a crafted file.
CVE-2016-9774
The postinst script in the tomcat6 package before 6.0.45+dfsg-1~deb7u4 on Debian wheezy, before 6.0.35-1ubuntu3.9 on Ubuntu 12.04 LTS and on Ubuntu 14.04 LTS; the tomcat7 package before 7.0.28-4+deb7u8 on Debian wheezy, before 7.0.56-3+deb8u6 on Debian jessie, before 7.0.52-1ubuntu0.8 on Ubuntu 14.04 LTS, and on Ubuntu 12.04 LTS, 16.04 LTS, and 16.10; and the tomcat8 package before 8.0.14-1+deb8u5 on Debian jessie, before 8.0.32-1ubuntu1.3 on Ubuntu 16.04 LTS, before 8.0.37-1ubuntu0.1 on Ubuntu 16.10, and before 8.0.38-2ubuntu1 on Ubuntu 17.04 might allow local users with access to the tomcat account to obtain sensitive information or gain root privileges via a symlink attack on the Catalina localhost directory.
CVE-2016-9775
The postrm script in the tomcat6 package before 6.0.45+dfsg-1~deb7u3 on Debian wheezy, before 6.0.45+dfsg-1~deb8u1 on Debian jessie, before 6.0.35-1ubuntu3.9 on Ubuntu 12.04 LTS and on Ubuntu 14.04 LTS; the tomcat7 package before 7.0.28-4+deb7u7 on Debian wheezy, before 7.0.56-3+deb8u6 on Debian jessie, before 7.0.52-1ubuntu0.8 on Ubuntu 14.04 LTS, and on Ubuntu 12.04 LTS, 16.04 LTS, and 16.10; and the tomcat8 package before 8.0.14-1+deb8u5 on Debian jessie, before 8.0.32-1ubuntu1.3 on Ubuntu 16.04 LTS, before 8.0.37-1ubuntu0.1 on Ubuntu 16.10, and before 8.0.38-2ubuntu1 on Ubuntu 17.04 might allow local users with access to the tomcat account to gain root privileges via a setgid program in the Catalina directory, as demonstrated by /etc/tomcat8/Catalina/attack.
CVE-2017-5206
Firejail before 0.9.44.4, when running on a Linux kernel before 4.8, allows context-dependent attackers to bypass a seccomp-based sandbox protection mechanism via the --allow-debuggers argument.
CVE-2017-5207
Firejail before 0.9.44.4, when running a bandwidth command, allows local users to gain root privileges via the --shell argument.
CVE-2017-5227
QNAP QTS before 4.2.4 Build 20170313 allows local users to obtain sensitive Domain Administrator password information by reading data in an XOR format within the /etc/config/uLinux.conf configuration file.
CVE-2017-5524
Plone 4.x through 4.3.11 and 5.x through 5.0.6 allow remote attackers to bypass a sandbox protection mechanism and obtain sensitive information by leveraging the Python string format method.
CVE-2017-5538
The kbase_dispatch function in arm/t7xx/r5p0/mali_kbase_core_linux.c in the GPU driver on Samsung devices with M(6.0) and N(7.0) software and Exynos AP chipsets allows attackers to have unspecified impact via unknown vectors, which trigger an out-of-bounds read, aka SVE-2016-6362.
CVE-2017-5897
The ip6gre_err function in net/ipv6/ip6_gre.c in the Linux kernel allows remote attackers to have unspecified impact via vectors involving GRE flags in an IPv6 packet, which trigger an out-of-bounds access.
CVE-2017-6191
Buffer overflow in APNGDis 2.8 and below allows a remote attacker to execute arbitrary code via a crafted filename.
CVE-2017-6359
QNAP QTS before 4.2.4 Build 20170313 allows attackers to gain administrator privileges and execute arbitrary commands via unspecified vectors.
CVE-2017-6360
QNAP QTS before 4.2.4 Build 20170313 allows attackers to gain administrator privileges and obtain sensitive information via unspecified vectors.
CVE-2017-6361
QNAP QTS before 4.2.4 Build 20170313 allows attackers to execute arbitrary commands via unspecified vectors.
CVE-2017-6517
Microsoft Skype 7.16.0.102 contains a vulnerability that could allow an unauthenticated, remote attacker to execute arbitrary code on the targeted system. This vulnerability exists due to the way .dll files are loaded by Skype. It allows an attacker to load a .dll of the attacker's choosing that could execute arbitrary code without the user's knowledge.The specific flaw exists within the handling of DLL (api-ms-win-core-winrt-string-l1-1-0.dll) loading by the Skype.exe process.
CVE-2017-6895
USB Pratirodh allows remote attackers to conduct XML External Entity (XXE) attacks via XML data in usb.xml.
CVE-2017-6911
USB Pratirodh is prone to sensitive information disclosure. It stores sensitive information such as username and password in simple usb.xml. An attacker with physical access to the system can modify the file according his own requirements that may aid in further attack.
CVE-2017-6950
SAP GUI 7.2 through 7.5 allows remote attackers to bypass intended security policy restrictions and execute arbitrary code via a crafted ABAP code, aka SAP Security Note 2407616.
CVE-2017-7199
Nessus 6.6.2 - 6.10.3 contains a flaw related to insecure permissions that may allow a local attacker to escalate privileges when the software is running in Agent Mode. Version 6.10.4 fixes this issue.
CVE-2017-7235
An issue was discovered in cloudflare-scrape 1.6.6 through 1.7.1. A malicious website owner could craft a page that executes arbitrary Python code against any cfscrape user who scrapes that website. This is fixed in 1.8.0.
CVE-2017-7242
Multiple Cross-Site Scripting (XSS) were discovered in admin/modules components in SLiMS 7 Cendana through 2017-03-23: the keywords parameter to bibliography/checkout_item.php, bibliography/dl_print.php, bibliography/item.php, bibliography/item_barcode_generator.php, bibliography/printed_card.php, circulation/loan_rules.php, master_file/author.php, master_file/coll_type.php, and master_file/doc_language.php and the quickReturnID field to circulation/ajax_action.php.
CVE-2017-7244
The _pcre32_xclass function in pcre_xclass.c in libpcre1 in PCRE 8.40 allows remote attackers to cause a denial of service (invalid memory read) via a crafted file.
CVE-2017-7245
Stack-based buffer overflow in the pcre32_copy_substring function in pcre_get.c in libpcre1 in PCRE 8.40 allows remote attackers to cause a denial of service (WRITE of size 4) or possibly have unspecified other impact via a crafted file.
CVE-2017-7246
Stack-based buffer overflow in the pcre32_copy_substring function in pcre_get.c in libpcre1 in PCRE 8.40 allows remote attackers to cause a denial of service (WRITE of size 268) or possibly have unspecified other impact via a crafted file.
CVE-2017-7247
Multiple Cross-Site Scripting (XSS) were discovered in Gazelle before 2017-03-19. The vulnerabilities exist due to insufficient filtration of user-supplied data (torrents, size) passed to the 'Gazelle-master/sections/tools/managers/multiple_freeleech.php' URL. An attacker could execute arbitrary HTML and script code in a browser in the context of the vulnerable website.
CVE-2017-7248
A Cross-Site Scripting (XSS) was discovered in Gazelle before 2017-03-19. The vulnerability exists due to insufficient filtration of user-supplied data (type) passed to the 'Gazelle-master/sections/better/transcode.php' URL. An attacker could execute arbitrary HTML and script code in a browser in the context of the vulnerable website.
CVE-2017-7249
Multiple Cross-Site Scripting (XSS) were discovered in Gazelle before 2017-03-19. The vulnerabilities exist due to insufficient filtration of user-supplied data (action, userid) passed to the 'Gazelle-master/sections/tools/data/ocelot_info.php' URL. An attacker could execute arbitrary HTML and script code in a browser in the context of the vulnerable website.
CVE-2017-7250
A Cross-Site Scripting (XSS) was discovered in Gazelle before 2017-03-19. The vulnerability exists due to insufficient filtration of user-supplied data (action) passed to the 'Gazelle-master/sections/tools/finances/bitcoin_balance.php' URL. An attacker could execute arbitrary HTML and script code in a browser in the context of the vulnerable website.
CVE-2017-7251
A Cross-Site Scripting (XSS) was discovered in pi-engine/pi 2.5.0. The vulnerability exists due to insufficient filtration of user-supplied data (preview) passed to the "pi-develop/www/script/editor/markitup/preview/markdown.php" URL. An attacker could execute arbitrary HTML and script code in a browser in the context of the vulnerable website.
