#!/bin/bash
# $1 = path to yearly CVE file from NIST
./fullextract_description.sh $1
./fullextract_CVE_CPE_URLs.sh $1
./fullextract_CPE_URL_KEYWORDS.sh $1