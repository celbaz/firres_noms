#!/bin/bash

# $1 = name of an experiment
echo "================ EVALUATION SUMMARY ================"
total=$(jq '.samples | length'  $1/tfidf.automated.evaluation.json )
echo "Number of evaluated vulnerabilities : " $total
echo "===================================================="
echo "First relevant keyword position, starting at index 0 (-1 = No relevant keyword found)"
jq '.samples[].first_relevant_keyword_position' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2" "$1" => "$1/total*100"%"}'

echo "===================================================="
echo "Number of necessary keywords for reconstruction, starting at index 0 (-1 = No reconstruction was possible)."
jq '.samples[].number_of_necessary_keywords_for_reconstruction' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2 "  " $1" => "$1/total*100"%"}'

echo "===================================================="
echo "Distribution of keyword list length, before truncation."
jq '.samples[].keyword_untruncated_list_length' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2" "$1" => "$1/total*100"%"}'

echo "===================================================="
echo "Distribution of keyword list length, after truncation."
jq '.samples[].keyword_truncated_list_length' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2" "$1" => "$1/total*100"%"}'

echo "===================================================="
echo "First relevant keyword position taking truncation into account, starting at index 0 (-1 = No relevant keyword found)"
jq '.samples[] | if .first_relevant_keyword_position <= .keyword_truncated_list_length then .first_relevant_keyword_position else -1 end' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2" "$1" => "$1/total*100"%"}'

echo "===================================================="
echo "Number of necessary keywords for reconstruction taking truncation into account, starting at index 0 (-1 = No reconstruction was possible)."
jq '.samples[] | if .number_of_necessary_keywords_for_reconstruction <= .keyword_truncated_list_length then .number_of_necessary_keywords_for_reconstruction else -1 end' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk -v total=$total '{ print $2" "$1" => "$1/total*100"%"}'

echo "===================================================="

jq '.samples[].first_relevant_keyword_position' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/first_relevant_keyword_position.csv
jq '.samples[].number_of_necessary_keywords_for_reconstruction' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/number_of_necessary_keywords_for_reconstruction.csv
jq '.samples[].keyword_untruncated_list_length' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/keyword_untruncated_list_length.csv
jq '.samples[].keyword_truncated_list_length' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/keyword_truncated_list_length.csv
jq '.samples[] | if .first_relevant_keyword_position <= .keyword_truncated_list_length then .first_relevant_keyword_position else -1 end' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/first_relevant_keyword_position_truncated.csv
jq '.samples[] | if .number_of_necessary_keywords_for_reconstruction <= .keyword_truncated_list_length then .number_of_necessary_keywords_for_reconstruction else -1 end' $1/tfidf.automated.evaluation.json | sort -n | uniq -c | awk '{ print $2","$1}' > $1/number_of_necessary_keywords_for_reconstruction_truncated.csv

jq -r '.samples[] | [.highest_keyword_weight, .first_relevant_keyword_position] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_HKW_FRKP.csv
jq -r '.samples[] | [.untruncated_keyword_vector_norm, .number_of_necessary_keywords_for_reconstruction] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_UKVN_NNKR.csv
jq -r '.samples[] | [.truncated_keyword_vector_norm, .first_relevant_keyword_position] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_TKVN_FRKP.csv
jq -r '.samples[] | [.highest_keyword_weight, .number_of_necessary_keywords_for_reconstruction] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_HKW_NNKR.csv
jq -r '.samples[] | [.untruncated_keyword_vector_norm, .first_relevant_keyword_position] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_UKVN_FRKP.csv
jq -r '.samples[] | [.truncated_keyword_vector_norm, .number_of_necessary_keywords_for_reconstruction] | @csv' $1/tfidf.automated.evaluation.json | sort -n > $1/correlation_TKVN_NNKR.csv
jq -r '.samples | to_entries[] | [.key, .value.description, .value.top_three_keywords] | flatten | @csv' $1/tfidf.automated.evaluation.json > $1/manual_xp.csv
